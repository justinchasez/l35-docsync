using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Entities.Payments;
using NUnit.Framework;

namespace DocSync.Tests.Server.Core.Entities
{
    [TestFixture]
    public class PaymentPlanTests 
    {
        private PaymentPlan plan;

        [SetUp]
        public void Setup()
        {
            plan = new PaymentPlan(new RegisteredUser());
        }

        [Test]
        public void PayedMoney_SinglePaymentMade_ReturnsSumOfPayments()
        {
            var computer = new Computer(plan);
            int expectedPayedMoney = 20;
            var prolongComputerPayment = new ProlongComputerPayment(computer)
                                             {
                                                 Amount = expectedPayedMoney
                                             };
            computer.Payments.Add(prolongComputerPayment);
            plan.Computers.Add(computer);

            double result = plan.PayedMoney;

            Assert.That(result, Is.EqualTo(expectedPayedMoney));
        }

        [Test]
        public void ContainsComputerWithName_ComputerIsInPlan_ReturnsTrue()
        {
            const string computerName = "name";
            var computer = new Computer(plan)
                               {
                                   Name = computerName
                               };

            plan.Computers.Add(computer);

            bool result = plan.ContainsComputerWithName(computerName);

            Assert.That(result, Is.True);
        }

        [Test]
        public void ContainsComputerWithName_ComputerIsNotInPlan_ReturnsFalse()
        {
            bool result = plan.ContainsComputerWithName("name");

            Assert.That(result, Is.False);
        }

        [Test]
        public void GetComputerById_ComputerWithIdPresentInPaymentPlan_ReturnsComputer()
        {
            const int computerId = 1;

            Computer expectedComputer = new Computer(plan);
            expectedComputer.SetIdTo(computerId);

            plan.Computers.Add(expectedComputer);

            Computer computerById = plan.GetComputerById(computerId);

            Assert.That(computerById, Is.SameAs(expectedComputer));
        }

        [Test]
        public void GetComputerById_ComputerIsNotInPlan_ReturnsNull()
        {
            Computer computerById = plan.GetComputerById(1);

            Assert.That(computerById, Is.Null);
        }

        [Test]
        public void AddTrialComputer_Called_AddsNewComputerWithNotRegisteredName()
        {
            const string computerName = "computer";
            
            plan.AddTrialComputer(computerName, 1, false);

            Assert.That(plan.Computers.Count, Is.EqualTo(1));

            Computer computer = plan.Computers.First();

            Assert.That(computer.Name, Is.EqualTo(computerName));
        }
    }
}