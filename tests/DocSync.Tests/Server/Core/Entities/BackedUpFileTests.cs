using NUnit.Framework;

namespace DocSync.Tests.Server.Core.Entities
{
    [TestFixture]
    public class BackedUpFileTests
    {
        private BackedUpFile file;

        [SetUp]
        public void Setup()
        {
            file = new BackedUpFile(new Computer(new PaymentPlan(new RegisteredUser())));
        }

        [Test]
        public void ctr_Called_SetsBackedUpFileVersionsCollection()
        {
            Assert.That(file.FileVersions, Is.Not.Null);
        }

        [Test]
        public void GetSpecificVersion_VersionIsInCollection_ReturnsVersionInstance()
        {
            const int fileVersion = 5;
            BackedUpFileVersion backedUpFileVersion = new BackedUpFileVersion(file)
                                                          {
                                                              FileVersion = fileVersion
                                                          };
            file.FileVersions.Add(backedUpFileVersion);

            BackedUpFileVersion result = file.GetSpecificVersion(fileVersion);

            Assert.That(result, Is.SameAs(backedUpFileVersion));
        }

        [Test]
        public void GetSpecificVersion_VersionIsNotInCollection_ReturnsNull()
        {
            BackedUpFileVersion backedUpFileVersion = new BackedUpFileVersion(file)
            {
                FileVersion = 5
            };
            file.FileVersions.Add(backedUpFileVersion);

            BackedUpFileVersion result = file.GetSpecificVersion(6);

            Assert.That(result, Is.Null);
        }
    }
}