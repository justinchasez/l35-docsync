using NUnit.Framework;

namespace DocSync.Tests.Server.Core.Entities
{
    [TestFixture]
    public class RegisteredUserTests
    {
        [Test]
        public void ctr_Called_SetsPaymentPlan()
        {
            RegisteredUser registeredUser = new RegisteredUser();
            Assert.That(registeredUser.PaymentPlan, Is.Not.Null, "Payment plan must be created with user");
        }
    }
}