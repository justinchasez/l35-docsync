using System;
using NUnit.Framework;

namespace DocSync.Tests.Server.Core.Entities
{
    [TestFixture]
    public class ComputerTests
    {
        private Computer computer;

        [SetUp]
        public void Setup()
        {
            computer = new Computer(new PaymentPlan(new RegisteredUser()));
        }

        [Test]
        public void ctr_called_SetsBackedUpFilesCollection()
        {
            Assert.That(computer.Files, Is.Not.Null);
        }

        [Test]
        public void ctr_Called_SetsPayedPeriodsCollection()
        {
            Assert.That(computer.PayedPeriods, Is.Not.Null);
        }

        [Test]
        public void ctr_Called_SetsPayments()
        {
            Assert.That(computer.Payments, Is.Not.Null);
        }

        [Test]
        public void GetEndDate_PayedPeriodSet_ReturnsMaxEndDate()
        {
            DateTime expectedEndDate = new DateTime(2010, 5, 5);
            PayedPeriod payedPeriod = new PayedPeriod(computer)
                                          {
                                              StartDate = new DateTime(2010, 1, 1),
                                              EndDate = expectedEndDate
                                          };
            computer.PayedPeriods.Add(payedPeriod);

            DateTime endDate = computer.GetEndDate();

            Assert.That(endDate, Is.EqualTo(expectedEndDate));
        }

        [Test]
        public void GetStartDate_PayedPeriodSet_ReturnsMinStartDate()
        {
            DateTime expectedStartDate = new DateTime(2010, 1, 1);
            PayedPeriod payedPeriod = new PayedPeriod(computer)
                                        {
                                            StartDate = expectedStartDate,
                                            EndDate = new DateTime(2010, 5, 5)
                                        };
            computer.PayedPeriods.Add(payedPeriod);

            DateTime startDate = computer.GetStartDate();

            Assert.That(startDate, Is.EqualTo(expectedStartDate));
        }

        [Test]
        public void SetStartDate_PayedPeriodSet_SetsStartDate()
        {
            PayedPeriod payedPeriod = new PayedPeriod(computer)
                                        {
                                            StartDate = new DateTime(2010, 1, 1),
                                            EndDate = new DateTime(2010, 5, 5)
                                        };
            computer.PayedPeriods.Add(payedPeriod);

            DateTime expectedStartDate = new DateTime(2009, 1, 1);
            computer.SetStartDate(expectedStartDate);

            Assert.That(computer.GetStartDate(), Is.EqualTo(expectedStartDate));
        }

        [Test]
        public void SetEndDate_PayedPeriodSet_SetsEndDate()
        {
            PayedPeriod payedPeriod = new PayedPeriod(computer)
            {
                StartDate = new DateTime(2010, 1, 1),
                EndDate = new DateTime(2010, 5, 5)
            };
            computer.PayedPeriods.Add(payedPeriod);

            DateTime expectedEndDate = new DateTime(2011, 1, 1);
            computer.SetEndDate(expectedEndDate);

            Assert.That(computer.GetEndDate(), Is.EqualTo(expectedEndDate));
        }

        [Test]
        public void ToString_WithComputerNameSet_ReturnsComputerName()
        {
            const string expectedName = "name";
            computer.Name = expectedName;

            string result = computer.ToString();

            Assert.That(result, Is.EqualTo(expectedName));
        }
    }
}