using System.Web.Security;
using NUnit.Framework;
using Rhino.Mocks;

namespace DocSync.Tests.Server.Services
{
    [TestFixture]
    public class RegistrationServiceTests
    {
        [Test]
        public void Register_CalledWithValidParameters_RegistersUser()
        {
            IMembershipService membershipService = MockRepository.GenerateStub<IMembershipService>();
            const string userName = "username";
            const string password = "password";
            const string email = "email";
            const string sourse = "source";
            membershipService.Expect(x => x.CreateUser(userName, password, email, sourse))
                .Return(MembershipCreateStatus.Success);

            UserRegistrationInputs registrationInputs = new UserRegistrationInputs
            {
                Password = password,
                Email = email
            };

            IUsersService usersService = new UsersService(membershipService);
            UserRegistrationResult createResult = usersService.Register(registrationInputs);

            Assert.That(createResult.CreateStatus == MembershipCreateStatus.Success);
            Assert.That(createResult.ErrorMessage, Is.Null, "If user registered error message should be empty");
        }

        [Test]
        public void Register_RegistrationUnsuccessful_ReturnsError()
        {
            var membershipService = MockRepository.GenerateStub<IMembershipService>();
            membershipService.Expect(x => x.CreateUser(null, null, null, null))
                .IgnoreArguments()
                .Return(MembershipCreateStatus.DuplicateUserName);

            UsersService usersService = new UsersService(membershipService);
            UserRegistrationResult userRegistrationResult = usersService.Register(new UserRegistrationInputs());

            Assert.That(userRegistrationResult.CreateStatus, Is.EqualTo(MembershipCreateStatus.DuplicateUserName));
            Assert.That(userRegistrationResult.ErrorMessage, 
                Is.EqualTo(AccountValidation.ErrorCodeToString(MembershipCreateStatus.DuplicateUserName)));
        }

        [Test]
        public void ValidateUser_CredentialsAreValid_ReturnsTrue()
        {
            var membershipService = MockRepository.GenerateStub<IMembershipService>();
            const string userName = "user name";
            const string password = "password";
            membershipService.Expect(x => x.ValidateUser(userName, password))
                .Return(true);

            IUsersService usersService = new UsersService(membershipService);
            bool validationResult = usersService.ValidateUser(userName, password);

            Assert.That(validationResult, Is.True, "User credentials are valid");
        }
    }
}