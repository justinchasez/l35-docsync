using System;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Services;
using DocSync.Server.Services;
using DocSync.Server.Services.Dtos;
using DocSync.Tests.Server.Fakes;
using NUnit.Framework;
using Rhino.Mocks;
using SharpArch.Core;

namespace DocSync.Tests.Server.Services
{
    [TestFixture]
    public class FilesServiceTests
    {
        private const string CurrentUserName = "Current user";

        [SetUp]
        public void TestFixtureSetup()
        {
            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(CurrentUserName, "Membership Provider"), new string[] { });
        }

        [Test]
        public void BackupFile_CalledFirstTimeForFile_CreatesNewFile()
        {
            // Arrange
            Computer computer = ComputerFakes.GetComputerWithOwner(CurrentUserName);
            var computersRepository = ComputerFakes.GetRepositoryWithComputer(computer);
            
            FilesService service = GetService(computersRepository);
            BackupFileInputs fileInputs = new BackupFileInputs
                                      {
                                          File = "C:/test",
                                          FileOld = "C:/test",
                                          Size = 500
                                      };

            // Act
            service.BackupFile(fileInputs);

            // Assert
            Assert.That(computer.Files.Count, Is.EqualTo(1), "A new file should be added to files collection of specified computer");
            BackedUpFile backedUpFile = computer.Files.First();

            Assert.That(backedUpFile.ClientPath, Is.EqualTo(fileInputs.File));

            Assert.That(backedUpFile.FileVersions.Count, Is.EqualTo(1), "A file version should be created");

            BackedUpFileVersion fileVersion = backedUpFile.FileVersions.First();
            Assert.That(fileVersion.Size, Is.EqualTo(fileInputs.Size));
        }

        [Test]
        public void BackupFile_CalledForBackedUpFile_CreatesNewFileVersion()
        {
            Computer computerWithFile = ComputerFakes.GetComputerWithFile("C:/file.exe");
            BackedUpFileVersion backedUpFileVersion = computerWithFile.Files.First().GetLastVersion();
            backedUpFileVersion.IsFinished = true;
            backedUpFileVersion.FileHash = "hash";

            IComputersRepository repositoryWithComputer = ComputerFakes.GetRepositoryWithComputer(computerWithFile);

            FilesService filesService = GetService(repositoryWithComputer);

            BackupFileInputs backupFileInputs = new BackupFileInputs
                                            {
                                                FileOld = "C:/file.exe",
                                                File = "C:/file.exe",
                                                FileHash = "hash1"
                                            };
            filesService.BackupFile(backupFileInputs);

            Assert.That(computerWithFile.Files.First().FileVersions.Count, Is.EqualTo(2), "New file version should be created and added to the computer files collection");
        }

        [Test]
        public void BackupFile_CalledForNewFile_StoresFileHash()
        {
            Computer computer = ComputerFakes.GetComputerWithOwner(CurrentUserName);
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);

            var service = GetService(repository);

            string fileHash = "fileHash";
            BackupFileInputs backupFileInputs = new BackupFileInputs
                                            {
                                                FileOld = "C:/temp.exe",
                                                File = "C:/temp.exe",
                                                FileHash = fileHash
                                            };
            service.BackupFile(backupFileInputs);

            
            Assert.That(computer.Files.First().GetLastVersion().FileHash, Is.EqualTo(fileHash), "Service should set file hash value to the last file version");
        }

        [Test]
        public void BackupFile_CalledWithEmptyFileName_ThrowsPreconditionException()
        {
            var service = GetService(MockRepository.GenerateStub<IComputersRepository>());

            Assert.Throws(typeof (PreconditionException), () => service.BackupFile(new BackupFileInputs()));
        }

        [Test]
        public void BackupFile_CalledForNewFile_SotresFileSize()
        {
            Computer computer = ComputerFakes.GetComputerWithOwner(CurrentUserName);
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);

            var service = GetService(repository);

            BackupFileInputs backupFileInputs = new BackupFileInputs
            {
                File = "C:/temp.exe",
                FileOld = "C:/temp.exe",
                Size = 500
            };
            service.BackupFile(backupFileInputs);

            Assert.That(computer.Files.First().GetLastVersion().Size, Is.EqualTo(backupFileInputs.Size), "Service should set file size value to the last file version");
        }

        [Test]
        public void BackupFile_CalledForNewFile_ChangesTotalUsedSpaceOnComputer()
        {
            Computer computer = ComputerFakes.GetComputerWithOwner(CurrentUserName);
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);
            
            FilesService service = GetService(repository);

            BackupFileInputs inputs = new BackupFileInputs();
            inputs.File = "C:/temp.exe";
            inputs.Size = 500;

            service.BackupFile(inputs);

            Assert.That(computer.BytesUsed, Is.EqualTo(500));
        }

        [Test]
        public void BackupFile_CalledForFinishedFileWithNewHash_StoresFileSize()
        {
            Computer computer = ComputerFakes.GetComputerWithFile("C:/temp.exe");
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);

            var service = GetService(repository);

            BackupFileInputs backupFileInputs = new BackupFileInputs
                                                    {
                                                        ComputerId = 1,
                                                        File = "C:/temp.exe",
                                                        FileOld = "C:/temp.exe",
                                                        FileHash = "hash",
                                                        Size = 500
                                                    };
            service.BackupFile(backupFileInputs);

            Assert.That(computer.Files.First().GetLastVersion().Size, Is.EqualTo(500));
        }

        [Test]
        public void BackupFile_CalledForFinishedFileWithNewHash_StoresFileHash()
        {
            Computer computer = ComputerFakes.GetComputerWithFile("C:/temp.exe");
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);

            var service = GetService(repository);

            BackupFileInputs backupFileInputs = new BackupFileInputs
            {
                ComputerId = 1,
                File = "C:/temp.exe",
                FileOld = "C:/temp.exe",
                FileHash = "hash",
                Size = 500
            };
            service.BackupFile(backupFileInputs);

            Assert.That(computer.Files.First().GetLastVersion().FileHash, Is.EqualTo("hash"));
        }

        [Test]
        public void BackupFile_CalledForUnFinishedFileWithChangedHash_DeletesNotFinishedVersionAndCreatesNewOne()
        {
            const string filePath = "C:/file.exe";

            Computer computer = ComputerFakes.GetComputerWithFile(filePath);
            computer.Files.First().GetLastVersion().FileHash = "hash";
            IComputersRepository repository = ComputerFakes.GetRepositoryWithComputer(computer);

            IS3Service s3Service = MockRepository.GenerateMock<IS3Service>();
            IBackedUpFilePartRepository backedUpFilePartRepository =
                MockRepository.GenerateMock<IBackedUpFilePartRepository>();

            // Shkil
            //FilesService service = GetService(s3Service, repository, backedUpFilePartRepository);
            //BackupFileInputs backupFileInputs = new BackupFileInputs
            //                                        {
            //                                            File = filePath,
            //                                            FileOld = filePath,
            //                                            FileHash = "new hash"
            //                                        };
            //service.BackupFile(backupFileInputs);

            //Assert.That(computer.Files.First().FileVersions.Count, Is.EqualTo(1));

            //s3Service.AssertWasCalled(x => x.DeleteFileVersion(null), m => m.IgnoreArguments());
        }

        [Test]
        public void BackupPartFinished_PartSizePassed_CreatesAndSavesPartSizeAndS3PartId()
        {
            Computer computer = ComputerFakes.GetComputerWithFile("C:/temp");
            IComputersRepository computersRepository = ComputerFakes.GetRepositoryWithComputer(computer);


            const int partLength = 4500;
            Guid s3PartId = Guid.NewGuid();
            var partBackupFinishedInputs = new PartBackupFinishedInputs
                                            {
                                                File = "C:/temp",
                                                PartLength = partLength,
                                                S3PartId = s3PartId,
                                                ComputerId = 1
                                            };

            FilesService service = GetService(computersRepository);
            service.BackupPartFinished(partBackupFinishedInputs);

            Assert.That(computer.Files.First().GetLastVersion().Parts.Count, Is.EqualTo(1), "New file part should be created and added");

            BackedUpFilePart backedUpFilePart = computer.Files.First().GetLastVersion().GetLastPart();

            Assert.That(backedUpFilePart.Length, Is.EqualTo(partLength), "Part length sould be set to the last part");
            Assert.That(backedUpFilePart.S3FileId, Is.EqualTo(s3PartId), "S3 Part id should be stored");
        }   

        [Test]
        public void BackupFileFinished_CalledForNotFinishedFile_MarksFileAsFinished()
        {
            Computer computer = ComputerFakes.GetComputerWithFile("C:/temp.tmp");
            var repository = ComputerFakes.GetRepositoryWithComputer(computer);

            var service = this.GetService(repository);

            FileBackupFinishedInputs fileBackupFinishedInputs = new FileBackupFinishedInputs
                                                                    {
                                                                        ComputerId = 1,
                                                                        File = "C:/temp.tmp"
                                                                    };
            service.BackupFileFinished(fileBackupFinishedInputs);

            Assert.That(computer.Files.First().GetLastVersion().IsFinished, Is.True);
        }

        [Test]
        public void BackupFileFinished_CalledForNotExistingComputer_ThowsPreconditionException()
        {
            IComputersRepository computersRepository = MockRepository.GenerateStub<IComputersRepository>();

            var service = this.GetService(computersRepository);

            Assert.Throws<PreconditionException>(() => service.BackupFileFinished(new FileBackupFinishedInputs()));
        }

        [Test]
        public void BackupFile_CalledForNotFinishedFileWithSameHash_ReturnsPartsNumber()
        {
            const string notFinishedFileName = "C:/file.exe";
            const string fileHash = "fileHash";

            Computer computer = ComputerFakes.GetComputerWithFile(notFinishedFileName);
            BackedUpFileVersion lastVersion = computer.Files.First().GetLastVersion();

            lastVersion.Parts.Add(new BackedUpFilePart(lastVersion));
            lastVersion.FileHash = fileHash;

            IComputersRepository repositoryWithComputer = ComputerFakes.GetRepositoryWithComputer(computer);


            BackupFileInputs backupFileInputs = new BackupFileInputs
            {
                File = notFinishedFileName,
                FileOld = notFinishedFileName,
                FileHash = fileHash
            };
            FilesService filesService = GetService(repositoryWithComputer);

            BackupFileResult backupFileResult = filesService.BackupFile(backupFileInputs);

            Assert.That(backupFileResult.IsFinished, Is.False);
            Assert.That(backupFileResult.PartsCount, Is.EqualTo(1));
        }

        [Test]
        public void BackupFile_CalledForFinisedFileWithSameHash_ReturnsIsFinishedTrue()
        {
            const string backedUpFileName = "C:/temp.exe";
            const string backedUpFileHash = "file hash";

            Computer computer = ComputerFakes.GetComputerWithFile(backedUpFileName);
            BackedUpFileVersion backedUpFileVersion = computer.Files.First().GetLastVersion();
            backedUpFileVersion.FileHash = backedUpFileHash;
            backedUpFileVersion.IsFinished = true;
            IComputersRepository repositoryWithComputer = ComputerFakes.GetRepositoryWithComputer(computer);

            BackupFileInputs inputs = new BackupFileInputs
                                          {
                                              File = backedUpFileName,
                                              FileOld = backedUpFileName, 
                                              FileHash = backedUpFileHash
                                          };
            FilesService filesService = GetService(repositoryWithComputer);
            BackupFileResult backupFileResult = filesService.BackupFile(inputs);

            Assert.That(backupFileResult.IsFinished, Is.True);
        }

        [Test]
        public void GetFileInfo_CalledForFinishedFile_ReturnsFileInfo()
        {
            const string backedUpFileName = "C:/temp.exe";
            Computer computer = ComputerFakes.GetComputerWithFile(backedUpFileName);
            BackedUpFileVersion backedUpFileVersion = computer.Files.First().GetLastVersion();
            backedUpFileVersion.IsFinished = true;

            IComputersRepository repositoryWithComputer = ComputerFakes.GetRepositoryWithComputer(computer);

            FilesService filesService = GetService(repositoryWithComputer);
            GetFileInfoInputs getFileInfoInputs = new GetFileInfoInputs
                                                      {
                                                          File = backedUpFileName
                                                      };
            FileInfoDto fileInfoDto = filesService.GetFileInfo(getFileInfoInputs);

            Assert.That(fileInfoDto.FileVersions.Count, Is.EqualTo(1));
        }

        [Test]
        public void RestoreFile_CalledForFinishedFile_ReturnsFilePartsInfo()
        {
            const string backedUpFileName = "C:/file.exe";
            Computer computer = ComputerFakes.GetComputerWithFile(backedUpFileName);
            BackedUpFileVersion backedUpFileVersion = computer.Files.First().GetLastVersion();
            backedUpFileVersion.IsFinished = true;
            backedUpFileVersion.Parts.Add(new BackedUpFilePart(backedUpFileVersion));
            IComputersRepository repositoryWithComputer = ComputerFakes.GetRepositoryWithComputer(computer);

            FilesService filesService = GetService(repositoryWithComputer);

            RestoreFileInputs restoreFileInputs = new RestoreFileInputs
                                                      {
                                                          File = backedUpFileName
                                                      };
            RestoreFileResult restoreFileResult = filesService.RestoreFile(restoreFileInputs);

            Assert.That(restoreFileResult.FileParts.Count, Is.EqualTo(1));
        }


        private FilesService GetService(IComputersRepository repository)
        {
            var result = GetService(MockRepository.GenerateStub<IS3Service>(),
                                    repository,
                                    MockRepository.GenerateStub<IBackedUpFilePartRepository>(),
                                    MockRepository.GenerateStub<IBackedUpFileRepository>());
            return result;
        }

        private FilesService GetService(IS3Service s3Service, IComputersRepository computersRepository, IBackedUpFilePartRepository backedUpFilePartRepository, IBackedUpFileRepository backedUpFileRepository)
        {
            FilesService result = new FilesService(computersRepository, s3Service, backedUpFilePartRepository, backedUpFileRepository);

            return result;
        }
    }
}