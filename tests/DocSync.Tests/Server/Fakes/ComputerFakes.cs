using Rhino.Mocks;

namespace DocSync.Tests.Server.Fakes
{
    public static class ComputerFakes
    {
        public static Computer GetComputerWithOwner(string ownerName)
        {
            RegisteredUser user = new RegisteredUser
                                      {
                                          Name = ownerName
                                      };

            Computer result = new Computer(user.PaymentPlan);
            user.PaymentPlan.Computers.Add(result);

            return result;
        }

        public static Computer GetComputerWithFile(string filePath)
        {
            Computer computer = GetComputerWithOwner("someOwner");
            BackedUpFile file = new BackedUpFile(computer)
                                    {
                                        ClientPath = filePath
                                    };
            BackedUpFileVersion version = new BackedUpFileVersion(file);
            
            file.FileVersions.Add(version);
            computer.Files.Add(file);

            return computer;
        }

        public static IComputersRepository GetRepositoryWithComputer(Computer computer)
        {
            IComputersRepository computersRepository = MockRepository.GenerateStub<IComputersRepository>();
            computersRepository.Expect(x => x.Get(1))
                .IgnoreArguments()
                .Return(computer);

            return computersRepository;
        }
    }
}