﻿using System;
using Castle.MicroKernel;

namespace DocSync.Server.ServiceLocation
{
	/// <summary>
	/// Release policy for Lifecycled Components
	/// </summary>
	[Serializable]
	public class LifecycledComponentsReleasePolicy : Castle.MicroKernel.Releasers.LifecycledComponentsReleasePolicy
	{
		/// <summary>
		/// instantiateAndForgetIt Type
		/// </summary>
		private readonly Type instantiateAndForgetItType = typeof(InstantiateAndForgetIt);

		/// <summary>
		/// Tracks the specified instance.
		/// </summary>
		/// <param name="instance">The instance.</param>
		/// <param name="burden">The burden.</param>
		public override void Track(object instance, Burden burden)
		{
			if (this.instantiateAndForgetItType.Equals(burden.Model.CustomLifestyle))
			{
				return;
			}

			base.Track(instance, burden);
		}
	}
}