using System.Linq;
using Castle.MicroKernel.Registration;

namespace DocSync.Server.ServiceLocation
{
    /// <summary>
    /// Castle windsor extensions
    /// </summary>
    public static class WindsorExtensions
    {
        /// <summary>
        /// Searches for the first interface found associated with the
        /// <see cref="ServiceDescriptor"/> which is not generic.
        /// </summary>
        /// <param name="descriptor">The descriptor.</param>
        /// <returns>BasedOnDescriptor type</returns>
        public static BasedOnDescriptor FirstNonGenericInterface(this ServiceDescriptor descriptor)
        {
            return descriptor.Select((type, baseType) =>
                                         {
                                             var interfaces = type
                                                 .GetInterfaces()
                                                 .Where(t => t.IsGenericType == false);

                                             if (interfaces.Count() > 0)
                                             {
                                                 return new[] { interfaces.ElementAt(0) };
                                             }

                                             return null;
                                         });
        }
    }
}