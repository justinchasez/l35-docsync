using Castle.Core;
using Castle.Facilities.FactorySupport;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CommonServiceLocator.WindsorAdapter;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Services;
using DocSync.Server.Core.Services.Implementations;
using DocSync.Server.Core.Settings;
using DocSync.Server.Core.Settings.Implementations;
using DocSync.Server.Data;
using DocSync.Server.Data.Repositories;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.ServiceLocation
{
    /// <summary>
    /// Used for IoC configuration
    /// </summary>
    public class ServiceLocatorInitializer
    {
        /// <summary>
        /// Initializes Castle Windsor.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void AddComponentsTo(IWindsorContainer container)
        {
            container.AddFacility<FactorySupportFacility>();
            container.Register(Component.For<ISessionFactory>()
                                   .LifeStyle.Singleton
                                   .UsingFactoryMethod(() => new NhibernateConfigurator().CreateSessionFactory()));

            container.Register(Component.For<ISession>()
                                   .LifeStyle.PerWebRequest
                                   .UsingFactoryMethod(kernel => kernel.Resolve<ISessionFactory>().OpenSession()));

            container.Register(Component.For<IMembershipService>()
                                   .ImplementedBy<AccountMembershipService>().LifeStyle.Transient);

            container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>().LifeStyle.Transient);
            container.Register(Component.For<IProductSettings>().ImplementedBy<ProductSettings>().LifeStyle.Transient);
            container.Register(Component.For<IS3Service>().ImplementedBy<S3Service>().LifeStyle.Transient);
            container.Register(Component.For<IS3Settings>().ImplementedBy<S3Settings>().LifeStyle.Transient);

            RegisterRepositories(container);

            ServiceLocator.SetLocatorProvider(() => new WindsorServiceLocator(container));
        }

        /// <summary>
        /// Adds the components for services to.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void AddComponentsForServicesTo(IWindsorContainer container)
        {
			container.Kernel.ReleasePolicy = new LifecycledComponentsReleasePolicy();
			container.Kernel.ComponentModelCreated += KernelComponentModelCreated;
            container.AddFacility<FactorySupportFacility>();
            container.Register(Component.For<ISessionFactory>()
                                   .LifeStyle.Singleton
                                   .UsingFactoryMethod(() => new NhibernateConfigurator().CreateSessionFactory()));

            container.Register(Component.For<ISession>()
                                   .LifeStyle.PerWebRequest
                                   .UsingFactoryMethod(kernel => kernel.Resolve<ISessionFactory>().OpenSession()));

            container.Register(Component.For<IMembershipService>()
                                   .ImplementedBy<AccountMembershipService>());

            container.Register(Component.For<IEmailService>().ImplementedBy<EmailService>());
            container.Register(Component.For<IProductSettings>().ImplementedBy<ProductSettings>());
            container.Register(Component.For<IS3Service>().ImplementedBy<S3Service>());
            container.Register(Component.For<IS3Settings>().ImplementedBy<S3Settings>());

            container.AddComponent("repositoryType", typeof(IRepository<>), typeof(Repository<>));
            container.AddComponent("repositoryWithTypedId", typeof(IRepositoryWithTypedId<,>), typeof(RepositoryWithTypedId<,>));
            container.Register(Component.For<IDbContext>().ImplementedBy<DbContext>().LifeStyle.PerWcfOperation());

            //container.Register(AllTypes.Pick()
            //                       .FromAssembly(typeof(StaticPagesRepository).Assembly)
            //                       .WithService.FirstNonGenericInterface()
            //                       .Configure(m => m.LifeStyle.Transient));

            container.Register(Component.For<IBackedUpFilePartRepository>().ImplementedBy<BackedUpFilePartRepository>());
            container.Register(Component.For<IBackedUpFileRepository>().ImplementedBy<BackedUpFileRepository>());
            container.Register(Component.For<IBackedUpFileVersionRepository>().ImplementedBy<BackedUpFileVersionRepository>());
            container.Register(Component.For<IComputersRepository>().ImplementedBy<ComputersRepository>());
            container.Register(Component.For<IFileLinksRepository>().ImplementedBy<FileLinksRepository>());
            container.Register(Component.For<IMessagesRepository>().ImplementedBy<MailMessagesRepository>());
            container.Register(Component.For<IPaymentsRepository>().ImplementedBy<PaymentsRepository>());
            container.Register(Component.For<ISentMessagesRepository>().ImplementedBy<SentMessagesRepository>());
            container.Register(Component.For<IStaticPagesRepository>().ImplementedBy<StaticPagesRepository>());
            container.Register(Component.For<IUsersRepository>().ImplementedBy<UsersRepository>());

            ServiceLocator.SetLocatorProvider(() => new WindsorServiceLocator(container));
        }

        /// <summary>
        /// Registers the repositories.
        /// </summary>
        /// <param name="container">The container.</param>
        private static void RegisterRepositories(IWindsorContainer container)
        {
            container.AddComponentLifeStyle("repositoryType", typeof(IRepository<>), typeof(Repository<>), LifestyleType.Transient);
            container.AddComponentLifeStyle("repositoryWithTypedId", typeof(IRepositoryWithTypedId<,>), typeof(RepositoryWithTypedId<,>), LifestyleType.Transient);
            container.Register(Component.For<IDbContext>().ImplementedBy<DbContext>().LifeStyle.PerWebRequest);

            container.Register(AllTypes.Pick()
                                   .FromAssembly(typeof(StaticPagesRepository).Assembly)
                                   .WithService.FirstNonGenericInterface()
                                   .Configure(m => m.LifeStyle.Transient));
        }
		
		/// <summary>
		/// Kernels the component model created.
		/// </summary>
		/// <param name="model">The model.</param>
		private static void KernelComponentModelCreated(ComponentModel model)
		{
			if (model.LifestyleType == LifestyleType.Undefined)
			{
				model.LifestyleType = LifestyleType.Custom;
				model.CustomLifestyle = typeof(InstantiateAndForgetIt);
			}
		}
    }
}