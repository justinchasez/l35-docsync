﻿using System;
using Castle.Core;
using Castle.MicroKernel;

namespace DocSync.Server.ServiceLocation
{
	/// <summary>
	/// InstantiateAndForgetIt lifestyle
	/// </summary>
	[Serializable]
	public class InstantiateAndForgetIt : ILifestyleManager
	{
		/// <summary>
		/// component activator instance
		/// </summary>
		private IComponentActivator componentActivator;

		/// <summary>
		/// Initializes the <c>ILifestyleManager</c> with the
		/// <see cref="T:Castle.MicroKernel.IComponentActivator"/>
		/// </summary>
		/// <param name="componentActivator">instance creation logic</param>
		/// <param name="kernel">the functionality the MicroKernel implements</param>
		/// <param name="model">meta information collected about a component</param>
		public void Init(IComponentActivator componentActivator, IKernel kernel, ComponentModel model)
		{
			this.componentActivator = componentActivator;
		}

		/// <summary>
		/// Implementors should return the component instance based
		/// on the lifestyle semantic.
		/// </summary>
		/// <param name="context">The creation context</param>
		/// <returns>component instance</returns>
		public object Resolve(CreationContext context)
		{
			return this.componentActivator.Create(context);
		}

		/// <summary>
		/// Implementors should release the component instance based
		/// on the lifestyle semantic, for example, singleton components
		/// should not be released on a call for release, instead they should
		/// release them when disposed is invoked.
		/// </summary>
		/// <param name="instance">component instance</param>
		/// <returns>result of operation</returns>
		public bool Release(object instance)
		{
			return true;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
		}
	}
}