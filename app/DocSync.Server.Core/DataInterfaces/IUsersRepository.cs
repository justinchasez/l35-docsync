using System;
using System.Collections.Generic;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// Users repository.
    /// </summary>
    public interface IUsersRepository : IRepositoryWithTypedId<RegisteredUser, Guid>
    {
        /// <summary>
        /// Gets the user by name.
        /// </summary>
        /// <param name="name">The name of user.</param>
        /// <returns><see cref="RegisteredUser"/> instance.</returns>
        RegisteredUser GetUserByName(string name);

        /// <summary>
        /// Searches the specified users by dto search criteria.
        /// </summary>
        /// <param name="dto">The dto search criteria.</param>
        /// <returns><see cref="UsersSearchResult"/> instance</returns>
        UsersSearchResult Search(UsersSearchCriteria dto);

        /// <summary>
        /// Gets the full user info with payment plan and computer with files fetched.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <returns><see cref="RegisteredUser"/> instance.</returns>
        RegisteredUser GetFullUserInfo(Guid id);

        /// <summary>
        /// Gets users sources width users count
        /// </summary>
        /// <returns>Dictionary of users sources width users count</returns>
        Dictionary<string, int> GetUsersSources();

        #region Statistics Information

        /// <summary>
        /// Gets the signed up count.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        int GetSignedUpCount(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the paid in range users count.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        int GetPaidInRangeUsersCount(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the amount of data for trial users.
        /// </summary>
        /// <returns></returns>
        long GetAmountOfDataForTrialUsers();

        /// <summary>
        /// Gets the amount of data for paid users.
        /// </summary>
        /// <returns></returns>
        long GetAmountOfDataForPaidUsers();

        /// <summary>
        /// Gets the paid users count.
        /// </summary>
        /// <returns></returns>
        int GetPaidUsersCount();

        /// <summary>
        /// Gets the paid computers count.
        /// </summary>
        /// <returns></returns>
        int GetPaidComputersCount();

        /// <summary>
        /// Gets the trial users count.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        int GetTrialUsersCount(DateTime startDate, DateTime endDate);

        #endregion

    }
}