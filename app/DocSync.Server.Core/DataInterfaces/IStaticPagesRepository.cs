using System.Collections.Generic;
using DocSync.Server.Core.Entities.StaticPages;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// Static pages repository
    /// </summary>
    public interface IStaticPagesRepository
    {
        /// <summary>
        /// Gets all static pages.
        /// </summary>
        /// <returns>List of static pages</returns>
        IList<StaticPage> GetAll();

        /// <summary>
        /// Gets the page by URL.
        /// </summary>
        /// <param name="pageUrl">The page URL.</param>
        /// <returns>Static page if page founded, otherwise <see langword="null"/></returns>
        StaticPage GetPageByUrl(string pageUrl);

        /// <summary>
        /// Saves the or updates a static page.
        /// </summary>
        /// <param name="staticPage">The static page.</param>
        /// <returns>Saved static page</returns>
        StaticPage SaveOrUpdate(StaticPage staticPage);
    }
}