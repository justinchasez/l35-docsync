using System.Collections.Generic;
using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// <see cref="BackedUpFile"/> repository.
    /// </summary>
    public interface IBackedUpFileRepository : IRepository<BackedUpFile>
    {
        IList<BackedUpFile> GetAllInClientPath(Computer computer, string clientPath);
    }
}