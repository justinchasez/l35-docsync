﻿using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    public interface IFileLinksRepository : IRepository<FileLink>
    {
    }
}
