using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// <see cref="Computer"/> repository.
    /// </summary>
    public interface IComputersRepository : IRepository<Computer>
    {
        /// <summary>
        /// Sets the total bytes used for specify computer.
        /// </summary>
        void BytesUsed(Computer computer);
    }
}