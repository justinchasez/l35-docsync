using System.Collections.Generic;
using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// Sent messages repository.
    /// </summary>
    public interface ISentMessagesRepository : IRepository<SentMessage>
    {
        /// <summary>
        /// Gets the messages paged.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List of paged messages.</returns>
        IList<SentMessage> GetMessagesPaged(int pageNumber, int pageSize);

        /// <summary>
        /// Gets the messages count.
        /// </summary>
        /// <returns>Total number of messages.</returns>
        int GetMessagesCount();
    }
}