using System.Collections.Generic;
using DocSync.Server.Core.Entities.Payments;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// Payments repository.
    /// </summary>
    public interface IPaymentsRepository : IRepository<Payment> 
    {
        /// <summary>
        /// Gets the payments paged.
        /// </summary>
        /// <param name="pageNumber">The page number. 1 based index.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>Paged payments.</returns>
        IList<Payment> GetPaymentsPaged(int pageNumber, int pageSize);

        /// <summary>
        /// Gets total number of payments.
        /// </summary>
        /// <returns>Total number of all payments.</returns>
        int GetPaymentsCount();
    }
}