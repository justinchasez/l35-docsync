using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// <see cref="BackedUpFilePart"/> repository.
    /// </summary>
    public interface IBackedUpFilePartRepository : IRepository<BackedUpFilePart>
    {
    }
}