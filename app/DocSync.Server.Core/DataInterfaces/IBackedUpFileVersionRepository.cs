using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    public interface IBackedUpFileVersionRepository : IRepository<BackedUpFileVersion>
    {
        /// <summary>
        /// Gets the last file version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        BackedUpFileVersion GetLastVersion(BackedUpFile file);

        /// <summary>
        /// Gets the last finished version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        BackedUpFileVersion GetLastFinishedVersion(BackedUpFile file);
    }
}