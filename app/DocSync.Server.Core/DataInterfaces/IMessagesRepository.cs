using System.Collections.Generic;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.DataInterfaces
{
    /// <summary>
    /// Messages repository.
    /// </summary>
    public interface IMessagesRepository : IRepository<InternalMailMessage>
    {
        /// <summary>
        /// Gets the messages paged.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List of founded messages.</returns>
        IList<InternalMailMessage> GetMessagesPaged(int pageNumber, int pageSize);

        /// <summary>
        /// Gets the total number of messages.
        /// </summary>
        /// <returns>Total number of messages.</returns>
        int GetTotalNumberOfMessages();

        /// <summary>
        /// Gets the sorted messages paged.
        /// </summary>
        /// <param name="messagesCriteria">The messages criteria.</param>
        /// <returns>List of founded messages.</returns>
        IList<InternalMailMessage> GetSortedMessagesPaged(SortableMessagesCriteria messagesCriteria);
    }
}