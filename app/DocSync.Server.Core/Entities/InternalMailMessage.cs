using System;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Internal mail message.
    /// </summary>
    public class InternalMailMessage : Entity
    {
        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        /// <value>The creation date.</value>
        public virtual DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>The subject.</value>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>The message body.</value>
        public virtual string Body { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is read.
        /// </summary>
        /// <value><c>true</c> if this instance is read; otherwise, <c>false</c>.</value>
        public virtual bool IsRead { get; set; }
    }
}