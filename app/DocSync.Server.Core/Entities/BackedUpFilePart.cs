using System;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// One part of the file.
    /// </summary>
    public class BackedUpFilePart : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFilePart"/> class.
        /// </summary>
        protected BackedUpFilePart()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFilePart"/> class.
        /// </summary>
        /// <param name="version">The version.</param>
        public BackedUpFilePart(BackedUpFileVersion version)
        {
            this.BackedUpFileVersion = version;
        }

        /// <summary>
        /// Gets or sets the length part.
        /// </summary>
        /// <value>The part length.</value>
        public virtual int Length { get; set; }

        /// <summary>
        /// Gets or sets the file id on s3 storage.
        /// </summary>
        /// <value>The s3 file id.</value>
        public virtual Guid S3FileId { get; set; }

        /// <summary>
        /// Gets or sets the part number.
        /// </summary>
        /// <value>The part number.</value>
        public virtual int Number { get; set; }

        /// <summary>
        /// Gets or sets the file version.
        /// </summary>
        /// <value>The version.</value>
        public virtual BackedUpFileVersion BackedUpFileVersion { get; protected set; }
    }
}