using System;
using System.Diagnostics;
using System.Linq;
using Iesi.Collections.Generic;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// User's payment plan.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, UserName = {User.Name}, Computers = {Computers.Count}")]
    public class PaymentPlan : Entity
    {
        /// <summary>
        /// Associated computers list.
        /// </summary>
        private ISet<Computer> computers;

        /// <summary>
        /// Gets the payed money.
        /// </summary>
        /// <value>The payed money.</value>
        public virtual double PayedMoney
        {
            get
            {
                double result = this.Computers.SelectMany(m => m.Payments).Sum(x => x.Amount);
                                
                return result;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPlan"/> class.
        /// </summary>
        protected PaymentPlan()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPlan"/> class.
        /// </summary>
        /// <param name="user">The registered user.</param>
        public PaymentPlan(RegisteredUser user)
        {
            this.User = user;
            this.computers = new HashedSet<Computer>();
        }

        /// <summary>
        /// Gets the owner.
        /// </summary>
        /// <value>The registered user.</value>
        public virtual RegisteredUser User { get; protected set; }

        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <value>The computers.</value>
        public virtual ISet<Computer> Computers
        {
            get
            {
                return this.computers;
            }
        }

        /// <summary>
        /// Determines whether plan contains computer with the specified name.
        /// </summary>
        /// <param name="name">The name of computer.</param>
        /// <returns>
        /// 	<c>true</c> if plan contains computer with the specified name; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool ContainsComputerWithName(string name)
        {
            return this.GetComputerByName(name) != null;
        }

        /// <summary>
        /// Gets the computer by id.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <returns><see langword="null"/> if computer was not founded.</returns>
        public virtual Computer GetComputerById(int computerId)
        {
            var computer = (from c in this.Computers
                            where c.Id == computerId
                            select c).FirstOrDefault();
            return computer;
        }

        /// <summary>
        /// Gets the computer by name.
        /// </summary>
        /// <param name="computerName">Name of the computer.</param>
        /// <returns><see langword="null"/> if not founded</returns>
        public virtual Computer GetComputerByName(string computerName)
        {
            var computer = (from c in this.Computers
                            where c.Name.Equals(computerName, StringComparison.InvariantCultureIgnoreCase)
                            select c).FirstOrDefault();
            return computer;
        }

        /// <summary>
        /// Adds the trial computer.
        /// </summary>
        /// <param name="computerName">Name of the computer.</param>
        /// <param name="backupLimit">The backup limit.</param>
        /// <param name="isManualEncryptionEnabled">if set to <c>true</c> [is manual encryption enabled].</param>
        public virtual void AddTrialComputer(string computerName, int? backupLimit, bool isManualEncryptionEnabled)
        {
          Computer computer = new Computer(this)
                                    {
                                        Name = computerName,
                                        BackupLimit = backupLimit,
                                        IsManualEncryptionEnabled = isManualEncryptionEnabled
                                    };
            PayedPeriod period = new PayedPeriod(computer)
                                     {
                                         StartDate = DateTime.Now,
                                         EndDate = DateTime.Now.AddDays(90)
                                     };
            computer.PayedPeriods.Add(period);
            this.Computers.Add(computer);
        }
    }
}