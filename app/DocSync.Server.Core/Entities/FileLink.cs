﻿using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// The file link
    /// </summary>
    public class FileLink : Entity
    {
        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        /// <value>The link.</value>
        public virtual string Link { get; set; }

        /// <summary>
        /// Gets or sets the client path.
        /// </summary>
        /// <value>The client path.</value>
        public virtual string Path { get; set; }

        /// <summary>
        /// Gets or sets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public virtual Computer Computer { get; set; }
    }
}
