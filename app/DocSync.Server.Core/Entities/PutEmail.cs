using System;
using System.Diagnostics;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    [DebuggerDisplay("Id = {Id}, Email = {Email}, DateTime = {DateTime}")]
    public class PutEmail : Entity
    {
        public virtual string Email { get; set; }

        public virtual DateTime DateTime { get; set; }
    }
}