using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents key value pair. (required for removing settings from App settings)
    /// </summary>
    public class SettingEntry : ValueObject
    {
        /// <summary>
        /// Gets the setting key.
        /// </summary>
        /// <value>The setting key.</value>
        public virtual string Key { get; protected set; }

        /// <summary>
        /// Gets the setting value.
        /// </summary>
        /// <value>The setting value.</value>
        public virtual string Value { get; set; }
    }
}