using System;
using System.Linq;
using Iesi.Collections.Generic;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents one version of backed up file.
    /// </summary>
    public class BackedUpFileVersion : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFileVersion"/> class.
        /// </summary>
        protected BackedUpFileVersion()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFileVersion"/> class.
        /// </summary>
        /// <param name="backedUpFile">The backed up file.</param>
        public BackedUpFileVersion(BackedUpFile backedUpFile)
        {
            this.BackedUpFile = backedUpFile;
            this.Parts = new HashedSet<BackedUpFilePart>();
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public virtual int FileVersion { get; set; }

        /// <summary>
        /// Gets or sets the file size.
        /// </summary>
        /// <value>The file size.</value>
        public virtual long Size { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is uploaded completely.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if upload is for this file is finished; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsFinished { get; set; }

        /// <summary>
        /// Gets or sets the file hash.
        /// </summary>
        /// <value>The file hash.</value>
        public virtual string FileHash { get; set; }

        /// <summary>
        /// Gets or sets the date when backup was finished.
        /// </summary>
        /// <value>The finised date.</value>
        public virtual DateTime? FinisedDate { get; set; }

        /// <summary>
        /// Gets or sets the last saved on.
        /// </summary>
        /// <value>The last saved on.</value>
        public virtual DateTime? LastSavedOn { get; set; }

        /// <summary>
        /// Gets the file parts.
        /// </summary>
        /// <value>The file parts.</value>
        public virtual ISet<BackedUpFilePart> Parts { get; protected set; }

        /// <summary>
        /// Gets or sets the backed up file.
        /// </summary>
        /// <value>The backed up file.</value>
        public virtual BackedUpFile BackedUpFile { get; protected set; }

        /// <summary>
        /// Gets the last file part.
        /// </summary>
        /// <returns><see cref="BackedUpFilePart"/> instance.</returns>
        public virtual BackedUpFilePart GetLastPart()
        {
            return (from p in this.Parts
                    where p.Number == this.Parts.Max(x => x.Number)
                    select p).SingleOrDefault();
        }
    }
}