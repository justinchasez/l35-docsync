using System;
using System.Diagnostics;
using System.Linq;
using Iesi.Collections.Generic;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Registered user entity
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    public class RegisteredUser : EntityWithTypedId<Guid>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegisteredUser"/> class.
        /// </summary>
        public RegisteredUser()
        {
            this.friends = new HashedSet<Friend>();
        }

        /// <summary>
        /// Used only after registration
        /// all other cases - is empty
        /// </summary>
        public virtual String Password { get; set; }

        /// <summary>
        /// Associated friends list.
        /// </summary>
        private ISet<Friend> friends;

        /// <summary>
        /// Current payment plan.
        /// </summary>
        private PaymentPlan paymentPlan;

        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>The registration date.</value>
        public virtual DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public virtual string Email { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name of user.</value>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the lowered user.
        /// </summary>
        /// <value>The name of the lowered user.</value>
        public virtual string LoweredUserName { get; set; }

        /// <summary>
        /// Gets or sets the heard about.
        /// </summary>
        /// <value>The heard about.</value>
        public virtual string HeardAbout { get; set; }

        /// <summary>
        /// Gets the payment plan.
        /// </summary>
        /// <value>The payment plan.</value>
        public virtual PaymentPlan PaymentPlan
        {
            get
            {
                if (this.paymentPlan == null)
                {
                    this.paymentPlan = new PaymentPlan(this);
                }

                return this.paymentPlan;
            }

            protected set
            {
                this.paymentPlan = value;
            }
        }

        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <value>The computers.</value>
        public virtual ISet<Friend> Friends
        {
            get
            {
                return this.friends;
            }
        }

        /// <summary>
        /// Sets the name of the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        public virtual void SetUserNameWithEmail(string userName)
        {
            this.Name = userName;
            this.Email = userName;
            this.LoweredUserName = userName.ToLower();
        }

        /// <summary>
        /// Adds the friend.
        /// </summary>
        /// <param name="friend">The friend.</param>
        public virtual void AddFriend(Friend friend)
        {
            var list = this.friends.ToList();

            if (!this.friends.Contains(friend))
            {
                this.friends.Add(friend);
            }
        }
    }
}