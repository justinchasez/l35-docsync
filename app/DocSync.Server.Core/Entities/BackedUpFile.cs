using System.Diagnostics;
using System.Linq;
using Iesi.Collections.Generic;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents single backed up file.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, ClientPath = {ClientPath}")]
    public class BackedUpFile : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFile"/> class.
        /// </summary>
        protected BackedUpFile()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackedUpFile"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public BackedUpFile(Computer computer)
        {
            this.FileVersions = new HashedSet<BackedUpFileVersion>();
            this.Computer = computer;
        }

        /// <summary>
        /// Gets or sets the file path on client computer.
        /// </summary>
        /// <value>The client path.</value>
        public virtual string ClientPath { get; set; }

        /// <summary>
        /// Gets or sets the special client path.
        /// </summary>
        /// <value>The special client path.</value>
        public virtual string SpecialClientPath { get; set; }

        /// <summary>
        /// Gets or sets the file versions.
        /// </summary>
        /// <value>The file versions.</value>
        public virtual ISet<BackedUpFileVersion> FileVersions { get; protected set; }

        /// <summary>
        /// Gets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public virtual Computer Computer { get; protected set; }

        /// <summary>
        /// Gets the last file version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        public virtual BackedUpFileVersion GetLastVersion()
        {
            return (from f in this.FileVersions
                    where f.FileVersion == this.FileVersions.Max(x => x.FileVersion)
                    select f).SingleOrDefault();
        }

        /// <summary>
        /// Gets the last finished version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        public virtual BackedUpFileVersion GetLastFinishedVersion()
        {
            var finishedVersions = this.FileVersions.Where(f => f.IsFinished);
            return (from f in finishedVersions
                    where f.FileVersion == finishedVersions.Max(x => x.FileVersion)
                    select f).SingleOrDefault();
        }

        /// <summary>
        /// Gets the last version number.
        /// </summary>
        /// <returns>last version number. If there is no versions yet - 1</returns>
        public virtual int GetLastVersionNumber()
        {
            if (this.FileVersions.IsEmpty)
            {
                return 1;
            }

            return this.FileVersions.Max(x => x.FileVersion);
        }

        /// <summary>
        /// Gets the specific file version.
        /// </summary>
        /// <param name="version">The required version.</param>
        /// <returns><see cref="BackedUpFileVersion"/> instance if founded, otherwise false.</returns>
        public virtual BackedUpFileVersion GetSpecificVersion(int version)
        {
            BackedUpFileVersion backedUpFileVersion = (this.FileVersions.Where(fv => fv.FileVersion == version)).FirstOrDefault();
            return backedUpFileVersion;
        }
    }
}