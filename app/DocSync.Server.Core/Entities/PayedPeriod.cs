using System;
using System.Diagnostics;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents single payment period
    /// </summary>
    [DebuggerDisplay("Id = {Id}, StartDate = {StartDate}, EndDate = {EndDate}")]
    public class PayedPeriod : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PayedPeriod"/> class.
        /// </summary>
        protected PayedPeriod()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PayedPeriod"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public PayedPeriod(Computer computer)
        {
            Computer = computer;
        }
        
        /// <summary>
        /// Gets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public virtual Computer Computer { get; protected set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public virtual DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public virtual DateTime EndDate { get; set; }
    }
}