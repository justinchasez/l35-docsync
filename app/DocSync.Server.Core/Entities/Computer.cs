using System;
using System.Diagnostics;
using System.Linq;
using DocSync.Server.Core.Entities.Payments;
using Iesi.Collections.Generic;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents user's computer.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Name = {Name}")]
    public class Computer : Entity
    {
        /// <summary>
        /// How many bytes were placed in gigabayte.
        /// </summary>
        private const long BYTES_IN_GB = 1073741824;

        /// <summary>
        /// Initializes a new instance of the <see cref="Computer"/> class.
        /// </summary>
        protected Computer()
        {
            this.InitMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Computer"/> class.
        /// </summary>
        /// <param name="paymentPlan">The payment plan.</param>
        public Computer(PaymentPlan paymentPlan)
        {
            PaymentPlan = paymentPlan;
            this.InitMembers();
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The computer name.</value>
        public virtual string Name { get; set; }

        /// <summary>
        /// Gets or sets the local computer GUID.
        /// </summary>
        /// <value>The local computer GUID.</value>
        public virtual string LocalComputerGuid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether manual encription enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if manual encription enabled; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsManualEncryptionEnabled { get; set; }

        /// <summary>
        /// Gets or sets the backup limit.
        /// </summary>
        /// <value>The backup limit.</value>
        public virtual int? BackupLimit { get; set; }

        /// <summary>
        /// Gets the payment plan.
        /// </summary>
        /// <value>The payment plan.</value>
        public virtual PaymentPlan PaymentPlan { get; protected set; }

        /// <summary>
        /// Gets the payed periods.
        /// </summary>
        /// <value>The payed periods.</value>
        public virtual ISet<PayedPeriod> PayedPeriods { get; protected set; }

        /// <summary>
        /// Gets or sets the prolong payments.
        /// </summary>
        /// <value>The prolong payments.</value>
        public virtual ISet<Payment> Payments { get; protected set; }

        /// <summary>
        /// Gets or sets the files.
        /// </summary>
        /// <value>The backed up files.</value>
        public virtual ISet<BackedUpFile> Files { get; protected set; }

        /// <summary>
        /// Gets or sets the local users.
        /// </summary>
        /// <value>The local users from computer.</value>
        public virtual ISet<ComputerLocalUser> LocalUsers { get; protected set; }

        /// <summary>
        /// Gets the total bytes used by this computer.
        /// </summary>
        /// <value>The bytes used.</value>
        public virtual long BytesUsed
        {
            //get; set;
            get
            {
                long result = this.Files.SelectMany(file => file.FileVersions)
                                        .Where(version => version.IsFinished)
                                        .Sum(backedUpFileVersion => backedUpFileVersion.Size);
                return result;
            }
        }

        /// <summary>
        /// Gets the free space.
        /// </summary>
        /// <value>The free space.</value>
        public virtual long? FreeSpace
        {
            get
            {
                if (this.BackupLimit == null)
                {
                    return null;
                }

                long limitInBytes = this.BackupLimit.Value * BYTES_IN_GB;
                long result = limitInBytes - this.BytesUsed;
                return (result > 0) ? result : 0;
            }
        }

        /// <summary>
        /// Gets the date when computer was fist started.
        /// </summary>
        /// <returns>Start date.</returns>
        public virtual DateTime GetStartDate()
        {
            DateTime startDate = (from p in this.PayedPeriods select p.StartDate).Min();
            return startDate;
        }

        /// <summary>
        /// Gets the latest expiration date.
        /// </summary>
        /// <returns>End payment plan date.</returns>
        public virtual DateTime GetEndDate()
        {
            DateTime endDate = (from p in this.PayedPeriods select p.EndDate).Max();
            return endDate;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Inits the members.
        /// </summary>
        private void InitMembers()
        {
            this.PayedPeriods = new HashedSet<PayedPeriod>();
            this.Payments = new HashedSet<Payment>();
            this.Files = new HashedSet<BackedUpFile>();
            this.LocalUsers = new HashedSet<ComputerLocalUser>();

            
        }

        /// <summary>
        /// Sets the start date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        public virtual void SetStartDate(DateTime startDate)
        {
            this.PayedPeriods.First().StartDate = startDate;
        }

        /// <summary>
        /// Sets the end date.
        /// </summary>
        /// <param name="endDate">The end date.</param>
        public virtual void SetEndDate(DateTime endDate)
        {
            this.PayedPeriods.Last().EndDate = endDate;
        }

        /// <summary>
        /// Gets the file specified by name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns><see langword="null"/> if file not founded, otherwise <see cref="BackedUpFile"/> instance.</returns>
        public virtual BackedUpFile GetFileNamed(string fileName)
        {
            var result = (from f in this.Files
                          where f.ClientPath.Equals(fileName, StringComparison.InvariantCultureIgnoreCase)
                          select f).SingleOrDefault();
            return result;
        }
    }
}