using System.Diagnostics;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Represents single backed up file.
    /// </summary>
    [DebuggerDisplay("Id = {Id}, UserSid = {UserSid}")]
    public class ComputerLocalUser : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerLocalUser"/> class.
        /// </summary>
        protected ComputerLocalUser()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerLocalUser"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public ComputerLocalUser(Computer computer)
        {
            this.Computer = computer;
        }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        public virtual string UserName { get; set; }

        /// <summary>
        /// Gets or sets the user sid.
        /// </summary>
        /// <value>The user sid.</value>
        public virtual string UserSid { get; set; }
        
        /// <summary>
        /// Gets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public virtual Computer Computer { get; protected set; }

        /// <summary>
        /// Gets or sets the documents folder.
        /// </summary>
        /// <value>The documents folder.</value>
        public virtual string DocumentsFolder { get; set; }

        /// <summary>
        /// Gets or sets the desktop folder.
        /// </summary>
        /// <value>The desktop folder.</value>
        public virtual string DesktopFolder { get; set; }
    }
}