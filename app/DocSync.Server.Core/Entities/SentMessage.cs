using System;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Sent message instance.
    /// </summary>
    public class SentMessage : Entity
    {
        /// <summary>
        /// Gets or sets to field.
        /// </summary>
        /// <value>To emails field.</value>
        public virtual string Recipients { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        /// <value>The message subject.</value>
        public virtual string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>The message body.</value>
        public virtual string Body { get; set; }

        /// <summary>
        /// Gets or sets the sending date.
        /// </summary>
        /// <value>The sending date.</value>
        public virtual DateTime SendingDate { get; set; }
    }
}