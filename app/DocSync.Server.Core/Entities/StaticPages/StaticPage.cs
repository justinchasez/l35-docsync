using System.ComponentModel;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities.StaticPages
{
    /// <summary>
    /// Base class for all static pages
    /// </summary>
    public class StaticPage : Entity
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The page title.</value>
        [DisplayName("Page title")]
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>The page body.</value>
        [DisplayName("Page Body")]
        public virtual string Body { get; set; }

        /// <summary>
        /// Gets or sets the page URL.
        /// </summary>
        /// <value>The page URL.</value>
        [DomainSignature]
        public virtual string PageUrl { get; set; }
    }
}