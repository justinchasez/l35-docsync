namespace DocSync.Server.Core.Entities.Payments
{
    /// <summary>
    /// Prolong computer payment
    /// </summary>
    public class AddMoreSpacePayment : Payment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddMoreSpacePayment"/> class.
        /// </summary>
        protected AddMoreSpacePayment()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddMoreSpacePayment"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public AddMoreSpacePayment(Computer computer)
        {
            this.Computer = computer;
        }

        /// <summary>
        /// Gets the payment reason.
        /// </summary>
        /// <value>The payment reason.</value>
        public override string PaymentReason
        {
            get
            {
                string result = string.Format("User {0} added more space to computer {1}.", this.Computer.PaymentPlan.User, this.Computer.Name);
                return result;
            }
        }
    }
}