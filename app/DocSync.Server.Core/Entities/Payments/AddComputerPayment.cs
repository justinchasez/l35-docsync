using System;

namespace DocSync.Server.Core.Entities.Payments
{
    /// <summary>
    /// Payment for add computer.
    /// </summary>
    public class AddComputerPayment : Payment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddComputerPayment"/> class.
        /// </summary>
        protected AddComputerPayment()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AddComputerPayment"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public AddComputerPayment(Computer computer)
        {
            this.Computer = computer;
        }

        /// <summary>
        /// Gets the payment reason.
        /// </summary>
        /// <value>The payment reason.</value>
        public override string PaymentReason
        {
            get
            {
                string result = String.Format("User {0} added computer {1}", Computer.PaymentPlan.User.Name, Computer.Name);
                return result;
            }
        }
    }
}