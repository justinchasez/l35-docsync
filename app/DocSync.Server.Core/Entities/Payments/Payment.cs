using System;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities.Payments
{
    /// <summary>
    /// Base payment class.
    /// </summary>
    public abstract class Payment : Entity
    {
        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount.</value>
        public virtual double Amount { get; set; }

        /// <summary>
        /// Gets or sets the purchase proof.
        /// </summary>
        /// <value>The purchase proof.</value>
        public virtual long PurchaseId { get; set; }

        /// <summary>
        /// Gets the payment reason.
        /// </summary>
        /// <value>The payment reason.</value>
        public abstract string PaymentReason { get; }

        /// <summary>
        /// Gets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public virtual Computer Computer { get; protected set; }

        /// <summary>
        /// Gets or sets the payment date.
        /// </summary>
        /// <value>The payment date.</value>
        public virtual DateTime PaymentDate { get; set; }
    }
}