namespace DocSync.Server.Core.Entities.Payments
{
    /// <summary>
    /// Prolong computer payment
    /// </summary>
    public class ProlongComputerPayment : Payment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProlongComputerPayment"/> class.
        /// </summary>
        protected ProlongComputerPayment()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProlongComputerPayment"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public ProlongComputerPayment(Computer computer)
        {
            this.Computer = computer;
        }

        /// <summary>
        /// Gets the payment reason.
        /// </summary>
        /// <value>The payment reason.</value>
        public override string PaymentReason
        {
            get
            {
                string result = string.Format("User {0} prolonged computer {1}.", this.Computer.PaymentPlan.User, this.Computer.Name);
                return result;
            }
        }
    }
}