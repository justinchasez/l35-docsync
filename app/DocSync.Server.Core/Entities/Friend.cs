﻿using System;
using System.Diagnostics;
using DocSync.Server.Core.Enums;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// The friend class
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Friend e-mail = {Email}")]
    public class Friend : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Friend"/> class.
        /// </summary>
        protected Friend()
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="Friend"/> class.
        /// </summary>
        /// <param name="user">The registered user.</param>
        public Friend(RegisteredUser user)
        {
            this.User = user;
            if (user != null)
            {
                user.AddFriend(this);
            }
        }

        /// <summary>
        /// The friend e-mail
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// Friend refer date
        /// </summary>
        public virtual DateTime ReferDate { get; set; }

        /// <summary>
        /// The friend state
        /// </summary>
        public virtual FriendStates State { get; set; }

        /// <summary>
        /// Free monthes
        /// </summary>
        public virtual int FreeMonthes { get; set; }

        /// <summary>
        /// Gets or sets the extended pc.
        /// </summary>
        /// <value>The extended pc.</value>
        public virtual string ExtendedPc { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        /// <value>The registered user.</value>
        public virtual RegisteredUser User { get; protected set; }
    }
}
