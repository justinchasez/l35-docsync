using System;
using SharpArch.Core.DomainModel;

namespace DocSync.Server.Core.Entities
{
    /// <summary>
    /// Holds unique link for password change
    /// </summary>
    public class PasswordRecoveryLink : Entity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordRecoveryLink"/> class.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public PasswordRecoveryLink(Guid userId)
        {
            this.UserId = userId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordRecoveryLink"/> class.
        /// </summary>
        protected PasswordRecoveryLink()
        {
        }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public virtual Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the link id.
        /// </summary>
        /// <value>The link id.</value>
        public virtual Guid LinkId { get; set; }

        /// <summary>
        /// Gets or sets the creation time.
        /// </summary>
        /// <value>The creation time.</value>
        public virtual DateTime CreationTime { get; set; }
    }
}