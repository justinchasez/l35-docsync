using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace DocSync.Server.Core.Services.Implementations
{
    public class EmailTemplateService
    {
        private String location = String.Empty;


        public EmailTemplateService()
        {
            string codeBase = Assembly.GetExecutingAssembly().GetName().CodeBase.Substring(8);
            location = Path.GetDirectoryName(codeBase) + "\\EmailTemplates\\";
        }

        
        public EmailTemplate GetTemplate(String templateName)
        {
            location += templateName + ".email";
            XDocument template = XDocument.Parse(File.ReadAllText(location));

            return new EmailTemplate
                       {
                           Subject = GetSubject(template),
                           Body = GetBody(template)
                       };
        }

        private String GetSubject(XDocument template)
        {
            return template.Descendants().Where(x => x.Name.LocalName == "Subject").Single().Value.Replace('\r', ' ').Replace('\n', ' '); ;
        }

        private String GetBody(XDocument template)
        {
            XElement bodyElement = template.Descendants().Where(x => x.Name.LocalName == "Body").Single();
            String body = String.Join("", bodyElement.Nodes().Select(n => n.ToString()).ToArray());
            return body;
        }
    }


    public class EmailTemplate
    {
        public string Subject { get; set; }

        public string Body { get; set; }
    }
}