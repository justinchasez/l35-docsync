using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Amazon.S3;
using Amazon.S3.Model;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Settings;
using ICSharpCode.SharpZipLib.Zip;

namespace DocSync.Server.Core.Services.Implementations
{
    /// <summary>
    /// s3 service implementation.
    /// </summary>
    public class S3Service : IS3Service
    {
        /// <summary>
        /// Storage settings container.
        /// </summary>
        private IS3Settings storageSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="S3Service"/> class.
        /// </summary>
        /// <param name="storageSettings">The storage settings.</param>
        public S3Service(IS3Settings storageSettings)
        {
            this.storageSettings = storageSettings;
        }

        /// <summary>
        /// Gets the size of the part.
        /// </summary>
        /// <param name="filePart">The file part.</param>
        /// <returns>File size in bytes.</returns>
        public int GetPartSize(BackedUpFilePart filePart)
        {
            using (AmazonS3 client = this.CreateAmazonS3Client())
            {
                var request = new GetObjectMetadataRequest
                                  {
                                      BucketName = this.storageSettings.BucketName,
                                      Key = filePart.S3FileId.ToString()
                                  };

                using (GetObjectMetadataResponse response = client.GetObjectMetadata(request))
                {
                    return (int)response.ContentLength;
                }
            }
        }

        /// <summary>
        /// Deletes the file version.
        /// </summary>
        /// <param name="backedUpFileVersion">The backed up file version.</param>
        public void DeleteFileVersion(BackedUpFileVersion backedUpFileVersion)
        {
            using (AmazonS3 client = Amazon.AWSClientFactory.CreateAmazonS3Client(this.storageSettings.AccessKeyId, this.storageSettings.SecretAccessKey))
            {
                foreach (var backedUpFilePart in backedUpFileVersion.Parts)
                {
                    var request = new DeleteObjectRequest()
                                            .WithBucketName(this.storageSettings.BucketName)
                                            .WithKey(backedUpFilePart.S3FileId.ToString());

                    using (DeleteObjectResponse response = client.DeleteObject(request))
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Clears all files from the specified computer.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public void Clear(Computer computer)
        {
            foreach (var backedUpFile in computer.Files)
            {
                foreach (var backedUpFileVersion in backedUpFile.FileVersions)
                {
                    this.DeleteFileVersion(backedUpFileVersion);
                }
            }
        }

        /// <summary>
        /// Gets the file.
        /// </summary>
        /// <param name="backedUpFileVersion">The backed up file version.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="httpResponse">The HTTP response stream.</param>
        public void GetFile(BackedUpFileVersion backedUpFileVersion, EncryptInfo encryptKey, HttpResponseBase httpResponse)
        {
            using (AmazonS3 amazonClient =
                Amazon.AWSClientFactory.CreateAmazonS3Client(
                    this.storageSettings.AccessKeyId,
                    this.storageSettings.SecretAccessKey))
            {
                int completedPartNumber = 0;

                foreach (BackedUpFilePart currentPart in backedUpFileVersion.Parts)
                {
                    // Skip downloaded parts.
                    if (currentPart.Number <= completedPartNumber)
                    {
                        continue;
                    }

                    // Create an amazon request.
                    GetObjectRequest request = new GetObjectRequest
                                                   {
                                                       BucketName = this.storageSettings.BucketName,
                                                       Key = currentPart.S3FileId.ToString()
                                                   };

                    // Handle amazon response.
                    using (S3Response response = amazonClient.GetObject(request))
                    {
                        using (Stream responseStream = response.ResponseStream)
                        {
                            RijndaelManaged algorithm = new RijndaelManaged();

                            using (
                                CryptoStream decStream = new CryptoStream(
                                    responseStream,
                                    algorithm.CreateDecryptor(encryptKey.Key, encryptKey.Vector),
                                    CryptoStreamMode.Read))
                            {
                                byte[] buffer = new byte[10240];

                                int bytesRead;
                                do
                                {
                                    buffer = new byte[10240];
                                    bytesRead = decStream.Read(buffer, 0, buffer.Length);
                                    if (bytesRead == 0)
                                    {
                                        break;
                                    }

                                    byte[] tempBuffer = new byte[bytesRead];
                                    Array.Copy(buffer, tempBuffer, bytesRead);

                                    httpResponse.BinaryWrite(tempBuffer);
                                }
                                while (bytesRead > 0);
                            }
                        }
                    }

                    completedPartNumber++;
                }
            }
        }

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="backedUpFileVersions">The backed up file versions.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="httpResponse">The HTTP response.</param>
        /// <param name="clientPath">The client path.</param>
        public void GetFiles(IList<BackedUpFileVersion> backedUpFileVersions, EncryptInfo encryptKey, HttpResponseBase httpResponse, string clientPath)
        {
            using (AmazonS3 amazonClient = Amazon.AWSClientFactory.CreateAmazonS3Client(this.storageSettings.AccessKeyId, this.storageSettings.SecretAccessKey))
            {
                using (var zipper = new ZipOutputStream(httpResponse.OutputStream, 1024))
                {
                    zipper.SetLevel(0);
                    foreach (var backedUpFileVersion in backedUpFileVersions)
                    {
                        var zipperEntry = new ZipEntry(backedUpFileVersion.BackedUpFile.ClientPath.Replace(clientPath, string.Empty).Trim('/', '\\')) { Size = backedUpFileVersion.Size };
                        zipper.PutNextEntry(zipperEntry);
                        //var zipperBuffer = new Byte[1024];

                        //var completedPartNumber = 0;
                        foreach (BackedUpFilePart currentPart in backedUpFileVersion.Parts.OrderBy(f => f.Number))
                        {
                            // Skip downloaded parts.
                            //if (currentPart.Number <= completedPartNumber)
                            //{
                            //    continue;
                            //}

                            // Create an amazon request.
                            var request = new GetObjectRequest
                                              {
                                                  BucketName = this.storageSettings.BucketName,
                                                  Key = currentPart.S3FileId.ToString()
                                              };

                            // Handle amazon response.
                            using (S3Response response = amazonClient.GetObject(request))
                            {
                                using (var responseStream = response.ResponseStream)
                                {
                                    var algorithm = new RijndaelManaged();

                                    using (var decStream = new CryptoStream(responseStream, algorithm.CreateDecryptor(encryptKey.Key, encryptKey.Vector), CryptoStreamMode.Read))
                                    {
                                        byte[] buffer;

                                        int bytesRead;
                                        do
                                        {
                                            buffer = new byte[1024];
                                            bytesRead = decStream.Read(buffer, 0, buffer.Length);
                                            if (bytesRead == 0)
                                            {
                                                break;
                                            }

                                            //var tempBuffer = new byte[bytesRead];
                                            //Array.Copy(buffer, tempBuffer, bytesRead);

                                            //httpResponse.BinaryWrite(tempBuffer);

                                            zipper.Write(buffer, 0, bytesRead);
                                            httpResponse.Flush();
                                        } while (bytesRead > 0);
                                    }
                                }
                            }

                            //completedPartNumber++;
                        }

                        zipper.CloseEntry();
                        httpResponse.Flush();
                    }
                }
            }
        }

        /// <summary>
        /// Creates the amazon s3 client.
        /// </summary>
        /// <returns><see cref="AmazonS3"/> instance.</returns>
        private AmazonS3 CreateAmazonS3Client()
        {
            return Amazon.AWSClientFactory.CreateAmazonS3Client(this.storageSettings.AccessKeyId, this.storageSettings.SecretAccessKey);
        }
    }
}