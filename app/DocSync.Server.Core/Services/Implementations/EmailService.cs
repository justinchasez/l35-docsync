using System.Net.Configuration;
using System.Net.Mail;
using System.Web.Configuration;

namespace DocSync.Server.Core.Services.Implementations
{
    /// <summary>
    /// Email service
    /// </summary>
    public class EmailService : IEmailService
    {
        /// <summary>
        /// Sends the specified email.
        /// </summary>
        /// <param name="to">Receiver email.</param>
        /// <param name="subject">The message subject.</param>
        /// <param name="body">The message body.</param>
        public void Send(string to, string subject, string body)
        {
            SmtpClient smtpClient = new SmtpClient();

            MailMessage message = new MailMessage(this.GetFromEmail(), to, subject, body)
                                      {
                                          IsBodyHtml = true
                                      };
            smtpClient.Send(message);
        }

        /// <summary>
        /// Gets from email.
        /// </summary>
        /// <returns>String with from email.</returns>
        private string GetFromEmail()
        {
            var config = WebConfigurationManager.OpenWebConfiguration("~/web.config");
            MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");
            string result = settings.Smtp.From;
            return result;
        }
    }
}