using System;
using System.Collections;
using System.Linq;
using System.Web.Security;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Enums;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.Services.Implementations
{
    /// <summary>
    /// Account membership service
    /// </summary>
    public class AccountMembershipService : IMembershipService
    {
        /// <summary>
        /// Membership provider
        /// </summary>
        private readonly MembershipProvider provider;

        /// <summary>
        /// Gets or sets the friends repository.
        /// </summary>
        /// <value>The friends repository.</value>
        private readonly IRepository<Friend> friendsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountMembershipService"/> class.
        /// </summary>
        /// <param name="friendsRepository">The friends repository.</param>
        public AccountMembershipService(IRepository<Friend> friendsRepository)
            : this(null, friendsRepository)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountMembershipService"/> class.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="friendsRepository">The friends repository.</param>
        public AccountMembershipService(MembershipProvider provider, IRepository<Friend> friendsRepository)
        {
            this.provider = provider ?? Membership.Provider;

            this.friendsRepository = friendsRepository;
        }

        /// <summary>
        /// Gets the length of the min password.
        /// </summary>
        /// <value>The length of the min password.</value>
        public int MinPasswordLength
        {
            get
            {
                return this.provider.MinRequiredPasswordLength;
            }
        }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>If user is valid</returns>
        public bool ValidateUser(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Value cannot be null or empty.", "userName");
            }

            if (String.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Value cannot be null or empty.", "password");
            }

            return this.provider.ValidateUser(userName, password);
        }

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <param name="comment">The comment.</param>
        /// <returns>Membership create status</returns>
        public MembershipCreateStatus CreateUser(string userName, string password, string email, string comment)
        {
            if (String.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Value cannot be null or empty.", "userName");
            }

            if (String.IsNullOrEmpty(password))
            {
                throw new ArgumentException("Value cannot be null or empty.", "password");
            }

            if (String.IsNullOrEmpty(email))
            {
                throw new ArgumentException("Value cannot be null or empty.", "email");
            }

            MembershipCreateStatus status;
            MembershipUser user = this.provider.CreateUser(userName, password, email, null, null, true, null, out status);

            if (status == MembershipCreateStatus.Success)
            {
                this.CheckInFriends(userName);

                if (!string.IsNullOrEmpty(comment))
                {
                    user.Comment = comment;
                    this.provider.UpdateUser(user);
                }
            }

            return status;
        }

        /// <summary>
        /// Checks the in friends.
        /// </summary>
        /// <param name="userName">The user email.</param>
        private void CheckInFriends(string userName)
        {
            var result = this.friendsRepository.GetAll().Where(f => f.Email.Equals(userName, StringComparison.InvariantCultureIgnoreCase));

            if (result.Count() > 0)
            {
                var friend = result.Single();
                friend.State = FriendStates.Registered;
                this.friendsRepository.SaveOrUpdate(friend);
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns>If change succeeded</returns>
        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            if (String.IsNullOrEmpty(userName))
            {
                throw new ArgumentException("Value cannot be null or empty.", "userName");
            }

            if (String.IsNullOrEmpty(oldPassword))
            {
                throw new ArgumentException("Value cannot be null or empty.", "oldPassword");
            }

            if (String.IsNullOrEmpty(newPassword))
            {
                throw new ArgumentException("Value cannot be null or empty.", "newPassword");
            }

            // The underlying ChangePassword() will throw an exception rather
            // than return false in certain failure scenarios.
            try
            {
                MembershipUser currentUser = this.provider.GetUser(userName, true /* userIsOnline */);
                return currentUser.ChangePassword(oldPassword, newPassword);
            }
            catch (ArgumentException)
            {
                return false;
            }
            catch (MembershipPasswordException)
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the user by id.
        /// </summary>
        /// <param name="id">The id of user.</param>
        /// <returns><see cref="MembershipUser"/> instance</returns>
        public MembershipUser GetById(Guid id)
        {
            MembershipUser user = this.provider.GetUser(id, false);
            return user;
        }

        /// <summary>
        /// Finds the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns><see cref="MembershipUser"/> instance</returns>
        public MembershipUser FindUserByEmail(string email)
        {
            int totalRecords;
            MembershipUserCollection collection = this.provider.FindUsersByEmail(email, 0, 1, out totalRecords);
            IEnumerator enumerator = collection.GetEnumerator();
            if (enumerator.MoveNext())
            {
                return (MembershipUser) enumerator.Current;
            }

            return null;
        }

        /// <summary>
        /// Sets the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        public void SetPassword(Guid userId, string password)
        {
            MembershipUser user = this.provider.GetUser(userId, false);
            string tempPassword = this.provider.ResetPassword(user.UserName, String.Empty);
            this.provider.ChangePassword(user.UserName, tempPassword, password);
        }

        /// <summary>
        /// Deletes the specified user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        public void Delete(Guid userId)
        {
            MembershipUser user = this.GetById(userId);
            this.provider.DeleteUser(user.UserName, true);
        }
    }
}