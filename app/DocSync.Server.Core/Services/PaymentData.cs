﻿using System;

namespace DocSync.Server.Core.Services
{
    public class PaymentData
    {
        public Int32 ProductId { get; set; }

        public Int64 PurchaseId { get; set; }


        public String UserName { get; set; }   
        
        public String Email { get; set; }

        public String Computer { get; set; }


        public Boolean IsBillingSubscription { get; set; }


        public Boolean IsManualEncryptionEnabled { get; set; }
    }
}