namespace DocSync.Server.Core.Services
{
    /// <summary>
    /// Email service
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="to">To email of recipient.</param>
        /// <param name="subject">The message subject.</param>
        /// <param name="body">The message body.</param>
        void Send(string to, string subject, string body);
    }
}