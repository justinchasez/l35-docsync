using System.Collections.Generic;
using System.Web;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Core.Services
{
    /// <summary>
    /// S3 service.
    /// </summary>
    public interface IS3Service
    {
        /// <summary>
        /// Gets the size of the file part on s3 storage.
        /// </summary>
        /// <param name="filePart">The file part.</param>
        /// <returns>File size in bytes.</returns>
        int GetPartSize(BackedUpFilePart filePart);

        /// <summary>
        /// Deletes the file version.
        /// </summary>
        /// <param name="backedUpFileVersion">The backed up file version.</param>
        void DeleteFileVersion(BackedUpFileVersion backedUpFileVersion);

        /// <summary>
        /// Clears all files from the specified computer.
        /// </summary>
        /// <param name="computer">The computer.</param>
        void Clear(Computer computer);

        /// <summary>
        /// Gets the file.
        /// </summary>
        /// <param name="backedUpFileVersion">The backed up file version.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="response">The response stream.</param>
        void GetFile(BackedUpFileVersion backedUpFileVersion, EncryptInfo encryptKey, HttpResponseBase response);

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="backedUpFileVersions">The backed up file versions.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="response">The response.</param>
        /// <param name="clientPath">The client path.</param>
        void GetFiles(IList<BackedUpFileVersion> backedUpFileVersions, EncryptInfo encryptKey, HttpResponseBase response, string clientPath);
    }
}