using System;
using System.Web.Security;

namespace DocSync.Server.Core.Services
{
    /// <summary>
    /// Membership service
    /// </summary>
    public interface IMembershipService
    {
        /// <summary>
        /// Gets the length of the min password.
        /// </summary>
        /// <value>The length of the min password.</value>
        int MinPasswordLength { get; }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>If user is valid</returns>
        bool ValidateUser(string userName, string password);

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <param name="comment">The comment.</param>
        /// <returns>Membership create status</returns>
        MembershipCreateStatus CreateUser(string userName, string password, string email, string comment);

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns>If change succeeded</returns>
        bool ChangePassword(string userName, string oldPassword, string newPassword);

        /// <summary>
        /// Gets the by id.
        /// </summary>
        /// <param name="id">The id of user.</param>
        /// <returns><see cref="MembershipUser"/> instance</returns>
        MembershipUser GetById(Guid id);

        /// <summary>
        /// Finds the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns><see cref="MembershipUser"/> instance</returns>
        MembershipUser FindUserByEmail(string email);

        /// <summary>
        /// Sets the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="password">The password.</param>
        void SetPassword(Guid userId, string password);

        /// <summary>
        /// Deletes the specified user.
        /// </summary>
        /// <param name="userId">The user id.</param>
        void Delete(Guid userId);
    }
}