using System.Collections.Generic;
using DocSync.Server.Core.Entities;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.Settings.Implementations
{
    /// <summary>
    /// S3 settings container implementation.
    /// </summary>
    public class S3Settings : IS3Settings
    {
        /// <summary>
        /// Settings storage.
        /// </summary>
        private IRepository<SettingEntry> settingsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="S3Settings"/> class.
        /// </summary>
        /// <param name="settingsRepository">The settings repository.</param>
        public S3Settings(IRepository<SettingEntry> settingsRepository)
        {
            this.settingsRepository = settingsRepository;
        }

        /// <summary>
        /// Gets the access key id.
        /// </summary>
        /// <value>The access key id.</value>
        public string AccessKeyId
        {
            get { return this.GetSetting("AccessKeyId"); }
        }

        /// <summary>
        /// Gets the secret access key.
        /// </summary>
        /// <value>The secret access key.</value>
        public string SecretAccessKey
        {
            get { return this.GetSetting("SecretAccessKey"); }
        }

        /// <summary>
        /// Gets the name of the bucket.
        /// </summary>
        /// <value>The name of the bucket.</value>
        public string BucketName
        {
            get { return this.GetSetting("BucketName"); }
        }

        /// <summary>
        /// Gets the setting from storage.
        /// </summary>
        /// <param name="key">The setting key.</param>
        /// <returns>Setting value.</returns>
        private string GetSetting(string key)
        {
            var dictionary = new Dictionary<string, object> { { "Key", key } };
            SettingEntry entry = this.settingsRepository.FindOne(dictionary);
            return entry.Value;
        }
    }
}