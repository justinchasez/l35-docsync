using System;
using System.Collections.Generic;
using System.Linq;
using DocSync.Server.Core.Entities;
using SharpArch.Core;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Core.Settings.Implementations
{
    /// <summary>
    /// Product settings storage.
    /// </summary>
    public class ProductSettings : IProductSettings
    {

        #region Privates

        /// <summary>
        /// Space sizes
        /// </summary>
        private enum SpaceSizes
        {
            /// <summary>
            /// Space size: 2Gb
            /// </summary>
            Add2Gb = 2,

            /// <summary>
            /// Space size: 5Gb
            /// </summary>
            Add5Gb = 5,

            /// <summary>
            /// Space size: 10Gb
            /// </summary>
            Add10Gb = 10
        } 

        /// <summary>
        /// Settings repository.
        /// </summary>
        private readonly IRepository<SettingEntry> repository;

        #endregion Privates

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductSettings"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public ProductSettings(IRepository<SettingEntry> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Gets the vendor name.
        /// </summary>
        /// <value>The vendor name.</value>
        public string Vendor
        {
            get
            {
                var result = this.GetSetting("Vendor");
                return result;
            }
        }


        /// <summary>
        /// Gets the product id for add computer action.
        /// </summary>
        /// <param name="yearsNumber">The years number.</param>
        /// <returns>ClickBank product id.</returns>
        public int GetProductIdForAddComputer(int yearsNumber)
        {
            Check.Require(yearsNumber < 3 || yearsNumber > 0, "You can't add computer for less then 1 year and more then 3 years", new ArgumentOutOfRangeException("yearsNumber", yearsNumber, "Years number is out of range"));

            string key = "AddComputer." + yearsNumber + ".year";
            int result = int.Parse(this.GetSetting(key));
            
            return result;
        }

        /// <summary>
        /// Gets the product id for prolong.
        /// </summary>
        /// <param name="yearsNumber">The years number.</param>
        /// <returns>Product id.</returns>
        public int GetProductIdForProlong(int yearsNumber)
        {
            Check.Require(yearsNumber < 3 || yearsNumber > 0, "You can't add computer for less then 1 year and more then 3 years", new ArgumentOutOfRangeException("yearsNumber", yearsNumber, "Years number is out of range"));

            string key = "Prolong." + yearsNumber + ".year";
            int result = int.Parse(this.GetSetting(key));

            return result;
        }

        /// <summary>
        /// Gets the add computer product ids.
        /// </summary>
        /// <returns>List of ids.</returns>
        public List<int> GetAddComputerIds()
        {
            var result = new List<int>(3);
            for (int i = 1; i < 4; i++)
            {
                string key = "AddComputer." + i + ".year";
                result.Add(int.Parse(this.GetSetting(key)));
            }

            return result;
        }

        /// <summary>
        /// Gets the number of years for prolong computer for specified product id.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Years number for product id.</returns>
        public int GetNumberOfYearsForProlong(int productId)
        {
            for (int yearsNumber = 1; yearsNumber < 4; yearsNumber++)
            {
                int localProductId = this.GetProductIdForProlong(yearsNumber);
                if (productId == localProductId)
                {
                    return yearsNumber;
                }
            }

            throw new ArgumentException("There is no such product id in system", "productId");
        }

        /// <summary>
        /// Gets the current client app version.
        /// </summary>
        /// <returns>Current client app version.</returns>
        public string GetCurrentClientAppVersion()
        {
            string result = this.GetSetting("CurrentClientAppVersion");
            return result;
        }

        /// <summary>
        /// Sets the current client app version.
        /// </summary>
        /// <param name="newVersion">The new version.</param>
        public void SetCurrentClientAppVersion(string newVersion)
        {
            this.SetSetting("CurrentClientAppVersion", newVersion);
        }

        /// <summary>
        /// Gets the backup limit.
        /// </summary>
        /// <returns>Current backup limit.</returns>
        public int? GetBackupLimit()
        {
            string settingValue = this.GetSetting("BackupLimit");
            int result = Int32.Parse(settingValue);

            if (result == 0)
            {
                return null;
            }
            else
            {
                return result;                
            }
        }

        /// <summary>
        /// Sets the backup limit.
        /// </summary>
        /// <param name="newLimit">The new limit.</param>
        public void SetBackupLimit(int? newLimit)
        {
            int limit = (newLimit == null) ? 0 : newLimit.Value;
            Check.Require(limit >= 0 && limit <= 15,
                          "You can't set backup limit less then 0 and more then 15");

            this.SetSetting("BackupLimit", limit.ToString());
        }

        /// <summary>
        /// Gets the trial backup limit.
        /// </summary>
        /// <returns>Current trial backup limit.</returns>
        public int? GetTrialBackupLimit()
        {
            string settingValue = this.GetSetting("TrialBackupLimit");
            int result = Int32.Parse(settingValue);

            if (result == 0)
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Sets the trial backup limit.
        /// </summary>
        /// <param name="newLimit">The new limit.</param>
        public void SetTrialBackupLimit(int? newLimit)
        {
            int limit = (newLimit == null) ? 0 : newLimit.Value;
            Check.Require(limit >= 0 && limit <= 15,
                          "You can't set backup limit less then 0 and more then 15");

            this.SetSetting("TrialBackupLimit", limit.ToString());
        }

        /// <summary>
        /// Gets the price for add more space.
        /// </summary>
        /// <param name="spaceSize">Size of the space.</param>
        /// <returns>The price of this space.</returns>
        public int GetProductIdForAddMoreSpace(int spaceSize)
        {
            var values = Enum.GetValues(typeof(SpaceSizes)).Cast<int>();
            Check.Require(values.Contains(spaceSize), "You can't add more space, if space size not equal 2, 5 or 10 Gb", new ArgumentOutOfRangeException("spaceSize", spaceSize, "Space size is out of range"));
            //Check.Require(spaceSize == 2 || spaceSize == 5 || spaceSize == 10, "You can't add more space, if space size not equal 2, 5 or 10 Gb", new ArgumentOutOfRangeException("spaceSize", spaceSize, "Space size is out of range"));

            string key = "AddMoreSpace." + spaceSize + ".Gb";
            int result = int.Parse(this.GetSetting(key));

            return result;
        }

        /// <summary>
        /// Gets the space size for add more.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Size of the space.</returns>
        public int GetSpaceSizeForAddMore(int productId)
        {
            var values = Enum.GetValues(typeof(SpaceSizes)).Cast<int>();
            foreach (var size in values)
            {
                int localProductId = this.GetProductIdForAddMoreSpace(size);
                if (productId == localProductId)
                {
                    return size;
                }
            }

            throw new ArgumentException("There is no such price in system", "productId");
        }

        /// <summary>
        /// Gets the add more space ids.
        /// </summary>
        /// <returns>return all ids for add more space</returns>
        public List<int> GetAddMoreSpaceIds()
        {
            var values = Enum.GetValues(typeof(SpaceSizes)).Cast<int>();
        
            List<int> ids = values.Select(this.GetProductIdForAddMoreSpace).ToList();
            return ids;
        }

        /// <summary>
        /// Gets the refer friends free months.
        /// </summary>
        /// <returns>Refer friends free months</returns>
        public int GetReferFriendsFreeMonths()
        {
            string settingValue = this.GetSetting("ReferFriends.FreeMonths");
            int result = Int32.Parse(settingValue);
            
            return result;
        }

        public List<int> GetProlongIds()
        {
            var result = new List<int>(3);
            for (int i = 1; i < 4; i++)
            {
                string key = "Prolong." + i + ".year";
                result.Add(int.Parse(this.GetSetting(key)));
            }

            return result;
        }

        /// <summary>
        /// Gets the years number for add computer by product id.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>
        /// Number of years for specified product id.
        /// </returns>
        public int GetYearsNumberForAddComputer(int productId)
        {
            for (int yearsNumber = 1; yearsNumber < 4; yearsNumber++)
            {
                int localProductId = this.GetProductIdForAddComputer(yearsNumber);
                if (productId == localProductId)
                {
                    return yearsNumber;
                }
            }

            throw new ArgumentException("There is no such product id in system", "productId");
        }


        public String GetDownloadUrl()
        {
            return this.GetSetting("downloadUrl");
        }

        /// <summary>
        /// Gets the price for product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Product price.</returns>
        public double GetPriceForProduct(int productId)
        {
            string key = "Product.Price." + productId;
            return double.Parse(this.GetSetting(key));
        }

        /// <summary>
        /// Gets the setting from storage.
        /// </summary>
        /// <param name="key">The setting key.</param>
        /// <returns>Setting value.</returns>
        private string GetSetting(string key)
        {
            var dictionary = new Dictionary<string, object> { { "Key", key } };
            SettingEntry entry = this.repository.FindOne(dictionary);
            return entry.Value;
        }

        /// <summary>
        /// Sets the setting from storage.
        /// </summary>
        /// <param name="key">The setting key.</param>
        /// <param name="newValue">The new value.</param>
        private void SetSetting(string key, string newValue)
        {
            var dictionary = new Dictionary<string, object> { { "Key", key } };
            SettingEntry entry = this.repository.FindOne(dictionary);
            entry.Value = newValue;
            this.repository.SaveOrUpdate(entry);
        }
    }
}