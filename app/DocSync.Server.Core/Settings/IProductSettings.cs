using System;
using System.Collections.Generic;

namespace DocSync.Server.Core.Settings
{
    /// <summary>
    /// Products settings container.
    /// </summary>
    public interface IProductSettings
    {
        /// <summary>
        /// Gets the vendor name.
        /// </summary>
        /// <value>The vendor name.</value>
        string Vendor { get; }

        /// <summary>
        /// Gets the product id for add computer action.
        /// </summary>
        /// <param name="yearsNumber">The years number.</param>
        /// <returns>ClickBank product id.</returns>
        int GetProductIdForAddComputer(int yearsNumber);

        /// <summary>
        /// Gets the years number for add computer by product id.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Number of years for specified product id.</returns>
        int GetYearsNumberForAddComputer(int productId);

        /// <summary>
        /// Gets the price for product.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Product price.</returns>
        double GetPriceForProduct(int productId);

        /// <summary>
        /// Gets the product id for prolong.
        /// </summary>
        /// <param name="yearsNumber">The years number.</param>
        /// <returns>Product id.</returns>
        int GetProductIdForProlong(int yearsNumber);

        /// <summary>
        /// Gets the add computer product ids.
        /// </summary>
        /// <returns>List of ids.</returns>
        List<int> GetAddComputerIds();

        /// <summary>
        /// Gets the number of years for prolong computer for specified product id.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Years number for product id.</returns>
        int GetNumberOfYearsForProlong(int productId);

        /// <summary>
        /// Gets the current client app version.
        /// </summary>
        /// <returns>Current client app version.</returns>
        string GetCurrentClientAppVersion();

        /// <summary>
        /// Sets the current client app version.
        /// </summary>
        /// <param name="newVersion">The new version.</param>
        void SetCurrentClientAppVersion(string newVersion);

        /// <summary>
        /// Gets the backup limit.
        /// </summary>
        /// <returns>Current backup limit.</returns>
        int? GetBackupLimit();

        /// <summary>
        /// Sets the backup limit.
        /// </summary>
        /// <param name="newLimit">The new limit.</param>
        void SetBackupLimit(int? newLimit);

        /// <summary>
        /// Gets the trial backup limit.
        /// </summary>
        /// <returns>Current trial backup limit.</returns>
        int? GetTrialBackupLimit();

        /// <summary>
        /// Sets the trial backup limit.
        /// </summary>
        /// <param name="newLimit">The new limit.</param>
        void SetTrialBackupLimit(int? newLimit);

        /// <summary>
        /// Gets the price for add more space.
        /// </summary>
        /// <param name="spaceSize">Size of the space.</param>
        /// <returns>The price of this space.</returns>
        int GetProductIdForAddMoreSpace(int spaceSize);

        /// <summary>
        /// Gets the space size for add more.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>Size of the space.</returns>
        int GetSpaceSizeForAddMore(int productId);

        /// <summary>
        /// Gets the add more space ids.
        /// </summary>
        /// <returns>return all ids for add more space</returns>
        List<int> GetAddMoreSpaceIds();


        /// <summary>
        /// Gets the refer friends free months.
        /// </summary>
        /// <returns>Refer friends free months</returns>
        int GetReferFriendsFreeMonths();

        List<int> GetProlongIds();

        String GetDownloadUrl();
    }
}