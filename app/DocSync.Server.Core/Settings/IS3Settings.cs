namespace DocSync.Server.Core.Settings
{
    /// <summary>
    /// S3 settings container.
    /// </summary>
    public interface IS3Settings 
    {
        /// <summary>
        /// Gets the access key id.
        /// </summary>
        /// <value>The access key id.</value>
        string AccessKeyId { get; }

        /// <summary>
        /// Gets the secret access key.
        /// </summary>
        /// <value>The secret access key.</value>
        string SecretAccessKey { get; }

        /// <summary>
        /// Gets the name of the bucket.
        /// </summary>
        /// <value>The name of the bucket.</value>
        string BucketName { get; }
    }
}