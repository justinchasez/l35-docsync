namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Add more space params dto.
    /// </summary>
    public class AddMoreSpaceParams
    {
        /// <summary>
        /// Gets or sets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public string Computer { get; set; }

        /// <summary>
        /// Gets or sets the size of the space.
        /// </summary>
        /// <value>The size of the space.</value>
        public int SpaceSize { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }
    }
}