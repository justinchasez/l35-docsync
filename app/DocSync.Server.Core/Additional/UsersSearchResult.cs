using System;
using System.Collections.Generic;
using System.ComponentModel;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Result of search with paging.
    /// </summary>
    public class UsersSearchResult : PageableModel<RegisteredUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersSearchResult"/> class.
        /// </summary>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="registeredUsers">The registered users.</param>
        /// <param name="totalFounded">Total records founded</param>
        public UsersSearchResult(int pageSize, int pageNumber, IList<RegisteredUser> registeredUsers, int totalFounded)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            this.FoundedUsers = registeredUsers;
            this.TotalItems = totalFounded;
        }

        /// <summary>
        /// Gets or sets the founded users.
        /// </summary>
        /// <value>The founded users.</value>
        public IList<RegisteredUser> FoundedUsers { get; set; }

        /// <summary>
        /// Gets or sets the email to search for.
        /// </summary>
        /// <value>The email.</value>
        [DisplayName("Email address")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the registration start date.
        /// </summary>
        /// <value>The registration start date.</value>
        [DisplayName("Registered after")]
        public DateTime? RegistrationStartDate { get; set; }

        /// <summary>
        /// Gets or sets the registration end date.
        /// </summary>
        /// <value>The registration end date.</value>
        [DisplayName("Registered before")]
        public DateTime? RegistrationEndDate { get; set; }

        /// <summary>
        /// Gets the entities list.
        /// </summary>
        /// <value>The entities list.</value>
        protected override IList<RegisteredUser> EntitiesList
        {
            get { return this.FoundedUsers; }
        }
    }
}