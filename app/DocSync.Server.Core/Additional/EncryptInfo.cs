using System;
using System.Collections.Generic;
using System.IO;

namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// The ecnryption information.
    /// </summary>
    [Serializable]
    public class EncryptInfo
    {
        /// <summary>
        /// Gets or sets the default key.
        /// </summary>
        /// <value>The default key.</value>
        public static EncryptInfo DefaultKey { get; set; }

        /// <summary>
        /// Initializes static members of the <see cref="EncryptInfo"/> class.
        /// </summary>
        static EncryptInfo()
        {
            DefaultKey = new EncryptInfo();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="EncryptInfo"/> class from being created.
        /// </summary>
        private EncryptInfo()
        {
            this.InitKeys();
        }

        /// <summary>
        /// Gets or sets the encryption key.
        /// </summary>
        /// <value>The encryption key.</value>
        public byte[] Key { get; set; }

        /// <summary>
        /// Gets or sets the encryption vector.
        /// </summary>
        /// <value>The encryption vector.</value>
        public byte[] Vector { get; set; }
        
        /// <summary>
        /// Inits the keys.
        /// </summary>
        private void InitKeys()
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory +
                              "\\default.pem";
            
            if (File.Exists(filePath))
            {
                byte[] data = File.ReadAllBytes(filePath);
                if (data.Length > 16)
                {
                    List<byte> buffer = new List<byte>(data);

                    this.Key = buffer.GetRange(0, 16).ToArray();
                    this.Vector = buffer.GetRange(16, 16).ToArray();
                }
                else
                {
                    throw new ArgumentException("data");
                }
            }
            else
            {
                throw new FileNotFoundException("File not exists", filePath);
            }
        }
    }
}