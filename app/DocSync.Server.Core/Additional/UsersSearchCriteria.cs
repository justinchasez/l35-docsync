using System;
using System.ComponentModel;

namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Encapsulates search parameters on Manage Users page 
    /// </summary>
    public class UsersSearchCriteria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UsersSearchCriteria"/> class.
        /// </summary>
        public UsersSearchCriteria()
        {
            this.PageSize = 50;
            this.PageNumber = 1;
        }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page number.</value>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [DisplayName("E-mail")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the registration start date.
        /// </summary>
        /// <value>The registration start date.</value>
        [DisplayName("Registered after")]
        public DateTime? RegistrationStartDate { get; set; }

        /// <summary>
        /// Gets or sets the registration end date.
        /// </summary>
        /// <value>The registration end date.</value>
        [DisplayName("Registered before")]
        public DateTime? RegistrationEndDate { get; set; }

        /// <summary>
        /// Gets or sets the sort by field name.
        /// </summary>
        /// <value>The sort by field name.</value>
        public string SortBy { get; set; }

        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        /// <value>The sort order.</value>
        public string SortOrder { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets the computers number.
        /// </summary>
        /// <value>The computers number.</value>
        [DisplayName("Computers number")]
        public int? ComputersNumber { get; set; }

        /// <summary>
        /// Gets or sets the min payed money.
        /// </summary>
        /// <value>The min payed money.</value>
        [DisplayName("Payed more then")]
        public int? MinPayedMoney { get; set; }

        /// <summary>
        /// Gets or sets the max payed money.
        /// </summary>
        /// <value>The max payed money.</value>
        [DisplayName("Payed less then")]
        public int? MaxPayedMoney { get; set; }

        /// <summary>
        /// Gets or sets the is paid.
        /// </summary>
        /// <value>The is paid.</value>
        public bool? IsPaid { get; set; }
    }
}