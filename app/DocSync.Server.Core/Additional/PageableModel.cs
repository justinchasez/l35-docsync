using System.Collections;
using System.Collections.Generic;
using MvcContrib.Pagination;

namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Base class for models with paging.
    /// </summary>
    /// <typeparam name="T">Entity type.</typeparam>
    public abstract class PageableModel<T> : IPagination
    {
        /// <summary>
        /// Gets the entities list.
        /// </summary>
        /// <value>The entities list.</value>
        protected abstract IList<T> EntitiesList { get; }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return this.EntitiesList.GetEnumerator();
        }

        /// <summary>
        /// The current page number.
        /// </summary>
        /// <value>Current page number</value>
        public int PageNumber { get; set; }

        /// <summary>
        /// The number of items in each page.
        /// </summary>
        /// <value>Page size value.</value>
        public int PageSize { get; set; }

        /// <summary>
        /// The total number of items.
        /// </summary>
        /// <value>Total founded number.</value>
        public int TotalItems { get; set; }

        /// <summary>
        /// The total number of pages.
        /// </summary>
        /// <value>Total pages number.</value>
        public int TotalPages
        {
            get
            {
                int wholePages = this.TotalItems / this.PageSize;
                int restItems = this.TotalItems % this.PageSize;
                int addPage = restItems == 0 ? 0 : 1;
                int totalPages = wholePages + addPage;

                return totalPages;
            }
        }

        /// <summary>
        /// The index of the first item in the page.
        /// </summary>
        /// <value>Index of first item on page.</value>
        public int FirstItem
        {
            get
            {
                int firstItem;
                if (this.EntitiesList.Count == 0)
                {
                    firstItem = 0;
                }
                else
                {
                    firstItem = ((this.PageNumber - 1) * this.PageSize) + 1;
                }

                return firstItem;
            }
        }

        /// <summary>
        /// The index of the last item in the page.
        /// </summary>
        /// <value>Last item index</value>
        public int LastItem
        {
            get
            {
                int lastItem;
                if (this.EntitiesList.Count == 0)
                {
                    lastItem = 0;
                }
                else
                {
                    if (this.HasNextPage)
                    {
                        lastItem = this.PageNumber * this.PageSize;
                    }
                    else
                    {
                        lastItem = ((this.PageNumber - 1) * this.PageSize) + this.EntitiesList.Count;
                    }
                }

                return lastItem;
            }
        }

        /// <summary>
        /// Whether there are pages before the current page.
        /// </summary>
        /// <value><see langword="true"/> if page exists, otherwise <see langword="false"/></value>
        public bool HasPreviousPage
        {
            get
            {
                return this.PageNumber > 1 && this.TotalItems > this.PageSize;
            }
        }

        /// <summary>
        /// Whether there are pages after the current page.
        /// </summary>
        /// <value><see langword="true"/> if next page exists, otherwise <see langword="false"/></value>
        public bool HasNextPage
        {
            get
            {
                return this.PageNumber < this.TotalPages;
            }
        }
    }
}