namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Encapsulates search parameters on Manage Users page 
    /// </summary>
    public class SortableMessagesCriteria
    {
        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page number.</value>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the sort by field name.
        /// </summary>
        /// <value>The sort by field name.</value>
        public string SortBy { get; set; }

        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        /// <value>The sort order.</value>
        public string SortOrder { get; set; }

        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}