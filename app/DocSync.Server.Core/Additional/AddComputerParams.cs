namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Parameter object for add computer.
    /// </summary>
    public class AddComputerParams
    {
        ///// <summary>
        ///// Gets or sets the name of the user.
        ///// </summary>
        ///// <value>The name of the user.</value>
        //public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the computer name.
        /// </summary>
        /// <value>The computer name.</value>
        public string Computer { get; set; }

        /// <summary>
        /// Gets or sets the years number.
        /// </summary>
        /// <value>The years number.</value>
        public int YearsNumber { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is manual encription enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is manual encription enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsManualEncriptionEnabled { get; set; }
    }
}