namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Prolog computer params dto.
    /// </summary>
    public class ProlongComputerParams
    {
        /// <summary>
        /// Gets or sets the computer.
        /// </summary>
        /// <value>The computer.</value>
        public string Computer { get; set; }

        /// <summary>
        /// Gets or sets the years number.
        /// </summary>
        /// <value>The years number.</value>
        public int YearsNumber { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }
    }
}