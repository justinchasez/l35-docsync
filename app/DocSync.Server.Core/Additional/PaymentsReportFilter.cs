using System;
using System.ComponentModel;
using DocSync.Server.Core.Enums;

namespace DocSync.Server.Core.Additional
{
    /// <summary>
    /// Payments report view model.
    /// </summary>
    public class PaymentsReportFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentsReportFilter"/> class.
        /// </summary>
        public PaymentsReportFilter()
        {
            this.PageSize = 25;
            this.PageNumber = 1;
        }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        /// <value>The page number.</value>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [DisplayName("Payment type")]
        public PaymentTypes? PaymentType { get; set; }

        /// <summary>
        /// Gets or sets the payed start date.
        /// </summary>
        /// <value>The payed start date.</value>
        [DisplayName("Payed after")]
        public DateTime? PayedStartDate { get; set; }

        /// <summary>
        /// Gets or sets the payed end date.
        /// </summary>
        /// <value>The payed end date.</value>
        [DisplayName("Payed before")]
        public DateTime? PayedEndDate { get; set; }
        
        /// <summary>
        /// Gets or sets the size of the page.
        /// </summary>
        /// <value>The size of the page.</value>
        public int PageSize { get; set; }
    }
}