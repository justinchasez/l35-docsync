﻿namespace DocSync.Server.Core.Enums
{
    /// <summary>
    /// Types if payments
    /// </summary>
    public enum PaymentTypes
    {
        /// <summary>
        /// Payment first computer (1 year)
        /// </summary>
        PaymentFirstPc1Year = 1,

        /// <summary>
        /// Payment first computer (2 year)
        /// </summary>
        PaymentFirstPc2Year,

        /// <summary>
        /// Payment first computer (3 year)
        /// </summary>
        PaymentFirstPc3Year,

        /// <summary>
        /// Payment next computer (1 year)
        /// </summary>
        PaymentNextPc1Year,

        /// <summary>
        /// Payment next computer (2 year)
        /// </summary>
        PaymentNextPc2Year,

        /// <summary>
        /// Payment next computer (3 year)
        /// </summary>
        PaymentNextPc3Year,

        /// <summary>
        /// Prolong computer (1 year)
        /// </summary>
        ProlongPc1Year,

        /// <summary>
        /// Prolong computer (2 year)
        /// </summary>
        ProlongPc2Year,

        /// <summary>
        /// Prolong computer (3 year)
        /// </summary>
        ProlongPc3Year,

        /// <summary>
        /// Buy additional place (2Gb)
        /// </summary>
        BuyAdditionalPlace2Gb,

        /// <summary>
        /// Buy additional place (5Gb)
        /// </summary>
        BuyAdditionalPlace5Gb,

        /// <summary>
        /// Buy additional place (10Gb)
        /// </summary>
        BuyAdditionalPlace10Gb,
    }
}
