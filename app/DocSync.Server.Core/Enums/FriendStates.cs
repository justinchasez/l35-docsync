﻿namespace DocSync.Server.Core.Enums
{
    /// <summary>
    /// Refered friends states
    /// </summary>
    public enum FriendStates
    {
        /// <summary>
        /// Friend is e-mailed
        /// </summary>
        Emailed,

        /// <summary>
        /// Friend is registered
        /// </summary>
        Registered,

        /// <summary>
        /// Friend is paid
        /// </summary>
        Paid
    }
}
