using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// Event argument for SetOption events
    /// </summary>
    [Serializable]
    public class SetOptionsEventArgs : EventArgs
    {
        /// <summary>
        /// Option type
        /// </summary>
        public AppOptions Option { get; set; }

        /// <summary>
        /// Option Value to set
        /// </summary>
        public int Value { get; set; }

        public string StringValue { get; set; }
    }
}