using System;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The parts event argument.
    /// </summary>
    [Serializable]
    public class SpeedChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpeedChangedEventArgs"/> class.
        /// </summary>
        public SpeedChangedEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeedChangedEventArgs"/> class.
        /// </summary>
        /// <param name="speed">The download/upload speed.</param>
        public SpeedChangedEventArgs(long speed)
        {
            this.Speed = speed;
        }

        /// <summary>
        /// Gets or sets the speed.
        /// </summary>
        /// <value>The speed.</value>
        public long Speed { get; set; }
    }
}