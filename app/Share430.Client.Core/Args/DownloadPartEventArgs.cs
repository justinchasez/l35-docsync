using System;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The parts event argument.
    /// </summary>
    public class DownloadPartEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadPartEventArgs"/> class.
        /// </summary>
        public DownloadPartEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadPartEventArgs"/> class.
        /// </summary>
        /// <param name="partSize">Size of the part.</param>
        /// <param name="partName">Name of the part.</param>
        public DownloadPartEventArgs(int partSize, string partName)
        {
            this.PartSize = partSize;
            this.PartName = partName;
        }

        /// <summary>
        /// Gets or sets the name of the part.
        /// </summary>
        /// <value>The name of the part.</value>
        public string PartName { get; set; }

        /// <summary>
        /// Gets or sets the size of the part.
        /// </summary>
        /// <value>The size of the part.</value>
        public int PartSize { get; set; }
    }
}