﻿using System.ComponentModel;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The Get Last Version Of Client App Completed Event Args
    /// </summary>
    public class GetLastVersionOfClientAppCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The async results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetLastVersionOfClientAppCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The async results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public GetLastVersionOfClientAppCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the async result.
        /// </summary>
        /// <value>The async result.</value>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}
