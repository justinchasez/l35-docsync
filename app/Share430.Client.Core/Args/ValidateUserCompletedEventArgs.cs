using System;
using System.ComponentModel;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The event argument for ValidateUserCompleted event.
    /// </summary>
    public class ValidateUserCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The results objects collection.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateUserCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public ValidateUserCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets a value indicating whether [validate user result].
        /// </summary>
        /// <value><c>true</c> if [validate user result]; otherwise, <c>false</c>.</value>
        public bool ValidateUserResult
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }

        /// <summary>
        /// Gets a value indicating whether [validate user result specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [validate user result specified]; otherwise, <c>false</c>.
        /// </value>
        public bool ValidateUserResultSpecified
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[1]));
            }
        }
    }
}