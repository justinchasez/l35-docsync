using System;
using System.ComponentModel;
using Share430.Client.Core.Entities.Computers;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The AddComputerCompleted async argument.
    /// </summary>
    public class GetComputerUsersCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The async results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetComputerUsersCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The async results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public GetComputerUsersCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the async result.
        /// </summary>
        /// <value>The async result.</value>
        public ComputerUsersResult Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ComputerUsersResult)(this.results[0]));
            }
        }
    }
}