using System;
using System.IO;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The event argument for UploadFileStarted event.
    /// </summary>
    public class UploadFileEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the file info.
        /// </summary>
        /// <value>The file info.</value>
        public FileInfo FileInfo { get; set; }

        /// <summary>
        /// Gets or sets file versions count
        /// </summary>
        public int VersionsCount { get; set; }
    }
}