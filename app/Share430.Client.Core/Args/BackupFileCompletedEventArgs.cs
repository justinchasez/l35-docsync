using System.ComponentModel;
using Share430.Client.Core.Entities.Files;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The BackupFileCompleted event argument.
    /// </summary>
    public class BackupFileCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="BackupFileCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public BackupFileCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the invocation result.
        /// </summary>
        /// <value>The invocation result.</value>
        public BackupFileResult Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((BackupFileResult)(this.results[0]));
            }
        }
    }
}