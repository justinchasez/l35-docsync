using System;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The event argument for DownloadFileProgressChanged event.
    /// </summary>
    public class DownloadFileProgressChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadFileProgressChangedEventArgs"/> class.
        /// </summary>
        /// <param name="progress">The progress.</param>
        public DownloadFileProgressChangedEventArgs(int progress)
        {
            this.Progress = progress;
        }

        /// <summary>
        /// Gets or sets the progress.
        /// </summary>
        /// <value>The progress.</value>
        public int Progress { get; set; }
    }
}