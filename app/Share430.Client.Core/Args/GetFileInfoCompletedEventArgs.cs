using System;
using System.ComponentModel;
using Share430.Client.Core.Entities.Files;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The argument for GetFileInfoCompleted event.
    /// </summary>
    public class GetFileInfoCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The async results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetFileInfoCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public GetFileInfoCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the async result.
        /// </summary>
        /// <value>The async result.</value>
        public FileInfoDto Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FileInfoDto)(this.results[0]));
            }
        }
    }
}