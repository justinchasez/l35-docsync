using System;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The parts event argument.
    /// </summary>
    public class UploadPartEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the name of the part.
        /// </summary>
        /// <value>The name of the part.</value>
        public string PartName { get; set; }

        /// <summary>
        /// Gets or sets the size of the part.
        /// </summary>
        /// <value>The size of the part.</value>
        public int PartSize { get; set; }
    }
}