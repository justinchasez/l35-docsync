using System;
using System.ComponentModel;
using Share430.Client.Core.Entities.Computers;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The GetComputersCompleted event argument.
    /// </summary>
    public class GetComputersCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="GetComputersCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The async results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public GetComputersCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState)
            : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the async result.
        /// </summary>
        /// <value>The async result.</value>
        public ComputerInfo[] Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((ComputerInfo[])(this.results[0]));
            }
        }
    }
}