using System;
using System.ComponentModel;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The AddComputerCompleted async argument.
    /// </summary>
    public class AddComputerCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The async results.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddComputerCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The async results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public AddComputerCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the async result.
        /// </summary>
        /// <value>The async result.</value>
        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}