using System;
using Share430.Client.Core.Collections;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The download file event argument.
    /// </summary>
    public class DownloadFileEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadFileEventArgs"/> class.
        /// </summary>
        public DownloadFileEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadFileEventArgs"/> class.
        /// </summary>
        /// <param name="item">The restore file item.</param>
        public DownloadFileEventArgs(RestoreFileQueueItem item)
        {
            this.RestoreFileInfo = item;
        }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public RestoreFileQueueItem RestoreFileInfo { get; set; }
    }
}