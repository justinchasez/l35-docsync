using System;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The download error event args.
    /// </summary>
    public class DownloadFileErrorEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadFileErrorEventArgs"/> class.
        /// </summary>
        public DownloadFileErrorEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadFileErrorEventArgs"/> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        public DownloadFileErrorEventArgs(Exception exception)
        {
            this.Exception = exception;
        }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>The exception.</value>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Gets or sets is limit reached
        /// </summary>
        public bool IsLimitReached { get; set; }

        /// <summary>
        /// Used space as bytes count.
        /// </summary>
        public long UsedSpace { get; set; }

        /// <summary>
        /// Is computer expired
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Is invalid time
        /// </summary>
        public bool IsInvalidTime { get; set; }
    }
}