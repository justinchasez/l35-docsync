using System;
using System.ComponentModel;
using Share430.Client.Core.Entities.Users;

namespace Share430.Client.Core.Args
{
    /// <summary>
    /// The event argument for RegisterCompleted event.
    /// </summary>
    public class RegisterCompletedEventArgs : AsyncCompletedEventArgs
    {
        /// <summary>
        /// The results objects collection.
        /// </summary>
        private readonly object[] results;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterCompletedEventArgs"/> class.
        /// </summary>
        /// <param name="results">The results.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="cancelled">if set to <c>true</c> [cancelled].</param>
        /// <param name="userState">State of the user.</param>
        public RegisterCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) : base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <value>The result.</value>
        public UserRegistrationResult Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UserRegistrationResult)(this.results[0]));
            }
        }
    }
}