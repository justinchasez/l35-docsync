﻿using System;
using System.ComponentModel;

namespace Share430.Client.Core.Args
{
    public class GetFileLinkCompletedEventArgs : AsyncCompletedEventArgs
    {
        private object[] results;

        public GetFileLinkCompletedEventArgs(object[] results, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
        {
            this.results = results;
        }

        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return (string)results[0];
            }
        }
    }
}
