using System;
using System.ComponentModel;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Application;

namespace Share430.Client.Core.Services
{
    /// <summary>
    /// The download service.
    /// </summary>
    public interface IDownloadService : IDisposable
    {
        #region Events

        /// <summary>
        /// Occurs when Download started.
        /// </summary>
        event EventHandler<DownloadFileEventArgs> DownloadStarted;

        /// <summary>
        /// Occurs when download part started.
        /// </summary>
        event EventHandler<CancelEventArgs> DownloadPartStarting;

        /// <summary>
        /// Occurs when download part started.
        /// </summary>
        event EventHandler<DownloadPartEventArgs> DownloadPartStarted;

        /// <summary>
        /// Occurs when Download paused.
        /// </summary>
        event EventHandler DownloadPaused;

        /// <summary>
        /// Occurs when Download continued.
        /// </summary>
        event EventHandler DownloadContinued;

        /// <summary>
        /// Occurs when Download canceled.
        /// </summary>
        event EventHandler DownloadCanceled;

        /// <summary>
        /// Occurs when download part completed.
        /// </summary>
        event EventHandler<DownloadPartEventArgs> DownloadPartCompleted;

        /// <summary>
        /// Occurs when Download completed.
        /// </summary>
        event EventHandler<DownloadFileEventArgs> DownloadCompleted;

        /// <summary>
        /// Occurs when Download error.
        /// </summary>
        event EventHandler<DownloadFileErrorEventArgs> DownloadError;

        #endregion

        /// <summary>
        /// Gets the Download file info.
        /// </summary>
        /// <value>The Download file info.</value>
        RestoreFileQueueItem CurrentFile { get; }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        DownloadState CurrentState { get; }

        /// <summary>
        /// Gets the speed man.
        /// </summary>
        /// <value>The speed man.</value>
        SpeedManager SpeedMan { get; }

        /// <summary>
        /// Starts the Download.
        /// </summary>
        /// <param name="restoreFileInfo">The restore file info.</param>
        void StartDownload(RestoreFileQueueItem restoreFileInfo);

        /// <summary>
        /// Pauses the download.
        /// </summary>
        void PauseDownload();

        /// <summary>
        /// Cancels the download.
        /// </summary>
        void CancelDownload();

        /// <summary>
        /// Continues the download.
        /// </summary>
        void ContinueDownload();
    }
}