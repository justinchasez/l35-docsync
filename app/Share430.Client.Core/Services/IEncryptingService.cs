namespace Share430.Client.Core.Services
{
    /// <summary>
    /// The ecoding service.
    /// </summary>
    public interface IEncryptingService
    {
        /// <summary>
        /// Encodes the data.
        /// </summary>
        /// <param name="data">The source data.</param>
        /// <returns>The encrypted data.</returns>
        byte[] EncryptData(byte[] data);

        /// <summary>
        /// Decodes the data.
        /// </summary>
        /// <param name="data">The encrypted data.</param>
        /// <returns>The decrypted data.</returns>
        byte[] DecryptData(byte[] data);
    }
}