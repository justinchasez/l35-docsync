﻿using System;
using Share430.Client.Core.Args;

namespace Share430.Client.Core.Services.Web
{
    /// <summary>
    /// The updates service
    /// </summary>
    public interface IUpdateService : IServiceBase
    {
        /// <summary>
        /// Occurs when [Get Last Version Of Client App Completed].
        /// </summary>
        event EventHandler<GetLastVersionOfClientAppCompletedEventArgs> GetLastVersionOfClientAppCompleted;

        /// <summary>
        /// Occurs when [Get Link To Last Version Completed].
        /// </summary>
        event EventHandler<GetLinkToLastVersionCompletedEventArgs> GetLinkToLastVersionCompleted;

        /// <summary>
        /// Get last version of client app
        /// </summary>
        /// <returns>Last version of client app</returns>
        string GetLastVersionOfClientApp();

        /// <summary>
        /// Get last version of client app async
        /// </summary>
        void GetLastVersionOfClientAppAsync();

        /// <summary>
        /// Get last version of client app async
        /// </summary>
        /// <param name="userState">State of user</param>
        void GetLastVersionOfClientAppAsync(object userState);

        /// <summary>
        /// Get Link To Last Version
        /// </summary>
        /// <returns>Link To Last Version</returns>
        string GetLinkToLastVersion();

        /// <summary>
        /// Get Link To Last Version Async
        /// </summary>
        void GetLinkToLastVersionAsync();

        /// <summary>
        /// Get Link To Last Version Async
        /// </summary>
        /// <param name="userState">State of user</param>
        void GetLinkToLastVersionAsync(object userState);
    }
}
