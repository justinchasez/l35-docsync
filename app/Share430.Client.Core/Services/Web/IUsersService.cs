using Share430.Client.Core.Entities.Users;

namespace Share430.Client.Core.Services.Web
{
    /// <summary>
    /// The registration service client.
    /// </summary>
    public interface IUsersService : IServiceBase
    {
        /// <summary>
        /// Registers the specified inputs.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>The user registration result.</returns>
        UserRegistrationResult Register(UserRegistrationInputs inputs);
        
        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>True if user validated, otherwise false.</returns>
        bool ValidateUser(string name, string password, out string token);
    }
}