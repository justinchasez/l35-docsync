using System.Net;

namespace Share430.Client.Core.Services.Web
{
    /// <summary>
    /// The computers service.
    /// </summary>
    public interface IServiceBase
    {
        /// <summary>
        /// Sets the web proxy.
        /// </summary>
        /// <param name="proxy">The proxy.</param>
        void SetWebProxy(WebProxy proxy);

        void SetToken(string token);
    }
}