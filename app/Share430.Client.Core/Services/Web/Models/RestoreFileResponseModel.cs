using System.Collections.Generic;

namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RestoreFileResponseModel
    {
        public long FileSize { get; set; }
        public string FilePath { get; set; }
        public string FileHash { get; set; }
        public List<FilePartModel> FileParts { get; set; }
    }
}