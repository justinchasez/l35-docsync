namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BackupFileResultModel
    {
        public bool IsFinished { get; set; }
        public bool IsLimitReached { get; set; }
        public int PartsCount { get; set; }
        public int VersionsCount { get; set; }
        public long UsedSpace { get; set; }
        public long? FreeSpace { get; set; }
    }
}