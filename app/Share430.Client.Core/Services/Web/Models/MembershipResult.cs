﻿using System.Collections.Generic;

namespace Share430.Client.Core.Services.Web.Models
{
	public class MembershipResult
	{
		public MembershipResult()
		{
			Errors = new List<string>();
		}

        public bool IsSuccess { get; set; }

		public long UserId { get; set; }

		public string Token { get; set; }

		public List<string> Errors { get; set; } 
	}
}
