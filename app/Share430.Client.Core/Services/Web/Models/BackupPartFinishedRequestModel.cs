using System;

namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BackupPartFinishedRequestModel
    {
        public string FileName { get; set; }
        public int PartLength { get; set; }
        public Guid S3PartId { get; set; }
    }
}