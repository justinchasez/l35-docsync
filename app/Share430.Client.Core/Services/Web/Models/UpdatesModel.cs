﻿using System;
using System.Collections.Generic;

namespace Share430.Client.Core.Services.Web.Models
{
    public class UpdatesModel
    {
        public List<string> Files { get; set; }
        public List<string> Removed { get; set; }
        public DateTime CheckDate { get; set; }
    }
}
