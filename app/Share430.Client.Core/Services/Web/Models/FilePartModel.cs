using System;

namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FilePartModel
    {
        public Guid S3PartId { get; set; }
        public int Length { get; set; }
        public int Number { get; set; }
    }
}