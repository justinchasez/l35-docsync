namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RestoreFileRequestModel
    {
        public string FileName { get; set; }
        public int Version { get; set; }
    }
}