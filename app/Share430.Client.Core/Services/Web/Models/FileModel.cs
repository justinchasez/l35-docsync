﻿using System.Collections.Generic;

namespace Share430.Client.Core.Services.Web.Models
{
    public class FileModel
    {
        public long Id { get; set; }

        public string FileName { get; set; }
    
        public List<FileVersionModel> FileVersions { get; set; }  
    }
}