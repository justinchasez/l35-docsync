﻿using System;

namespace Share430.Client.Core.Services.Web.Models
{
    [Serializable]
    public class FolderModel
    {
        public long Id { get; set; }

        public string LocalComputerGuid { get; set; }

        public bool IsManualEncryptionEnabled { get; set; }

        public int? BackupLimit { get; set; }

        public long BytesUsed { get; set; }

        public long? FreeSpace { get; set; }
    }
}