using System;

namespace Share430.Client.Core.Services.Web.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class BackupFileRequestModel
    {
        public string FileName { get; set; }
        public long Size { get; set; }
        public string OldFileName { get; set; }
        public bool CreateCopy { get; set; }
        public string FileHash { get; set; }
        public DateTime? LastSavedDate { get; set; }
    }
}