using System;

namespace Share430.Client.Core.Services.Web.Models
{
    public class FileVersionModel
    {
        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        /// <value>The version number.</value>
        public int FileVersion { get; set; }

        /// <summary>
        /// Gets or sets the finished date.
        /// </summary>
        /// <value>The finished date.</value>
        public DateTime FinisedDate { get; set; }

        /// <summary>
        /// Gets or sets the last saved date.
        /// </summary>
        /// <value>The last saved date.</value>
        public DateTime LastSavedOn { get; set; }

        /// <summary>
        /// Gets or sets the size of the file.
        /// </summary>
        /// <value>The size of the file.</value>
        public long Size { get; set; }
    }
}