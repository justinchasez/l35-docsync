using System;
using System.Collections.Generic;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Core.Services.Web
{
    /// <summary>
    /// The computers service.
    /// </summary>
    public interface IComputersService : IServiceBase
    {
   

        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <returns>The list of the computers.</returns>
        FolderModel[] GetComputers();

       

        /// <summary>
        /// Adds the computer.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>The Id of the computer.</returns>
        string AddComputer(ComputerInfo computerInfo);

       
        /// <summary>
        /// Gets the computer users.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>Users from computer</returns>
        ComputerUsersResult GetComputerUsers(ComputerInfo computerInfo);

    

        /// <summary>
        /// Syncronize windows users
        /// </summary>
        /// <param name="syncronizeUsersInputs">Syncronization inputs</param>
        /// <returns>Result of syncronization</returns>
        SyncronizeUsersResult SyncronizeUsers(SyncronizeUsersInputs syncronizeUsersInputs);

     

        /// <summary>
        /// Updates the computer GUID.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        void UpdateComputerGuid(UpdateComputerGuidInputs inputs);

        /// <summary>
        /// Get updated files list
        /// </summary>
        /// <param name="lastDate">last update date</param>
        /// <returns></returns>
        UpdatesModel GetUpdates(DateTime lastDate);

        List<string> GetAllFolders();

        void CreateFolder(string folderName);

        void RemoveFolder(string folderName);
    }
}