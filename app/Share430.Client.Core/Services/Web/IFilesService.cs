using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Core.Services.Web
{
    /// <summary>
    /// The files service.
    /// </summary>
    public interface IFilesService : IServiceBase
    {
      
        /// <summary>
        /// Backups the file.
        /// </summary>
        /// <param name="backupFileInputs">The backup file inputs.</param>
        /// <returns>Backup file result.</returns>
        BackupFileResultModel BackupFile(BackupFileRequestModel backupFileInputs);

     
     
      
        /// <summary>
        /// Parts the backup finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The backup finished inputs.</param>
        void BackupPartFinished(BackupPartFinishedRequestModel backupFinishedInputs);

        
         /// <summary>
        /// Files the backup finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The backup finished inputs.</param>
        void BackupFileFinished(BackupFileFinishedInputs backupFinishedInputs);

     
        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="restoreFileInputs">The restore file inputs.</param>
        /// <returns>The restore file results.</returns>
        RestoreFileResponseModel RestoreFile(RestoreFileRequestModel restoreFileInputs);

            /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <param name="fileInfoInputs">The file info inputs.</param>
        /// <returns>The file info.</returns>
        FileModel GetFileInfo(GetFileInfoInputs fileInfoInputs);

            /// <summary>
        /// Gets all files from computer.
        /// </summary>
        /// <param name="inputs">The GetAllFilesFromComputer inputs.</param>
        /// <returns>Files list.</returns>
        ComputerFilesDto GetAllFilesFromComputer(GetAllFilesFromComputerInputs inputs);

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="inputs">The file inputs.</param>
        /// <returns>
        /// [true] if the removal was successful, otherwise - [false]
        /// </returns>
        RemovingFileResult RemoveFile(GetFileInfoInputs inputs);

         /// <summary>
        /// Gets the file link.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>The link to the file</returns>
        string GetFileLink(BackupFileInputs inputs);
    }
}