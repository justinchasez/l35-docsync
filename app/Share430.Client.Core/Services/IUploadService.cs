using System;
using System.ComponentModel;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Application;
using Share430.Client.Core.Services.Web;

namespace Share430.Client.Core.Services
{
    /// <summary>
    /// The upload files service.
    /// </summary>
    public interface IUploadService : IDisposable
    {
        #region Events

        /// <summary>
        /// Occurs when upload starting.
        /// </summary>
        event EventHandler<UploadFileEventArgs> UploadFileStarting;

        /// <summary>
        /// Occurs when upload started].
        /// </summary>
        event EventHandler<UploadFileEventArgs> UploadFileStarted;

        /// <summary>
        /// Occurs when [upload part starting].
        /// </summary>
        event EventHandler<CancelEventArgs> UploadPartStarting;

        /// <summary>
        /// Occurs when [upload part started].
        /// </summary>
        event EventHandler<UploadPartEventArgs> UploadPartStarted;

        /// <summary>
        /// Occurs when [upload part completed].
        /// </summary>
        event EventHandler<UploadPartEventArgs> UploadPartCompleted;

        /// <summary>
        /// Occurs when upload paused.
        /// </summary>
        event EventHandler UploadFilePaused;

        /// <summary>
        /// Occurs when upload continued.
        /// </summary>
        event EventHandler UploadFileContinued;

        /// <summary>
        /// Occurs when upload canceled.
        /// </summary>
        event EventHandler UploadFileCanceled;

        /// <summary>
        /// Occurs when upload completed.
        /// </summary>
        event EventHandler<UploadFileEventArgs> UploadFileCompleted;

        /// <summary>
        /// Occurs when upload error.
        /// </summary>
        event EventHandler<DownloadFileErrorEventArgs> UploadFileError;

        #endregion
        
        /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <value>The file info.</value>
//        FileInfo FileInfo { get; }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        UploadState CurrentState { get; }

        /// <summary>
        /// Gets the file service.
        /// </summary>
        /// <value>The file service.</value>
        IFilesService FileService { get; }

        /// <summary>
        /// Gets the speed man.
        /// </summary>
        /// <value>The speed man.</value>
        SpeedManager SpeedMan { get; }

        /// <summary>
        /// Starts the upload.
        /// </summary>
        /// <param name="fileItem">The file item.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptInfo">The encrypt info.</param>
        void StartUpload(FileQueueItem fileItem, int computerId, EncryptInfo encryptInfo);

        /// <summary>
        /// Pauses the download.
        /// </summary>
        void PauseUpload();

        /// <summary>
        /// Cancels the download.
        /// </summary>
        void CancelUpload();

        /// <summary>
        /// Continues the download.
        /// </summary>
        void ContinueUpload();
    }
}