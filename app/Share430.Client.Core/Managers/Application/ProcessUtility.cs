using System;
using System.Runtime.InteropServices;

namespace Share430.Client.Core.Managers.Application
{
    /// <summary>
    /// Class ProcessUtility
    /// </summary>
    public class ProcessUtility
    {
        /// <summary>
        /// StartInfo struct
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct STARTUPINFO
        {
            /// <summary>
            /// The struct field
            /// </summary>
            public int Size;

            /// <summary>
            /// The struct field
            /// </summary>
            public string Reserved;

            /// <summary>
            /// The struct field
            /// </summary>
            public string Desktop;

            /// <summary>
            /// The struct field
            /// </summary>
            public string Title;

            /// <summary>
            /// The struct field
            /// </summary>
            public int X;

            /// <summary>
            /// The struct field
            /// </summary>
            public int Y;

            /// <summary>
            /// The struct field
            /// </summary>
            public int XSize;

            /// <summary>
            /// The struct field
            /// </summary>
            public int XCountChars;

            /// <summary>
            /// The struct field
            /// </summary>
            public int YCountChars;

            /// <summary>
            /// The struct field
            /// </summary>
            public int FillAttribute;

            /// <summary>
            /// The struct field
            /// </summary>
            public int Flags;

            /// <summary>
            /// The struct field
            /// </summary>
            public short ShowWindow;

            /// <summary>
            /// The struct field
            /// </summary>
            public short Reserved2;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr PReserved2;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr StdInput;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr StdOutput;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr StdError;
        }

        /// <summary>
        /// PROCESS_INFORMATION Struct
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct PROCESS_INFORMATION
        {
            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr Process;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr Thread;

            /// <summary>
            /// The struct field
            /// </summary>
            public int ProcessID;

            /// <summary>
            /// The struct field
            /// </summary>
            public int ThreadID;
        }

        /// <summary>
        /// SECURITY_ATTRIBUTES struct
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            /// <summary>
            /// The struct field
            /// </summary>
            public int Length;

            /// <summary>
            /// The struct field
            /// </summary>
            public IntPtr SecurityDescriptor;

            /// <summary>
            /// The struct field
            /// </summary>
            public bool InheritHandle;
        }

        /// <summary>
        /// SECURITY_IMPERSONATION_LEVEL enum
        /// </summary>
        public enum SECURITY_IMPERSONATION_LEVEL
        {
            /// <summary>
            /// The enum value
            /// </summary>
            SecurityAnonymous,

            /// <summary>
            /// The enum value
            /// </summary>
            SecurityIdentification,

            /// <summary>
            /// The enum value
            /// </summary>
            SecurityImpersonation,

            /// <summary>
            /// The enum value
            /// </summary>
            SecurityDelegation
        }

        /// <summary>
        /// TOKEN_TYPE enum
        /// </summary>
        public enum TOKEN_TYPE
        {
            /// <summary>
            /// The enum value
            /// </summary>
            TokenPrimary = 1,

            /// <summary>
            /// The enum value
            /// </summary>
            TokenImpersonation
        }

        /// <summary>
        /// All access generic
        /// </summary>
        public const int GENERIC_ALL_ACCESS = 0x10000000;

        /// <summary>
        /// CloseHandle method
        /// </summary>
        /// <param name="handle">handle value</param>
        /// <returns>return result</returns>
        [DllImport("kernel32.dll",
              EntryPoint = "CloseHandle", SetLastError = true,
              CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CloseHandle(IntPtr handle);

        /// <summary>
        /// CreateProcessAsUser method
        /// </summary>
        /// <param name="token">user token</param>
        /// <param name="applicationName">application name</param>
        /// <param name="commandLine">command line</param>
        /// <param name="processAttributes">process attributes</param>
        /// <param name="threadAttributes">thread attributes</param>
        /// <param name="inheritHandle">inherit handle</param>
        /// <param name="creationFlags">creation flags</param>
        /// <param name="envrionment">environment variables</param>
        /// <param name="currentDirectory">current directory</param>
        /// <param name="startupInfo">start up info</param>
        /// <param name="processInformation">process information</param>
        /// <returns>return result</returns>
        [DllImport("advapi32.dll",
              EntryPoint = "CreateProcessAsUser", SetLastError = true,
              CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CreateProcessAsUser(IntPtr token,
                                                      string applicationName,
                                                      string commandLine,
                                                      ref SECURITY_ATTRIBUTES processAttributes,
                                                      ref SECURITY_ATTRIBUTES threadAttributes,
                                                      bool inheritHandle,
                                                      int creationFlags,
                                                      IntPtr envrionment,
                                                      string currentDirectory,
                                                      ref STARTUPINFO startupInfo,
                                                      ref PROCESS_INFORMATION processInformation);

        /// <summary>
        /// DuplicateTokenEx method
        /// </summary>
        /// <param name="existingToken">existing token</param>
        /// <param name="desiredAccess">desired access</param>
        /// <param name="threadAttributes">thread attributes</param>
        /// <param name="impersonationLevel">impersonation level</param>
        /// <param name="tokenType">th token type</param>
        /// <param name="newToken">new token value</param>
        /// <returns>return result</returns>
        [DllImport("advapi32.dll",
              EntryPoint = "DuplicateTokenEx")]
        public static extern bool DuplicateTokenEx(IntPtr existingToken,
                                                   int desiredAccess,
                                                   ref SECURITY_ATTRIBUTES threadAttributes,
                                                   int impersonationLevel,
                                                   int tokenType,
                                                   ref IntPtr newToken);
    }
}