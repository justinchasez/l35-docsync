using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using Share430.Client.Core.Managers.Settings;

namespace Share430.Client.Core.Managers.Application
{
    /// <summary>
    /// Application instance manager
    /// </summary>
    public static class ApplicationInstanceManager
    {
        /// <summary>
        /// The sync object key.
        /// </summary>
        private const string MUTEX_KEY = "ONLINE_BACKUP_MUTEX_KEY";

        /// <summary>
        /// The sync object key.
        /// </summary>
        private const string AGENT_MUTEX_KEY = "ONLINE_BACKUP_AGENT_MUTEX_KEY";

        /// <summary>
        /// The wait event key.
        /// </summary>
        public const string WAIT_EVENT_KEY = "ONLINE_BACKUP_WAIT_LOADING_KEY";

        /// <summary>
        /// The wait event key.
        /// </summary>
        public const string AGENT_WAIT_EVENT_KEY = "ONLINE_BACKUP_AGENT_WAIT_LOADING_KEY";

        /// <summary>
        /// The sync event object.
        /// </summary>
        private static EventWaitHandle waitEvent;

        /// <summary>
        /// The sync object.
        /// </summary>
        private static Mutex singleInstanceMutex;

        /// <summary>
        /// The sync object.
        /// </summary>
        private static Mutex singleAgentInstanceMutex;

        /// <summary>
        /// The system broadcast.
        /// </summary>
        public const int HWND_BROADCAST = 0xffff;

        /// <summary>
        /// The show application message.
        /// </summary>
        public static readonly int WM_SHOW_ONLINE_BACKUP = RegisterWindowMessage("WM_SHOW_ONLINE_BACKUP");

        /// <summary>
        /// The close application message.
        /// </summary>
        public static readonly int WM_CLOSE_ONLINE_BACKUP = RegisterWindowMessage("WM_CLOSE_ONLINE_BACKUP");

        /// <summary>
        /// The set options message.
        /// </summary>
        public static readonly int WM_SET_ONLINE_BACKUP_OPTIONS = RegisterWindowMessage("WM_SET_ONLINE_BACKUP_OPTIONS");

        /// <summary>
        /// The show options message.
        /// </summary>
        public static readonly int WM_SHOW_ONLINE_BACKUP_OPTIONS = RegisterWindowMessage("WM_SHOW_ONLINE_BACKUP_OPTIONS");

        /// <summary>
        /// The check permision for unpause message.
        /// </summary>
        public static readonly int WM_UNPAUSE_PERMISION = RegisterWindowMessage("WM_UNPAUSE_PERMISION");

        /// <summary>
        /// The show Search For Files To Restore message.
        /// </summary>
        public static readonly int WM_SEARCH_FOR_FILES_TO_RESTORE = RegisterWindowMessage("WM_SEARCH_FOR_FILES_TO_RESTORE");

        /// <summary>
        /// The show status message.
        /// </summary>
        public static readonly int WM_SHOW_ONLINE_BACKUP_STATUS = RegisterWindowMessage("WM_SHOW_ONLINE_BACKUP_STATUS");

        /// <summary>
        /// The show about message.
        /// </summary>
        public static readonly int WM_SHOW_ONLINE_BACKUP_ABOUT = RegisterWindowMessage("WM_SHOW_ONLINE_BACKUP_ABOUT");

        /// <summary>
        /// The check permision for unpause message.
        /// </summary>
        public static readonly int WM_LAUNCH_RESTORE_WIZARD = RegisterWindowMessage("WM_LAUNCH_RESTORE_WIZARD");

        /// <summary>
        /// Update status info in virtual drive
        /// </summary>
        public static readonly int WM_UPDATE_STATUS_INFO = RegisterWindowMessage("WM_UPDATE_STATUS_INFO");

        /// <summary>
        /// The end session message key.
        /// </summary>
        public static readonly int WM_QUERYENDSESSION = 0x11;

        /// <summary>
        /// Posts the message.
        /// </summary>
        /// <param name="hwnd">The windows handle.</param>
        /// <param name="msg">The message key.</param>
        /// <param name="wparam">The wparam.</param>
        /// <param name="lparam">The lparam.</param>
        /// <returns>The post result.</returns>
        [DllImport("user32.dll")]
        private static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        /// <summary>
        /// Sends the message.
        /// </summary>
        /// <param name="hwnd">The windows handle.</param>
        /// <param name="msg">The message key.</param>
        /// <param name="wparam">The wparam.</param>
        /// <param name="lparam">The lparam.</param>
        /// <returns>The post result.</returns>
        [DllImport("user32.dll")]
        private static extern bool SendMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        /// <summary>
        /// Registers the window message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>The message registration ID.</returns>
        [DllImport("user32.dll")]
        private static extern int RegisterWindowMessage(string message);

        /// <summary>
        /// Details on http://www.ai.uga.edu/mc/SingleInstance.html
        /// </summary>
        /// <returns>
        /// True if only one instance is running, otherwise false
        /// </returns>
        public static bool IsFirstInstance()
        {
            // if new mutex has been created then application runs for the first, and no another instance loaded.
            bool result = true, createdNewMutex;
            
            singleInstanceMutex = new Mutex(true, MUTEX_KEY, out createdNewMutex);
            if (!createdNewMutex)
            {
                result = false;
            }

            GC.KeepAlive(createdNewMutex);

            return result;
        }

        /// <summary>
        /// Details on http://www.ai.uga.edu/mc/SingleInstance.html
        /// </summary>
        /// <returns>True if only one instance is running, otherwise false</returns>
        public static bool IsFirstAgentInstance()
        {
            bool result = true, createdNewMutex;

            singleAgentInstanceMutex = new Mutex(true, AGENT_MUTEX_KEY, out createdNewMutex);
            if (!createdNewMutex)
            {
                result = false;
            }

            GC.KeepAlive(createdNewMutex);

            return result;
        }

        /// <summary>
        /// Details on http://www.sanity-free.org/143/csharp_dotnet_single_instance_application.html
        /// </summary>
        /// <param name="messageId">The message Id.</param>
        public static void SendBroadcastMessage(int messageId)
        {
            SendMessage((IntPtr)HWND_BROADCAST, messageId, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Details on http://www.sanity-free.org/143/csharp_dotnet_single_instance_application.html
        /// </summary>
        /// <param name="messageId">The message Id.</param>
        public static void PostBroadcastMessage(int messageId)
        {
            PostMessage((IntPtr)HWND_BROADCAST, messageId, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Details on http://www.sanity-free.org/143/csharp_dotnet_single_instance_application.html
        /// </summary>
        /// <param name="window">handle of window</param>
        /// <param name="messageId">The message Id.</param>
        /// <param name="msglParam">l parameter to post message</param>
        /// <param name="msgwParam">w parameter to post message</param>
        public static void PostMessage(int window, int messageId, int msglParam, int msgwParam)
        {
            PostMessage((IntPtr)window, messageId, (IntPtr)msgwParam, (IntPtr)msglParam);
        }

        /// <summary>
        /// Details on http://www.sanity-free.org/143/csharp_dotnet_single_instance_application.html
        /// </summary>
        /// <param name="messageId">The message Id.</param>
        /// <param name="msglParam">l parameter to post message</param>
        /// <param name="msgwParam">w parameter to post message</param>
        public static void PostBroadcastMessage(int messageId, int msglParam, int msgwParam)
        {
            PostMessage((IntPtr)HWND_BROADCAST, messageId, (IntPtr)msgwParam, (IntPtr)msglParam);
        }

        /// <summary>
        /// Posts the broadcast message with checking app running.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        public static void PostBroadcastMessageWithCheckingAppRunning(int messageId)
        {
            CheckClientRunning(String.Empty);
            PostBroadcastMessage(messageId);
        }

        /// <summary>
        /// Posts the broadcast message with checking app running.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        /// <param name="msglParam">l parameter to post message</param>
        /// <param name="msgwParam">w parameter to post message</param>
        public static void PostBroadcastMessageWithCheckingAppRunning(int messageId, int msglParam, int msgwParam)
        {
            CheckClientRunning(String.Empty);
            PostBroadcastMessage(messageId, msglParam, msgwParam);
        }
        
        /// <summary>
        /// Checks the client running.
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is client running]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsClientRunning()
        {
            string clientName = SettingsManager.CLIENT_FILE_NAME;
            var processes = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(clientName));
            for (int index = 0; index < processes.Length; index++)
            {
                try
                {
                    if ((processes[index].MainModule != null) && (processes[index].MainModule.ModuleName == clientName))
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Close client application
        /// </summary>
        public static void CloseClient()
        {
            if (IsClientRunning())
            {
                PostBroadcastMessage(WM_CLOSE_ONLINE_BACKUP);
            }
        }

        /// <summary>
        /// Send message when instances the loading completed.
        /// </summary>
        public static void InstanceLoadingCompleted()
        {
            try
            {
                /*EventWaitHandle*/ waitEvent = EventWaitHandle.OpenExisting(WAIT_EVENT_KEY);
                waitEvent.Set();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Send message when instances the loading completed.
        /// </summary>
        public static void AgentInstanceLoadingCompleted()
        {
            try
            {
                /*EventWaitHandle*/
                waitEvent = EventWaitHandle.OpenExisting(AGENT_WAIT_EVENT_KEY);
                waitEvent.Set();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Checks the client running.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <returns>
        /// 	<c>true</c> if [is client running] [the specified arguments]; otherwise, <c>false</c>.
        /// </returns>
        public static bool CheckClientRunning(string arguments)
        {
            if (!IsClientRunning())
            {
                try
                {
                    waitEvent = EventWaitHandle.OpenExisting(WAIT_EVENT_KEY);
                }
                catch
                {
                    waitEvent = null;
                }
                finally
                {
                    if (waitEvent == null)
                    {
                        waitEvent = new EventWaitHandle(
                        false,
                        EventResetMode.ManualReset,
                        WAIT_EVENT_KEY);
                    }
                }

                /*EventWaitHandle*/
                waitEvent.Reset();

                string applicationPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                                                      SettingsManager.CLIENT_FILE_NAME);

                if (File.Exists(applicationPath))
                {
//                    CreateProcessAsUser(applicationPath, arguments);
                    Process.Start(applicationPath, arguments);

                    TimeSpan waitingTime = new TimeSpan(0, 0, 5);
//                    Thread.Sleep(waitingTime);
                    waitEvent.WaitOne(waitingTime, false);
                }

                waitEvent.Close();
                return false;
            }

            return true;
        }

    /// <summary>
        /// CreateProcessAsUser method
        /// </summary>
        /// <param name="applicationName">application name</param>
        /// <param name="args">run arguments</param>
        public static void CreateProcessAsUser(string applicationName, string args)
        {
            IntPtr existingToken = WindowsIdentity.GetCurrent().Token;
            IntPtr dupedToken = IntPtr.Zero;

            ProcessUtility.PROCESS_INFORMATION pi = new ProcessUtility.PROCESS_INFORMATION();

            try
            {
                ProcessUtility.SECURITY_ATTRIBUTES sa = new ProcessUtility.SECURITY_ATTRIBUTES();
                sa.Length = Marshal.SizeOf(sa);

                bool result = ProcessUtility.DuplicateTokenEx(
                      existingToken,
                      ProcessUtility.GENERIC_ALL_ACCESS,
                      ref sa,
                      (int)ProcessUtility.SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation,
                      (int)ProcessUtility.TOKEN_TYPE.TokenPrimary,
                      ref dupedToken);

                if (!result)
                {
                    throw new ApplicationException("DuplicateTokenEx failed");
                }

                ProcessUtility.STARTUPINFO si = new ProcessUtility.STARTUPINFO();
                si.Size = Marshal.SizeOf(si);
                si.Desktop = String.Empty;

                result = ProcessUtility.CreateProcessAsUser(
                                     dupedToken,
                                     applicationName,
                                     args,
                                     ref sa,
                                     ref sa,
                                     false,
                                     0,
                                     IntPtr.Zero,
                                     string.Empty,
                                     ref si,
                                     ref pi);

                if (!result)
                {
                    int error = Marshal.GetLastWin32Error();
                    string message = String.Format("CreateProcessAsUser Error: {0}", error);
                    throw new ApplicationException(message);
                }
            }
            finally
            {
                if (pi.Process != IntPtr.Zero)
                {
                    ProcessUtility.CloseHandle(pi.Process);
                }

                if (pi.Thread != IntPtr.Zero)
                {
                    ProcessUtility.CloseHandle(pi.Thread);
                }

                if (dupedToken != IntPtr.Zero)
                {
                    ProcessUtility.CloseHandle(dupedToken);
                }
            }
        }
    }
}