using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using Share430.Client.Core.Args;

namespace Share430.Client.Core.Managers.Application
{
    /// <summary>
    /// Class SpeedManager
    /// </summary>
    public class SpeedManager
    {
        /// <summary>
        /// Syncronization object
        /// </summary>
        private object syncObject;

        /// <summary>
        /// speed manager data
        /// </summary>
        private List<int> data;

        /// <summary>
        /// Speed update tomer
        /// </summary>
        private Timer timer;

        /// <summary>
        /// Download/upload speed.
        /// </summary>
        private long speed;

        /// <summary>
        /// Last speed update.
        /// </summary>
        private DateTime lastUpdate;

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeedManager"/> class.
        /// </summary>
        public SpeedManager()
        {
            this.timer = new Timer(3000);
            this.timer.Elapsed += this.TimerTick;
            this.data = new List<int>();
            this.syncObject = new object();
        }

        /// <summary>
        /// Timers the tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerTick(object sender, ElapsedEventArgs e)
        {
            if (this.data.Count > 0)
            {
                long dataLength = 0;
                DateTime now;

                lock (this.syncObject)
                {
                    this.data.ForEach(x => dataLength += x);
                    this.data.Clear();
                    now = DateTime.Now;
                }

                if (dataLength > 0)
                {
                    this.Speed = (long)((dataLength / (now - this.lastUpdate).TotalMilliseconds) * 1000);
                }

                Trace.WriteLine(String.Format("Speed calculated: : {0}.{1}, {2} bytes, {3} bytes/s",
                                              DateTime.Now.ToLongTimeString(),
                                              DateTime.Now.Millisecond,
                                              dataLength,
                                              this.Speed));
                this.lastUpdate = now;
            }
        }

        /// <summary>
        /// Gets the speed.
        /// </summary>
        /// <value>The speed.</value>
        public long Speed
        {
            get
            {
                return this.speed;
            }

            protected set
            {
                if (this.speed != value)
                {
                    this.OnSpeedChanged(new SpeedChangedEventArgs(value));
                }

                this.speed = value;
            }
        }

        /// <summary>
        /// Starts the manager.
        /// </summary>
        public void StartManager()
        {
            lock (this.syncObject)
            {
                this.data.Clear();
                this.lastUpdate = DateTime.Now;
            }

            this.timer.Start();
        }

        /// <summary>
        /// Updates the speed.
        /// </summary>
        /// <param name="dataLength">Length of the data.</param>
        public void UpdateSpeed(int dataLength)
        {
            lock (this.syncObject)
            {
                this.data.Add(dataLength);
                //Trace.WriteLine(String.Format("Speed Updated: : {0}.{1}, {2} bytes",
                //                              DateTime.Now.ToLongTimeString(),
                //                              DateTime.Now.Millisecond,
                //                              dataLength));
            }
        }

        /// <summary>
        /// Occurs when [speed changed].
        /// </summary>
        public event EventHandler<SpeedChangedEventArgs> SpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:SpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="Share430.Client.Core.Args.SpeedChangedEventArgs"/> instance containing the event data.</param>
        public void OnSpeedChanged(SpeedChangedEventArgs args)
        {
            EventHandler<SpeedChangedEventArgs> handler = this.SpeedChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}