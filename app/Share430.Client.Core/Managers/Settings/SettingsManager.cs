using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Settings;
using Share430.Client.Core.Managers.Logging;
using Share430.Client.Core.Utils;

namespace Share430.Client.Core.Managers.Settings
{
    /// <summary>
    /// FileSystemFeature enum
    /// </summary>
    [Flags]
    public enum FileSystemFeature : uint
    {
        /// <summary>
        /// The file system supports case-sensitive file names.
        /// </summary>
        CaseSensitiveSearch = 1,

        /// <summary>
        /// The file system preserves the case of file names when it places a name on disk.
        /// </summary>
        CasePreservedNames = 2,

        /// <summary>
        /// The file system supports Unicode in file names as they appear on disk.
        /// </summary>
        UnicodeOnDisk = 4,

        /// <summary>
        /// The file system preserves and enforces access control lists (ACL).
        /// </summary>
        PersistentACLS = 8,

        /// <summary>
        /// The file system supports file-based compression.
        /// </summary>
        FileCompression = 0x10,

        /// <summary>
        /// The file system supports disk quotas.
        /// </summary>
        VolumeQuotas = 0x20,

        /// <summary>
        /// The file system supports sparse files.
        /// </summary>
        SupportsSparseFiles = 0x40,

        /// <summary>
        /// The file system supports re-parse points.
        /// </summary>
        SupportsReparsePoints = 0x80,

        /// <summary>
        /// The specified volume is a compressed volume, for example, a DoubleSpace volume.
        /// </summary>
        VolumeIsCompressed = 0x8000,

        /// <summary>
        /// The file system supports object identifiers.
        /// </summary>
        SupportsObjectIDs = 0x10000,

        /// <summary>
        /// The file system supports the Encrypted File System (EFS).
        /// </summary>
        SupportsEncryption = 0x20000,

        /// <summary>
        /// The file system supports named streams.
        /// </summary>
        NamedStreams = 0x40000,

        /// <summary>
        /// The specified volume is read-only.
        /// </summary>
        ReadOnlyVolume = 0x80000,

        /// <summary>
        /// The volume supports a single sequential write.
        /// </summary>
        SequentialWriteOnce = 0x100000,

        /// <summary>
        /// The volume supports transactions.
        /// </summary>
        SupportsTransactions = 0x200000,
    }

    /// <summary>
    /// Manages the Settings information data.
    /// </summary>
    public static class SettingsManager
    {
        #region Constants

        /// <summary>
        /// Init command for service
        /// </summary>
        public static readonly int SERVICE_COMMAND_INIT = 200;

        /// <summary>
        /// Notify after finish first run wizard
        /// </summary>
        public static readonly int SERVICE_COMMAND_FINISH = 201;

        public static readonly int SERVICE_COMMAND_STOP = 202;

        public static readonly int SERVICE_COMMAND_START_FOLDER = 203;

        /// <summary>
        /// The default part size (10 MB)
        /// </summary>
        public static readonly int DEFAULT_PART_SIZE = 10485760;

        /// <summary>
        /// The default backet name.
        /// </summary>
        public static readonly string DEFAULT_BACKET_NAME = "backupduty_test";

        /// <summary>
        /// The client file name.
        /// </summary>
		public static readonly string CLIENT_FILE_NAME = "Share430.app.exe";

        #endregion

        /// <summary>
        /// The name of the settings file.
        /// </summary>
        private const string SETTINGS_FILE_NAME = "Share430.cfg";

        /// <summary>
        /// The synchronization object.
        /// </summary>
        private static readonly object SyncRoot = new object();

        /// <summary>
        /// The encode/decode key
        /// </summary>
        private static string key;

        /// <summary>
        /// The encode/decode key
        /// </summary>
        public static string Key
        {
            get
            {
                if (string.IsNullOrEmpty(key))
                {
                    key = GetEncodeDecodeKey();
                }

                return key;
            }
        }

        /// <summary>
        /// The settings info object.
        /// </summary>
        private static SettingsInfo settings;

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <value>The settings.</value>
        public static SettingsInfo Settings
        {
            get
            {
                lock (SyncRoot)
                {
                    if (settings == null)
                    {
                        Initialize(CoreConstantsHelper.PathForSaveData);

                        if (settings == null)
                        {
                            settings = CreateDefaultSettings();
                            Save(CoreConstantsHelper.PathForSaveData, settings);
                            Initialize(CoreConstantsHelper.PathForSaveData);
                        }
                    }

                    return settings;
                }
            }
        }

        /// <summary>
        /// Initializes the specified application data path.
        /// </summary>
        /// <param name="applicationDataPath">The application data path.</param>
        private static void Initialize(string applicationDataPath)
        {
            LoggingManager.LogInfo("Settings manager: Initialize.");

            if (string.IsNullOrEmpty(applicationDataPath))
            {
                applicationDataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }

            string filePath = Path.Combine(applicationDataPath, SETTINGS_FILE_NAME);
            if (File.Exists(filePath))
            {
                using (Stream stream = File.OpenRead(filePath))
                {
                    IFormatter formatter = new BinaryFormatter();
                    try
                    {
                        settings = formatter.Deserialize(stream) as SettingsInfo;
                    }
                    catch (Exception ex)
                    {
                        LoggingManager.LogError("Settings manager: Exception " + ex);
                    }

                    if (settings == null)
                    {
                        LoggingManager.LogError("Settings manager: Settings not deserialized");
                    }

                    string decodedName = DecodeString(settings.Credentials.Name);
                    LoggingManager.LogInfo("Settings manager: Decoded name");
                    settings.Credentials.Name = decodedName;
                    string decodedToken = DecodeString(settings.Credentials.Token);
                    LoggingManager.LogInfo("Settings manager: Decoded token");
                    settings.Credentials.Token = decodedToken;
                }
            }
            else
            {
                LoggingManager.LogInfo("Settings manager: Settings not exists. Create default settings");
                settings = CreateDefaultSettings();
                Save(applicationDataPath, settings);
                Initialize(applicationDataPath);
            }
        }

        /// <summary>
        /// Saves the specified application data path.
        /// </summary>
        /// <param name="applicationDataPath">The application data path.</param>
        /// <param name="settingsInfo">The settings info.</param>
        public static void Save(string applicationDataPath, SettingsInfo settingsInfo)
        {
            lock (SyncRoot)
            {
                if (settings != null)
                {
                    settings = settingsInfo;

                    if (settings == null)
                    {
                        string fp = Path.Combine(applicationDataPath, SETTINGS_FILE_NAME);
                        if (File.Exists(fp))
                        {
                            File.Delete(fp);
                        }
                        return;
                    }

                    string encodedName = EncodeString(settingsInfo.Credentials.Name ?? string.Empty);
                    //Loger.Instance.Log("Settings manager: Encode name from '" + (settingsInfo.Credentials.Name ?? string.Empty) + "' to '" + encodedName + "'");
                    settingsInfo.Credentials.Name = encodedName;
                    string encodedToken = EncodeString(settingsInfo.Credentials.Token ?? string.Empty);
                    //Loger.Instance.Log("Settings manager: Encode password from '" + (settingsInfo.Credentials.Password ?? string.Empty) + "' to '" + encodedPassword + "'");
                    settingsInfo.Credentials.Token = encodedToken;

                    if (string.IsNullOrEmpty(applicationDataPath))
                    {
                        applicationDataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    }

                    string filePath = Path.Combine(applicationDataPath, SETTINGS_FILE_NAME);
                    using (Stream steam = File.Open(filePath, FileMode.OpenOrCreate))
                    {
                        IFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(steam, settingsInfo);
                    }

                    settings = null;
                }
                
            }
        }

        /// <summary>
        /// Create default settings
        /// </summary>
        /// <returns>The default settings</returns>
        private static SettingsInfo CreateDefaultSettings()
        {
            return new SettingsInfo
                       {
                           IsFirstRun = true,
                           LowPrioritySpeed = 100,
                           TargetFolderLocation = string.Empty,
                           ProxyServerPort = 3128
                       };
        }

        /// <summary>
        /// Encode string value
        /// </summary>
        /// <param name="str">The input string</param>
        /// <returns>Result encoding string</returns>
        private static string EncodeString(string str)
        {
            return System.Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(str), Key));
        }

        /// <summary>
        /// Decode string value
        /// </summary>
        /// <param name="str">The input string</param>
        /// <returns>Result decoding string</returns>
        private static string DecodeString(string str)
        {
            CryptoStream cs = InternalDecrypt(System.Convert.FromBase64String(str), Key);
            StreamReader sr = new StreamReader(cs);
            try
            {
                return sr.ReadToEnd();
            }
            catch
            {
                return str;
            }
        }

        /// <summary>
        /// Method for encrypt data
        /// </summary>
        /// <param name="data">The data for encription</param>
        /// <param name="password">The encription key</param>
        /// <returns>The encrypted data</returns>
        private static byte[] Encrypt(byte[] data, string password)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateEncryptor((new PasswordDeriveBytes(password, null)).GetBytes(16), new byte[16]);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);

            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();

            return ms.ToArray();
        }

        /// <summary>
        /// Internal decryption
        /// </summary>
        /// <param name="data">The decript data</param>
        /// <param name="password">The key for decription</param>
        /// <returns>Stream for decript</returns>
        private static CryptoStream InternalDecrypt(byte[] data, string password)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateDecryptor(
            (new PasswordDeriveBytes(password, null)).GetBytes(16),
            new byte[16]);
            MemoryStream ms = new MemoryStream(data);
            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        }

        /// <summary>
        /// GetVolumeInformation api method
        /// </summary>
        /// <param name="rootPathName">path to folder</param>
        /// <param name="volumeNameBuffer">use null for param</param>
        /// <param name="volumeNameSize">set zero for param</param>
        /// <param name="volumeSerialNumber">out param - serial number of volume</param>
        /// <param name="maximumComponentLength">out param - max length</param>
        /// <param name="fileSystemFlags">system flags</param>
        /// <param name="fileSystemNameBuffer">system buffer</param>
        /// <param name="fileSystemNameSize">system buffer size</param>
        /// <returns>return true or false</returns>
        [DllImport("Kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool GetVolumeInformation(
          string rootPathName,
          StringBuilder volumeNameBuffer,
          int volumeNameSize,
          out uint volumeSerialNumber,
          out uint maximumComponentLength,
          out FileSystemFeature fileSystemFlags,
          StringBuilder fileSystemNameBuffer,
          int fileSystemNameSize);

        /// <summary>
        /// Get encode/decode key
        /// </summary>
        /// <returns>encode/decode key</returns>
        private static string GetEncodeDecodeKey()
        {
            string result = string.Empty;

            uint volumeSerialNumber = 0;
            uint maxLength = 0;
            FileSystemFeature feature;
            if (GetVolumeInformation(Assembly.GetExecutingAssembly().Location.Substring(0, 3), null, 0, out volumeSerialNumber, out maxLength, out feature, null, 0))
            {
                result = volumeSerialNumber.ToString("X");
            }
            else
            {
                Loger.Instance.Log("Settings manager: GetEncodeDecodeKey is failed");
//                ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_LogicalDisk");
//
//                foreach (ManagementObject obj in searcher.Get())
//                {
//                    if (obj["DeviceID"].Equals(Assembly.GetExecutingAssembly().Location.Substring(0, 2)))
//                    {
//                        result = obj["VolumeSerialNumber"].ToString();
//                    }
//                }
            }

            Loger.Instance.Log("Settings manager: Encode/Decode key is '" + result + "'");
            return result;
        }
    }
}
