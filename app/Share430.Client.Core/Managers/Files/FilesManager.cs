using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Share430.Client.Core.Managers.Files
{
    /// <summary>
    /// The files manager.
    /// </summary>
    public static class FilesManager
    {
        private static object lockObject = new object();
        /// <summary>
        /// Gets the file hash code.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The calculated MD5 hash code.</returns>
        public static string GetFileHashCode(string filePath)
        {
            lock (lockObject)
            {
                string hashCode;

                using (FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    MD5 md5 = new MD5CryptoServiceProvider();
                    byte[] hash = md5.ComputeHash(file);
                    file.Close();

                    hashCode = Encoding.Default.GetString(hash);
                }

                return hashCode;
            }
        }
    }
}