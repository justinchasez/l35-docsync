using System;
using System.Globalization;

namespace Share430.Client.Core.Managers.Convert
{
    /// <summary>
    /// Data converter.
    /// </summary>
    public static class ConvertData
    {
        /// <summary>
        /// DateTime converter.
        /// </summary>
        public static class DateConverter
        {
            /// <summary>
            /// Converts the specified value.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <param name="format">The format.</param>
            /// <returns>The string represenataion with sufixes.</returns>
            public static string Convert(DateTime value, string format)
            {
                return Convert(value, format, CultureInfo.CurrentCulture);
            }

            /// <summary>
            /// Converts the specified value.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <param name="format">The format.</param>
            /// <param name="currentCulture">The current culture.</param>
            /// <returns>The string represenataion with sufixes.</returns>
            public static string Convert(DateTime value, string format, CultureInfo currentCulture)
            {
                return value.ToString(format, currentCulture);
            }
        }

        /// <summary>
        /// Information size converter.
        /// </summary>
        public static class InformationSizeConvertor
        {
            #region Size constants

            /// <summary>
            /// The bytes sufix.
            /// </summary>
            private const string BYTES = "Bytes";

            /// <summary>
            /// The kilobytes sufix.
            /// </summary>
            private const string KILOBYTES = "KB";

            /// <summary>
            /// The megabytes sufix.
            /// </summary>
            private const string MEGABYTES = "MB";

            /// <summary>
            /// The gigabytes sufix.
            /// </summary>
            private const string GIGABYTES = "GB";

            #endregion

            /// <summary>
            /// Converts the specified value.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns>The information size string.</returns>
            public static string Convert(long value)
            {
                if (value >= 1073741824)
                {
                    return String.Format("{0:F2} {1}", value / 1073741824d, GIGABYTES);
                }

                if (value >= 1048576)
                {
                    return String.Format("{0:F2} {1}", value / 1048576d, MEGABYTES);
                }

                if (value >= 1024)
                {
                    return String.Format("{0:F2} {1}", value / 1024d, KILOBYTES);
                }

                return String.Format("{0:F2} {1}", value, BYTES);
            }

            /// <summary>
            /// Speeds the convert.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns>The information speed string</returns>
            public static string SpeedConvert(long value)
            {
                return String.Format("{0}/s", Convert(value));
            }
        }

        /// <summary>
        /// Frequency size converter.
        /// </summary>
        public static class FrequencySizeConverter
        {
            #region Size constants

            /// <summary>
            /// The herz sufix.
            /// </summary>
            private const string HERZ = "Hz";

            /// <summary>
            /// The kiloherz sufix.
            /// </summary>
            private const string KILOHERZ = "KHz";

            /// <summary>
            /// The megaherz sufix.
            /// </summary>
            private const string MEGAHERZ = "MHz";

            /// <summary>
            /// The gigaherz sufix.
            /// </summary>
            private const string GIGAHERZ = "GHz";

            #endregion

            /// <summary>
            /// Converts the specified value.
            /// </summary>
            /// <param name="value">The value.</param>
            /// <returns>The frequency size string</returns>
            public static string Convert(long value)
            {
                if (value >= 1000000000)
                {
                    return String.Format("{0:F2} {1}", value / 1000000000d, GIGAHERZ);
                }

                if (value >= 1000000)
                {
                    return String.Format("{0:F2} {1}", value / 1000000d, MEGAHERZ);
                }

                if (value >= 1000)
                {
                    return String.Format("{0:F2} {1}", value / 1000d, KILOHERZ);
                }

                return String.Format("{0:F2} {1}", value, HERZ);
            }
        }
    }
}
