﻿using System;
using log4net;

namespace Share430.Client.Core.Managers.Logging
{
    /// <summary>
    /// This class is used for logging.
    /// </summary>
    public static class LoggingManager
    {
        /// <summary>
        /// Separator in log
        /// </summary>
        public const string Separator = "----------------------------------------------------------------------------------";

        /// <summary>
        /// extension for file, name will be assembly name
        /// </summary>
        public const string LOG_FILE_EXTENTION = ".log";

        /// <summary>
        /// Logging File key
        /// </summary>
        public const string LOGING_FILE_KEY = "logfilename";

        /// <summary>
        /// Logger key
        /// </summary>
        private const string LOGGER_KEY = "Share430.Logging";

        /// <summary>
        /// Log instance
        /// </summary>
        private static ILog log;

        /// <summary>
        /// Gets the ILog object for logging.
        /// </summary>
        private static ILog Log
        {
            get
            {
                if (log == null)
                {
                    log4net.Config.XmlConfigurator.Configure();
                    log = LogManager.GetLogger(LOGGER_KEY);
                }

                return log;
            }
        }

        /// <summary>
        /// Logs an error message entry.
        /// </summary>
        /// <param name="message">Error message.</param>
        public static void LogError(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                Log.Error(message);
            }
        }

        /// <summary>
        /// Logs an error message entry from exception.
        /// </summary>
        /// <param name="ex">An exception.</param>
        public static void LogError(Exception ex)
        {
            if (ex != null)
            {
                Log.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Logs debug message entry.
        /// </summary>
        /// <param name="message">Debug message.</param>
        public static void LogDebug(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                Log.Debug(message);
            }
        }

        /// <summary>
        /// Logs debug message entry from exception.
        /// </summary>
        /// <param name="ex">An exception.</param>
        public static void LogDebug(Exception ex)
        {
            if (ex != null)
            {
                Log.Debug(ex.ToString());
            }
        }

        /// <summary>
        /// Logs information message entry.
        /// </summary>
        /// <param name="message">The information message.</param>
        public static void LogInfo(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                Log.Info(message);
            }
        }
    }
}

