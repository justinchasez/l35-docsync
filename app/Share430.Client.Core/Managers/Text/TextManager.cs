using System.Security.Cryptography;
using System.Text;

namespace Share430.Client.Core.Managers.Text
{
    /// <summary>
    /// The text manager.
    /// </summary>
    public static class TextManager
    {
        /// <summary>
        /// The hash provider.
        /// </summary>
        private static MD5CryptoServiceProvider hashProvider;

        /// <summary>
        /// Computes the hash.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <returns>The base 64 string with hash.</returns>
        public static string ComputeHash(string content)
        {
            if (hashProvider == null)
            {
                hashProvider = new MD5CryptoServiceProvider();
            }

            return System.Convert.ToBase64String(hashProvider.ComputeHash(Encoding.Default.GetBytes(content)));
        }
    }
}