﻿namespace Share430.Client.Core.Utils
{
    /// <summary>
    /// The string utils class
    /// </summary>
    public static class StringUtils
    {
        /// <summary>
        /// Max file name length
        /// </summary>
        private const int MAX_FILE_NAME_LENGTH = 60;

        /// <summary>
        /// Get Short File Name
        /// </summary>
        /// <param name="fileName">The name of file</param>
        /// <returns>Short file name</returns>
        public static string GetShortFileName(string fileName)
        {
            string result;

            if (fileName.Length < MAX_FILE_NAME_LENGTH)
            {
                result = fileName;
            }
            else
            {
                string[] names = fileName.Split('\\');

                if (names.Length <= 2)
                {
                    result = fileName;
                }
                else
                {
                    result = string.Format("{0}\\...\\{1}", names[0], names[names.Length - 1]);

                    for (int i = 1; i < names.Length - 1; i++)
                    {
                        if ((result.Length + names[i].Length) < MAX_FILE_NAME_LENGTH)
                        {
                            result = result.Replace("...", string.Concat(names[i], "\\..."));
                        }
                        else
                        {
                            i = names.Length;
                        }
                    }
                }
            }

            return result;
        }
    }
}
