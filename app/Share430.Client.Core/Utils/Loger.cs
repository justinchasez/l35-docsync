﻿using System.IO;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Managers.Logging;

namespace Share430.Client.Core.Utils
{
    /// <summary>
    /// The loger class.
    /// </summary>
    public class Loger
    {
        /// <summary>
        /// Path to log file
        /// </summary>
        private readonly string filePath;

        /// <summary>
        /// The loger instance
        /// </summary>
        private static Loger instance;

        /// <summary>
        /// The loger instance
        /// </summary>
        public static Loger Instance
        {
            get { return instance ?? (instance = new Loger()); }
        }

        /// <summary>
        /// Prevents a default instance of the Loger class from being created.
        /// </summary>
        private Loger()
        {
            this.filePath = Path.Combine(CoreConstantsHelper.PathForSaveData, "Log.txt");
        }

        /// <summary>
        /// Log string to file
        /// </summary>
        /// <param name="str">The string for loging</param>
        public void Log(string str)
        {
            LoggingManager.LogError(str);
//            try
//            {
//                using (FileStream fs = File.Open(this.filePath, FileMode.Append, FileAccess.Write))
//                {
//                    using (StreamWriter writer = new StreamWriter(fs))
//                    {
//                        writer.WriteLine(string.Format("[{0}]  {1}", DateTime.Now, str));
//                    }
//                }
//            }
//            catch (Exception)
//            {
//            }
        }
    }
}
