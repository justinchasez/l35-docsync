﻿using System;
using System.Collections.Generic;
using System.Management;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Utils
{
    /// <summary>
    /// The Disk Util class
    /// </summary>
    public static class DiskUtil
    {
        /// <summary>
        /// Types of disks
        /// </summary>
        private static Dictionary<string, uint> disksTypes;

        /// <summary>
        /// the sync object
        /// </summary>
        private static object syncObject = new object();

        /// <summary>
        /// Types of disks
        /// </summary>
        public static Dictionary<string, uint> DisksTypes
        {
            get
            {
                lock (syncObject)
                {
                    if (disksTypes == null)
                    {
                        try
                        {
                            Dictionary<string, uint> disks = new Dictionary<string, uint>();

                            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_LogicalDisk");

                            foreach (ManagementObject obj in searcher.Get())
                            {
                                try
                                {
                                    disks.Add(obj["DeviceID"].ToString(), (uint) obj["DriveType"]);
                                }
                                catch (ArgumentException)
                                {
                                }
                            }

                            foreach (KeyValuePair<string, uint> disk in disks)
                            {
                                Loger.Instance.Log("Disk utill: Disk " + disk.Key + " typeof " + disk.Value);
                            }

                            disksTypes = disks;
                        }
                        catch (Exception ex)
                        {
                            disksTypes = null;
                            Loger.Instance.Log("Disk utill: Exception : " + ex);
                        }
                    }
                }

                return disksTypes;
            }
        }

        /// <summary>
        /// Check if the type of the disk from specify path is supported
        /// </summary>
        /// <param name="path">path to file or folder</param>
        /// <param name="type">type of disk to check</param>
        /// <returns>return true if type is correct</returns>
        public static bool CheckDiskType(string path, DiskTypes type)
        {
            if (DisksTypes != null)
            {
                return DisksTypes[path.Split('\\')[0]] == (uint) type;
            }

            return false;
        }
    }
}
