namespace Share430.Client.Core.Utils
{
    /// <summary>
    /// The utilites for URLs.
    /// </summary>
    public static class UrlUtil
    {
        /// <summary>
        /// Combines the specified uri.
        /// </summary>
        /// <param name="uri1">The first url.</param>
        /// <param name="uri2">The second uril.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns>Combined url.</returns>
        public static string Combine(string uri1, string uri2, string userName)
        {
            uri1 = uri1.TrimEnd('/');
            uri2 = uri2.TrimStart('/');

            return string.Format("{0}/Account/LogOn?email={2}&returnUrl={0}/{1}", uri1, uri2, userName);
        }
    }
}
