using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Share430.Client.Core.Collections
{
    /// <summary>
    /// The files queue
    /// </summary>
    /// <typeparam name="T">The collection items type</typeparam>
    public class BackupFileQueue<T> : BaseFileQueue<T> where T : FileQueueItem 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackupFileQueue&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="dataFilePath">The data file path.</param>
        public BackupFileQueue(string dataFilePath)
            : base(dataFilePath)
        {
        }

        /// <summary>
        /// The synchronization object.
        /// </summary>
        private static readonly object SyncObject = new object();

        /// <summary>
        /// The files for rename collection
        /// </summary>
        private Dictionary<string, string> filesForRename;

        #region Public interface

        /// <summary>
        /// Gets the total size.
        /// </summary>
        /// <value>The total size.</value>
        public override long Size
        {
            get
            {
                var pathes =
                    this.Files
                        .Select(f => f.Value.IsUpload ? f.Value.FilePath : f.Value.IsRename ? f.Value.RenamedFilePath : null)
                        .ToList()
                        .Where(f => f != null)
                        .Where(File.Exists)
                        .Select(f => new FileInfo(f).Length)
                        .ToList();

                if (pathes.Any())
                {
                    return pathes.Sum();
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the size of the completed.
        /// </summary>
        /// <value>The size of the completed.</value>
        public override long CompletedSize
        {
            get
            {
                long totalSize = 0;

                foreach (KeyValuePair<string, T> item in this.Files)
                {
                    //if (item.Value.Completed && item.Value.File.Exists)
                    if (item.Value.Completed)
                    {
                        if (item.Value.IsUpload && File.Exists(item.Value.FilePath))
                        {
                            totalSize += new FileInfo(item.Value.FilePath).Length;
                        }
                        else if (item.Value.IsRename && File.Exists(item.Value.RenamedFilePath))
                        {
                            totalSize += new FileInfo(item.Value.RenamedFilePath).Length;
                        }
                    }
                }

                return totalSize;
            }
        }

        /// <summary>
        /// Set file force flag in queue
        /// </summary>
        /// <param name="fullPath">full path to the file</param>
        /// <param name="force">set true for back up this file first</param>
        public override void SetFileForce(string fullPath, bool force)
        {
            lock (SyncObject)
            {
                if (this.Files.ContainsKey(fullPath))
                {
                    this.Files[fullPath].Force = force;
                }
                else if (this.filesForRename.ContainsValue(fullPath))
                {
                    foreach (KeyValuePair<string, string> renFile in this.filesForRename)
                    {
                        if (renFile.Value.Equals(fullPath))
                        {
                            this.Files[renFile.Key].Force = force;
                            break;
                        }
                    }
                }
            }

            this.Save();
        }
        
        /// <summary>
        /// Removes the file from queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public override void RemoveFileFromQueue(string filePath)
        {
            lock (SyncObject)
            {
                if (this.Files.ContainsKey(filePath))
                {
                    this.Files.Remove(filePath);
                    if (this.filesForRename.ContainsKey(filePath))
                    {
                        this.filesForRename.Remove(filePath);
                    }
                }
            }

            this.Save();
        }

        /// <summary>
        /// Remove file collection from queue
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        public override void RemoveFileCollectionFromQueue(string[] filePathes)
        {
            lock (SyncObject)
            {
                foreach (string filePath in filePathes)
                {
                    if (this.Files.ContainsKey(filePath))
                    {
                        this.Files.Remove(filePath);
                        if (this.filesForRename.ContainsKey(filePath))
                        {
                            this.filesForRename.Remove(filePath);
                        }
                    }
                }
            }

            this.Save();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Remove unexist files
        /// </summary>
        private void CleanFiles()
        {
            List<string> filesForRemove = new List<string>();
            List<string> renamedFilesForRemove = new List<string>();

            foreach (string oldFilePath in this.Files.Keys)
            {
                try
                {
                    if ((File.GetAttributes(oldFilePath) & FileAttributes.Hidden) == FileAttributes.Hidden)
                    {
                        filesForRemove.Add(oldFilePath);
                        if (this.filesForRename.ContainsKey(oldFilePath))
                        {
                            renamedFilesForRemove.Add(oldFilePath);
                        }
                    }
                    else
                    {
                        FileQueueItem curItem = this.Files[oldFilePath];
                        if (curItem.IsRename && !File.Exists(curItem.RenamedFilePath))
                        {
                            curItem.IsRename = false;
                            curItem.RenamedFilePath = null;
                            renamedFilesForRemove.Add(oldFilePath);
                        }
                    }
                }
                catch (IOException)
                {
                    if (this.filesForRename.ContainsKey(oldFilePath))
                    {
                        string newFilePath = this.filesForRename[oldFilePath];
                        if (this.Files.ContainsKey(newFilePath))
                        {
                            filesForRemove.Add(oldFilePath);
                            renamedFilesForRemove.Add(oldFilePath);
                        }
                        else
                        {
                            try
                            {
                                if ((File.GetAttributes(newFilePath) & FileAttributes.Hidden) == FileAttributes.Hidden)
                                {
                                    filesForRemove.Add(oldFilePath);
                                    renamedFilesForRemove.Add(oldFilePath);
                                }
                            }
                            catch (IOException)
                            {
                                filesForRemove.Add(oldFilePath);
                                renamedFilesForRemove.Add(oldFilePath);
                            }
                        }
                    }
                    else
                    {
                        filesForRemove.Add(oldFilePath);
                    }
                }
            }

            lock (SyncObject)
            {
                foreach (string key in filesForRemove)
                {
                    this.Files.Remove(key);
                }

                foreach (string key in renamedFilesForRemove)
                {
                    this.filesForRename.Remove(key);
                }
            }
        }

        /// <summary>
        /// Add item to queue
        /// </summary>
        /// <param name="item">Item for add</param>
        protected override void AddItemToQueue(T item)
        {
            lock (SyncObject)
            {
                if (item.IsUpload && !item.IsRename)
                {
                    if (this.Files.ContainsKey(item.FilePath))
                    {
                        //this.Files[item.FilePath] = item;
                        this.Files[item.FilePath].IsUpload = true;
                        this.Files[item.FilePath].Completed = false;
                    }
                    else if (this.filesForRename.ContainsValue(item.FilePath))
                    {
                        string currentKey = this.filesForRename.Keys.First(key => this.filesForRename[key].Equals(item.FilePath));

                        this.Files[currentKey].IsUpload = true;
                        this.Files[currentKey].Completed = false;
                    }
                    else
                    {
                        this.Files.Add(item.FilePath, item);
                    }
                }
                else
                {
                    if (this.filesForRename.ContainsValue(item.FilePath))
                    {
                        foreach (KeyValuePair<string, string> renameItem in this.filesForRename)
                        {
                            if (renameItem.Value.Equals(item.FilePath))
                            {
                                this.filesForRename[renameItem.Key] = item.RenamedFilePath;
                                this.Files[renameItem.Key].RenamedFilePath = item.RenamedFilePath;
                                break;
                            }
                        }
                    }
                    else if (this.Files.ContainsKey(item.FilePath))
                    {
                        this.Files[item.FilePath].RenamedFilePath = item.RenamedFilePath;
                        this.Files[item.FilePath].IsRename = true;
                        this.Files[item.FilePath].Completed = false;

                        this.filesForRename.Add(item.FilePath, item.RenamedFilePath);
                    }
                    else
                    {
                        this.Files.Add(item.FilePath, item);
                        this.filesForRename.Add(item.FilePath, item.RenamedFilePath);
                    }
                }
            }
        }

        /// <summary>
        /// Initializes the data.
        /// </summary>
        protected override void Initialize()
        {
            if (!String.IsNullOrEmpty(this.DataFilePath) && File.Exists(this.DataFilePath))
            {
                lock (SyncObject)
                {
                    using (Stream steam = File.OpenRead(this.DataFilePath))
                    {
                        IFormatter formatter = new BinaryFormatter();
                        this.Files = formatter.Deserialize(steam) as Dictionary<string, T>;
                    }
                }
            }

            if (this.Files == null)
            {
                this.Files = new Dictionary<string, T>();
            }

            if (this.filesForRename == null)
            {
                this.filesForRename = new Dictionary<string, string>();
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        protected override void Save()
        {
            this.CleanFiles();
            // notify that state of queue was changed, method change calls after any changes with queue
            this.RaiseStateChanged();

            if (!String.IsNullOrEmpty(this.DataFilePath))
            {
                bool notCompletedExist = false;
                foreach (T item in this.Files.Values)
                {
                    if (!item.Completed)
                    {
                        notCompletedExist = true;
                        break;
                    }
                }

                if (notCompletedExist)
                {
                    lock (SyncObject)
                    {
                        using (Stream steam = File.Open(this.DataFilePath, FileMode.OpenOrCreate))
                        {
                            IFormatter formatter = new BinaryFormatter();
                            formatter.Serialize(steam, this.Files);
                        }
                    }
                }
                else if (File.Exists(this.DataFilePath))
                {
                    this.Files.Clear();
                    this.filesForRename.Clear();
                    File.Delete(this.DataFilePath);
                }
            }
        }

        #endregion
    }
}