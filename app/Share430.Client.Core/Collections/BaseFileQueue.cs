using System;
using System.Collections.Generic;
using System.Linq;

namespace Share430.Client.Core.Collections
{
    /// <summary>
    /// The files queue
    /// </summary>
    /// <typeparam name="T">The collection items type</typeparam>
    public abstract class BaseFileQueue<T> where T : FileQueueItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseFileQueue&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="dataFilePath">The data file path.</param>
        protected BaseFileQueue(string dataFilePath)
        {
            if (String.IsNullOrEmpty(dataFilePath))
            {
                throw new ArgumentNullException("dataFilePath");
            }

            this.DataFilePath = dataFilePath;

            this.Initialize();
        }

        #region Fields
        
        /// <summary>
        /// Occurs when state of queue was changed
        /// </summary>
        public event EventHandler StateChanged;

        /// <summary>
        /// The data file path.
        /// </summary>
        protected readonly string DataFilePath;

        /// <summary>
        /// The files collection.
        /// </summary>
        private Dictionary<string, T> files;

        /// <summary>
        /// Gets or sets the files.
        /// </summary>
        /// <value>The files.</value>
        protected Dictionary<string, T> Files
        {
            get
            {
                return this.files;
            }

            set
            {
                this.files = value;
            }
        }

        #endregion

        #region Public interface

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The items count.</value>
        public int Count
        {
            get { return this.Files.Count; }
        }

        /// <summary>
        /// Gets the total size.
        /// </summary>
        /// <value>The total size.</value>
        public abstract long Size { get; }

        /// <summary>
        /// Gets the completed items count.
        /// </summary>
        /// <value>The completed items count.</value>
        public int CompletedCount
        {
            get
            {
                return this.Files.Values.ToList().Count(item => item.Completed);
            }
        }

        /// <summary>
        /// Gets the size of the completed.
        /// </summary>
        /// <value>The size of the completed.</value>
        public abstract long CompletedSize { get; }

        /// <summary>
        /// Gets the progress.
        /// </summary>
        /// <value>The progress.</value>
        public int Progress
        {
            get
            {
                if (this.Files.Count == 0 || this.Size == 0)
                {
                    return 0;
                }

                return (int)((100 * this.CompletedSize) / this.Size);
            }
        }

        /// <summary>
        /// Adds the file to queue.
        /// </summary>
        /// <param name="item">The queue item.</param>
        public void AddFileToQueue(T item)
        {
            //this.files.Add(item.File.FullName, item);
            this.AddItemToQueue(item);

            this.Save();
        }

        /// <summary>
        /// Add file collection to queue
        /// </summary>
        /// <param name="items">The file collection</param>
        public void AddFileCollectionToQueue(IList<T> items)
        {
            foreach (T item in items)
            {
                this.AddItemToQueue(item);
            }

            this.Save();
        }

        /// <summary>
        /// Set file force flag in queue
        /// </summary>
        /// <param name="fullPath">full path to the file</param>
        /// <param name="force">set true for back up this file first</param>
        public abstract void SetFileForce(string fullPath, bool force);

         /// <summary>
         /// Gets the specify file from queue.
         /// </summary>
         /// <param name="fullPath">full path to the file</param>
         /// <returns>return file info if exist, else null</returns>
        public T GetFileFromQueue(string fullPath)
        {
            if (this.Files.ContainsKey(fullPath))
            {
                return this.Files[fullPath];
            }

            return null;
        }

        /// <summary>
        /// Gets the file from queue.
        /// </summary>
        /// <returns>The queue file info.</returns>
        public T GetFileFromQueue()
        {
            T findItem = null;

            foreach (T item in this.Files.Values.ToList())
            {
                if (!item.Completed && item.CompareTo(findItem) > 0)
                {
                    findItem = item;
                }
            }

            return findItem;
        }

        /// <summary>
        /// Gets all files.
        /// </summary>
        /// <returns>All files list</returns>
        public Dictionary<string, T> GetAllFiles()
        {
            return this.Files;
        }

        /// <summary>
        /// Sets the file complete.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="state">if set to <c>true</c> [state].</param>
        public void SetFileState(string filePath, bool state)
        {
            if (this.Files.ContainsKey(filePath))
            {
                T item = this.Files[filePath];
                item.Completed = state;
                this.Save();
            }
        }

        /// <summary>
        /// Removes the file from queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public abstract void RemoveFileFromQueue(string filePath);

        /// <summary>
        /// Remove file collection from queue
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        public abstract void RemoveFileCollectionFromQueue(string[] filePathes);

        #endregion

        #region Private Methods

        /// <summary>
        /// Raise state changed event
        /// </summary>
        protected void RaiseStateChanged()
        {
            if (this.StateChanged != null)
            {
                this.StateChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// Add item to queue
        /// </summary>
        /// <param name="item">Item for add</param>
        protected abstract void AddItemToQueue(T item);

        /// <summary>
        /// Initializes the data.
        /// </summary>
        protected abstract void Initialize();

        /// <summary>
        /// Saves this instance.
        /// </summary>
        protected abstract void Save();

        #endregion
    }
}