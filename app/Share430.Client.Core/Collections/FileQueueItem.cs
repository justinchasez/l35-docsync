using System;
using System.Runtime.Serialization;

namespace Share430.Client.Core.Collections
{
    /// <summary>
    /// The file queue item.
    /// </summary>
    [Serializable]
    public class FileQueueItem : IComparable<FileQueueItem>, IDeserializationCallback
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileQueueItem"/> class.
        /// </summary>
        public FileQueueItem()
        {
        }

        /// <summary>
        /// File full path
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Get or set renamed file
        /// </summary>
        public string RenamedFilePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is locked.
        /// </summary>
        /// <value><c>true</c> if this instance is locked; otherwise, <c>false</c>.</value>
        public bool IsLocked { get; set; }
        
        /// <summary>
        /// Get or set is upload bool value
        /// </summary>
        public bool IsUpload { get; set; }

        /// <summary>
        /// Get or set is rename bool value
        /// </summary>
        public bool IsRename { get; set; }

        /// <summary>
        /// Gets or sets the progress.
        /// </summary>
        /// <value>The progress.</value>
        public int Progress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileQueueItem"/> is completed.
        /// </summary>
        /// <value><c>true</c> if completed; otherwise, <c>false</c>.</value>
        public bool Completed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileQueueItem"/> is force.
        /// </summary>
        /// <value><c>true</c> if force; otherwise, <c>false</c>.</value>
        public bool Force { get; set; }

        /// <summary>
        /// Gets or sets failed count times
        /// </summary>
        public int FailedCount { get; set; }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            //return this.File.FullName.GetHashCode();
            return this.FilePath.GetHashCode();
        }

        /// <summary>
        /// On Deserialization event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        public void OnDeserialization(object sender)
        {
            this.FailedCount = 0;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            FileQueueItem item = obj as FileQueueItem;
            if (item != null)
            {
                return this.FilePath.Equals(item.FilePath);
            }

            return false;
        }

        /// <summary>
        /// Compare with other item
        /// </summary>
        /// <param name="other">Other FileQueueItem</param>
        /// <returns>Value of compare</returns>
        public virtual int CompareTo(FileQueueItem other)
        {
            if (other == null)
            {
                return 1;
            }

            if (other.FailedCount > this.FailedCount)
            {
                return 1;
            }

            if (this.IsLocked != other.IsLocked)
            {
                if (other.IsLocked)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (other.Force == this.Force)
            {
                return 0;
            }
            else if (this.Force)
            {
                return 1;
            }

            return -1;
        }
    }
}