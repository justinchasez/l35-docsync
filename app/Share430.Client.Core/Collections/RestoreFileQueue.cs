using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Share430.Client.Core.Collections
{
    /// <summary>
    /// The files queue
    /// </summary>
    /// <typeparam name="T">The collection items type</typeparam>
    public class RestoreFileQueue<T> : BaseFileQueue<T> where T : RestoreFileQueueItem 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreFileQueue&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="dataFilePath">The data file path.</param>
        public RestoreFileQueue(string dataFilePath)
            : base(dataFilePath)
        {
        }

        /// <summary>
        /// The synchronization object.
        /// </summary>
        private static readonly object SyncObject = new object();

        #region Public interface

        /// <summary>
        /// Gets the total size.
        /// </summary>
        /// <value>The total size.</value>
        public override long Size
        {
            get
            {
                long totalSize = 0;

                foreach (KeyValuePair<string, T> item in this.Files)
                {
                    totalSize += item.Value.Size;
                }

                return totalSize;
            }
        }

        /// <summary>
        /// Gets the size of the completed.
        /// </summary>
        /// <value>The size of the completed.</value>
        public override long CompletedSize
        {
            get
            {
                long totalSize = 0;

                foreach (KeyValuePair<string, T> item in this.Files)
                {
                    if (item.Value.Completed)
                    {
                        totalSize += item.Value.Size;
                    }
                }

                return totalSize;
            }
        }

        /// <summary>
        /// Set file force flag in queue
        /// </summary>
        /// <param name="fullPath">full path to the file</param>
        /// <param name="force">set true for back up this file first</param>
        public override void SetFileForce(string fullPath, bool force)
        {
            lock (SyncObject)
            {
                if (this.Files.ContainsKey(fullPath))
                {
                    this.Files[fullPath].Force = force;
                }
            }

            this.Save();
        }

        /// <summary>
        /// Removes the file from queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public override void RemoveFileFromQueue(string filePath)
        {
            lock (SyncObject)
            {
                if (this.Files.ContainsKey(filePath))
                {
                    this.Files.Remove(filePath);
                }
            }

            this.Save();
        }

        /// <summary>
        /// Remove file collection from queue
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        public override void RemoveFileCollectionFromQueue(string[] filePathes)
        {
            lock (SyncObject)
            {
                foreach (string filePath in filePathes)
                {
                    if (this.Files.ContainsKey(filePath))
                    {
                        this.Files.Remove(filePath);
                    }
                }
            }

            this.Save();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Add item to queue
        /// </summary>
        /// <param name="item">Item for add</param>
        protected override void AddItemToQueue(T item)
        {
            lock (SyncObject)
            {
                if (this.Files.ContainsKey(item.FilePath))
                {
                    this.Files[item.FilePath] = item;
                    this.Files[item.FilePath].IsUpload = true;
                    this.Files[item.FilePath].Completed = false;
                }
                else
                {
                    this.Files.Add(item.FilePath, item);
                }
            }
        }

        /// <summary>
        /// Initializes the data.
        /// </summary>
        protected override void Initialize()
        {
            if (!String.IsNullOrEmpty(this.DataFilePath) && File.Exists(this.DataFilePath))
            {
                lock (SyncObject)
                {
                    using (Stream steam = File.OpenRead(this.DataFilePath))
                    {
                        IFormatter formatter = new BinaryFormatter();
                        this.Files = formatter.Deserialize(steam) as Dictionary<string, T>;
                    }
                }
            }

            if (this.Files == null)
            {
                this.Files = new Dictionary<string, T>();
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        protected override void Save()
        {
            // notify that state of queue was changed, method change calls after any changes with queue
            this.RaiseStateChanged();

            if (!String.IsNullOrEmpty(this.DataFilePath))
            {
                bool notCompletedExist = false;
                foreach (T item in this.Files.Values)
                {
                    if (!item.Completed)
                    {
                        notCompletedExist = true;
                        break;
                    }
                }

                if (notCompletedExist)
                {
                    lock (SyncObject)
                    {
                        using (Stream steam = File.Open(this.DataFilePath, FileMode.OpenOrCreate))
                        {
                            IFormatter formatter = new BinaryFormatter();
                            formatter.Serialize(steam, this.Files);
                        }
                    }
                }
                else
                {
                    this.ClearIfComplete();
                }
            }
        }

        /// <summary>
        /// Clears if complete.
        /// </summary>
        private void ClearIfComplete()
        {
            bool notCompletedExist = false;
            foreach (T item in this.Files.Values)
            {
                if (!item.Completed)
                {
                    notCompletedExist = true;
                    break;
                }
            }

            if (!notCompletedExist)
            {
                this.Files.Clear();

                if (File.Exists(this.DataFilePath))
                {
                    File.Delete(this.DataFilePath);
                }
            }
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        public void SaveChanges()
        {
            this.Save();
        }

        #endregion
    }
}