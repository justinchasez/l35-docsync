using System;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Collections
{
    /// <summary>
    /// The restore queue item.
    /// </summary>
    [Serializable]
    public class RestoreFileQueueItem : FileQueueItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreFileQueueItem"/> class.
        /// </summary>
        public RestoreFileQueueItem()
        {
            this.State = ErrorState.NoErrors;
            this.Priority = RestorePriority.Normal;
        }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the restore file path.
        /// </summary>
        /// <value>The restore file path.</value>
        public string RestoreFilePath { get; set; }

        /// <summary>
        /// Gets or sets the restore file version.
        /// </summary>
        /// <value>The restore file version.</value>
        public int RestoreFileVersion { get; set; }

        /// <summary>
        /// Gets or sets the encrypt key.
        /// </summary>
        /// <value>The encrypt key.</value>
        public EncryptInfo EncryptKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [in progress].
        /// </summary>
        /// <value><c>true</c> if [in progress]; otherwise, <c>false</c>.</value>
        public bool InProgress { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>
        public RestorePriority Priority { get; set; }

        /// <summary>
        /// Gets or sets the last restored.
        /// </summary>
        /// <value>The last restored.</value>
        public DateTime LastRestored { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The error state.</value>
        public ErrorState State { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The file size.</value>
        public long Size { get; set; }
        
        /// <summary>
        /// Compare with other item
        /// </summary>
        /// <param name="other">Other FileQueueItem</param>
        /// <returns>Value of compare</returns>
        public override int CompareTo(FileQueueItem other)
        {
            var otherRestoreFileQueueItem = other as RestoreFileQueueItem;
            if (otherRestoreFileQueueItem == null)
            {
                return 1;
            }

            if (otherRestoreFileQueueItem.FailedCount > this.FailedCount)
            {
                return 1;
            }

            if (this.IsLocked != otherRestoreFileQueueItem.IsLocked)
            {
                if (otherRestoreFileQueueItem.IsLocked)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            if (otherRestoreFileQueueItem.Priority == this.Priority)
            {
                return 0;
            }
            else if (this.Priority > otherRestoreFileQueueItem.Priority)
            {
                return 1;
            }

            return -1;
        }

        /// <summary>
        /// Gets the state message.
        /// </summary>
        /// <value>The state message.</value>
        public string StateMessage
        {
            get
            {
                switch (this.State)
                {
                    case ErrorState.AnotherError:
                        return "Restore encountered an error and did not restore all files";
                    case ErrorState.RestoreCanceled:
                        return "Restore was cancelled";
                    case ErrorState.NoErrors:
                        if (this.Completed)
                        {
                            return "Restore completed successfully";
                        }
                        else
                        {
                            if (this.InProgress)
                            {
                                return "Restore is currently in progress";
                            }
                            else
                            {
                                return "Restore is queued for processing";
                            }
                        }

                    default:
                        return String.Empty;
                }
            }
        }
    }
}