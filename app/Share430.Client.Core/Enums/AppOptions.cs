namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The option which can be set on option tab in app
    /// </summary>
    public enum AppOptions
    {
        /// <summary>
        /// Recover Mode Option
        /// </summary>
        RecoverMode,

        /// <summary>
        /// Low Priority Option
        /// </summary>
        LowPriority,

        TargetLocation,

        /// <summary>
        /// Use proxy server option
        /// </summary>
        ProxyServer
    }
}