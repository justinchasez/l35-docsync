﻿namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The path types
    /// </summary>
    public enum PathType
    {
        /// <summary>
        /// The path is folder
        /// </summary>
        Folder,

        /// <summary>
        /// The path is file
        /// </summary>
        File,

        /// <summary>
        /// This path not found
        /// </summary>
        NotFound
    }
}