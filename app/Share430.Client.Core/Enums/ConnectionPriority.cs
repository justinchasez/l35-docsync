namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// Internet connection priority
    /// </summary>
    public enum ConnectionPriority
    {
        /// <summary>
        /// The high priority
        /// </summary>
        High,

        /// <summary>
        /// The normal priority
        /// </summary>
        Normal,

        /// <summary>
        /// The low priority
        /// </summary>
        Low
    }
}
