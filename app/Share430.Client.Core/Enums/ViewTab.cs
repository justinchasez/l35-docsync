namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The panes in app.
    /// </summary>
    public enum ViewTab
    {
        /// <summary>
        /// The status pane
        /// </summary>
        Status,

        /// <summary>
        /// The settings pane
        /// </summary>
        Options,

        /// <summary>
        /// The restore files pane
        /// </summary>
        RestoreFiles,

        /// <summary>
        /// The support pane
        /// </summary>
        Support,

        /// <summary>
        /// The seatch pane
        /// </summary>
        Search,

        /// <summary>
        /// The advanced sheduling pane
        /// </summary>
        AdvancedScheduling,

        /// <summary>
        /// The rules editor pane
        /// </summary>
        RulesEditor
    }
}