namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The error state
    /// </summary>
    public enum ErrorState
    {
        /// <summary>
        /// Pending restore
        /// </summary>
        NoErrors,

        /// <summary>
        /// Restore was cancelled
        /// </summary>
        RestoreCanceled,

        /// <summary>
        /// Restore encountered an error and did not restore all files
        /// </summary>
        AnotherError
    }
}
