namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The state of download process.
    /// </summary>
    public enum DownloadState
    {
        /// <summary>
        /// The download not initialized.
        /// </summary>
        NotStarted,

        /// <summary>
        /// The downloading is in progress.
        /// </summary>
        InProgress,

        /// <summary>
        /// The downloading paused.
        /// </summary>
        Paused,

        /// <summary>
        /// The downloading finished.
        /// </summary>
        Finished,

        /// <summary>
        /// The downloading canceled.
        /// </summary>
        Canceled
    }
}