using System;

namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The dayls of the week for scheduling.
    /// </summary>
    [Flags]
    public enum ScheduleDayOfWeek
    {
        /// <summary>
        /// The Sunday.
        /// </summary>
        Sunday = 1,

        /// <summary>
        /// The Monday
        /// </summary>
        Monday = 2,

        /// <summary>
        /// The Tuesday
        /// </summary>
        Tuesday = 4,

        /// <summary>
        /// The Wednesday
        /// </summary>
        Wednesday = 8,

        /// <summary>
        /// The Thursday
        /// </summary>
        Thursday = 16,

        /// <summary>
        /// The Friday
        /// </summary>
        Friday = 32,

        /// <summary>
        /// The Saturday
        /// </summary>
        Saturday = 64,

        /// <summary>
        /// Not selected days.
        /// </summary>
        None = 128
    }
}
