namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The scheduling mode.
    /// </summary>
    public enum ScheduleMode
    {
        /// <summary>
        /// Automatic scheduling
        /// </summary>
        Automatic,

        /// <summary>
        /// The daily scheduling
        /// </summary>
        Daily,

        /// <summary>
        /// Scheduling in specified period.
        /// </summary>
        InSpecifiedPeriod,

        /// <summary>
        /// Advanced scheduling options.
        /// </summary>
        Advanced
    }
}
