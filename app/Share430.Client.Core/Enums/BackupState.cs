namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The backup state
    /// </summary>
    public enum BackupState
    {
        /// <summary>
        /// file without mark.
        /// </summary>
        Nothing,

        /// <summary>
        /// new file - not backed up yet
        /// </summary>
        New,

        /// <summary>
        /// file in queue to back up
        /// </summary>
        Pending,

        /// <summary>
        /// backed up file
        /// </summary>
        Backedup
    }
}