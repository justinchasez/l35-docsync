namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The backup status states.
    /// </summary>
    public enum BackupStatus
    {
        /// <summary>
        /// No files are scheduled
        /// </summary>
        Empty,

        /// <summary>
        /// Backup in progress.
        /// </summary>
        InProgress,
        
        /// <summary>
        /// Backup failed.
        /// </summary>
        Failed,
        
        /// <summary>
        /// Backup completed.
        /// </summary>
        Completed,

        /// <summary>
        /// Files are pending backup
        /// </summary>
        Pending
    }
}
