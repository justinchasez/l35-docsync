using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The membership status.
    /// </summary>
    [Serializable]
    [XmlType(Namespace = "http://schemas.datacontract.org/2004/07/System.Web.Security")]
    public enum MembershipCreateStatus
    {
        /// <summary>
        /// Is success
        /// </summary>
        Success,

        /// <summary>
        /// If invalid user name
        /// </summary>
        InvalidUserName,

        /// <summary>
        /// If invalid password
        /// </summary>
        InvalidPassword,

        /// <summary>
        /// If invalid question
        /// </summary>
        InvalidQuestion,

        /// <summary>
        /// If incvalid answer
        /// </summary>
        InvalidAnswer,

        /// <summary>
        /// If invalid email
        /// </summary>
        InvalidEmail,

        /// <summary>
        /// If use name already exists
        /// </summary>
        DuplicateUserName,

        /// <summary>
        /// Is email already exists
        /// </summary>
        DuplicateEmail,

        /// <summary>
        /// If user rejected
        /// </summary>
        UserRejected,

        /// <summary>
        /// If user key is invalid
        /// </summary>
        InvalidProviderUserKey,

        /// <summary>
        /// If duplicated user key.
        /// </summary>
        DuplicateProviderUserKey,

        /// <summary>
        /// If user error occurs.
        /// </summary>
        ProviderError,
    }
}