namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The restore status states.
    /// </summary>
    public enum RestoreStatus
    {
        /// <summary>
        /// Restore in progress.
        /// </summary>
        InProgress,

        /// <summary>
        /// Restore failed.
        /// </summary>
        Failed,

        /// <summary>
        /// Restore finished.
        /// </summary>
        Finished,

        /// <summary>
        /// Restore on hold.
        /// </summary>
        Pending,

        /// <summary>
        /// No files are scheduled
        /// </summary>
        Empty
    }
}
