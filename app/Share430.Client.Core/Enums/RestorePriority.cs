namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The restore priority states.
    /// </summary>
    public enum RestorePriority
    {
        /// <summary>
        /// Low priority (restore later)
        /// </summary>
        Lowest,

        /// <summary>
        /// Normal priority
        /// </summary>
        Normal,

        /// <summary>
        /// High priority (restore sooner)
        /// </summary>
        Highest
    }
}