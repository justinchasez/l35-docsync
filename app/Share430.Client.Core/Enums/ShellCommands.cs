namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// Commands from shell modules
    /// </summary>
    public enum ShellCommands
    {
        /// <summary>
        /// Nothing to do
        /// </summary>
        Nothing,

        /// <summary>
        /// Backup this entry
        /// </summary>
        BackThisUp,

        /// <summary>
        /// Do not backup this entry
        /// </summary>
        DonotBackThisUp,

        /// <summary>
        /// Backup this entry ASAP
        /// </summary>
        BackUpAsap,

        /// <summary>
        /// Restore this entry
        /// </summary>
        Restore,

        /// <summary>
        /// Restore entry to specify path
        /// </summary>
        RestoreTo,

        /// <summary>
        /// Restore previous version of the file
        /// </summary>
        RestorePrevVersion,

        /// <summary>
        /// Remove file from backup
        /// </summary>
        RemoveFromBackup,

        /// <summary>
        /// launch the restore wizard, open info center if needed
        /// </summary>
        LaunchRestoreWizard
    }
}