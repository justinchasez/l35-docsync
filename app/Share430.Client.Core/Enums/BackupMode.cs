namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The mode of backup process.
    /// </summary>
    public enum BackupMode
    {
        /// <summary>
        /// Automatically backup.
        /// </summary>
        Automatic,

        /// <summary>
        /// Backup daily
        /// </summary>
        Daily,

        /// <summary>
        /// Backup not between hours.
        /// </summary>
        NotBetween,

        /// <summary>
        /// Advanced backup settings.
        /// </summary>
        Advanced
    }
}
