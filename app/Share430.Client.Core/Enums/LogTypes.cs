namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The option defines, what type of recovery logs will see
    /// </summary>
    public enum RecoveryLogTypes
    {
        /// <summary>
        /// Recovery logs: Pending restore files 
        /// </summary>
        Pending,

        /// <summary>
        /// Recovery logs: Errors logs 
        /// </summary>
        Errors,

        /// <summary>
        /// Recovery logs: Completed restoring 
        /// </summary>
        Completed
    }
}