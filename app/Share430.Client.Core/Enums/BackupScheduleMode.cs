namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The backup mode.
    /// </summary>
    public enum BackupScheduleMode
    {
        /// <summary>
        /// Backup every day.
        /// </summary>
        EveryDay,

        /// <summary>
        /// Backup on working days (Mon-Fri)
        /// </summary>
        WorkDays,

        /// <summary>
        /// Backup in specified days.
        /// </summary>
        Specific
    }
}
