namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// Types of disks
    /// </summary>
    public enum DiskTypes : uint
    {
        /// <summary>
        /// unknown type
        /// </summary>
        Unknown,

        /// <summary>
        /// removable devices, usb flash drive etc.
        /// </summary>
        Removable = 2,

        /// <summary>
        /// Hard drive disks
        /// </summary>
        Hdd = 3,

        /// <summary>
        /// CD - DVD devices
        /// </summary>
        CdRom = 5
    }
}