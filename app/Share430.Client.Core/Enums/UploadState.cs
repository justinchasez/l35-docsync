namespace Share430.Client.Core.Enums
{
    /// <summary>
    /// The uplading process state.
    /// </summary>
    public enum UploadState
    {
        /// <summary>
        /// The uploading not initialized.
        /// </summary>
        NotStarted,

        /// <summary>
        /// The uploading is in progress.
        /// </summary>
        InProgress,

        /// <summary>
        /// The uploading paused.
        /// </summary>
        Paused,

        /// <summary>
        /// The uploading finished.
        /// </summary>
        Finished,

        /// <summary>
        /// The uploading canceled.
        /// </summary>
        Canceled
    }
}