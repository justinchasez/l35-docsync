using System;
using System.IO;

namespace Share430.Client.Core.Constants
{
    /// <summary>
    /// Constants static class
    /// </summary>
    public static class CoreConstantsHelper
    {
        /// <summary>
        /// Path for save data
        /// </summary>
        public static string PathForSaveData
        {
            get
            {
				string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Share430");

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                return path;
            }
        }

        /// <summary>
        /// Mutex name to syncronize service and clients
        /// </summary>
		public static readonly string ServiceMutexName = "Share430ServerTerminalMutex";

        /// <summary>
        /// Name for event 
        /// </summary>
		public static readonly string ServiceEventName = "Share430ServiceInitEventName";
    }
}
