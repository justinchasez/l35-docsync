using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Entities.Settings
{
    /// <summary>
    /// Contains scheduling settings.
    /// </summary>
    [Serializable]
    public class ScheduleSettingsInfo
    {
        /// <summary>
        /// Gets or sets the scheduling mode.
        /// </summary>
        /// <value>The scheduling mode.</value>
        public ScheduleMode ScheduleMode { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }
    }
}
