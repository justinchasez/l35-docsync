using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Entities.Users;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Entities.Settings
{
    /// <summary>
    /// Contains global settings
    /// </summary>
    [Serializable]
    public class SettingsInfo
    {
        /// <summary>
        /// The schedule settings.
        /// </summary>
        private ScheduleSettingsInfo scheduleSettings;

        /// <summary>
        /// The users credentials.
        /// </summary>
        private UserCredential userCredentials;

        /// <summary>
        /// Users for automatic backup
        /// </summary>
        private List<string> usersForAutomaticBackup;

        private List<string> excludedLocations;
        private List<string> includedLocations;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is first run.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is first run; otherwise, <c>false</c>.
        /// </value>
        public bool IsFirstRun { get; set; }

        public string TargetFolderLocation { get; set; }

        public DateTime? LastUpdateDate { get; set; }

        /// <summary>
        /// Gets or sets the connection priority.
        /// </summary>
        /// <value>The connection priority.</value>
        public ConnectionPriority ConnectionPriority { get; set; }

        /// <summary>
        /// Gets or sets the low priority speed
        /// </summary>
        public int LowPrioritySpeed { get; set; }

        /// <summary>
        /// Gets or sets users for automatic backup
        /// </summary>
        public List<string> UsersForAutomaticBackup
        {
            get
            {
                if (this.usersForAutomaticBackup == null)
                {
                    this.usersForAutomaticBackup = new List<string>();
                }

                return this.usersForAutomaticBackup;
            }

            set
            {
                this.usersForAutomaticBackup = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is recover mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is recover mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsRecoverMode { get; set; }

        #region Internal options

        /// <summary>
        /// Gets or sets the current computer id.
        /// </summary>
        /// <value>The current computer id.</value>
        public ComputerInfo CurrentComputer { get; set; }

        /// <summary>
        /// Gets or sets the current computer encrypt info.
        /// </summary>
        /// <value>The current computer encrypt info.</value>
        public EncryptInfo CurrentComputerEncryptInfo { get; set; }

        /// <summary>
        /// Gets the name of the upload data file.
        /// </summary>
        /// <value>The name of the upload data file.</value>
        public string BackupDataFilePath
        {
            get
            {
                return Path.Combine(CoreConstantsHelper.PathForSaveData, "backup-files.data");
            }
        }

        /// <summary>
        /// Gets the name of the download data file.
        /// </summary>
        /// <value>The name of the download data file.</value>
        public string RestoreDataFilePath
        {
            get
            {
                return Path.Combine(CoreConstantsHelper.PathForSaveData, "restore-files.data");
            }
        }

        #endregion

        /// <summary>
        /// Gets the users credentials.
        /// </summary>
        /// <value>The users credentials.</value>
        public UserCredential Credentials
        {
            get
            {
                if (this.userCredentials == null)
                {
                    this.userCredentials = new UserCredential();
                }

                return this.userCredentials;
            }
        }
        
        /// <summary>
        /// Gets or sets the schedule settings.
        /// </summary>
        /// <value>The schedule settings.</value>
        public ScheduleSettingsInfo ScheduleSettings
        {
            get
            {
                if (this.scheduleSettings == null)
                {
                    this.scheduleSettings = new ScheduleSettingsInfo();
                }

                return this.scheduleSettings;
            }

            set
            {
                this.scheduleSettings = value;
            }
        }

        /// <summary>
        /// get help url
        /// </summary>
        public string HelpUrl { get; set; }

        /// <summary>
        /// get my account url
        /// </summary>
        public string MyAccountUrl { get; set; }

        /// <summary>
        /// Get refer friend url
        /// </summary>
        public string ReferFriendUrl { get; set; }

        /// <summary>
        /// Get or Set time period for pause backing up process
        /// </summary>
        public DateTime PauseTime { get; set; }

        /// <summary>
        /// Is proxy server used
        /// </summary>
        public bool IsProxyServerUsed { get; set; }

        /// <summary>
        /// The proxy server address
        /// </summary>
        public string ProxyServerAddress { get; set; }

        /// <summary>
        /// The proxy server port
        /// </summary>
        public int ProxyServerPort { get; set; }

        public List<string> ExcludedLocations
        {
            get
            {
                if (this.excludedLocations == null)
                {
                    this.excludedLocations = new List<string>();
                }
                
                return excludedLocations;
            }
            set { excludedLocations = value; }
        }

        public List<string> IncludedLocations
        {
            get
            {
                if (this.includedLocations == null)
                {
                    this.includedLocations = new List<string>();
                }

                return includedLocations;
            }
            set { includedLocations = value; }
        }

        public bool IsPathIncluded(string path)
        {
            bool isExcluded = false,
                isIncluded = false;
            if (this.ExcludedLocations.Contains("*"))
            {
                isExcluded = true;
            }
            else if (this.ExcludedLocations.Any(l => path.ToLower().Contains(l.ToLower())))
            {
                isExcluded = true;
            }

            if (this.IncludedLocations.Any(l => path.ToLower().Contains(l.ToLower())))
            {
                isIncluded = true;
            }
            
            return isIncluded || !isExcluded;
        }

    }
}
