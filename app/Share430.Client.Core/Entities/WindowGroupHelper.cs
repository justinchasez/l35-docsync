using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;

namespace Share430.Client.Core.Entities
{
    /// <summary>
    /// Use name of sid
    /// </summary>
    public enum SID_NAME_USE
    {
        /// <summary>
        /// sid type user
        /// </summary>
        SidTypeUser = 1,

        /// <summary>
        /// sid type group
        /// </summary>
        SidTypeGroup,

        /// <summary>
        /// sid type domain
        /// </summary>
        SidTypeDomain,

        /// <summary>
        /// sid type alias
        /// </summary>
        SidTypeAlias,

        /// <summary>
        /// sid type well known group
        /// </summary>
        SidTypeWellKnownGroup,

        /// <summary>
        /// sid type deleted account
        /// </summary>
        SidTypeDeletedAccount,

        /// <summary>
        /// sid type invalid
        /// </summary>
        SidTypeInvalid,

        /// <summary>
        /// sid type unknown
        /// </summary>
        SidTypeUnknown,

        /// <summary>
        /// sid type computer
        /// </summary>
        SidTypeComputer,

        /// <summary>
        /// sid type label
        /// </summary>
        SidTypeLabel
    }

    /// <summary>
    /// Helper for window groups
    /// </summary>
    public class WindowGroupHelper
    {
        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool LookupAccountSid(
            string systemName,
            [MarshalAs(UnmanagedType.LPArray)] byte[] sid,
            System.Text.StringBuilder name,
            ref uint cchname,
            System.Text.StringBuilder referencedDomainName,
            ref uint cchreferencedDomainName,
            out SID_NAME_USE peuse);

        /// <summary>
        /// Gets group name by sid
        /// </summary>
        /// <param name="sidType">well known sid type</param>
        /// <param name="domain">domain, can be null</param>
        /// <param name="withDomain">if true than return name with domain part</param>
        /// <returns>return group name</returns>
        public static string GetGroupName(WellKnownSidType sidType, SecurityIdentifier domain, bool withDomain)
        {
            SecurityIdentifier securityIdentifier = new SecurityIdentifier(sidType, domain);
            byte[] sidBuffer = new byte[securityIdentifier.BinaryLength];
            securityIdentifier.GetBinaryForm(sidBuffer, 0);

            StringBuilder name = new StringBuilder();
            name.Capacity = 255;
            uint chname = 255;

            StringBuilder domainName = new StringBuilder();
            domainName.Capacity = 255;
            uint chdomainName = 255;

            SID_NAME_USE type;
            bool res = LookupAccountSid(null, sidBuffer, name, ref chname, domainName, ref chdomainName, out type);
            return (withDomain) ? string.Format("{0}\\{1}", domainName, name) : name.ToString();

            //            SecurityIdentifier securityIdentifier = new SecurityIdentifier(WellKnownSidType., WindowsIdentity.GetCurrent().User.AccountDomainSid);
            //            NTAccount acc = securityIdentifier.Translate(typeof(NTAccount)) as NTAccount;
            //            return acc.Value;
        }
    }
}