﻿using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    /// <summary>
    /// The syncronize users entry class.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class SyncronizeUsersEntry
    {
        /// <summary>
        /// Path to user desktop
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string DesktopPath { get; set; }

        /// <summary>
        /// Path to user documents
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string DocumentsPath { get; set; }

        /// <summary>
        /// The user windows SID
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string Sid { get; set; }

        /// <summary>
        /// Windows user name
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string UserName { get; set; }
    }
}
