using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    /// <summary>
    /// UpdateComputerGuid inputs.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class UpdateComputerGuidInputs
    {
        /// <summary>
        /// Gets or sets the computer GUID.
        /// </summary>
        /// <value>The computer GUID.</value>
        public string ComputerGuid { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool ComputerIdSpecified { get; set; }
    }
}