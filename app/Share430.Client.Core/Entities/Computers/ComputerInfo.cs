using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    /// <summary>
    /// The computer information.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class ComputerInfo
    {
        /// <summary>
        /// Gets or sets the bytes used.
        /// </summary>
        /// <value>The bytes used.</value>
        public long BytesUsed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [bytes used specified].
        /// </summary>
        /// <value><c>true</c> if [bytes used specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool BytesUsedSpecified { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [expiration date specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [expiration date specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ExpirationDateSpecified { get; set; }

        /// <summary>
        /// Gets or sets the free space.
        /// </summary>
        /// <value>The free space.</value>
        public long? FreeSpace { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [free space specified].
        /// </summary>
        /// <value><c>true</c> if [free space specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool FreeSpaceSpecified { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [id specified].
        /// </summary>
        /// <value><c>true</c> if [id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool IdSpecified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is trial computer.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is trial computer; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrialComputer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is trial computer specified.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is trial computer specified; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsTrialComputerSpecified { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public bool ManualEncriptionState { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [id specified].
        /// </summary>
        /// <value><c>true</c> if [id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool ManualEncriptionStateSpecified { get; set; }

        /// <summary>
        /// Gets or sets the computer name.
        /// </summary>
        /// <value>The computer name.</value>
        [XmlElement(IsNullable = true)]
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>The registration date.</value>
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [registration date specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [registration date specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool RegistrationDateSpecified { get; set; }
    }
}