﻿using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    /// <summary>
    /// Syncronize Users Result class
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class SyncronizeUsersResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is syncronized.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is syncronized; otherwise, <c>false</c>.
        /// </value>
        public bool IsSyncronized { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is syncronized specified.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is syncronized specified; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsSyncronizedSpecified { get; set; }
    }
}