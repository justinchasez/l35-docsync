using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    ///// <summary>
    ///// Computer info dto.
    ///// </summary>
    //[Serializable]
	//[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    //public class ComputerUsersResult
    //{
    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="ComputerUsersResult"/> class.
    //    /// </summary>
    //    //public ComputerUsersResult()
    //    //{
    //    //    this.Users = new List<string>();
    //    //}

    //    /// <summary>
    //    /// Gets or sets the users.
    //    /// </summary>
    //    /// <value>The users.</value>
    //    public List<string> Users { get; set; }
    //}

    /// <summary>
    /// Users from computer
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public partial class ComputerUsersResult
    {
        /// <summary>
        /// Array of users
        /// </summary>
        private SyncronizeUsersEntry[] usersField;

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>The users.</value>
        /// <remarks/>
        [XmlArray(IsNullable = true)]
        public SyncronizeUsersEntry[] Users
        {
            get { return this.usersField; }
            set { this.usersField = value; }
        }
    }
}