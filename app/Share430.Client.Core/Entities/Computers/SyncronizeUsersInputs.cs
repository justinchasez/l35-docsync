﻿using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Computers
{
    /// <summary>
    /// The syncronize users inputs class
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class SyncronizeUsersInputs
    {
        /// <summary>
        /// The computer info
        /// </summary>
        [XmlElement(IsNullable = true)]
        public ComputerInfo Computer { get; set; }

        /// <summary>
        /// Gets or sets the computer GUID.
        /// </summary>
        /// <value>The computer GUID.</value>
        [XmlElement(IsNullable = true)]
        public string ComputerGuid { get; set; }

        /// <summary>
        /// The users list
        /// </summary>
        [XmlArray(IsNullable = true)]
        public SyncronizeUsersEntry[] UsersList { get; set; }
    }
}
