using System;
using Share430.Client.Core.Entities.Files.Interfaces;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// RestorePercentCalculator to calculate percents for restore progress
    /// </summary>
    [Serializable]
    public class RestorePercentCalculator : IPercentageCalculator
    {
        /// <summary>
        /// Calculate percents
        /// </summary>
        /// <param name="progress">progress object to calculate</param>
        /// <returns>return percent value</returns>
        public int Calculate(Progress progress)
        {
            if (progress.FilesCompletedSize == 0)
            {
                return 0;
            }

            if (progress.FilesPendingSize == 0)
            {
                return 100;
            }

            return (int)((100f * progress.FilesCompletedSize) / progress.FilesTotalSize);
//            return (int)((100f * progress.FilesCompletedSize) / (progress.FilesCompletedSize + progress.FilesPendingSize));
        }
    }
}