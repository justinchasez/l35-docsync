using System;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// Xml file info class
    /// </summary>
    [Serializable]
    public class XmlFileInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="XmlFileInfo"/> class.
        /// </summary>
        public XmlFileInfo()
        {
            this.Size = new SizeEntity();
        }

        /// <summary>
        /// Size of the file
        /// </summary>
        public SizeEntity Size { get; set; }

        /// <summary>
        /// File last modified date
        /// </summary>
        public DateTime LastModifiedDate { get; set; }
    }
}
