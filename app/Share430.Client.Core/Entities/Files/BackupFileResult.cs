using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The backup file results.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class BackupFileResult
    {
        /// <summary>
        /// Gets or sets the free space.
        /// </summary>
        /// <value>The free space.</value>
        public long? FreeSpace { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [free space specified].
        /// </summary>
        /// <value><c>true</c> if [free space specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool FreeSpaceSpecified { get; set; }

        /// <summary>
        /// Is computer expired
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Is computer expired Specified
        /// </summary>
        [XmlIgnore]
        public bool IsComputerExpiredSpecified { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is finished.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is finished; otherwise, <c>false</c>.
        /// </value>
        public bool IsFinished { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is finished specified.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is finished specified; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsFinishedSpecified { get; set; }

        /// <summary>
        /// Is limit reached
        /// </summary>
        public bool IsLimitReached { get; set; }

        /// <summary>
        /// Is limit reached specified
        /// </summary>
        [XmlIgnore]
        public bool IsLimitReachedSpecified { get; set; }

        /// <summary>
        /// Gets or sets the parts count.
        /// </summary>
        /// <value>The parts count.</value>
        public int PartsCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [parts count specified].
        /// </summary>
        /// <value><c>true</c> if [parts count specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool PartsCountSpecified { get; set; }

        /// <summary>
        /// Used space
        /// </summary>
        public long UsedSpace { get; set; }

        /// <summary>
        /// Used space specified
        /// </summary>
        [XmlIgnore]
        public bool UsedSpaceSpecified { get; set; }

        /// <summary>
        /// Get or set file versions count
        /// </summary>
        public int VersionsCount { get; set; }

        /// <summary>
        /// Versions Count specified
        /// </summary>
        [XmlIgnore]
        public bool VersionsCountSpecified { get; set; }
    }
}