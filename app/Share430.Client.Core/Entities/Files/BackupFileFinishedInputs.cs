using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The backup file finished params.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class BackupFileFinishedInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool ComputerIdSpecified { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>The backuped file.</value>
        [XmlElement(IsNullable = true)]
        public string File { get; set; }
    }
}