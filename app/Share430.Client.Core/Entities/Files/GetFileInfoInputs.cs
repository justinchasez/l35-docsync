namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The input params for GetFileInfo method. 
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public partial class GetFileInfoInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        private int computerIdField;

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        private bool computerIdFieldSpecified;

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        private string fileField;

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId
        {
            get { return this.computerIdField; }
            set { this.computerIdField = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ComputerIdSpecified
        {
            get { return this.computerIdFieldSpecified; }
            set { this.computerIdFieldSpecified = value; }
        }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        /// <value>The file name.</value>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string File
        {
            get { return this.fileField; }
            set { this.fileField = value; }
        }
    }
}