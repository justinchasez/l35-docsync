using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// Removing file result.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCode("System.Xml", "4.0.30319.1")]
    [Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class RemovingFileResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        private bool isSuccessField;

        /// <summary>
        /// Gets or sets a value indicating whether [is success specified].
        /// </summary>
        private bool isSuccessFieldSpecified;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess
        {
            get
            {
                return this.isSuccessField;
            }

            set
            {
                this.isSuccessField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [is success specified].
        /// </summary>
        /// <value><c>true</c> if [is success specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool IsSuccessSpecified
        {
            get
            {
                return this.isSuccessFieldSpecified;
            }

            set
            {
                this.isSuccessFieldSpecified = value;
            }
        }
    }
}