using System;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The file version.
    /// </summary>
    public class BackedUpFileVersion
    {
        /// <summary>
        /// Gets or sets the backup date.
        /// </summary>
        /// <value>The backup date.</value>
        public object BackupDate
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The file size.</value>
        public long Size
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public long Version
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}