using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// Teh file part info.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class FilePartDto
    {
        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>The part length.</value>
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [length specified].
        /// </summary>
        /// <value><c>true</c> if [length specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool LengthSpecified { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The part number.</value>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [number specified].
        /// </summary>
        /// <value><c>true</c> if [number specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool NumberSpecified { get; set; }

        /// <summary>
        /// Gets or sets the s3 part id.
        /// </summary>
        /// <value>The s3 part id.</value>
        public string S3PartId { get; set; }
    }
}