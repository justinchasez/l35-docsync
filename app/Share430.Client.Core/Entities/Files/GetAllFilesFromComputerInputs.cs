using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// GetAllFilesFromComputer inputs.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class GetAllFilesFromComputerInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool ComputerIdSpecified { get; set; }

        /// <summary>
        /// Gets or sets the files category.
        /// </summary>
        /// <value>The files category.</value>
        public SelectFilesMode FilesCategory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [files category specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [files category specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool FilesCategorySpecified { get; set; }

        /// <summary>
        /// Gets or sets the user sid.
        /// </summary>
        /// <value>The user sid.</value>
        public string UserSid { get; set; }
    }
}