using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// Select files mode for GetAllFilesFromComputer method
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public enum SelectFilesMode
    {
        /// <summary>
        /// Select all files
        /// </summary>
        All,

        /// <summary>
        /// Select only documents files
        /// </summary>
        Documents,

        /// <summary>
        /// Select all files without documents
        /// </summary>
        WithoutDocuments
    }
}