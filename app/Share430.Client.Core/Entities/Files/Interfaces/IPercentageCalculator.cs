namespace Share430.Client.Core.Entities.Files.Interfaces
{
    /// <summary>
    /// IPercentageCalculator interface
    /// </summary>
    public interface IPercentageCalculator
    {
        /// <summary>
        /// Calculate percents
        /// </summary>
        /// <param name="progress">progress object to calculate</param>
        /// <returns>return percents value</returns>
        int Calculate(Progress progress);
    }
}