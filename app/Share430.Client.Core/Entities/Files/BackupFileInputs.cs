using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    ///// <summary>
    ///// The backup params.
    ///// </summary>
    //[Serializable]
	//[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    //public class BackupFileInputs
    //{
    //    /// <summary>
    //    /// Gets or sets the computer id.
    //    /// </summary>
    //    /// <value>The computer id.</value>
    //    public int ComputerId { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value indicating whether [computer id specified].
    //    /// </summary>
    //    /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
    //    [XmlIgnoreAttribute]
    //    public bool ComputerIdSpecified { get; set; }

    //    /// <summary>
    //    /// Gets or sets the file.
    //    /// </summary>
    //    /// <value>The backup file.</value>
    //    [XmlElement(IsNullable = true)]
    //    public string File { get; set; }

    //    /// <summary>
    //    /// Get or set old file path
    //    /// </summary>
    //    [XmlElement(IsNullable = true)]
    //    public string FileOld { get; set; }

    //    /// <summary>
    //    /// Gets or sets the file hash.
    //    /// </summary>
    //    /// <value>The file hash.</value>
    //    [XmlElement(IsNullable = true)]
    //    public string FileHash { get; set; }

    //    /// <summary>
    //    /// Gets or sets the size.
    //    /// </summary>
    //    /// <value>The file size.</value>
    //    public long Size { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value indicating whether [size specified].
    //    /// </summary>
    //    /// <value><c>true</c> if [size specified]; otherwise, <c>false</c>.</value>
    //    [XmlIgnoreAttribute]
    //    public bool SizeSpecified { get; set; }

    //    /// <summary>
    //    /// Is file upload boolean value
    //    /// </summary>
    //    public bool Upload { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value indicating whether [Upload specified].
    //    /// </summary>
    //    [XmlIgnoreAttribute]
    //    public bool UploadSpecified { get; set; }

    //    /// <summary>
    //    /// Is file rename boolean value
    //    /// </summary>
    //    public bool Rename { get; set; }

    //    /// <summary>
    //    /// Gets or sets a value indicating whether [Rename specified].
    //    /// </summary>
    //    [XmlIgnoreAttribute]
    //    public bool RenameSpecified { get; set; }
    //}

    /// <summary>
    /// The backup params.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.1")]
    [Serializable()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public partial class BackupFileInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        private int computerIdField;

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        private bool computerIdFieldSpecified;

        /// <summary>
        /// Is file create copy boolean value.
        /// </summary>
        private bool createCopyField;

        /// <summary>
        /// Gets or sets a value indicating whether [CreateCopy specified].
        /// </summary>
        /// <value><c>true</c> if [CreateCopy specified]; otherwise, <c>false</c>.</value>
        private bool createCopyFieldSpecified;

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>The backup file.</value>
        private string fileField;

        /// <summary>
        /// Gets or sets the file hash.
        /// </summary>
        /// <value>The file hash.</value>
        private string fileHashField;

        /// <summary>
        /// Get or set old file path
        /// </summary>
        private string fileOldField;

        /// <summary>
        /// Gets or sets the last saved on.
        /// </summary>
        private DateTime lastSavedDateField;

        /// <summary>
        /// Gets or sets a value indicating whether [last saved date specified].
        /// </summary>
        private bool lastSavedDateFieldSpecified;

        /// <summary>
        /// Is file rename boolean value
        /// </summary>
        private bool renameField;

        /// <summary>
        /// Gets or sets a value indicating whether [Rename specified].
        /// </summary>
        private bool renameFieldSpecified;

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The file size.</value>
        private long sizeField;

        /// <summary>
        /// Gets or sets a value indicating whether [size specified].
        /// </summary>
        /// <value><c>true</c> if [size specified]; otherwise, <c>false</c>.</value>
        private bool sizeFieldSpecified;
        
        /// <summary>
        /// Is file upload boolean value
        /// </summary>
        private bool uploadField;

        /// <summary>
        /// Gets or sets a value indicating whether [Upload specified].
        /// </summary>
        private bool uploadFieldSpecified;

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId
        {
            get { return this.computerIdField; }
            set { this.computerIdField = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore()]
        public bool ComputerIdSpecified
        {
            get { return this.computerIdFieldSpecified; }
            set { this.computerIdFieldSpecified = value; }
        }

        /// <summary>
        /// Is file create copy boolean value.
        /// </summary>
        public bool CreateCopy
        {
            get { return this.createCopyField; }
            set { this.createCopyField = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [CreateCopy specified].
        /// </summary>
        [XmlIgnore()]
        public bool CreateCopySpecified
        {
            get { return this.createCopyFieldSpecified; }
            set { this.createCopyFieldSpecified = value; }
        }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>The backup file.</value>
        [XmlElement(IsNullable = true)]
        public string File
        {
            get { return this.fileField; }
            set { this.fileField = value; }
        }

        /// <summary>
        /// Gets or sets the file hash.
        /// </summary>
        /// <value>The file hash.</value>
        [XmlElement(IsNullable = true)]
        public string FileHash
        {
            get
            {
                return this.fileHashField;
            }

            set
            {
                this.fileHashField = value;
            }
        }

        /// <summary>
        /// Get or set old file path
        /// </summary>
        [XmlElement(IsNullable = true)]
        public string FileOld
        {
            get
            {
                return this.fileOldField;
            }

            set
            {
                this.fileOldField = value;
            }
        }

        /// <summary>
        /// Gets or sets the last saved date.
        /// </summary>
        /// <value>The last saved date.</value>
        public DateTime LastSavedDate
        {
            get
            {
                return this.lastSavedDateField;
            }

            set
            {
                this.lastSavedDateField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [last saved date specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [last saved date specified]; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore()]
        public bool LastSavedDateSpecified
        {
            get
            {
                return this.lastSavedDateFieldSpecified;
            }

            set
            {
                this.lastSavedDateFieldSpecified = value;
            }
        }

        /// <summary>
        /// Is file rename boolean value
        /// </summary>
        public bool Rename
        {
            get
            {
                return this.renameField;
            }

            set
            {
                this.renameField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [Rename specified].
        /// </summary>
        [XmlIgnore()]
        public bool RenameSpecified
        {
            get
            {
                return this.renameFieldSpecified;
            }

            set
            {
                this.renameFieldSpecified = value;
            }
        }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>The file size.</value>
        public long Size
        {
            get
            {
                return this.sizeField;
            }

            set
            {
                this.sizeField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [size specified].
        /// </summary>
        /// <value><c>true</c> if [size specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore()]
        public bool SizeSpecified
        {
            get
            {
                return this.sizeFieldSpecified;
            }

            set
            {
                this.sizeFieldSpecified = value;
            }
        }

        /// <summary>
        /// Is file upload boolean value
        /// </summary>
        public bool Upload
        {
            get
            {
                return this.uploadField;
            }

            set
            {
                this.uploadField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [Upload specified].
        /// </summary>
        [XmlIgnore()]
        public bool UploadSpecified
        {
            get
            {
                return this.uploadFieldSpecified;
            }

            set
            {
                this.uploadFieldSpecified = value;
            }
        }
    }
}
