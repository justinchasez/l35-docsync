using System;
using Share430.Client.Core.Entities.Files.Interfaces;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The progress info.
    /// </summary>
    [Serializable]
    public class Progress
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Progress"/> class.
        /// </summary>
        /// <param name="calculator">calculator object</param>
        public Progress(IPercentageCalculator calculator)
        {
            this.PercentCalculator = calculator;
        }

        /// <summary>
        /// PercentCalculator object
        /// </summary>
        private IPercentageCalculator PercentCalculator { get; set; }

        /// <summary>
        /// Gets or sets the files total.
        /// </summary>
        /// <value>The files total.</value>
        public int FilesTotal { get; set; }

        /// <summary>
        /// Gets or sets the files pending.
        /// </summary>
        /// <value>The files pending.</value>
        public int FilesPending { get; set; }

        /// <summary>
        /// Gets or sets the files backuped.
        /// </summary>
        /// <value>The files backuped.</value>
        public int FilesCompleted { get; set; }

        /// <summary>
        /// Gets or sets the size of the files backuped.
        /// </summary>
        /// <value>The size of the files backuped.</value>
        public long FilesTotalSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the files pending.
        /// </summary>
        /// <value>The size of the files pending.</value>
        public long FilesPendingSize { get; set; }

        /// <summary>
        /// Gets or sets the size of the files completed.
        /// </summary>
        /// <value>The size of the files completed.</value>
        public long FilesCompletedSize { get; set; }

        /// <summary>
        /// Gets or sets the current file path.
        /// </summary>
        /// <value>The current file path.</value>
        public string CurrentFilePath { get; set; }

        /// <summary>
        /// Gets or sets error message
        /// </summary>
        /// <value>last error for current file.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets the percentage.
        /// </summary>
        /// <value>The percentage.</value>
        public int Percentage
        {
            get
            {
                return this.PercentCalculator.Calculate(this);
//                if (this.FilesTotalSize == 0)
//                {
//                    return 0;
//                }
//
//                if (this.FilesPendingSize == 0)
//                {
//                    return 100;
//                }
//
//                return (int)((100f * this.FilesTotalSize) / (this.FilesTotalSize + this.FilesPendingSize));
            }
        }

        /// <summary>
        /// Gets or sets the files errors count.
        /// </summary>
        /// <value>The files errors count.</value>
        public int FilesErrors { get; set; }
    }
}