using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// Dto for getting information about files in computer.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class ComputerFilesDto
    {
        /// <summary>
        /// Gets or sets the files info.
        /// </summary>
        /// <value>The files info.</value>
        public List<FileModel> FilesInfo { get; set; }
    }
}