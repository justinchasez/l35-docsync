using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The BackupPartFinished params.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class BackupPartFinishedInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [computer id specified].
        /// </summary>
        /// <value><c>true</c> if [computer id specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool ComputerIdSpecified { get; set; }

        /// <summary>
        /// Gets or sets the file.
        /// </summary>
        /// <value>The backup file.</value>
        [XmlElement(IsNullable = true)]
        public string File { get; set; }

        /// <summary>
        /// Gets or sets the length of the part.
        /// </summary>
        /// <value>The length of the part.</value>
        public int PartLength { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [part length specified].
        /// </summary>
        /// <value><c>true</c> if [part length specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool PartLengthSpecified { get; set; }

        /// <summary>
        /// Gets or sets the s3 part id.
        /// </summary>
        /// <value>The s3 part id.</value>
        public string S3PartId { get; set; }
    }
}