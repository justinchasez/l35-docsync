using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The restore file results.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class RestoreFileResult
    {
        /// <summary>
        /// Gets or sets the file parts.
        /// </summary>
        /// <value>The file parts.</value>
        [XmlArray(IsNullable = true)]
        public FilePartDto[] FileParts { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        [XmlElement(IsNullable = true)]
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets the size of the file.
        /// </summary>
        /// <value>The size of the file.</value>
        public long FileSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [file size specified].
        /// </summary>
        /// <value><c>true</c> if [file size specified]; otherwise, <c>false</c>.</value>
        [XmlIgnore]
        public bool FileSizeSpecified { get; set; }

        /// <summary>
        /// Gets or sets is computer expired value
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Gets or sets is computer expired value
        /// </summary>
        [XmlIgnore]
        public bool IsComputerExpiredSpecified { get; set; }
    }
}