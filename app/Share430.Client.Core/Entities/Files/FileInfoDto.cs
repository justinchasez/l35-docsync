using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The file info
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class FileInfoDto
    {
        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file versions.
        /// </summary>
        /// <value>The file versions.</value>
        public List<FileVersionDto> FileVersions { get; set; }

        /// <summary>
        /// Gets or sets the name of the owner.
        /// </summary>
        /// <value>The name of the owner.</value>
        public string SpecialClientPath { get; set; }
    }
}