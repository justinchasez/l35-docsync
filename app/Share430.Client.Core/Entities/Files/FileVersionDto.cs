using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// The file version
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class FileVersionDto
    {
        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        /// <value>The version number.</value>
        public int VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the finished date.
        /// </summary>
        /// <value>The finished date.</value>
        public DateTime FinishedDate { get; set; }

        /// <summary>
        /// Gets or sets the last saved date.
        /// </summary>
        /// <value>The last saved date.</value>
        public DateTime LastSavedDate { get; set; }

        /// <summary>
        /// Gets or sets the size of the file.
        /// </summary>
        /// <value>The size of the file.</value>
        public long FileSize { get; set; }
    }
}