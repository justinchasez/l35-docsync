using System;
using Share430.Client.Core.Entities.Files.Interfaces;

namespace Share430.Client.Core.Entities.Files
{
    /// <summary>
    /// BackupPercentCalculator to calculate percents for backup progress
    /// </summary>
    [Serializable]
    public class BackupPercentCalculator : IPercentageCalculator
    {
        /// <summary>
        /// Calculate percent value
        /// </summary>
        /// <param name="progress">progress object to calculate</param>
        /// <returns>return percent value</returns>
        public int Calculate(Progress progress)
        {
            if (progress.FilesTotalSize == 0)
            {
                return 0;
            }

            if (progress.FilesPendingSize == 0)
            {
                return 100;
            }

            return (int)((100f * progress.FilesTotalSize) / (progress.FilesTotalSize + progress.FilesPendingSize));
        }
    }
}