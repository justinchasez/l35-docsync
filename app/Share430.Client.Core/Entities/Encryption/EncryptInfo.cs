using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Share430.Client.Core.Entities.Encryption
{
    /// <summary>
    /// The ecnryption information.
    /// </summary>
    [Serializable]
    public class EncryptInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptInfo"/> class.
        /// </summary>
        /// <param name="data">The enc key data.</param>
        public EncryptInfo(byte[] data)
        {
            this.InitKeys(data);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptInfo"/> class.
        /// </summary>
        /// <param name="key">The encryption key.</param>
        /// <param name="vector">The encryption vector.</param>
        public EncryptInfo(byte[] key, byte[] vector)
        {
            this.Key = key;
            this.Vector = vector;
        }

        /// <summary>
        /// Gets or sets the encryption key.
        /// </summary>
        /// <value>The encryption key.</value>
        public byte[] Key { get; set; }

        /// <summary>
        /// Gets or sets the encryption vector.
        /// </summary>
        /// <value>The encryption vector.</value>
        public byte[] Vector { get; set; }

        /// <summary>
        /// Gets or sets the password hash.
        /// </summary>
        /// <value>The password hash.</value>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Gets or sets the hint.
        /// </summary>
        /// <value>The password hint.</value>
        public string Hint { get; set; }

        /// <summary>
        /// Loads from file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>The initialized key.</returns>
        public static EncryptInfo LoadFromFile(string filePath)
        {
            if (filePath == null)
            {
                throw new ArgumentNullException("filePath");
            }

            if (File.Exists(filePath))
            {
                return new EncryptInfo(File.ReadAllBytes(filePath));
            }

            return null;
        }

        /// <summary>
        /// Loads the default key.
        /// </summary>
        /// <returns>Default encrypt info</returns>
        public static EncryptInfo LoadDefault()
        {
            EncryptInfo defaultKey = EncryptInfo.LoadFromFile(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\default.pem");

            return defaultKey;
        }

        /// <summary>
        /// Saves to file.
        /// </summary>
        /// <param name="info">The encrypt info.</param>
        /// <param name="filePath">The file path.</param>
        public static void SaveToFile(EncryptInfo info, string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            using (FileStream fileStream = File.OpenWrite(filePath))
            {
                info.Hint = Convert.ToBase64String(Encoding.Default.GetBytes(info.Hint));

                List<byte> buffer = new List<byte>();
                buffer.AddRange(info.Key);
                buffer.AddRange(info.Vector);
                buffer.AddRange(Convert.FromBase64String(info.PasswordHash));
                buffer.AddRange(Encoding.Default.GetBytes(info.Hint));

                fileStream.Write(buffer.ToArray(), 0, buffer.Count);
            }
        }

        /// <summary>
        /// Inits the keys.
        /// </summary>
        /// <param name="data">The encrypt keys data.</param>
        private void InitKeys(byte[] data)
        {
            if (data != null && data.Length > 16)
            {
                List<byte> buffer = new List<byte>(data);

                this.Key = buffer.GetRange(0, 16).ToArray();
                this.Vector = buffer.GetRange(16, 16).ToArray();
                this.PasswordHash = Convert.ToBase64String(buffer.GetRange(32, 16).ToArray());
                this.Hint = Encoding.Default.GetString(buffer.GetRange(48, buffer.Count - 48).ToArray());
                this.Hint = Encoding.Default.GetString(Convert.FromBase64String(this.Hint));
            }
            else
            {
                throw new ArgumentException("data");
            }
        }

        /// <summary>
        /// Generates the encryption info.
        /// </summary>
        /// <returns>The encryption info.</returns>
        public static EncryptInfo GenerateEncryptionInfo()
        {
            RijndaelManaged algorithm = new RijndaelManaged { KeySize = 128 };

            algorithm.GenerateKey();
            algorithm.GenerateIV();

            return new EncryptInfo(algorithm.Key, algorithm.IV);
        }
    }
}