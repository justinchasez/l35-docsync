using System;
using Share430.Client.Core.Managers.Convert;

namespace Share430.Client.Core.Entities
{
    /// <summary>
    /// File size class
    /// </summary>
    [Serializable]
    public class SizeEntity : IComparable
    {
        /// <summary>
        /// Size of file
        /// </summary>
        public long Size { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SizeEntity"/> class.
        /// </summary>
        /// <param name="size">Size of file</param>
        public SizeEntity(long size)
        {
            this.Size = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SizeEntity"/> class.
        /// </summary>
        public SizeEntity() : this(0)
        {
        }

        /// <summary>
        /// Compare with other SizeEntity
        /// </summary>
        /// <param name="obj">Other SizeEntity object</param>
        /// <returns>Compare result</returns>
        public int CompareTo(object obj)
        {
            return this.Size.CompareTo(((SizeEntity)obj).Size);
        }

        /// <summary>
        /// To string method
        /// </summary>
        /// <returns>String of the size</returns>
        public override string ToString()
        {
            return ConvertData.InformationSizeConvertor.Convert(this.Size);
        }
    }
}
