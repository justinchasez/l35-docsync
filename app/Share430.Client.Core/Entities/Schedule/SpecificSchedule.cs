using System;
using System.Collections.Generic;
using Share430.Client.Core.Entities.Schedule.Interface;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Entities.Schedule
{
    /// <summary>
    /// The specific schedule options.
    /// </summary>
    [Serializable]
    public class SpecificSchedule : ISchedule
    {
        /// <summary>
        /// The specific days of the week.
        /// </summary>
        public ScheduleDayOfWeek Days { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificSchedule"/> class.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public SpecificSchedule(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException("The start date can't be creater than end date");
            }

            this.StartDate = startDate;
            this.EndDate = endDate;

            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecificSchedule"/> class.
        /// </summary>
        /// <param name="days">The specific days.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public SpecificSchedule(ScheduleDayOfWeek days, DateTime startDate, DateTime endDate) : this(startDate, endDate)
        {
            this.Days = days;
        }

        #region ISchedule Members

        /// <summary>
        /// Gets the ID.
        /// </summary>
        /// <value>The schedule ID.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; private set; }

        /// <summary>
        /// Gets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; private set; }

        /// <summary>
        /// Converts the day of week.
        /// </summary>
        /// <param name="dateDayOfWeek">The date day of week.</param>
        /// <returns>The ScheduleDayOfWeek object.</returns>
        public static ScheduleDayOfWeek ConvertDayOfWeek(DayOfWeek dateDayOfWeek)
        {
            string currentDay = dateDayOfWeek.ToString();
            return (ScheduleDayOfWeek)Enum.Parse(typeof(ScheduleDayOfWeek), currentDay);
        }

        /// <summary>
        /// Determines whether [is date in period] [the specified date time].
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>
        /// 	<c>true</c> if [is date in period] [the specified date time]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDateInPeriod(DateTime dateTime)
        {
            if (this.IsDayInPeriod(dateTime.DayOfWeek))
            {
                if (this.EndDate.Date > this.StartDate.Date)
                {
                    this.StartDate = dateTime.Date.AddHours(this.StartDate.Hour).AddMinutes(this.StartDate.Minute);
                    this.EndDate = dateTime.Date.AddHours(this.EndDate.Hour).AddMinutes(this.EndDate.Minute).AddDays(1);
                }
                else
                {
                    this.StartDate = dateTime.Date.AddHours(this.StartDate.Hour).AddMinutes(this.StartDate.Minute);
                    this.EndDate = dateTime.Date.AddHours(this.EndDate.Hour).AddMinutes(this.EndDate.Minute);
                }

                return this.StartDate < dateTime && this.EndDate > dateTime;
            }

            return false;
        }

        /// <summary>
        /// Determines whether [is day in period] [the specified day of week].
        /// </summary>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>
        /// 	<c>true</c> if [is day in period] [the specified day of week]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDayInPeriod(DayOfWeek dayOfWeek)
        {
            ScheduleDayOfWeek currentDayOfWeek = ConvertDayOfWeek(dayOfWeek);
            return (this.Days & currentDayOfWeek) == currentDayOfWeek;
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return String.Format("{0} from {1} until {2}", this.GetDaysString(this.Days), this.StartDate.ToShortTimeString(), this.EndDate.ToShortTimeString());
        }

        /// <summary>
        /// Gets the days string.
        /// </summary>
        /// <param name="daysEnum">The days enum.</param>
        /// <returns>A string that represents days.</returns>
        private string GetDaysString(ScheduleDayOfWeek daysEnum)
        {
            List<string> days = new List<string>();

            if ((daysEnum & ScheduleDayOfWeek.Sunday) == ScheduleDayOfWeek.Sunday)
            {
                days.Add("Sun");
            }

            if ((daysEnum & ScheduleDayOfWeek.Monday) == ScheduleDayOfWeek.Monday)
            {
                days.Add("Mon");
            }

            if ((daysEnum & ScheduleDayOfWeek.Tuesday) == ScheduleDayOfWeek.Tuesday)
            {
                days.Add("Tue");
            }

            if ((daysEnum & ScheduleDayOfWeek.Wednesday) == ScheduleDayOfWeek.Wednesday)
            {
                days.Add("Wed");
            }

            if ((daysEnum & ScheduleDayOfWeek.Thursday) == ScheduleDayOfWeek.Thursday)
            {
                days.Add("Thu");
            }

            if ((daysEnum & ScheduleDayOfWeek.Friday) == ScheduleDayOfWeek.Friday)
            {
                days.Add("Fri");
            }

            if ((daysEnum & ScheduleDayOfWeek.Saturday) == ScheduleDayOfWeek.Saturday)
            {
                days.Add("Sat");
            }

            return String.Join(", ", days.ToArray());
        }
    }
}