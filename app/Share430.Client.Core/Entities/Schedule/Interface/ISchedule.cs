using System;

namespace Share430.Client.Core.Entities.Schedule.Interface
{
    /// <summary>
    /// The schedule interface.
    /// </summary>
    public interface ISchedule
    {
        /// <summary>
        /// Gets the ID.
        /// </summary>
        /// <value>The schedule ID.</value>
        Guid Id { get; set; }

        /// <summary>
        /// Gets the start date.
        /// </summary>
        /// <value>The start date.</value>
        DateTime StartDate { get; }

        /// <summary>
        /// Gets the end date.
        /// </summary>
        /// <value>The end date.</value>
        DateTime EndDate { get; }

        /// <summary>
        /// Determines whether [is date in period] [the specified date time].
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>
        /// 	<c>true</c> if [is date in period] [the specified date time]; otherwise, <c>false</c>.
        /// </returns>
        bool IsDateInPeriod(DateTime dateTime);

        /// <summary>
        /// Determines whether [is day in period] [the specified day of week].
        /// </summary>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>
        /// 	<c>true</c> if [is day in period] [the specified day of week]; otherwise, <c>false</c>.
        /// </returns>
        bool IsDayInPeriod(DayOfWeek dayOfWeek);
    }
}