using System;
using Share430.Client.Core.Entities.Schedule.Interface;

namespace Share430.Client.Core.Entities.Schedule
{
    /// <summary>
    /// The schedule of working days.
    /// </summary>
    [Serializable]
    public class WorkingDaySchedule : ISchedule
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkingDaySchedule"/> class.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public WorkingDaySchedule(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException("The start date can't be creater than end date");
            }

            this.StartDate = startDate;
            this.EndDate = endDate;

            this.Id = Guid.NewGuid();
        }

        #region ISchedule Members

        /// <summary>
        /// Gets the ID.
        /// </summary>
        /// <value>The schedule ID.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; private set; }

        /// <summary>
        /// Gets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; private set; }

        /// <summary>
        /// Determines whether [is date in period] [the specified date time].
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>
        /// 	<c>true</c> if [is date in period] [the specified date time]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDateInPeriod(DateTime dateTime)
        {
            if (this.IsDayInPeriod(dateTime.DayOfWeek))
            {
                if (this.EndDate.Date > this.StartDate.Date)
                {
                    this.StartDate = dateTime.Date.AddHours(this.StartDate.Hour).AddMinutes(this.StartDate.Minute);
                    this.EndDate = dateTime.Date.AddHours(this.EndDate.Hour).AddMinutes(this.EndDate.Minute).AddDays(1);
                }
                else
                {
                    this.StartDate = dateTime.Date.AddHours(this.StartDate.Hour).AddMinutes(this.StartDate.Minute);
                    this.EndDate = dateTime.Date.AddHours(this.EndDate.Hour).AddMinutes(this.EndDate.Minute);
                }

                return this.StartDate < dateTime && this.EndDate > dateTime;
            }

            return false;
        }

        /// <summary>
        /// Determines whether [is day in period] [the specified day of week].
        /// </summary>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns>
        /// 	<c>true</c> if [is day in period] [the specified day of week]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsDayInPeriod(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Monday ||
                   dayOfWeek == DayOfWeek.Tuesday ||
                   dayOfWeek == DayOfWeek.Wednesday ||
                   dayOfWeek == DayOfWeek.Thursday ||
                   dayOfWeek == DayOfWeek.Friday;
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return String.Format("Mon-Fri from {0} until {1}", this.StartDate.ToShortTimeString(), this.EndDate.ToShortTimeString());
        }
    }
}