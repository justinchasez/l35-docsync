using System;
using System.Xml.Serialization;
using Share430.Client.Core.Enums;

namespace Share430.Client.Core.Entities.Users
{
    /// <summary>
    /// The user registration result data.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class UserRegistrationResult
    {
        /// <summary>
        /// Gets or sets the create status.
        /// </summary>
        /// <value>The create status.</value>
        public MembershipCreateStatus CreateStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [create status specified].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [create status specified]; otherwise, <c>false</c>.
        /// </value>
        public bool CreateStatusSpecified { get; set; }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        [XmlElement(IsNullable = true)]
        public string ErrorMessage { get; set; }
    }
}