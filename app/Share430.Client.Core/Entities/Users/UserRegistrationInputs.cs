using System;
using System.Xml.Serialization;

namespace Share430.Client.Core.Entities.Users
{
    /// <summary>
    /// The user regiotration info.
    /// </summary>
    [Serializable]
	[XmlType(Namespace = "http://schemas.datacontract.org/2004/07/Share430.Server.Services.Dtos")]
    public class UserRegistrationInputs
    {
        /// <summary>
        /// Gets or sets the users email.
        /// </summary>
        /// <value>The users email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the users password.
        /// </summary>
        /// <value>The users password.</value>
        public string Password { get; set; }
    }
}