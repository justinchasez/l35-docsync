﻿using System;

namespace Share430.Client.Core.Entities.Users
{
    /// <summary>
    /// User folders class.
    /// </summary>
    [Serializable]
    public class UserFolders
    {
        /// <summary>
        /// Path to desktop folder
        /// </summary>
        public string PathToDesktop { get; set; }

        /// <summary>
        /// Path to documents folder
        /// </summary>
        public string PathToDocuments { get; set; }
    }
}
