using System;

namespace Share430.Client.Core.Entities.Users
{
    /// <summary>
    /// The user credential
    /// </summary>
    [Serializable]
    public class UserCredential
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        /// <value>The user name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        /// <value>The user password.</value>
        public string Token { get; set; }
    }
}