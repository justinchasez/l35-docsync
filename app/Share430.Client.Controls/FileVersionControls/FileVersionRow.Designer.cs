namespace Share430.Client.Controls.FileVersionControls
{
    partial class FileVersionRow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBack = new System.Windows.Forms.Panel();
            this.lblLastSaved = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblBackupDate = new System.Windows.Forms.Label();
            this.pnlRight = new System.Windows.Forms.Panel();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlBack.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBack
            // 
            this.pnlBack.Controls.Add(this.lblLastSaved);
            this.pnlBack.Controls.Add(this.lblVersion);
            this.pnlBack.Controls.Add(this.lblSize);
            this.pnlBack.Controls.Add(this.lblBackupDate);
            this.pnlBack.Controls.Add(this.pnlRight);
            this.pnlBack.Controls.Add(this.pnlLeft);
            this.pnlBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBack.Location = new System.Drawing.Point(1, 1);
            this.pnlBack.Name = "pnlBack";
            this.pnlBack.Size = new System.Drawing.Size(529, 22);
            this.pnlBack.TabIndex = 0;
            this.pnlBack.Click += new System.EventHandler(this.FileVersionRow_Click);
            // 
            // lblLastSaved
            // 
            this.lblLastSaved.AutoSize = true;
            this.lblLastSaved.BackColor = System.Drawing.Color.Transparent;
            this.lblLastSaved.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblLastSaved.Location = new System.Drawing.Point(382, 3);
            this.lblLastSaved.Name = "lblLastSaved";
            this.lblLastSaved.Size = new System.Drawing.Size(77, 17);
            this.lblLastSaved.TabIndex = 2;
            this.lblLastSaved.Text = "lblLastSaved";
            this.lblLastSaved.Click += new System.EventHandler(this.FileVersionRow_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblVersion.Location = new System.Drawing.Point(260, 3);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(62, 17);
            this.lblVersion.TabIndex = 2;
            this.lblVersion.Text = "lblVersion";
            this.lblVersion.Click += new System.EventHandler(this.FileVersionRow_Click);
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.BackColor = System.Drawing.Color.Transparent;
            this.lblSize.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblSize.Location = new System.Drawing.Point(169, 3);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(43, 17);
            this.lblSize.TabIndex = 2;
            this.lblSize.Text = "lblSize";
            this.lblSize.Click += new System.EventHandler(this.FileVersionRow_Click);
            // 
            // lblBackupDate
            // 
            this.lblBackupDate.AutoSize = true;
            this.lblBackupDate.BackColor = System.Drawing.Color.Transparent;
            this.lblBackupDate.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupDate.Location = new System.Drawing.Point(6, 3);
            this.lblBackupDate.Name = "lblBackupDate";
            this.lblBackupDate.Size = new System.Drawing.Size(90, 17);
            this.lblBackupDate.TabIndex = 2;
            this.lblBackupDate.Text = "lblBackupDate";
            this.lblBackupDate.Click += new System.EventHandler(this.FileVersionRow_Click);
            // 
            // pnlRight
            // 
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRight.Location = new System.Drawing.Point(526, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(3, 22);
            this.pnlRight.TabIndex = 1;
            // 
            // pnlLeft
            // 
            this.pnlLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(3, 22);
            this.pnlLeft.TabIndex = 0;
            // 
            // FileVersionRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBack);
            this.Name = "FileVersionRow";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(531, 24);
            this.pnlBack.ResumeLayout(false);
            this.pnlBack.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBack;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.Label lblLastSaved;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Label lblBackupDate;

    }
}
