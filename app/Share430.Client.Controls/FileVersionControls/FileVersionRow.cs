using System;
using System.Windows.Forms;
using Share430.Client.Controls.Core;
using Share430.Client.Controls.Core.Events.Args;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Managers.Convert;

namespace Share430.Client.Controls.FileVersionControls
{
    /// <summary>
    /// The file versions row.
    /// </summary>
    public partial class FileVersionRow : UserControl
    {
        /// <summary>
        /// The current version entry.
        /// </summary>
        private readonly FileVersionDto versionEntry;

        /// <summary>
        /// True if current control is selected, othewise false.
        /// </summary>
        private bool selected;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileVersionRow"/> class.
        /// </summary>
        /// <param name="versionEntry">The version entry.</param>
        public FileVersionRow(FileVersionDto versionEntry)
        {
            this.versionEntry = versionEntry;

            this.InitializeComponent();

            this.Load += this.FileVersionRow_Load;
            this.Click += this.FileVersionRow_Click;

            EventPublisher.UIEventManagerInstance.FileVersionChanged += this.UiEventManagerInstanceFileVersionChanged;
        }

        /// <summary>
        /// UIs the event manager instance_ file version canged.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="FileVersionChangedEventArgs"/> instance containing the event data.</param>
        private void UiEventManagerInstanceFileVersionChanged(object sender, FileVersionChangedEventArgs args)
        {
            this.Selected = false;
        }

        /// <summary>
        /// Handles the Click event of the FileVersionRow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FileVersionRow_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnFileVersionCanged(new FileVersionChangedEventArgs(this.versionEntry));
            
            this.Selected = !this.Selected;
        }

        /// <summary>
        /// Handles the Load event of the FileVersionRow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FileVersionRow_Load(object sender, EventArgs e)
        {
            this.lblBackupDate.Text = this.versionEntry.FinishedDate.ToString();
            this.lblLastSaved.Text = this.versionEntry.LastSavedDate.ToString();

            this.lblSize.Text = ConvertData.InformationSizeConvertor.Convert(this.versionEntry.FileSize);
            this.lblVersion.Text = Convert.ToString(this.versionEntry.VersionNumber);
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileVersionRow"/> is selected.
        /// </summary>
        /// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
        public bool Selected
        {
            get { return this.selected; }
            set
            {
                this.selected = value;
                this.SetSelectedState(this.selected);
            }
        }

        /// <summary>
        /// Sets the state of the selected.
        /// </summary>
        /// <param name="isSelected">if set to <c>true</c> [selected].</param>
        private void SetSelectedState(bool isSelected)
        {
            this.pnlBack.BackgroundImage = isSelected ? Properties.Resources.RowBg22 : null;
            this.pnlLeft.BackgroundImage = isSelected ? Properties.Resources.RowLeft22 : null;
            this.pnlRight.BackgroundImage = isSelected ? Properties.Resources.RowRight22 : null;
        }
    }
}
