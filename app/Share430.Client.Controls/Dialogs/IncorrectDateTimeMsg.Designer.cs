﻿namespace Share430.Client.Controls.Dialogs
{
    partial class IncorrectDateTimeMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IncorrectDateTimeMsg));
            this.label3 = new System.Windows.Forms.Label();
            this.btnCancel = new ImageButton.ImageButton();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(20, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(304, 44);
            this.label3.TabIndex = 49;
            this.label3.Text = "The difference between the system time\r\n and the real time is too large!";
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(156, 61);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::Share430.Client.Controls.Properties.Resources.ButtonOk;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(183, 34);
            this.btnCancel.TabIndex = 50;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // IncorrectDateTimeMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 107);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(359, 135);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(359, 135);
            this.Name = "IncorrectDateTimeMsg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Incorrect system time";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private ImageButton.ImageButton btnCancel;
    }
}