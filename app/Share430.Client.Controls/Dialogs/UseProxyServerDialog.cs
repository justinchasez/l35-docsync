﻿using System;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// The uce proxy derver dialog class.
    /// </summary>
    public partial class UseProxyServerDialog : Form
    {
        /// <summary>
        /// Is proxy used
        /// </summary>
        public bool IsProxyUsed { get; private set; }

        /// <summary>
        /// Proxy server address
        /// </summary>
        public string ProxyAddress { get; private set; }

        /// <summary>
        /// Proxy server port
        /// </summary>
        public int ProxyPort { get; private set; }

        /// <summary>
        /// Is ok result
        /// </summary>
        public bool IsOk { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="UseProxyServerDialog"/> class.
        /// </summary>
        /// <param name="isProxyUsed">Is proxy used</param>
        /// <param name="proxyAddress">Proxy server address</param>
        /// <param name="proxyPort">Proxy server port</param>
        public UseProxyServerDialog(bool isProxyUsed, string proxyAddress, int proxyPort)
        {
            this.InitializeComponent();

            this.IsProxyUsed = isProxyUsed;
            this.ProxyAddress = proxyAddress;
            this.ProxyPort = proxyPort;
            this.IsOk = false;

            if (!string.IsNullOrEmpty(proxyAddress))
            {
                this.txtAddress.Text = proxyAddress;
            }

            if (proxyPort > 0)
            {
                this.numPort.Value = Convert.ToDecimal(proxyPort);
            }

            this.checkBoxUseProxy.Checked = this.IsProxyUsed;
        }

        /// <summary>
        /// Proxy used checked changed even handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void CheckBoxUseProxy_CheckedChanged(object sender, EventArgs e)
        {
            this.txtAddress.Enabled = this.checkBoxUseProxy.Checked;
            this.numPort.Enabled = this.checkBoxUseProxy.Checked;
            this.IsProxyUsed = this.checkBoxUseProxy.Checked;
        }

        /// <summary>
        /// Ok click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnOk_Click(object sender, EventArgs e)
        {
            Uri validationUri;

            if (this.IsProxyUsed)
            {
                if (!string.IsNullOrEmpty(this.txtAddress.Text) && Uri.TryCreate(this.txtAddress.Text, UriKind.RelativeOrAbsolute, out validationUri))
                {
                    this.IsProxyUsed = this.checkBoxUseProxy.Checked;
                    this.ProxyAddress = this.txtAddress.Text;
                    this.ProxyPort = Convert.ToInt32(this.numPort.Value);
                    this.IsOk = true;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Incorrect server address. Please review your data.", "Incorrect server address");
                }
            }
            else
            {
                this.IsOk = true;
                this.Close();
            }
        }

        /// <summary>
        /// Cancel click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
