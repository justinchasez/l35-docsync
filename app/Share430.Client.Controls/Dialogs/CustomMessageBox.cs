﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Share430.Client.Controls.Core.Enums;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// Custom message box
    /// </summary>
    public partial class CustomMessageBox : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomMessageBox"/> class.
        /// </summary>
        public CustomMessageBox()
        {
            this.InitializeComponent();
            this.CustomMessageBoxResult = CustomMessageBoxResult.Cancel;
        }

        /// <summary>
        /// Preffered width of the box
        /// </summary>
        private static readonly int PrefferedWidthInChars = 70;

        /// <summary>
        /// result user click
        /// </summary>
        private CustomMessageBoxResult CustomMessageBoxResult { get; set; }

        /// <summary>
        /// Event handler for OK button
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnOkClick(object sender, EventArgs e)
        {
            this.CustomMessageBoxResult = CustomMessageBoxResult.OK;
            this.Close();
        }

        /// <summary>
        /// event handler for YasToAll button
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnYesToAllClick(object sender, EventArgs e)
        {
            this.CustomMessageBoxResult = CustomMessageBoxResult.YesToAll;
            this.Close();
        }

        /// <summary>
        /// Event handler for No buttons
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnNoClick(object sender, EventArgs e)
        {
            this.CustomMessageBoxResult = CustomMessageBoxResult.No;
            this.Close();
        }

        /// <summary>
        /// Event handler for Yes button
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnYesClick(object sender, EventArgs e)
        {
            this.CustomMessageBoxResult = CustomMessageBoxResult.Yes;
            this.Close();
        }

        /// <summary>
        /// Event object
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.CustomMessageBoxResult = CustomMessageBoxResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Show message box modal dialog
        /// </summary>
        /// <param name="parent">parent window</param>
        /// <param name="message">message to be shown</param>
        /// <param name="caption">title text</param>
        /// <param name="buttons">visible buttons</param>
        /// <returns>return result what button was clicked by user</returns>
        public static CustomMessageBoxResult Show(IWin32Window parent, string message, string caption, CustomMessageBoxButtons buttons)
        {
            CustomMessageBox box = new CustomMessageBox();
            box.Text = caption;

            string targetMessage = string.Empty;
            int startIndex = 0;
            int count = message.Length < PrefferedWidthInChars ? message.Length : PrefferedWidthInChars;

            while (startIndex < message.Length)
            {
                if ((count > PrefferedWidthInChars * 0.3) && !string.IsNullOrEmpty(targetMessage))
                {
                    // only for non first parts
                    targetMessage += "\r\n";
                }

                targetMessage += message.Substring(startIndex, count);
                startIndex += count;

                count = message.Length - startIndex < PrefferedWidthInChars ? message.Length - startIndex : PrefferedWidthInChars;
            }

            box.messageText.Text = targetMessage;
//            Size txtsize = box.messageText.GetPreferredSize(box.ClientSize);
//            box.messageText.Width = txtsize.Width;
//            box.messageText.Height = txtsize.Height;

            box.btnOk.Visible = false;
            box.btnCancel.Visible = false;
            box.btnNo.Visible = false;
            box.btnYes.Visible = false;
            box.btnYesToAll.Visible = false;

            int buttonsWidth = 0;
            int buttonsHeight = 0;

            List<ImageButton.ImageButton> imageButtons = new List<ImageButton.ImageButton>();
            switch (buttons)
            {
                case CustomMessageBoxButtons.OK:
                    imageButtons.Add(box.btnOk);
                    break;
                case CustomMessageBoxButtons.OKCancel:
                    imageButtons.Add(box.btnOk);
                    imageButtons.Add(box.btnCancel);
                    break;
                case CustomMessageBoxButtons.RetryCancel:
                    imageButtons.Add(box.btnCancel);
                    break;
                case CustomMessageBoxButtons.YesNo:
                    imageButtons.Add(box.btnYes);
                    imageButtons.Add(box.btnNo);
                    break;
                case CustomMessageBoxButtons.YesNoCancel:
                    imageButtons.Add(box.btnYes);
                    imageButtons.Add(box.btnNo);
                    imageButtons.Add(box.btnCancel);
                    break;
                case CustomMessageBoxButtons.YesYesToAllNoCancel:
                    imageButtons.Add(box.btnYes);
                    imageButtons.Add(box.btnYesToAll);
                    imageButtons.Add(box.btnNo);
                    imageButtons.Add(box.btnCancel);
                    break;
                case CustomMessageBoxButtons.AbortRetryIgnore:
                    break;
            }

            foreach (ImageButton.ImageButton imageButton in imageButtons)
            {
                imageButton.Visible = true;
                buttonsWidth += imageButton.Width + 10;
            }

            buttonsHeight += box.btnYes.Height + 2 * 10;

            box.Width = buttonsWidth > box.messageText.Location.X + box.messageText.Width ? buttonsWidth : box.messageText.Location.X + box.messageText.Width;
            box.Height = buttonsHeight + box.messageText.Location.Y + box.messageText.Height + 20;

            // set buttons at the center
            int cX = (box.Width - buttonsWidth) / 2;
            int cY = box.Bottom - (buttonsHeight + 10);

            foreach (ImageButton.ImageButton imageButton in imageButtons)
            {
                imageButton.Location = new Point(cX, cY);
                cX += imageButton.Width + 10;
            }

            box.ShowDialog(parent);

            return box.CustomMessageBoxResult;
        }

        /// <summary>
        /// event handler for load
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void CustomMessageBox_Load(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                Activate();
            }
        }
    }
}
