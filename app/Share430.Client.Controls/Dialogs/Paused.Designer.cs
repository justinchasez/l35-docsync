namespace Share430.Client.Controls.Dialogs
{
    partial class Paused
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Paused));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblDays = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pauseLabel = new System.Windows.Forms.Label();
            this.btnUnpause = new ImageButton.ImageButton();
            this.btnDismiss = new ImageButton.ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 219);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(413, 70);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.label2.Location = new System.Drawing.Point(501, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 14);
            this.label2.TabIndex = 7;
			this.label2.Text = "� Copyright 2005 - 2010 Share430, Inc";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.linkLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.linkLabel1.LinkArea = new System.Windows.Forms.LinkArea(8, 25);
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.linkLabel1.Location = new System.Drawing.Point(535, 243);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(144, 22);
            this.linkLabel1.TabIndex = 6;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "left in your subscription";
            this.linkLabel1.UseCompatibleTextRendering = true;
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.BackColor = System.Drawing.Color.Transparent;
            this.lblDays.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblDays.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.lblDays.Location = new System.Drawing.Point(548, 226);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(110, 17);
            this.lblDays.TabIndex = 5;
            this.lblDays.Text = "You have 365 days";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(28, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(334, 29);
            this.label3.TabIndex = 8;
			this.label3.Text = "Share430 is currently paused.";
            // 
            // pauseLabel
            // 
            this.pauseLabel.AutoSize = true;
            this.pauseLabel.BackColor = System.Drawing.Color.Transparent;
            this.pauseLabel.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.pauseLabel.ForeColor = System.Drawing.Color.White;
            this.pauseLabel.Location = new System.Drawing.Point(71, 81);
            this.pauseLabel.Name = "pauseLabel";
            this.pauseLabel.Size = new System.Drawing.Size(603, 44);
            this.pauseLabel.TabIndex = 8;
			this.pauseLabel.Text = "Your backup will resume automatically in 1 hour. Click the �Unpause Share430�\r\n" +
				"button below to unpause Share430...";
            // 
            // btnUnpause
            // 
            this.btnUnpause.AllowTransparency = false;
            this.btnUnpause.AnimatePress = true;
            this.btnUnpause.BackColor = System.Drawing.Color.Transparent;
            this.btnUnpause.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnUnpause.HoverImage = null;
            this.btnUnpause.Location = new System.Drawing.Point(489, 144);
            this.btnUnpause.Name = "btnUnpause";
            this.btnUnpause.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnUnpause.NormalImage")));
            this.btnUnpause.PressedImage = null;
            this.btnUnpause.Size = new System.Drawing.Size(190, 50);
            this.btnUnpause.TabIndex = 9;
            this.btnUnpause.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnUnpause.Click += new System.EventHandler(this.BtnUnpauseClick);
            // 
            // btnDismiss
            // 
            this.btnDismiss.AllowTransparency = false;
            this.btnDismiss.AnimatePress = true;
            this.btnDismiss.BackColor = System.Drawing.Color.Transparent;
            this.btnDismiss.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnDismiss.HoverImage = null;
            this.btnDismiss.Location = new System.Drawing.Point(372, 146);
            this.btnDismiss.Name = "btnDismiss";
            this.btnDismiss.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnDismiss.NormalImage")));
            this.btnDismiss.PressedImage = null;
            this.btnDismiss.Size = new System.Drawing.Size(111, 50);
            this.btnDismiss.TabIndex = 9;
            this.btnDismiss.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnDismiss.Click += new System.EventHandler(this.BtnDismissClick);
            // 
            // Paused
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(732, 301);
            this.Controls.Add(this.btnUnpause);
            this.Controls.Add(this.btnDismiss);
            this.Controls.Add(this.pauseLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.lblDays);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Paused";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Share430 InfoCenter  -  Paused!";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label pauseLabel;
        private ImageButton.ImageButton btnDismiss;
        private ImageButton.ImageButton btnUnpause;

    }
}