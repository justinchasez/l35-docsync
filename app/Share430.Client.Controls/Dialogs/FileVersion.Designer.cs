namespace Share430.Client.Controls.Dialogs
{
    partial class FileVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileVersion));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblLastSaved = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblBackupDate = new System.Windows.Forms.Label();
            this.btnOk = new ImageButton.ImageButton();
            this.btnCancel = new ImageButton.ImageButton();
            this.btnBrowse = new ImageButton.ImageButton();
            this.txtRestoreFilePath = new System.Windows.Forms.TextBox();
            this.pnlVersions = new System.Windows.Forms.Panel();
            this.lblBackupFilePath = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.sfdRestoreFile = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnBrowse);
            this.panel1.Controls.Add(this.txtRestoreFilePath);
            this.panel1.Controls.Add(this.pnlVersions);
            this.panel1.Controls.Add(this.lblBackupFilePath);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(7, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(606, 300);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.lblLastSaved);
            this.panel4.Controls.Add(this.lblVersion);
            this.panel4.Controls.Add(this.lblSize);
            this.panel4.Controls.Add(this.lblBackupDate);
            this.panel4.Location = new System.Drawing.Point(27, 53);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(555, 20);
            this.panel4.TabIndex = 6;
            // 
            // lblLastSaved
            // 
            this.lblLastSaved.AutoSize = true;
            this.lblLastSaved.BackColor = System.Drawing.Color.Transparent;
            this.lblLastSaved.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblLastSaved.Location = new System.Drawing.Point(383, 2);
            this.lblLastSaved.Name = "lblLastSaved";
            this.lblLastSaved.Size = new System.Drawing.Size(91, 17);
            this.lblLastSaved.TabIndex = 5;
            this.lblLastSaved.Text = "Last Saved On";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblVersion.Location = new System.Drawing.Point(261, 2);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(52, 17);
            this.lblVersion.TabIndex = 6;
            this.lblVersion.Text = "Version";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.BackColor = System.Drawing.Color.Transparent;
            this.lblSize.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblSize.Location = new System.Drawing.Point(170, 2);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(31, 17);
            this.lblSize.TabIndex = 3;
            this.lblSize.Text = "Size";
            // 
            // lblBackupDate
            // 
            this.lblBackupDate.AutoSize = true;
            this.lblBackupDate.BackColor = System.Drawing.Color.Transparent;
            this.lblBackupDate.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupDate.Location = new System.Drawing.Point(7, 2);
            this.lblBackupDate.Name = "lblBackupDate";
            this.lblBackupDate.Size = new System.Drawing.Size(83, 17);
            this.lblBackupDate.TabIndex = 4;
            this.lblBackupDate.Text = "Backup Date";
            // 
            // btnOk
            // 
            this.btnOk.AllowTransparency = false;
            this.btnOk.AnimatePress = true;
            this.btnOk.BackColor = System.Drawing.Color.Transparent;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnOk.HoverImage = null;
            this.btnOk.Location = new System.Drawing.Point(248, 256);
            this.btnOk.Name = "btnOk";
            this.btnOk.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnOk.NormalImage")));
            this.btnOk.PressedImage = null;
            this.btnOk.Size = new System.Drawing.Size(164, 33);
            this.btnOk.TabIndex = 5;
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(418, 256);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.AllowTransparency = false;
            this.btnBrowse.AnimatePress = true;
            this.btnBrowse.BackColor = System.Drawing.Color.Transparent;
            this.btnBrowse.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBrowse.HoverImage = null;
            this.btnBrowse.Location = new System.Drawing.Point(418, 217);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnBrowse.NormalImage")));
            this.btnBrowse.PressedImage = null;
            this.btnBrowse.Size = new System.Drawing.Size(164, 33);
            this.btnBrowse.TabIndex = 5;
            this.btnBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBrowse.Click += new System.EventHandler(this.ButtonBrowse_Click);
            // 
            // txtRestoreFilePath
            // 
            this.txtRestoreFilePath.BackColor = System.Drawing.SystemColors.Window;
            this.txtRestoreFilePath.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRestoreFilePath.Location = new System.Drawing.Point(27, 220);
            this.txtRestoreFilePath.Name = "txtRestoreFilePath";
            this.txtRestoreFilePath.Size = new System.Drawing.Size(385, 25);
            this.txtRestoreFilePath.TabIndex = 4;
            this.txtRestoreFilePath.TextChanged += new System.EventHandler(this.TxtRestoreFilePath_TextChanged);
            // 
            // pnlVersions
            // 
            this.pnlVersions.AutoScroll = true;
            this.pnlVersions.BackColor = System.Drawing.Color.White;
            this.pnlVersions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVersions.Location = new System.Drawing.Point(27, 73);
            this.pnlVersions.Name = "pnlVersions";
            this.pnlVersions.Size = new System.Drawing.Size(555, 140);
            this.pnlVersions.TabIndex = 3;
            // 
            // lblBackupFilePath
            // 
            this.lblBackupFilePath.BackColor = System.Drawing.Color.Transparent;
            this.lblBackupFilePath.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupFilePath.Location = new System.Drawing.Point(126, 10);
            this.lblBackupFilePath.Name = "lblBackupFilePath";
            this.lblBackupFilePath.Size = new System.Drawing.Size(456, 17);
            this.lblBackupFilePath.TabIndex = 2;
            this.lblBackupFilePath.Text = "lblBackupFilePath";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(24, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select version:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(24, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Backed-up from:";
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(588, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(18, 300);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(18, 300);
            this.panel2.TabIndex = 0;
            // 
            // FileVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(619, 313);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileVersion";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "File Versions: ";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtRestoreFilePath;
        private System.Windows.Forms.Panel pnlVersions;
        private System.Windows.Forms.Label lblBackupFilePath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ImageButton.ImageButton btnBrowse;
        private ImageButton.ImageButton btnOk;
        private ImageButton.ImageButton btnCancel;
        private System.Windows.Forms.SaveFileDialog sfdRestoreFile;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblLastSaved;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Label lblBackupDate;
    }
}