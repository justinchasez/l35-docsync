﻿using System;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// Incorrect Date and Time message
    /// </summary>
    public partial class IncorrectDateTimeMsg : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncorrectDateTimeMsg"/> class.
        /// </summary>
        public IncorrectDateTimeMsg()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
