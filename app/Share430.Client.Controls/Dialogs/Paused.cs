using System;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// The paused form.
    /// </summary>
    public partial class Paused : Form
    {
        /// <summary>
        /// Text for days count
        /// </summary>
        private const string DAYS_FORMAT = "You have {0} days";

        /// <summary>
        /// Initializes a new instance of the <see cref="Paused"/> class.
        /// </summary>
        /// <param name="pauseValue">The pause value.</param>
        /// <param name="days">Count of days</param>
        public Paused(int pauseValue, int days)
        {
            this.InitializeComponent();

            this.lblDays.Text = string.Format(DAYS_FORMAT, days < 0 ? 0 : days);

            //TimeSpan stillPaused = new TimeSpan(0, 0, /*RemotingManager.Instance.ProxyManager.AppManger.GetPause()*/ pauseValue, 0);
            DateTime stillPaused = DateTime.Now.AddMinutes(pauseValue);
            
            this.pauseLabel.Text =
                string.Format(
					"Your backup will resume automatically in {0}. Click the �Unpause Share430�\r\n button below to unpause Share430...",
                    stillPaused.ToShortTimeString());
        }

        /// <summary>
        /// Event handler unpause button click
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnUnpauseClick(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Event handler Dismiss Button Click
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void BtnDismissClick(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
