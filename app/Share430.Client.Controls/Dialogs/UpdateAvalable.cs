﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// Update avalable message
    /// </summary>
    public partial class UpdateAvalable : Form
    {
        /// <summary>
        /// Link for download update
        /// </summary>
        private readonly string link;

        /// <summary>
        /// Initializes a new instance of the UpdateAvalable class.
        /// </summary>
        /// <param name="link">Link for download update</param>
        public UpdateAvalable(string link)
        {
            this.link = link;
            this.InitializeComponent();
        }

        /// <summary>
        /// Button download click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Argements of the event</param>
        private void BtnDownload_Click(object sender, EventArgs e)
        {
            Process.Start(this.link);
            this.Close();
        }

        /// <summary>
        /// Button cancel click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Argements of the event</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
