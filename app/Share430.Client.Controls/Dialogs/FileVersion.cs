using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Share430.Client.Controls.Core;
using Share430.Client.Controls.Core.Events.Args;
using Share430.Client.Controls.FileVersionControls;
using Share430.Client.Core.Entities.Files;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// The file version dialog.
    /// </summary>
    public partial class FileVersion : Form
    {
        /// <summary>
        /// The file info entry.
        /// </summary>
         private readonly FileInfoDto entry;
        //private readonly RestoreFileResult entry;

        /// <summary>
        /// The mappings controls.
        /// </summary>
        private readonly List<FileVersionRow> mapingsControlList = new List<FileVersionRow>();

        /// <summary>
        /// The selected file version.
        /// </summary>
         public FileVersionDto SelectedVersion { get; private set; }

        /// <summary>
        /// Gets the save path.
        /// </summary>
        /// <value>The save path.</value>
        public string SavePath { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileVersion"/> class.
        /// </summary>
        /// <param name="entry">The file entry.</param>
        public FileVersion(FileInfoDto entry)
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.entry = entry;

            this.InitializeComponent();
            this.Load += this.FileVersion_Load;
            EventPublisher.UIEventManagerInstance.FileVersionChanged += this.UIEventManagerInstance_FileVersionChanged;
        }

        /// <summary>
        /// UIs the event manager instance_ file version canged.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="FileVersionChangedEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_FileVersionChanged(object sender, FileVersionChangedEventArgs args)
        {
            this.SelectedVersion = args.Version;
        }

        /// <summary>
        /// Handles the Load event of the FileVersion control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FileVersion_Load(object sender, EventArgs e)
        {
            if ((this.entry != null) && (this.entry.FileVersions.Count > 1))
            {
                this.sfdRestoreFile.Filter = String.Format("{0} files | *{0}", Path.GetExtension(this.entry.FileName));
                this.sfdRestoreFile.FileName = this.entry.FileName;
                this.txtRestoreFilePath.Text = this.entry.FileName;
                this.SavePath = this.entry.FileName;

                this.Text += Path.GetFileName(this.entry.FileName);
                this.lblBackupFilePath.Text = this.entry.FileName;
                ToolTip tt = new ToolTip();
                tt.SetToolTip(this.lblBackupFilePath, this.entry.FileName);

                foreach (FileVersionDto currentVersion in this.entry.FileVersions)
                {
                    FileVersionRow versionRow = new FileVersionRow(currentVersion) { Dock = DockStyle.Top };
                    this.mapingsControlList.Add(versionRow);
                    this.pnlVersions.Controls.Add(versionRow);
                }

                ((FileVersionRow) this.pnlVersions.Controls[0]).Selected = true;
                this.SelectedVersion = this.entry.FileVersions[0];
            }
            else
            {
                this.Close();
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonBrowse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBrowse_Click(object sender, EventArgs e)
        {
            if (this.sfdRestoreFile.ShowDialog() == DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(this.sfdRestoreFile.FileName))
                {
                    this.txtRestoreFilePath.Text = this.sfdRestoreFile.FileName;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(this.SavePath))
            {
                MessageBox.Show("Path to restore not selected.");
                return;
            }

            if (Path.GetFileName(this.SavePath).IndexOfAny(Path.GetInvalidFileNameChars()) != -1 || Path.GetDirectoryName(this.SavePath).IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                MessageBox.Show("File path exist incorrect symbols.", "Invalid file path");
                return;
            }

            if (this.SavePath.IndexOf(":\\") != 1)
            {
                MessageBox.Show("Please specify logical disk", "Invalid file path");
                return;
            }

            if (!Directory.Exists(Path.GetDirectoryName(this.SavePath)))
            {
                try
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(this.SavePath));
                }
                catch (Exception)
                {
                    MessageBox.Show("Can not create directory for file. Please verify file path.", "Invalid file path");
                    return;
                }
            }
            else
            {
                if (Directory.Exists(this.SavePath))
                {
                    MessageBox.Show("Incorrect file path. You already have folder with same name.", "Invalid file path");
                    return;
                }
            }

            // dialog returns SavePath and SelectedVersion
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Restore file path changed
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void TxtRestoreFilePath_TextChanged(object sender, EventArgs e)
        {
            this.SavePath = this.txtRestoreFilePath.Text.Trim();
            this.sfdRestoreFile.FileName = this.txtRestoreFilePath.Text.Trim();
        }
    }
}
