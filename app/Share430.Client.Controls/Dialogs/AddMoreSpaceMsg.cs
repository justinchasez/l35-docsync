﻿using System.Diagnostics;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// Add more space message
    /// </summary>
    public partial class AddMoreSpaceMsg : Form
    {
        /// <summary>
        /// The message format
        /// </summary>
        private const string USED_SPACE_FORMAT = "You have {0} of data in your backup.";

        /// <summary>
        /// Link to membership plan
        /// </summary>
        private string link;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddMoreSpaceMsg"/> class.
        /// </summary>
        /// <param name="sizeDataInBackup">Size of the backup</param>
        /// <param name="linkToMembershipPlan">Link to membership plan</param>
        public AddMoreSpaceMsg(string sizeDataInBackup, string linkToMembershipPlan)
        {
            this.InitializeComponent();
            this.lblSpace.Text = string.Format(USED_SPACE_FORMAT, sizeDataInBackup);
            this.link = linkToMembershipPlan;
        }

        /// <summary>
        /// Virtual Drive Link Clicked handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void LinkVirtualDrive_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("explorer", "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\"");
            this.Close();
        }

        /// <summary>
        /// Membership Plan Link Clicked handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void LinkMembershipPlan_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(this.link);
            this.Close();
        }

        /// <summary>
        /// Cancel Click handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void BtnCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
