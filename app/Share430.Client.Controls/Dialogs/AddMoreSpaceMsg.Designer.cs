﻿namespace Share430.Client.Controls.Dialogs
{
    partial class AddMoreSpaceMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddMoreSpaceMsg));
            this.lblSpace = new System.Windows.Forms.Label();
            this.linkVirtualDrive = new System.Windows.Forms.LinkLabel();
            this.linkMembershipPlan = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCancel = new ImageButton.ImageButton();
            this.SuspendLayout();
            // 
            // lblSpace
            // 
            this.lblSpace.BackColor = System.Drawing.Color.Transparent;
            this.lblSpace.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblSpace.ForeColor = System.Drawing.Color.White;
            this.lblSpace.Location = new System.Drawing.Point(12, 9);
            this.lblSpace.Name = "lblSpace";
            this.lblSpace.Size = new System.Drawing.Size(434, 22);
            this.lblSpace.TabIndex = 47;
            this.lblSpace.Text = "You have 100 GB of data in your backup.";
            // 
            // linkVirtualDrive
            // 
            this.linkVirtualDrive.AutoSize = true;
            this.linkVirtualDrive.BackColor = System.Drawing.Color.Transparent;
            this.linkVirtualDrive.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.linkVirtualDrive.ForeColor = System.Drawing.Color.White;
            this.linkVirtualDrive.LinkColor = System.Drawing.Color.White;
            this.linkVirtualDrive.Location = new System.Drawing.Point(30, 135);
            this.linkVirtualDrive.Name = "linkVirtualDrive";
            this.linkVirtualDrive.Size = new System.Drawing.Size(204, 22);
            this.linkVirtualDrive.TabIndex = 48;
            this.linkVirtualDrive.TabStop = true;
            this.linkVirtualDrive.Text = "Browse my backed-up files";
            this.linkVirtualDrive.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkVirtualDrive_LinkClicked);
            // 
            // linkMembershipPlan
            // 
            this.linkMembershipPlan.AutoSize = true;
            this.linkMembershipPlan.BackColor = System.Drawing.Color.Transparent;
            this.linkMembershipPlan.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.linkMembershipPlan.ForeColor = System.Drawing.Color.White;
            this.linkMembershipPlan.LinkArea = new System.Windows.Forms.LinkArea(0, 22);
            this.linkMembershipPlan.LinkColor = System.Drawing.Color.White;
            this.linkMembershipPlan.Location = new System.Drawing.Point(30, 170);
            this.linkMembershipPlan.Name = "linkMembershipPlan";
            this.linkMembershipPlan.Size = new System.Drawing.Size(180, 22);
            this.linkMembershipPlan.TabIndex = 48;
            this.linkMembershipPlan.TabStop = true;
            this.linkMembershipPlan.Text = "Upgrade my space limit";
            this.linkMembershipPlan.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkMembershipPlan_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(359, 22);
            this.label3.TabIndex = 47;
            this.label3.Text = "Please, go to upgrade your space limit or delete ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(298, 22);
            this.label4.TabIndex = 47;
            this.label4.Text = "unnecessary files from the backup drive";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(12, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(236, 22);
            this.label5.TabIndex = 47;
            this.label5.Text = "to be able to backup more files";
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(337, 229);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(109, 34);
            this.btnCancel.TabIndex = 46;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // AddMoreSpaceMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(458, 275);
            this.Controls.Add(this.linkMembershipPlan);
            this.Controls.Add(this.linkVirtualDrive);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSpace);
            this.Controls.Add(this.btnCancel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddMoreSpaceMsg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add more space";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton.ImageButton btnCancel;
        private System.Windows.Forms.Label lblSpace;
        private System.Windows.Forms.LinkLabel linkVirtualDrive;
        private System.Windows.Forms.LinkLabel linkMembershipPlan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}