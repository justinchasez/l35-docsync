﻿namespace Share430.Client.Controls.Dialogs
{
    partial class CustomMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomMessageBox));
            this.messageText = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new ImageButton.ImageButton();
            this.btnNo = new ImageButton.ImageButton();
            this.btnOk = new ImageButton.ImageButton();
            this.btnYesToAll = new ImageButton.ImageButton();
            this.btnYes = new ImageButton.ImageButton();
            this.SuspendLayout();
            // 
            // messageText
            // 
            this.messageText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.messageText.AutoSize = true;
            this.messageText.BackColor = System.Drawing.Color.Transparent;
            this.messageText.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.messageText.ForeColor = System.Drawing.Color.White;
            this.messageText.Location = new System.Drawing.Point(19, 9);
            this.messageText.Name = "messageText";
            this.messageText.Size = new System.Drawing.Size(106, 22);
            this.messageText.TabIndex = 10;
            this.messageText.Text = "Test message";
            this.messageText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 91);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(620, 0);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(499, 56);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(109, 34);
            this.btnCancel.TabIndex = 45;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // btnNo
            // 
            this.btnNo.AllowTransparency = false;
            this.btnNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNo.AnimatePress = true;
            this.btnNo.BackColor = System.Drawing.Color.Transparent;
            this.btnNo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNo.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNo.HoverImage = null;
            this.btnNo.Location = new System.Drawing.Point(386, 56);
            this.btnNo.Name = "btnNo";
            this.btnNo.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnNo.NormalImage")));
            this.btnNo.PressedImage = null;
            this.btnNo.Size = new System.Drawing.Size(107, 34);
            this.btnNo.TabIndex = 44;
            this.btnNo.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnNo.Click += new System.EventHandler(this.BtnNoClick);
            // 
            // btnOk
            // 
            this.btnOk.AllowTransparency = false;
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.AnimatePress = true;
            this.btnOk.BackColor = System.Drawing.Color.Transparent;
            this.btnOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnOk.HoverImage = null;
            this.btnOk.Location = new System.Drawing.Point(18, 56);
            this.btnOk.Name = "btnOk";
            this.btnOk.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnOk.NormalImage")));
            this.btnOk.PressedImage = null;
            this.btnOk.Size = new System.Drawing.Size(107, 34);
            this.btnOk.TabIndex = 43;
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnOk.Click += new System.EventHandler(this.BtnOkClick);
            // 
            // btnYesToAll
            // 
            this.btnYesToAll.AllowTransparency = false;
            this.btnYesToAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYesToAll.AnimatePress = true;
            this.btnYesToAll.BackColor = System.Drawing.Color.Transparent;
            this.btnYesToAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnYesToAll.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnYesToAll.HoverImage = null;
            this.btnYesToAll.Location = new System.Drawing.Point(255, 56);
            this.btnYesToAll.Name = "btnYesToAll";
            this.btnYesToAll.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnYesToAll.NormalImage")));
            this.btnYesToAll.PressedImage = null;
            this.btnYesToAll.Size = new System.Drawing.Size(125, 34);
            this.btnYesToAll.TabIndex = 42;
            this.btnYesToAll.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnYesToAll.Click += new System.EventHandler(this.BtnYesToAllClick);
            // 
            // btnYes
            // 
            this.btnYes.AllowTransparency = false;
            this.btnYes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnYes.AnimatePress = true;
            this.btnYes.BackColor = System.Drawing.Color.Transparent;
            this.btnYes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnYes.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnYes.HoverImage = null;
            this.btnYes.Location = new System.Drawing.Point(131, 56);
            this.btnYes.Name = "btnYes";
            this.btnYes.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnYes.NormalImage")));
            this.btnYes.PressedImage = null;
            this.btnYes.Size = new System.Drawing.Size(118, 34);
            this.btnYes.TabIndex = 41;
            this.btnYes.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnYes.Click += new System.EventHandler(this.BtnYesClick);
            // 
            // CustomMessageBox
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(620, 91);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnNo);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnYesToAll);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.messageText);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CustomMessageBox";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.CustomMessageBox_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label messageText;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private ImageButton.ImageButton btnYes;
        private ImageButton.ImageButton btnYesToAll;
        private ImageButton.ImageButton btnOk;
        private ImageButton.ImageButton btnNo;
        private ImageButton.ImageButton btnCancel;
    }
}