﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Share430.Client.Controls.Dialogs
{
    /// <summary>
    /// The trial notification window class
    /// </summary>
    public partial class TrialNotificationWindow : Form
    {
        /// <summary>
        /// Text format for mesage
        /// </summary>
        private const string NOT_EXPIRED_MESSAGE_FORMAT = "Your trial period will expire in {0} day(s).";

        /// <summary>
        /// Text for message
        /// </summary>
        private const string EXPIRED_MESSAGE = "Your trial period has expired.";

        /// <summary>
        /// Text for days count
        /// </summary>
        private const string DAYS_FORMAT = "You have {0} days";

        /// <summary>
        /// Link to membership plan
        /// </summary>
        private readonly string link;

        /// <summary>
        /// Set parameters of window
        /// </summary>
        /// <param name="isExpired">Is computer expired</param>
        /// <param name="days">How many days left</param>
        public void SetParameters(bool isExpired, int days)
        {
            this.lblText.Text = isExpired ? EXPIRED_MESSAGE : string.Format(NOT_EXPIRED_MESSAGE_FORMAT, days);

            this.lblDays.Text = string.Format(DAYS_FORMAT, days);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TrialNotificationWindow"/> class.
        /// </summary>
        /// <param name="linkToMembership">Link to membership plan</param>
        public TrialNotificationWindow(string linkToMembership)
        {
            this.InitializeComponent();

            int pointX = SystemInformation.WorkingArea.Right - this.Width;
            int pointY = SystemInformation.WorkingArea.Bottom - this.Height;
            
            this.Location = new Point(pointX, pointY);

            this.link = linkToMembership;
        }

        /// <summary>
        /// Prolong click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnProlong_Click(object sender, EventArgs e)
        {
            Process.Start(this.link);
            this.Visible = false;
        }

        /// <summary>
        /// Cancel click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
