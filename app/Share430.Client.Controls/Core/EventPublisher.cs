using Share430.Client.Controls.Core.Events;
using Share430.Client.Controls.Core.Events.Interface;

namespace Share430.Client.Controls.Core
{
    /// <summary>
    /// The main event manager
    /// </summary>
    internal static class EventPublisher
    {
        #region Event managers fields

        /// <summary>
        /// The event manger field for UI.
        /// </summary>
        private static IUIEventManager interfaceEventManager;

        #endregion

        /// <summary>
        /// Gets the UI event manager instance.
        /// </summary>
        /// <value>The UI event manager instance.</value>
        public static IUIEventManager UIEventManagerInstance
        {
            get
            {
                if (interfaceEventManager == null)
                {
                    Initialize();
                }

                return interfaceEventManager;
            }
        }

        /// <summary>
        /// Initializes nested event managers.
        /// </summary>
        private static void Initialize()
        {
            interfaceEventManager = new UIEventManager();
        }
    }
}
