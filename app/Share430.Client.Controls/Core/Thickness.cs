using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Share430.Client.Controls.Core
{
    /// <summary>
    /// Contains the thickness information.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    [TypeConverter(typeof(ThicknessConvertor))]    
    public struct Thickness
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Thickness"/> struct.
        /// </summary>
        /// <param name="all">All sides width.</param>
        public Thickness(int all) : this()
        {
            this.All = all;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Thickness"/> struct.
        /// </summary>
        /// <param name="top">The top width.</param>
        /// <param name="right">The right width.</param>
        /// <param name="bottom">The bottom width.</param>
        /// <param name="left">The left width.</param>
        public Thickness(int top, int right, int bottom, int left) : this()
        {
            this.Top = top;
            this.Right = right;
            this.Bottom = bottom;
            this.Left = left;
        }

        /// <summary>
        /// Sets all corners radius.
        /// </summary>
        public int All
        {
            set
            {
                this.Top = this.Right = this.Left = this.Bottom = value;
            }
        }

        /// <summary>
        /// Gets or sets the top.
        /// </summary>
        public int Top { get; set; }

        /// <summary>
        /// Gets or sets the right.
        /// </summary>
        public int Right { get; set; }

        /// <summary>
        /// Gets or sets the bottom.
        /// </summary>
        public int Bottom { get; set; }

        /// <summary>
        /// Gets or sets the left.
        /// </summary>
        public int Left { get; set; }
    }
}