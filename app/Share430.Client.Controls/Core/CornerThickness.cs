using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Share430.Client.Controls.Core
{
    /// <summary>
    /// The class whtch contains corner thickness data.
    /// </summary>
    [Serializable]
    [StructLayout(LayoutKind.Sequential)]
    [TypeConverter(typeof(CornerThicknessConvertor))] 
    public struct CornerThickness
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CornerThickness"/> struct.
        /// </summary>
        /// <param name="allCorners">All corners.</param>
        public CornerThickness(int allCorners) : this()
        {
            this.All = allCorners;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CornerThickness"/> struct.
        /// </summary>
        /// <param name="topLeft">The top left.</param>
        /// <param name="topRight">The top right.</param>
        /// <param name="bottomRight">The bottom right.</param>
        /// <param name="bottomLeft">The bottom left.</param>
        public CornerThickness(int topLeft, int topRight, int bottomRight, int bottomLeft) : this()
        {
            this.TopLeft = topLeft;
            this.TopRight = topRight;
            this.BottomLeft = bottomLeft;
            this.BottomRight = bottomRight;
        }

        /// <summary>
        /// Sets all corners radius.
        /// </summary>
        public int All
        {
            set
            {
                this.TopLeft = this.TopRight = this.BottomLeft = this.BottomRight = value;
            }
        }

        /// <summary>
        /// Gets or sets the top left.
        /// </summary>
        [Browsable(true)]
        [NotifyParentProperty(true)]
        public int TopLeft { get; set; }

        /// <summary>
        /// Gets or sets the top right.
        /// </summary>
        /// <value>The top right.</value>
        [Browsable(true)]
        [NotifyParentProperty(true)]
        public int TopRight { get; set; }

        /// <summary>
        /// Gets or sets the bottom left.
        /// </summary>
        /// <value>The bottom left.</value>
        [Browsable(true)]
        [NotifyParentProperty(true)]
        public int BottomLeft { get; set; }

        /// <summary>
        /// Gets or sets the bottom right.
        /// </summary>
        /// <value>The bottom right.</value>
        [Browsable(true)]
        [NotifyParentProperty(true)]
        public int BottomRight { get; set; }
    }
}