using System;
using Share430.Client.Controls.Core.Events.Args;
using Share430.Client.Controls.Core.Events.Interface;

namespace Share430.Client.Controls.Core.Events
{
    /// <summary>
    /// Contains events for UI.
    /// </summary>
    public class UIEventManager : IUIEventManager
    {
        /// <summary>
        /// Occurs when [file version canged].
        /// </summary>
        public event EventHandler<FileVersionChangedEventArgs> FileVersionChanged;

        /// <summary>
        /// Raises the <see cref="E:FileVersionChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="FileVersionChangedEventArgs"/> instance containing the event data.</param>
        public void OnFileVersionCanged(FileVersionChangedEventArgs args)
        {
            EventHandler<FileVersionChangedEventArgs> handler = this.FileVersionChanged;
            if (handler != null)
            {
                handler(null, args);
            }
        }
    }
}