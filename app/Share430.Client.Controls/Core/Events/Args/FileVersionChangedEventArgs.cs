using System;
using Share430.Client.Core.Entities.Files;

namespace Share430.Client.Controls.Core.Events.Args
{
    /// <summary>
    /// The event argument for the FileVersionChanged event.
    /// </summary>
    public class FileVersionChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileVersionChangedEventArgs"/> class.
        /// </summary>
        public FileVersionChangedEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileVersionChangedEventArgs"/> class.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public FileVersionChangedEventArgs(FileVersionDto entry)
        {
            this.Version = entry;
        }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public FileVersionDto Version { get; set; }
    }
}
