using System;
using Share430.Client.Controls.Core.Events.Args;

namespace Share430.Client.Controls.Core.Events.Interface
{
    /// <summary>
    /// The event manager for UI.
    /// </summary>
    public interface IUIEventManager
    {
        /// <summary>
        /// Occurs when [file version canged].
        /// </summary>
        event EventHandler<FileVersionChangedEventArgs> FileVersionChanged;

        /// <summary>
        /// Raises the <see cref="E:FileVersionChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="FileVersionChangedEventArgs"/> instance containing the event data.</param>
        void OnFileVersionCanged(FileVersionChangedEventArgs args);
    }
}