namespace Share430.Client.Controls.Core.Enums
{
    /// <summary>
    /// Results for custom message box
    /// </summary>
    public enum CustomMessageBoxResult
    {
        /// <summary>
        /// None value
        /// </summary>
        None,

        /// <summary>
        /// OK button pressed
        /// </summary>
        OK,

        /// <summary>
        /// Cancel button pressed
        /// </summary>
        Cancel,

        /// <summary>
        /// Abort button pressed
        /// </summary>
        Abort,

        /// <summary>
        /// Retry button pressed
        /// </summary>
        Retry,

        /// <summary>
        /// Ignore button pressed
        /// </summary>
        Ignore,

        /// <summary>
        /// Yes button pressed
        /// </summary>
        Yes,

        /// <summary>
        /// No button pressed
        /// </summary>
        No,

        /// <summary>
        /// YesToAll button pressed
        /// </summary>
        YesToAll
    }
}