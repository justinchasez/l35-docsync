namespace Share430.Client.Controls.Core.Enums
{
    /// <summary>
    /// Enum specify buttons to be shown on the message box
    /// </summary>
    public enum CustomMessageBoxButtons
    {
        /// <summary>
        /// Only OK button is visible
        /// </summary>
        OK,

        /// <summary>
        /// Ok and Cancel buttons are visible
        /// </summary>
        OKCancel,

        /// <summary>
        /// Abort, Retry and Ignore buttons are visible
        /// not supported at this time
        /// </summary>
        AbortRetryIgnore,

        /// <summary>
        /// Yes, No and Cancel buttons are visible
        /// </summary>
        YesNoCancel,

        /// <summary>
        /// Yes and No buttons are visible
        /// </summary>
        YesNo,

        /// <summary>
        /// Retry and Cancel buttons are visible
        /// not supported at this time
        /// </summary>
        RetryCancel,

        /// <summary>
        /// Yes, YesToAll, No and Cancel buttons are visible
        /// </summary>
        YesYesToAllNoCancel
    }
}