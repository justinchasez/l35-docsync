using System;
using System.ComponentModel;
using System.Globalization;

namespace Share430.Client.Controls.Core
{
    /// <summary>
    /// Converts the string to cor
    /// </summary>
    public class CornerThicknessConvertor : ExpandableObjectConverter
    {
        /// <summary>
        /// Returns whether this converter can convert the object to the specified type, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="destinationType">A <see cref="T:System.Type"/> that represents the type you want to convert to.</param>
        /// <returns>
        /// true if this converter can perform the conversion; otherwise, false.
        /// </returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, System.Type destinationType)
        {
            if (destinationType == typeof(CornerThickness))
            {
                return true;
            }

            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// Returns whether this converter can convert an object of the given type to the type of this converter, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="sourceType">A <see cref="T:System.Type"/> that represents the type you want to convert from.</param>
        /// <returns>
        /// true if this converter can perform the conversion; otherwise, false.
        /// </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Converts the given value object to the specified type, using the specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="culture">A <see cref="T:System.Globalization.CultureInfo"/>. If null is passed, the current culture is assumed.</param>
        /// <param name="value">The <see cref="T:System.Object"/> to convert.</param>
        /// <param name="destinationType">The <see cref="T:System.Type"/> to convert the <paramref name="value"/> parameter to.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/> that represents the converted value.
        /// </returns>
        /// <exception cref="T:System.ArgumentNullException">
        /// The <paramref name="destinationType"/> parameter is null.
        /// </exception>
        /// <exception cref="T:System.NotSupportedException">
        /// The conversion cannot be performed.
        /// </exception>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, System.Type destinationType)
        {
            if (destinationType == typeof(string) && value is CornerThickness)
            {
                CornerThickness thickness = (CornerThickness)value;

                return String.Format("{0}; {1}; {2}; {3}", thickness.TopRight, thickness.BottomRight, thickness.BottomLeft, thickness.TopLeft);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Converts the given object to the type of this converter, using the specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo"/> to use as the current culture.</param>
        /// <param name="value">The <see cref="T:System.Object"/> to convert.</param>
        /// <returns>
        /// An <see cref="T:System.Object"/> that represents the converted value.
        /// </returns>
        /// <exception cref="T:System.NotSupportedException">
        /// The conversion cannot be performed.
        /// </exception>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                string thicknessString = Convert.ToString(value);
                if (!string.IsNullOrEmpty(thicknessString))
                {
                    CornerThickness thickness = new CornerThickness();

                    if (thicknessString.Contains(";"))
                    {
                        string[] thicknessParts = thicknessString.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                        if (thicknessParts.Length == 4)
                        {
                            thickness.TopRight = Convert.ToInt32(thicknessParts[0]);
                            thickness.BottomRight = Convert.ToInt32(thicknessParts[1]);
                            thickness.BottomLeft = Convert.ToInt32(thicknessParts[2]);
                            thickness.TopLeft = Convert.ToInt32(thicknessParts[3]);
                        }
                        else
                        {
                            throw new ArgumentException("Can not convert '" + thicknessString + "' to type CornerThickness");
                        }
                    }
                    else
                    {
                        thickness.All = Convert.ToInt32(thicknessString);
                    }

                    return thickness;
                }
                else
                {
                    throw new ArgumentException();
                }
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
}