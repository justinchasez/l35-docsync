using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Share430.Client.Controls.RadioButtonList
{
    /// <summary>
    /// The radio button list box control.
    /// </summary>
    public sealed class RadioListBox : ListBox
    {
        /// <summary>
        /// The texxt align.
        /// </summary>
        private readonly StringFormat align;

        /// <summary>
        /// The transparency.
        /// </summary>
        private bool isTransparent;

        /// <summary>
        /// The background brush.
        /// </summary>
        private Brush backgroundBrush;

        /// <summary>
        /// Allows the BackColor to be transparent
        /// </summary>
        /// <value>The background color.</value>
        /// <returns>
        /// A <see cref="T:System.Drawing.Color"/> that represents the background color of the control. The default is the value of the <see cref="P:System.Windows.Forms.Control.DefaultBackColor"/> property.
        /// </returns>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// </PermissionSet>
        public override Color BackColor
        {
            get
            {
                if (this.isTransparent)
                {
                    return Color.Transparent;
                }

                return base.BackColor;
            }

            set
            {
                if (value == Color.Transparent)
                {
                    this.isTransparent = true;
                    base.BackColor = (this.Parent == null) ? SystemColors.Window : this.Parent.BackColor;
                }
                else
                {
                    this.isTransparent = false;
                    base.BackColor = value;
                }

                if (this.backgroundBrush != null)
                {
                    this.backgroundBrush.Dispose();
                }

                this.backgroundBrush = new SolidBrush(base.BackColor);

                this.Invalidate();
            }
        }

        /// <summary>
        /// Gets or sets the drawing mode for the control.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DrawMode"/> values representing the mode for drawing the items of the control. The default is DrawMode.Normal.
        /// </returns>
        /// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
        /// The value assigned to the property is not a member of the <see cref="T:System.Windows.Forms.DrawMode"/> enumeration.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// A multicolumn <see cref="T:System.Windows.Forms.ListBox"/> cannot have a variable-sized height.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/>
        /// 	<IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// </PermissionSet>
        [Browsable(false)]
        public override sealed DrawMode DrawMode
        {
            get
            {
                return base.DrawMode;
            }

            set
            {
                if (value != DrawMode.OwnerDrawFixed)
                {
                    throw new Exception("Invalid value for DrawMode property");
                }

                base.DrawMode = value;
            }
        }

        /// <summary>
        /// Gets or sets the method in which items are selected in the <see cref="T:System.Windows.Forms.ListBox"/>.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.SelectionMode"/> values. The default is SelectionMode.One.
        /// </returns>
        /// <exception cref="T:System.ComponentModel.InvalidEnumArgumentException">
        /// The assigned value is not one of the <see cref="T:System.Windows.Forms.SelectionMode"/> values.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode, ControlEvidence"/>
        /// 	<IPermission class="System.Diagnostics.PerformanceCounterPermission, System, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// </PermissionSet>
        [Browsable(false)]
        public override sealed SelectionMode SelectionMode
        {
            get { return base.SelectionMode; }
            set
            {
                if (value != SelectionMode.One)
                {
                    throw new Exception("Invalid value for SelectionMode property");
                }

                base.SelectionMode = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RadioListBox"/> class.
        /// </summary>
        public RadioListBox()
        {
            this.DrawMode = DrawMode.OwnerDrawFixed;
            this.SelectionMode = SelectionMode.One;
            this.ItemHeight = this.FontHeight;

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            this.align = new StringFormat(StringFormat.GenericDefault) { LineAlignment = StringAlignment.Center };

            // Force transparent analisys
            this.BackColor = this.BackColor;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.ListBox.DrawItem"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.DrawItemEventArgs"/> that contains the event data.</param>
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            int maxItem = this.Items.Count - 1;

            if (e.Index < 0 || e.Index > maxItem)
            {
                // Erase all background if control has no items
                e.Graphics.FillRectangle(this.backgroundBrush, this.ClientRectangle);
                return;
            }

            int size = e.Font.Height; // button size depends on font height, not on item height

            // Calculate bounds for background, if last item paint up to bottom of control
            Rectangle backRect = e.Bounds;
            if (e.Index == maxItem)
            {
                backRect.Height = this.ClientRectangle.Top + this.ClientRectangle.Height - e.Bounds.Top;
            }

            e.Graphics.FillRectangle(this.backgroundBrush, backRect);

            // Determines text color/brush
            Brush textBrush;
            bool isChecked = (e.State & DrawItemState.Selected) == DrawItemState.Selected;

            RadioButtonState state = isChecked ? RadioButtonState.CheckedNormal : RadioButtonState.UncheckedNormal;

            if ((e.State & DrawItemState.Disabled) == DrawItemState.Disabled)
            {
                textBrush = SystemBrushes.GrayText;
                state = isChecked ? RadioButtonState.CheckedDisabled : RadioButtonState.UncheckedDisabled;
            }
            else if ((e.State & DrawItemState.Grayed) == DrawItemState.Grayed)
            {
                textBrush = SystemBrushes.GrayText;
                state = isChecked ? RadioButtonState.CheckedDisabled : RadioButtonState.UncheckedDisabled;
            }
            else
            {
                textBrush = SystemBrushes.FromSystemColor(this.ForeColor);
            }

            // Determines bounds for text and radio button
            Size glyphSize = RadioButtonRenderer.GetGlyphSize(e.Graphics, state);
            Point glyphLocation = e.Bounds.Location;
            glyphLocation.Y += (e.Bounds.Height - glyphSize.Height) / 2;

            Rectangle bounds = new Rectangle(e.Bounds.X + glyphSize.Width, e.Bounds.Y, e.Bounds.Width - glyphSize.Width, e.Bounds.Height);

            // Draws the radio button
            RadioButtonRenderer.DrawRadioButton(e.Graphics, glyphLocation, state);

            // Draws the text
            // Bound Datatable? Then show the column written in Displaymember
            if (!string.IsNullOrEmpty(this.DisplayMember))
            {
                e.Graphics.DrawString(
                                      ((System.Data.DataRowView)this.Items[e.Index])[this.DisplayMember].ToString(),
                                      e.Font,
                                      textBrush,
                                      bounds,
                                      this.align);
            }
            else
            {
                e.Graphics.DrawString(this.Items[e.Index].ToString(), e.Font, textBrush, bounds, this.align);
            }

            // If the ListBox has focus, draw a focus rectangle around the selected item.
            e.DrawFocusRectangle();
        }

        /// <summary>
        /// Sends the specified message to the default window procedure.
        /// </summary>
        /// <param name="m">The Windows <see cref="T:System.Windows.Forms.Message"/> to process.</param>
        protected override void DefWndProc(ref Message m)
        {
            // WM_ERASEBKGND
            if (m.Msg == 0x0014)
            {
                m.Result = (IntPtr)1; // avoid default background erasing
                return;
            }

            base.DefWndProc(ref m);
        }

        /// <summary>
        /// Specifies when the window handle has been created so that column width and other characteristics can be set. Inheriting classes should call base.OnHandleCreated.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnHandleCreated(EventArgs e)
        {
            if (this.FontHeight > this.ItemHeight)
            {
                this.ItemHeight = this.FontHeight;
            }

            base.OnHandleCreated(e);
        }

        /// <summary>
        /// Raises the FontChanged event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            if (this.FontHeight > this.ItemHeight)
            {
                this.ItemHeight = this.FontHeight;
            }

            this.Update();
        }

        /// <summary>
        /// Raises the ParentChanged event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnParentChanged(EventArgs e)
        {
            // Force to change backcolor
            this.BackColor = this.BackColor;
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.BackColorChanged"/> event when the <see cref="P:System.Windows.Forms.Control.BackColor"/> property value of the control's container changes.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnParentBackColorChanged(EventArgs e)
        {
            // Force to change backcolor
            this.BackColor = this.BackColor;
        }
    }
}