using System.Drawing;
using System.Drawing.Imaging;

namespace Share430.Client.Controls.Managers
{
    /// <summary>
    /// Manager for changing images.
    /// </summary>
    public static class ImageManager
    {
        /// <summary>
        /// Makes the grayscale.
        /// </summary>
        /// <param name="original">The original.</param>
        /// <returns>Grayscaled image.</returns>
        public static Image MakeGrayscale(Image original)
        {
            // create a blank bitmap the same size as original
            Image newBitmap = new Bitmap(original.Width, original.Height);

            // get a graphics object from the new image
            using (Graphics g = Graphics.FromImage(newBitmap))
            {
                // create the grayscale ColorMatrix
                ColorMatrix colorMatrix = new ColorMatrix(
                    new float[][]
                        {
                            //new Single[] { .3f, .3f, .3f, 0, 0 },
                            new float[] { .7f, .7f, .7f, 0, 0 },
                            new float[] { .6f, .6f, .6f, 0, 0 },
                            new float[] { .3f, .3f, .3f, 0, 0 },
                            new float[] { 0, 0, 0, 1, 0 },
                            new float[] { 0, 0, 0, 0, 1 }
                        });

                // create some image attributes
                ImageAttributes attributes = new ImageAttributes();

                // set the color matrix attribute
                attributes.SetColorMatrix(colorMatrix);

                // draw the original image on the new image
                // using the grayscale color matrix
                g.DrawImage(
                            original,
                            new Rectangle(0, 0, original.Width, original.Height),
                            0, 
                            0, 
                            original.Width, 
                            original.Height, 
                            GraphicsUnit.Pixel, 
                            attributes);
            }

            return newBitmap;
        }

        /// <summary>
        /// Backgrounds the color of the image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>The color of first pixel.</returns>
        internal static Color BackgroundImageColor(Image image)
        {
            Bitmap bmp = new Bitmap(image);
            return bmp.GetPixel(0, 0);
        }
    }
}