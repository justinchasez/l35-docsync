using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Share430.Client.Controls.Core;

namespace Share430.Client.Controls.Managers
{
    /// <summary>The drawing manager.</summary>
    public static class DrawingManager
    {
        /// <summary>The background image color.</summary>
        /// <param name="image">The image.</param>
        /// <returns>The color of first pixel.</returns>
        public static Color BackgroundImageColor(Image image)
        {
            if (image != null)
            {
                var bmp = new Bitmap(image);
                return bmp.GetPixel(0, 0);
            }

            return Color.White;
        }

        /// <summary>Draws a rounded rectangle.</summary>
        /// <param name="g">The graphics path.</param>
        /// <param name="control">The control.</param>
        /// <param name="gradientStartColor">The gradient start color.</param>
        /// <param name="gradientEndColor">The gradient end color.</param>
        /// <param name="gradientMode">The gradient mode.</param>
        /// <param name="borderColor">The border color.</param>
        /// <param name="borderWidth">The border width.</param>
        /// <param name="cornerRadius">The corner radius.</param>
        public static void DrawRoundedRectangle(
            Graphics g, 
            Control control, 
            Color gradientStartColor, 
            Color gradientEndColor, 
            LinearGradientMode gradientMode, 
            Color borderColor, 
            int borderWidth, 
            CornerThickness cornerRadius)
        {
            DrawRoundedRectangle(
                g, 
                control, 
                0, 
                0, 
                Color.Empty, 
                gradientStartColor, 
                gradientEndColor, 
                gradientMode, 
                borderColor, 
                borderWidth, 
                cornerRadius, 
                false);
        }

        /// <summary>Draws a rounded rectangle.</summary>
        /// <param name="g">The graphics path.</param>
        /// <param name="control">The control.</param>
        /// <param name="shadowOffset">The shadow offset.</param>
        /// <param name="shadowOpacity">The shadow opacity.</param>
        /// <param name="shadowColor">The shadow color.</param>
        /// <param name="gradientStartColor">The gradient start color.</param>
        /// <param name="gradientEndColor">The gradient end color.</param>
        /// <param name="gradientMode">The gradient mode.</param>
        /// <param name="borderColor">The border color.</param>
        /// <param name="borderWidth">The border width.</param>
        /// <param name="cornerRadius">The corner radius.</param>
        /// <param name="isSmooth">The is smooth.</param>
        public static void DrawRoundedRectangle(
            Graphics g, 
            Control control, 
            int shadowOffset, 
            int shadowOpacity, 
            Color shadowColor, 
            Color gradientStartColor, 
            Color gradientEndColor, 
            LinearGradientMode gradientMode, 
            Color borderColor, 
            int borderWidth, 
            CornerThickness cornerRadius, 
            bool isSmooth)
        {
            if (isSmooth)
            {
                g.SmoothingMode = SmoothingMode.AntiAlias;
            }

            int tmpShadowOffSet = Math.Min(Math.Min(shadowOffset, control.Width - 2), control.Height - 2);

            // Int32 tmpRoundCornerRadius = Math.Min(Math.Min(cornerRadius, control.Width - 2), control.Height - 2);

            if (control.Width > 1 && control.Height > 1)
            {
                Rectangle rect = new Rectangle(0, 0, control.Width - tmpShadowOffSet - 1, control.Height - tmpShadowOffSet - 1);
                Rectangle rectShadow = new Rectangle(
                    tmpShadowOffSet,
                    tmpShadowOffSet,
                    control.Width - tmpShadowOffSet - 1,
                    control.Height - tmpShadowOffSet - 1);

                GraphicsPath graphPathShadow = DrawRoundedRectanglePath(rectShadow, cornerRadius);
                GraphicsPath graphPath = DrawRoundedRectanglePath(rect, cornerRadius);

                if (shadowOffset > 0)
                {
                    using (PathGradientBrush shadowBrush = new PathGradientBrush(graphPathShadow))
                    {
                        shadowBrush.WrapMode = WrapMode.Clamp;
                        ColorBlend colorBlend = new ColorBlend(3)
                                                    {
                                                        Colors = new[]
                                                                     {
                                                                         Color.Transparent, 
                                                                         Color.FromArgb(shadowOpacity, shadowColor), 
                                                                         Color.FromArgb(shadowOpacity, shadowColor)
                                                                     },
                                                        Positions = new[] { 0f, .1f, 1f }
                                                    };

                        shadowBrush.InterpolationColors = colorBlend;
                        g.FillPath(shadowBrush, graphPathShadow);
                    }
                }

                // Draw backgroup
                LinearGradientBrush brush = new LinearGradientBrush(
                    rect,
                    gradientStartColor,
                    gradientEndColor,
                    gradientMode);
                g.FillPath(brush, graphPath);
                g.DrawPath(new Pen(borderColor, borderWidth), graphPath);
            }
        }

        /// <summary>The draw rounded rectangle path.</summary>
        /// <param name="rectangle">The rectangle.</param>
        /// <param name="radiusThickness">The radius thickness.</param>
        /// <returns><see cref="GraphicsPath"/> object.</returns>
        private static GraphicsPath DrawRoundedRectanglePath(Rectangle rectangle, CornerThickness radiusThickness)
        {
            GraphicsPath graphPath = new GraphicsPath();

            if (radiusThickness.BottomLeft != 0 || radiusThickness.BottomRight != 0 ||
                radiusThickness.TopLeft != 0 || radiusThickness.TopRight != 0)
            {
                // top
                graphPath.AddLine(
                    rectangle.X + radiusThickness.TopLeft, 
                    rectangle.Y, 
                    rectangle.X + rectangle.Width - (radiusThickness.TopLeft + radiusThickness.TopRight), 
                    rectangle.Y);

                // top-right
                if (radiusThickness.TopRight > 0)
                {
                    graphPath.AddArc(
                        rectangle.X + rectangle.Width - (radiusThickness.TopRight * 2), 
                        rectangle.Y, 
                        radiusThickness.TopRight * 2, 
                        radiusThickness.TopRight * 2, 
                        270, 
                        90);
                }

                // right
                graphPath.AddLine(
                    rectangle.X + rectangle.Width, 
                    rectangle.Y + radiusThickness.TopRight, 
                    rectangle.X + rectangle.Width, 
                    rectangle.Y + rectangle.Height - (radiusThickness.TopRight + radiusThickness.BottomRight));

                // right-bottom
                if (radiusThickness.BottomRight > 0)
                {
                    graphPath.AddArc(
                        rectangle.X + rectangle.Width - (radiusThickness.BottomRight * 2), 
                        rectangle.Y + rectangle.Height - (radiusThickness.BottomRight * 2), 
                        radiusThickness.BottomRight * 2, 
                        radiusThickness.BottomRight * 2, 
                        0, 
                        90);
                }

                // bottom
                graphPath.AddLine(
                    rectangle.X + rectangle.Width - (radiusThickness.BottomRight * 2), 
                    rectangle.Y + rectangle.Height, 
                    rectangle.X + radiusThickness.BottomLeft, 
                    rectangle.Y + rectangle.Height);

                // left-bottom
                if (radiusThickness.BottomLeft > 0)
                {
                    graphPath.AddArc(
                        rectangle.X, 
                        rectangle.Y + rectangle.Height - (radiusThickness.BottomLeft * 2), 
                        radiusThickness.BottomLeft * 2, 
                        radiusThickness.BottomLeft * 2, 
                        90, 
                        90);
                }

                // left
                graphPath.AddLine(
                    rectangle.X, 
                    rectangle.Y + rectangle.Height - (radiusThickness.BottomLeft * 2), 
                    rectangle.X, 
                    rectangle.Y + radiusThickness.TopLeft);

                // top-left
                if (radiusThickness.TopLeft > 0)
                {
                    graphPath.AddArc(rectangle.X, rectangle.Y, radiusThickness.TopLeft * 2, radiusThickness.TopLeft * 2, 180, 90);
                }

                graphPath.CloseFigure();
            }
            else
            {
                graphPath.AddRectangle(rectangle);
            }

            return graphPath;
        }
    }
}