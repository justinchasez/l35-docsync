namespace Share430.Client.Controls.ImageButton
{
    /// <summary>
    /// The atate of image button.
    /// </summary>
    internal enum ImageButtonState
    {
        /// <summary>
        /// It's default control state
        /// </summary>
        Normal,

        /// <summary>
        /// When button is hovered
        /// </summary>
        Hover,

        /// <summary>
        /// When button is pressed
        /// </summary>
        Pressed
    }
}