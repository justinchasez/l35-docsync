using System.Drawing;
using System.Drawing.Imaging;
using Share430.Client.Controls.Managers;

namespace Share430.Client.Controls.ImageButton.DrawAdapters
{
    /// <summary>
    /// Draw adapter witch renders image above text
    /// </summary>
    internal class TextAboveImageDrawAdapter : IDrawAdapter
    {
        #region IDrawAdapter Members

        /// <summary>
        /// Draws the layout.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="g">The graphics object.</param>
        public void DrawLayout(ImageButton control, Graphics g)
        {
            Rectangle imgRect;
            Image image = control.Image;
            Color foreColor = control.ForeColor;
            int offset = 0;

            if (g != null)
            {
                if (image != null)
                {
                    if (control.State != ImageButtonState.Pressed && control.PressedImage == null && control.AnimatePress)
                    {
                        // Shift the image by one pixel 
                        offset = 1;
                    }

                    if (!control.Enabled)
                    {
                        image = ImageManager.MakeGrayscale(image);
                        foreColor = Color.Gray;
                    }

                    SizeF drawStringSize = g.MeasureString(control.Text, control.Font);

                    int imageLeft = (control.Width - image.Width) / 2;
                    int imageTop = (control.Height - (image.Height - (int)drawStringSize.Height)) / 2;

                    imgRect = new Rectangle(imageLeft - offset, imageTop - offset, image.Width, image.Height);

                    // Set transparent key 
                    if (control.AllowTransparency)
                    {
                        ImageAttributes imageAttr = new ImageAttributes();
                        Color transparentColor = DrawingManager.BackgroundImageColor(image);
                        imageAttr.SetColorKey(transparentColor, transparentColor);

                        // Draw image
                        g.DrawImage(image, imgRect, offset, offset, image.Width, image.Height, GraphicsUnit.Pixel, imageAttr);
                    }
                    else
                    {
                        g.DrawImage(image, imgRect, offset, offset, image.Width, image.Height, GraphicsUnit.Pixel);
                    }

                    PointF drawPoint = new PointF(control.Width / 2f - drawStringSize.Width / 2, imageTop - drawStringSize.Height);

                    g.DrawString(control.Text, control.Font, new SolidBrush(foreColor), drawPoint);
                }
            }
        }

        #endregion
    }
}