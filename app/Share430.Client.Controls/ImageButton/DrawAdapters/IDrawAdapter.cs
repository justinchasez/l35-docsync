using System.Drawing;

namespace Share430.Client.Controls.ImageButton.DrawAdapters
{
    /// <summary>
    /// Drawing manager
    /// </summary>
    internal interface IDrawAdapter
    {
        /// <summary>
        /// Draws the layout.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="g">The graphics object.</param>
        void DrawLayout(ImageButton control, Graphics g);
    }
}