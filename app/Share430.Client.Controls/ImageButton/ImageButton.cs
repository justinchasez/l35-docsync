using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Share430.Client.Controls.ImageButton.DrawAdapters;

namespace Share430.Client.Controls.ImageButton
{
    /// <summary>
    /// The button with image.
    /// </summary>
    public partial class ImageButton : Control, IButtonControl
    {
        #region Private fields

        /// <summary>
        /// Indicates where is the defauls state
        /// </summary>
        private bool isDefault;

        /// <summary>
        /// The Drawing Adapter field.
        /// </summary>
        private IDrawAdapter drawingAdapter;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageButton"/> class.
        /// </summary>
        public ImageButton()
        {
            this.Size = new Size(45, 45);
            this.State = ImageButtonState.Normal;
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            this.InitializeComponent();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [allow transparency].
        /// </summary>
        /// <value><c>true</c> if [allow transparency]; otherwise, <c>false</c>.</value>
        [Category("Appearance")]
        [Description("Makes the image transparent.")]
        public bool AllowTransparency { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [animate press].
        /// </summary>
        /// <value><c>true</c> if [animate press]; otherwise, <c>false</c>.</value>
        [Category("Appearance")]
        [Description("Animcate press action.")]
        public bool AnimatePress { get; set; }

        /// <summary>
        /// Gets or sets the normal image.
        /// </summary>
        /// <value>The normal image.</value>
        [Category("Appearance")]
        [Description("Image to show when the button in normal state.")]
        public Image NormalImage { get; set; }

        /// <summary>
        /// Gets or sets the hover image.
        /// </summary>
        /// <value>The hover image.</value>
        [Category("Appearance")]
        [Description("Image to show when the button when hovered.")]
        public Image HoverImage { get; set; }

        /// <summary>
        /// Gets or sets the pressed image.
        /// </summary>
        /// <value>The pressed image.</value>
        [Category("Appearance")]
        [Description("Image to show when the button is pressed.")]
        public Image PressedImage { get; set; }

        /// <summary>
        /// Gets or sets the text image relation.
        /// </summary>
        /// <value>The text image relation.</value>
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Category("Appearance")]
        [Description("Specifies the relative location of the image to the text on the button.")]
        [DefaultValue(0)]
        public TextImageRelation TextImageRelation { get; set; }

        /// <summary>
        /// Gets or sets the text vertical offcet.
        /// </summary>
        /// <value>The text vertical offcet.</value>
        [Category("Appearance")]
        [DefaultValue(0)]
        public int TextVerticalOffcet { get; set; }

        /// <summary>
        /// Gets or sets the text horisontal offcet.
        /// </summary>
        /// <value>The text horisontal offcet.</value>
        [Category("Appearance")]
        [DefaultValue(0)]
        public int TextHorisontalOffcet { get; set; }

        /// <summary>
        /// Gets the current image.
        /// </summary>
        /// <value>The cuurent image.</value>
        internal Image Image
        {
            get
            {
                switch (this.State)
                {
                    case ImageButtonState.Normal:
                        return this.NormalImage;
                    case ImageButtonState.Hover:
                        return this.HoverImage ?? this.NormalImage;
                    case ImageButtonState.Pressed:
                        return this.PressedImage ?? this.HoverImage ?? this.NormalImage;
                    default: return null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The button state.</value>
        internal ImageButtonState State { get; set; }

        #endregion

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseDown"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left)
            {
                this.State = ImageButtonState.Pressed;
            }

            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseUp"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.MouseEventArgs"/> that contains the event data.</param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            this.State = ImageButtonState.Hover;
            this.Invalidate();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.State = ImageButtonState.Normal;
            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseEnter"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.State = ImageButtonState.Hover;
            this.Refresh();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.Click"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClick(EventArgs e)
        {
            Form form = this.FindForm();
            if (form != null)
            {
                form.DialogResult = this.DialogResult;
            }

            base.OnClick(e);
        }
        
        #region Events

        #endregion

        #region IButtonControl Members

        /// <summary>
        /// Gets or sets the value returned to the parent form when the button is clicked.
        /// </summary>
        /// <returns>
        /// One of the <see cref="T:System.Windows.Forms.DialogResult"/> values.
        /// </returns>
        public DialogResult DialogResult { get; set; }

        /// <summary>
        /// Notifies a control that it is the default button so that its appearance and behavior is adjusted accordingly.
        /// </summary>
        /// <param name="value">true if the control should behave as a default button; otherwise false.</param>
        public void NotifyDefault(bool value)
        {
            this.isDefault = value;
        }

        /// <summary>
        /// Generates a <see cref="E:System.Windows.Forms.Control.Click"/> event for the control.
        /// </summary>
        public void PerformClick()
        {
            this.OnClick(EventArgs.Empty);
        }

        #endregion

        /// <summary>
        /// Ensures the drawing adapter.
        /// </summary>
        /// <param name="value">The value.</param>
        private void EnsureDrawingAdapter(TextImageRelation value)
        {
            switch (value)
            {
                case TextImageRelation.Overlay:
                    this.drawingAdapter = new OverlayDrawAdapter();
                    break;
                case TextImageRelation.ImageAboveText:
                    this.drawingAdapter = new ImageAboveTextDrawAdapter();
                    break;
                case TextImageRelation.ImageBeforeText:
                    this.drawingAdapter = new ImageBeforeTextDrawAdapter();
                    break;
                case TextImageRelation.TextAboveImage:
                    this.drawingAdapter = new TextAboveImageDrawAdapter();
                    break;
                case TextImageRelation.TextBeforeImage:
                    this.drawingAdapter = new TextBeforeImageDrawAdapter();
                    break;
            }
        }

        #region Overriden members

        /// <summary>
        /// Gets the required creation parameters when the control handle is created.
        /// </summary>
        /// <value></value>
        /// <returns>
        /// A <see cref="T:System.Windows.Forms.CreateParams"/> that contains the required creation parameters when the handle to the control is created.
        /// </returns>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams drawParams = base.CreateParams;
                drawParams.ExStyle |= 0x20;
                return drawParams;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Paint"/> event.
        /// </summary>
        /// <param name="pe">The <see cref="System.Windows.Forms.PaintEventArgs"/> instance containing the event data.</param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            this.EnsureDrawingAdapter(this.TextImageRelation);
            this.drawingAdapter.DrawLayout(this, pe.Graphics);
        }

        #endregion
    }
}