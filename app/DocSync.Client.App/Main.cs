﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using DocSync.Client.App.Controls;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Managers.Application;
using DocSync.Client.Core.Utils;
using DocSync.Client.Remoting.Args;

namespace DocSync.Client.App
{
    /// <summary>
    /// The Main form.
    /// </summary>
    public partial class Main : FormBase
    {
        #region Private members

        /// <summary>
        /// Get last version delegate
        /// </summary>
        private delegate void GetLastVersionDelegate();

        /// <summary>
        /// The tab controls collection.
        /// </summary>
        private readonly Dictionary<ViewTab, SwitchControl> tabs = new Dictionary<ViewTab, SwitchControl>();

        /// <summary>
        /// The tab views controls collection.
        /// </summary>
        private readonly Dictionary<ViewTab, UserControl> tabViews = new Dictionary<ViewTab, UserControl>();

        /// <summary>
        /// Currently selected tab.
        /// </summary>
        private ViewTab selctedTab = ViewTab.Options;

        /// <summary>
        /// The startup arguments.
        /// </summary>
        private string[] args;

        #endregion

        /// <summary>
        /// Initializes a new instance of the Main class.
        /// </summary>
        public Main()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);
            this.SetStyle();

            this.Load += this.Main_Load;
            this.Shown += this.Main_Shown;
            this.Closed += this.Main_Closed;
        }

        /// <summary>
		/// DocSync closed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void Main_Closed(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Main"/> class.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public Main(string[] args) : this()
        {
            this.args = args;
        }

        /// <summary>
        /// Handles the Load event of the Main control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Main_Load(object sender, EventArgs e)
        {
            // Initialize tab controls
            this.tabs.Add(ViewTab.Status, this.tabViewStatus);
            this.tabs.Add(ViewTab.Options, this.tabSetOptions);
            this.tabs.Add(ViewTab.RestoreFiles, this.tabRestoreFiles);
            //this.tabs.Add(ViewTab.Support, this.tabGetSupport);
            this.tabs.Add(ViewTab.Search, this.tabRestoreFiles);
            this.tabs.Add(ViewTab.AdvancedScheduling, this.tabSetOptions);
            this.tabs.Add(ViewTab.RulesEditor, this.tabSetOptions);

            // Initialize tab views
            this.tabViews.Add(ViewTab.Status, this.ctrlViewViewStatus);
            this.tabViews.Add(ViewTab.Options, this.ctrlViewSetOptions);
            this.tabViews.Add(ViewTab.RestoreFiles, this.ctrlViewRestoreFiles);
            //this.tabViews.Add(ViewTab.Support, this.ctrlViewGetSupport);
            this.tabViews.Add(ViewTab.Search, this.ctrlSearchView);
            this.tabViews.Add(ViewTab.AdvancedScheduling, this.ctrlAdvancedSchedulingView);
            this.tabViews.Add(ViewTab.RulesEditor, this.ctrlRulesEditorView);

            EventPublisher.UIEventManagerInstance.SwitchTab += this.UIEventManagerInstance_SwitchTab;

            if (this.args != null && this.args.Length > 0)
            {
                ViewTab initTab = ViewTab.Status;

                if (this.args[0].Equals("-options", StringComparison.OrdinalIgnoreCase))
                {
                    initTab = ViewTab.Options;
                }

                if (this.args[0].Equals("-status", StringComparison.OrdinalIgnoreCase))
                {
                    initTab = ViewTab.Status;
                }

              
                EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = initTab, ForceSwitch = true });

                this.args = null;
            }
            else
            {
                EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Status, ForceSwitch = true });
            }

            ApplicationInstanceManager.InstanceLoadingCompleted();
            RemotingManager.Instance.ProxyPublisher.RestorePreviousVersion += this.ProxyPublisher_RestorePreviousVersion;

            GetLastVersionDelegate getLastVersion = this.GetLastVersion;
            getLastVersion.BeginInvoke(this.EndAsyncOperation, getLastVersion);
        }

        /// <summary>
        /// Get last version
        /// </summary>
        private void GetLastVersion()
        {
            string serverVersion = RemotingManager.Instance.ProxyManager.AppManger.LastClientAppVersion;

            Loger.Instance.Log("Server version: " + serverVersion);

            if (!string.IsNullOrEmpty(serverVersion))
            {
                Version lastVersion = new Version(serverVersion);
                Version currentVersion = new Version(Application.ProductVersion);

                Loger.Instance.Log("Current version: " + currentVersion);

                if (lastVersion > currentVersion)
                {
                    string link = RemotingManager.Instance.ProxyManager.AppManger.LinkToLastVersionApp;

                    Loger.Instance.Log("New version available " + link);

                    if (!string.IsNullOrEmpty(link))
                    {
                        UpdateAvalable dialog = new UpdateAvalable(link);
                        dialog.ShowDialog();
                    }
                }
            }
        }

        /// <summary>
        /// End Async Operation
        /// </summary>
        /// <param name="result">Result of operation</param>
        private void EndAsyncOperation(IAsyncResult result)
        {
            GetLastVersionDelegate getLastVersion = (GetLastVersionDelegate) result.AsyncState;
            try
            {
                getLastVersion.EndInvoke(result);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Event handler for RestorePreviousVersion event
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void ProxyPublisher_RestorePreviousVersion(object sender, RestoreFileEventArgs e)
        {
            FileInfoDto version = RemotingManager.Instance.ProxyManager.BackupManager.GetFileInfo(
                e.FilePath, ClientSettingsManager.Settings.CurrentComputer.Id);

            version.FileName = e.FilePath;

            //this.BeginInvoke(new MethodInvoker(delegate()
            //                                       {
            //                                           FileVersion dialog = new FileVersion(version);
            //                                           dialog.ShowDialog(this);
            //                                       }));
        }

        /// <summary>
        /// Handles the Shown event of the Main control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Main_Shown(object sender, EventArgs e)
        {
            ClientSettingsManager.Settings.MyAccountUrl = AppSettingsManager.MembershipPageUrl;
            ClientSettingsManager.Settings.HelpUrl = AppSettingsManager.ViewHelpPage;
            ClientSettingsManager.Settings.ReferFriendUrl = AppSettingsManager.ReferFriendUrl;
            ClientSettingsManager.Save();

            if (ClientSettingsManager.Settings.IsFirstRun)
            {
                this.Enabled = false;

                StartupContainer startup = new StartupContainer();
                startup.ShowDialog();

                this.Enabled = true;
            }

            if (ClientSettingsManager.Settings.IsFirstRun)
            {
                Application.Exit();
            }

        }

        /// <summary>
        /// UIs the event manager instance_ switch tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchTabEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_SwitchTab(SwitchTabEventArgs args)
        {
            if (this.selctedTab != args.SelectedTab || args.ForceSwitch)
            {
                this.SwitchTab(args.SelectedTab);
            }
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="viewTab">The view tab.</param>
        private void SwitchTab(ViewTab viewTab)
        {
            this.tabs[this.selctedTab].IsSelected = false;
            this.tabs[viewTab].IsSelected = true;

            this.tabViews[this.selctedTab].Visible = false;
            this.tabViews[viewTab].Visible = true;

            this.selctedTab = viewTab;
        }

        #region Tab controls event handlers

        /// <summary>
        /// Handles the Click event of the SwitchTabViewStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabViewStatus_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Status });
        }

        /// <summary>
        /// Handles the Click event of the SwitchTabOptions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabOptions_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Options });
        }

        /// <summary>
        /// Handles the Click event of the SwitchTabSupport control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabGetSupport_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Support });
        }

        /// <summary>
        /// Handles the Click event of the SwitchTabViewFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabRestoreFiles_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.RestoreFiles });
        }

        #endregion

        /// <summary>
        /// WNDs the proc.
        /// </summary>
        /// <param name="m">The system message.</param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == ApplicationInstanceManager.WM_SHOW_ONLINE_BACKUP)
            {
                this.WindowState = FormWindowState.Normal;
            }

            if (m.Msg == ApplicationInstanceManager.WM_CLOSE_ONLINE_BACKUP)
            {
                Application.Exit();
            }

            if (m.Msg == ApplicationInstanceManager.WM_QUERYENDSESSION)
            {
                Environment.Exit(0);
            }

            if (m.Msg == ApplicationInstanceManager.WM_SHOW_ONLINE_BACKUP_OPTIONS)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                }

                this.Activate();
                EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Options });
            }

            if (m.Msg == ApplicationInstanceManager.WM_SHOW_ONLINE_BACKUP_STATUS)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                }

                this.Activate();
                EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Status });
            }

            
            if (m.Msg == ApplicationInstanceManager.WM_SEARCH_FOR_FILES_TO_RESTORE)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                }

                this.Activate();
                EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Search });
            }

            if (m.Msg == ApplicationInstanceManager.WM_LAUNCH_RESTORE_WIZARD)
            {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.WindowState = FormWindowState.Normal;
                }

                this.Activate();

                if (!ClientSettingsManager.Settings.IsFirstRun)
                {
                    EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.RestoreFiles });
                    EventPublisher.UIEventManagerInstance.OnLaunchRestoreWizard(new SetOptionsEventArgs());
                }
            }

            if (m.Msg == ApplicationInstanceManager.WM_UNPAUSE_PERMISION)
            {
                //Paused dialog = new Paused();
                //if (dialog.ShowDialog(this) == DialogResult.OK)
                //{
                //    RemotingManager.Instance.ProxyManager.AppManger.SetPause(0);
                //    //RemotingManager.Instance.ProxyPublisher.NotifyUnPaused(new PauseEventArgs(0));
                //}
            }

            base.WndProc(ref m);
        }

        /// <summary>
        /// Handles the LinkClicked event of the LinkSubscription control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LinkSubscription_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.MembershipPageUrl);
        }
    }
}