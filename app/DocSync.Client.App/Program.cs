using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Constants;
using DocSync.Client.Core.Managers.Application;
using DocSync.Client.Core.Managers.Logging;

namespace DocSync.Client.App
{
    /// <summary>
    /// The main program class
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The tray mode command line argument
        /// </summary>
        private const string TRAY_PARAM_KEY = "-tray";

        /// <summary>
        /// The settings mode command line argument
        /// </summary>
        private const string SETTINGS_PARAM_KEY = "-options";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">The command args.</param>
        [STAThread]
        public static void Main(string[] args)
        {
            log4net.GlobalContext.Properties[LoggingManager.LOGING_FILE_KEY] = Path.Combine(CoreConstantsHelper.PathForSaveData, Assembly.GetExecutingAssembly().GetName().Name + LoggingManager.LOG_FILE_EXTENTION);

            if (ApplicationInstanceManager.IsFirstInstance())
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Application.ApplicationExit += Application_ApplicationExit;
                Application.ThreadException += Application_ThreadException;

                Main main = new Main(args);

                Application.Run(main);
            }
            else
            {
                ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_SHOW_ONLINE_BACKUP);
                Application.Exit();
            }
        }

        #region Application event handlers

        /// <summary>
        /// Handles the ApplicationExit event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private static void Application_ApplicationExit(object sender, EventArgs e)
        {
            RemotingManager.Instance.Disconnect();
        }

        /// <summary>
        /// Handles the ThreadException event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Threading.ThreadExceptionEventArgs"/> instance containing the event data.</param>
        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
        }

        #endregion
    }
}
