using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Events.Interface;

namespace DocSync.Client.App.Core.Events
{
    /// <summary>
    /// The wizard event manager.
    /// </summary>
    public class WizardEventManager : IWizardEventManager
    {
        #region IWizardEventManager Members

        /// <summary>
        /// Occurs when [switch tab].
        /// </summary>
        public event EventHandler<SwitchWizardTabEventArgs> SwitchWizardTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchWizardTabEventArgs"/> instance containing the event data.</param>
        public void OnSwitchWizardTab(SwitchWizardTabEventArgs args)
        {
            EventHandler<SwitchWizardTabEventArgs> handler = this.SwitchWizardTab;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [computer added].
        /// </summary>
        public event ComputerAddedDelegate ComputerAdded;

        /// <summary>
        /// Raises the cpmputer added event.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        /// <returns>Is computer added</returns>
        public bool OnComputerAdded(ComputerEventArgs args)
        {
            ComputerAddedDelegate handler = this.ComputerAdded;
            if (handler != null)
            {
                return handler(args);
            }

            return false;
        }

        /// <summary>
        /// Occurs when [computer selected].
        /// </summary>
        public event EventHandler<ComputerEventArgs> ComputerSelected;

        /// <summary>
        /// Raises the <see cref="E:ComputerSelected"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        public void OnComputerSelected(ComputerEventArgs args)
        {
            EventHandler<ComputerEventArgs> handler = this.ComputerSelected;
            if (handler != null)
            {
                handler(args);
            }
        }

        #endregion
    }
}