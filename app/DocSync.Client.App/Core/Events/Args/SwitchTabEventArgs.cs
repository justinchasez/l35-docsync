using System;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for SwitchTab event.
    /// </summary>
    public class SwitchTabEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the SwitchTabEventArgs class.
        /// </summary>
        public SwitchTabEventArgs()
        {
            this.ForceSwitch = false;
        }

        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>The selected tab.</value>
        public ViewTab SelectedTab { get; set; }

        /// <summary>
        /// true to force switch tabs
        /// </summary>
        public bool ForceSwitch { get; set; }
    }
}