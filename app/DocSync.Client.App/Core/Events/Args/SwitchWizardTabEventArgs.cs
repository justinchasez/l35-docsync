using System;
using DocSync.Client.App.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for SwitchWizardTab event.
    /// </summary>
    public class SwitchWizardTabEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>The selected tab.</value>
        public WizardViewTab SelectedTab { get; set; }
    }
}