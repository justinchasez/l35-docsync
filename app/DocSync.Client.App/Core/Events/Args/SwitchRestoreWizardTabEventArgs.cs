using System;
using DocSync.Client.App.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argumetn for SwitchRestoreWizardTab event.
    /// </summary>
    public class SwitchRestoreWizardTabEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>The selected tab.</value>
        public RestoreWizardTab SelectedTab { get; set; }
    }
}