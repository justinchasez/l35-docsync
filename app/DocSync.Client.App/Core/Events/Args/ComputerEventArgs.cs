using System;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for ComputerRemoved event.
    /// </summary>
    public class ComputerEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerEventArgs"/> class.
        /// </summary>
        public ComputerEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerEventArgs"/> class.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        public ComputerEventArgs(ComputerInfo computerInfo)
        {
            this.ComputerInfo = computerInfo;
        }

        /// <summary>
        /// Gets or sets the computer info.
        /// </summary>
        /// <value>The computer info.</value>
        public ComputerInfo ComputerInfo { get; set; }
    }
}