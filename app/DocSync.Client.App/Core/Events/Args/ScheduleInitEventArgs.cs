using System.Collections.Generic;
using DocSync.Client.Core.Entities.Schedule.Interface;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argu,ent for schedules initializing.
    /// </summary>
    public class ScheduleInitEventArgs
    {
        /// <summary>
        /// The schedules list.
        /// </summary>
        private List<ISchedule> schedules;

        /// <summary>
        /// Gets the schedules.
        /// </summary>
        /// <value>The schedules.</value>
        public List<ISchedule> Schedules
        {
            get
            {
                if (this.schedules == null)
                {
                    this.schedules = new List<ISchedule>();
                }

                return this.schedules;
            }
        }
    }
}