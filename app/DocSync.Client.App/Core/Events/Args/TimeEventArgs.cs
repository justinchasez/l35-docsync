using System;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// Time event argument
    /// </summary>
    public class TimeEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the TimeEventArgs class
        /// </summary>
        /// <param name="value">new value of time</param>
        public TimeEventArgs(DateTime value)
        {
            this.Time = value;
        }

        /// <summary>
        /// Gets time value
        /// </summary>
        public DateTime Time { get; private set; }
    }
}