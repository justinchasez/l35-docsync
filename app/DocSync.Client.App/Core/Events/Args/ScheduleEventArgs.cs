using System;
using DocSync.Client.Core.Entities.Schedule.Interface;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for ScheduleSaving event.
    /// </summary>
    [Serializable]
    public class ScheduleEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        /// <value>The schedule.</value>
        public ISchedule Schedule { get; set; }

        ///// <summary>
        ///// Gets or sets the mode.
        ///// </summary>
        ///// <value>The mode.</value>
        //public BackupScheduleMode Mode { get; set; }
        
        ///// <summary>
        ///// Gets or sets the days.
        ///// </summary>
        ///// <value>The selected days.</value>
        //public ScheduleDayOfWeek Days { get; set; }
        
        ///// <summary>
        ///// Gets or sets the start date.
        ///// </summary>
        ///// <value>The start date.</value>
        //public DateTime StartDate { get; set; }

        ///// <summary>
        ///// Gets or sets the end date.
        ///// </summary>
        ///// <value>The end date.</value>
        //public DateTime EndDate { get; set; }
    }
}