using DocSync.Client.App.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for SwitchStatusTab event.
    /// </summary>
    public class SwitchStatusTabEventArgs
    {
        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>The selected tab.</value>
        public StatusViewTab SelectedTab { get; set; }
    }
}