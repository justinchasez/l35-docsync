using System;
using DocSync.Client.App.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for SwitchOptionsTab event.
    /// </summary>
    public class SwitchOptionsTabEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the selected tab.
        /// </summary>
        /// <value>The selected tab.</value>
        public OptionsTab SelectedTab { get; set; }
    }
}