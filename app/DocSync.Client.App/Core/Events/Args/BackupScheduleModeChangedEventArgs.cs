using System;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Core.Events.Args
{
    /// <summary>
    /// The event argument for BackupScheduleModeChanged event.
    /// </summary>
    [Serializable]
    public class BackupScheduleModeChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>The backup schedule mode.</value>
        public BackupScheduleMode Mode { get; set; }
    }
}