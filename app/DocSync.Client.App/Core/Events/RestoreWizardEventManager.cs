using System;
using System.ComponentModel;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Events.Interface;

namespace DocSync.Client.App.Core.Events
{
    /// <summary>
    /// The restore wizard event manager.
    /// </summary>
    public class RestoreWizardEventManager : IRestoreWizardEventManager
    {
        #region IRestoreWizardEventManager Members

        /// <summary>
        /// Occurs when [switch restore wizard view].
        /// </summary>
        public event EventHandler<SwitchRestoreWizardTabEventArgs> SwitchRestoreWizardView;

        /// <summary>
        /// Raises the <see cref="E:SwitchRestoreWizardView"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        public void OnSwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args)
        {
            EventHandler<SwitchRestoreWizardTabEventArgs> handler = this.SwitchRestoreWizardView;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [restore wizard close].
        /// </summary>
        public event EventHandler<CancelEventArgs> RestoreWizardClose;

        /// <summary>
        /// Raises the <see cref="E:RestoreWizardClose"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        public void OnRestoreWizardClose(CancelEventArgs args)
        {
            EventHandler<CancelEventArgs> handler = this.RestoreWizardClose;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [restore wizard finished].
        /// </summary>
        public event EventHandler RestoreWizardFinished;

        /// <summary>
        /// Raises the <see cref="E:RestoreWizardFinished"/> event.
        /// </summary>
        public void OnRestoreWizardFinished()
        {
            EventHandler finished = this.RestoreWizardFinished;
            if (finished != null)
            {
                finished(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}