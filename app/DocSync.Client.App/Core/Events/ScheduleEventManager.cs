using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Events.Interface;
using DocSync.Client.Remoting.Handlers;

namespace DocSync.Client.App.Core.Events
{
    /// <summary>
    /// The schedule event manager.
    /// </summary>
    public class ScheduleEventManager : IScheduleEventManager
    {
        #region IScheduleEventManager Members

        /// <summary>
        /// Occurs when [backup schedule mode changed].
        /// </summary>
        public event RemotingEventHandler<BackupScheduleModeChangedEventArgs> BackupScheduleModeChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupScheduleModeChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="BackupScheduleModeChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupScheduleModeChanged(BackupScheduleModeChangedEventArgs args)
        {
            RemotingEventHandler<BackupScheduleModeChangedEventArgs> handler = this.BackupScheduleModeChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [schedule initialized].
        /// </summary>
        public event RemotingEventHandler<ScheduleInitEventArgs> ScheduleInitialized;

        /// <summary>
        /// Raises the <see cref="E:ScheduleInitialized"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleInitEventArgs"/> instance containing the event data.</param>
        public void OnScheduleInitialized(ScheduleInitEventArgs args)
        {
            RemotingEventHandler<ScheduleInitEventArgs> handler = this.ScheduleInitialized;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [save schedule].
        /// </summary>
        public event RemotingEventHandler<ScheduleEventArgs> ScheduleSaved;

        /// <summary>
        /// Raises the <see cref="E:ScheduleSaving"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        public void OnScheduleSaved(ScheduleEventArgs args)
        {
            RemotingEventHandler<ScheduleEventArgs> handler = this.ScheduleSaved;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [remove schedule].
        /// </summary>
        public event RemotingEventHandler<ScheduleEventArgs> ScheduleRemoved;

        /// <summary>
        /// Raises the <see cref="E:ScheduleRemoving"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        public void OnScheduleRemoved(ScheduleEventArgs args)
        {
            RemotingEventHandler<ScheduleEventArgs> handler = this.ScheduleRemoved;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [edit schedule].
        /// </summary>
        public event EventHandler<ScheduleEventArgs> EditSchedule;

        /// <summary>
        /// Raises the <see cref="E:EditSchedule"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        public void OnEditSchedule(ScheduleEventArgs args)
        {
            EventHandler<ScheduleEventArgs> handler = this.EditSchedule;
            if (handler != null)
            {
                handler(args);
            }
        }

        #endregion
    }
}