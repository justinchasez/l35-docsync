using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Events.Interface;
using DocSync.Client.Core.Args;

namespace DocSync.Client.App.Core.Events
{
    /// <summary>
    /// Contains events for UI.
    /// </summary>
    public class UIEventManager : IUIEventManager
    {
        /// <summary>
        /// Occurs when [switch tab].
        /// </summary>
        public event EventHandler<SwitchTabEventArgs> SwitchTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchTabEventArgs"/> instance containing the event data.</param>
        public void OnSwitchTab(SwitchTabEventArgs args)
        {
            EventHandler<SwitchTabEventArgs> handler = this.SwitchTab;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [switch status tab].
        /// </summary>
        public event EventHandler<SwitchStatusTabEventArgs> SwitchStatusTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchStatusTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchStatusTabEventArgs"/> instance containing the event data.</param>
        public void OnSwitchStatusTab(SwitchStatusTabEventArgs args)
        {
            EventHandler<SwitchStatusTabEventArgs> handler = this.SwitchStatusTab;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [switch options tab].
        /// </summary>
        public event EventHandler<SwitchOptionsTabEventArgs> SwitchOptionsTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchOptionsTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchOptionsTabEventArgs"/> instance containing the event data.</param>
        public void OnSwitchOptionsTab(SwitchOptionsTabEventArgs args)
        {
            EventHandler<SwitchOptionsTabEventArgs> handler = this.SwitchOptionsTab;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when need to set option
        /// </summary>
        public event EventHandler<SetOptionsEventArgs> SetOptions;

        /// <summary>
        /// Raises the <see cref="E:SetOptions"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        public void OnSetOptions(SetOptionsEventArgs args)
        {
            EventHandler<SetOptionsEventArgs> handler = this.SetOptions;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// event to launch restore wizard
        /// </summary>
        public event EventHandler<SetOptionsEventArgs> LaunchRestoreWizard;

        /// <summary>
        /// Raises the event LaunchRestoreWizard
        /// </summary>
        /// <param name="args">event argument</param>
        public void OnLaunchRestoreWizard(SetOptionsEventArgs args)
        {
            EventHandler<SetOptionsEventArgs> handler = this.LaunchRestoreWizard;
            if (handler != null)
            {
                handler(args);
            }
        }
    }
}