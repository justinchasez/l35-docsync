using System;
using System.ComponentModel;
using DocSync.Client.App.Core.Events.Args;

namespace DocSync.Client.App.Core.Events.Interface
{
    /// <summary>
    /// The restore wizard event manager.
    /// </summary>
    public interface IRestoreWizardEventManager
    {
        /// <summary>
        /// Occurs when [switch restore wizard view].
        /// </summary>
        event EventHandler<SwitchRestoreWizardTabEventArgs> SwitchRestoreWizardView;

        /// <summary>
        /// Raises the <see cref="E:SwitchRestoreWizardView"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        void OnSwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args);

        /// <summary>
        /// Occurs when [restore wizard close].
        /// </summary>
        event EventHandler<CancelEventArgs> RestoreWizardClose;

        /// <summary>
        /// Raises the <see cref="E:RestoreWizardClose"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        void OnRestoreWizardClose(CancelEventArgs args);

        /// <summary>
        /// Occurs when [restore wizard finished].
        /// </summary>
        event EventHandler RestoreWizardFinished;

        /// <summary>
        /// Raises the <see cref="E:RestoreWizardFinished"/> event.
        /// </summary>
        void OnRestoreWizardFinished();
    }
}