using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Remoting.Handlers;

namespace DocSync.Client.App.Core.Events.Interface
{
    /// <summary>
    /// The scheduling event manager.
    /// </summary>
    public interface IScheduleEventManager
    {
        /// <summary>
        /// Occurs when [backup schedule mode changed].
        /// </summary>
        event RemotingEventHandler<BackupScheduleModeChangedEventArgs> BackupScheduleModeChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupScheduleModeChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="BackupScheduleModeChangedEventArgs"/> instance containing the event data.</param>
        void OnBackupScheduleModeChanged(BackupScheduleModeChangedEventArgs args);

        /// <summary>
        /// Occurs when [schedule initialized].
        /// </summary>
        event RemotingEventHandler<ScheduleInitEventArgs> ScheduleInitialized;

        /// <summary>
        /// Raises the <see cref="E:ScheduleInitialized"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleInitEventArgs"/> instance containing the event data.</param>
        void OnScheduleInitialized(ScheduleInitEventArgs args);

        /// <summary>
        /// Occurs when [save schedule].
        /// </summary>
        event RemotingEventHandler<ScheduleEventArgs> ScheduleSaved;

        /// <summary>
        /// Raises the <see cref="E:ScheduleSaving"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        void OnScheduleSaved(ScheduleEventArgs args);

        /// <summary>
        /// Occurs when [remove schedule].
        /// </summary>
        event RemotingEventHandler<ScheduleEventArgs> ScheduleRemoved;

        /// <summary>
        /// Raises the <see cref="E:ScheduleRemoving"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        void OnScheduleRemoved(ScheduleEventArgs args);

        /// <summary>
        /// Occurs when [edit schedule].
        /// </summary>
        event EventHandler<ScheduleEventArgs> EditSchedule;

        /// <summary>
        /// Raises the <see cref="E:EditSchedule"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        void OnEditSchedule(ScheduleEventArgs args);
    }
}