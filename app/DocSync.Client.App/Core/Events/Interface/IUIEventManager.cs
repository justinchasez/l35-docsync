using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Core.Args;

namespace DocSync.Client.App.Core.Events.Interface
{
    /// <summary>
    /// The event manager for UI.
    /// </summary>
    public interface IUIEventManager
    {
        /// <summary>
        /// Occurs when [switch tab].
        /// </summary>
        event EventHandler<SwitchTabEventArgs> SwitchTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchTabEventArgs"/> instance containing the event data.</param>
        void OnSwitchTab(SwitchTabEventArgs args);

        /// <summary>
        /// Occurs when [switch status tab].
        /// </summary>
        event EventHandler<SwitchStatusTabEventArgs> SwitchStatusTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchStatusTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchStatusTabEventArgs"/> instance containing the event data.</param>
        void OnSwitchStatusTab(SwitchStatusTabEventArgs args);

        /// <summary>
        /// Occurs when [switch options tab].
        /// </summary>
        event EventHandler<SwitchOptionsTabEventArgs> SwitchOptionsTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchOptionsTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchOptionsTabEventArgs"/> instance containing the event data.</param>
        void OnSwitchOptionsTab(SwitchOptionsTabEventArgs args);

        /// <summary>
        /// Occurs when [need to set options].
        /// </summary>
        event EventHandler<SetOptionsEventArgs> SetOptions;

        /// <summary>
        /// Raises the <see cref="E:SetOptionsEventArgs"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        void OnSetOptions(SetOptionsEventArgs args);

        /// <summary>
        /// Occurs when [need to set options].
        /// </summary>
        event EventHandler<SetOptionsEventArgs> LaunchRestoreWizard;

        /// <summary>
        /// Raises the <see cref="E:SetOptionsEventArgs"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        void OnLaunchRestoreWizard(SetOptionsEventArgs args);
    }
}