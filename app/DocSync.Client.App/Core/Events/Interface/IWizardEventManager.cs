using DocSync.Client.App.Core.Events.Args;

namespace DocSync.Client.App.Core.Events.Interface
{
    /// <summary>
    /// Computer added delegate
    /// </summary>
    /// <param name="computerEventArgs">The computer event arguments</param>
    /// <returns>Is success added</returns>
    public delegate bool ComputerAddedDelegate(ComputerEventArgs computerEventArgs);

    /// <summary>
    /// The wizard event manager.
    /// </summary>
    public interface IWizardEventManager
    {
        /// <summary>
        /// Occurs when [switch tab].
        /// </summary>
        event EventHandler<SwitchWizardTabEventArgs> SwitchWizardTab;

        /// <summary>
        /// Raises the <see cref="E:SwitchWizardTab"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SwitchWizardTabEventArgs"/> instance containing the event data.</param>
        void OnSwitchWizardTab(SwitchWizardTabEventArgs args);

        /// <summary>
        /// Occurs when [computer selected].
        /// </summary>
        event EventHandler<ComputerEventArgs> ComputerSelected;

        /// <summary>
        /// Raises the <see cref="E:ComputerSelected"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        void OnComputerSelected(ComputerEventArgs args);

        /// <summary>
        /// Occurs when [computer added].
        /// </summary>
        event ComputerAddedDelegate ComputerAdded;

        /// <summary>
        /// Raises the <see cref="E:ComputerAdded"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        /// <returns>Is computer added</returns>
        bool OnComputerAdded(ComputerEventArgs args);
    }
}