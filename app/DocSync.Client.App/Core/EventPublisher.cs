using DocSync.Client.App.Core.Events;
using DocSync.Client.App.Core.Events.Interface;

namespace DocSync.Client.App.Core
{
    /// <summary>
    /// The main event manager
    /// </summary>
    internal static class EventPublisher
    {
        #region Event managers fields

        /// <summary>
        /// The event manger field for UI.
        /// </summary>
        private static IUIEventManager interfaceEventManager;

        /// <summary>
        /// The event manger field for schedule.
        /// </summary>
        private static IScheduleEventManager scheduleEventManager;

        /// <summary>
        /// The event manger field for wizard.
        /// </summary>
        private static IWizardEventManager wizardEventManager;

        /// <summary>
        /// The event manager field for retore wizard.
        /// </summary>
        private static IRestoreWizardEventManager restoreWizardEventManager;

        #endregion

        /// <summary>
        /// Gets the schedule event proxy.
        /// </summary>
        /// <value>The schedule event proxy.</value>
        public static IScheduleEventManager ScheduleManager
        {
            get
            {
                if (scheduleEventManager == null)
                {
                    Initialize();
                }

                return scheduleEventManager;
            }
        }

        /// <summary>
        /// Gets the UI event manager instance.
        /// </summary>
        /// <value>The UI event manager instance.</value>
        public static IUIEventManager UIEventManagerInstance
        {
            get
            {
                if (interfaceEventManager == null)
                {
                    Initialize();
                }

                return interfaceEventManager;
            }
        }

        /// <summary>
        /// Gets the wizard event manager.
        /// </summary>
        /// <value>The wizard event manager.</value>
        public static IWizardEventManager WizardEventManager
        {
            get
            {
                if (wizardEventManager == null)
                {
                    Initialize();
                }

                return wizardEventManager;
            }
        }

        /// <summary>
        /// Gets the wizard event manager.
        /// </summary>
        /// <value>The wizard event manager.</value>
        public static IRestoreWizardEventManager RestoreWizardEventManager
        {
            get
            {
                if (restoreWizardEventManager == null)
                {
                    Initialize();
                }

                return restoreWizardEventManager;
            }
        }

        /// <summary>
        /// Initializes nested event managers.
        /// </summary>
        private static void Initialize()
        {
            interfaceEventManager = new UIEventManager();
            scheduleEventManager = new ScheduleEventManager();
            wizardEventManager = new WizardEventManager();
            restoreWizardEventManager = new RestoreWizardEventManager();
        }
    }
}
