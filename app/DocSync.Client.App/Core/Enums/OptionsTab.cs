namespace DocSync.Client.App.Core.Enums
{
    /// <summary>
    /// The options tab.
    /// </summary>
    public enum OptionsTab
    {
        /// <summary>
        /// The options tab.
        /// </summary>
        Options,

        /// <summary>
        /// The backup schedule tab.
        /// </summary>
        BackupSchedule
    }
}
