namespace DocSync.Client.App.Core.Enums
{
    /// <summary>
    /// The restore wizard tabs.
    /// </summary>
    public enum RestoreWizardTab
    {
        /// <summary>
        /// The initial tab.
        /// </summary>
        Start,

        /// <summary>
        /// The coputer selection tab
        /// </summary>
        ComputerSelect,

        /// <summary>
        /// The encryption key validation tab.
        /// </summary>
        EncryptionKey,

        /// <summary>
        /// The restore mode tab.
        /// </summary>
        RestoreMode,

        /// <summary>
        /// The account selection tab.
        /// </summary>
        AccountSelect,

        /// <summary>
        /// The summary  tab.
        /// </summary>
        Summary
    }
}