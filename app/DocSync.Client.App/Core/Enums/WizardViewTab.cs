namespace DocSync.Client.App.Core.Enums
{
    /// <summary>
    /// The wizard tabs enum.
    /// </summary>
    public enum WizardViewTab
    {
        /// <summary>
        /// The users tab.
        /// </summary>
        Users = 1,

        /// <summary>
        /// The computers management tab.
        /// </summary>
        Computers = 2,

        /// <summary>
        /// The new computer tab.
        /// </summary>
        NewComputer = 3,

        /// <summary>
        /// The encryption key management tab.
        /// </summary>
        EncryptionKey = 4,

        /// <summary>
        /// The encryption key validation tab.
        /// </summary>
        ValidateEncryptionKey = 5,

        /// <summary>
        /// The finish tab.
        /// </summary>
        Finish = 6
    }
}