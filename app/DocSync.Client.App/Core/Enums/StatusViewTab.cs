namespace DocSync.Client.App.Core.Enums
{
    /// <summary>
    /// The status tabs.
    /// </summary>
    public enum StatusViewTab
    {
        /// <summary>
        /// The backup status tab.
        /// </summary>
        Backup,

        /// <summary>
        /// The restore status tab.
        /// </summary>
        Restore
    }
}
