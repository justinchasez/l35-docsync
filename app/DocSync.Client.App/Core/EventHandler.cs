namespace DocSync.Client.App.Core
{
    /// <summary>
    /// The event handler for the event manager.
    /// </summary>
    /// <typeparam name="T">The type of EventArgs</typeparam>
    /// <param name="args">The class with data for event.</param>
    public delegate void EventHandler<T>(T args);
}
