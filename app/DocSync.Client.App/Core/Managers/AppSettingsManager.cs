using System.Configuration;
using DocSync.Client.Core.Utils;

namespace DocSync.Client.App.Core.Managers
{
    /// <summary>
    /// The application settings manager.
    /// </summary>
    public static class AppSettingsManager
    {
        #region App settings constants

        /// <summary>
        /// The site url.
        /// </summary>
		private const string SITE_URL_KEY = "Settings.SiteUrl";

        /// <summary>
        /// The view help page relative url.
        /// </summary>
		private const string VIEW_HELP_PAGE_URL_KEY = "Settings.ViewHelpPage";

        /// <summary>
        /// The tutorial settings key how to see or change what is backed up.
        /// </summary>
		private const string HOW_TO_SEE_OR_CHANGE_WHAT_IS_BACKED_UP_TUTORIAL_KEY = "Settings.Tutorials.HowToSeeOrChangeWhatIsBackedUp";

        /// <summary>
        /// The tutorial settings key how to change settings.
        /// </summary>
		private const string HOW_TO_CHANGE_SETTINGS_TUTORIAL_KEY = "Settings.Tutorials.HowToChangeSettings";

        /// <summary>
        /// The tutorial settings key how to restore files.
        /// </summary>
		private const string HOW_TO_RESTORE_FILES_TUTORIAL_KEY = "Settings.Tutorials.HowToRestoreFiles";

        /// <summary>
        /// The profile settings key.
        /// </summary>
		private const string PROFILE_PAGE_KEY = "Settings.ProfilePage";

        /// <summary>
        /// The renew page settings key.
        /// </summary>
		private const string RENEW_PLAN_PAGE_KEY = "Settings.RenewPage";

        /// <summary>
        /// The membership page settings key.
        /// </summary>
		private const string MEMBERSHIP_PAGE_KEY = "Settings.Membership";

        /// <summary>
        /// The refer friends page settings key
        /// </summary>
		private const string REFER_FRIENDS_PAGE_KEY = "Settings.ReferFriends";

        /// <summary>
        /// The how refer friends page settings key
        /// </summary>
		private const string HOW_REFER_FRIENDS_PAGE_KEY = "Settings.HowReferFriends";

        /// <summary>
        /// The coupon page key
        /// </summary>
		private const string COUPON_PAGE_KEY = "Settings.Coupon";

        #endregion

        /// <summary>
        /// Gets the SiteUrl app setting.
        /// </summary>
        public static string SiteUrl
        {
            get 
            { 
                return ConfigurationManager.AppSettings[SITE_URL_KEY];
            }
        }

        /// <summary>
        /// Gets the ViewHelpPage app setting.
        /// </summary>
        public static string ViewHelpPage
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[VIEW_HELP_PAGE_URL_KEY], UserName); }
        }

        #region Tutorials

        /// <summary>
        /// Gets the tutorial how to see or change what is backed up.
        /// </summary>
        /// <value>The tutorial how to see or change what is backed up.</value>
        public static string TutorialHowToSeeOrChangeWhatIsBackedUp
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[HOW_TO_SEE_OR_CHANGE_WHAT_IS_BACKED_UP_TUTORIAL_KEY], UserName); }
        }

        /// <summary>
        /// Gets the tutorial how to change settings.
        /// </summary>
        /// <value>The tutorial how to change settings.</value>
        public static string TutorialHowToChangeSettings
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[HOW_TO_CHANGE_SETTINGS_TUTORIAL_KEY], UserName); }
        }

        /// <summary>
        /// Gets the tutorial how to restore files.
        /// </summary>
        /// <value>The tutorial how to restore files.</value>
        public static string TutorialHowToRestoreFiles
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[HOW_TO_RESTORE_FILES_TUTORIAL_KEY], UserName); }
        }

        /// <summary>
        /// Gets the profile page URL.
        /// </summary>
        /// <value>The profile page URL.</value>
        public static string ProfilePageUrl
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[PROFILE_PAGE_KEY], UserName); }
        }

        /// <summary>
        /// Gets the renew page URL.
        /// </summary>
        /// <value>The renew page URL.</value>
        public static string RenewPageUrl
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[RENEW_PLAN_PAGE_KEY], UserName); }
        }

        /// <summary>
        /// Gets the membership page URL.
        /// </summary>
        /// <value>The membership page URL.</value>
        public static string MembershipPageUrl
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[MEMBERSHIP_PAGE_KEY], UserName); }
        }

        /// <summary>
        /// Gets the refer frieng page url
        /// </summary>
        public static string ReferFriendUrl
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[REFER_FRIENDS_PAGE_KEY], UserName); }
        }

        /// <summary>
        /// Gets the how refer friend url
        /// </summary>
        public static string HowReferFriendUrl
        {
            get { return UrlUtil.Combine(SiteUrl, ConfigurationManager.AppSettings[HOW_REFER_FRIENDS_PAGE_KEY], UserName); }
        }

        /// <summary>
        /// Gets the coupon url
        /// </summary>
        public static string CouponUrl
        {
            get { return string.Format("{0}/{1}", SiteUrl, ConfigurationManager.AppSettings[COUPON_PAGE_KEY]); }
        }

        #endregion

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        /// <value>The name of the user.</value>
        private static string UserName
        {
            get
            {
                return ClientSettingsManager.Settings.Credentials.Name;
            }
        }
        }
}
