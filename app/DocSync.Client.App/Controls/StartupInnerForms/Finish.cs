using System;
using System.IO;
using System.ServiceProcess;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Encryption;
using DocSync.Client.Core.Managers.Settings;
using DocSync.Client.Core.Utils;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The finish tab.
    /// </summary>
    public partial class Finish : WizardViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Finish"/> class.
        /// </summary>
        public Finish()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonFinish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonFinish_Click(object sender, EventArgs e)
        {
            bool isNeedBackupDesktop = false;

            StartupContainer container = this.GetContainer<StartupContainer>();

            int compId = container.SelectedComputer.Id;

            ClientSettingsManager.Settings.IsFirstRun = false;
            ClientSettingsManager.Settings.CurrentComputer = container.SelectedComputer;
            ClientSettingsManager.Settings.CurrentComputerEncryptInfo = container.SelectedComputerEncryptInfo ??
                                                                       EncryptInfo.LoadDefault();
            
            //ClientSettingsManager.Settings.BackupDesktop = this.rbtnManual.Checked;
            if (this.rbntAutomatic.Checked)
            {
                ClientSettingsManager.Settings.UsersForAutomaticBackup.Add(Environment.UserName);
                isNeedBackupDesktop = true;
            }

            try
            {
                RemotingManager.Instance.ProxyManager.ComputerManager.UpdateComputerGuid(compId);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Client: Can not update computer GUID in server");
            }

            ClientSettingsManager.Save();

            RemotingManager.Instance.ProxyManager.BackupManager.ResetInitEvent();

            ServiceController serviceController = new ServiceController("DocSyncService");
            serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_INIT);

            try
            {
                if (RemotingManager.Instance.ProxyManager.BackupManager.EnsureInited(10000))
                {
                    RemotingManager.Instance.ProxyManager.BackupManager.GetFilesFromServer(compId);
                }
                else
                {
                    Loger.Instance.Log("Client: Can not download file list from server");
                }
            }
            catch (Exception ex)
            {
                Loger.Instance.Log(ex.ToString());
            }

            if (isNeedBackupDesktop)
            {
                //RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
                //RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
				if (Directory.Exists("c:\\temp\\backup"))
				{
					RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand("c:\\temp\\backup");
				}
            }

            this.ParentForm.Close();
        }

        /// <summary>
        /// Cancel event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.GetContainer<StartupContainer>().Close();
        }

        /// <summary>
        /// Back event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnBack_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
            {
                SelectedTab = WizardViewTab.Computers
            });
        }
    }
}
