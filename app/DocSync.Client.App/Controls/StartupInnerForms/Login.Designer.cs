using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    partial class Login
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.label1 = new System.Windows.Forms.Label();
            this.rbtnAuthenticate = new System.Windows.Forms.RadioButton();
            this.rbtnRegister = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLoginEmail = new System.Windows.Forms.TextBox();
            this.txtLoginPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRegisterEmail = new System.Windows.Forms.TextBox();
            this.txtRegisterPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRegisterPasswordConfirm = new System.Windows.Forms.TextBox();
            this.btnLogin = new ImageButton();
            this.pnlAuthenticate = new System.Windows.Forms.Panel();
            this.pnlRegister = new System.Windows.Forms.Panel();
            this.btnCancel = new ImageButton();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.checkBoxUseProxy = new System.Windows.Forms.CheckBox();
            this.pnlAuthenticate.SuspendLayout();
            this.pnlRegister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(22, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Authentication";
            // 
            // rbtnAuthenticate
            // 
            this.rbtnAuthenticate.AutoSize = true;
            this.rbtnAuthenticate.Checked = true;
            this.rbtnAuthenticate.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnAuthenticate.Location = new System.Drawing.Point(44, 60);
            this.rbtnAuthenticate.Name = "rbtnAuthenticate";
            this.rbtnAuthenticate.Size = new System.Drawing.Size(130, 24);
            this.rbtnAuthenticate.TabIndex = 1;
            this.rbtnAuthenticate.TabStop = true;
            this.rbtnAuthenticate.Text = "Authenticate";
            this.rbtnAuthenticate.UseVisualStyleBackColor = true;
            this.rbtnAuthenticate.CheckedChanged += new System.EventHandler(this.RadioMode_CheckedChanged);
            // 
            // rbtnRegister
            // 
            this.rbtnRegister.AutoSize = true;
            this.rbtnRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnRegister.Location = new System.Drawing.Point(44, 123);
            this.rbtnRegister.Name = "rbtnRegister";
            this.rbtnRegister.Size = new System.Drawing.Size(234, 24);
            this.rbtnRegister.TabIndex = 4;
            this.rbtnRegister.Text = "Register && Authentication";
            this.rbtnRegister.UseVisualStyleBackColor = true;
            this.rbtnRegister.Visible = false;
            this.rbtnRegister.CheckedChanged += new System.EventHandler(this.RadioMode_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(175, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "(if you have registered)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(270, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(308, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "(if you haven\'t registered in the system yet)";
            this.label3.Visible = false;
            // 
            // txtLoginEmail
            // 
            this.txtLoginEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtLoginEmail.Location = new System.Drawing.Point(144, 7);
            this.txtLoginEmail.Name = "txtLoginEmail";
            this.txtLoginEmail.Size = new System.Drawing.Size(260, 26);
            this.txtLoginEmail.TabIndex = 2;
            // 
            // txtLoginPassword
            // 
            this.txtLoginPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtLoginPassword.Location = new System.Drawing.Point(144, 40);
            this.txtLoginPassword.Name = "txtLoginPassword";
            this.txtLoginPassword.Size = new System.Drawing.Size(260, 26);
            this.txtLoginPassword.TabIndex = 3;
            this.txtLoginPassword.UseSystemPasswordChar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "E-mail:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(3, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "E-mail:";
            this.label6.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label7.Location = new System.Drawing.Point(3, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Password:";
            this.label7.Visible = false;
            // 
            // txtRegisterEmail
            // 
            this.txtRegisterEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegisterEmail.Location = new System.Drawing.Point(144, 6);
            this.txtRegisterEmail.Name = "txtRegisterEmail";
            this.txtRegisterEmail.Size = new System.Drawing.Size(260, 26);
            this.txtRegisterEmail.TabIndex = 5;
            this.txtRegisterEmail.Visible = false;
            // 
            // txtRegisterPassword
            // 
            this.txtRegisterPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegisterPassword.Location = new System.Drawing.Point(144, 39);
            this.txtRegisterPassword.Name = "txtRegisterPassword";
            this.txtRegisterPassword.Size = new System.Drawing.Size(260, 26);
            this.txtRegisterPassword.TabIndex = 6;
            this.txtRegisterPassword.UseSystemPasswordChar = true;
            this.txtRegisterPassword.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label8.Location = new System.Drawing.Point(3, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Confirm password:";
            this.label8.Visible = false;
            // 
            // txtRegisterPasswordConfirm
            // 
            this.txtRegisterPasswordConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtRegisterPasswordConfirm.Location = new System.Drawing.Point(144, 72);
            this.txtRegisterPasswordConfirm.Name = "txtRegisterPasswordConfirm";
            this.txtRegisterPasswordConfirm.Size = new System.Drawing.Size(260, 26);
            this.txtRegisterPasswordConfirm.TabIndex = 7;
            this.txtRegisterPasswordConfirm.UseSystemPasswordChar = true;
            this.txtRegisterPasswordConfirm.Visible = false;
            // 
            // btnLogin
            // 
            this.btnLogin.AllowTransparency = false;
            this.btnLogin.AnimatePress = true;
            this.btnLogin.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnLogin.HoverImage = null;
            this.btnLogin.Location = new System.Drawing.Point(384, 353);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnLogin.NormalImage")));
            this.btnLogin.PressedImage = null;
            this.btnLogin.Size = new System.Drawing.Size(164, 33);
            this.btnLogin.TabIndex = 8;
            this.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnLogin.Click += new System.EventHandler(this.ButtonLogin_Click);
            // 
            // pnlAuthenticate
            // 
            this.pnlAuthenticate.Controls.Add(this.label4);
            this.pnlAuthenticate.Controls.Add(this.label5);
            this.pnlAuthenticate.Controls.Add(this.txtLoginEmail);
            this.pnlAuthenticate.Controls.Add(this.txtLoginPassword);
            this.pnlAuthenticate.Location = new System.Drawing.Point(59, 84);
            this.pnlAuthenticate.Name = "pnlAuthenticate";
            this.pnlAuthenticate.Size = new System.Drawing.Size(458, 71);
            this.pnlAuthenticate.TabIndex = 9;
            // 
            // pnlRegister
            // 
            this.pnlRegister.Controls.Add(this.label6);
            this.pnlRegister.Controls.Add(this.label7);
            this.pnlRegister.Controls.Add(this.label8);
            this.pnlRegister.Controls.Add(this.txtRegisterPasswordConfirm);
            this.pnlRegister.Controls.Add(this.txtRegisterEmail);
            this.pnlRegister.Controls.Add(this.txtRegisterPassword);
            this.pnlRegister.Enabled = false;
            this.pnlRegister.Location = new System.Drawing.Point(59, 153);
            this.pnlRegister.Name = "pnlRegister";
            this.pnlRegister.Size = new System.Drawing.Size(458, 102);
            this.pnlRegister.TabIndex = 9;
            this.pnlRegister.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(554, 353);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(101, 33);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // numPort
            // 
            this.numPort.Enabled = false;
            this.numPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.numPort.Location = new System.Drawing.Point(203, 279);
            this.numPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(260, 26);
            this.numPort.TabIndex = 61;
            this.numPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtAddress
            // 
            this.txtAddress.Enabled = false;
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAddress.Location = new System.Drawing.Point(203, 246);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(260, 26);
            this.txtAddress.TabIndex = 60;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label9.Location = new System.Drawing.Point(55, 276);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 59;
            this.label9.Text = "Server port:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(55, 246);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 20);
            this.label10.TabIndex = 58;
            this.label10.Text = "Server address:";
            // 
            // checkBoxUseProxy
            // 
            this.checkBoxUseProxy.AutoSize = true;
            this.checkBoxUseProxy.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxUseProxy.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.checkBoxUseProxy.ForeColor = System.Drawing.SystemColors.ControlText;
            this.checkBoxUseProxy.Location = new System.Drawing.Point(44, 220);
            this.checkBoxUseProxy.Name = "checkBoxUseProxy";
            this.checkBoxUseProxy.Size = new System.Drawing.Size(161, 24);
            this.checkBoxUseProxy.TabIndex = 57;
            this.checkBoxUseProxy.Text = "Use proxy server";
            this.checkBoxUseProxy.UseVisualStyleBackColor = false;
            this.checkBoxUseProxy.CheckedChanged += new System.EventHandler(this.CheckBoxUseProxy_CheckedChanged);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.numPort);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.checkBoxUseProxy);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlRegister);
            this.Controls.Add(this.pnlAuthenticate);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtnRegister);
            this.Controls.Add(this.rbtnAuthenticate);
            this.Controls.Add(this.label1);
            this.Name = "Login";
            this.Size = new System.Drawing.Size(678, 384);
            this.pnlAuthenticate.ResumeLayout(false);
            this.pnlAuthenticate.PerformLayout();
            this.pnlRegister.ResumeLayout(false);
            this.pnlRegister.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbtnAuthenticate;
        private System.Windows.Forms.RadioButton rbtnRegister;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLoginEmail;
        private System.Windows.Forms.TextBox txtLoginPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRegisterEmail;
        private System.Windows.Forms.TextBox txtRegisterPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRegisterPasswordConfirm;
        private ImageButton btnLogin;
        private System.Windows.Forms.Panel pnlAuthenticate;
        private System.Windows.Forms.Panel pnlRegister;
        private ImageButton btnCancel;
        private System.Windows.Forms.NumericUpDown numPort;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox checkBoxUseProxy;
    }
}
