using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    partial class Finish
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Finish));
            this.btnFinish = new ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbntAutomatic = new System.Windows.Forms.RadioButton();
            this.rbtnManual = new System.Windows.Forms.RadioButton();
            this.btnCancel = new ImageButton();
            this.btnBack = new ImageButton();
            this.SuspendLayout();
            // 
            // btnFinish
            // 
            this.btnFinish.AllowTransparency = false;
            this.btnFinish.AnimatePress = true;
            this.btnFinish.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnFinish.HoverImage = null;
            this.btnFinish.Location = new System.Drawing.Point(389, 275);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnFinish.NormalImage")));
            this.btnFinish.PressedImage = null;
            this.btnFinish.Size = new System.Drawing.Size(164, 33);
            this.btnFinish.TabIndex = 6;
            this.btnFinish.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnFinish.Click += new System.EventHandler(this.ButtonFinish_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(22, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 22);
            this.label1.TabIndex = 7;
            this.label1.Text = "Backup personal data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(41, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(586, 19);
            this.label2.TabIndex = 8;
            this.label2.Text = "Include Desktop and My Documents folders to the list of folders that should be ba" +
                "ckuped";
            // 
            // rbntAutomatic
            // 
            this.rbntAutomatic.AutoSize = true;
            this.rbntAutomatic.Checked = true;
            this.rbntAutomatic.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbntAutomatic.Location = new System.Drawing.Point(72, 86);
            this.rbntAutomatic.Name = "rbntAutomatic";
            this.rbntAutomatic.Size = new System.Drawing.Size(356, 23);
            this.rbntAutomatic.TabIndex = 11;
            this.rbntAutomatic.TabStop = true;
            this.rbntAutomatic.Text = "Automatically back-up Desktop and My Documents";
            this.rbntAutomatic.UseVisualStyleBackColor = true;
            // 
            // rbtnManual
            // 
            this.rbtnManual.AutoSize = true;
            this.rbtnManual.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnManual.Location = new System.Drawing.Point(72, 115);
            this.rbtnManual.Name = "rbtnManual";
            this.rbtnManual.Size = new System.Drawing.Size(139, 23);
            this.rbtnManual.TabIndex = 11;
            this.rbtnManual.Text = "I�ll do it manually";
            this.rbtnManual.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(559, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(101, 33);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(292, 275);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnBack.NormalImage")));
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(91, 33);
            this.btnBack.TabIndex = 12;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // Finish
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.rbtnManual);
            this.Controls.Add(this.rbntAutomatic);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFinish);
            this.Name = "Finish";
            this.Size = new System.Drawing.Size(678, 324);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton btnFinish;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbntAutomatic;
        private System.Windows.Forms.RadioButton rbtnManual;
        private ImageButton btnCancel;
        private ImageButton btnBack;
    }
}
