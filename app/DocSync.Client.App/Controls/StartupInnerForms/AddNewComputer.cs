using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Properties;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The new computer tab.
    /// </summary>
    public partial class AddNewComputer : WizardViewBase
    {
        /// <summary>
        /// The form error messages.
        /// </summary>
        private readonly List<string> errorMessages = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="AddNewComputer"/> class.
        /// </summary>
        public AddNewComputer()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonOK control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonOK_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm() && this.ValidateExistingComputer(this.txtComputerName.Text))
            {
                this.GetContainer<StartupContainer>().SelectedComputer = new ComputerInfo { Name = this.txtComputerName.Text };

                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.EncryptionKey });
            }
            else
            {
                MessageBox.Show(String.Join(Environment.NewLine, this.errorMessages.ToArray()), Resources.WizardNewComputerErrorDialogTitle);
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.Computers });
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>True if form is valid, otherwise false.</returns>
        private bool ValidateForm()
        {
            bool result = true;
            this.errorMessages.Clear();

            if (String.IsNullOrEmpty(this.txtComputerName.Text) 
                || (this.txtComputerName.Text.Length < 3)
                || (this.txtComputerName.Text.Length > 30))
            {
                result = false;
                this.errorMessages.Add(Resources.WizardNewComputerNameShouldBeProvided);
            }

            return result;
        }

        /// <summary>
        /// Validates the existing computer.
        /// </summary>
        /// <param name="computerName">Name of the computer.</param>
        /// <returns>True if computer doesn't exists, othrewise false.</returns>
        private bool ValidateExistingComputer(string computerName)
        {
            var result = true;
            StartupContainer container = this.GetContainer<StartupContainer>();
            ComputerInfo existingComputer = container.Computers.Find(x => x.Name.Equals(computerName, StringComparison.InvariantCultureIgnoreCase));

            if (existingComputer != null)
            {
                result = false;
                this.errorMessages.Add(Resources.WizardNewComputerExists);
            }

            return result;
        }
    }
}
