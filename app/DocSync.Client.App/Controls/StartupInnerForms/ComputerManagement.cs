using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Core.Enums;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The computer management tab.
    /// </summary>
    public partial class ComputerManagement : WizardViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerManagement"/> class.
        /// </summary>
        public ComputerManagement()
        {
            this.InitializeComponent();
            this.VisibleChanged += this.ComputerManagement_VisibleChanged;

            this.Load += this.ComputerManagement_Load;
        }

        /// <summary>
        /// Visible changed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void ComputerManagement_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                this.InitializeComputers();
            }
        }

        /// <summary>
        /// Handles the Load event of the ComputerManagement control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ComputerManagement_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
            {
                return;
            }

            //this.InitializeComputers();

            EventPublisher.WizardEventManager.ComputerAdded += this.WizardEventManager_ComputerAdded;
        }

        /// <summary>
        /// Wizards the event manager_ computer added.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        /// <returns>Is computer added</returns>
        private bool WizardEventManager_ComputerAdded(ComputerEventArgs args)
        {
            string url = null;

            try
            {
                url = RemotingManager.Instance.ProxyManager.ComputerManager.AddComputer(args.ComputerInfo);
            }
            catch (Exception)
            {
				CustomMessageBox.Show(this, "DocSync  is not responding at the moment, please try again in a few minutes.", "DocSync is not responding at the moment, please try again in a few minutes.", CustomMessageBoxButtons.OK);
                return false;
            }

            if (!String.IsNullOrEmpty(url))
            {
                Process.Start(url);
            }
            else
            {
                this.InitializeComputers();
            }

            return true;
        }

        /// <summary>
        /// Handles the Click event of the ButtonAddComputer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonAddComputer_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.NewComputer });
        }

        /// <summary>
        /// Handles the Click event of the ButtonRefresh control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            this.GetContainer<StartupContainer>().Computers.Clear();
            this.InitializeComputers();
        }

        /// <summary>
        /// Initializes the computers.
        /// </summary>
        private void InitializeComputers()
        {
            this.pnlComputersContainer.Controls.Clear();

            List<ComputerInfo> computerInfos = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();
            this.GetContainer<StartupContainer>().Computers = computerInfos;

            this.label3.Visible = computerInfos.Count > 0;

            foreach (ComputerInfo currentComputerInfo in computerInfos)
            {
                ComputerInfoRow computerInfoRow = new ComputerInfoRow(currentComputerInfo) { Dock = DockStyle.Top };
                computerInfoRow.ComputerSelected += this.ComputerInfoRow_ComputerSelected;
                this.pnlComputersContainer.Controls.Add(computerInfoRow);
            }
        }

        /// <summary>
        /// Computers the info row_ computer selected.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        private void ComputerInfoRow_ComputerSelected(ComputerEventArgs args)
        {
            this.GetContainer<StartupContainer>().SelectedComputer = args.ComputerInfo;

            if (args.ComputerInfo.ManualEncriptionState)
            {
                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                                                                        {
                                                                            SelectedTab =
                                                                                WizardViewTab.ValidateEncryptionKey
                                                                        });
            }
            else
            {
                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                                                                        {
                                                                            SelectedTab = WizardViewTab.Finish
                                                                        });
            }
        }

        /// <summary>
        /// Back event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnBack_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
            {
                SelectedTab = WizardViewTab.Users
            });
        }

        /// <summary>
        /// Cancel event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.GetContainer<StartupContainer>().Close();
        }
    }
}
