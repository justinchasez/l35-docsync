using System;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Properties;
using DocSync.Client.Core.Entities.Encryption;
using DocSync.Client.Core.Managers.Text;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The validate encryption key tab.
    /// </summary>
    public partial class ValidateEncryptionKey : WizardViewBase
    {
        /// <summary>
        /// The current encrypt info.
        /// </summary>
        private EncryptInfo encryptInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateEncryptionKey"/> class.
        /// </summary>
        public ValidateEncryptionKey()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonOk control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonOk_Click(object sender, EventArgs e)
        {
            if (this.ValidateEncryptInfo() && this.ValidatePassword())
            {
                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.Finish });
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.Computers });
        }

        /// <summary>
        /// Handles the Click event of the ButtonBrowse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBrowse_Click(object sender, EventArgs e)
        {
            if (this.ofdEncryptionKey.ShowDialog() == DialogResult.OK)
            {
                if (!String.IsNullOrEmpty(this.ofdEncryptionKey.FileName))
                {
                    this.txtEncryptionKeyPath.Text = this.ofdEncryptionKey.FileName;

                    this.encryptInfo = EncryptInfo.LoadFromFile(this.ofdEncryptionKey.FileName);

                    if (this.encryptInfo != null)
                    {
                        this.GetContainer<StartupContainer>().SelectedComputerEncryptInfo = this.encryptInfo;

                        this.lblHint.Text = this.encryptInfo.Hint;

                        return;
                    }
                }

                this.lblHint.Text = String.Empty;
                this.encryptInfo = null;
                MessageBox.Show(Resources.WizardValidateEncryptionKeyInvalidKeyFile);
            }
        }

        /// <summary>
        /// Validates the encrypt info.
        /// </summary>
        /// <returns>Results of the validation</returns>
        private bool ValidateEncryptInfo()
        {
            if (this.encryptInfo != null)
            {
                return true;
            }
            else
            {
                MessageBox.Show(Resources.WizardValidateEncryptionKeyInvalidKeyFile);
                return false;
            }
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        /// <returns>Results of the validation</returns>
        private bool ValidatePassword()
        {
            if (TextManager.ComputeHash(this.txtPassword.Text) == this.encryptInfo.PasswordHash)
            {
                return true;
            }
            else
            {
                MessageBox.Show(Resources.WizardValidateEncryptionKeyInvalidPassword);
                return false;
            }
        }
    }
}
