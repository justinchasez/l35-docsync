using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    partial class ValidateEncryptionKey
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtEncryptionKeyPath = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnCancel = new ImageButton();
            this.btnOk = new ImageButton();
            this.btnBrowse = new ImageButton();
            this.ofdEncryptionKey = new System.Windows.Forms.OpenFileDialog();
            this.lblHint = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label2.Location = new System.Drawing.Point(45, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(325, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Please, input an encryption key for this computer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(22, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 22);
            this.label1.TabIndex = 3;
            this.label1.Text = "Encryption key validation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label3.Location = new System.Drawing.Point(62, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "File with key:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label4.Location = new System.Drawing.Point(62, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password:";
            // 
            // txtEncryptionKeyPath
            // 
            this.txtEncryptionKeyPath.BackColor = System.Drawing.Color.White;
            this.txtEncryptionKeyPath.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtEncryptionKeyPath.Location = new System.Drawing.Point(167, 89);
            this.txtEncryptionKeyPath.Name = "txtEncryptionKeyPath";
            this.txtEncryptionKeyPath.ReadOnly = true;
            this.txtEncryptionKeyPath.Size = new System.Drawing.Size(317, 27);
            this.txtEncryptionKeyPath.TabIndex = 5;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.Location = new System.Drawing.Point(167, 122);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(317, 27);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(493, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.AllowTransparency = false;
            this.btnOk.AnimatePress = true;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnOk.HoverImage = null;
            this.btnOk.Location = new System.Drawing.Point(323, 275);
            this.btnOk.Name = "btnOk";
            this.btnOk.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonOk;
            this.btnOk.PressedImage = null;
            this.btnOk.Size = new System.Drawing.Size(164, 33);
            this.btnOk.TabIndex = 9;
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnOk.Click += new System.EventHandler(this.ButtonOk_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.AllowTransparency = false;
            this.btnBrowse.AnimatePress = true;
            this.btnBrowse.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBrowse.HoverImage = null;
            this.btnBrowse.Location = new System.Drawing.Point(493, 87);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonBrowse;
            this.btnBrowse.PressedImage = null;
            this.btnBrowse.Size = new System.Drawing.Size(164, 33);
            this.btnBrowse.TabIndex = 9;
            this.btnBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBrowse.Click += new System.EventHandler(this.ButtonBrowse_Click);
            // 
            // ofdEncryptionKey
            // 
            this.ofdEncryptionKey.Filter = "Encryption key (*.pem)|*.pem|All files (*.*)|*.*";
            // 
            // lblHint
            // 
            this.lblHint.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblHint.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblHint.ForeColor = System.Drawing.Color.Red;
            this.lblHint.Location = new System.Drawing.Point(45, 166);
            this.lblHint.Name = "lblHint";
            this.lblHint.Size = new System.Drawing.Size(612, 97);
            this.lblHint.TabIndex = 10;
            // 
            // ValidateEncryptionKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblHint);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEncryptionKeyPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ValidateEncryptionKey";
            this.Size = new System.Drawing.Size(678, 324);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtEncryptionKeyPath;
        private System.Windows.Forms.TextBox txtPassword;
        private ImageButton btnCancel;
        private ImageButton btnOk;
        private ImageButton btnBrowse;
        private System.Windows.Forms.OpenFileDialog ofdEncryptionKey;
        private System.Windows.Forms.Label lblHint;
    }
}
