using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Computers;
using DocSync.Client.Core.Entities.Encryption;
using DocSync.Client.Core.Managers.Text;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The encryption key management form.
    /// </summary>
    public partial class EncryptionKey : WizardViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptionKey"/> class.
        /// </summary>
        public EncryptionKey()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonSave_Click(object sender, EventArgs e)
        {
            ComputerInfo currentCompInfo = this.GetContainer<StartupContainer>().SelectedComputer;
         //   currentCompInfo.IsManualEncriptionEnabled = this.rbtnManual.Checked;
            currentCompInfo.ManualEncriptionState = this.rbtnManual.Checked;
            currentCompInfo.ManualEncriptionStateSpecified = true;

            if (this.rbtnManual.Checked)
            {
                if (this.ValidatePassword(this.txtPassword.Text, this.txtConfirm.Text))
                {
                    if (this.txtHint.Text.Length > 2 && this.txtHint.Text.Length < 31)
                    {
                        EncryptInfo keyInfo = EncryptInfo.GenerateEncryptionInfo();
                        keyInfo.PasswordHash = TextManager.ComputeHash(this.txtPassword.Text);
                        keyInfo.Hint = this.txtHint.Text;

                        this.sfdEncryptionKey.FileName = currentCompInfo.Name.ToLower();

                        if (this.sfdEncryptionKey.ShowDialog() == DialogResult.OK)
                        {
                            EncryptInfo.SaveToFile(keyInfo, this.sfdEncryptionKey.FileName);

                            if (EventPublisher.WizardEventManager.OnComputerAdded(new ComputerEventArgs(currentCompInfo)))
                            {
                                this.InitializeCurrentComp();

                                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                                                                                        {
                                                                                            SelectedTab = WizardViewTab.Finish
                                                                                        });
                            }
                            else
                            {
                                EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                                {
                                    SelectedTab = WizardViewTab.Computers
                                });
                            }
                        }  
                    }
                    else
                    {
                        MessageBox.Show("The hint length must be between 3 and 30 characters.");
                    }
                }
                else
                {
                    MessageBox.Show("The password or username is invalid. Please review your data.");
                }
            }
            else
            {
                if (EventPublisher.WizardEventManager.OnComputerAdded(new ComputerEventArgs(currentCompInfo)))
                {
                    this.InitializeCurrentComp();

                    EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                                                                            {
                                                                                SelectedTab = WizardViewTab.Finish
                                                                            });
                }
                else
                {
                    EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs
                    {
                        SelectedTab = WizardViewTab.Computers
                    });
                }
            }
        }

        /// <summary>
        /// Initialize current computer
        /// </summary>
        private void InitializeCurrentComp()
        {
            List<ComputerInfo> computerInfos = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();
            this.GetContainer<StartupContainer>().Computers = computerInfos;

            foreach (ComputerInfo computerInfo in computerInfos)
            {
                if (computerInfo.Name.Equals(this.GetContainer<StartupContainer>().SelectedComputer.Name))
                {
                    this.GetContainer<StartupContainer>().SelectedComputer = computerInfo;
                    break;
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.Computers });
        }

        /// <summary>
        /// Handles the CheckedChanged event of the KeyManagementMode control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void KeyManagementMode_CheckedChanged(object sender, EventArgs e)
        {
            this.pnlManualEncKey.Enabled = this.rbtnManual.Checked;
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        /// <param name="pwd">The password.</param>
        /// <param name="confirm">The confirm.</param>
        /// <returns>True if password and confiramtion are valid, otherwise false.</returns>
        private bool ValidatePassword(string pwd, string confirm)
        {
            return !String.IsNullOrEmpty(pwd) && !String.IsNullOrEmpty(confirm) && pwd.Length > 2 && pwd.Length < 31 && pwd.Equals(confirm);
        }
    }
}
