using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    partial class EncryptionKey
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnAutomatic = new System.Windows.Forms.RadioButton();
            this.rbtnManual = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtConfirm = new System.Windows.Forms.TextBox();
            this.txtHint = new System.Windows.Forms.TextBox();
            this.btnCancel = new ImageButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSave = new ImageButton();
            this.pnlManualEncKey = new System.Windows.Forms.Panel();
            this.sfdEncryptionKey = new System.Windows.Forms.SaveFileDialog();
            this.pnlManualEncKey.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbtnAutomatic
            // 
            this.rbtnAutomatic.AutoSize = true;
            this.rbtnAutomatic.Checked = true;
            this.rbtnAutomatic.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnAutomatic.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.rbtnAutomatic.Location = new System.Drawing.Point(43, 51);
            this.rbtnAutomatic.Name = "rbtnAutomatic";
            this.rbtnAutomatic.Size = new System.Drawing.Size(348, 23);
            this.rbtnAutomatic.TabIndex = 0;
            this.rbtnAutomatic.TabStop = true;
            this.rbtnAutomatic.Text = "Allow System to manage the encryption key";
            this.rbtnAutomatic.UseVisualStyleBackColor = true;
            this.rbtnAutomatic.CheckedChanged += new System.EventHandler(this.KeyManagementMode_CheckedChanged);
            // 
            // rbtnManual
            // 
            this.rbtnManual.AutoSize = true;
            this.rbtnManual.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnManual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.rbtnManual.Location = new System.Drawing.Point(43, 80);
            this.rbtnManual.Name = "rbtnManual";
            this.rbtnManual.Size = new System.Drawing.Size(349, 23);
            this.rbtnManual.TabIndex = 2;
            this.rbtnManual.Text = "I want to manage the encryption key by myself";
            this.rbtnManual.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(22, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "Encryption key management";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label2.Location = new System.Drawing.Point(62, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(239, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Input a password for encryption key";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.Location = new System.Drawing.Point(141, 14);
            this.txtPassword.MaxLength = 30;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(296, 25);
            this.txtPassword.TabIndex = 2;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtConfirm
            // 
            this.txtConfirm.BackColor = System.Drawing.SystemColors.Window;
            this.txtConfirm.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtConfirm.Location = new System.Drawing.Point(141, 45);
            this.txtConfirm.MaxLength = 30;
            this.txtConfirm.Name = "txtConfirm";
            this.txtConfirm.Size = new System.Drawing.Size(296, 25);
            this.txtConfirm.TabIndex = 3;
            this.txtConfirm.UseSystemPasswordChar = true;
            // 
            // txtHint
            // 
            this.txtHint.BackColor = System.Drawing.SystemColors.Window;
            this.txtHint.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtHint.Location = new System.Drawing.Point(141, 76);
            this.txtHint.Name = "txtHint";
            this.txtHint.Size = new System.Drawing.Size(296, 25);
            this.txtHint.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(497, 279);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label4.Location = new System.Drawing.Point(3, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 19);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label5.Location = new System.Drawing.Point(3, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Confirm password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label6.Location = new System.Drawing.Point(3, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 19);
            this.label6.TabIndex = 4;
            this.label6.Text = "Hint:";
            // 
            // btnSave
            // 
            this.btnSave.AllowTransparency = false;
            this.btnSave.AnimatePress = true;
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnSave.HoverImage = null;
            this.btnSave.Location = new System.Drawing.Point(327, 279);
            this.btnSave.Name = "btnSave";
            this.btnSave.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonSave;
            this.btnSave.PressedImage = null;
            this.btnSave.Size = new System.Drawing.Size(164, 33);
            this.btnSave.TabIndex = 5;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // pnlManualEncKey
            // 
            this.pnlManualEncKey.Controls.Add(this.label4);
            this.pnlManualEncKey.Controls.Add(this.txtConfirm);
            this.pnlManualEncKey.Controls.Add(this.txtPassword);
            this.pnlManualEncKey.Controls.Add(this.txtHint);
            this.pnlManualEncKey.Controls.Add(this.label5);
            this.pnlManualEncKey.Controls.Add(this.label6);
            this.pnlManualEncKey.Enabled = false;
            this.pnlManualEncKey.Location = new System.Drawing.Point(66, 140);
            this.pnlManualEncKey.Name = "pnlManualEncKey";
            this.pnlManualEncKey.Size = new System.Drawing.Size(448, 111);
            this.pnlManualEncKey.TabIndex = 6;
            // 
            // sfdEncryptionKey
            // 
            this.sfdEncryptionKey.Filter = "Encryption key (*.pem)|*.pem|All files (*.*)|*.*";
            // 
            // EncryptionKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlManualEncKey);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtnManual);
            this.Controls.Add(this.rbtnAutomatic);
            this.Name = "EncryptionKey";
            this.Size = new System.Drawing.Size(678, 324);
            this.pnlManualEncKey.ResumeLayout(false);
            this.pnlManualEncKey.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnAutomatic;
        private System.Windows.Forms.RadioButton rbtnManual;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtConfirm;
        private System.Windows.Forms.TextBox txtHint;
        private ImageButton btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private ImageButton btnSave;
        private System.Windows.Forms.Panel pnlManualEncKey;
        private System.Windows.Forms.SaveFileDialog sfdEncryptionKey;
    }
}
