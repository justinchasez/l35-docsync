using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Core.Enums;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Controls.Properties;
using DocSync.Client.Core.Entities.Computers;
using DocSync.Client.Core.Entities.Encryption;
using DocSync.Client.Core.Entities.Users;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Managers.Settings;
using DocSync.Client.Core.Utils;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    /// <summary>
    /// The users login tab.
    /// </summary>
    public partial class Login : WizardViewBase
    {
        /// <summary>
        /// The form error messages.
        /// </summary>
        private readonly List<string> errorMessages = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Login"/> class.
        /// </summary>
        public Login()
        {
            this.InitializeComponent();

            this.Load += this.Login_Load;
        }

        /// <summary>
        /// Login form loaded event
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void Login_Load(object sender, EventArgs e)
        {
            this.checkBoxUseProxy.Checked = ClientSettingsManager.Settings.IsProxyServerUsed;

            if (!string.IsNullOrEmpty(ClientSettingsManager.Settings.ProxyServerAddress))
            {
                this.txtAddress.Text = ClientSettingsManager.Settings.ProxyServerAddress;
            }

            if (ClientSettingsManager.Settings.ProxyServerPort > 0)
            {
                this.numPort.Value = Convert.ToDecimal(ClientSettingsManager.Settings.ProxyServerPort);
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            bool authenticationResult = false;

            if (this.ValidateForm())
            {
                ClientSettingsManager.Settings.IsProxyServerUsed = this.checkBoxUseProxy.Checked;
                ClientSettingsManager.Settings.ProxyServerAddress = this.txtAddress.Text;
                ClientSettingsManager.Settings.ProxyServerPort = Convert.ToInt32(this.numPort.Value);
                ClientSettingsManager.Save();

                if (this.rbtnAuthenticate.Checked)
                {
                    try
                    {
                        authenticationResult = RemotingManager.Instance.ProxyManager.UserManager.ValidateUser(this.txtLoginEmail.Text, this.txtLoginPassword.Text);
                    }
                    catch (Exception)
                    {
                        CustomMessageBox.Show(this, "The service for validate user not answer", "Service not answer", CustomMessageBoxButtons.OK);
                        return;
                    }
                }

                if (this.rbtnRegister.Checked)
                {
                    UserRegistrationInputs registrationinfo = new UserRegistrationInputs
                                                                  {
                                                                      Email = this.txtRegisterEmail.Text,
                                                                      Password = this.txtRegisterPasswordConfirm.Text
                                                                  };
                    
                    UserRegistrationResult result = null;

                    try
                    {
                        result = RemotingManager.Instance.ProxyManager.UserManager.RegisterUser(registrationinfo);
                    }
                    catch (Exception)
                    {
						CustomMessageBox.Show(this, "DocSync is not responding at the moment, please try again in a few minutes.", "DocSync is not responding at the moment, please try again in a few minutes.", CustomMessageBoxButtons.OK);
                        return;
                    }
                    
                    if (result.CreateStatus == MembershipCreateStatus.Success)
                    {
                        try
                        {
                            authenticationResult = RemotingManager.Instance.ProxyManager.UserManager.ValidateUser(registrationinfo.Email, registrationinfo.Password);
                        }
                        catch (Exception)
                        {
							CustomMessageBox.Show(this, "DocSync is not responding at the moment, please try again in a few minutes.", "DocSync is not responding at the moment, please try again in a few minutes.", CustomMessageBoxButtons.OK);
                            return;
                        }
                    }
                    else
                    {
                        this.errorMessages.Add(result.ErrorMessage);
                    }
                }

                if (authenticationResult)
                {
                    ClientSettingsManager.Settings.Credentials.Name = this.rbtnAuthenticate.Checked ? this.txtLoginEmail.Text : this.txtRegisterEmail.Text;
                    ClientSettingsManager.Settings.Credentials.Password = this.rbtnAuthenticate.Checked ? this.txtLoginPassword.Text : this.txtRegisterPassword.Text;
                    ClientSettingsManager.Save();

                    RemotingManager.Instance.ProxyManager.BackupManager.ResetInitEvent();

                    ServiceController serviceController = new ServiceController("DocSyncService");
                    serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_INIT);

                    Thread thr = new Thread(() =>
                                                {
                                                    if (RemotingManager.Instance.ProxyManager.BackupManager.EnsureInited(10000))
                                                    {
                                                        this.BeginInvoke(new MethodInvoker(this.DefaultFirstRun));
                                                    }
                                                });
                    thr.Start();

                    //EventPublisher.WizardEventManager.OnSwitchWizardTab(new SwitchWizardTabEventArgs { SelectedTab = WizardViewTab.Computers });
                }
                else
                {
                    MessageBox.Show("Login or registration failed. Please review your data.");
                }
            }
            else
            {
                MessageBox.Show(String.Join(Environment.NewLine, this.errorMessages.ToArray()), "Form errors");
            }
        }

        /// <summary>
        /// Default first run
        /// </summary>
        private void DefaultFirstRun()
        {
            List<ComputerInfo> computerInfos;
            try
            {
                computerInfos = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();
            }
            catch (Exception)
            {
                MessageBox.Show("Get computers failed. Please log in again.");
                return;
            }
            
            this.GetContainer<StartupContainer>().Computers = computerInfos;

            ComputerInfo currentComputer;

            if (computerInfos.Count > 0)
            {
                currentComputer = computerInfos[0];
            }
            else
            {
                currentComputer = new ComputerInfo { Name = "Computer 1" };

                if (!this.AddComputer(currentComputer))
                {
                    return;
                }

                currentComputer = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers()[0];
            }

            ClientSettingsManager.Settings.IsFirstRun = false;
            ClientSettingsManager.Settings.CurrentComputer = currentComputer;
            ClientSettingsManager.Settings.CurrentComputerEncryptInfo = EncryptInfo.LoadDefault();
            ClientSettingsManager.Settings.UsersForAutomaticBackup.Add(Environment.UserName);

            try
            {
                RemotingManager.Instance.ProxyManager.ComputerManager.UpdateComputerGuid(currentComputer.Id);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Client: Can not update computer GUID in server");
            }

            ClientSettingsManager.Save();

            RemotingManager.Instance.ProxyManager.BackupManager.ResetInitEvent();

			ServiceController serviceController = new ServiceController("DocSyncService");
            serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_INIT);

            try
            {
                if (RemotingManager.Instance.ProxyManager.BackupManager.EnsureInited(10000))
                {
                    RemotingManager.Instance.ProxyManager.BackupManager.GetFilesFromServer(currentComputer.Id);
                }
                else
                {
                    Loger.Instance.Log("Client: Can not download file list from server");
                }
            }
            catch (Exception ex)
            {
                Loger.Instance.Log(ex.ToString());
            }

            //RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            //RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
			if (Directory.Exists("c:\\temp\\backup"))
			{
				RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand("c:\\temp\\backup");
			}

            this.ParentForm.Close();
        }

        /// <summary>
        /// Add new computer
        /// </summary>
        /// <param name="computer">A new computer</param>
        /// <returns>Is add success</returns>
        private bool AddComputer(ComputerInfo computer)
        {
            string url = null;

            try
            {
                url = RemotingManager.Instance.ProxyManager.ComputerManager.AddComputer(computer);
            }
            catch (Exception)
            {
				CustomMessageBox.Show(this, "DocSync is not responding at the moment, please try again in a few minutes.", "DocSync is not responding at the moment, please try again in a few minutes.", CustomMessageBoxButtons.OK);
                return false;
            }

            if (!String.IsNullOrEmpty(url))
            {
                Process.Start(url);
            }

            return true;
        }

        /// <summary>
        /// Handles the CheckedChanged event of the RadioMode control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RadioMode_CheckedChanged(object sender, EventArgs e)
        {
            this.pnlAuthenticate.Enabled = this.rbtnAuthenticate.Checked;
            this.pnlRegister.Enabled = this.rbtnRegister.Checked;
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns>True of form is valid, otherwise false.</returns>
        private bool ValidateForm()
        {
            bool result = true;
            this.errorMessages.Clear();

            if (this.rbtnAuthenticate.Checked)
            {
                if (!String.IsNullOrEmpty(this.txtLoginEmail.Text))
                {
                    if (!this.ValidateEmail(this.txtLoginEmail.Text))
                    {
                        result = false;
                        this.errorMessages.Add(Resources.WizardLoginErrorEmailInvalid);
                    }
                }
                else
                {
                    result = false;
                    this.errorMessages.Add(Resources.WizardLoginErrorEmailShouldBeProvided);
                }

                if (String.IsNullOrEmpty(this.txtLoginPassword.Text))
                {
                    result = false;
                    this.errorMessages.Add(Resources.WizardLoginErrorPasswordShouldBeProvided);
                }
            }

            if (this.rbtnRegister.Checked)
            {
                if (!String.IsNullOrEmpty(this.txtRegisterEmail.Text))
                {
                    if (!this.ValidateEmail(this.txtRegisterEmail.Text))
                    {
                        result = false;
                        this.errorMessages.Add(Resources.WizardLoginErrorEmailInvalid);
                    }
                }
                else
                {
                    result = false;
                    this.errorMessages.Add(Resources.WizardLoginErrorEmailShouldBeProvided);
                }

                if (String.IsNullOrEmpty(this.txtRegisterPassword.Text) || String.IsNullOrEmpty(this.txtRegisterPasswordConfirm.Text))
                {
                    result = false;
                    this.errorMessages.Add(Resources.WizardLoginErrorPasswordShouldBeProvided);
                }

                if (this.txtRegisterPassword.Text != this.txtRegisterPasswordConfirm.Text)
                {
                    result = false;
                    this.errorMessages.Add(Resources.WizardLoginErrorPasswordNotConfirmed);
                }
            }

            if (this.checkBoxUseProxy.Checked)
            {
                Uri validationUri;

                if (string.IsNullOrEmpty(this.txtAddress.Text) || !Uri.TryCreate(this.txtAddress.Text, UriKind.RelativeOrAbsolute, out validationUri))
                {
                    result = false;
                    this.errorMessages.Add("Incorrect proxy server address. Please review your data");
                }
            }

            return result;
        }

        /// <summary>
        /// Validates the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>True if email is valid, otherwise false.</returns>
        private bool ValidateEmail(string email)
        {
            Regex emailRegular = new Regex(@"^[\w.]+@[\w.]+\.\w+$");
            return emailRegular.IsMatch(email) && email.Length < 51;
        }

        /// <summary>
        /// Cancel event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.GetContainer<StartupContainer>().Close();
        }

        /// <summary>
        /// Use proxy event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void CheckBoxUseProxy_CheckedChanged(object sender, EventArgs e)
        {
            this.txtAddress.Enabled = this.numPort.Enabled = this.checkBoxUseProxy.Checked;
        }
    }
}
