using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.StartupInnerForms
{
    partial class ComputerManagement
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComputerManagement));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlComputersContainer = new System.Windows.Forms.Panel();
            this.btnAddComputer = new ImageButton();
            this.btnRefresh = new ImageButton();
            this.btnBack = new ImageButton();
            this.btnCancel = new ImageButton();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(22, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Computers management";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label2.Location = new System.Drawing.Point(40, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Your current membership plan:";
            // 
            // pnlComputersContainer
            // 
            this.pnlComputersContainer.AutoScroll = true;
            this.pnlComputersContainer.Location = new System.Drawing.Point(43, 68);
            this.pnlComputersContainer.Name = "pnlComputersContainer";
            this.pnlComputersContainer.Size = new System.Drawing.Size(607, 157);
            this.pnlComputersContainer.TabIndex = 2;
            // 
            // btnAddComputer
            // 
            this.btnAddComputer.AllowTransparency = false;
            this.btnAddComputer.AnimatePress = true;
            this.btnAddComputer.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAddComputer.HoverImage = null;
            this.btnAddComputer.Location = new System.Drawing.Point(222, 275);
            this.btnAddComputer.Name = "btnAddComputer";
            this.btnAddComputer.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAddComputer.NormalImage")));
            this.btnAddComputer.PressedImage = null;
            this.btnAddComputer.Size = new System.Drawing.Size(164, 33);
            this.btnAddComputer.TabIndex = 9;
            this.btnAddComputer.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnAddComputer.Click += new System.EventHandler(this.ButtonAddComputer_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AllowTransparency = false;
            this.btnRefresh.AnimatePress = true;
            this.btnRefresh.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnRefresh.HoverImage = null;
            this.btnRefresh.Location = new System.Drawing.Point(392, 275);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRefresh.NormalImage")));
            this.btnRefresh.PressedImage = null;
            this.btnRefresh.Size = new System.Drawing.Size(164, 33);
            this.btnRefresh.TabIndex = 9;
            this.btnRefresh.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(125, 275);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnBack.NormalImage")));
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(91, 33);
            this.btnBack.TabIndex = 10;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(559, 275);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(101, 33);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label3.Location = new System.Drawing.Point(40, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(433, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Please Click on the Computer Account you wish to use to Proceed";
            // 
            // ComputerManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnAddComputer);
            this.Controls.Add(this.pnlComputersContainer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ComputerManagement";
            this.Size = new System.Drawing.Size(678, 324);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlComputersContainer;
        private ImageButton btnAddComputer;
        private ImageButton btnRefresh;
        private ImageButton btnBack;
        private ImageButton btnCancel;
        private System.Windows.Forms.Label label3;

    }
}
