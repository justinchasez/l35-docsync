using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// The user account mapping row.
    /// </summary>
    public partial class AccountMapping : UserControl
    {
        /// <summary>
        /// The backup user.
        /// </summary>
        private string backupUser;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountMapping"/> class.
        /// </summary>
        public AccountMapping()
        {
            this.InitializeComponent();

            this.Load += this.AccountMapping_Load;
        }

        /// <summary>
        /// Sets the users list.
        /// </summary>
        /// <value>The users list.</value>
        public List<string> UsersList
        {
            set
            {
                this.cmbUsers.DataSource = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected user.
        /// </summary>
        /// <value>The selected user.</value>
        public string SelectedUser
        {
            get
            {
                if (this.cmbUsers.SelectedIndex == 0)
                {
                    return String.Empty;
                }

                return Convert.ToString(this.cmbUsers.SelectedItem);
            }

            set
            {
                this.cmbUsers.SelectedItem = value;
            }
        }

        /// <summary>
        /// Gets or sets the backup user.
        /// </summary>
        /// <value>The backup user.</value>
        public string BackupUser
        {
            get
            {
                return this.backupUser;
            }

            set
            {
                this.backupUser = value;
                if (String.IsNullOrEmpty(this.backupUser))
                {
                    throw new ArgumentNullException("value");
                }

                this.lblAccount.Text = String.Format(this.lblAccount.Text, this.backupUser);
            }
        }

        /// <summary>
        /// Handles the Load event of the AccountMapping control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AccountMapping_Load(object sender, EventArgs e)
        {
        }
    }
}
