using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The sarck tab view.
    /// </summary>
    public partial class SearchView : ViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchView"/> class.
        /// </summary>
        public SearchView()
        {
            this.InitializeComponent();
            EventPublisher.UIEventManagerInstance.SwitchTab += this.UIEventManagerInstance_SwitchTab;
            this.VisibleChanged += this.SearchView_VisibleChanged;
        }

        /// <summary>
        /// Visible changed event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">the event argements</param>
        private void SearchView_VisibleChanged(object sender, EventArgs e)
        {
            this.checkBoxSelectAll.Checked = false;
        }

        /// <summary>
        /// UIs the event manager instance_ switch tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchTabEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_SwitchTab(SwitchTabEventArgs args)
        {
            if (args.SelectedTab == ViewTab.Search)
            {
                this.ApplyData(RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetBackedUpFilesForSearch());
            }
        }

        /// <summary>
        /// Apply data to grid view 
        /// </summary>
        /// <param name="data">Loaded data</param>
        private void ApplyData(DataTable data)
        {
            this.filesGridView.DataSource = data;

            for (int i = 1; i < this.filesGridView.ColumnCount; i++)
            {
                this.filesGridView.Columns[i].ReadOnly = true;
            }

            this.lblResultsCount.Text = data.Rows.Count.ToString();

            this.checkBoxSelectAll.Enabled = data.Rows.Count > 0;
        }

        /// <summary>
        /// Restore selected button cklick event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void BtnRestoreSelected_Click(object sender, EventArgs e)
        {
            List<string> filesToRestore = new List<string>();

            foreach (DataGridViewRow curRow in this.filesGridView.Rows)
            {
                if (((DataGridViewCheckBoxCell) curRow.Cells[0]).Value != null) 
                {
                    if (bool.Parse(((DataGridViewCheckBoxCell)curRow.Cells[0]).Value.ToString()) == true)
                    {
                        filesToRestore.Add((string)curRow.Cells["Location"].Value);
                    }
                }
            }

            List<FileInfoDto> files = new List<FileInfoDto>();
            foreach (string fileName in filesToRestore)
            {
                files.Add(new FileInfoDto
                              {
                                  FileName = fileName
                              });
            }

            RemotingManager.Instance.ProxyManager.BackupManager.RestoreFiles(
                files,
                ClientSettingsManager.Settings.CurrentComputer.Id,
                ClientSettingsManager.Settings.CurrentComputerEncryptInfo);
        }

        /// <summary>
        /// Check all click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void CheckBoxSelectAll_Click(object sender, EventArgs e)
        {
            if (this.checkBoxSelectAll.CheckState == CheckState.Indeterminate)
            {
                this.checkBoxSelectAll.CheckState = CheckState.Unchecked;
            }

            foreach (DataGridViewRow curRow in this.filesGridView.Rows)
            {
                ((DataGridViewCheckBoxCell)curRow.Cells[0]).Value = this.checkBoxSelectAll.Checked;
            }

            this.btnRestoreSelected.Enabled = this.checkBoxSelectAll.Checked;
        }

        #region Grid view events

        /// <summary>
        /// Grid view cell click
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void FilesGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex) == 2 && (e.RowIndex > 0))
            {
                DataGridViewRow row = this.filesGridView.Rows[e.RowIndex];
                string filePath = Path.GetDirectoryName(row.Cells["Location"].Value.ToString());
                Process.Start("explorer", "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\\" + filePath);
            }
        }

        /// <summary>
        /// Files grid view selection changed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void FilesGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.filesGridView.SelectedRows)
            {
                row.Selected = false;
            }
        }

        /// <summary>
        /// CurrentCellDirtyStateChanged event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void FilesGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            this.filesGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        /// <summary>
        /// FilesGridView_CellMouseUp event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void FilesGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 0 && this.filesGridView.Rows.Count > 0)
            {
                bool isChecked = false, isUncheked = false;

                foreach (DataGridViewRow curRow in this.filesGridView.Rows)
                {
                    if (curRow.Cells[0].Value == null)
                    {
                        isUncheked = true;
                    }
                    else if (bool.Parse(curRow.Cells[0].Value.ToString()) == true)
                    {
                        isChecked = true;
                    }
                    else
                    {
                        isUncheked = true;
                    }
                }

                if (!isUncheked)
                {
                    this.checkBoxSelectAll.CheckState = CheckState.Checked;
                    this.btnRestoreSelected.Enabled = true;
                }
                else if (!isChecked)
                {
                    this.checkBoxSelectAll.CheckState = CheckState.Unchecked;
                    this.btnRestoreSelected.Enabled = false;
                }
                else
                {
                    this.checkBoxSelectAll.CheckState = CheckState.Indeterminate;
                    this.btnRestoreSelected.Enabled = true;
                }
            }
        }

        #endregion

        /// <summary>
        /// Update data event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void UpdateDataEventHandler(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            DateTimePicker picker = sender as DateTimePicker;

            this.dateTimePickerAfter.Enabled = this.rbtnSpecific.Checked;
            this.dateTimePickerBefore.Enabled = this.rbtnSpecific.Checked;

            if (checkBox != null)
            {
                if (!checkBox.Checked)
                {
                    return;
                }
            }

            if (picker != null)
            {
                if (!this.rbtnSpecific.Checked)
                {
                    return;
                }
            }

            this.UpdateData();
        }

        /// <summary>
        /// Update the data
        /// </summary>
        private void UpdateData()
        {
            DataTable data = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetBackedUpFilesForSearch();
            List<DataRow> rowsForRemove = new List<DataRow>();

            foreach (DataRow row in data.Rows)
            {
                if ((!string.IsNullOrEmpty(this.txtSearch.Text) && !row["Name"].ToString().ToLower().Contains(this.txtSearch.Text.ToLower())) ||
                    (this.rbtnModifiedToday.Checked && ((DateTime)row["Date modified"]).Date != DateTime.Now.Date) ||
                    (this.rbtnSinceYesterday.Checked && ((DateTime)row["Date modified"]).Date < DateTime.Now.Date.Subtract(TimeSpan.FromDays(1))) ||
                    (this.rbtnLast7days.Checked && ((DateTime)row["Date modified"]).Date < DateTime.Now.Date.Subtract(TimeSpan.FromDays(7))) ||
                    (this.rbtnLast30Days.Checked && ((DateTime)row["Date modified"]).Date < DateTime.Now.Date.Subtract(TimeSpan.FromDays(30))) ||
                    (this.rbtnSpecific.Checked && (((DateTime)row["Date modified"]).Date < this.dateTimePickerAfter.Value.Date || ((DateTime)row["Date modified"]).Date > this.dateTimePickerBefore.Value.Date.AddHours(24))))
                {
                    rowsForRemove.Add(row);
                }
            }

            foreach (DataRow row in rowsForRemove)
            {
                data.Rows.Remove(row);
            }

            this.ApplyData(data);
            this.checkBoxSelectAll.Checked = false;
            this.btnRestoreSelected.Enabled = false;
        }

        /// <summary>
        /// Clear results event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void LblClear_Click(object sender, EventArgs e)
        {
            this.txtSearch.Text = string.Empty;
            this.rbtnSearchAllDates.Checked = true;
            ((DataTable)this.filesGridView.DataSource).Clear();
            this.lblResultsCount.Text = "0";
            this.checkBoxSelectAll.Checked = false;
            this.checkBoxSelectAll.Enabled = false;
            this.btnRestoreSelected.Enabled = false;
        }

        /// <summary>
        /// Link to backup drive click handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void LinkMyBackup_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("explorer", "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\"");
        }

        /// <summary>
        /// Link for help restore clicked event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void LinkHelpRestore_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnLaunchRestoreWizard(new SetOptionsEventArgs());
        }
    }
}
