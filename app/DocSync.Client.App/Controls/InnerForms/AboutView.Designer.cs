using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class AboutView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.lblMembershipPlanName = new System.Windows.Forms.Label();
			this.lblBuildVersion = new System.Windows.Forms.Label();
			this.lblMembershipPlan = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel5 = new System.Windows.Forms.Panel();
			this.lblSubscriptionEnds = new System.Windows.Forms.Label();
			this.lblEmail = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.lbtnRenewSubscription = new System.Windows.Forms.LinkLabel();
			this.lbtnLoginToProfile = new System.Windows.Forms.LinkLabel();
			this.panel4.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel6.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label1.Location = new System.Drawing.Point(33, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(166, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Product Information";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label2.Location = new System.Drawing.Point(33, 120);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(197, 22);
			this.label2.TabIndex = 0;
			this.label2.Text = "Registration Information";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label3.Location = new System.Drawing.Point(33, 227);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(179, 22);
			this.label3.TabIndex = 0;
			this.label3.Text = "Account Management";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.DimGray;
			this.panel1.Location = new System.Drawing.Point(35, 28);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(414, 77);
			this.panel1.TabIndex = 22;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.lblMembershipPlanName);
			this.panel4.Controls.Add(this.lblBuildVersion);
			this.panel4.Controls.Add(this.lblMembershipPlan);
			this.panel4.Controls.Add(this.label4);
			this.panel4.Location = new System.Drawing.Point(37, 30);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(410, 73);
			this.panel4.TabIndex = 22;
			// 
			// lblMembershipPlanName
			// 
			this.lblMembershipPlanName.AutoSize = true;
			this.lblMembershipPlanName.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblMembershipPlanName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lblMembershipPlanName.Location = new System.Drawing.Point(163, 40);
			this.lblMembershipPlanName.Name = "lblMembershipPlanName";
			this.lblMembershipPlanName.Size = new System.Drawing.Size(172, 19);
			this.lblMembershipPlanName.TabIndex = 2;
			this.lblMembershipPlanName.Text = "lblMembershipPlanName";
			// 
			// lblBuildVersion
			// 
			this.lblBuildVersion.AutoSize = true;
			this.lblBuildVersion.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblBuildVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lblBuildVersion.Location = new System.Drawing.Point(163, 10);
			this.lblBuildVersion.Name = "lblBuildVersion";
			this.lblBuildVersion.Size = new System.Drawing.Size(106, 19);
			this.lblBuildVersion.TabIndex = 1;
			this.lblBuildVersion.Text = "lblBuildVersion";
			// 
			// lblMembershipPlan
			// 
			this.lblMembershipPlan.AutoSize = true;
			this.lblMembershipPlan.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblMembershipPlan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.lblMembershipPlan.Location = new System.Drawing.Point(32, 40);
			this.lblMembershipPlan.Name = "lblMembershipPlan";
			this.lblMembershipPlan.Size = new System.Drawing.Size(133, 19);
			this.lblMembershipPlan.TabIndex = 0;
			this.lblMembershipPlan.Text = "Membership plan:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label4.Location = new System.Drawing.Point(32, 10);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(128, 19);
			this.label4.TabIndex = 0;
			this.label4.Text = "Software Version:";
			// 
			// panel2
			// 
			this.panel2.BackColor = System.Drawing.Color.DimGray;
			this.panel2.Location = new System.Drawing.Point(35, 144);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(414, 68);
			this.panel2.TabIndex = 22;
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.lblSubscriptionEnds);
			this.panel5.Controls.Add(this.lblEmail);
			this.panel5.Controls.Add(this.label7);
			this.panel5.Location = new System.Drawing.Point(37, 146);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(410, 64);
			this.panel5.TabIndex = 22;
			// 
			// lblSubscriptionEnds
			// 
			this.lblSubscriptionEnds.AutoSize = true;
			this.lblSubscriptionEnds.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblSubscriptionEnds.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.lblSubscriptionEnds.Location = new System.Drawing.Point(32, 33);
			this.lblSubscriptionEnds.Name = "lblSubscriptionEnds";
			this.lblSubscriptionEnds.Size = new System.Drawing.Size(154, 19);
			this.lblSubscriptionEnds.TabIndex = 0;
			this.lblSubscriptionEnds.Text = "Subscription ends {0}";
			// 
			// lblEmail
			// 
			this.lblEmail.AutoSize = true;
			this.lblEmail.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lblEmail.Location = new System.Drawing.Point(163, 8);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Size = new System.Drawing.Size(61, 19);
			this.lblEmail.TabIndex = 1;
			this.lblEmail.Text = "lblEmail";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label7.Location = new System.Drawing.Point(32, 8);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(55, 19);
			this.label7.TabIndex = 1;
			this.label7.Text = "E-mail:";
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.DimGray;
			this.panel3.Location = new System.Drawing.Point(35, 252);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(414, 57);
			this.panel3.TabIndex = 22;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.lbtnRenewSubscription);
			this.panel6.Controls.Add(this.lbtnLoginToProfile);
			this.panel6.Location = new System.Drawing.Point(37, 254);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(410, 53);
			this.panel6.TabIndex = 22;
			// 
			// lbtnRenewSubscription
			// 
			this.lbtnRenewSubscription.AutoSize = true;
			this.lbtnRenewSubscription.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lbtnRenewSubscription.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.lbtnRenewSubscription.Location = new System.Drawing.Point(32, 28);
			this.lbtnRenewSubscription.Name = "lbtnRenewSubscription";
			this.lbtnRenewSubscription.Size = new System.Drawing.Size(212, 19);
			this.lbtnRenewSubscription.TabIndex = 0;
			this.lbtnRenewSubscription.TabStop = true;
			this.lbtnRenewSubscription.Text = "Renew your subscription now";
			this.lbtnRenewSubscription.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RenewSubscription_LinkClicked);
			// 
			// lbtnLoginToProfile
			// 
			this.lbtnLoginToProfile.AutoSize = true;
			this.lbtnLoginToProfile.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lbtnLoginToProfile.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.lbtnLoginToProfile.Location = new System.Drawing.Point(32, 7);
			this.lbtnLoginToProfile.Name = "lbtnLoginToProfile";
			this.lbtnLoginToProfile.Size = new System.Drawing.Size(320, 19);
			this.lbtnLoginToProfile.TabIndex = 0;
			this.lbtnLoginToProfile.TabStop = true;
			this.lbtnLoginToProfile.Text = "Log in to edit account and billing information";
			this.lbtnLoginToProfile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LoginToProfile_LinkClicked);
			// 
			// AboutView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panel6);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panel5);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "AboutView";
			this.Size = new System.Drawing.Size(712, 338);
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.panel5.ResumeLayout(false);
			this.panel5.PerformLayout();
			this.panel6.ResumeLayout(false);
			this.panel6.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblBuildVersion;
        private System.Windows.Forms.Label lblMembershipPlan;
        private System.Windows.Forms.Label lblSubscriptionEnds;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.LinkLabel lbtnRenewSubscription;
        private System.Windows.Forms.LinkLabel lbtnLoginToProfile;
        private System.Windows.Forms.Label lblMembershipPlanName;
    }
}
