using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Entities.Schedule;
using DocSync.Client.Core.Entities.Schedule.Interface;
using DocSync.Client.Core.Enums;
using DocSync.Client.Scheduler;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The Rules Editor View control.
    /// </summary>
    public partial class RulesEditorView : ViewBase
    {
        /// <summary>
        /// The current select mode.
        /// </summary>
        private BackupScheduleMode selectedMode = BackupScheduleMode.EveryDay;

        /// <summary>
        /// The edit schedule id.
        /// </summary>
        private Guid editScheduleId = Guid.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="RulesEditorView"/> class.
        /// </summary>
        public RulesEditorView()
        {
            this.InitializeComponent();

            this.Load += this.RulesEditorView_Load;
        }

        /// <summary>
        /// Handles the Load event of the RulesEditorView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RulesEditorView_Load(object sender, System.EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.SwitchTab += this.UIEventManagerInstance_SwitchTab;
            EventPublisher.ScheduleManager.BackupScheduleModeChanged += this.ScheduleManager_BackupScheduleModeChanged;
            EventPublisher.ScheduleManager.EditSchedule += this.ScheduleManager_EditSchedule;
        }

        /// <summary>
        /// Schedules the manager_ edit schedule.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        private void ScheduleManager_EditSchedule(ScheduleEventArgs args)
        {
            this.rbtnEveryDay.Checked = (args.Schedule is EveryDaySchedule);
            this.rbtnWorkDays.Checked = (args.Schedule is WorkingDaySchedule);
            this.rbtnSpecific.Checked = (args.Schedule is SpecificSchedule);

            this.selectedMode = this.GetScheduleMode();

            //this.numbStartHours.Value = args.Schedule.StartDate.Hour;
            //this.numbStartMins.Value = args.Schedule.StartDate.Minute;

            //this.numbStopHours.Value = args.Schedule.EndDate.Hour;
            //this.numbStartMins.Value = args.Schedule.EndDate.Minute;

            this.timeSelectorStart.Value = args.Schedule.StartDate;
            this.timeSelectorStop.Value = args.Schedule.EndDate;

            this.chbSun.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Sunday);
            this.chbMon.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Monday);
            this.chbTue.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Tuesday);
            this.chbWed.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Wednesday);
            this.chbThu.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Thursday);
            this.chbFri.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Friday);
            this.chbSat.Checked = args.Schedule.IsDayInPeriod(DayOfWeek.Saturday);

            this.editScheduleId = args.Schedule.Id;
        }

        /// <summary>
        /// UIs the event manager instance_ switch tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchTabEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_SwitchTab(SwitchTabEventArgs args)
        {
            if (args.SelectedTab == ViewTab.RulesEditor)
            {
                this.rbtnEveryDay.Checked = true;
                //this.numbStartHours.Value = this.numbStopHours.Value = 12;
                //this.numbStartMins.Value = this.numbStopMins.Value = 0;

                this.timeSelectorStart.Value = DateTime.Today.AddHours(12);
                this.timeSelectorStop.Value = DateTime.Today.AddHours(12);

                EventPublisher.ScheduleManager.OnBackupScheduleModeChanged(new BackupScheduleModeChangedEventArgs { Mode = BackupScheduleMode.EveryDay });
            }
        }

        /// <summary>
        /// UIs the event manager instance_ backup schedule mode changed.
        /// </summary>
        /// <param name="args">The <see cref="BackupScheduleModeChangedEventArgs"/> instance containing the event data.</param>
        private void ScheduleManager_BackupScheduleModeChanged(BackupScheduleModeChangedEventArgs args)
        {
            this.selectedMode = args.Mode;

            this.Invoke(new MethodInvoker(() =>
            {
                switch (args.Mode)
                {
                    case BackupScheduleMode.EveryDay:
                        this.ChangeCheckedState(
                            ScheduleDayOfWeek.Sunday |
                            ScheduleDayOfWeek.Monday |
                            ScheduleDayOfWeek.Tuesday |
                            ScheduleDayOfWeek.Wednesday |
                            ScheduleDayOfWeek.Thursday |
                            ScheduleDayOfWeek.Friday |
                            ScheduleDayOfWeek.Saturday);
                        break;
                    case BackupScheduleMode.WorkDays:
                        this.ChangeCheckedState(
                           ScheduleDayOfWeek.Monday |
                           ScheduleDayOfWeek.Tuesday |
                           ScheduleDayOfWeek.Wednesday |
                           ScheduleDayOfWeek.Thursday |
                           ScheduleDayOfWeek.Friday);
                        break;
                    case BackupScheduleMode.Specific:
                        this.ChangeCheckedState(ScheduleDayOfWeek.None);
                        break;
                }
            }));
        }

        /// <summary>
        /// Handles the CheckedChanged event of the RButtonSchedule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RButtonSchedule_CheckedChanged(object sender, EventArgs e)
        {
            EventPublisher.ScheduleManager.OnBackupScheduleModeChanged(new BackupScheduleModeChangedEventArgs { Mode = this.GetScheduleMode() });
        }

        /// <summary>
        /// Gets the schedule mode.
        /// </summary>
        /// <returns>The backup schedule mode.</returns>
        private BackupScheduleMode GetScheduleMode()
        {
            BackupScheduleMode mode = default(BackupScheduleMode);

            if (this.rbtnEveryDay.Checked)
            {
                mode = BackupScheduleMode.EveryDay;
            }

            if (this.rbtnWorkDays.Checked)
            {
                mode = BackupScheduleMode.WorkDays;
            }

            if (this.rbtnSpecific.Checked)
            {
                mode = BackupScheduleMode.Specific;
            }

            return mode;
        }

        /// <summary>
        /// Changes the state of the checked.
        /// </summary>
        /// <param name="days">The selected days.</param>
        public void ChangeCheckedState(ScheduleDayOfWeek days)
        {
            this.chbSun.Checked = (days & ScheduleDayOfWeek.Sunday) == ScheduleDayOfWeek.Sunday;
            this.chbMon.Checked = (days & ScheduleDayOfWeek.Monday) == ScheduleDayOfWeek.Monday;
            this.chbTue.Checked = (days & ScheduleDayOfWeek.Tuesday) == ScheduleDayOfWeek.Tuesday;
            this.chbWed.Checked = (days & ScheduleDayOfWeek.Wednesday) == ScheduleDayOfWeek.Wednesday;
            this.chbThu.Checked = (days & ScheduleDayOfWeek.Thursday) == ScheduleDayOfWeek.Thursday;
            this.chbFri.Checked = (days & ScheduleDayOfWeek.Friday) == ScheduleDayOfWeek.Friday;
            this.chbSat.Checked = (days & ScheduleDayOfWeek.Saturday) == ScheduleDayOfWeek.Saturday;
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.editScheduleId = Guid.Empty;
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.AdvancedScheduling });
        }

        /// <summary>
        /// Handles the Click event of the ButtonSaveRule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonSaveRule_Click(object sender, EventArgs e)
        {
            //DateTime startDate = DateTime.Today.AddHours(Convert.ToDouble(this.numbStartHours.Value))
            //                                   .AddMinutes(Convert.ToDouble(this.numbStartMins.Value));

            //DateTime endDate = DateTime.Today.AddHours(Convert.ToDouble(this.numbStopHours.Value))
            //                                 .AddMinutes(Convert.ToDouble(this.numbStopMins.Value));

            if (this.timeSelectorStop.Value <= this.timeSelectorStart.Value)
            {
                MessageBox.Show("Please specify correct time");
                return;
            }

            DateTime startDate = this.timeSelectorStart.Value;
            DateTime endDate = this.timeSelectorStop.Value;

            foreach (KeyValuePair<Guid, ISchedule> curSchedule in RemotingManager.Instance.ProxyManager.ScheduleManager.Schedules)
            {
                if (curSchedule.Value.StartDate.Hour == startDate.Hour &&
                    curSchedule.Value.StartDate.Minute == startDate.Minute &&
                    curSchedule.Value.EndDate.Hour == endDate.Hour &&
                    curSchedule.Value.EndDate.Minute == endDate.Minute)
                {
                    SpecificSchedule specific = curSchedule.Value as SpecificSchedule;
                    if (specific != null)
                    {
                        if (this.GetSelectedDays(this.selectedMode) == specific.Days)
                        {
                            MessageBox.Show("You already have a backup rule that matches the options you selected");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("You already have a backup rule that matches the options you selected");
                        return;
                    }
                }
            }

            ISchedule schedule = ScheduleFactory.GetSchedule(this.selectedMode, this.GetSelectedDays(this.selectedMode), startDate, endDate);
            if (this.editScheduleId != Guid.Empty)
            {
                schedule.Id = this.editScheduleId;
                this.editScheduleId = Guid.Empty;
            }

            //RemotingManager.Instance.ProxyManager.ScheduleManager.

            RemotingManager.Instance.ProxyManager.ScheduleManager.SaveSchedule(schedule);

            EventPublisher.ScheduleManager.OnScheduleSaved(new ScheduleEventArgs { Schedule = schedule });
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.AdvancedScheduling });
        }

        /// <summary>
        /// Gets the selected days.
        /// </summary>
        /// <param name="mode">The seelcted mode.</param>
        /// <returns>The selected days.</returns>
        private ScheduleDayOfWeek GetSelectedDays(BackupScheduleMode mode)
        {
            if (mode == BackupScheduleMode.Specific)
            {
                ScheduleDayOfWeek days = default(ScheduleDayOfWeek);
                if (this.chbMon.Checked)
                {
                    days |= ScheduleDayOfWeek.Monday;
                }

                if (this.chbTue.Checked)
                {
                    days |= ScheduleDayOfWeek.Tuesday;
                }

                if (this.chbWed.Checked)
                {
                    days |= ScheduleDayOfWeek.Wednesday;
                }

                if (this.chbThu.Checked)
                {
                    days |= ScheduleDayOfWeek.Thursday;
                }

                if (this.chbFri.Checked)
                {
                    days |= ScheduleDayOfWeek.Friday;
                }

                if (this.chbSat.Checked)
                {
                    days |= ScheduleDayOfWeek.Saturday;
                }

                if (this.chbSun.Checked)
                {
                    days |= ScheduleDayOfWeek.Sunday;
                }

                return days;
            }

            return default(ScheduleDayOfWeek);
        }

        /// <summary>
        /// Handles the CheckedChanged event of the CheckBoxSun control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void CheckBoxDay_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chbSun.Checked && this.chbMon.Checked && this.chbTue.Checked && this.chbWed.Checked && this.chbThu.Checked && this.chbFri.Checked && this.chbSat.Checked)
            {
                this.rbtnEveryDay.Checked = true;
            }
            else if (!this.chbSun.Checked && this.chbMon.Checked && this.chbTue.Checked && this.chbWed.Checked && this.chbThu.Checked && this.chbFri.Checked && !this.chbSat.Checked)
            {
                this.rbtnWorkDays.Checked = true;
            }
            else
            {
                this.rbtnSpecific.Checked = true;
            }
        }

		private void RulesEditorView_Load_1(object sender, EventArgs e)
		{

		}
    }
}