using System;
using System.Diagnostics;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core.Managers;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The view for GetSupportView tab.
    /// </summary>
    public partial class GetSupportView : ViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetSupportView"/> class.
        /// </summary>
        public GetSupportView()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.Load += this.GetSupportView_Load;
        }

        /// <summary>
        /// Handles the Load event of the GetSupportView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void GetSupportView_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
            {
                return;
            }

            this.cmbHelpTopics.DataSource = RemotingManager.Instance.ProxyManager.HelpManger.GetHelpTopics();
            this.cmbHelpTopics.SelectedIndex = 0;
        }

        /// <summary>
        /// Handles the LinkClicked event of the HowToBackup control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void HowToBackup_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.TutorialHowToSeeOrChangeWhatIsBackedUp);
        }

        /// <summary>
        /// Handles the LinkClicked event of the HowToChangeSettings control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void HowToChangeSettings_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.TutorialHowToChangeSettings);
        }

        /// <summary>
        /// Handles the LinkClicked event of the HowToRestore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void HowToRestore_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.TutorialHowToRestoreFiles);
        }
    }
}
