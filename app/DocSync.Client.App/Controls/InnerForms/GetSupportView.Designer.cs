using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class GetSupportView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GetSupportView));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.btnContactSupport = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.pictureBox4 = new System.Windows.Forms.PictureBox();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.pictureBox6 = new System.Windows.Forms.PictureBox();
			this.cmbHelpTopics = new System.Windows.Forms.ComboBox();
			this.lbtnHowToBackup = new System.Windows.Forms.LinkLabel();
			this.lbtnHowToChangeSettings = new System.Windows.Forms.LinkLabel();
			this.lbtnHowToRestore = new System.Windows.Forms.LinkLabel();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.btnSearchHelp = new DocSync.Client.Controls.ImageButton.ImageButton();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label1.Location = new System.Drawing.Point(70, 23);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(297, 22);
			this.label1.TabIndex = 1;
			this.label1.Text = "Search FAQs and Troubleshooting Tips";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label2.Location = new System.Drawing.Point(70, 91);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(210, 22);
			this.label2.TabIndex = 1;
			this.label2.Text = "Browse Topics by Category";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label3.Location = new System.Drawing.Point(70, 155);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(115, 22);
			this.label3.TabIndex = 1;
			this.label3.Text = "View Tutorials";
			// 
			// btnContactSupport
			// 
			this.btnContactSupport.AllowTransparency = false;
			this.btnContactSupport.AnimatePress = true;
			this.btnContactSupport.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnContactSupport.HoverImage = null;
			this.btnContactSupport.Location = new System.Drawing.Point(74, 263);
			this.btnContactSupport.Name = "btnContactSupport";
			this.btnContactSupport.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnContactSupport.NormalImage")));
			this.btnContactSupport.PressedImage = null;
			this.btnContactSupport.Size = new System.Drawing.Size(277, 54);
			this.btnContactSupport.TabIndex = 18;
			// 
			// pictureBox4
			// 
			this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
			this.pictureBox4.Location = new System.Drawing.Point(89, 186);
			this.pictureBox4.Name = "pictureBox4";
			this.pictureBox4.Size = new System.Drawing.Size(10, 10);
			this.pictureBox4.TabIndex = 19;
			this.pictureBox4.TabStop = false;
			// 
			// pictureBox5
			// 
			this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
			this.pictureBox5.Location = new System.Drawing.Point(89, 210);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(10, 10);
			this.pictureBox5.TabIndex = 19;
			this.pictureBox5.TabStop = false;
			// 
			// pictureBox6
			// 
			this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
			this.pictureBox6.Location = new System.Drawing.Point(89, 232);
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new System.Drawing.Size(10, 10);
			this.pictureBox6.TabIndex = 19;
			this.pictureBox6.TabStop = false;
			// 
			// cmbHelpTopics
			// 
			this.cmbHelpTopics.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.cmbHelpTopics.FormattingEnabled = true;
			this.cmbHelpTopics.Location = new System.Drawing.Point(74, 117);
			this.cmbHelpTopics.Name = "cmbHelpTopics";
			this.cmbHelpTopics.Size = new System.Drawing.Size(206, 27);
			this.cmbHelpTopics.TabIndex = 20;
			// 
			// lbtnHowToBackup
			// 
			this.lbtnHowToBackup.AutoSize = true;
			this.lbtnHowToBackup.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lbtnHowToBackup.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lbtnHowToBackup.Location = new System.Drawing.Point(105, 179);
			this.lbtnHowToBackup.Name = "lbtnHowToBackup";
			this.lbtnHowToBackup.Size = new System.Drawing.Size(302, 22);
			this.lbtnHowToBackup.TabIndex = 21;
			this.lbtnHowToBackup.TabStop = true;
			this.lbtnHowToBackup.Text = "How to see or change what is backed up";
			this.lbtnHowToBackup.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.HowToBackup_LinkClicked);
			// 
			// lbtnHowToChangeSettings
			// 
			this.lbtnHowToChangeSettings.AutoSize = true;
			this.lbtnHowToChangeSettings.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lbtnHowToChangeSettings.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lbtnHowToChangeSettings.Location = new System.Drawing.Point(105, 202);
			this.lbtnHowToChangeSettings.Name = "lbtnHowToChangeSettings";
			this.lbtnHowToChangeSettings.Size = new System.Drawing.Size(256, 22);
			this.lbtnHowToChangeSettings.TabIndex = 21;
			this.lbtnHowToChangeSettings.TabStop = true;
			this.lbtnHowToChangeSettings.Text = "How to change DocSync�s settings";
			this.lbtnHowToChangeSettings.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.HowToChangeSettings_LinkClicked);
			// 
			// lbtnHowToRestore
			// 
			this.lbtnHowToRestore.AutoSize = true;
			this.lbtnHowToRestore.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lbtnHowToRestore.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.lbtnHowToRestore.Location = new System.Drawing.Point(105, 226);
			this.lbtnHowToRestore.Name = "lbtnHowToRestore";
			this.lbtnHowToRestore.Size = new System.Drawing.Size(192, 22);
			this.lbtnHowToRestore.TabIndex = 21;
			this.lbtnHowToRestore.TabStop = true;
			this.lbtnHowToRestore.Text = "How to restore your files";
			this.lbtnHowToRestore.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.HowToRestore_LinkClicked);
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.textBox1.Location = new System.Drawing.Point(74, 49);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(206, 27);
			this.textBox1.TabIndex = 22;
			// 
			// btnSearchHelp
			// 
			this.btnSearchHelp.AllowTransparency = false;
			this.btnSearchHelp.AnimatePress = true;
			this.btnSearchHelp.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnSearchHelp.HoverImage = null;
			this.btnSearchHelp.Location = new System.Drawing.Point(289, 49);
			this.btnSearchHelp.Name = "btnSearchHelp";
			this.btnSearchHelp.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSearchHelp.NormalImage")));
			this.btnSearchHelp.PressedImage = null;
			this.btnSearchHelp.Size = new System.Drawing.Size(98, 31);
			this.btnSearchHelp.TabIndex = 23;
			// 
			// GetSupportView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnSearchHelp);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.lbtnHowToRestore);
			this.Controls.Add(this.lbtnHowToChangeSettings);
			this.Controls.Add(this.lbtnHowToBackup);
			this.Controls.Add(this.cmbHelpTopics);
			this.Controls.Add(this.pictureBox6);
			this.Controls.Add(this.pictureBox5);
			this.Controls.Add(this.pictureBox4);
			this.Controls.Add(this.btnContactSupport);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "GetSupportView";
			this.Size = new System.Drawing.Size(712, 338);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private ImageButton btnContactSupport;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ComboBox cmbHelpTopics;
        private System.Windows.Forms.LinkLabel lbtnHowToBackup;
        private System.Windows.Forms.LinkLabel lbtnHowToChangeSettings;
        private System.Windows.Forms.LinkLabel lbtnHowToRestore;
        private System.Windows.Forms.TextBox textBox1;
        private ImageButton btnSearchHelp;
    }
}
