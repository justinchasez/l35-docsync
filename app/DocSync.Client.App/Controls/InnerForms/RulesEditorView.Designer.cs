using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class RulesEditorView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RulesEditorView));
			this.btnCancel = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.btnSaveRule = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.chbSat = new System.Windows.Forms.CheckBox();
			this.chbFri = new System.Windows.Forms.CheckBox();
			this.chbThu = new System.Windows.Forms.CheckBox();
			this.chbWed = new System.Windows.Forms.CheckBox();
			this.chbTue = new System.Windows.Forms.CheckBox();
			this.chbMon = new System.Windows.Forms.CheckBox();
			this.chbSun = new System.Windows.Forms.CheckBox();
			this.rbtnSpecific = new System.Windows.Forms.RadioButton();
			this.rbtnWorkDays = new System.Windows.Forms.RadioButton();
			this.rbtnEveryDay = new System.Windows.Forms.RadioButton();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.timeSelectorStart = new DocSync.Client.App.Controls.TimeSelector();
			this.timeSelectorStop = new DocSync.Client.App.Controls.TimeSelector();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.AllowTransparency = false;
			this.btnCancel.AnimatePress = true;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnCancel.HoverImage = null;
			this.btnCancel.Location = new System.Drawing.Point(22, 277);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
			this.btnCancel.PressedImage = null;
			this.btnCancel.Size = new System.Drawing.Size(164, 33);
			this.btnCancel.TabIndex = 32;
			this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
			// 
			// btnSaveRule
			// 
			this.btnSaveRule.AllowTransparency = false;
			this.btnSaveRule.AnimatePress = true;
			this.btnSaveRule.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnSaveRule.HoverImage = null;
			this.btnSaveRule.Location = new System.Drawing.Point(312, 277);
			this.btnSaveRule.Name = "btnSaveRule";
			this.btnSaveRule.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSaveRule.NormalImage")));
			this.btnSaveRule.PressedImage = null;
			this.btnSaveRule.Size = new System.Drawing.Size(164, 33);
			this.btnSaveRule.TabIndex = 33;
			this.btnSaveRule.Click += new System.EventHandler(this.ButtonSaveRule_Click);
			// 
			// chbSat
			// 
			this.chbSat.AutoSize = true;
			this.chbSat.Checked = true;
			this.chbSat.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbSat.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbSat.Location = new System.Drawing.Point(412, 174);
			this.chbSat.Name = "chbSat";
			this.chbSat.Size = new System.Drawing.Size(48, 23);
			this.chbSat.TabIndex = 26;
			this.chbSat.Text = "Sat";
			this.chbSat.UseVisualStyleBackColor = true;
			this.chbSat.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbFri
			// 
			this.chbFri.AutoSize = true;
			this.chbFri.Checked = true;
			this.chbFri.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbFri.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbFri.Location = new System.Drawing.Point(352, 174);
			this.chbFri.Name = "chbFri";
			this.chbFri.Size = new System.Drawing.Size(44, 23);
			this.chbFri.TabIndex = 26;
			this.chbFri.Text = "Fri";
			this.chbFri.UseVisualStyleBackColor = true;
			this.chbFri.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbThu
			// 
			this.chbThu.AutoSize = true;
			this.chbThu.Checked = true;
			this.chbThu.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbThu.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbThu.Location = new System.Drawing.Point(287, 174);
			this.chbThu.Name = "chbThu";
			this.chbThu.Size = new System.Drawing.Size(52, 23);
			this.chbThu.TabIndex = 26;
			this.chbThu.Text = "Thu";
			this.chbThu.UseVisualStyleBackColor = true;
			this.chbThu.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbWed
			// 
			this.chbWed.AutoSize = true;
			this.chbWed.Checked = true;
			this.chbWed.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbWed.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbWed.Location = new System.Drawing.Point(217, 174);
			this.chbWed.Name = "chbWed";
			this.chbWed.Size = new System.Drawing.Size(57, 23);
			this.chbWed.TabIndex = 26;
			this.chbWed.Text = "Wed";
			this.chbWed.UseVisualStyleBackColor = true;
			this.chbWed.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbTue
			// 
			this.chbTue.AutoSize = true;
			this.chbTue.Checked = true;
			this.chbTue.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbTue.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbTue.Location = new System.Drawing.Point(155, 174);
			this.chbTue.Name = "chbTue";
			this.chbTue.Size = new System.Drawing.Size(51, 23);
			this.chbTue.TabIndex = 26;
			this.chbTue.Text = "Tue";
			this.chbTue.UseVisualStyleBackColor = true;
			this.chbTue.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbMon
			// 
			this.chbMon.AutoSize = true;
			this.chbMon.Checked = true;
			this.chbMon.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbMon.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbMon.Location = new System.Drawing.Point(87, 174);
			this.chbMon.Name = "chbMon";
			this.chbMon.Size = new System.Drawing.Size(57, 23);
			this.chbMon.TabIndex = 26;
			this.chbMon.Text = "Mon";
			this.chbMon.UseVisualStyleBackColor = true;
			this.chbMon.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// chbSun
			// 
			this.chbSun.AutoSize = true;
			this.chbSun.Checked = true;
			this.chbSun.CheckState = System.Windows.Forms.CheckState.Checked;
			this.chbSun.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.chbSun.Location = new System.Drawing.Point(25, 174);
			this.chbSun.Name = "chbSun";
			this.chbSun.Size = new System.Drawing.Size(51, 23);
			this.chbSun.TabIndex = 26;
			this.chbSun.Text = "Sun";
			this.chbSun.UseVisualStyleBackColor = true;
			this.chbSun.CheckedChanged += new System.EventHandler(this.CheckBoxDay_CheckedChanged);
			// 
			// rbtnSpecific
			// 
			this.rbtnSpecific.AutoSize = true;
			this.rbtnSpecific.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.rbtnSpecific.Location = new System.Drawing.Point(55, 143);
			this.rbtnSpecific.Name = "rbtnSpecific";
			this.rbtnSpecific.Size = new System.Drawing.Size(110, 23);
			this.rbtnSpecific.TabIndex = 25;
			this.rbtnSpecific.Text = "Specific days";
			this.rbtnSpecific.UseVisualStyleBackColor = true;
			this.rbtnSpecific.Click += new System.EventHandler(this.RButtonSchedule_CheckedChanged);
			// 
			// rbtnWorkDays
			// 
			this.rbtnWorkDays.AutoSize = true;
			this.rbtnWorkDays.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.rbtnWorkDays.Location = new System.Drawing.Point(55, 114);
			this.rbtnWorkDays.Name = "rbtnWorkDays";
			this.rbtnWorkDays.Size = new System.Drawing.Size(77, 23);
			this.rbtnWorkDays.TabIndex = 25;
			this.rbtnWorkDays.Text = "Mon-Fri";
			this.rbtnWorkDays.UseVisualStyleBackColor = true;
			this.rbtnWorkDays.Click += new System.EventHandler(this.RButtonSchedule_CheckedChanged);
			// 
			// rbtnEveryDay
			// 
			this.rbtnEveryDay.AutoSize = true;
			this.rbtnEveryDay.Checked = true;
			this.rbtnEveryDay.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.rbtnEveryDay.Location = new System.Drawing.Point(55, 85);
			this.rbtnEveryDay.Name = "rbtnEveryDay";
			this.rbtnEveryDay.Size = new System.Drawing.Size(89, 23);
			this.rbtnEveryDay.TabIndex = 25;
			this.rbtnEveryDay.TabStop = true;
			this.rbtnEveryDay.Text = "Every day";
			this.rbtnEveryDay.UseVisualStyleBackColor = true;
			this.rbtnEveryDay.Click += new System.EventHandler(this.RButtonSchedule_CheckedChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label4.Location = new System.Drawing.Point(21, 239);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(129, 19);
			this.label4.TabIndex = 24;
			this.label4.Text = "Stop backing-up at";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label3.Location = new System.Drawing.Point(21, 206);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(131, 19);
			this.label3.TabIndex = 24;
			this.label3.Text = "Start backing-up at";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label2.Location = new System.Drawing.Point(21, 59);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(86, 22);
			this.label2.TabIndex = 24;
			this.label2.Text = "Back-up...";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.label1.Location = new System.Drawing.Point(3, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(482, 44);
			this.label1.TabIndex = 23;
			this.label1.Text = "Create a backup rule, then click \"Save\". You can add as many rules as you\'d like." +
    "";
			// 
			// timeSelectorStart
			// 
			this.timeSelectorStart.Location = new System.Drawing.Point(176, 199);
			this.timeSelectorStart.Name = "timeSelectorStart";
			this.timeSelectorStart.Size = new System.Drawing.Size(220, 33);
			this.timeSelectorStart.TabIndex = 34;
			this.timeSelectorStart.Value = new System.DateTime(2014, 1, 17, 12, 0, 0, 0);
			// 
			// timeSelectorStop
			// 
			this.timeSelectorStop.Location = new System.Drawing.Point(176, 232);
			this.timeSelectorStop.Name = "timeSelectorStop";
			this.timeSelectorStop.Size = new System.Drawing.Size(220, 33);
			this.timeSelectorStop.TabIndex = 35;
			this.timeSelectorStop.Value = new System.DateTime(2014, 1, 17, 12, 0, 0, 0);
			// 
			// RulesEditorView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.timeSelectorStop);
			this.Controls.Add(this.timeSelectorStart);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnSaveRule);
			this.Controls.Add(this.chbSat);
			this.Controls.Add(this.chbFri);
			this.Controls.Add(this.chbThu);
			this.Controls.Add(this.chbWed);
			this.Controls.Add(this.chbTue);
			this.Controls.Add(this.chbMon);
			this.Controls.Add(this.chbSun);
			this.Controls.Add(this.rbtnSpecific);
			this.Controls.Add(this.rbtnWorkDays);
			this.Controls.Add(this.rbtnEveryDay);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "RulesEditorView";
			this.Size = new System.Drawing.Size(712, 338);
			this.Load += new System.EventHandler(this.RulesEditorView_Load_1);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtnEveryDay;
        private System.Windows.Forms.RadioButton rbtnWorkDays;
        private System.Windows.Forms.RadioButton rbtnSpecific;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private ImageButton btnCancel;
        private ImageButton btnSaveRule;
        public System.Windows.Forms.CheckBox chbSun;
        public System.Windows.Forms.CheckBox chbMon;
        public System.Windows.Forms.CheckBox chbTue;
        public System.Windows.Forms.CheckBox chbWed;
        public System.Windows.Forms.CheckBox chbThu;
        public System.Windows.Forms.CheckBox chbFri;
        public System.Windows.Forms.CheckBox chbSat;
        private TimeSelector timeSelectorStart;
        private TimeSelector timeSelectorStop;
    }
}
