using DocSync.Client.App.Controls.ViewStatusForms;
using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class ViewStatusView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewStatusView));
			this.panel1 = new System.Windows.Forms.Panel();
			this.ctrlStatusViewRestore = new DocSync.Client.App.Controls.ViewStatusForms.RestoreStatusView();
			this.ctrlStatusViewBackup = new DocSync.Client.App.Controls.ViewStatusForms.BackupStatusView();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.tabRstoreStatus = new DocSync.Client.App.Controls.SwitchControl();
			this.tabBackupStatus = new DocSync.Client.App.Controls.SwitchControl();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabRstoreStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabBackupStatus)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.ctrlStatusViewRestore);
			this.panel1.Controls.Add(this.ctrlStatusViewBackup);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(7, 6);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(500, 298);
			this.panel1.TabIndex = 5;
			// 
			// ctrlStatusViewRestore
			// 
			this.ctrlStatusViewRestore.Location = new System.Drawing.Point(7, 6);
			this.ctrlStatusViewRestore.Name = "ctrlStatusViewRestore";
			this.ctrlStatusViewRestore.Size = new System.Drawing.Size(486, 286);
			this.ctrlStatusViewRestore.TabIndex = 2;
			// 
			// ctrlStatusViewBackup
			// 
			this.ctrlStatusViewBackup.Location = new System.Drawing.Point(7, 6);
			this.ctrlStatusViewBackup.Name = "ctrlStatusViewBackup";
			this.ctrlStatusViewBackup.Size = new System.Drawing.Size(486, 286);
			this.ctrlStatusViewBackup.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
			this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(5, 298);
			this.panel3.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(495, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(5, 298);
			this.panel2.TabIndex = 0;
			// 
			// tabRstoreStatus
			// 
			this.tabRstoreStatus.HoverImage = null;
			this.tabRstoreStatus.Image = ((System.Drawing.Image)(resources.GetObject("tabRstoreStatus.Image")));
			this.tabRstoreStatus.IsSelected = false;
			this.tabRstoreStatus.Location = new System.Drawing.Point(191, 300);
			this.tabRstoreStatus.Name = "tabRstoreStatus";
			this.tabRstoreStatus.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabRstoreStatus.NormalImage")));
			this.tabRstoreStatus.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabRstoreStatus.SelectedImage")));
			this.tabRstoreStatus.Size = new System.Drawing.Size(185, 33);
			this.tabRstoreStatus.TabIndex = 6;
			this.tabRstoreStatus.TabStop = false;
			this.tabRstoreStatus.Click += new System.EventHandler(this.TabRestoreStatus_Click);
			// 
			// tabBackupStatus
			// 
			this.tabBackupStatus.HoverImage = null;
			this.tabBackupStatus.Image = ((System.Drawing.Image)(resources.GetObject("tabBackupStatus.Image")));
			this.tabBackupStatus.IsSelected = false;
			this.tabBackupStatus.Location = new System.Drawing.Point(7, 300);
			this.tabBackupStatus.Name = "tabBackupStatus";
			this.tabBackupStatus.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabBackupStatus.NormalImage")));
			this.tabBackupStatus.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabBackupStatus.SelectedImage")));
			this.tabBackupStatus.Size = new System.Drawing.Size(185, 33);
			this.tabBackupStatus.TabIndex = 6;
			this.tabBackupStatus.TabStop = false;
			this.tabBackupStatus.Click += new System.EventHandler(this.TabBackupStatus_Click);
			// 
			// ViewStatusView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tabRstoreStatus);
			this.Controls.Add(this.tabBackupStatus);
			this.Controls.Add(this.panel1);
			this.Name = "ViewStatusView";
			this.Size = new System.Drawing.Size(712, 338);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabRstoreStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabBackupStatus)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private SwitchControl tabBackupStatus;
        private SwitchControl tabRstoreStatus;
        private BackupStatusView ctrlStatusViewBackup;
        private RestoreStatusView ctrlStatusViewRestore;
    }
}
