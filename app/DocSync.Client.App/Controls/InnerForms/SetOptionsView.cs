using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Entities.Settings;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The view for Set Options tab.
    /// </summary>
    public partial class SetOptionsView : ViewBase
    {
        /// <summary>
        /// Current selected tab.
        /// </summary>
        private OptionsTab selectedTab = OptionsTab.BackupSchedule;

        /// <summary>
        /// The application settings info.
        /// </summary>
        private SettingsInfo settings;

        /// <summary>
        /// The tab controls.
        /// </summary>
        private Dictionary<OptionsTab, SwitchControl> tabs = new Dictionary<OptionsTab, SwitchControl>();

        /// <summary>
        /// The tab view controls.
        /// </summary>
        private Dictionary<OptionsTab, UserControl> tabViews = new Dictionary<OptionsTab, UserControl>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SetOptionsView"/> class.
        /// </summary>
        public SetOptionsView()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.Load += this.SetOptionsView_Load;

            EventPublisher.UIEventManagerInstance.SetOptions += this.UiEventManagerInstanceSetOptions;
            RemotingManager.Instance.ProxyPublisher.OptionChanged += this.ProxyPublisher_OptionChanged;
        }

        /// <summary>
        /// Event handler for SetOption event
        /// </summary>
        /// <param name="args">option argument</param>
        private void UiEventManagerInstanceSetOptions(SetOptionsEventArgs args)
        {
            if (this.settings == null)
            {
                this.settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
            }

            switch (args.Option)
            {
                case AppOptions.LowPriority:
                    this.settings.ConnectionPriority = this.ctrlOptionsView.UseLowPriority
                                                           ? ConnectionPriority.Low
                                                           : ConnectionPriority.Normal;
                    break;
                case AppOptions.RecoverMode:
                    this.settings.IsRecoverMode = this.ctrlOptionsView.RecoveryModeEnabled;
                    break;
                case AppOptions.TargetLocation:
                    this.settings.TargetFolderLocation = this.ctrlOptionsView.TargetFolderLocation;
                    break;
                case AppOptions.ProxyServer:
                    this.settings.IsProxyServerUsed = this.ctrlOptionsView.IsProxyUsed;
                    this.settings.ProxyServerAddress = this.ctrlOptionsView.ProxyAddress;
                    this.settings.ProxyServerPort = this.ctrlOptionsView.ProxyPort;
                    break;
            }

            RemotingManager.Instance.ProxyManager.SettingsManager.SaveSettings(this.settings);
            RemotingManager.Instance.ProxyPublisher.NotifyOptionChanged(args);
        }

        /// <summary>
        /// Handles the VisibleChanged event of the SetOptionsView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SetOptionsView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.DesignMode)
            {
                return;
            }

            if (this.Visible)
            {
                this.settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();

                //this.ctrlOptionsView.BackupDesctop = this.settings.BackupDesktop;
                this.ctrlOptionsView.BackupDesctop = this.settings.UsersForAutomaticBackup.Contains(Environment.UserName);
                this.ctrlOptionsView.TargetFolderLocation = this.settings.TargetFolderLocation;
                this.ctrlOptionsView.UseLowPriority = (this.settings.ConnectionPriority == ConnectionPriority.Low);
                this.ctrlOptionsView.LowPrioritySpeed = this.settings.LowPrioritySpeed;
                this.ctrlOptionsView.RecoveryModeEnabled = this.settings.IsRecoverMode;
                this.ctrlOptionsView.IsProxyUsed = this.settings.IsProxyServerUsed;
                this.ctrlOptionsView.ProxyAddress = this.settings.ProxyServerAddress;
                this.ctrlOptionsView.ProxyPort = this.settings.ProxyServerPort;

                //this.ctrlBackupScheduleView.ScheduleInfo = this.settings.ScheduleSettings;
            }
            else
            {
                this.settings.TargetFolderLocation = this.ctrlOptionsView.TargetFolderLocation;
                this.settings.ConnectionPriority = this.ctrlOptionsView.UseLowPriority ? ConnectionPriority.Low : ConnectionPriority.High;
                this.settings.LowPrioritySpeed = this.ctrlOptionsView.LowPrioritySpeed;
                this.settings.IsRecoverMode = this.ctrlOptionsView.RecoveryModeEnabled;
                this.settings.IsProxyServerUsed = this.ctrlOptionsView.IsProxyUsed;
                this.settings.ProxyServerAddress = this.ctrlOptionsView.ProxyAddress;
                this.settings.ProxyServerPort = this.ctrlOptionsView.ProxyPort;

				//this.settings.BackupDesktop = this.ctrlOptionsView.BackupDesctop;
				if (this.ctrlOptionsView.BackupDesctop && !this.settings.UsersForAutomaticBackup.Contains(Environment.UserName))
				{
					this.settings.UsersForAutomaticBackup.Add(Environment.UserName);
					//RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
					//RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

					if (Directory.Exists(this.settings.TargetFolderLocation))
					{
						RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(this.settings.TargetFolderLocation);
					}
				}
				else if (!this.ctrlOptionsView.BackupDesctop && this.settings.UsersForAutomaticBackup.Contains(Environment.UserName))
				{
					this.settings.UsersForAutomaticBackup.Remove(Environment.UserName);
				}

                //this.settings.ScheduleSettings = this.ctrlBackupScheduleView.ScheduleInfo;
                this.settings.ScheduleSettings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().ScheduleSettings;

                RemotingManager.Instance.ProxyManager.SettingsManager.SaveSettings(this.settings);
            }
        }

        /// <summary>
        /// Handles the Load event of the SetOptionsView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SetOptionsView_Load(object sender, EventArgs e)
        {
            if (this.IsDesignMode)
            {
                return;
            }

            // Initialize tabs.
            this.tabs.Add(OptionsTab.Options, this.tabOptions);
            this.tabs.Add(OptionsTab.BackupSchedule, this.tabBackupSchedule);

            // Initialize tab views.
            this.tabViews.Add(OptionsTab.Options, this.ctrlOptionsView);
            this.tabViews.Add(OptionsTab.BackupSchedule, this.ctrlBackupScheduleView);

            this.VisibleChanged += this.SetOptionsView_VisibleChanged;

            EventPublisher.UIEventManagerInstance.SwitchOptionsTab += this.UIEventManagerInstance_SwitchOptionsTab;
            EventPublisher.UIEventManagerInstance.OnSwitchOptionsTab(new SwitchOptionsTabEventArgs { SelectedTab = OptionsTab.Options });
        }

        /// <summary>
        /// UIs the event manager instance_ switch options tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchOptionsTabEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_SwitchOptionsTab(SwitchOptionsTabEventArgs args)
        {
            if (this.selectedTab != args.SelectedTab)
            {
                this.SwitchTab(args.SelectedTab);
            }
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="tab">The tab current.</param>
        private void SwitchTab(OptionsTab tab)
        {
            this.tabs[this.selectedTab].IsSelected = false;
            this.tabs[tab].IsSelected = true;

            this.tabViews[this.selectedTab].Visible = false;
            this.tabViews[tab].Visible = true;

            this.selectedTab = tab;
        }

        /// <summary>
        /// Handles the Click event of the TabOptions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabOptions_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchOptionsTab(new SwitchOptionsTabEventArgs { SelectedTab = OptionsTab.Options });
        }

        /// <summary>
        /// Handles the Click event of the TabBackupSchedule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabBackupSchedule_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchOptionsTab(new SwitchOptionsTabEventArgs { SelectedTab = OptionsTab.BackupSchedule });
        }        
        
        /// <summary>
        /// Event handler [when Option Changed]
        /// </summary>
        /// <param name="args">event argument</param>
        private void ProxyPublisher_OptionChanged(SetOptionsEventArgs args)
        {
            this.BeginInvoke(new MethodInvoker(delegate
                                                   {
                                                       switch (args.Option)
                                                       {
                                                           case AppOptions.RecoverMode:
                                                               this.ctrlOptionsView.RecoveryModeEnabled = args.Value ==
                                                                                                          1;
                                                               break;

                                                           case AppOptions.LowPriority:
                                                               this.ctrlOptionsView.UseLowPriority = args.Value == 1;
                                                               break;

                                                           case AppOptions.TargetLocation:
                                                               this.ctrlOptionsView.TargetFolderLocation = args.StringValue;
                                                               break;
                                                       }
                                                   }));
        }

        /// <summary>
        /// Refer friend event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnReferFriends_Click(object sender, EventArgs e)
        {
            Process.Start(AppSettingsManager.ReferFriendUrl);
        }
    }
}
