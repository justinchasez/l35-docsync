using DocSync.Client.App.Controls.OptionsForms;
using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class SetOptionsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetOptionsView));
			this.tabBackupSchedule = new DocSync.Client.App.Controls.SwitchControl();
			this.tabOptions = new DocSync.Client.App.Controls.SwitchControl();
			this.panel1 = new System.Windows.Forms.Panel();
			this.ctrlOptionsView = new DocSync.Client.App.Controls.OptionsForms.OptionsView();
			this.ctrlBackupScheduleView = new DocSync.Client.App.Controls.OptionsForms.BackupScheduleView();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.tabBackupSchedule)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabOptions)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabBackupSchedule
			// 
			this.tabBackupSchedule.HoverImage = null;
			this.tabBackupSchedule.Image = ((System.Drawing.Image)(resources.GetObject("tabBackupSchedule.Image")));
			this.tabBackupSchedule.IsSelected = false;
			this.tabBackupSchedule.Location = new System.Drawing.Point(191, 300);
			this.tabBackupSchedule.Name = "tabBackupSchedule";
			this.tabBackupSchedule.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabBackupSchedule.NormalImage")));
			this.tabBackupSchedule.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabBackupSchedule.SelectedImage")));
			this.tabBackupSchedule.Size = new System.Drawing.Size(185, 33);
			this.tabBackupSchedule.TabIndex = 11;
			this.tabBackupSchedule.TabStop = false;
			this.tabBackupSchedule.Click += new System.EventHandler(this.TabBackupSchedule_Click);
			// 
			// tabOptions
			// 
			this.tabOptions.HoverImage = null;
			this.tabOptions.Image = ((System.Drawing.Image)(resources.GetObject("tabOptions.Image")));
			this.tabOptions.IsSelected = false;
			this.tabOptions.Location = new System.Drawing.Point(7, 300);
			this.tabOptions.Name = "tabOptions";
			this.tabOptions.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabOptions.NormalImage")));
			this.tabOptions.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabOptions.SelectedImage")));
			this.tabOptions.Size = new System.Drawing.Size(185, 33);
			this.tabOptions.TabIndex = 12;
			this.tabOptions.TabStop = false;
			this.tabOptions.Click += new System.EventHandler(this.TabOptions_Click);
			// 
			// panel1
			// 
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.ctrlOptionsView);
			this.panel1.Controls.Add(this.ctrlBackupScheduleView);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Location = new System.Drawing.Point(7, 6);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(500, 298);
			this.panel1.TabIndex = 10;
			// 
			// ctrlOptionsView
			// 
			this.ctrlOptionsView.BackupDesctop = false;
			this.ctrlOptionsView.TargetFolderLocation = "c:\\temp\\backup";
			this.ctrlOptionsView.IsProxyUsed = false;
			this.ctrlOptionsView.Location = new System.Drawing.Point(6, 6);
			this.ctrlOptionsView.LowPrioritySpeed = 100;
			this.ctrlOptionsView.Name = "ctrlOptionsView";
			this.ctrlOptionsView.ProxyAddress = null;
			this.ctrlOptionsView.ProxyPort = 0;
			this.ctrlOptionsView.RecoveryModeEnabled = false;
			this.ctrlOptionsView.Size = new System.Drawing.Size(486, 286);
			this.ctrlOptionsView.TabIndex = 1;
			this.ctrlOptionsView.UseLowPriority = false;
			// 
			// ctrlBackupScheduleView
			// 
			this.ctrlBackupScheduleView.EndDate = new System.DateTime(((long)(0)));
			this.ctrlBackupScheduleView.Location = new System.Drawing.Point(6, 6);
			this.ctrlBackupScheduleView.Name = "ctrlBackupScheduleView";
			this.ctrlBackupScheduleView.ScheduleInfo = ((DocSync.Client.Core.Entities.Settings.ScheduleSettingsInfo)(resources.GetObject("ctrlBackupScheduleView.ScheduleInfo")));
			this.ctrlBackupScheduleView.Size = new System.Drawing.Size(486, 286);
			this.ctrlBackupScheduleView.StartDate = new System.DateTime(((long)(0)));
			this.ctrlBackupScheduleView.TabIndex = 2;
			// 
			// panel3
			// 
			this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
			this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(5, 298);
			this.panel3.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel2.Location = new System.Drawing.Point(495, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(5, 298);
			this.panel2.TabIndex = 0;
			// 
			// SetOptionsView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tabBackupSchedule);
			this.Controls.Add(this.tabOptions);
			this.Controls.Add(this.panel1);
			this.Name = "SetOptionsView";
			this.Size = new System.Drawing.Size(712, 338);
			((System.ComponentModel.ISupportInitialize)(this.tabBackupSchedule)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabOptions)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private SwitchControl tabBackupSchedule;
        private SwitchControl tabOptions;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel2;
        private BackupScheduleView ctrlBackupScheduleView;
        private OptionsView ctrlOptionsView;

    }
}
