using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class AdvancedSchedulingView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvancedSchedulingView));
			this.btnAddBackupRule = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.btnClose = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.pnlScheduleOptionsContainer = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnAddBackupRule
			// 
			this.btnAddBackupRule.AllowTransparency = false;
			this.btnAddBackupRule.AnimatePress = true;
			this.btnAddBackupRule.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnAddBackupRule.HoverImage = null;
			this.btnAddBackupRule.Location = new System.Drawing.Point(18, 277);
			this.btnAddBackupRule.Name = "btnAddBackupRule";
			this.btnAddBackupRule.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAddBackupRule.NormalImage")));
			this.btnAddBackupRule.PressedImage = null;
			this.btnAddBackupRule.Size = new System.Drawing.Size(164, 33);
			this.btnAddBackupRule.TabIndex = 30;
			this.btnAddBackupRule.Click += new System.EventHandler(this.ButtonAddBackupRule_Click);
			// 
			// btnClose
			// 
			this.btnClose.AllowTransparency = false;
			this.btnClose.AnimatePress = true;
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnClose.HoverImage = null;
			this.btnClose.Location = new System.Drawing.Point(320, 277);
			this.btnClose.Name = "btnClose";
			this.btnClose.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClose.NormalImage")));
			this.btnClose.PressedImage = null;
			this.btnClose.Size = new System.Drawing.Size(164, 33);
			this.btnClose.TabIndex = 31;
			this.btnClose.Click += new System.EventHandler(this.ButtonCancel_Click);
			// 
			// pnlScheduleOptionsContainer
			// 
			this.pnlScheduleOptionsContainer.AutoScroll = true;
			this.pnlScheduleOptionsContainer.Location = new System.Drawing.Point(18, 64);
			this.pnlScheduleOptionsContainer.Name = "pnlScheduleOptionsContainer";
			this.pnlScheduleOptionsContainer.Size = new System.Drawing.Size(466, 199);
			this.pnlScheduleOptionsContainer.TabIndex = 29;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
			this.label2.Location = new System.Drawing.Point(14, 39);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(182, 22);
			this.label2.TabIndex = 27;
			this.label2.Text = "Back-up my computer...";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
			this.label1.Location = new System.Drawing.Point(3, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(283, 27);
			this.label1.TabIndex = 28;
			this.label1.Text = "Advanced Scheduling Options";
			// 
			// AdvancedSchedulingView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnAddBackupRule);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.pnlScheduleOptionsContainer);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "AdvancedSchedulingView";
			this.Size = new System.Drawing.Size(712, 338);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private ImageButton btnAddBackupRule;
        private ImageButton btnClose;
        private System.Windows.Forms.Panel pnlScheduleOptionsContainer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}