using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class RestoreFilesView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreFilesView));
			this.btnSearchFiles = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.btnRestoreFiles = new DocSync.Client.Controls.ImageButton.ImageButton();
			this.pictureBox5 = new System.Windows.Forms.PictureBox();
			this.pictureBox6 = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
			this.SuspendLayout();
			// 
			// btnSearchFiles
			// 
			this.btnSearchFiles.AllowTransparency = false;
			this.btnSearchFiles.AnimatePress = true;
			this.btnSearchFiles.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnSearchFiles.HoverImage = null;
			this.btnSearchFiles.Location = new System.Drawing.Point(106, 12);
			this.btnSearchFiles.Name = "btnSearchFiles";
			this.btnSearchFiles.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSearchFiles.NormalImage")));
			this.btnSearchFiles.PressedImage = null;
			this.btnSearchFiles.Size = new System.Drawing.Size(277, 44);
			this.btnSearchFiles.TabIndex = 14;
			this.btnSearchFiles.Click += new System.EventHandler(this.ButtonSearchFiles_Click);
			// 
			// btnRestoreFiles
			// 
			this.btnRestoreFiles.AllowTransparency = false;
			this.btnRestoreFiles.AnimatePress = true;
			this.btnRestoreFiles.DialogResult = System.Windows.Forms.DialogResult.None;
			this.btnRestoreFiles.HoverImage = null;
			this.btnRestoreFiles.Location = new System.Drawing.Point(106, 127);
			this.btnRestoreFiles.Name = "btnRestoreFiles";
			this.btnRestoreFiles.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRestoreFiles.NormalImage")));
			this.btnRestoreFiles.PressedImage = null;
			this.btnRestoreFiles.Size = new System.Drawing.Size(277, 44);
			this.btnRestoreFiles.TabIndex = 14;
			this.btnRestoreFiles.Click += new System.EventHandler(this.ButtonRestoreFiles_Click);
			// 
			// pictureBox5
			// 
			this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
			this.pictureBox5.Location = new System.Drawing.Point(14, 139);
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(60, 80);
			this.pictureBox5.TabIndex = 15;
			this.pictureBox5.TabStop = false;
			// 
			// pictureBox6
			// 
			this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
			this.pictureBox6.Location = new System.Drawing.Point(14, 12);
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new System.Drawing.Size(60, 80);
			this.pictureBox6.TabIndex = 15;
			this.pictureBox6.TabStop = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label1.Location = new System.Drawing.Point(101, 58);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(313, 34);
			this.label1.TabIndex = 16;
			this.label1.Text = "Select this option to search for files to restore by name\r\nand / or the last time" +
    " they were modified.";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.label3.Location = new System.Drawing.Point(101, 174);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(331, 51);
			this.label3.TabIndex = 18;
			this.label3.Text = "Select �Restore all of my files� to launch the DocSync\r\nRestore Wizard. The Resto" +
    "re Wizard will help you recover\r\nall the files in your backup to your new or rep" +
    "aired PC.";
			// 
			// RestoreFilesView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox6);
			this.Controls.Add(this.pictureBox5);
			this.Controls.Add(this.btnRestoreFiles);
			this.Controls.Add(this.btnSearchFiles);
			this.Name = "RestoreFilesView";
			this.Size = new System.Drawing.Size(712, 338);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private ImageButton btnSearchFiles;
		private ImageButton btnRestoreFiles;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;

    }
}
