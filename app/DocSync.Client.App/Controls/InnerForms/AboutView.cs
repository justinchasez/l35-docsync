using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Controls.Properties;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The view for About tab.
    /// </summary>
    public partial class AboutView : ViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AboutView"/> class.
        /// </summary>
        public AboutView()
        {
            this.InitializeComponent();

            this.Load += this.AboutView_Load;
        }

        /// <summary>
        /// Handles the Load event of the AboutView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AboutView_Load(object sender, EventArgs e)
        {
            this.lblEmail.Text = ClientSettingsManager.Settings.Credentials.Name;
            this.lblBuildVersion.Text = Application.ProductVersion;
            this.lblSubscriptionEnds.Text = String.Format(this.lblSubscriptionEnds.Text, ClientSettingsManager.Settings.CurrentComputer.ExpirationDate.ToString("D", new CultureInfo("en-us")));
            this.lblMembershipPlanName.Text = (ClientSettingsManager.Settings.CurrentComputer.IsTrialComputer)
                                                  ? Resources.AboutViewMembershipPlanTrial
                                                  : Resources.AboutViewMembershipPlanPaid;
        }

        /// <summary>
        /// Handles the LinkClicked event of the LoginToProfile control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LoginToProfile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Process.Start(AppSettingsManager.ProfilePageUrl);
            Process.Start(AppSettingsManager.CouponUrl);
        }

        /// <summary>
        /// Handles the LinkClicked event of the RenewSubscription control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void RenewSubscription_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Process.Start(AppSettingsManager.RenewPageUrl);
            Process.Start(AppSettingsManager.CouponUrl);
        }

        /// <summary>
        /// Refer friend event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnReferFriends_Click(object sender, EventArgs e)
        {
            Process.Start(AppSettingsManager.ReferFriendUrl);
        }
    }
}
