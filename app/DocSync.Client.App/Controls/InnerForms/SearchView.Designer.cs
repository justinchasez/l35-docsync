using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.InnerForms
{
    partial class SearchView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.rbtnSearchAllDates = new System.Windows.Forms.RadioButton();
            this.rbtnModifiedToday = new System.Windows.Forms.RadioButton();
            this.rbtnSinceYesterday = new System.Windows.Forms.RadioButton();
            this.rbtnLast7days = new System.Windows.Forms.RadioButton();
            this.rbtnLast30Days = new System.Windows.Forms.RadioButton();
            this.rbtnSpecific = new System.Windows.Forms.RadioButton();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerAfter = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerBefore = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.filesGridView = new System.Windows.Forms.DataGridView();
            this.selectColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Icon = new System.Windows.Forms.DataGridViewImageColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.lblMatchesCount = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.linkHelpRestore = new System.Windows.Forms.LinkLabel();
            this.linkMyBackup = new System.Windows.Forms.LinkLabel();
            this.checkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.lblClear = new System.Windows.Forms.Label();
            this.btnRestoreSelected = new ImageButton();
            this.lblResultsCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(1, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "SEARCH CRITERIA";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 74);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 2);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(190, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(2, 337);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtSearch.Location = new System.Drawing.Point(1, 44);
            this.txtSearch.MaxLength = 30;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(155, 22);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.TextChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(-1, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Full or partial filename:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(163, 43);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(23, 23);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(14, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 19);
            this.label3.TabIndex = 5;
            this.label3.Text = "Last-modified date";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(4, 87);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(10, 10);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // rbtnSearchAllDates
            // 
            this.rbtnSearchAllDates.AutoSize = true;
            this.rbtnSearchAllDates.Checked = true;
            this.rbtnSearchAllDates.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnSearchAllDates.Location = new System.Drawing.Point(4, 103);
            this.rbtnSearchAllDates.Name = "rbtnSearchAllDates";
            this.rbtnSearchAllDates.Size = new System.Drawing.Size(113, 17);
            this.rbtnSearchAllDates.TabIndex = 7;
            this.rbtnSearchAllDates.TabStop = true;
            this.rbtnSearchAllDates.Text = "         Search all days";
            this.rbtnSearchAllDates.UseVisualStyleBackColor = true;
            this.rbtnSearchAllDates.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // rbtnModifiedToday
            // 
            this.rbtnModifiedToday.AutoSize = true;
            this.rbtnModifiedToday.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnModifiedToday.Location = new System.Drawing.Point(4, 123);
            this.rbtnModifiedToday.Name = "rbtnModifiedToday";
            this.rbtnModifiedToday.Size = new System.Drawing.Size(114, 17);
            this.rbtnModifiedToday.TabIndex = 7;
            this.rbtnModifiedToday.Text = "         Modified today";
            this.rbtnModifiedToday.UseVisualStyleBackColor = true;
            this.rbtnModifiedToday.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // rbtnSinceYesterday
            // 
            this.rbtnSinceYesterday.AutoSize = true;
            this.rbtnSinceYesterday.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnSinceYesterday.Location = new System.Drawing.Point(4, 143);
            this.rbtnSinceYesterday.Name = "rbtnSinceYesterday";
            this.rbtnSinceYesterday.Size = new System.Drawing.Size(161, 17);
            this.rbtnSinceYesterday.TabIndex = 7;
            this.rbtnSinceYesterday.Text = "         Modified since yesterday";
            this.rbtnSinceYesterday.UseVisualStyleBackColor = true;
            this.rbtnSinceYesterday.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // rbtnLast7days
            // 
            this.rbtnLast7days.AutoSize = true;
            this.rbtnLast7days.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnLast7days.Location = new System.Drawing.Point(4, 163);
            this.rbtnLast7days.Name = "rbtnLast7days";
            this.rbtnLast7days.Size = new System.Drawing.Size(166, 17);
            this.rbtnLast7days.TabIndex = 7;
            this.rbtnLast7days.Text = "         Modified in the last 7 days";
            this.rbtnLast7days.UseVisualStyleBackColor = true;
            this.rbtnLast7days.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // rbtnLast30Days
            // 
            this.rbtnLast30Days.AutoSize = true;
            this.rbtnLast30Days.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnLast30Days.Location = new System.Drawing.Point(4, 183);
            this.rbtnLast30Days.Name = "rbtnLast30Days";
            this.rbtnLast30Days.Size = new System.Drawing.Size(172, 17);
            this.rbtnLast30Days.TabIndex = 7;
            this.rbtnLast30Days.Text = "         Modified in the last 30 days";
            this.rbtnLast30Days.UseVisualStyleBackColor = true;
            this.rbtnLast30Days.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // rbtnSpecific
            // 
            this.rbtnSpecific.AutoSize = true;
            this.rbtnSpecific.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnSpecific.Location = new System.Drawing.Point(4, 203);
            this.rbtnSpecific.Name = "rbtnSpecific";
            this.rbtnSpecific.Size = new System.Drawing.Size(138, 17);
            this.rbtnSpecific.TabIndex = 7;
            this.rbtnSpecific.Text = "         Specific Date Range:";
            this.rbtnSpecific.UseVisualStyleBackColor = true;
            this.rbtnSpecific.CheckedChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(21, 103);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.TabIndex = 8;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(21, 123);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 8;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(21, 143);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.TabIndex = 8;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(21, 163);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(16, 16);
            this.pictureBox11.TabIndex = 8;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(21, 203);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(16, 16);
            this.pictureBox12.TabIndex = 8;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(21, 183);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(16, 16);
            this.pictureBox13.TabIndex = 8;
            this.pictureBox13.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(18, 226);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "After:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(18, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Before:";
            // 
            // dateTimePickerAfter
            // 
            this.dateTimePickerAfter.Enabled = false;
            this.dateTimePickerAfter.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerAfter.Location = new System.Drawing.Point(64, 222);
            this.dateTimePickerAfter.Name = "dateTimePickerAfter";
            this.dateTimePickerAfter.Size = new System.Drawing.Size(112, 20);
            this.dateTimePickerAfter.TabIndex = 11;
            this.dateTimePickerAfter.ValueChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // dateTimePickerBefore
            // 
            this.dateTimePickerBefore.Enabled = false;
            this.dateTimePickerBefore.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerBefore.Location = new System.Drawing.Point(65, 246);
            this.dateTimePickerBefore.Name = "dateTimePickerBefore";
            this.dateTimePickerBefore.Size = new System.Drawing.Size(112, 20);
            this.dateTimePickerBefore.TabIndex = 11;
            this.dateTimePickerBefore.ValueChanged += new System.EventHandler(this.UpdateDataEventHandler);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(195, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 61);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(510, 57);
            this.panel2.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(11, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(464, 51);
            this.label6.TabIndex = 0;
            this.label6.Text = "To restore the latest version of any file, check the box next to the filename and" +
                " \r\nclick �Restore Selected�. For previous versions, click on the filename to loc" +
                "ate the \r\nfile in the Back-up Drive.";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(0, 269);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(460, 2);
            this.pictureBox6.TabIndex = 1;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(252, 269);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(460, 2);
            this.pictureBox7.TabIndex = 1;
            this.pictureBox7.TabStop = false;
            // 
            // filesGridView
            // 
            this.filesGridView.AllowUserToAddRows = false;
            this.filesGridView.AllowUserToDeleteRows = false;
            this.filesGridView.AllowUserToResizeRows = false;
            this.filesGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.filesGridView.BackgroundColor = System.Drawing.Color.White;
            this.filesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.filesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.selectColumn,
            this.Icon});
            this.filesGridView.Location = new System.Drawing.Point(194, 93);
            this.filesGridView.Name = "filesGridView";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.filesGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.filesGridView.RowHeadersVisible = false;
            this.filesGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.filesGridView.ShowCellToolTips = false;
            this.filesGridView.Size = new System.Drawing.Size(515, 174);
            this.filesGridView.TabIndex = 13;
            this.filesGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.FilesGridView_CellClick);
            this.filesGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.FilesGridView_CellMouseUp);
            this.filesGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.FilesGridView_CurrentCellDirtyStateChanged);
            this.filesGridView.SelectionChanged += new System.EventHandler(this.FilesGridView_SelectionChanged);
            // 
            // selectColumn
            // 
            this.selectColumn.FalseValue = "false";
            this.selectColumn.HeaderText = "";
            this.selectColumn.Name = "selectColumn";
            this.selectColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.selectColumn.TrueValue = "true";
            this.selectColumn.Width = 5;
            // 
            // Icon
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.NullValue = "null";
            this.Icon.DefaultCellStyle = dataGridViewCellStyle1;
            this.Icon.HeaderText = "";
            this.Icon.Image = ((System.Drawing.Image)(resources.GetObject("Icon.Image")));
            this.Icon.Name = "Icon";
            this.Icon.ReadOnly = true;
            this.Icon.Width = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label7.Location = new System.Drawing.Point(196, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 22);
            this.label7.TabIndex = 14;
            this.label7.Text = "Results";
            // 
            // lblMatchesCount
            // 
            this.lblMatchesCount.AutoSize = true;
            this.lblMatchesCount.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblMatchesCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblMatchesCount.Location = new System.Drawing.Point(567, 68);
            this.lblMatchesCount.Name = "lblMatchesCount";
            this.lblMatchesCount.Size = new System.Drawing.Size(139, 22);
            this.lblMatchesCount.TabIndex = 14;
            this.lblMatchesCount.Text = "Match(es) Found";
            this.lblMatchesCount.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label8.Location = new System.Drawing.Point(14, 274);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(155, 19);
            this.label8.TabIndex = 5;
            this.label8.Text = "Other Restore Options";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(4, 282);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(10, 10);
            this.pictureBox8.TabIndex = 6;
            this.pictureBox8.TabStop = false;
            // 
            // linkHelpRestore
            // 
            this.linkHelpRestore.AutoSize = true;
            this.linkHelpRestore.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.linkHelpRestore.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.linkHelpRestore.Location = new System.Drawing.Point(18, 317);
            this.linkHelpRestore.Name = "linkHelpRestore";
            this.linkHelpRestore.Size = new System.Drawing.Size(157, 14);
            this.linkHelpRestore.TabIndex = 15;
            this.linkHelpRestore.TabStop = true;
            this.linkHelpRestore.Text = "Help me restore all my files";
            this.linkHelpRestore.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkHelpRestore_LinkClicked);
            // 
            // linkMyBackup
            // 
            this.linkMyBackup.AutoSize = true;
            this.linkMyBackup.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.linkMyBackup.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.linkMyBackup.Location = new System.Drawing.Point(18, 298);
            this.linkMyBackup.Name = "linkMyBackup";
            this.linkMyBackup.Size = new System.Drawing.Size(137, 14);
            this.linkMyBackup.TabIndex = 15;
            this.linkMyBackup.TabStop = true;
            this.linkMyBackup.Text = "Browse my backup drive";
            this.linkMyBackup.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkMyBackup_LinkClicked);
            // 
            // checkBoxSelectAll
            // 
            this.checkBoxSelectAll.AutoSize = true;
            this.checkBoxSelectAll.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.checkBoxSelectAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.checkBoxSelectAll.Location = new System.Drawing.Point(198, 273);
            this.checkBoxSelectAll.Name = "checkBoxSelectAll";
            this.checkBoxSelectAll.Size = new System.Drawing.Size(79, 21);
            this.checkBoxSelectAll.TabIndex = 16;
            this.checkBoxSelectAll.Text = "Select All";
            this.checkBoxSelectAll.ThreeState = true;
            this.checkBoxSelectAll.UseVisualStyleBackColor = true;
            this.checkBoxSelectAll.Click += new System.EventHandler(this.CheckBoxSelectAll_Click);
            // 
            // lblClear
            // 
            this.lblClear.AutoSize = true;
            this.lblClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblClear.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblClear.Location = new System.Drawing.Point(559, 275);
            this.lblClear.Name = "lblClear";
            this.lblClear.Size = new System.Drawing.Size(150, 17);
            this.lblClear.TabIndex = 17;
            this.lblClear.Text = "[X]  Clear Search Results";
            this.lblClear.Click += new System.EventHandler(this.LblClear_Click);
            // 
            // btnRestoreSelected
            // 
            this.btnRestoreSelected.AllowTransparency = false;
            this.btnRestoreSelected.AnimatePress = true;
            this.btnRestoreSelected.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnRestoreSelected.Enabled = false;
            this.btnRestoreSelected.HoverImage = null;
            this.btnRestoreSelected.Location = new System.Drawing.Point(198, 299);
            this.btnRestoreSelected.Name = "btnRestoreSelected";
            this.btnRestoreSelected.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRestoreSelected.NormalImage")));
            this.btnRestoreSelected.PressedImage = null;
            this.btnRestoreSelected.Size = new System.Drawing.Size(164, 33);
            this.btnRestoreSelected.TabIndex = 18;
            this.btnRestoreSelected.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnRestoreSelected.Click += new System.EventHandler(this.BtnRestoreSelected_Click);
            // 
            // lblResultsCount
            // 
            this.lblResultsCount.AutoSize = true;
            this.lblResultsCount.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblResultsCount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.lblResultsCount.Location = new System.Drawing.Point(510, 68);
            this.lblResultsCount.Name = "lblResultsCount";
            this.lblResultsCount.Size = new System.Drawing.Size(19, 22);
            this.lblResultsCount.TabIndex = 19;
            this.lblResultsCount.Text = "0";
            // 
            // SearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblResultsCount);
            this.Controls.Add(this.btnRestoreSelected);
            this.Controls.Add(this.lblClear);
            this.Controls.Add(this.checkBoxSelectAll);
            this.Controls.Add(this.linkMyBackup);
            this.Controls.Add(this.linkHelpRestore);
            this.Controls.Add(this.lblMatchesCount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.filesGridView);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dateTimePickerBefore);
            this.Controls.Add(this.dateTimePickerAfter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.rbtnSpecific);
            this.Controls.Add(this.rbtnLast30Days);
            this.Controls.Add(this.rbtnLast7days);
            this.Controls.Add(this.rbtnSinceYesterday);
            this.Controls.Add(this.rbtnModifiedToday);
            this.Controls.Add(this.rbtnSearchAllDates);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Name = "SearchView";
            this.Size = new System.Drawing.Size(712, 338);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filesGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.RadioButton rbtnSearchAllDates;
        private System.Windows.Forms.RadioButton rbtnModifiedToday;
        private System.Windows.Forms.RadioButton rbtnSinceYesterday;
        private System.Windows.Forms.RadioButton rbtnLast7days;
        private System.Windows.Forms.RadioButton rbtnLast30Days;
        private System.Windows.Forms.RadioButton rbtnSpecific;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePickerAfter;
        private System.Windows.Forms.DateTimePicker dateTimePickerBefore;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.DataGridView filesGridView;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblMatchesCount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.LinkLabel linkHelpRestore;
        private System.Windows.Forms.LinkLabel linkMyBackup;
        private System.Windows.Forms.CheckBox checkBoxSelectAll;
        private System.Windows.Forms.Label lblClear;
        private ImageButton btnRestoreSelected;
        private System.Windows.Forms.Label lblResultsCount;
        private System.Windows.Forms.DataGridViewCheckBoxColumn selectColumn;
        private System.Windows.Forms.DataGridViewImageColumn Icon;

    }
}
