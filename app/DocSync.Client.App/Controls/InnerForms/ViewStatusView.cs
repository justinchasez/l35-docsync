using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The view for View Status tab.
    /// </summary>
    public partial class ViewStatusView : ViewBase
    {
        /// <summary>
        /// Current selected tab.
        /// </summary>
        private StatusViewTab selectedTab = StatusViewTab.Restore;

        /// <summary>
        /// The tab controls.
        /// </summary>
        private Dictionary<StatusViewTab, SwitchControl> tabs = new Dictionary<StatusViewTab, SwitchControl>();

        /// <summary>
        /// The tab view controls.
        /// </summary>
        private Dictionary<StatusViewTab, UserControl> tabViews = new Dictionary<StatusViewTab, UserControl>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewStatusView"/> class.
        /// </summary>
        public ViewStatusView()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.Load += this.ViewStatusView_Load;
        }

        /// <summary>
        /// Handles the Load event of the ViewStatusView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void ViewStatusView_Load(object sender, EventArgs e)
        {
            if (this.IsDesignMode)
            {
                return;
            }

            // Initialize tabs.
            this.tabs.Add(StatusViewTab.Backup, this.tabBackupStatus);
            this.tabs.Add(StatusViewTab.Restore, this.tabRstoreStatus);

            // Initialize tab views.
            this.tabViews.Add(StatusViewTab.Backup, this.ctrlStatusViewBackup);
            this.tabViews.Add(StatusViewTab.Restore, this.ctrlStatusViewRestore);

            EventPublisher.UIEventManagerInstance.SwitchStatusTab += this.UIEventManagerInstance_SwitchStatusTab;
            EventPublisher.UIEventManagerInstance.OnSwitchStatusTab(new SwitchStatusTabEventArgs { SelectedTab = StatusViewTab.Backup });
        }

        /// <summary>
        /// UIs the event manager instance_ switch status tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchStatusTabEventArgs"/> instance containing the event data.</param>
        private void UIEventManagerInstance_SwitchStatusTab(SwitchStatusTabEventArgs args)
        {
            if (this.selectedTab != args.SelectedTab)
            {
                this.SwitchTab(args.SelectedTab);
            }
        }

        /// <summary>
        /// Refer friend event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnReferFriends_Click(object sender, EventArgs e)
        {
            Process.Start(AppSettingsManager.ReferFriendUrl);
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="tab">The view tab.</param>
        private void SwitchTab(StatusViewTab tab)
        {
            this.tabs[this.selectedTab].IsSelected = false;
            this.tabs[tab].IsSelected = true;

            this.tabViews[this.selectedTab].Visible = false;
            this.tabViews[tab].Visible = true;

            this.selectedTab = tab;
        }

        /// <summary>
        /// Handles the Click event of the TabBackupStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabBackupStatus_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchStatusTab(new SwitchStatusTabEventArgs { SelectedTab = StatusViewTab.Backup });
        }

        /// <summary>
        /// Handles the Click event of the TabRestoreStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void TabRestoreStatus_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchStatusTab(new SwitchStatusTabEventArgs { SelectedTab = StatusViewTab.Restore });
        }
    }
}
