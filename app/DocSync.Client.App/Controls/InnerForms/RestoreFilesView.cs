using System;
using System.Diagnostics;
using System.Threading;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Utils;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// The view for RestoreFiles tab.
    /// </summary>
    public partial class RestoreFilesView : ViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreFilesView"/> class.
        /// </summary>
        public RestoreFilesView()
        {
            this.InitializeComponent();

            if (!this.DesignMode)
            {
            }

            EventPublisher.UIEventManagerInstance.LaunchRestoreWizard += this.UIEventManagerInstance_LaunchRestoreWizard;

            this.restoreWizard = new RestoreWizard();
            this.restoreWizardSyncObject = new EventWaitHandle(true, EventResetMode.AutoReset);
        }

        /// <summary>
        /// restore wizard window, must be single for app
        /// </summary>
        private readonly RestoreWizard restoreWizard;

        /// <summary>
        /// Mutex object to sync access to restore wizard
        /// </summary>
        private EventWaitHandle restoreWizardSyncObject;

        /// <summary>
        /// Event handler to launch restore wizard
        /// </summary>
        /// <param name="args">event argument</param>
        private void UIEventManagerInstance_LaunchRestoreWizard(SetOptionsEventArgs args)
        {
            if (this.restoreWizardSyncObject.WaitOne(500, false))
            {
                try
                {
                    this.restoreWizard.ShowDialog(this);
                    EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Start });
                }
                catch (Exception ex)
                {
                    Loger.Instance.Log(ex.ToString());
                }
                finally
                {
                    this.restoreWizardSyncObject.Set();
                }
            }
            else
            {
                this.restoreWizard.Activate();
//                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
//                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Start });
            }
        }

        /// <summary>
        /// Handles the Click event of the SearchFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonSearchFiles_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Search });
        }

        /// <summary>
        /// Handles the Click event of the ButtonRestoreFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonRestoreFiles_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnLaunchRestoreWizard(new SetOptionsEventArgs());
        }

        /// <summary>
        /// Handles the Click event of the ButtonBrowseBackupDrive control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBrowseBackupDrive_Click(object sender, EventArgs e)
        {
            Process.Start("explorer", "/e, ::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\\");
        }

        /// <summary>
        /// Refer friend event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnReferFriends_Click(object sender, EventArgs e)
        {
            Process.Start(AppSettingsManager.ReferFriendUrl);
        }
    }
}
