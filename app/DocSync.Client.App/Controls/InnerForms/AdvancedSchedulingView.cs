using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Entities.Schedule.Interface;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls.InnerForms
{
    /// <summary>
    /// Advanced Scheduling View cotnrol.
    /// </summary>
    public partial class AdvancedSchedulingView : ViewBase
    {
        /// <summary>
        /// The schedule info controls.
        /// </summary>
        private readonly Dictionary<Guid, ScheduleRuleInfo> scheduleControls;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvancedSchedulingView"/> class.
        /// </summary>
        public AdvancedSchedulingView()
        {
            this.scheduleControls = new Dictionary<Guid, ScheduleRuleInfo>();

            this.InitializeComponent();

            this.Load += this.AdvancedSchedulingView_Load;
            this.VisibleChanged += this.AdvancedSchedulingView_VisibleChanged;
        }

        /// <summary>
        /// Handles the Load event of the AdvancedSchedulingView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AdvancedSchedulingView_Load(object sender, EventArgs e)
        {
            EventPublisher.ScheduleManager.ScheduleSaved += this.ScheduleManager_ScheduleSaved;
            EventPublisher.ScheduleManager.ScheduleRemoved += this.ScheduleManager_ScheduleRemoved;
        }

        /// <summary>
        /// Schedules the manager_ schedule removed.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        private void ScheduleManager_ScheduleRemoved(ScheduleEventArgs args)
        {
            if (this.scheduleControls.ContainsKey(args.Schedule.Id))
            {
                ScheduleRuleInfo currentControl = this.scheduleControls[args.Schedule.Id];
                if (currentControl != null)
                {
                    this.pnlScheduleOptionsContainer.Controls.Remove(currentControl);
                }
            }
        }

        /// <summary>
        /// Handles the VisibleChanged event of the AdvancedSchedulingView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AdvancedSchedulingView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.DesignMode)
            {
                return;
            }

            if (this.Visible && this.scheduleControls.Count == 0)
            {
                this.pnlScheduleOptionsContainer.Controls.Clear();

                Dictionary<Guid, ISchedule> schedules = RemotingManager.Instance.ProxyManager.ScheduleManager.InitializeSchedules();
                foreach (ISchedule schedule in schedules.Values)
                {
                    ScheduleRuleInfo info = new ScheduleRuleInfo(schedule) { Dock = DockStyle.Top };
                    this.pnlScheduleOptionsContainer.Controls.Add(info);

                    this.scheduleControls.Add(schedule.Id, info);
                }
            }
        }

        /// <summary>
        /// Schedules the event proxy_ load schedule.
        /// </summary>
        /// <param name="args">The <see cref="ScheduleEventArgs"/> instance containing the event data.</param>
        private void ScheduleManager_ScheduleSaved(ScheduleEventArgs args)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                ScheduleRuleInfo info = new ScheduleRuleInfo(args.Schedule) { Dock = DockStyle.Top };

                if (this.scheduleControls.ContainsKey(args.Schedule.Id))
                {
                    this.pnlScheduleOptionsContainer.Controls.Remove(this.scheduleControls[args.Schedule.Id]);
                    this.pnlScheduleOptionsContainer.Controls.Add(info);
                    this.scheduleControls[args.Schedule.Id] = info;
                }
                else
                {
                    this.pnlScheduleOptionsContainer.Controls.Add(info);
                    this.scheduleControls.Add(args.Schedule.Id, info);
                }
            }));
        }

        /// <summary>
        /// Handles the Click event of the btnCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Options });
        }

        /// <summary>
        /// Handles the Click event of the btnAddBackupRule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonAddBackupRule_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.RulesEditor });
        }
    }
}