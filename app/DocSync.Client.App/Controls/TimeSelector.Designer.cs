namespace DocSync.Client.App.Controls
{
    partial class TimeSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbMins = new System.Windows.Forms.ComboBox();
            this.rbtnPm = new System.Windows.Forms.RadioButton();
            this.cmbHours = new System.Windows.Forms.ComboBox();
            this.rbtnAm = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmbMins
            // 
            this.cmbMins.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMins.Font = new System.Drawing.Font("Calibri", 12F);
            this.cmbMins.FormattingEnabled = true;
            this.cmbMins.Items.AddRange(new object[] {
            "00",
            "05",
            "10",
            "15",
            "20",
            "25",
            "30",
            "35",
            "40",
            "45",
            "50",
            "55"});
            this.cmbMins.Location = new System.Drawing.Point(63, 3);
            this.cmbMins.Name = "cmbMins";
            this.cmbMins.Size = new System.Drawing.Size(50, 27);
            this.cmbMins.TabIndex = 8;
            this.cmbMins.SelectedValueChanged += new System.EventHandler(this.CmbMinsSelectedValueChanged);
            // 
            // rbtnPm
            // 
            this.rbtnPm.AutoSize = true;
            this.rbtnPm.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnPm.Location = new System.Drawing.Point(172, 3);
            this.rbtnPm.Name = "rbtnPm";
            this.rbtnPm.Size = new System.Drawing.Size(53, 27);
            this.rbtnPm.TabIndex = 5;
            this.rbtnPm.TabStop = true;
            this.rbtnPm.Text = "pm";
            this.rbtnPm.UseVisualStyleBackColor = true;
            // 
            // cmbHours
            // 
            this.cmbHours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHours.Font = new System.Drawing.Font("Calibri", 12F);
            this.cmbHours.FormattingEnabled = true;
            this.cmbHours.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cmbHours.Location = new System.Drawing.Point(3, 3);
            this.cmbHours.Name = "cmbHours";
            this.cmbHours.Size = new System.Drawing.Size(50, 27);
            this.cmbHours.TabIndex = 7;
            this.cmbHours.SelectedValueChanged += new System.EventHandler(this.CmbHoursSelectedValueChanged);
            // 
            // rbtnAm
            // 
            this.rbtnAm.AutoSize = true;
            this.rbtnAm.Checked = true;
            this.rbtnAm.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnAm.Location = new System.Drawing.Point(119, 3);
            this.rbtnAm.Name = "rbtnAm";
            this.rbtnAm.Size = new System.Drawing.Size(52, 27);
            this.rbtnAm.TabIndex = 6;
            this.rbtnAm.TabStop = true;
            this.rbtnAm.Text = "am";
            this.rbtnAm.UseVisualStyleBackColor = true;
            this.rbtnAm.CheckedChanged += new System.EventHandler(this.RbtnAmCheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F);
            this.label2.Location = new System.Drawing.Point(51, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = ":";
            // 
            // TimeSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cmbMins);
            this.Controls.Add(this.rbtnPm);
            this.Controls.Add(this.cmbHours);
            this.Controls.Add(this.rbtnAm);
            this.Controls.Add(this.label2);
            this.Name = "TimeSelector";
            this.Size = new System.Drawing.Size(220, 33);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbMins;
        private System.Windows.Forms.RadioButton rbtnPm;
        private System.Windows.Forms.ComboBox cmbHours;
        private System.Windows.Forms.RadioButton rbtnAm;
        private System.Windows.Forms.Label label2;

    }
}
