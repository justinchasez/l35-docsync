using System;
using System.Windows.Forms;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Controls.Properties;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls.OptionsForms
{
    /// <summary>
    /// The options view.
    /// </summary>
    public partial class OptionsView : UserControl
    {
        /// <summary>
        /// The desctop backup mode.
        /// </summary>
        private bool desctopAutomaticBackup;

        /// <summary>
        /// Gets or sets a value indicating whether [desctop backup mode].
        /// </summary>
        /// <value><c>true</c> if [desctop backup mode]; otherwise, <c>false</c>.</value>
        public bool BackupDesctop
        {
            get { return this.desctopAutomaticBackup; }
            set
            {
                this.desctopAutomaticBackup = value;
                this.ChangeDesctopControlsMode(this.desctopAutomaticBackup);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [display folder dots].
        /// </summary>
        /// <value><c>true</c> if [display folder dots]; otherwise, <c>false</c>.</value>
        public string TargetFolderLocation
        {
            get { return this.tbTargetFolder.Text; }
            set { this.tbTargetFolder.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [use low priority].
        /// </summary>
        /// <value><c>true</c> if [use low priority]; otherwise, <c>false</c>.</value>
        public bool UseLowPriority
        {
            get { return this.chbUseLowPriority.Checked; }

            set
            {
                this.chbUseLowPriority.Checked = value;
                this.numericSpeed.Enabled = value;
            }
        }

        /// <summary>
        /// Gets or sets low priority speed
        /// </summary>
        public int LowPrioritySpeed
        {
            get { return Convert.ToInt32(this.numericSpeed.Value); }
            set { this.numericSpeed.Value = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [recovery mode enabled].
        /// </summary>
        /// <value><c>true</c> if [recovery mode enabled]; otherwise, <c>false</c>.</value>
        public bool RecoveryModeEnabled
        {
            get { return this.chbRecoverMode.Checked; }
            set { this.chbRecoverMode.Checked = value; }
        }

        /// <summary>
        /// Is prixy used
        /// </summary>
        public bool IsProxyUsed { get; set; }

        /// <summary>
        /// The proxy address
        /// </summary>
        public string ProxyAddress { get; set; }

        /// <summary>
        /// The proxy port
        /// </summary>
        public int ProxyPort { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OptionsView"/> class.
        /// </summary>
        public OptionsView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the LinkClicked event of the ButtonDesctopBackup control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void ButtonDesctopBackup_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.desctopAutomaticBackup = !this.desctopAutomaticBackup;

            this.ChangeDesctopControlsMode(this.desctopAutomaticBackup);
        }

        /// <summary>
        /// Changes the desctop controls mode.
        /// </summary>
        /// <param name="isAutomatic">if set to <c>true</c> [is automatic].</param>
        private void ChangeDesctopControlsMode(bool isAutomatic)
        {
            this.lblBackupMode.Text = isAutomatic ?
                            Resources.BackupDesktopModeAutomatic :
                            Resources.BackupDecktopModeManual;

            this.lbtnDesctopBackup.Text = isAutomatic ?
                            Resources.BackupDesktopLinkStop :
                            Resources.BackupDesktopLink;
        }

        /// <summary>
        /// Event handler [when Checked Changed UseLowPriority property]
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event agrument</param>
        private void ChbUseLowPriorityCheckedChanged(object sender, EventArgs e)
        {
            this.numericSpeed.Enabled = this.chbUseLowPriority.Checked;
            RemotingManager.Instance.ProxyPublisher.NotifyOptionChanged(new SetOptionsEventArgs()
                                                                        {
                                                                            Option = AppOptions.LowPriority,
                                                                            Value = this.UseLowPriority ? 1 : 0
                                                                        });
        }

        /// <summary>
        /// Event handler [when Checked Changed RecoverMode property]
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event agrument</param>
        private void ChbRecoverModeCheckedChanged(object sender, EventArgs e)
        {
            RemotingManager.Instance.ProxyPublisher.NotifyOptionChanged(new SetOptionsEventArgs()
                                                                        {
                                                                            Option = AppOptions.RecoverMode,
                                                                            Value = this.RecoveryModeEnabled ? 1 : 0
                                                                        });
        }

        /// <summary>
        /// BtnUseProxy click event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event argements</param>
        private void BtnUseProxy_Click(object sender, EventArgs e)
        {
            UseProxyServerDialog useProxyServerDialog = new UseProxyServerDialog(this.IsProxyUsed, this.ProxyAddress, this.ProxyPort);
            useProxyServerDialog.ShowDialog();
            if (useProxyServerDialog.IsOk)
            {
                this.IsProxyUsed = useProxyServerDialog.IsProxyUsed;
                this.ProxyAddress = useProxyServerDialog.ProxyAddress;
                this.ProxyPort = useProxyServerDialog.ProxyPort;
                
                RemotingManager.Instance.ProxyPublisher.NotifyOptionChanged(new SetOptionsEventArgs()
                {
                    Option = AppOptions.ProxyServer
                });
            }
        }

		private void button1_Click(object sender, EventArgs e)
		{
			folderBrowserDialog1.SelectedPath = this.tbTargetFolder.Text;
			if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
			{
				this.tbTargetFolder.Text = folderBrowserDialog1.SelectedPath;
			}
		}

		private void tbTargetFolder_TextChanged(object sender, EventArgs e)
		{
			RemotingManager.Instance.ProxyPublisher.NotifyOptionChanged(new SetOptionsEventArgs()
			{
				Option = AppOptions.TargetLocation,
				StringValue = this.TargetFolderLocation
			});
		}
    }
}
