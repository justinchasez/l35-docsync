using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.OptionsForms
{
    partial class BackupScheduleView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnAutmaticBackupMode = new System.Windows.Forms.RadioButton();
            this.rbtnDailyBackup = new System.Windows.Forms.RadioButton();
            this.rbtnBetween = new System.Windows.Forms.RadioButton();
            this.rbtnCustom = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbtnViewSetShedulingOptions = new System.Windows.Forms.LinkLabel();
            this.pnlNotBetween = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.ctrlTimeSelectorBetweenEnd = new TimeSelector();
            this.ctrlTimeSelectorBetweenStart = new TimeSelector();
            this.pnlDailySettings = new System.Windows.Forms.Panel();
            this.ctrlTimeSelectorDaily = new TimeSelector();
            this.btnApplyChanges = new ImageButton();
            this.btnCancel = new ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlNotBetween.SuspendLayout();
            this.pnlDailySettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbtnAutmaticBackupMode
            // 
            this.rbtnAutmaticBackupMode.AutoSize = true;
            this.rbtnAutmaticBackupMode.Checked = true;
            this.rbtnAutmaticBackupMode.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnAutmaticBackupMode.Location = new System.Drawing.Point(10, 17);
            this.rbtnAutmaticBackupMode.Name = "rbtnAutmaticBackupMode";
            this.rbtnAutmaticBackupMode.Size = new System.Drawing.Size(280, 27);
            this.rbtnAutmaticBackupMode.TabIndex = 0;
            this.rbtnAutmaticBackupMode.TabStop = true;
            this.rbtnAutmaticBackupMode.Text = "Update my backup automatically";
            this.rbtnAutmaticBackupMode.UseVisualStyleBackColor = true;
            this.rbtnAutmaticBackupMode.CheckedChanged += new System.EventHandler(this.RbtnAutmaticBackupModeCheckedChanged);
            this.rbtnAutmaticBackupMode.Click += new System.EventHandler(this.BackupScheduleMode_Click);
            // 
            // rbtnDailyBackup
            // 
            this.rbtnDailyBackup.AutoSize = true;
            this.rbtnDailyBackup.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnDailyBackup.Location = new System.Drawing.Point(10, 47);
            this.rbtnDailyBackup.Name = "rbtnDailyBackup";
            this.rbtnDailyBackup.Size = new System.Drawing.Size(346, 27);
            this.rbtnDailyBackup.TabIndex = 0;
            this.rbtnDailyBackup.TabStop = true;
            this.rbtnDailyBackup.Text = "Update my backup once a day starting at:";
            this.rbtnDailyBackup.UseVisualStyleBackColor = true;
            this.rbtnDailyBackup.CheckedChanged += new System.EventHandler(this.RbtnDailyBackupCheckedChanged);
            this.rbtnDailyBackup.Click += new System.EventHandler(this.BackupScheduleMode_Click);
            // 
            // rbtnBetween
            // 
            this.rbtnBetween.AutoSize = true;
            this.rbtnBetween.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnBetween.Location = new System.Drawing.Point(10, 122);
            this.rbtnBetween.Name = "rbtnBetween";
            this.rbtnBetween.Size = new System.Drawing.Size(318, 27);
            this.rbtnBetween.TabIndex = 0;
            this.rbtnBetween.TabStop = true;
            this.rbtnBetween.Text = "Do not back up between the hours of:";
            this.rbtnBetween.UseVisualStyleBackColor = true;
            this.rbtnBetween.CheckedChanged += new System.EventHandler(this.RbtnBetweenCheckedChanged);
            this.rbtnBetween.Click += new System.EventHandler(this.BackupScheduleMode_Click);
            // 
            // rbtnCustom
            // 
            this.rbtnCustom.AutoSize = true;
            this.rbtnCustom.Font = new System.Drawing.Font("Calibri", 14F);
            this.rbtnCustom.Location = new System.Drawing.Point(10, 198);
            this.rbtnCustom.Name = "rbtnCustom";
            this.rbtnCustom.Size = new System.Drawing.Size(284, 27);
            this.rbtnCustom.TabIndex = 0;
            this.rbtnCustom.TabStop = true;
            this.rbtnCustom.Text = "Use advanced scheduling options";
            this.rbtnCustom.UseVisualStyleBackColor = true;
            this.rbtnCustom.CheckedChanged += new System.EventHandler(this.RbtnCustomCheckedChanged);
            this.rbtnCustom.Click += new System.EventHandler(this.BackupScheduleMode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(296, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "(Recommended)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DocSync.Client.Controls.Properties.Resources.Help;
            this.pictureBox1.Location = new System.Drawing.Point(428, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(24, 24);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // lbtnViewSetShedulingOptions
            // 
            this.lbtnViewSetShedulingOptions.AutoSize = true;
            this.lbtnViewSetShedulingOptions.Enabled = false;
            this.lbtnViewSetShedulingOptions.Font = new System.Drawing.Font("Calibri", 12F);
            this.lbtnViewSetShedulingOptions.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lbtnViewSetShedulingOptions.Location = new System.Drawing.Point(294, 203);
            this.lbtnViewSetShedulingOptions.Name = "lbtnViewSetShedulingOptions";
            this.lbtnViewSetShedulingOptions.Size = new System.Drawing.Size(85, 19);
            this.lbtnViewSetShedulingOptions.TabIndex = 7;
            this.lbtnViewSetShedulingOptions.TabStop = true;
            this.lbtnViewSetShedulingOptions.Text = "(View / Set)";
            this.lbtnViewSetShedulingOptions.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkViewSetShedulingOptions_LinkClicked);
            // 
            // pnlNotBetween
            // 
            this.pnlNotBetween.Controls.Add(this.label2);
            this.pnlNotBetween.Controls.Add(this.ctrlTimeSelectorBetweenEnd);
            this.pnlNotBetween.Controls.Add(this.ctrlTimeSelectorBetweenStart);
            this.pnlNotBetween.Enabled = false;
            this.pnlNotBetween.Location = new System.Drawing.Point(10, 158);
            this.pnlNotBetween.Name = "pnlNotBetween";
            this.pnlNotBetween.Size = new System.Drawing.Size(473, 35);
            this.pnlNotBetween.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F);
            this.label2.Location = new System.Drawing.Point(225, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "&&";
            // 
            // ctrlTimeSelectorBetweenEnd
            // 
            this.ctrlTimeSelectorBetweenEnd.Location = new System.Drawing.Point(250, 1);
            this.ctrlTimeSelectorBetweenEnd.Name = "ctrlTimeSelectorBetweenEnd";
            this.ctrlTimeSelectorBetweenEnd.Size = new System.Drawing.Size(220, 33);
            this.ctrlTimeSelectorBetweenEnd.TabIndex = 0;
            this.ctrlTimeSelectorBetweenEnd.Value = new System.DateTime(2010, 5, 26, 12, 0, 0, 0);
            this.ctrlTimeSelectorBetweenEnd.TimeSelectChanged += new System.EventHandler<TimeEventArgs>(this.TimeSelectChanged);
            // 
            // ctrlTimeSelectorBetweenStart
            // 
            this.ctrlTimeSelectorBetweenStart.Location = new System.Drawing.Point(3, 1);
            this.ctrlTimeSelectorBetweenStart.Name = "ctrlTimeSelectorBetweenStart";
            this.ctrlTimeSelectorBetweenStart.Size = new System.Drawing.Size(220, 33);
            this.ctrlTimeSelectorBetweenStart.TabIndex = 0;
            this.ctrlTimeSelectorBetweenStart.Value = new System.DateTime(2010, 5, 26, 12, 0, 0, 0);
            this.ctrlTimeSelectorBetweenStart.TimeSelectChanged += new System.EventHandler<TimeEventArgs>(this.TimeSelectChanged);
            // 
            // pnlDailySettings
            // 
            this.pnlDailySettings.Controls.Add(this.ctrlTimeSelectorDaily);
            this.pnlDailySettings.Enabled = false;
            this.pnlDailySettings.Location = new System.Drawing.Point(10, 80);
            this.pnlDailySettings.Name = "pnlDailySettings";
            this.pnlDailySettings.Size = new System.Drawing.Size(473, 35);
            this.pnlDailySettings.TabIndex = 8;
            // 
            // ctrlTimeSelectorDaily
            // 
            this.ctrlTimeSelectorDaily.Location = new System.Drawing.Point(3, 1);
            this.ctrlTimeSelectorDaily.Name = "ctrlTimeSelectorDaily";
            this.ctrlTimeSelectorDaily.Size = new System.Drawing.Size(220, 33);
            this.ctrlTimeSelectorDaily.TabIndex = 0;
            this.ctrlTimeSelectorDaily.Value = new System.DateTime(2010, 5, 26, 12, 0, 0, 0);
            this.ctrlTimeSelectorDaily.TimeSelectChanged += new System.EventHandler<TimeEventArgs>(this.TimeSelectChanged);
            // 
            // btnApplyChanges
            // 
            this.btnApplyChanges.AllowTransparency = false;
            this.btnApplyChanges.AnimatePress = true;
            this.btnApplyChanges.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnApplyChanges.HoverImage = null;
            this.btnApplyChanges.Location = new System.Drawing.Point(10, 238);
            this.btnApplyChanges.Name = "btnApplyChanges";
            this.btnApplyChanges.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonApply;
            this.btnApplyChanges.PressedImage = null;
            this.btnApplyChanges.Size = new System.Drawing.Size(164, 33);
            this.btnApplyChanges.TabIndex = 32;
            this.btnApplyChanges.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnApplyChanges.Click += new System.EventHandler(this.BtnApplyChanges_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(316, 238);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 33;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // BackupScheduleView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnApplyChanges);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.pnlNotBetween);
            this.Controls.Add(this.pnlDailySettings);
            this.Controls.Add(this.lbtnViewSetShedulingOptions);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtnCustom);
            this.Controls.Add(this.rbtnBetween);
            this.Controls.Add(this.rbtnDailyBackup);
            this.Controls.Add(this.rbtnAutmaticBackupMode);
            this.Name = "BackupScheduleView";
            this.Size = new System.Drawing.Size(486, 286);
            this.VisibleChanged += new System.EventHandler(this.BackupScheduleView_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlNotBetween.ResumeLayout(false);
            this.pnlNotBetween.PerformLayout();
            this.pnlDailySettings.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnAutmaticBackupMode;
        private System.Windows.Forms.RadioButton rbtnDailyBackup;
        private System.Windows.Forms.RadioButton rbtnBetween;
        private System.Windows.Forms.RadioButton rbtnCustom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel lbtnViewSetShedulingOptions;
        private System.Windows.Forms.Panel pnlNotBetween;
        private System.Windows.Forms.Panel pnlDailySettings;
        private TimeSelector ctrlTimeSelectorBetweenEnd;
        private TimeSelector ctrlTimeSelectorBetweenStart;
        private TimeSelector ctrlTimeSelectorDaily;
        private System.Windows.Forms.Label label2;
        private ImageButton btnApplyChanges;
        private ImageButton btnCancel;
    }
}
