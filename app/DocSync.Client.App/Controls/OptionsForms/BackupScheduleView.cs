using System;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Entities.Settings;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls.OptionsForms
{
    /// <summary>
    /// The backup schedule view.
    /// </summary>
    public partial class BackupScheduleView : ViewBase
    {
        /// <summary>
        /// The schedule info object.
        /// </summary>
        private ScheduleSettingsInfo scheduleInfo;

        /// <summary>
        /// Gets or sets the backup mode.
        /// </summary>
        /// <value>The backup mode.</value>
        public ScheduleSettingsInfo ScheduleInfo
        {
            get { return this.CalculateScheduleInfo(); }
            set { this.InitializeScheduleInfo(value); }
        }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or Sets Is dirty property, true if options were changed
        /// </summary>
        private bool IsDirty { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackupScheduleView"/> class.
        /// </summary>
        public BackupScheduleView()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.LoadSettings();
        }

        /// <summary>
        /// Handles the LinkClicked event of the LinkViewSetShedulingOptions control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LinkViewSetShedulingOptions_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.AdvancedScheduling });
        }

        /// <summary>
        /// Handles the Click event of the BackupScheduleMode control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void BackupScheduleMode_Click(object sender, EventArgs e)
        {
            //this.SetButtonsVisible(true);

            if (this.rbtnAutmaticBackupMode.Checked)
            {
                this.ScheduleInfo.ScheduleMode = ScheduleMode.Automatic;
            }

            if (this.rbtnDailyBackup.Checked)
            {
                this.ScheduleInfo.ScheduleMode = ScheduleMode.Daily;
            }

            if (this.rbtnBetween.Checked)
            {
                this.ScheduleInfo.ScheduleMode = ScheduleMode.InSpecifiedPeriod;
            }

            if (this.rbtnCustom.Checked)
            {
                this.ScheduleInfo.ScheduleMode = ScheduleMode.Advanced;
            }

            this.SetBackupMode(this.ScheduleInfo.ScheduleMode);
        }

        /// <summary>
        /// Sets the backup mode.
        /// </summary>
        /// <param name="mode">The backup mode.</param>
        private void SetBackupMode(ScheduleMode mode)
        {
            this.rbtnAutmaticBackupMode.Checked = (mode == ScheduleMode.Automatic);
            this.rbtnDailyBackup.Checked = (mode == ScheduleMode.Daily);
            this.rbtnBetween.Checked = (mode == ScheduleMode.InSpecifiedPeriod);
            this.rbtnCustom.Checked = (mode == ScheduleMode.Advanced);

            this.pnlDailySettings.Enabled = (this.scheduleInfo.ScheduleMode == ScheduleMode.Daily);
            this.pnlNotBetween.Enabled = (this.scheduleInfo.ScheduleMode == ScheduleMode.InSpecifiedPeriod);
            this.lbtnViewSetShedulingOptions.Enabled = (this.scheduleInfo.ScheduleMode == ScheduleMode.Advanced);
        }

        /// <summary>
        /// Sets the backup data.
        /// </summary>
        /// <param name="mode">The backup mode.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        private void SetBackupData(ScheduleMode mode, DateTime startDate, DateTime endDate)
        {
            switch (mode)
            {
                case ScheduleMode.Daily:
                    this.ctrlTimeSelectorDaily.Value = startDate;
                    break;
                case ScheduleMode.InSpecifiedPeriod:
                    this.ctrlTimeSelectorBetweenStart.Value = startDate;
                    this.ctrlTimeSelectorBetweenEnd.Value = endDate;
                    break;
            }
        }

        /// <summary>
        /// Calculates the schedule info.
        /// </summary>
        /// <returns>The schedule info object.</returns>
        private ScheduleSettingsInfo CalculateScheduleInfo()
        {
            switch (this.scheduleInfo.ScheduleMode)
            {
                case ScheduleMode.Daily:
                    this.scheduleInfo.StartDate = this.ctrlTimeSelectorDaily.Value;
                    break;
                case ScheduleMode.InSpecifiedPeriod:
                    this.scheduleInfo.StartDate = this.ctrlTimeSelectorBetweenStart.Value;
                    this.scheduleInfo.EndDate = this.ctrlTimeSelectorBetweenEnd.Value;
                    break;
            }

            return this.scheduleInfo;
        }

        /// <summary>
        /// Initializes the schedule info.
        /// </summary>
        /// <param name="info">The schedule info.</param>
        private void InitializeScheduleInfo(ScheduleSettingsInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            this.scheduleInfo = info;

            this.SetBackupMode(this.scheduleInfo.ScheduleMode);
            this.SetBackupData(this.scheduleInfo.ScheduleMode, this.scheduleInfo.StartDate, this.scheduleInfo.EndDate);
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            //EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.Options });
            this.LoadSettings();
        }

        /// <summary>
        /// Apply button click handler
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void BtnApplyChanges_Click(object sender, EventArgs e)
        {
            if (this.scheduleInfo.ScheduleMode == ScheduleMode.InSpecifiedPeriod &&
                this.ctrlTimeSelectorBetweenStart.Value >= this.ctrlTimeSelectorBetweenEnd.Value)
            {
                MessageBox.Show("Please specify correct time");
            }
            else
            {
                this.SaveSettings();
            }
        }

        /// <summary>
        /// Control visible changed handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void BackupScheduleView_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
//                this.LoadSettings();
            }
        }

        /// <summary>
        /// Load settings
        /// </summary>
        private void LoadSettings()
        {
            this.ScheduleInfo = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().ScheduleSettings;
            
            this.IsDirty = false;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Save settings
        /// </summary>
        private void SaveSettings()
        {
            SettingsInfo settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
            settings.ScheduleSettings = this.ScheduleInfo;
            RemotingManager.Instance.ProxyManager.SettingsManager.SaveSettings(settings);
            RemotingManager.Instance.ProxyManager.ScheduleManager.IsSchedulesChanged = true;

            this.IsDirty = false;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void RbtnAutmaticBackupModeCheckedChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void RbtnDailyBackupCheckedChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void RbtnBetweenCheckedChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void RbtnCustomCheckedChanged(object sender, EventArgs e)
        {
            this.IsDirty = true;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Event handler
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void TimeSelectChanged(object sender, TimeEventArgs e)
        {
            this.IsDirty = true;
            this.SetButtonsVisible();
        }

        /// <summary>
        /// Set the apply and cancel buttons visible, depends on IsDirty property
        /// </summary>
        private void SetButtonsVisible()
        {
            this.btnApplyChanges.Visible = this.IsDirty;
            this.btnCancel.Visible = this.IsDirty;
        }
    }
}