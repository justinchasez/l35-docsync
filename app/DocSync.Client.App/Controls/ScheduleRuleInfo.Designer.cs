using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls
{
    partial class ScheduleRuleInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblScheduleOptionsName = new System.Windows.Forms.Label();
            this.btnDeleteRow = new ImageButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.RowBg;
            this.panel1.Controls.Add(this.lblScheduleOptionsName);
            this.panel1.Controls.Add(this.btnDeleteRow);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(446, 31);
            this.panel1.TabIndex = 0;
            // 
            // lblScheduleOptionsName
            // 
            this.lblScheduleOptionsName.AutoSize = true;
            this.lblScheduleOptionsName.BackColor = System.Drawing.Color.Transparent;
            this.lblScheduleOptionsName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblScheduleOptionsName.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblScheduleOptionsName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblScheduleOptionsName.Location = new System.Drawing.Point(8, 7);
            this.lblScheduleOptionsName.Name = "lblScheduleOptionsName";
            this.lblScheduleOptionsName.Size = new System.Drawing.Size(148, 17);
            this.lblScheduleOptionsName.TabIndex = 1;
            this.lblScheduleOptionsName.Text = "lblScheduleOptionsName";
            this.lblScheduleOptionsName.Click += new System.EventHandler(this.ScheduleOptionsName_Click);
            // 
            // btnDeleteRow
            // 
            this.btnDeleteRow.AllowTransparency = false;
            this.btnDeleteRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteRow.AnimatePress = true;
            this.btnDeleteRow.BackColor = System.Drawing.Color.Transparent;
            this.btnDeleteRow.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnDeleteRow.HoverImage = null;
            this.btnDeleteRow.Location = new System.Drawing.Point(419, 7);
            this.btnDeleteRow.Name = "btnDeleteRow";
            this.btnDeleteRow.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonDeleteRow;
            this.btnDeleteRow.PressedImage = null;
            this.btnDeleteRow.Size = new System.Drawing.Size(16, 16);
            this.btnDeleteRow.TabIndex = 0;
            this.btnDeleteRow.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnDeleteRow.Click += new System.EventHandler(this.ButtonDeleteRow_Click);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.RowLeft;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(1, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(3, 31);
            this.panel2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.RowRight;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(444, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(3, 31);
            this.panel3.TabIndex = 0;
            // 
            // ScheduleRuleInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "ScheduleRuleInfo";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(448, 33);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private ImageButton btnDeleteRow;
        private System.Windows.Forms.Label lblScheduleOptionsName;
    }
}
