using System;
using System.Drawing;
using System.Windows.Forms;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// Three state ttab control.
    /// </summary>
    public class SwitchControl : PictureBox
    {
        #region Private fields

        /// <summary>
        /// Indicates when current tab selected.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Image for normal state.
        /// </summary>
        private Image normalImage;

        /// <summary>
        /// Image for hover state.
        /// </summary>
        private Image hoverImage;

        /// <summary>
        /// Image for selected state.
        /// </summary>
        private Image selectedImage;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SwitchControl"/> class.
        /// </summary>
        public SwitchControl()
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;

                this.EnsureImage();
                this.Invalidate();
            }
        }

        /// <summary>
        /// Gets or sets the normal image.
        /// </summary>
        /// <value>The normal image.</value>
        public Image NormalImage
        {
            get
            {
                return this.normalImage;
            }

            set
            {
                this.normalImage = value;

                this.EnsureImage();
                this.Invalidate();
            }
        }

        /// <summary>
        /// Gets or sets the hover image.
        /// </summary>
        /// <value>The hover image.</value>
        public Image HoverImage
        {
            get
            {
                return this.hoverImage;
            }

            set
            {
                this.hoverImage = value;

                this.EnsureImage();
                this.Invalidate();
            }
        }

        /// <summary>
        /// Gets or sets the selected image.
        /// </summary>
        /// <value>The selected image.</value>
        public Image SelectedImage
        {
            get
            {
                return this.selectedImage;
            }

            set
            {
                this.selectedImage = value;

                this.EnsureImage();
                this.Invalidate();
            }
        }

        #endregion

        #region Overriden members

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseHover"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnMouseHover(EventArgs e)
        {
            if (this.HoverImage != null)
            {
                this.Image = this.HoverImage;
            }

            base.OnMouseHover(e);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Control.MouseLeave"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            this.EnsureImage();

            base.OnMouseLeave(e);
        }

        /// <summary>
        /// Ensures the image.
        /// </summary>
        private void EnsureImage()
        {
            if (this.IsSelected && this.SelectedImage != null)
            {
                this.Image = this.SelectedImage;
            }
            else
            {
                this.Image = this.NormalImage;
            }
        }

        #endregion
    }
}
