using System.Drawing;
using System.Windows.Forms;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// StatusProgress control.
    /// </summary>
    public partial class StatusProgress : UserControl
    {
        #region Private fields

        /// <summary>
        /// The current progress value.
        /// </summary>
        private int value;

        /// <summary>
        /// The maximum progress value.
        /// </summary>
        private int maximum;

        /// <summary>
        /// The minimum progress value.
        /// </summary>
        private int minimum;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the maximum.
        /// </summary>
        /// <value>The maximum.</value>
        public int Maximum
        {
            get { return this.maximum; }
            set { this.maximum = value; }
        }

        /// <summary>
        /// Gets or sets the minimum.
        /// </summary>
        /// <value>The minimum.</value>
        public int Minimum
        {
            get { return this.minimum; }
            set { this.minimum = value; }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public int Value
        {
            get
            {
                return this.value;
            }

            set
            {
                this.value = value;
                this.EnsureProgress();
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusProgress"/> class.
        /// </summary>
        public StatusProgress()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Ensures the progress.
        /// </summary>
        private void EnsureProgress()
        {
            int tmpValue = this.Value;

            if (tmpValue > 0)
            {
                if (tmpValue > this.Maximum)
                {
                    tmpValue = this.Maximum;
                }

                this.pbStart.Visible = true;
                this.pbProgressBar.Visible = true;

                int offcet = -349 + (int)((tmpValue / (this.Maximum / 100f)) / 100f * 349);

                if (offcet < -340)
                {
                    offcet = -340;
                }

                this.pbProgressBar.Location = new Point(offcet, this.pbProgressBar.Location.Y);
            }
            else
            {
                this.pbStart.Visible = false;
                this.pbProgressBar.Visible = false;
            }
        }
    }
}
