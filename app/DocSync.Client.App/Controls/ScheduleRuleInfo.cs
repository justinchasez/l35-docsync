using System;
using System.Windows.Forms;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Core.Entities.Schedule.Interface;
using DocSync.Client.Core.Enums;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// The schedule row control.
    /// </summary>
    public partial class ScheduleRuleInfo : UserControl
    {
        /// <summary>
        /// The schedule option.
        /// </summary>
        private readonly ISchedule schedule;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleRuleInfo"/> class.
        /// </summary>
        public ScheduleRuleInfo()
        {
            this.InitializeComponent();

            this.Load += this.ScheduleRuleInfo_Load;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleRuleInfo"/> class.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        public ScheduleRuleInfo(ISchedule schedule)
            : this()
        {
            this.schedule = schedule;
        }

        /// <summary>
        /// Handles the Load event of the ScheduleRuleInfo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ScheduleRuleInfo_Load(object sender, EventArgs e)
        {
            if (this.schedule != null)
            {
                this.lblScheduleOptionsName.Text = this.schedule.ToString();
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonDeleteRow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonDeleteRow_Click(object sender, EventArgs e)
        {
            RemotingManager.Instance.ProxyManager.ScheduleManager.RemoveSchedule(this.schedule);

            EventPublisher.ScheduleManager.OnScheduleRemoved(new ScheduleEventArgs { Schedule = this.schedule });
        }

        /// <summary>
        /// Handles the Click event of the ScheduleOptionsName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ScheduleOptionsName_Click(object sender, EventArgs e)
        {
            EventPublisher.UIEventManagerInstance.OnSwitchTab(new SwitchTabEventArgs { SelectedTab = ViewTab.RulesEditor });
            EventPublisher.ScheduleManager.OnEditSchedule(new ScheduleEventArgs { Schedule = this.schedule });
        }
    }
}
