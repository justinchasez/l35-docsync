using System.Windows.Forms;

namespace DocSync.Client.App.Controls.Base
{
    /// <summary>
    /// The base control for wizard views.
    /// </summary>
    /// <typeparam name="T">The type of the container.</typeparam>
    public class WizardViewBase : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WizardViewBase"/> class.
        /// </summary>
        public WizardViewBase()
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);
        }

        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <typeparam name="T">The type of the container.</typeparam>
        /// <returns>The wizard container.</returns>
        protected T GetContainer<T>() where T : Form
        {
            return (T)this.ParentForm;
        }
    }
}