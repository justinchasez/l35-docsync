using System.ComponentModel.Design;
using System.Windows.Forms;

namespace DocSync.Client.App.Controls.Base
{
    /// <summary>
    /// The base class for all views.
    /// </summary>
    public class ViewBase : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewBase"/> class.
        /// </summary>
        public ViewBase()
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
        }

        /// <summary>
        /// Gets a value indicating whether this instance is design mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is design mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsDesignMode
        {
            get
            {
                return this.GetService(typeof(IDesignerHost)) != null;
            }
        }
    }
}