using System.ComponentModel.Design;
using System.Reflection;
using System.Windows.Forms;

namespace DocSync.Client.App.Controls.Base
{
    /// <summary>
    /// The base form.
    /// </summary>
    public class FormBase : Form
    {
        /// <summary>
        /// Controls action handler.
        /// </summary>
        /// <param name="control">The control instance.</param>
        public delegate void ControlActionDelegate(Control control);

        /// <summary>
        /// Initializes a new instance of the <see cref="FormBase"/> class.
        /// </summary>
        public FormBase()
        {
            this.SetStyle();
        }

        /// <summary>
        /// Gets a value indicating whether this instance is design mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is design mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsDesignMode
        {
            get
            {
                return this.GetService(typeof(IDesignerHost)) != null;
            }
        }

        /// <summary>
        /// Sets the double buffered.
        /// </summary>
        /// <param name="control">The control.</param>
        protected static void SetDoubleBuffered(Control control)
        {
            //Taxes: Remote Desktop Connection and painting
            //http://blogs.msdn.com/oldnewthing/archive/2006/01/03/508694.aspx
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
            {
                return;
            }

            MethodInfo setStyle = typeof(Control).GetMethod("SetStyle", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod);
            setStyle.Invoke(control, new object[] { ControlStyles.OptimizedDoubleBuffer, true });
            setStyle.Invoke(control, new object[] { ControlStyles.AllPaintingInWmPaint, true });
        }

        /// <summary>
        /// Sets predefined styles to form and its controls, must be called 
        /// after <c>InitializeComponent()</c> method.
        /// </summary>
        protected void SetStyle()
        {
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.CacheText, true);

            this.ForEachControl(SetDoubleBuffered);
        }

        /// <summary>
        /// Executes specified action for each control on the form.
        /// </summary>
        /// <param name="controlAction">ControlAction delegate.</param>
        protected void ForEachControl(ControlActionDelegate controlAction)
        {
            this.ForEachControlInternal(this, controlAction);
        }

        /// <summary>
        /// Fors the each control internal.
        /// </summary>
        /// <param name="parentControl">The parent control.</param>
        /// <param name="controlAction">The control action.</param>
        private void ForEachControlInternal(Control parentControl, ControlActionDelegate controlAction)
        {
            foreach (Control control in parentControl.Controls)
            {
                controlAction.Invoke(control);
                this.ForEachControlInternal(control, controlAction);
            }
        }
    }
}