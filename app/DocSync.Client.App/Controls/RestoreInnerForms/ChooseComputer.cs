using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The choose computer view.
    /// </summary>
    public partial class ChooseComputer : WizardViewBase
    {
        /// <summary>
        /// The selected comp name.
        /// </summary>
        private string selectedCompName;

        /// <summary>
        /// The computers list.
        /// </summary>
        private List<ComputerInfo> computerInfos;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChooseComputer"/> class.
        /// </summary>
        public ChooseComputer()
        {
            this.InitializeComponent();

            EventPublisher.RestoreWizardEventManager.SwitchRestoreWizardView += this.RestoreWizardEventManager_SwitchRestoreWizardView;
        }

        /// <summary>
        /// Restores the wizard event manager_ switch restore wizard view.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_SwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args)
        {
            if (args.SelectedTab == RestoreWizardTab.ComputerSelect)
            {
                this.pnlContainer.Controls.Clear();

                var settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
                int currentComputerId = settings.CurrentComputer.Id;

                this.computerInfos = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();
                this.computerInfos.Reverse();

                foreach (ComputerInfo current in this.computerInfos)
                {
                    if (current.Id == currentComputerId)
                    {
                        continue;
                    }

                    RadioButton rbtnCurrentComp = new RadioButton { Text = current.Name, Dock = DockStyle.Top };

                    rbtnCurrentComp.CheckedChanged += this.RadioCurrentComp_CheckedChanged;

                    if ((this.GetContainer<RestoreWizard>().SelectedComputer != null) && 
                        (current.Id == this.GetContainer<RestoreWizard>().SelectedComputer.Id))
                    {
                        rbtnCurrentComp.Checked = true;
                    }

                    this.pnlContainer.Controls.Add(rbtnCurrentComp);
                }
            }
        }

        /// <summary>
        /// Handles the CheckedChanged event of the RadioCurrentComp control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event da;ta.</param>
        private void RadioCurrentComp_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton ctrl = sender as RadioButton;
            if (ctrl != null && ctrl.Checked)
            {
                this.selectedCompName = ctrl.Text;
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBack_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Start });
        }

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (this.selectedCompName == null)
            {
                return;
            }

            ComputerInfo selectedComp = this.computerInfos.Find(info => info.Name == this.selectedCompName);

            this.GetContainer<RestoreWizard>().SelectedComputer = selectedComp;

            if (selectedComp.ManualEncriptionState)
            {
                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.EncryptionKey });
            }
            else
            {
                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.RestoreMode });
            }
        }

        /// <summary>
        /// Cancel restore event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }
    }
}
