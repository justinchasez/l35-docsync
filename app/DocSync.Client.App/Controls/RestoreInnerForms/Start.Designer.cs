using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    partial class Start
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnCurrentPC = new System.Windows.Forms.RadioButton();
            this.rbntOtherPC = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new ImageButton();
            this.btnNext = new ImageButton();
            this.SuspendLayout();
            // 
            // rbtnCurrentPC
            // 
            this.rbtnCurrentPC.AutoSize = true;
            this.rbtnCurrentPC.Checked = true;
            this.rbtnCurrentPC.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnCurrentPC.Location = new System.Drawing.Point(46, 75);
            this.rbtnCurrentPC.Name = "rbtnCurrentPC";
            this.rbtnCurrentPC.Size = new System.Drawing.Size(200, 21);
            this.rbtnCurrentPC.TabIndex = 0;
            this.rbtnCurrentPC.TabStop = true;
            this.rbtnCurrentPC.Text = "I want to restore files of this PC";
            this.rbtnCurrentPC.UseVisualStyleBackColor = true;
            // 
            // rbntOtherPC
            // 
            this.rbntOtherPC.AutoSize = true;
            this.rbntOtherPC.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbntOtherPC.Location = new System.Drawing.Point(46, 114);
            this.rbntOtherPC.Name = "rbntOtherPC";
            this.rbntOtherPC.Size = new System.Drawing.Size(302, 21);
            this.rbntOtherPC.TabIndex = 0;
            this.rbntOtherPC.Text = "I want to restore files backed-up from another PC";
            this.rbntOtherPC.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select computer";
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(3, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // btnNext
            // 
            this.btnNext.AllowTransparency = false;
            this.btnNext.AnimatePress = true;
            this.btnNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNext.HoverImage = null;
            this.btnNext.Location = new System.Drawing.Point(447, 244);
            this.btnNext.Name = "btnNext";
            this.btnNext.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonNext;
            this.btnNext.PressedImage = null;
            this.btnNext.Size = new System.Drawing.Size(164, 33);
            this.btnNext.TabIndex = 2;
            this.btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbntOtherPC);
            this.Controls.Add(this.rbtnCurrentPC);
            this.Name = "Start";
            this.Size = new System.Drawing.Size(614, 280);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnCurrentPC;
        private System.Windows.Forms.RadioButton rbntOtherPC;
        private System.Windows.Forms.Label label1;
        private ImageButton btnCancel;
        private ImageButton btnNext;
    }
}
