using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Computers;
using DocSync.Client.Core.Entities.Encryption;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The summary tab.
    /// </summary>
    public partial class Summary : WizardViewBase
    {
        /// <summary>
        /// Parent form. Added becouse this.ParentForm sometimes has a null value
        /// </summary>
        public RestoreWizard RestoreWizardForm { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Summary"/> class.
        /// </summary>
        public Summary()
        {
            this.InitializeComponent();

            EventPublisher.RestoreWizardEventManager.SwitchRestoreWizardView += this.RestoreWizardEventManager_SwitchRestoreWizardView;
        }

        /// <summary>
        /// Restores the wizard event manager_ switch restore wizard view.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_SwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args)
        {
            if (args.SelectedTab == RestoreWizardTab.Summary)
            {
                RestoreWizard container = this.RestoreWizardForm; //this.GetContainer<RestoreWizard>();

                this.pnlRestoreComputer.Visible = container.SelectedComputer != null;
                this.pnlUserMappings.Visible = !container.RestoreToDefaultLocation;
                this.pnlUserMappings.Controls.Clear();

                if (container.SelectedComputer != null)
                {
                    if (container.SelectedComputer.Id == ClientSettingsManager.Settings.CurrentComputer.Id)
                    {
                        this.lblRestoredFrom.Visible = false;
                        this.lblComputerName.Text = String.Empty;
                    }
                    else
                    {
                        this.lblRestoredFrom.Visible = true;
                        this.lblComputerName.Text = container.SelectedComputer.Name;
                    }
                }

                if (!container.RestoreToDefaultLocation)
                {
                    foreach (KeyValuePair<string, string> currentMapping in container.RestoreUserMappings)
                    {
                        if (!String.IsNullOrEmpty(currentMapping.Value))
                        {
                            Label userLabel = new Label();
                            userLabel.Text = String.Format(" - {0} will be restored to {1}", 
                                                           currentMapping.Key,
                                                           currentMapping.Value);
                            userLabel.Padding = new Padding(3);
                            userLabel.Dock = DockStyle.Top;
                            this.pnlUserMappings.Controls.Add(userLabel);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }

        /// <summary>
        /// Handles the Click event of the ButtonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBack_Click(object sender, EventArgs e)
        {
            if (this.GetContainer<RestoreWizard>().RestoreToDefaultLocation)
            {
                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.RestoreMode });
            }
            else
            {
                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.AccountSelect });
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonFinish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonFinish_Click(object sender, EventArgs e)
        {
            //var settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
            var container = this.GetContainer<RestoreWizard>();

            int computerId = container.SelectedComputer.Id;
            EncryptInfo encrypt;

            if (computerId == ClientSettingsManager.Settings.CurrentComputer.Id)
            {
                encrypt = ClientSettingsManager.Settings.CurrentComputerEncryptInfo ??
                          EncryptInfo.LoadDefault();
            }
            else
            {
                encrypt = container.SelectedEncryptionInfo ??
                          EncryptInfo.LoadDefault();
            }

            RemotingManager.Instance.ProxyManager.BackupManager.RestoreComputer(computerId, encrypt);

            List<SyncronizeUsersEntry> backupUsersList =
    RemotingManager.Instance.ProxyManager.ComputerManager.GetBackupUsersList(container.SelectedComputer);

            if (!container.RestoreToDefaultLocation)
            {
                List<SyncronizeUsersEntry> localUsers =
                    RemotingManager.Instance.ProxyManager.ComputerManager.GetLocalUsersList();

                foreach (SyncronizeUsersEntry currentUser in backupUsersList)
                {
                    string targetUser = container.RestoreUserMappings[currentUser.UserName];
                    if (!String.IsNullOrEmpty(targetUser))
                    {
                        foreach (SyncronizeUsersEntry localUser in localUsers)
                        {
                            if (localUser.UserName.Equals(targetUser))
                            {
                                RemotingManager.Instance.ProxyManager.BackupManager.RestoreDocuments(currentUser,
                                                                                                     localUser,
                                                                                                     computerId,
                                                                                                     encrypt);
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (SyncronizeUsersEntry user in backupUsersList)
                {
                    RemotingManager.Instance.ProxyManager.BackupManager.RestoreDocuments(user, user, computerId, encrypt);
                }
            }

            EventPublisher.RestoreWizardEventManager.OnRestoreWizardFinished();

            this.ParentForm.Close();
        }
    }
}
