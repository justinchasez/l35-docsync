using System;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Encryption;
using DocSync.Client.Core.Managers.Text;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The encryption key view.
    /// </summary>
    public partial class EncryptionKey : WizardViewBase
    {
        /// <summary>
        /// Gets or sets the current encrypt info.
        /// </summary>
        /// <value>The current encrypt info.</value>
        private EncryptInfo CurrentEncryptInfo { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptionKey"/> class.
        /// </summary>
        public EncryptionKey()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (this.CurrentEncryptInfo != null)
            {
                if (this.ValidatePassword(this.txtPassword.Text, this.CurrentEncryptInfo.PasswordHash))
                {
                    this.GetContainer<RestoreWizard>().SelectedEncryptionInfo = this.CurrentEncryptInfo;

                    EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                        new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.RestoreMode });
                }
                else
                {
                    MessageBox.Show("Invalid password.");
                }
            }
            else
            {
                MessageBox.Show("Correct file is not selected.");
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBack_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Start });
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        /// <param name="pwd">The password.</param>
        /// <param name="hash">The password hash.</param>
        /// <returns>
        /// True if password and confiramtion are valid, otherwise false.
        /// </returns>
        private bool ValidatePassword(string pwd, string hash)
        {
            return hash.Equals(TextManager.ComputeHash(pwd));
        }

        /// <summary>
        /// Handles the Click event of the btnBrowse control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBrowse_Click(object sender, EventArgs e)
        {
            if (this.ofdEncryptionKey.ShowDialog() == DialogResult.OK)
            {
                this.txtEncryptionKeyPath.Text = this.ofdEncryptionKey.FileName;
                try
                {
                    this.CurrentEncryptInfo = EncryptInfo.LoadFromFile(this.txtEncryptionKeyPath.Text);
                }
                catch
                {
                    this.CurrentEncryptInfo = null;
                }

                this.lblPasswordHint.Text = (this.CurrentEncryptInfo != null)
                                                ? this.CurrentEncryptInfo.Hint
                                                : String.Empty;
            }
        }
    }
}
