using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    partial class EncryptionKey
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBack = new ImageButton();
            this.btnNext = new ImageButton();
            this.btnCancel = new ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonBrowse = new ImageButton();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtEncryptionKeyPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ofdEncryptionKey = new System.Windows.Forms.OpenFileDialog();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPasswordHint = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(276, 244);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonBack;
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(164, 33);
            this.btnBack.TabIndex = 11;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.AllowTransparency = false;
            this.btnNext.AnimatePress = true;
            this.btnNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNext.HoverImage = null;
            this.btnNext.Location = new System.Drawing.Point(446, 244);
            this.btnNext.Name = "btnNext";
            this.btnNext.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonNext;
            this.btnNext.PressedImage = null;
            this.btnNext.Size = new System.Drawing.Size(164, 33);
            this.btnNext.TabIndex = 10;
            this.btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(4, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Encryption key";
            // 
            // ButtonBrowse
            // 
            this.ButtonBrowse.AllowTransparency = false;
            this.ButtonBrowse.AnimatePress = true;
            this.ButtonBrowse.DialogResult = System.Windows.Forms.DialogResult.None;
            this.ButtonBrowse.HoverImage = null;
            this.ButtonBrowse.Location = new System.Drawing.Point(446, 81);
            this.ButtonBrowse.Name = "ButtonBrowse";
            this.ButtonBrowse.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonBrowse;
            this.ButtonBrowse.PressedImage = null;
            this.ButtonBrowse.Size = new System.Drawing.Size(164, 33);
            this.ButtonBrowse.TabIndex = 18;
            this.ButtonBrowse.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.ButtonBrowse.Click += new System.EventHandler(this.ButtonBrowse_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPassword.Location = new System.Drawing.Point(133, 114);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(307, 27);
            this.txtPassword.TabIndex = 16;
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // txtEncryptionKeyPath
            // 
            this.txtEncryptionKeyPath.BackColor = System.Drawing.Color.White;
            this.txtEncryptionKeyPath.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtEncryptionKeyPath.Location = new System.Drawing.Point(133, 81);
            this.txtEncryptionKeyPath.Name = "txtEncryptionKeyPath";
            this.txtEncryptionKeyPath.ReadOnly = true;
            this.txtEncryptionKeyPath.Size = new System.Drawing.Size(307, 27);
            this.txtEncryptionKeyPath.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label4.Location = new System.Drawing.Point(34, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 19);
            this.label4.TabIndex = 13;
            this.label4.Text = "Password:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label3.Location = new System.Drawing.Point(34, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "File with key:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label2.Location = new System.Drawing.Point(34, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(326, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "Please, input an encryption key for this computer";
            // 
            // ofdEncryptionKey
            // 
            this.ofdEncryptionKey.Filter = "Encryption key (*.pem)|*.pem|All files (*.*)|*.*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.label5.Location = new System.Drawing.Point(34, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 19);
            this.label5.TabIndex = 19;
            this.label5.Text = "Hint:";
            // 
            // lblPasswordHint
            // 
            this.lblPasswordHint.AutoSize = true;
            this.lblPasswordHint.Location = new System.Drawing.Point(136, 156);
            this.lblPasswordHint.Name = "lblPasswordHint";
            this.lblPasswordHint.Size = new System.Drawing.Size(0, 13);
            this.lblPasswordHint.TabIndex = 20;
            // 
            // EncryptionKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblPasswordHint);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ButtonBrowse);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEncryptionKeyPath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Name = "EncryptionKey";
            this.Size = new System.Drawing.Size(614, 280);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton btnBack;
        private ImageButton btnNext;
        private ImageButton btnCancel;
        private System.Windows.Forms.Label label1;
        private ImageButton ButtonBrowse;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtEncryptionKeyPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog ofdEncryptionKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPasswordHint;
    }
}
