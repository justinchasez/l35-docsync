using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    partial class ChooseComputer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseComputer));
            this.btnNext = new ImageButton();
            this.btnCancel = new ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new ImageButton();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.AllowTransparency = false;
            this.btnNext.AnimatePress = true;
            this.btnNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNext.HoverImage = null;
            this.btnNext.Location = new System.Drawing.Point(445, 244);
            this.btnNext.Name = "btnNext";
            this.btnNext.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnNext.NormalImage")));
            this.btnNext.PressedImage = null;
            this.btnNext.Size = new System.Drawing.Size(164, 33);
            this.btnNext.TabIndex = 4;
            this.btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(3, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 22);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select computer";
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(275, 244);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnBack.NormalImage")));
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(164, 33);
            this.btnBack.TabIndex = 4;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // pnlContainer
            // 
            this.pnlContainer.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.pnlContainer.Location = new System.Drawing.Point(38, 45);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(563, 193);
            this.pnlContainer.TabIndex = 6;
            // 
            // ChooseComputer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlContainer);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Name = "ChooseComputer";
            this.Size = new System.Drawing.Size(614, 280);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton btnNext;
        private ImageButton btnCancel;
        private System.Windows.Forms.Label label1;
        private ImageButton btnBack;
        private System.Windows.Forms.Panel pnlContainer;
    }
}
