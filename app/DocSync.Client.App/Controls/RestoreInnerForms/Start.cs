using System;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Properties;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The initizl wizard view.
    /// </summary>
    public partial class Start : WizardViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Start"/> class.
        /// </summary>
        public Start()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonNext_Click(object sender, EventArgs e)
        {
            if (this.rbtnCurrentPC.Checked)
            {
                var settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();

                this.GetContainer<RestoreWizard>().SelectedComputer = settings.CurrentComputer;
                if (settings.CurrentComputer.ManualEncriptionState)
                {
                    this.GetContainer<RestoreWizard>().SelectedEncryptionInfo = settings.CurrentComputerEncryptInfo;
                }

                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.RestoreMode });
            }
            else
            {
                var computers = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();
                if (computers.Count > 1)
                {
                    EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                        new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.ComputerSelect });
                }
                else
                {
                    MessageBox.Show(Resources.RestoreWizardStartComputerSelectError);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }
    }
}
