using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The accoutns selection tab.
    /// </summary>
    public partial class ChooseAccounts : WizardViewBase
    {
        /// <summary>
        /// The local users list.
        /// </summary>
        private readonly List<string> localUsersList;

        /// <summary>
        /// The mappings controls.
        /// </summary>
        private readonly List<AccountMapping> mapingsControlList;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChooseAccounts"/> class.
        /// </summary>
        public ChooseAccounts()
        {
            this.InitializeComponent();

            this.localUsersList = new List<string>();
            this.mapingsControlList = new List<AccountMapping>();

            EventPublisher.RestoreWizardEventManager.SwitchRestoreWizardView += this.RestoreWizardEventManager_SwitchRestoreWizardView;
        }

        /// <summary>
        /// Restores the wizard event manager_ switch restore wizard view.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_SwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args)
        {
            if (args.SelectedTab == RestoreWizardTab.AccountSelect)
            {
                this.pnlMappings.Controls.Clear();
                this.mapingsControlList.Clear();

                RestoreWizard container = this.GetContainer<RestoreWizard>();

                List<SyncronizeUsersEntry> backupUsersList =
                    RemotingManager.Instance.ProxyManager.ComputerManager.GetBackupUsersList(container.SelectedComputer);
                backupUsersList.Reverse();

                List<SyncronizeUsersEntry> localUsers =
                    RemotingManager.Instance.ProxyManager.ComputerManager.GetLocalUsersList();

                List<string> localUsersNames = new List<string> { "Do not restore" };
                foreach (SyncronizeUsersEntry usersEntry in localUsers)
                {
                    localUsersNames.Add(usersEntry.UserName);
                }

                foreach (SyncronizeUsersEntry currentUser in backupUsersList)
                {
                    AccountMapping currentMapping = new AccountMapping
                                                        {
                                                            Dock = DockStyle.Top,
                                                            BackupUser = currentUser.UserName,
                                                            UsersList = localUsersNames
                                                        };

                    this.mapingsControlList.Add(currentMapping);
                    this.pnlMappings.Controls.Add(currentMapping);
                }
            }
        }

        ///// <summary>
        ///// Gets the local users list.
        ///// </summary>
        ///// <returns>The list of the local users in administratino group.</returns>
        //private List<string> GetLocalUsersList()
        //{
        //    if (this.localUsersList.Count == 0)
        //    {
        //        this.localUsersList.Add("Do not restore");
        //        using (DirectoryEntry groupEntry = new DirectoryEntry("WinNT://./Administrators,group"))
        //        {
        //            foreach (object member in (IEnumerable)groupEntry.Invoke("Members"))
        //            {
        //                using (DirectoryEntry memberEntry = new DirectoryEntry(member))
        //                {
        //                    this.localUsersList.Add(memberEntry.Name);
        //                }
        //            }
        //        }
        //    }

        //    return this.localUsersList;
        //}

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonNext_Click(object sender, System.EventArgs e)
        {
            RestoreWizard container = this.GetContainer<RestoreWizard>();
            container.RestoreUserMappings.Clear();

            foreach (AccountMapping currentMapping in this.mapingsControlList)
            {
      //          if (!String.IsNullOrEmpty(currentMapping.SelectedUser))
       //         {
                container.RestoreUserMappings.Add(currentMapping.BackupUser, currentMapping.SelectedUser);
      //          }
            }

            EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Summary });
        }

        /// <summary>
        /// Handles the Click event of the ButtonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBack_Click(object sender, System.EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.RestoreMode });
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, System.EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }
    }
}
