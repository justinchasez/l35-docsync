using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    partial class Summary
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Summary));
            this.btnBack = new ImageButton();
            this.btnFinish = new ImageButton();
            this.btnCancel = new ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlUserMappings = new System.Windows.Forms.Panel();
            this.pnlRestoreComputer = new System.Windows.Forms.Panel();
            this.lblComputerName = new System.Windows.Forms.Label();
            this.lblRestoredFrom = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlRestoreComputer.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(276, 236);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnBack.NormalImage")));
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(164, 33);
            this.btnBack.TabIndex = 15;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.AllowTransparency = false;
            this.btnFinish.AnimatePress = true;
            this.btnFinish.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnFinish.HoverImage = null;
            this.btnFinish.Location = new System.Drawing.Point(446, 236);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnFinish.NormalImage")));
            this.btnFinish.PressedImage = null;
            this.btnFinish.Size = new System.Drawing.Size(164, 33);
            this.btnFinish.TabIndex = 14;
            this.btnFinish.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnFinish.Click += new System.EventHandler(this.ButtonFinish_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(4, 236);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 16;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 22);
            this.label1.TabIndex = 13;
            this.label1.Text = "Summary";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlUserMappings);
            this.panel1.Controls.Add(this.pnlRestoreComputer);
            this.panel1.Location = new System.Drawing.Point(43, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 155);
            this.panel1.TabIndex = 17;
            // 
            // pnlUserMappings
            // 
            this.pnlUserMappings.AutoScroll = true;
            this.pnlUserMappings.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlUserMappings.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.pnlUserMappings.Location = new System.Drawing.Point(0, 23);
            this.pnlUserMappings.Name = "pnlUserMappings";
            this.pnlUserMappings.Padding = new System.Windows.Forms.Padding(3);
            this.pnlUserMappings.Size = new System.Drawing.Size(550, 131);
            this.pnlUserMappings.TabIndex = 1;
            // 
            // pnlRestoreComputer
            // 
            this.pnlRestoreComputer.Controls.Add(this.lblComputerName);
            this.pnlRestoreComputer.Controls.Add(this.lblRestoredFrom);
            this.pnlRestoreComputer.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRestoreComputer.Location = new System.Drawing.Point(0, 0);
            this.pnlRestoreComputer.Name = "pnlRestoreComputer";
            this.pnlRestoreComputer.Padding = new System.Windows.Forms.Padding(3);
            this.pnlRestoreComputer.Size = new System.Drawing.Size(550, 23);
            this.pnlRestoreComputer.TabIndex = 0;
            // 
            // lblComputerName
            // 
            this.lblComputerName.AutoSize = true;
            this.lblComputerName.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblComputerName.Location = new System.Drawing.Point(182, 3);
            this.lblComputerName.Name = "lblComputerName";
            this.lblComputerName.Size = new System.Drawing.Size(115, 17);
            this.lblComputerName.TabIndex = 0;
            this.lblComputerName.Text = "lblComputerName";
            // 
            // lblRestoredFrom
            // 
            this.lblRestoredFrom.AutoSize = true;
            this.lblRestoredFrom.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestoredFrom.Location = new System.Drawing.Point(6, 3);
            this.lblRestoredFrom.Name = "lblRestoredFrom";
            this.lblRestoredFrom.Size = new System.Drawing.Size(177, 17);
            this.lblRestoredFrom.TabIndex = 0;
            this.lblRestoredFrom.Text = "The files will be restored from ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(30, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(426, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Please confirm your selections, then click �Start Restoring Files� to continue";
            // 
            // Summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnFinish);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label1);
            this.Name = "Summary";
            this.Size = new System.Drawing.Size(614, 280);
            this.panel1.ResumeLayout(false);
            this.pnlRestoreComputer.ResumeLayout(false);
            this.pnlRestoreComputer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton btnBack;
        private ImageButton btnFinish;
        private ImageButton btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlUserMappings;
        private System.Windows.Forms.Panel pnlRestoreComputer;
        private System.Windows.Forms.Label lblRestoredFrom;
        private System.Windows.Forms.Label lblComputerName;
        private System.Windows.Forms.Label label4;
    }
}
