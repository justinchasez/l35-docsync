using System;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.App.Dialogs;
using DocSync.Client.Controls.Properties;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    /// <summary>
    /// The restore mode view.
    /// </summary>
    public partial class RestoreModeSelect : WizardViewBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreModeSelect"/> class.
        /// </summary>
        public RestoreModeSelect()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the ButtonBack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonBack_Click(object sender, EventArgs e)
        {
            RestoreWizardTab selTab = (this.GetContainer<RestoreWizard>().SelectedComputer.Id ==
                                      ClientSettingsManager.Settings.CurrentComputer.Id)
                                          ? RestoreWizardTab.Start
                                          : RestoreWizardTab.ComputerSelect;

            EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(new SwitchRestoreWizardTabEventArgs
            {
                SelectedTab = selTab
            });
        }

        /// <summary>
        /// Handles the Click event of the ButtonNext control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonNext_Click(object sender, EventArgs e)
        {
            this.GetContainer<RestoreWizard>().RestoreToDefaultLocation = this.rbtnDafultLocations.Checked;

            if (this.rbtnDafultLocations.Checked)
            {
                EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                    new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.Summary });
            }
            else
            {
                RestoreWizard container = this.GetContainer<RestoreWizard>();
                int backupUsersCount =
                    RemotingManager.Instance.ProxyManager.ComputerManager.GetBackupUsersList(container.SelectedComputer)
                        .Count;

                if (backupUsersCount > 0)
                {
                    EventPublisher.RestoreWizardEventManager.OnSwitchRestoreWizardView(
                        new SwitchRestoreWizardTabEventArgs { SelectedTab = RestoreWizardTab.AccountSelect });
                }
                else
                {
                    MessageBox.Show(string.Concat(Resources.RestoreWizardModeSelectNoUsersError, "'", container.SelectedComputer.Name, "'"));
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the ButtonCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.OnRestoreWizardClose(new CancelEventArgs(false));
        }
    }
}
