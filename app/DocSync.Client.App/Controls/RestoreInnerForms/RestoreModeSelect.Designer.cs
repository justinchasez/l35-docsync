using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.RestoreInnerForms
{
    partial class RestoreModeSelect
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbtnDafultLocations = new System.Windows.Forms.RadioButton();
            this.rbtnCustomLocations = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBack = new ImageButton();
            this.btnNext = new ImageButton();
            this.btnCancel = new ImageButton();
            this.SuspendLayout();
            // 
            // rbtnDafultLocations
            // 
            this.rbtnDafultLocations.AutoSize = true;
            this.rbtnDafultLocations.Checked = true;
            this.rbtnDafultLocations.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnDafultLocations.Location = new System.Drawing.Point(46, 58);
            this.rbtnDafultLocations.Name = "rbtnDafultLocations";
            this.rbtnDafultLocations.Size = new System.Drawing.Size(298, 21);
            this.rbtnDafultLocations.TabIndex = 0;
            this.rbtnDafultLocations.TabStop = true;
            this.rbtnDafultLocations.Text = "Restore all missing files to their original locations";
            this.rbtnDafultLocations.UseVisualStyleBackColor = true;
            // 
            // rbtnCustomLocations
            // 
            this.rbtnCustomLocations.AutoSize = true;
            this.rbtnCustomLocations.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.rbtnCustomLocations.Location = new System.Drawing.Point(46, 105);
            this.rbtnCustomLocations.Name = "rbtnCustomLocations";
            this.rbtnCustomLocations.Size = new System.Drawing.Size(283, 21);
            this.rbtnCustomLocations.TabIndex = 0;
            this.rbtnCustomLocations.TabStop = true;
            this.rbtnCustomLocations.Text = "Restore my files to the user account(s) I select";
            this.rbtnCustomLocations.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Restore mode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(350, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "(Recommended)";
            // 
            // btnBack
            // 
            this.btnBack.AllowTransparency = false;
            this.btnBack.AnimatePress = true;
            this.btnBack.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnBack.HoverImage = null;
            this.btnBack.Location = new System.Drawing.Point(276, 244);
            this.btnBack.Name = "btnBack";
            this.btnBack.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonBack;
            this.btnBack.PressedImage = null;
            this.btnBack.Size = new System.Drawing.Size(164, 33);
            this.btnBack.TabIndex = 7;
            this.btnBack.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnBack.Click += new System.EventHandler(this.ButtonBack_Click);
            // 
            // btnNext
            // 
            this.btnNext.AllowTransparency = false;
            this.btnNext.AnimatePress = true;
            this.btnNext.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnNext.HoverImage = null;
            this.btnNext.Location = new System.Drawing.Point(446, 244);
            this.btnNext.Name = "btnNext";
            this.btnNext.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonNext;
            this.btnNext.PressedImage = null;
            this.btnNext.Size = new System.Drawing.Size(164, 33);
            this.btnNext.TabIndex = 6;
            this.btnNext.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnNext.Click += new System.EventHandler(this.ButtonNext_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AllowTransparency = false;
            this.btnCancel.AnimatePress = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.HoverImage = null;
            this.btnCancel.Location = new System.Drawing.Point(4, 244);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnCancel.PressedImage = null;
            this.btnCancel.Size = new System.Drawing.Size(164, 33);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.btnCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // RestoreModeSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rbtnCustomLocations);
            this.Controls.Add(this.rbtnDafultLocations);
            this.Name = "RestoreModeSelect";
            this.Size = new System.Drawing.Size(614, 280);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbtnDafultLocations;
        private System.Windows.Forms.RadioButton rbtnCustomLocations;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private ImageButton btnBack;
        private ImageButton btnNext;
        private ImageButton btnCancel;
    }
}
