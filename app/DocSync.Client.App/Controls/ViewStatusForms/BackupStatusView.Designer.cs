namespace DocSync.Client.App.Controls.ViewStatusForms
{
    partial class BackupStatusView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BackupStatusView));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBackupedSize = new System.Windows.Forms.Label();
            this.lblBackupPendingSize = new System.Windows.Forms.Label();
            this.lblBackuped = new System.Windows.Forms.Label();
            this.lblBackupPending = new System.Windows.Forms.Label();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lnkSubscriptionDetails = new System.Windows.Forms.LinkLabel();
            this.lblBackupStatus = new System.Windows.Forms.Label();
            this.pgbBackupProgress = new StatusProgress();
            this.pnlBackupStatus = new System.Windows.Forms.Panel();
            this.pnlNoBackup = new System.Windows.Forms.Panel();
            this.lbtnShowAddFiles = new System.Windows.Forms.LinkLabel();
            this.lblNoFilesSelected = new System.Windows.Forms.Label();
            this.pnlCurrentFile = new System.Windows.Forms.Panel();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblASAP = new System.Windows.Forms.Label();
            this.pbxErrorInformation = new System.Windows.Forms.PictureBox();
            this.ttpError = new System.Windows.Forms.ToolTip(this.components);
            this.labelSpeed = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlBackupStatus.SuspendLayout();
            this.pnlNoBackup.SuspendLayout();
            this.pnlCurrentFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxErrorInformation)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(30, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Your backup contains:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(30, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 22);
            this.label2.TabIndex = 0;
            this.label2.Text = "Awaiting backup:";
            // 
            // lblBackupedSize
            // 
            this.lblBackupedSize.AutoSize = true;
            this.lblBackupedSize.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupedSize.Location = new System.Drawing.Point(204, 7);
            this.lblBackupedSize.Name = "lblBackupedSize";
            this.lblBackupedSize.Size = new System.Drawing.Size(19, 22);
            this.lblBackupedSize.TabIndex = 1;
            this.lblBackupedSize.Text = "0";
            // 
            // lblBackupPendingSize
            // 
            this.lblBackupPendingSize.AutoSize = true;
            this.lblBackupPendingSize.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupPendingSize.Location = new System.Drawing.Point(204, 31);
            this.lblBackupPendingSize.Name = "lblBackupPendingSize";
            this.lblBackupPendingSize.Size = new System.Drawing.Size(19, 22);
            this.lblBackupPendingSize.TabIndex = 2;
            this.lblBackupPendingSize.Text = "0";
            // 
            // lblBackuped
            // 
            this.lblBackuped.AutoSize = true;
            this.lblBackuped.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBackuped.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackuped.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lblBackuped.Location = new System.Drawing.Point(342, 11);
            this.lblBackuped.Name = "lblBackuped";
            this.lblBackuped.Size = new System.Drawing.Size(48, 17);
            this.lblBackuped.TabIndex = 3;
            this.lblBackuped.Text = "(0 files)";
            this.lblBackuped.Click += new System.EventHandler(this.LinkBackuped_Click);
            // 
            // lblBackupPending
            // 
            this.lblBackupPending.AutoSize = true;
            this.lblBackupPending.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBackupPending.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupPending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lblBackupPending.Location = new System.Drawing.Point(342, 35);
            this.lblBackupPending.Name = "lblBackupPending";
            this.lblBackupPending.Size = new System.Drawing.Size(48, 17);
            this.lblBackupPending.TabIndex = 4;
            this.lblBackupPending.Text = "(0 files)";
            this.lblBackupPending.Click += new System.EventHandler(this.LinkBackupPending_Click);
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Font = new System.Drawing.Font("Calibri", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblPercentage.Location = new System.Drawing.Point(367, 8);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(61, 27);
            this.lblPercentage.TabIndex = 6;
            this.lblPercentage.Text = "100%";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(44, 196);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(63, 65);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(114, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(287, 51);
            this.label3.TabIndex = 8;
            this.label3.Text = "Did you know that you can add free months to \r\nyour subscription for each friend " +
				"that you refer to \r\nDocSync?\r\n";
            // 
            // lnkSubscriptionDetails
            // 
            this.lnkSubscriptionDetails.AutoSize = true;
            this.lnkSubscriptionDetails.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lnkSubscriptionDetails.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lnkSubscriptionDetails.Location = new System.Drawing.Point(114, 265);
            this.lnkSubscriptionDetails.Name = "lnkSubscriptionDetails";
            this.lnkSubscriptionDetails.Size = new System.Drawing.Size(100, 14);
            this.lnkSubscriptionDetails.TabIndex = 9;
            this.lnkSubscriptionDetails.TabStop = true;
            this.lnkSubscriptionDetails.Text = "(Click for details...)";
            this.lnkSubscriptionDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkSubscriptionDetails_LinkClicked);
            // 
            // lblBackupStatus
            // 
            this.lblBackupStatus.AutoSize = true;
            this.lblBackupStatus.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblBackupStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblBackupStatus.Location = new System.Drawing.Point(32, 97);
            this.lblBackupStatus.Name = "lblBackupStatus";
            this.lblBackupStatus.Size = new System.Drawing.Size(166, 22);
            this.lblBackupStatus.TabIndex = 0;
            this.lblBackupStatus.Text = "Your backup is failed";
            // 
            // pgbBackupProgress
            // 
            this.pgbBackupProgress.BackColor = System.Drawing.Color.Transparent;
            this.pgbBackupProgress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pgbBackupProgress.BackgroundImage")));
            this.pgbBackupProgress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pgbBackupProgress.Location = new System.Drawing.Point(3, 3);
            this.pgbBackupProgress.Maximum = 100;
            this.pgbBackupProgress.Minimum = 0;
            this.pgbBackupProgress.Name = "pgbBackupProgress";
            this.pgbBackupProgress.Size = new System.Drawing.Size(358, 36);
            this.pgbBackupProgress.TabIndex = 5;
            this.pgbBackupProgress.Value = 0;
            // 
            // pnlBackupStatus
            // 
            this.pnlBackupStatus.Controls.Add(this.pgbBackupProgress);
            this.pnlBackupStatus.Controls.Add(this.lblPercentage);
            this.pnlBackupStatus.Location = new System.Drawing.Point(29, 58);
            this.pnlBackupStatus.Name = "pnlBackupStatus";
            this.pnlBackupStatus.Size = new System.Drawing.Size(431, 41);
            this.pnlBackupStatus.TabIndex = 10;
            // 
            // pnlNoBackup
            // 
            this.pnlNoBackup.Controls.Add(this.lbtnShowAddFiles);
            this.pnlNoBackup.Controls.Add(this.lblNoFilesSelected);
            this.pnlNoBackup.Location = new System.Drawing.Point(29, 70);
            this.pnlNoBackup.Name = "pnlNoBackup";
            this.pnlNoBackup.Size = new System.Drawing.Size(431, 29);
            this.pnlNoBackup.TabIndex = 11;
            // 
            // lbtnShowAddFiles
            // 
            this.lbtnShowAddFiles.AutoSize = true;
            this.lbtnShowAddFiles.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbtnShowAddFiles.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lbtnShowAddFiles.Location = new System.Drawing.Point(278, 13);
            this.lbtnShowAddFiles.Name = "lbtnShowAddFiles";
            this.lbtnShowAddFiles.Size = new System.Drawing.Size(150, 17);
            this.lbtnShowAddFiles.TabIndex = 17;
            this.lbtnShowAddFiles.TabStop = true;
            this.lbtnShowAddFiles.Text = "Show me how to add files";
            // 
            // lblNoFilesSelected
            // 
            this.lblNoFilesSelected.AutoSize = true;
            this.lblNoFilesSelected.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblNoFilesSelected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblNoFilesSelected.Location = new System.Drawing.Point(3, 9);
            this.lblNoFilesSelected.Name = "lblNoFilesSelected";
            this.lblNoFilesSelected.Size = new System.Drawing.Size(218, 22);
            this.lblNoFilesSelected.TabIndex = 16;
            this.lblNoFilesSelected.Text = "No files selected for backup";
            // 
            // pnlCurrentFile
            // 
            this.pnlCurrentFile.Controls.Add(this.lblFileName);
            this.pnlCurrentFile.Controls.Add(this.label5);
            this.pnlCurrentFile.Location = new System.Drawing.Point(29, 149);
            this.pnlCurrentFile.Name = "pnlCurrentFile";
            this.pnlCurrentFile.Size = new System.Drawing.Size(441, 40);
            this.pnlCurrentFile.TabIndex = 12;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoEllipsis = true;
            this.lblFileName.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblFileName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblFileName.Location = new System.Drawing.Point(6, 20);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(432, 19);
            this.lblFileName.TabIndex = 2;
            this.lblFileName.Text = "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(6, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 19);
            this.label5.TabIndex = 1;
            this.label5.Text = "Backing Up Now:";
            // 
            // lblASAP
            // 
            this.lblASAP.AutoSize = true;
            this.lblASAP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblASAP.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblASAP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lblASAP.Location = new System.Drawing.Point(376, 101);
            this.lblASAP.Name = "lblASAP";
            this.lblASAP.Size = new System.Drawing.Size(85, 17);
            this.lblASAP.TabIndex = 3;
            this.lblASAP.Text = "Back-up ASAP";
            this.lblASAP.Visible = false;
            this.lblASAP.Click += new System.EventHandler(this.ASAP_click);
            // 
            // pbxErrorInformation
            // 
            this.pbxErrorInformation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxErrorInformation.Image = ((System.Drawing.Image)(resources.GetObject("pbxErrorInformation.Image")));
            this.pbxErrorInformation.Location = new System.Drawing.Point(10, 97);
            this.pbxErrorInformation.Name = "pbxErrorInformation";
            this.pbxErrorInformation.Size = new System.Drawing.Size(20, 20);
            this.pbxErrorInformation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxErrorInformation.TabIndex = 13;
            this.pbxErrorInformation.TabStop = false;
            this.pbxErrorInformation.Visible = false;
            // 
            // ttpError
            // 
            this.ttpError.AutoPopDelay = 10000;
            this.ttpError.InitialDelay = 500;
            this.ttpError.IsBalloon = true;
            this.ttpError.ReshowDelay = 100;
            this.ttpError.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttpError.ToolTipTitle = "Your backup is failed";
            this.ttpError.UseAnimation = false;
            this.ttpError.UseFading = false;
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.labelSpeed.Location = new System.Drawing.Point(288, 97);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(102, 22);
            this.labelSpeed.TabIndex = 20;
            this.labelSpeed.Text = "0,00 Bytes/s";
            this.labelSpeed.Visible = false;
            this.labelSpeed.Visible = false;
            // 
            // BackupStatusView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelSpeed);
            this.Controls.Add(this.pbxErrorInformation);
            this.Controls.Add(this.pnlCurrentFile);
            this.Controls.Add(this.lnkSubscriptionDetails);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblBackupPending);
            this.Controls.Add(this.lblASAP);
            this.Controls.Add(this.lblBackuped);
            this.Controls.Add(this.lblBackupPendingSize);
            this.Controls.Add(this.lblBackupedSize);
            this.Controls.Add(this.lblBackupStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlBackupStatus);
            this.Controls.Add(this.pnlNoBackup);
            this.Name = "BackupStatusView";
            this.Size = new System.Drawing.Size(486, 286);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlBackupStatus.ResumeLayout(false);
            this.pnlBackupStatus.PerformLayout();
            this.pnlNoBackup.ResumeLayout(false);
            this.pnlNoBackup.PerformLayout();
            this.pnlCurrentFile.ResumeLayout(false);
            this.pnlCurrentFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxErrorInformation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBackupedSize;
        private System.Windows.Forms.Label lblBackupPendingSize;
        private System.Windows.Forms.Label lblBackuped;
        private System.Windows.Forms.Label lblBackupPending;
        private StatusProgress pgbBackupProgress;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel lnkSubscriptionDetails;
        private System.Windows.Forms.Label lblBackupStatus;
        private System.Windows.Forms.Panel pnlBackupStatus;
        private System.Windows.Forms.Panel pnlNoBackup;
        private System.Windows.Forms.LinkLabel lbtnShowAddFiles;
        private System.Windows.Forms.Label lblNoFilesSelected;
        private System.Windows.Forms.Panel pnlCurrentFile;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblASAP;
        private System.Windows.Forms.PictureBox pbxErrorInformation;
        private System.Windows.Forms.ToolTip ttpError;
        private System.Windows.Forms.Label labelSpeed;
    }
}
