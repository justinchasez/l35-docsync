using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Controls.ViewStatusForms
{
    partial class RestoreStatusView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreStatusView));
            this.pnlCurrentFile = new System.Windows.Forms.Panel();
            this.lblFileName = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbtnCancelRestoring = new System.Windows.Forms.LinkLabel();
            this.pnlNoRestore = new System.Windows.Forms.Panel();
            this.labelSpeed = new System.Windows.Forms.Label();
            this.lbtnShowAddFiles = new System.Windows.Forms.LinkLabel();
            this.lblRestoreStatus = new System.Windows.Forms.Label();
            this.pnlRestoreStatus = new System.Windows.Forms.Panel();
            this.pgbRestoreProgress = new StatusProgress();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.imageButton2 = new ImageButton();
            this.imageButton1 = new ImageButton();
            this.lblRestorePending = new System.Windows.Forms.Label();
            this.lblRestored = new System.Windows.Forms.Label();
            this.lblRestorePendingSize = new System.Windows.Forms.Label();
            this.lblRestoredSize = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pbxErrorInformation = new System.Windows.Forms.PictureBox();
            this.ttpError = new System.Windows.Forms.ToolTip(this.components);
            this.pnlCurrentFile.SuspendLayout();
            this.pnlNoRestore.SuspendLayout();
            this.pnlRestoreStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxErrorInformation)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCurrentFile
            // 
            this.pnlCurrentFile.Controls.Add(this.lblFileName);
            this.pnlCurrentFile.Controls.Add(this.label5);
            this.pnlCurrentFile.Location = new System.Drawing.Point(29, 129);
            this.pnlCurrentFile.Name = "pnlCurrentFile";
            this.pnlCurrentFile.Size = new System.Drawing.Size(431, 40);
            this.pnlCurrentFile.TabIndex = 17;
            this.pnlCurrentFile.Visible = false;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoEllipsis = true;
            this.lblFileName.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblFileName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblFileName.Location = new System.Drawing.Point(6, 20);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(422, 19);
            this.lblFileName.TabIndex = 2;
            this.lblFileName.Text = "Backup in Progress";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(6, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 19);
            this.label5.TabIndex = 1;
            this.label5.Text = "Now restoring:";
            // 
            // lbtnCancelRestoring
            // 
            this.lbtnCancelRestoring.AutoSize = true;
            this.lbtnCancelRestoring.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbtnCancelRestoring.Font = new System.Drawing.Font("Calibri", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbtnCancelRestoring.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lbtnCancelRestoring.Location = new System.Drawing.Point(309, 7);
            this.lbtnCancelRestoring.Name = "lbtnCancelRestoring";
            this.lbtnCancelRestoring.Size = new System.Drawing.Size(119, 19);
            this.lbtnCancelRestoring.TabIndex = 16;
            this.lbtnCancelRestoring.TabStop = true;
            this.lbtnCancelRestoring.Text = "Cancel Restoring";
            this.lbtnCancelRestoring.Visible = false;
            this.lbtnCancelRestoring.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkCancelRestoring_LinkClicked);
            // 
            // pnlNoRestore
            // 
            this.pnlNoRestore.Controls.Add(this.labelSpeed);
            this.pnlNoRestore.Controls.Add(this.lbtnCancelRestoring);
            this.pnlNoRestore.Controls.Add(this.lbtnShowAddFiles);
            this.pnlNoRestore.Controls.Add(this.lblRestoreStatus);
            this.pnlNoRestore.Location = new System.Drawing.Point(29, 93);
            this.pnlNoRestore.Name = "pnlNoRestore";
            this.pnlNoRestore.Size = new System.Drawing.Size(431, 30);
            this.pnlNoRestore.TabIndex = 16;
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.labelSpeed.Location = new System.Drawing.Point(180, 4);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(102, 22);
            this.labelSpeed.TabIndex = 19;
            this.labelSpeed.Text = "0,00 Bytes/s";
            this.labelSpeed.Visible = false;
            // 
            // lbtnShowAddFiles
            // 
            this.lbtnShowAddFiles.AutoSize = true;
            this.lbtnShowAddFiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbtnShowAddFiles.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lbtnShowAddFiles.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lbtnShowAddFiles.Location = new System.Drawing.Point(278, 9);
            this.lbtnShowAddFiles.Name = "lbtnShowAddFiles";
            this.lbtnShowAddFiles.Size = new System.Drawing.Size(150, 17);
            this.lbtnShowAddFiles.TabIndex = 15;
            this.lbtnShowAddFiles.TabStop = true;
            this.lbtnShowAddFiles.Text = "Show me how to add files";
            this.lbtnShowAddFiles.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkShowAddFiles_LinkClicked);
            // 
            // lblRestoreStatus
            // 
            this.lblRestoreStatus.AutoSize = true;
            this.lblRestoreStatus.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestoreStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            this.lblRestoreStatus.Location = new System.Drawing.Point(3, 4);
            this.lblRestoreStatus.Name = "lblRestoreStatus";
            this.lblRestoreStatus.Size = new System.Drawing.Size(215, 22);
            this.lblRestoreStatus.TabIndex = 14;
            this.lblRestoreStatus.Text = "No files selected for restore";
            // 
            // pnlRestoreStatus
            // 
            this.pnlRestoreStatus.Controls.Add(this.pgbRestoreProgress);
            this.pnlRestoreStatus.Controls.Add(this.lblPercentage);
            this.pnlRestoreStatus.Location = new System.Drawing.Point(29, 58);
            this.pnlRestoreStatus.Name = "pnlRestoreStatus";
            this.pnlRestoreStatus.Size = new System.Drawing.Size(431, 41);
            this.pnlRestoreStatus.TabIndex = 15;
            // 
            // pgbRestoreProgress
            // 
            this.pgbRestoreProgress.BackColor = System.Drawing.Color.Transparent;
            this.pgbRestoreProgress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pgbRestoreProgress.BackgroundImage")));
            this.pgbRestoreProgress.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pgbRestoreProgress.Location = new System.Drawing.Point(3, 3);
            this.pgbRestoreProgress.Maximum = 100;
            this.pgbRestoreProgress.Minimum = 0;
            this.pgbRestoreProgress.Name = "pgbRestoreProgress";
            this.pgbRestoreProgress.Size = new System.Drawing.Size(357, 36);
            this.pgbRestoreProgress.TabIndex = 12;
            this.pgbRestoreProgress.Value = 0;
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Font = new System.Drawing.Font("Calibri", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblPercentage.Location = new System.Drawing.Point(367, 8);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(61, 27);
            this.lblPercentage.TabIndex = 13;
            this.lblPercentage.Text = "100%";
            // 
            // imageButton2
            // 
            this.imageButton2.AccessibleDescription = "This is description.";
            this.imageButton2.AccessibleName = "This is accessible name";
            this.imageButton2.AllowTransparency = false;
            this.imageButton2.AnimatePress = true;
            this.imageButton2.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton2.HoverImage = null;
            this.imageButton2.Location = new System.Drawing.Point(102, 172);
            this.imageButton2.Name = "imageButton2";
            this.imageButton2.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton2.NormalImage")));
            this.imageButton2.PressedImage = null;
            this.imageButton2.Size = new System.Drawing.Size(280, 54);
            this.imageButton2.TabIndex = 11;
            this.imageButton2.Tag = "This is the tag.";
            this.imageButton2.Text = "Select restore files";
            this.imageButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.imageButton2.TextVerticalOffcet = 100;
            this.imageButton2.Click += new System.EventHandler(this.SelectFiles_Click);
            // 
            // imageButton1
            // 
            this.imageButton1.AllowTransparency = false;
            this.imageButton1.AnimatePress = true;
            this.imageButton1.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton1.HoverImage = null;
            this.imageButton1.Location = new System.Drawing.Point(105, 232);
            this.imageButton1.Name = "imageButton1";
            this.imageButton1.NormalImage = ((System.Drawing.Image)(resources.GetObject("imageButton1.NormalImage")));
            this.imageButton1.PressedImage = null;
            this.imageButton1.Size = new System.Drawing.Size(277, 54);
            this.imageButton1.TabIndex = 11;
            this.imageButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.imageButton1.Click += new System.EventHandler(this.ViewRestoredFiles_Click);
            // 
            // lblRestorePending
            // 
            this.lblRestorePending.AutoSize = true;
            this.lblRestorePending.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRestorePending.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestorePending.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lblRestorePending.Location = new System.Drawing.Point(342, 35);
            this.lblRestorePending.Name = "lblRestorePending";
            this.lblRestorePending.Size = new System.Drawing.Size(40, 17);
            this.lblRestorePending.TabIndex = 10;
            this.lblRestorePending.Text = "File(s)";
            this.lblRestorePending.Click += new System.EventHandler(this.FilesPending_Click);
            // 
            // lblRestored
            // 
            this.lblRestored.AutoSize = true;
            this.lblRestored.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRestored.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestored.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.lblRestored.Location = new System.Drawing.Point(342, 11);
            this.lblRestored.Name = "lblRestored";
            this.lblRestored.Size = new System.Drawing.Size(40, 17);
            this.lblRestored.TabIndex = 9;
            this.lblRestored.Text = "File(s)";
            this.lblRestored.Click += new System.EventHandler(this.FilesRestored_Click);
            // 
            // lblRestorePendingSize
            // 
            this.lblRestorePendingSize.AutoSize = true;
            this.lblRestorePendingSize.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestorePendingSize.Location = new System.Drawing.Point(204, 31);
            this.lblRestorePendingSize.Name = "lblRestorePendingSize";
            this.lblRestorePendingSize.Size = new System.Drawing.Size(19, 22);
            this.lblRestorePendingSize.TabIndex = 8;
            this.lblRestorePendingSize.Text = "0";
            // 
            // lblRestoredSize
            // 
            this.lblRestoredSize.AutoSize = true;
            this.lblRestoredSize.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblRestoredSize.Location = new System.Drawing.Point(204, 7);
            this.lblRestoredSize.Name = "lblRestoredSize";
            this.lblRestoredSize.Size = new System.Drawing.Size(19, 22);
            this.lblRestoredSize.TabIndex = 7;
            this.lblRestoredSize.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(30, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(170, 22);
            this.label2.TabIndex = 5;
            this.label2.Text = "Files pending restore:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(30, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "Files restored:";
            // 
            // pbxErrorInformation
            // 
            this.pbxErrorInformation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxErrorInformation.Image = ((System.Drawing.Image)(resources.GetObject("pbxErrorInformation.Image")));
            this.pbxErrorInformation.Location = new System.Drawing.Point(12, 98);
            this.pbxErrorInformation.Name = "pbxErrorInformation";
            this.pbxErrorInformation.Size = new System.Drawing.Size(20, 20);
            this.pbxErrorInformation.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxErrorInformation.TabIndex = 18;
            this.pbxErrorInformation.TabStop = false;
            this.pbxErrorInformation.Visible = false;
            // 
            // ttpError
            // 
            this.ttpError.IsBalloon = true;
            this.ttpError.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttpError.ToolTipTitle = "Restoring is failed";
            // 
            // RestoreStatusView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pbxErrorInformation);
            this.Controls.Add(this.pnlCurrentFile);
            this.Controls.Add(this.pnlRestoreStatus);
            this.Controls.Add(this.imageButton2);
            this.Controls.Add(this.imageButton1);
            this.Controls.Add(this.lblRestorePending);
            this.Controls.Add(this.lblRestored);
            this.Controls.Add(this.lblRestorePendingSize);
            this.Controls.Add(this.lblRestoredSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pnlNoRestore);
            this.Name = "RestoreStatusView";
            this.Size = new System.Drawing.Size(486, 286);
            this.pnlCurrentFile.ResumeLayout(false);
            this.pnlCurrentFile.PerformLayout();
            this.pnlNoRestore.ResumeLayout(false);
            this.pnlNoRestore.PerformLayout();
            this.pnlRestoreStatus.ResumeLayout(false);
            this.pnlRestoreStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxErrorInformation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRestorePending;
        private System.Windows.Forms.Label lblRestored;
        private System.Windows.Forms.Label lblRestorePendingSize;
        private System.Windows.Forms.Label lblRestoredSize;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ImageButton imageButton1;
        private ImageButton imageButton2;
        private StatusProgress pgbRestoreProgress;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.Label lblRestoreStatus;
        private System.Windows.Forms.Panel pnlRestoreStatus;
        private System.Windows.Forms.Panel pnlNoRestore;
        private System.Windows.Forms.LinkLabel lbtnShowAddFiles;
        private System.Windows.Forms.Panel pnlCurrentFile;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel lbtnCancelRestoring;
        private System.Windows.Forms.PictureBox pbxErrorInformation;
        private System.Windows.Forms.ToolTip ttpError;
        private System.Windows.Forms.Label labelSpeed;

    }
}
