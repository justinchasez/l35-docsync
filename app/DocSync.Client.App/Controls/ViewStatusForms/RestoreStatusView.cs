using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Managers.Convert;
using DocSync.Client.Core.Utils;
using DocSync.Client.Remoting.Args;

namespace DocSync.Client.App.Controls.ViewStatusForms
{
    /// <summary>
    /// The restore status view control.
    /// </summary>
    public partial class RestoreStatusView : ViewBase
    {
        /// <summary>
        /// Path to VirtualDrive in explorer
        /// </summary>
        private readonly string virtualDrivePath = "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}";

        /// <summary>
        /// Name of Explorer process
        /// </summary>
        private readonly string processName = "explorer";

        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreStatusView"/> class.
        /// </summary>
        public RestoreStatusView()
        {
            this.InitializeComponent();

            this.Load += this.RestoreStatus_Load;
        }

        /// <summary>
        /// Handles the Load event of the RestoreStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RestoreStatus_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
            {
                return;
            }

            RemotingManager.Instance.ProxyPublisher.RestoreStatusChanged += this.BackupManager_RestoreStatusChanged;
            RemotingManager.Instance.ProxyPublisher.RestoreProgressChanged += this.BackupManager_RestoreProgressChanged;
            RemotingManager.Instance.ProxyPublisher.RestoreSpeedChanged += this.BackupManager_RestoreSpeedChanged;

            this.BackupManager_RestoreStatusChanged(new RestoreStatusChangedEventArgs
                                                        {
                                                            Status =
                                                                RemotingManager.Instance.ProxyManager.BackupManager.
                                                                GetRestoreStatus()
                                                        });

            Thread progressUpdateThread = new Thread(this.CallRestoreProgressChanged);

            progressUpdateThread.Start();
        }

        /// <summary>
        /// Backups the manager_ restore speed changed.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_RestoreSpeedChanged(SpeedChangedEventArgs args)
        {
            this.Invoke(new MethodInvoker(delegate
                                              {
                                                  this.labelSpeed.Text =
                                                      ConvertData.InformationSizeConvertor.SpeedConvert(args.Speed);
                                              }));
        }

        /// <summary>
        /// UIs the event manager instance_ restore progress changed.
        /// </summary>
        /// <param name="args">The <see cref="ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_RestoreProgressChanged(ProgressChangedEventArgs args)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                this.lblRestored.Text = string.Format("({0} files)", args.CurrentProgress.FilesCompleted);
                int restorePenging = args.CurrentProgress.FilesPending - args.CurrentProgress.FilesErrors;
                this.lblRestorePending.Text = string.Format("({0} files)", restorePenging);

                this.lblRestoredSize.Text = ConvertData.InformationSizeConvertor.Convert(args.CurrentProgress.FilesCompletedSize);
                this.lblRestorePendingSize.Text = ConvertData.InformationSizeConvertor.Convert(args.CurrentProgress.FilesPendingSize);

                this.lblPercentage.Text = string.Format("{0}%", args.CurrentProgress.Percentage);

                this.pgbRestoreProgress.Value = args.CurrentProgress.Percentage;

                this.lblFileName.Text = StringUtils.GetShortFileName(!string.IsNullOrEmpty(args.CurrentProgress.CurrentFilePath)
                                            ? args.CurrentProgress.CurrentFilePath
                                            : "-");

                if (!String.IsNullOrEmpty(args.CurrentProgress.ErrorMessage))
                {
                    this.ttpError.SetToolTip(this.pbxErrorInformation, args.CurrentProgress.ErrorMessage);
                }
            }));
        }

        /// <summary>
        /// Backups the manager_ backup status changed.
        /// </summary>
        /// <param name="args">The <see cref="BackupStatusChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_RestoreStatusChanged(RestoreStatusChangedEventArgs args)
        {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.pbxErrorInformation.Visible = false;
                    this.lbtnShowAddFiles.Visible = false;
                    this.pnlCurrentFile.Visible = false;
                    this.lbtnCancelRestoring.Visible = false;
                    this.labelSpeed.Visible = false;

                    if (RemotingManager.Instance.ProxyManager.AppManger.Disable)
                    {
                        this.lblRestoreStatus.Text = "Restoring is disabled";
                    }
                    else
                    {
                        switch (args.Status)
                        {
                            case RestoreStatus.InProgress:
                                this.lblRestoreStatus.Text = "Restoring is in progress";
                                this.pnlCurrentFile.Visible = true;
                                this.lbtnCancelRestoring.Visible = true;
                                this.labelSpeed.Visible = true;
                                break;
                            case RestoreStatus.Failed:
                                this.lblRestoreStatus.Text = "Restoring has failed";
                                this.pbxErrorInformation.Visible = true;

                                if (args.IsComputerExpired)
                                {
                                    TrialNotificationWindow notificationWindow = new TrialNotificationWindow(AppSettingsManager.CouponUrl);
                                    notificationWindow.SetParameters(true, 0);
                                    notificationWindow.ShowDialog();
                                    notificationWindow.Close();
                                }

                                if (args.IsInvalidTime)
                                {
                                    var errorMsg = new IncorrectDateTimeMsg();
                                    errorMsg.ShowDialog();
                                }

                                break;
                            case RestoreStatus.Finished:
                                this.lblRestoreStatus.Text = "All restore operations are completed";
                                break;
                            case RestoreStatus.Pending:
                                this.lblRestoreStatus.Text = "Restore is pending";
                                break;
                            case RestoreStatus.Empty:
                                this.lblRestoreStatus.Text = "No files were restored";
                                this.lbtnShowAddFiles.Visible = true;
                                break;
                        }
                    }

                    this.CallRestoreProgressChanged();
                }));
        }

        /// <summary>
        /// Call_s the restore progress changed.
        /// </summary>
        public void CallRestoreProgressChanged()
        {
            Progress restoreProgress = RemotingManager.Instance.ProxyManager.BackupManager.GetRestoreProgress();
            if (restoreProgress != null)
            {
                this.BackupManager_RestoreProgressChanged(new ProgressChangedEventArgs { CurrentProgress = restoreProgress });
            }
        }

        /// <summary>
        /// Handles the LinkClicked event of the LinkShowAddFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LinkShowAddFiles_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.TutorialHowToRestoreFiles);
        }

        /// <summary>
        /// Handles the LinkClicked event of the LinkCancelRestoring control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LinkCancelRestoring_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RemotingManager.Instance.ProxyManager.BackupManager.CancelRestoring();
        }

        /// <summary>
        /// Handles the Click event of the SelectFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void SelectFiles_Click(object sender, EventArgs e)
        {
            Process.Start(this.processName, this.virtualDrivePath + "\\Backed-up Files\"");
        }

        /// <summary>
        /// Handles the Click event of the ViewRestoredFiles control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ViewRestoredFiles_Click(object sender, EventArgs e)
        {
            Process.Start(this.processName, this.virtualDrivePath + "\\Recovery Log\"");
        }

        /// <summary>
        /// Handles the Click event of the FilesRestored control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FilesRestored_Click(object sender, EventArgs e)
        {
            Process.Start(this.processName, this.virtualDrivePath + "\\Recovery Log\\Completed\"");
        }

        /// <summary>
        /// Handles the Click event of the FilesPending control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void FilesPending_Click(object sender, EventArgs e)
        {
            Process.Start(this.processName, this.virtualDrivePath + "\\Recovery Log\\Pending\"");
        }
    }
}
