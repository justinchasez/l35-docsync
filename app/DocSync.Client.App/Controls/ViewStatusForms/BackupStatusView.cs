using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core.Managers;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Args;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Managers.Convert;
using DocSync.Client.Core.Utils;
using DocSync.Client.Remoting.Args;

namespace DocSync.Client.App.Controls.ViewStatusForms
{
    /// <summary>
    /// The backup status view control.
    /// </summary>
    public partial class BackupStatusView : ViewBase
    {
        /// <summary>
        /// Text for ASAP link
        /// </summary>
        private const string TXT_ASAP = "Back-up ASAP";

        /// <summary>
        /// Text for ASAP link
        /// </summary>
        private const string TXT_TRY_AGAIN = "Try again";

        /// <summary>
        /// Initializes a new instance of the <see cref="BackupStatusView"/> class.
        /// </summary>
        public BackupStatusView()
        {
            this.InitializeComponent();

            this.Load += this.BackupStatus_Load;
        }

        /// <summary>
        /// Handles the Load event of the BackupStatus control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void BackupStatus_Load(object sender, EventArgs e)
        {
            if (this.DesignMode || this.IsDesignMode)
            {
                return;
            }

            RemotingManager.Instance.ProxyPublisher.BackupStatusChanged += this.BackupManager_BackupStatusChanged;
            RemotingManager.Instance.ProxyPublisher.BackupProgressChanged += this.BackupManager_BackupProgressChanged;
            RemotingManager.Instance.ProxyPublisher.BackupSpeedChanged += this.BackupManager_BackupSpeedChanged;

            Progress backupProgress = RemotingManager.Instance.ProxyManager.BackupManager.GetBackupProgress();

            BackupStatusChangedEventArgs args = RemotingManager.Instance.ProxyManager.BackupManager.GetBackupStatus();
            args.ErrorMessage = backupProgress.ErrorMessage;

            this.BackupManager_BackupStatusChanged(args);

            Thread progressUpdateThread = new Thread(() =>
                                                         {
                                                             //Thread.Sleep(1000);
                                                             if (backupProgress != null)
                                                             {
                                                                 this.BackupManager_BackupProgressChanged(new ProgressChangedEventArgs { CurrentProgress = backupProgress });
                                                             }
                                                         });

            progressUpdateThread.Start();
        }

        /// <summary>
        /// Backups the manager_ backup speed changed.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_BackupSpeedChanged(SpeedChangedEventArgs args)
        {
            this.Invoke(new MethodInvoker(delegate
                                              {
                                                  this.labelSpeed.Text =
                                                      ConvertData.InformationSizeConvertor.SpeedConvert(args.Speed);
                                              }));
        }

        /// <summary>
        /// Backups the manager_ backup progress changed.
        /// </summary>
        /// <param name="args">The <see cref="ProgressChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_BackupProgressChanged(ProgressChangedEventArgs args)
        {
            this.Invoke(new MethodInvoker(delegate
            {
                //this.pnlNoBackup.Visible = (args.CurrentProgress.Percentage == 0 && String.IsNullOrEmpty(args.CurrentProgress.CurrentFilePath));
                //this.pnlCurrentFile.Visible = !this.pnlNoBackup.Visible;

                if (!string.IsNullOrEmpty(args.CurrentProgress.CurrentFilePath))
                {
                    this.lblFileName.Text = StringUtils.GetShortFileName(args.CurrentProgress.CurrentFilePath);
                }
                else
                {
                    this.lblFileName.Text = string.Empty;
                }

                this.lblBackuped.Text = string.Format("({0} files)", args.CurrentProgress.FilesTotal);
                this.lblBackupPending.Text = string.Format("({0} files)", args.CurrentProgress.FilesPending);

                this.lblBackupedSize.Text = ConvertData.InformationSizeConvertor.Convert(args.CurrentProgress.FilesTotalSize);
                this.lblBackupPendingSize.Text = ConvertData.InformationSizeConvertor.Convert(args.CurrentProgress.FilesPendingSize);

                this.lblPercentage.Text = String.Format("{0}%", args.CurrentProgress.Percentage);

                this.pgbBackupProgress.Value = args.CurrentProgress.Percentage;

                if (!String.IsNullOrEmpty(args.CurrentProgress.ErrorMessage))
                {
                    this.ttpError.SetToolTip(this.pbxErrorInformation, args.CurrentProgress.ErrorMessage);
                }
            }));
        }

        /// <summary>
        /// Backups the manager_ backup status changed.
        /// </summary>
        /// <param name="args">The <see cref="BackupStatusChangedEventArgs"/> instance containing the event data.</param>
        private void BackupManager_BackupStatusChanged(BackupStatusChangedEventArgs args)
        {
            if (RemotingManager.Instance.ProxyManager.AppManger.Disable)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.lblBackupStatus.Text = "Backup is disabled";
                    this.pnlCurrentFile.Visible = false;
                }));
            }
            else if (RemotingManager.Instance.ProxyManager.AppManger.GetPause() > 0)
            {
                this.Invoke(new MethodInvoker(delegate
                    {
                        this.lblBackupStatus.Text = "Backup is paused";
                        this.pnlCurrentFile.Visible = false;
                    }));
            }       
            else if (RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().IsRecoverMode)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.lblBackupStatus.Text = "Recovery mode is enabled";
                }));
            }
            else
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.pbxErrorInformation.Visible = false;
                    this.lblASAP.Visible = false;
                    this.labelSpeed.Visible = false;

                    switch (args.Status)
                    {
                        case BackupStatus.InProgress:
                            //this.lblBackupStatus.Text = Client.Controls.Properties.Resources.BackupStatusProgress;
                            this.lblBackupStatus.Text = "Your backup is in progress";
                            this.labelSpeed.Visible = true;
                            break;
                        case BackupStatus.Failed:
                            this.lblBackupStatus.Text = "Your backup has failed";
                            this.lblASAP.Visible = true;
                            this.pbxErrorInformation.Visible = true;
                            this.lblASAP.Text = TXT_TRY_AGAIN;

                            if (args.IsLimitReached)
                            {
                                AddMoreSpaceMsg msg = new AddMoreSpaceMsg(ConvertData.InformationSizeConvertor.Convert(args.UsedSpace), RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().MyAccountUrl);
                                msg.ShowDialog();
                            }
                            else if (args.IsComputerExpired)
                            {
                                TrialNotificationWindow notificationWindow = new TrialNotificationWindow(AppSettingsManager.CouponUrl);
                                notificationWindow.SetParameters(true, 0);
                                notificationWindow.ShowDialog();
                                notificationWindow.Close();
                            }
                            else
                            {
                                if (args.IsInvalidTime)
                                {
                                    var errorMsg = new IncorrectDateTimeMsg();
                                    errorMsg.ShowDialog();
                                }
//                                MessageBox.Show(this, args.ErrorMessage, "Test", MessageBoxButtons.OK);
                            }
                            
                            break;
                        case BackupStatus.Completed:
                            //this.lblBackupStatus.Text = Client.Controls.Properties.Resources.BackupStatusFinished;
                            this.lblBackupStatus.Text = "Your backup is completed";
                            break;
                        case BackupStatus.Pending:
                            //this.lblBackupStatus.Text = Client.Controls.Properties.Resources.BackupStatusOnHold;
                            this.lblBackupStatus.Text = "Your backup is pending";
                            this.lblASAP.Visible = true;
                            this.lblASAP.Text = TXT_ASAP;
                            break;
                        case BackupStatus.Empty:
                            this.lblBackupStatus.Text = "No files selected for backup";
                            break;
                    }

                    this.pnlNoBackup.Visible = (args.Status != BackupStatus.InProgress);
                    this.pnlCurrentFile.Visible = (args.Status == BackupStatus.InProgress);
                }));
            }
        }

        /// <summary>
        /// Handles the Click event of the LinkBackuped control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void LinkBackuped_Click(object sender, EventArgs e)
        {
            Process.Start("explorer", "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\"");
        }

        /// <summary>
        /// Handles the Click event of the LinkBackupPending control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void LinkBackupPending_Click(object sender, EventArgs e)
        {
            Process.Start("explorer", "\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Pending Back-up\"");
        }

        /// <summary>
        /// Handles the LinkClicked event of the LinkSubscriptionDetails control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.LinkLabelLinkClickedEventArgs"/> instance containing the event data.</param>
        private void LinkSubscriptionDetails_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(AppSettingsManager.HowReferFriendUrl);
        }

        /// <summary>
        /// ASAP click handler
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">Arguments of the event</param>
        private void ASAP_click(object sender, EventArgs e)
        {
            RemotingManager.Instance.ProxyManager.ScheduleManager.IsASAP = true;
            this.lblASAP.Visible = false;
        }
    }
}
