namespace DocSync.Client.App.Controls
{
    partial class StatusProgress
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbStart = new System.Windows.Forms.PictureBox();
            this.pbProgressBar = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbProgressBar)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbStart
            // 
            this.pbStart.Image = global::DocSync.Client.Controls.Properties.Resources.ProgressStart;
            this.pbStart.Location = new System.Drawing.Point(4, 3);
            this.pbStart.Name = "pbStart";
            this.pbStart.Size = new System.Drawing.Size(5, 29);
            this.pbStart.TabIndex = 0;
            this.pbStart.TabStop = false;
            this.pbStart.Visible = false;
            // 
            // pbProgressBar
            // 
            this.pbProgressBar.Image = global::DocSync.Client.Controls.Properties.Resources.ProgressBar;
            this.pbProgressBar.ImageLocation = "";
            this.pbProgressBar.Location = new System.Drawing.Point(-339, 0);
            this.pbProgressBar.Name = "pbProgressBar";
            this.pbProgressBar.Size = new System.Drawing.Size(348, 29);
            this.pbProgressBar.TabIndex = 1;
            this.pbProgressBar.TabStop = false;
            this.pbProgressBar.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbProgressBar);
            this.panel1.Location = new System.Drawing.Point(5, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(349, 29);
            this.panel1.TabIndex = 2;
            // 
            // StatusProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.ProgressBg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.pbStart);
            this.Controls.Add(this.panel1);
            this.Name = "StatusProgress";
            this.Size = new System.Drawing.Size(357, 36);
            ((System.ComponentModel.ISupportInitialize)(this.pbStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbProgressBar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbStart;
        private System.Windows.Forms.PictureBox pbProgressBar;
        private System.Windows.Forms.Panel panel1;
    }
}
