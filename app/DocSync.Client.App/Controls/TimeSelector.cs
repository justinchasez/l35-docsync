using System;
using System.Windows.Forms;
using DocSync.Client.App.Core.Events.Args;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// Represents the time selector control.
    /// </summary>
    public partial class TimeSelector : UserControl
    {
        /// <summary>
        /// Event when time changed
        /// </summary>
        public event EventHandler<TimeEventArgs> TimeSelectChanged;

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public DateTime Value
        {
            get { return this.CalculateTime(); }
            set { this.SetTime(value); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeSelector"/> class.
        /// </summary>
        public TimeSelector()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Raise TimeSelectChanged event with current Value
        /// </summary>
        private void OnTimeSelectChanged()
        {
            if (this.TimeSelectChanged != null)
            {
                this.TimeSelectChanged(this, new TimeEventArgs(this.Value));
            }
        }

        /// <summary>
        /// Calculates the time.
        /// </summary>
        /// <returns>The current date time object with spoecified time.</returns>
        private DateTime CalculateTime()
        {
            DateTime date = DateTime.Today;

            int selectedHours = Convert.ToInt32(this.cmbHours.SelectedItem);
            int selectedMins = Convert.ToInt32(this.cmbMins.SelectedItem);

            if (this.rbtnPm.Checked && selectedHours != 12)
            {
                selectedHours += 12;
            }
            else if (this.rbtnAm.Checked && selectedHours == 12)
            {
                selectedHours = 0;
            }

            date = date.AddHours(selectedHours);
            date = date.AddMinutes(selectedMins);

            return date;
        }

        /// <summary>
        /// Sets the time.
        /// </summary>
        /// <param name="value">The value.</param>
        private void SetTime(DateTime value)
        {
            int hours = value.Hour;

            if (hours == 0)
            {
                this.rbtnAm.Checked = true;
                hours = 12;
            }
            else if (hours == 12)
            {
                this.rbtnPm.Checked = true;
            }
            else if (hours > 12)
            {
                hours -= 12;
                this.rbtnPm.Checked = true;
            }
            else
            {
                this.rbtnAm.Checked = true;
            }

            //if (value.Hour == 12)
            //{
            //    this.rbtnPm.Checked = true;
            //}
            //else if (value.Hour > 12)
            //{
            //    hours -= 12;
            //    this.rbtnPm.Checked = true;
            //}

            this.cmbHours.SelectedIndex = hours - 1;
            this.cmbMins.SelectedIndex = value.Minute / 5;
        }

        /// <summary>
        /// Event handler when SelectedValueChanged
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void CmbHoursSelectedValueChanged(object sender, EventArgs e)
        {
            this.OnTimeSelectChanged();
        }

        /// <summary>
        /// Event handler when SelectedValueChanged
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void CmbMinsSelectedValueChanged(object sender, EventArgs e)
        {
            this.OnTimeSelectChanged();
        }

        /// <summary>
        /// Event handler when SelectedValueChanged
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void RbtnAmCheckedChanged(object sender, EventArgs e)
        {
            this.OnTimeSelectChanged();
        }
    }
}
