using System;
using System.Windows.Forms;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Core.Entities.Computers;

namespace DocSync.Client.App.Controls
{
    /// <summary>
    /// The current computer info
    /// </summary>
    public partial class ComputerInfoRow : UserControl
    {
        /// <summary>
        /// The computer name;
        /// </summary>
        private readonly ComputerInfo computer;

        /// <summary>
        /// Occurs when [computer selected].
        /// </summary>
        public event Core.EventHandler<ComputerEventArgs> ComputerSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerInfoRow"/> class.
        /// </summary>
        public ComputerInfoRow()
        {
            this.InitializeComponent();

            this.Load += this.ComputerInfo_Load;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerInfoRow"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public ComputerInfoRow(ComputerInfo computer) : this()
        {
            this.computer = computer;
        }

        /// <summary>
        /// Handles the Load event of the ComputerInfo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ComputerInfo_Load(object sender, EventArgs e)
        {
            this.lblComputerName.Text = String.Format("{0} [expire on {1}]", this.computer.Name, this.computer.ExpirationDate.ToShortDateString());
        }

        /// <summary>
        /// Handles the Click event of the ComputerName control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void ComputerName_Click(object sender, EventArgs e)
        {
            this.OnComputerSelected(new ComputerEventArgs(this.computer));
        }

        /// <summary>
        /// Raises the <see cref="E:ComputerSelected"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ComputerEventArgs"/> instance containing the event data.</param>
        private void OnComputerSelected(ComputerEventArgs args)
        {
            Core.EventHandler<ComputerEventArgs> handler = this.ComputerSelected;
            if (handler != null)
            {
                handler(args);
            }
        }
    }
}
