using DocSync.Client.App.Controls;
using DocSync.Client.App.Controls.InnerForms;

namespace DocSync.Client.App
{
    public partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.Panel panel1;
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
			System.Windows.Forms.PictureBox pictureBox9;
			System.Windows.Forms.PictureBox pictureBox8;
			System.Windows.Forms.PictureBox pictureBox7;
			System.Windows.Forms.Panel panel3;
			this.ctrlRulesEditorView = new DocSync.Client.App.Controls.InnerForms.RulesEditorView();
			this.ctrlAdvancedSchedulingView = new DocSync.Client.App.Controls.InnerForms.AdvancedSchedulingView();
			this.ctrlSearchView = new DocSync.Client.App.Controls.InnerForms.SearchView();
			this.ctrlViewGetSupport = new DocSync.Client.App.Controls.InnerForms.GetSupportView();
			this.ctrlViewRestoreFiles = new DocSync.Client.App.Controls.InnerForms.RestoreFilesView();
			this.ctrlViewSetOptions = new DocSync.Client.App.Controls.InnerForms.SetOptionsView();
			this.ctrlViewViewStatus = new DocSync.Client.App.Controls.InnerForms.ViewStatusView();
			this.tabRestoreFiles = new DocSync.Client.App.Controls.SwitchControl();
			this.tabSetOptions = new DocSync.Client.App.Controls.SwitchControl();
			this.tabViewStatus = new DocSync.Client.App.Controls.SwitchControl();
			panel1 = new System.Windows.Forms.Panel();
			pictureBox9 = new System.Windows.Forms.PictureBox();
			pictureBox8 = new System.Windows.Forms.PictureBox();
			pictureBox7 = new System.Windows.Forms.PictureBox();
			panel3 = new System.Windows.Forms.Panel();
			panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabRestoreFiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSetOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViewStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox7)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			panel1.BackColor = System.Drawing.Color.Transparent;
			panel1.Controls.Add(this.ctrlRulesEditorView);
			panel1.Controls.Add(this.ctrlAdvancedSchedulingView);
			panel1.Controls.Add(this.ctrlSearchView);
			panel1.Controls.Add(this.ctrlViewGetSupport);
			panel1.Controls.Add(this.ctrlViewRestoreFiles);
			panel1.Controls.Add(this.ctrlViewSetOptions);
			panel1.Controls.Add(this.ctrlViewViewStatus);
			panel1.Controls.Add(this.tabRestoreFiles);
			panel1.Controls.Add(this.tabSetOptions);
			panel1.Controls.Add(this.tabViewStatus);
			panel1.Controls.Add(pictureBox9);
			panel1.Controls.Add(pictureBox8);
			panel1.Controls.Add(pictureBox7);
			panel1.Controls.Add(panel3);
			panel1.Location = new System.Drawing.Point(0, -1);
			panel1.Name = "panel1";
			panel1.Size = new System.Drawing.Size(525, 426);
			panel1.TabIndex = 4;
			// 
			// ctrlRulesEditorView
			// 
			this.ctrlRulesEditorView.Location = new System.Drawing.Point(8, 70);
			this.ctrlRulesEditorView.Name = "ctrlRulesEditorView";
			this.ctrlRulesEditorView.Size = new System.Drawing.Size(495, 338);
			this.ctrlRulesEditorView.TabIndex = 10;
			this.ctrlRulesEditorView.Visible = false;
			// 
			// ctrlAdvancedSchedulingView
			// 
			this.ctrlAdvancedSchedulingView.Location = new System.Drawing.Point(8, 70);
			this.ctrlAdvancedSchedulingView.Name = "ctrlAdvancedSchedulingView";
			this.ctrlAdvancedSchedulingView.Size = new System.Drawing.Size(495, 338);
			this.ctrlAdvancedSchedulingView.TabIndex = 9;
			this.ctrlAdvancedSchedulingView.Visible = false;
			// 
			// ctrlSearchView
			// 
			this.ctrlSearchView.Location = new System.Drawing.Point(8, 70);
			this.ctrlSearchView.Name = "ctrlSearchView";
			this.ctrlSearchView.Size = new System.Drawing.Size(495, 338);
			this.ctrlSearchView.TabIndex = 5;
			this.ctrlSearchView.Visible = false;
			// 
			// ctrlViewGetSupport
			// 
			this.ctrlViewGetSupport.Location = new System.Drawing.Point(8, 70);
			this.ctrlViewGetSupport.Name = "ctrlViewGetSupport";
			this.ctrlViewGetSupport.Size = new System.Drawing.Size(495, 338);
			this.ctrlViewGetSupport.TabIndex = 7;
			this.ctrlViewGetSupport.Visible = false;
			// 
			// ctrlViewRestoreFiles
			// 
			this.ctrlViewRestoreFiles.Location = new System.Drawing.Point(8, 70);
			this.ctrlViewRestoreFiles.Name = "ctrlViewRestoreFiles";
			this.ctrlViewRestoreFiles.Size = new System.Drawing.Size(495, 338);
			this.ctrlViewRestoreFiles.TabIndex = 6;
			this.ctrlViewRestoreFiles.Visible = false;
			// 
			// ctrlViewSetOptions
			// 
			this.ctrlViewSetOptions.Location = new System.Drawing.Point(8, 70);
			this.ctrlViewSetOptions.Name = "ctrlViewSetOptions";
			this.ctrlViewSetOptions.Size = new System.Drawing.Size(495, 338);
			this.ctrlViewSetOptions.TabIndex = 5;
			this.ctrlViewSetOptions.Visible = false;
			// 
			// ctrlViewViewStatus
			// 
			this.ctrlViewViewStatus.Location = new System.Drawing.Point(8, 70);
			this.ctrlViewViewStatus.Name = "ctrlViewViewStatus";
			this.ctrlViewViewStatus.Size = new System.Drawing.Size(495, 338);
			this.ctrlViewViewStatus.TabIndex = 4;
			// 
			// tabRestoreFiles
			// 
			this.tabRestoreFiles.HoverImage = null;
			this.tabRestoreFiles.Image = ((System.Drawing.Image)(resources.GetObject("tabRestoreFiles.Image")));
			this.tabRestoreFiles.IsSelected = false;
			this.tabRestoreFiles.Location = new System.Drawing.Point(347, 0);
			this.tabRestoreFiles.Name = "tabRestoreFiles";
			this.tabRestoreFiles.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabRestoreFiles.NormalImage")));
			this.tabRestoreFiles.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabRestoreFiles.SelectedImage")));
			this.tabRestoreFiles.Size = new System.Drawing.Size(178, 66);
			this.tabRestoreFiles.TabIndex = 3;
			this.tabRestoreFiles.TabStop = false;
			this.tabRestoreFiles.Click += new System.EventHandler(this.TabRestoreFiles_Click);
			// 
			// tabSetOptions
			// 
			this.tabSetOptions.HoverImage = null;
			this.tabSetOptions.Image = ((System.Drawing.Image)(resources.GetObject("tabSetOptions.Image")));
			this.tabSetOptions.IsSelected = false;
			this.tabSetOptions.Location = new System.Drawing.Point(169, 0);
			this.tabSetOptions.Name = "tabSetOptions";
			this.tabSetOptions.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabSetOptions.NormalImage")));
			this.tabSetOptions.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabSetOptions.SelectedImage")));
			this.tabSetOptions.Size = new System.Drawing.Size(178, 66);
			this.tabSetOptions.TabIndex = 3;
			this.tabSetOptions.TabStop = false;
			this.tabSetOptions.Click += new System.EventHandler(this.TabOptions_Click);
			// 
			// tabViewStatus
			// 
			this.tabViewStatus.HoverImage = null;
			this.tabViewStatus.Image = ((System.Drawing.Image)(resources.GetObject("tabViewStatus.Image")));
			this.tabViewStatus.IsSelected = false;
			this.tabViewStatus.Location = new System.Drawing.Point(-1, 0);
			this.tabViewStatus.Name = "tabViewStatus";
			this.tabViewStatus.NormalImage = ((System.Drawing.Image)(resources.GetObject("tabViewStatus.NormalImage")));
			this.tabViewStatus.SelectedImage = ((System.Drawing.Image)(resources.GetObject("tabViewStatus.SelectedImage")));
			this.tabViewStatus.Size = new System.Drawing.Size(170, 66);
			this.tabViewStatus.TabIndex = 1;
			this.tabViewStatus.TabStop = false;
			this.tabViewStatus.Click += new System.EventHandler(this.TabViewStatus_Click);
			// 
			// pictureBox9
			// 
			pictureBox9.Location = new System.Drawing.Point(553, 0);
			pictureBox9.Name = "pictureBox9";
			pictureBox9.Size = new System.Drawing.Size(4, 66);
			pictureBox9.TabIndex = 2;
			pictureBox9.TabStop = false;
			// 
			// pictureBox8
			// 
			pictureBox8.Location = new System.Drawing.Point(365, 0);
			pictureBox8.Name = "pictureBox8";
			pictureBox8.Size = new System.Drawing.Size(4, 66);
			pictureBox8.TabIndex = 2;
			pictureBox8.TabStop = false;
			// 
			// pictureBox7
			// 
			pictureBox7.Location = new System.Drawing.Point(182, 0);
			pictureBox7.Name = "pictureBox7";
			pictureBox7.Size = new System.Drawing.Size(4, 66);
			pictureBox7.TabIndex = 2;
			pictureBox7.TabStop = false;
			// 
			// panel3
			// 
			panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			panel3.Dock = System.Windows.Forms.DockStyle.Left;
			panel3.Location = new System.Drawing.Point(0, 0);
			panel3.Name = "panel3";
			panel3.Size = new System.Drawing.Size(18, 426);
			panel3.TabIndex = 0;
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.LightGray;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(525, 422);
			this.Controls.Add(panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Main";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DocSync";
			panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabRestoreFiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabSetOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tabViewStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(pictureBox7)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

		private SwitchControl tabViewStatus;
		private SwitchControl tabSetOptions;
        //private SwitchControl tabGetSupport;
        private SwitchControl tabRestoreFiles;
        private ViewStatusView ctrlViewViewStatus;
        private GetSupportView ctrlViewGetSupport;
        private RestoreFilesView ctrlViewRestoreFiles;
        private SetOptionsView ctrlViewSetOptions;
        private SearchView ctrlSearchView;
        private RulesEditorView ctrlRulesEditorView;
        private AdvancedSchedulingView ctrlAdvancedSchedulingView;
    }
}

