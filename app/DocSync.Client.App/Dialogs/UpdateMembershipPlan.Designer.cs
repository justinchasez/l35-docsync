using DocSync.Client.Controls.ImageButton;

namespace DocSync.Client.App.Dialogs
{
    partial class UpdateMembershipPlan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateMembershipPlan));
            this.btnLogin = new ImageButton();
            this.imageButton1 = new ImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnLogin
            // 
            this.btnLogin.AllowTransparency = false;
            this.btnLogin.AnimatePress = true;
            this.btnLogin.BackColor = System.Drawing.Color.Transparent;
            this.btnLogin.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLogin.HoverImage = null;
            this.btnLogin.Location = new System.Drawing.Point(293, 87);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonCancel;
            this.btnLogin.PressedImage = null;
            this.btnLogin.Size = new System.Drawing.Size(164, 33);
            this.btnLogin.TabIndex = 9;
            this.btnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // imageButton1
            // 
            this.imageButton1.AllowTransparency = false;
            this.imageButton1.AnimatePress = true;
            this.imageButton1.BackColor = System.Drawing.Color.Transparent;
            this.imageButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.imageButton1.HoverImage = null;
            this.imageButton1.Location = new System.Drawing.Point(123, 87);
            this.imageButton1.Name = "imageButton1";
            this.imageButton1.NormalImage = global::DocSync.Client.Controls.Properties.Resources.ButtonUpgrade;
            this.imageButton1.PressedImage = null;
            this.imageButton1.Size = new System.Drawing.Size(164, 33);
            this.imageButton1.TabIndex = 9;
            this.imageButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(431, 66);
            this.label1.TabIndex = 11;
            this.label1.Text = "Your membership plan doesn\'t allow adding additional PC.\r\nPlease go to upgrade yo" +
                "ur memebership or remove \r\nunnessesary PCs.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UpdateMembershipPlan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 132);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imageButton1);
            this.Controls.Add(this.btnLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateMembershipPlan";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Update membership plan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageButton btnLogin;
        private ImageButton imageButton1;
        private System.Windows.Forms.Label label1;
    }
}