using DocSync.Client.App.Controls.RestoreInnerForms;

namespace DocSync.Client.App.Dialogs
{
    partial class RestoreWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestoreWizard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.ctrlSummary = new Summary();
            this.ctrlChooseAccounts = new ChooseAccounts();
            this.ctrlRestoreModeSelect = new RestoreModeSelect();
            this.ctrlEncryptionKey = new EncryptionKey();
            this.ctrlChooseComputer = new ChooseComputer();
            this.ctrlStart = new Start();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.PanelBg300;
            this.panel1.Controls.Add(this.ctrlSummary);
            this.panel1.Controls.Add(this.ctrlChooseAccounts);
            this.panel1.Controls.Add(this.ctrlRestoreModeSelect);
            this.panel1.Controls.Add(this.ctrlEncryptionKey);
            this.panel1.Controls.Add(this.ctrlChooseComputer);
            this.panel1.Controls.Add(this.ctrlStart);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(629, 300);
            this.panel1.TabIndex = 0;
            // 
            // ctrlSummary
            // 
            this.ctrlSummary.BackColor = System.Drawing.Color.Transparent;
            this.ctrlSummary.Location = new System.Drawing.Point(7, 7);
            this.ctrlSummary.Name = "ctrlSummary";
            this.ctrlSummary.Size = new System.Drawing.Size(614, 280);
            this.ctrlSummary.TabIndex = 6;
            this.ctrlSummary.Visible = false;
            // 
            // ctrlChooseAccounts
            // 
            this.ctrlChooseAccounts.BackColor = System.Drawing.Color.Transparent;
            this.ctrlChooseAccounts.Location = new System.Drawing.Point(7, 7);
            this.ctrlChooseAccounts.Name = "ctrlChooseAccounts";
            this.ctrlChooseAccounts.Size = new System.Drawing.Size(614, 280);
            this.ctrlChooseAccounts.TabIndex = 5;
            this.ctrlChooseAccounts.Visible = false;
            // 
            // ctrlRestoreModeSelect
            // 
            this.ctrlRestoreModeSelect.BackColor = System.Drawing.Color.Transparent;
            this.ctrlRestoreModeSelect.Location = new System.Drawing.Point(7, 7);
            this.ctrlRestoreModeSelect.Name = "ctrlRestoreModeSelect";
            this.ctrlRestoreModeSelect.Size = new System.Drawing.Size(614, 280);
            this.ctrlRestoreModeSelect.TabIndex = 4;
            this.ctrlRestoreModeSelect.Visible = false;
            // 
            // ctrlEncryptionKey
            // 
            this.ctrlEncryptionKey.BackColor = System.Drawing.Color.Transparent;
            this.ctrlEncryptionKey.Location = new System.Drawing.Point(7, 7);
            this.ctrlEncryptionKey.Name = "ctrlEncryptionKey";
            this.ctrlEncryptionKey.Size = new System.Drawing.Size(614, 280);
            this.ctrlEncryptionKey.TabIndex = 3;
            this.ctrlEncryptionKey.Visible = false;
            // 
            // ctrlChooseComputer
            // 
            this.ctrlChooseComputer.BackColor = System.Drawing.Color.Transparent;
            this.ctrlChooseComputer.Location = new System.Drawing.Point(7, 7);
            this.ctrlChooseComputer.Name = "ctrlChooseComputer";
            this.ctrlChooseComputer.Size = new System.Drawing.Size(614, 280);
            this.ctrlChooseComputer.TabIndex = 2;
            this.ctrlChooseComputer.Visible = false;
            // 
            // ctrlStart
            // 
            this.ctrlStart.BackColor = System.Drawing.Color.Transparent;
            this.ctrlStart.Location = new System.Drawing.Point(7, 7);
            this.ctrlStart.Name = "ctrlStart";
            this.ctrlStart.Size = new System.Drawing.Size(614, 280);
            this.ctrlStart.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.PanelBgRight300;
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(611, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(18, 300);
            this.panel3.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::DocSync.Client.Controls.Properties.Resources.PanelBgLeft300;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(18, 300);
            this.panel2.TabIndex = 0;
            // 
            // RestoreWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(653, 323);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RestoreWizard";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Restore Files Wizard";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private Start ctrlStart;
        private RestoreModeSelect ctrlRestoreModeSelect;
        private EncryptionKey ctrlEncryptionKey;
        private ChooseComputer ctrlChooseComputer;
        private ChooseAccounts ctrlChooseAccounts;
        private Summary ctrlSummary;
    }
}