using System.Windows.Forms;

namespace DocSync.Client.App.Dialogs
{
    /// <summary>
    /// The upgrade membership notification popup.
    /// </summary>
    public partial class UpdateMembershipPlan : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateMembershipPlan"/> class.
        /// </summary>
        public UpdateMembershipPlan()
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.InitializeComponent();
        }
    }
}
