using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Core.Entities.Computers;
using DocSync.Client.Core.Entities.Encryption;

namespace DocSync.Client.App.Dialogs
{
    /// <summary>
    /// The restore files wizard.
    /// </summary>
    public partial class RestoreWizard : FormBase
    {
        /// <summary>
        /// The wizard views.
        /// </summary>
        private readonly Dictionary<RestoreWizardTab, WizardViewBase> views = new Dictionary<RestoreWizardTab, WizardViewBase>();

        /// <summary>
        /// The restore user mappings.
        /// </summary>
        private Dictionary<string, string> restoreUserMappings;

        /// <summary>
        /// The current selected view.
        /// </summary>
        private RestoreWizardTab selectedView = RestoreWizardTab.Start;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreWizard"/> class.
        /// </summary>
        public RestoreWizard()
        {
            this.InitializeComponent();

            this.Closed += this.RestoreWizard_Closed;
            this.Load += this.RestoreWizard_Load;

            EventPublisher.RestoreWizardEventManager.SwitchRestoreWizardView += this.RestoreWizardEventManager_SwitchRestoreWizardView;
        }

        /// <summary>
        /// Handles the Closed event of the RestoreWizard control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RestoreWizard_Closed(object sender, EventArgs e)
        {
            EventPublisher.RestoreWizardEventManager.RestoreWizardClose -= this.RestoreWizardEventManager_RestoreWizardClose;
            EventPublisher.RestoreWizardEventManager.RestoreWizardFinished -= this.RestoreWizardEventManager_RestoreWizardFinished;
        }

        /// <summary>
        /// Gets or sets the selected computer.
        /// </summary>
        /// <value>The selected computer.</value>
        internal ComputerInfo SelectedComputer { get; set; }

        /// <summary>
        /// Gets or sets the selected encryption key.
        /// </summary>
        /// <value>The selected encryption key.</value>
        internal string SelectedEncryptionKey { get; set; }

        /// <summary>
        /// Gets or sets the selected encryption info.
        /// </summary>
        /// <value>The selected encryption info.</value>
        internal EncryptInfo SelectedEncryptionInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [restore to default location].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [restore to default location]; otherwise, <c>false</c>.
        /// </value>
        internal bool RestoreToDefaultLocation { get; set; }

        /// <summary>
        /// Gets the backup user mapping.
        /// </summary>
        /// <value>The backup user mapping.</value>
        internal Dictionary<string, string> RestoreUserMappings
        {
            get
            {
                if (this.restoreUserMappings == null)
                {
                    this.restoreUserMappings = new Dictionary<string, string>();
                }

                return this.restoreUserMappings;
            }
        }

        /// <summary>
        /// Handles the Load event of the RestoreWizard control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RestoreWizard_Load(object sender, EventArgs e)
        {
            this.views.Clear();

            this.views.Add(RestoreWizardTab.Start, this.ctrlStart);
            this.views.Add(RestoreWizardTab.ComputerSelect, this.ctrlChooseComputer);
            this.views.Add(RestoreWizardTab.AccountSelect, this.ctrlChooseAccounts);
            this.views.Add(RestoreWizardTab.EncryptionKey, this.ctrlEncryptionKey);
            this.views.Add(RestoreWizardTab.RestoreMode, this.ctrlRestoreModeSelect);
            this.views.Add(RestoreWizardTab.Summary, this.ctrlSummary);

            this.ctrlSummary.RestoreWizardForm = this;

            EventPublisher.RestoreWizardEventManager.RestoreWizardClose += this.RestoreWizardEventManager_RestoreWizardClose;
            EventPublisher.RestoreWizardEventManager.RestoreWizardFinished += this.RestoreWizardEventManager_RestoreWizardFinished;
        }

        /// <summary>
        /// Restores the wizard event manager_ switch restore wizard view.
        /// </summary>
        /// <param name="args">The <see cref="SwitchRestoreWizardTabEventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_SwitchRestoreWizardView(SwitchRestoreWizardTabEventArgs args)
        {
            if (this.selectedView != args.SelectedTab)
            {
                this.SwitchTab(args.SelectedTab);
            }
        }

        /// <summary>
        /// Restores the wizard event manager_ restore wizard close.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_RestoreWizardClose(CancelEventArgs args)
        {
            if (MessageBox.Show("Do you really want to close the restore wizard?", "Restore wizard", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                == DialogResult.Yes)
            {
                this.Close();
            }
        }

        /// <summary>
        /// Handles the RestoreWizardFinished event of the RestoreWizardEventManager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void RestoreWizardEventManager_RestoreWizardFinished(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="viewTab">The view tab.</param>
        private void SwitchTab(RestoreWizardTab viewTab)
        {
            this.views[this.selectedView].Visible = false;
            this.views[viewTab].Visible = true;

            this.selectedView = viewTab;
        }
    }
}
