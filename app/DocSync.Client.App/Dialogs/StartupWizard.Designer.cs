using DocSync.Client.App.Controls.StartupInnerForms;

namespace DocSync.Client.App.Dialogs
{
    public partial class StartupContainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartupContainer));
            this.panel1 = new System.Windows.Forms.Panel();
            this.ctrlLogin = new Login();
            this.ctrlComputerManagement = new ComputerManagement();
            this.imgArrow3 = new System.Windows.Forms.PictureBox();
            this.imgArrow2 = new System.Windows.Forms.PictureBox();
            this.imgArrow1 = new System.Windows.Forms.PictureBox();
            this.imgFinish = new System.Windows.Forms.PictureBox();
            this.imgEncryption = new System.Windows.Forms.PictureBox();
            this.imgComputers = new System.Windows.Forms.PictureBox();
            this.imgUsers = new System.Windows.Forms.PictureBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ctrlValidateEncryptionKey = new ValidateEncryptionKey();
            this.ctrlAddNewComputer = new AddNewComputer();
            this.ctrlFinish = new Finish();
            this.ctrlEncryptionKey = new EncryptionKey();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFinish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEncryption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgComputers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.ctrlLogin);
            this.panel1.Controls.Add(this.ctrlComputerManagement);
            this.panel1.Controls.Add(this.imgArrow3);
            this.panel1.Controls.Add(this.imgArrow2);
            this.panel1.Controls.Add(this.imgArrow1);
            this.panel1.Controls.Add(this.imgFinish);
            this.panel1.Controls.Add(this.imgEncryption);
            this.panel1.Controls.Add(this.imgComputers);
            this.panel1.Controls.Add(this.imgUsers);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.ctrlValidateEncryptionKey);
            this.panel1.Controls.Add(this.ctrlAddNewComputer);
            this.panel1.Controls.Add(this.ctrlFinish);
            this.panel1.Controls.Add(this.ctrlEncryptionKey);
            this.panel1.Location = new System.Drawing.Point(5, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(726, 486);
            this.panel1.TabIndex = 0;
            // 
            // ctrlLogin
            // 
            this.ctrlLogin.BackColor = System.Drawing.Color.Transparent;
            this.ctrlLogin.Location = new System.Drawing.Point(24, 81);
            this.ctrlLogin.Name = "ctrlLogin";
            this.ctrlLogin.Size = new System.Drawing.Size(678, 384);
            this.ctrlLogin.TabIndex = 9;
            // 
            // ctrlComputerManagement
            // 
            this.ctrlComputerManagement.BackColor = System.Drawing.Color.Transparent;
            this.ctrlComputerManagement.Location = new System.Drawing.Point(24, 81);
            this.ctrlComputerManagement.Name = "ctrlComputerManagement";
            this.ctrlComputerManagement.Size = new System.Drawing.Size(678, 324);
            this.ctrlComputerManagement.TabIndex = 8;
            this.ctrlComputerManagement.Visible = false;
            // 
            // imgArrow3
            // 
            this.imgArrow3.BackColor = System.Drawing.Color.Transparent;
            this.imgArrow3.Image = ((System.Drawing.Image)(resources.GetObject("imgArrow3.Image")));
            this.imgArrow3.Location = new System.Drawing.Point(496, 31);
            this.imgArrow3.Name = "imgArrow3";
            this.imgArrow3.Size = new System.Drawing.Size(47, 22);
            this.imgArrow3.TabIndex = 7;
            this.imgArrow3.TabStop = false;
            // 
            // imgArrow2
            // 
            this.imgArrow2.BackColor = System.Drawing.Color.Transparent;
            this.imgArrow2.Image = ((System.Drawing.Image)(resources.GetObject("imgArrow2.Image")));
            this.imgArrow2.Location = new System.Drawing.Point(334, 31);
            this.imgArrow2.Name = "imgArrow2";
            this.imgArrow2.Size = new System.Drawing.Size(47, 22);
            this.imgArrow2.TabIndex = 7;
            this.imgArrow2.TabStop = false;
            // 
            // imgArrow1
            // 
            this.imgArrow1.BackColor = System.Drawing.Color.Transparent;
            this.imgArrow1.Image = ((System.Drawing.Image)(resources.GetObject("imgArrow1.Image")));
            this.imgArrow1.Location = new System.Drawing.Point(163, 31);
            this.imgArrow1.Name = "imgArrow1";
            this.imgArrow1.Size = new System.Drawing.Size(47, 22);
            this.imgArrow1.TabIndex = 7;
            this.imgArrow1.TabStop = false;
            // 
            // imgFinish
            // 
            this.imgFinish.BackColor = System.Drawing.Color.Transparent;
            this.imgFinish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgFinish.Image = ((System.Drawing.Image)(resources.GetObject("imgFinish.Image")));
            this.imgFinish.Location = new System.Drawing.Point(577, 11);
            this.imgFinish.Name = "imgFinish";
            this.imgFinish.Size = new System.Drawing.Size(91, 64);
            this.imgFinish.TabIndex = 6;
            this.imgFinish.TabStop = false;
            // 
            // imgEncryption
            // 
            this.imgEncryption.BackColor = System.Drawing.Color.Transparent;
            this.imgEncryption.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgEncryption.Image = ((System.Drawing.Image)(resources.GetObject("imgEncryption.Image")));
            this.imgEncryption.Location = new System.Drawing.Point(405, 11);
            this.imgEncryption.Name = "imgEncryption";
            this.imgEncryption.Size = new System.Drawing.Size(64, 64);
            this.imgEncryption.TabIndex = 6;
            this.imgEncryption.TabStop = false;
            // 
            // imgComputers
            // 
            this.imgComputers.BackColor = System.Drawing.Color.Transparent;
            this.imgComputers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgComputers.Image = ((System.Drawing.Image)(resources.GetObject("imgComputers.Image")));
            this.imgComputers.Location = new System.Drawing.Point(234, 11);
            this.imgComputers.Name = "imgComputers";
            this.imgComputers.Size = new System.Drawing.Size(64, 64);
            this.imgComputers.TabIndex = 6;
            this.imgComputers.TabStop = false;
            // 
            // imgUsers
            // 
            this.imgUsers.BackColor = System.Drawing.Color.Transparent;
            this.imgUsers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.imgUsers.Image = ((System.Drawing.Image)(resources.GetObject("imgUsers.Image")));
            this.imgUsers.Location = new System.Drawing.Point(78, 11);
            this.imgUsers.Name = "imgUsers";
            this.imgUsers.Size = new System.Drawing.Size(64, 64);
            this.imgUsers.TabIndex = 6;
            this.imgUsers.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Gray;
            this.panel6.Location = new System.Drawing.Point(2, 76);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(721, 1);
            this.panel6.TabIndex = 5;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Location = new System.Drawing.Point(2, 77);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(721, 1);
            this.panel7.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(708, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(18, 486);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(18, 486);
            this.panel2.TabIndex = 0;
            // 
            // ctrlValidateEncryptionKey
            // 
            this.ctrlValidateEncryptionKey.BackColor = System.Drawing.Color.Transparent;
            this.ctrlValidateEncryptionKey.Location = new System.Drawing.Point(24, 81);
            this.ctrlValidateEncryptionKey.Name = "ctrlValidateEncryptionKey";
            this.ctrlValidateEncryptionKey.Size = new System.Drawing.Size(678, 324);
            this.ctrlValidateEncryptionKey.TabIndex = 13;
            this.ctrlValidateEncryptionKey.Visible = false;
            // 
            // ctrlAddNewComputer
            // 
            this.ctrlAddNewComputer.BackColor = System.Drawing.Color.Transparent;
            this.ctrlAddNewComputer.Location = new System.Drawing.Point(24, 81);
            this.ctrlAddNewComputer.Name = "ctrlAddNewComputer";
            this.ctrlAddNewComputer.Size = new System.Drawing.Size(678, 324);
            this.ctrlAddNewComputer.TabIndex = 12;
            this.ctrlAddNewComputer.Visible = false;
            // 
            // ctrlFinish
            // 
            this.ctrlFinish.BackColor = System.Drawing.Color.Transparent;
            this.ctrlFinish.Location = new System.Drawing.Point(24, 81);
            this.ctrlFinish.Name = "ctrlFinish";
            this.ctrlFinish.Size = new System.Drawing.Size(678, 324);
            this.ctrlFinish.TabIndex = 11;
            this.ctrlFinish.Visible = false;
            // 
            // ctrlEncryptionKey
            // 
            this.ctrlEncryptionKey.BackColor = System.Drawing.Color.Transparent;
            this.ctrlEncryptionKey.Location = new System.Drawing.Point(24, 81);
            this.ctrlEncryptionKey.Name = "ctrlEncryptionKey";
            this.ctrlEncryptionKey.Size = new System.Drawing.Size(678, 324);
            this.ctrlEncryptionKey.TabIndex = 10;
            this.ctrlEncryptionKey.Visible = false;
            // 
            // StartupContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(734, 494);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StartupContainer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "First run wizard";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgArrow1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgFinish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEncryption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgComputers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox imgUsers;
        private System.Windows.Forms.PictureBox imgFinish;
        private System.Windows.Forms.PictureBox imgEncryption;
        private System.Windows.Forms.PictureBox imgComputers;
        private System.Windows.Forms.PictureBox imgArrow3;
        private System.Windows.Forms.PictureBox imgArrow2;
        private System.Windows.Forms.PictureBox imgArrow1;
        private Finish ctrlFinish;
        private EncryptionKey ctrlEncryptionKey;
        private Login ctrlLogin;
        private ComputerManagement ctrlComputerManagement;
        private AddNewComputer ctrlAddNewComputer;
        private ValidateEncryptionKey ctrlValidateEncryptionKey;

    }
}