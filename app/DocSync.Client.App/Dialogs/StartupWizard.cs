using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocSync.Client.App.Controls.Base;
using DocSync.Client.App.Core;
using DocSync.Client.App.Core.Enums;
using DocSync.Client.App.Core.Events.Args;
using DocSync.Client.Core.Entities.Computers;
using DocSync.Client.Core.Entities.Encryption;

namespace DocSync.Client.App.Dialogs
{
    /// <summary>
    /// The Startup container form.
    /// </summary>
    public partial class StartupContainer : FormBase
    {
        /// <summary>
        /// The tabs collection.
        /// </summary>
        private readonly Dictionary<WizardViewTab, UserControl> tabs = new Dictionary<WizardViewTab, UserControl>();

        /// <summary>
        /// The current selected tab.
        /// </summary>
        private WizardViewTab selectedTab = WizardViewTab.Users;

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        /// <value>The user id.</value>
        internal int UserId { get; set; }

        /// <summary>
        /// Gets or sets the user email.
        /// </summary>
        /// <value>The user email.</value>
        internal string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets the user password.
        /// </summary>
        /// <value>The user password.</value>
        internal string UserPassword { get; set; }

        /// <summary>
        /// Gets or sets the selected computer.
        /// </summary>
        /// <value>The selected computer.</value>
        internal ComputerInfo SelectedComputer { get; set; }

        /// <summary>
        /// Gets or sets the selected computer encrypt info.
        /// </summary>
        /// <value>The selected computer encrypt info.</value>
        internal EncryptInfo SelectedComputerEncryptInfo { get; set; }

        /// <summary>
        /// Gets or sets the computers.
        /// </summary>
        /// <value>The computers.</value>
        internal List<ComputerInfo> Computers { get; set; }

        /// <summary>
        /// Initializes a new instance of the StartupContainer class.
        /// </summary>
        public StartupContainer()
        {
            this.InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.ContainerControl, true);

            this.Load += this.StartupContainer_Load;
        }

        /// <summary>
        /// Handles the Load event of the StartupContainer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void StartupContainer_Load(object sender, EventArgs e)
        {
            this.tabs.Add(WizardViewTab.Users, this.ctrlLogin);
            this.tabs.Add(WizardViewTab.Computers, this.ctrlComputerManagement);
            this.tabs.Add(WizardViewTab.NewComputer, this.ctrlAddNewComputer);
            this.tabs.Add(WizardViewTab.EncryptionKey, this.ctrlEncryptionKey);
            this.tabs.Add(WizardViewTab.ValidateEncryptionKey, this.ctrlValidateEncryptionKey);
            this.tabs.Add(WizardViewTab.Finish, this.ctrlFinish);

            EventPublisher.WizardEventManager.SwitchWizardTab += this.WizardEventManager_SwitchWizardTab;
        }

        /// <summary>
        /// Wizards the event manager_ switch wizard tab.
        /// </summary>
        /// <param name="args">The <see cref="SwitchWizardTabEventArgs"/> instance containing the event data.</param>
        private void WizardEventManager_SwitchWizardTab(SwitchWizardTabEventArgs args)
        {
            if (this.selectedTab != args.SelectedTab)
            {
                this.SwitchTab(args.SelectedTab);
            }
        }

        /// <summary>
        /// Switches the tab.
        /// </summary>
        /// <param name="viewTab">The view tab.</param>
        private void SwitchTab(WizardViewTab viewTab)
        {
            this.tabs[this.selectedTab].Visible = false;
            this.tabs[viewTab].Visible = true;

            this.selectedTab = viewTab;

            int tabOrder = (int)viewTab;

			this.imgArrow1.Image = tabOrder >= 2 ? DocSync.Client.Controls.Properties.Resources.Arrow : DocSync.Client.Controls.Properties.Resources.ArrowDisabled;
			this.imgArrow2.Image = tabOrder >= 4 ? DocSync.Client.Controls.Properties.Resources.Arrow : DocSync.Client.Controls.Properties.Resources.ArrowDisabled;
			this.imgArrow3.Image = tabOrder >= 6 ? DocSync.Client.Controls.Properties.Resources.Arrow : DocSync.Client.Controls.Properties.Resources.ArrowDisabled;

			this.imgComputers.Image = tabOrder >= 2 ? DocSync.Client.Controls.Properties.Resources.WizardStageComputers : DocSync.Client.Controls.Properties.Resources.WizardStageComputersDisabled;
			this.imgEncryption.Image = tabOrder >= 4 ? DocSync.Client.Controls.Properties.Resources.WizardEncryptionKey : DocSync.Client.Controls.Properties.Resources.WizardEncryptionKeyDisabled;
			this.imgFinish.Image = tabOrder >= 6 ? DocSync.Client.Controls.Properties.Resources.WizardStageFinish : DocSync.Client.Controls.Properties.Resources.WizardStageFinishDisabled;
        }
    }
}