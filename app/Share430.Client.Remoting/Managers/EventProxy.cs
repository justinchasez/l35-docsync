using System;
using System.Collections.Generic;
using Share430.Client.Core.Args;
using Share430.Client.Remoting.Args;
using Share430.Client.Remoting.Handlers;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Remoting.Managers
{
    /// <summary>
    /// The remote event publisher.
    /// </summary>
    [Serializable]
    public class EventProxy : MarshalByRefObject, IProxyEventPublisher
    {
        /// <summary>
        /// Handle window to notify events
        /// </summary>
        public int NotifyWindow { get; set; }

        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        /// <summary>
        /// Smart Raise event, remove invalid dleegates
        /// </summary>
        /// <typeparam name="T">type of event argument</typeparam>
        /// <param name="eventHandler">event handler</param>
        /// <param name="args">event argument</param>
        private void RaiseRemotingEvent<T>(ref RemotingEventHandler<T> eventHandler, T args)
        {
            if (eventHandler != null)
            {
                List<Delegate> invalidDelegates = new List<Delegate>();

                foreach (Delegate invokeDelegate in eventHandler.GetInvocationList())
                {
                    try
                    {
                        invokeDelegate.DynamicInvoke(args);
                    }
                    catch
                    {
                        invalidDelegates.Add(invokeDelegate);
                    }
                }

                foreach (Delegate invalidDelegate in invalidDelegates)
                {
                    eventHandler -= (RemotingEventHandler<T>)invalidDelegate;
                }
            }
        }

        /// <summary>
        /// Smart Raise event, remove invalid dleegates
        /// </summary>
        /// <typeparam name="T">type of event argument</typeparam>
        /// <param name="eventHandler">event handler</param>
        /// <param name="args">event argument</param>
        private void RaiseEvent<T>(ref EventHandler<T> eventHandler, T args) where T : EventArgs
        {
            if (eventHandler != null)
            {
                List<Delegate> invalidDelegates = new List<Delegate>();
                foreach (Delegate invokeDelegate in eventHandler.GetInvocationList())
                {
                    try
                    {
                        invokeDelegate.DynamicInvoke(new object[]
                                                         {
                                                             this,
                                                             args
                                                         });
                    }
                    catch (Exception)
                    {
                        invalidDelegates.Add(invokeDelegate);
                    }
                }

                foreach (Delegate invalidDelegate in invalidDelegates)
                {
                    eventHandler -= (EventHandler<T>)invalidDelegate;
                }
            }
        }

        /// <summary>
        /// Occurs when [SetOption message received].
        /// </summary>
        public event RemotingEventHandler<SetOptionsEventArgs> OptionChanged;

        /// <summary>
        /// Raises the <see cref="E:SetOption"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        public void OnOptionChanged(SetOptionsEventArgs args)
        {
            RaiseRemotingEvent(ref this.OptionChanged, args);
        }

        /// <summary>
        /// Occurs when [message received].
        /// </summary>
        public event RemotingEventHandler<MessageEventArgs> MessageReceived;

        /// <summary>
        /// Raises the <see cref="E:MessageReceived"/> event.
        /// </summary>
        /// <param name="args">The <see cref="MessageEventArgs"/> instance containing the event data.</param>
        public void OnMessageReceived(MessageEventArgs args)
        {
            RaiseRemotingEvent(ref this.MessageReceived, args);
        }

        /// <summary>
        /// Occurs when [backup status changed].
        /// </summary>
        public event RemotingEventHandler<BackupStatusChangedEventArgs> BackupStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="BackupStatusChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupStatusChanged(BackupStatusChangedEventArgs args)
        {
            RaiseRemotingEvent(ref this.BackupStatusChanged, args);
        }

        /// <summary>
        /// Occurs when [backup progress changed].
        /// </summary>
        public event RemotingEventHandler<ProgressChangedEventArgs> BackupProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupProgressChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ProgressChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupProgressChanged(ProgressChangedEventArgs args)
        {
            RaiseRemotingEvent(ref this.BackupProgressChanged, args);
        }

        /// <summary>
        /// Occurs when [restore status changed].
        /// </summary>
        public event RemotingEventHandler<RestoreStatusChangedEventArgs> RestoreStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="RestoreStatusChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreStatusChanged(RestoreStatusChangedEventArgs args)
        {
            RaiseRemotingEvent(ref this.RestoreStatusChanged, args);
        }

        /// <summary>
        /// Occurs when [restore progress changed].
        /// </summary>
        public event RemotingEventHandler<ProgressChangedEventArgs> RestoreProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreProgressChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="RestoreProgressChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreProgressChanged(ProgressChangedEventArgs args)
        {
            RaiseRemotingEvent(ref this.RestoreProgressChanged, args);
        }

        /// <summary>
        /// Occurs when [application state changed].
        /// </summary>
        public event EventHandler<ApplicationStateChangedEventArgs> ApplicationStateChanged;

        /// <summary>
        /// Raises the <see cref="E:ApplicationStateChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="ApplicationStateChangedEventArgs"/> instance containing the event data.</param>
        public void OnApplicationStateChanged(ApplicationStateChangedEventArgs e)
        {
            RaiseEvent(ref this.ApplicationStateChanged, e);
        }

        /// <summary>
        /// Occurs when [application mode changed].
        /// </summary>
        public event EventHandler<ApplicationModeChangedEventArgs> ApplicationModeChanged;

        /// <summary>
        /// Raises the <see cref="E:ApplicationModeChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="ApplicationModeChangedEventArgs"/> instance containing the event data.</param>
        public void OnApplicationModeChanged(ApplicationModeChangedEventArgs e)
        {
            RaiseEvent(ref this.ApplicationModeChanged, e);
        }

        /// <summary>
        /// Occurs when [connection priority changed].
        /// </summary>
        public event EventHandler<ConnectionPriorityChangedEventArgs> ConnectionPriorityChanged;

        /// <summary>
        /// Raises the <see cref="E:ConnectionPriorityChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="ConnectionPriorityChangedEventArgs"/> instance containing the event data.</param>
        public void OnConnectionPriorityChanged(ConnectionPriorityChangedEventArgs e)
        {
            RaiseEvent(ref this.ConnectionPriorityChanged, e);
        }

        /// <summary>
        /// Occurs when [paused].
        /// </summary>
        public event EventHandler<PauseEventArgs> Paused;

        /// <summary>
        /// Raises the <see cref="E:Paused"/> event.
        /// </summary>
        /// <param name="e">The <see cref="PauseEventArgs"/> instance containing the event data.</param>
        public void OnPaused(PauseEventArgs e)
        {
            RaiseEvent(ref this.Paused, e);
        }

        /// <summary>
        /// Occurs when [RestorePreviousVersion].
        /// </summary>
        public event EventHandler<RestoreFileEventArgs> RestorePreviousVersion;

        /// <summary>
        /// Raises the <see cref="E:RestorePreviousVersion"/> event.
        /// </summary>
        /// <param name="args">The <see cref="Share430.Client.Remoting.Args.RestoreFileEventArgs"/> instance containing the event data.</param>
        public void OnRestorePreviousVersion(RestoreFileEventArgs args)
        {
            this.RaiseEvent(ref this.RestorePreviousVersion, args);
        }

        /// <summary>
        /// Occurs when [restore speed changed].
        /// </summary>
        public event RemotingEventHandler<SpeedChangedEventArgs> RestoreSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreSpeedChanged(SpeedChangedEventArgs args)
        {
            this.RaiseRemotingEvent(ref this.RestoreSpeedChanged, args);
        }

        /// <summary>
        /// Occurs when [backup speed changed].
        /// </summary>
        public event RemotingEventHandler<SpeedChangedEventArgs> BackupSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupSpeedChanged(SpeedChangedEventArgs args)
        {
            this.RaiseRemotingEvent(ref this.BackupSpeedChanged, args);
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}