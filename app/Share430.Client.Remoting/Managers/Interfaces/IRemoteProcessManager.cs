using System;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote process manager.
    /// </summary>
    public interface IRemoteProcessManager : IDisposable
    {
        /// <summary>
        /// Starts the process.
        /// </summary>
        /// <param name="appPath">The app path.</param>
        /// <param name="params">The inline params.</param>
        void StartProcess(string appPath, string @params);
    }
}