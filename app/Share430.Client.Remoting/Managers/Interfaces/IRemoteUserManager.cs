using Share430.Client.Core.Entities.Users;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote user manager.
    /// </summary>
    public interface IRemoteUserManager
    {
        /// <summary>
        /// Registers the user.
        /// </summary>
        /// <param name="userInfo">The user info.</param>
        /// <returns>The user registration result.</returns>
        UserRegistrationResult RegisterUser(UserRegistrationInputs userInfo);
        
        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns>The user ID in validation success, otherwise Guid.Empty.</returns>
        bool ValidateUser(string email, string password, out string token);
    }
}