using System;
using System.Collections.Generic;
using Share430.Client.Core.Entities.Schedule.Interface;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remoting schedule manager.
    /// </summary>
    public interface IRemoteScheduleManager : IDisposable
    {
        /// <summary>
        /// Is Backup ASAP
        /// </summary>
        bool IsASAP { get; set; }

        /// <summary>
        /// Get or set is schedules changed
        /// </summary>
        bool IsSchedulesChanged { get; set; }

        /// <summary>
        /// Get schedules.
        /// </summary>
        Dictionary<Guid, ISchedule> Schedules { get; }

        /// <summary>
        /// Initializes the schedules.
        /// </summary>
        /// <returns>The list of existing schedules.</returns>
        Dictionary<Guid, ISchedule> InitializeSchedules();

        /// <summary>
        /// Saves the schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        void SaveSchedule(ISchedule schedule);

        /// <summary>
        /// Removes the schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        void RemoveSchedule(ISchedule schedule);
    }
}