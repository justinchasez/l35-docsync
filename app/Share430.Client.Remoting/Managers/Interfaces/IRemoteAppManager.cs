using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote application manager.
    /// </summary>
    public interface IRemoteAppManager : IDisposable
    {
        /// <summary>
        /// Gets or sets the syncronization manager.
        /// </summary>
        /// <value>The syncronization manager.</value>
        ISyncronizationManager SyncronizationManager { get; set; }

        /// <summary>
        /// Link to last version of client application
        /// </summary>
        string LinkToLastVersionApp { get; }

        /// <summary>
        /// Gets the last version of client application
        /// </summary>
        string LastClientAppVersion { get; }

        /// <summary>
        /// Show dialog to restore previous version of file
        /// </summary>
        /// <param name="fullPath">full path to the file</param>
        void ShowRestorePrevVersion(string fullPath);

        /// <summary>
        /// Gets the state of the application enabled.
        /// </summary>
        /// <returns>True if application enabled, otherwise false.</returns>
        bool GetApplicationEnabledState();

        /// <summary>
        /// Gets the application recovery mode.
        /// </summary>
        /// <returns>True if application in recovery mode, otherwise false.</returns>
        bool GetApplicationRecoveryMode();

        /// <summary>
        /// Gets the connectionpriority.
        /// </summary>
        /// <returns>The connection priority</returns>
        ConnectionPriority GetConnectionpriority();

        /// <summary>
        /// Gets the pause.
        /// </summary>
        /// <returns>The minutes application paused for.</returns>
        int GetPause();

        /// <summary>
        /// Sets the state of the application.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        void SetApplicationState(bool enabled);

        /// <summary>
        /// Sets the application recovery mode.
        /// </summary>
        /// <param name="isRecoveryMode">if set to <c>true</c> [is recovery mode].</param>
        void SetApplicationRecoveryMode(bool isRecoveryMode);

        /// <summary>
        /// Sets the connectionpriority.
        /// </summary>
        /// <param name="priority">The priority.</param>
        void SetConnectionpriority(ConnectionPriority priority);

        /// <summary>
        /// Sets the pause.
        /// </summary>
        /// <param name="minutes">The minutes.</param>
        void SetPause(int minutes);

        /// <summary>
        /// Disable Property, when property is set the app will not backup pending files
        /// </summary>
        bool Disable { get; set; }

        /// <summary>
        /// Gets the app synchronization object.
        /// </summary>
        /// <value>The app synchronization object.</value>
        object AppSyncObject { get; }
    }
}