using System;
using System.Collections.Generic;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote help manager.
    /// </summary>
    public interface IRemoteHelpManager : IDisposable
    {
        /// <summary>
        /// Gets the help topics.
        /// </summary>
        /// <returns>The list of help topics.</returns>
        List<string> GetHelpTopics();
    }
}