using System;
using System.Collections.Generic;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Remoting.Args;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// Delegate for update XML
    /// </summary>
    /// <param name="files">List of files from the server</param>
    public delegate void GetFilesFromServerCompleteDelegate(ComputerFilesDto files);

    /// <summary>
    /// The remoting backup session manager.
    /// </summary>
    public interface IRemoteBackupManager : IDisposable
    {
        /// <summary>
        /// Get files from server complete event
        /// </summary>
        event GetFilesFromServerCompleteDelegate GetFilesFromServerComplete;

        /// <summary>
        /// Gets the backup progress.
        /// </summary>
        /// <returns>The backup progress.</returns>
        Progress GetBackupProgress();

        /// <summary>
        /// Raise backup progress changed
        /// </summary>
        void RaiseBackupProgressChanged();

        /// <summary>
        /// Get current backup status
        /// </summary>
        /// <returns>The backup status</returns>
        BackupStatusChangedEventArgs GetBackupStatus();

        /// <summary>
        /// Gets the restore progress.
        /// </summary>
        /// <returns>The restore progress.</returns>
        Progress GetRestoreProgress();

        /// <summary>
        /// Gets the restore status.
        /// </summary>
        /// <returns>The restore status</returns>
        RestoreStatus GetRestoreStatus();

        /// <summary>
        /// Gets current restoring file
        /// </summary>
        /// <returns>current restoring file</returns>
        RestoreFileQueueItem GetCurrentRestoreFile();

        /// <summary>
        /// Gets current backuping file
        /// </summary>
        /// <returns>current backuping file</returns>
        FileQueueItem GetCurrentBackupFile();

        /// <summary>
        /// Backup this file asap, move to the top of queue
        /// </summary>
        /// <param name="filePath">full path to the file</param>
        void BackupFileAsap(string filePath);

        /// <summary>
        /// Adds the file to backup.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        void AddFileToBackup(string filePath, bool force);

        /// <summary>
        /// Add files to backup
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        /// <param name="force">if set to <c>true</c> [force]</param>
        void AddFileCollectionToBackup(string[] filePathes, bool force);

        /// <summary>
        /// Rename files in Backup
        /// </summary>
        /// <param name="files">Dictionary of file names</param>
        void RenameFilesInBackup(Dictionary<string, string> files);

        /// <summary>
        /// Remove file from queue to backup
        /// </summary>
        /// <param name="filePath">The file path</param>
        void RemoveFileFromQueueToBackup(string filePath);

        /// <summary>
        /// Remove files from queue to backup
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        void RemoveFileCollectionFromFromQueueToBackup(string[] filePathes);

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="restoreFilePath">The restore file path.</param>
        /// <param name="restoreVersion">The restore version.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        void RestoreFile(string filePath, string restoreFilePath, int restoreVersion, bool force);

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="restoreFilePath">The restore file path.</param>
        /// <param name="restoreVersion">The restore version.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        void RestoreFile(int computerId, EncryptInfo encryptKey, string filePath, string restoreFilePath, int restoreVersion, bool force);

        /// <summary>
        /// Set pause for upload
        /// </summary>
        void SetPause();

        /// <summary>
        /// Continue upload
        /// </summary>
        void UnsetPause();

        /// <summary>
        /// Searches the files.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns>The list of files.</returns>
        List<string> SearchFiles(string pattern);

        /// <summary>
        /// Restores the computer.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        void RestoreComputer(int computerId, EncryptInfo encryptKey);

        /// <summary>
        /// Cancels the restoring.
        /// </summary>
        void CancelRestoring();

        /// <summary>
        /// Restores the documents.
        /// </summary>
        /// <param name="originalUser">The original user.</param>
        /// <param name="targetUser">The target user.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        void RestoreDocuments(SyncronizeUsersEntry originalUser, SyncronizeUsersEntry targetUser, int computerId, EncryptInfo encryptKey);

        /// <summary>
        /// Adds the existing file to backup.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="oldFilePath">The old file path.</param>
        /// <param name="newFilePath">The new file path.</param>
        /// <returns>Backup file result</returns>
        BackupFileResultModel AddExistingFileToBackup(int computerId, string oldFilePath, string newFilePath);

        /// <summary>
        /// Sets the file priority.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="priority">The priority.</param>
        /// <returns>Results of operation.</returns>
        bool SetFilePriority(string filePath, RestorePriority priority);

        /// <summary>
        /// Cancels the restore file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        void CancelRestoreFile(string filePath);

        /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="computerId">The computer id.</param>
        /// <returns>Returns file info.</returns>
        FileModel GetFileInfo(string filePath, int computerId);

        /// <summary>
        /// Get files from server for XML
        /// </summary>
        /// <param name="compId">Computer ID</param>
        void GetFilesFromServer();

        DateTime UpdateFilesFromServer(DateTime lastUpdate, EncryptInfo encryptKey);

        /// <summary>
        /// Remove file from S3
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <returns>Is successed</returns>
        bool RemoveFileFromS3(string filePath);

        /// <summary>
        /// Restores the files.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        void RestoreFiles(IEnumerable<FileModel> files, int computerId, EncryptInfo encryptKey);

        /// <summary>
        /// Reset init event for client app
        /// </summary>
        void ResetInitEvent();

        /// <summary>
        /// call this method to make sure that repository is already inited
        /// </summary>
        /// <param name="timeout">The time out</param>
        /// <returns>The bool value</returns>
        bool EnsureInited(int timeout);
    }
}