using System;
using Share430.Client.Core.Args;
using Share430.Client.Remoting.Args;
using Share430.Client.Remoting.Handlers;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote event publisher.
    /// </summary>
    public interface IProxyEventPublisher : IDisposable
    {
        /// <summary>
        /// Occurs when [SetOption message received].
        /// </summary>
        event RemotingEventHandler<SetOptionsEventArgs> OptionChanged;

        /// <summary>
        /// Raises the <see cref="E:SetOption"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        void OnOptionChanged(SetOptionsEventArgs args);

        /// <summary>
        /// Occurs when [message received].
        /// </summary>
        event RemotingEventHandler<MessageEventArgs> MessageReceived;

        /// <summary>
        /// Raises the <see cref="E:MessageReceived"/> event.
        /// </summary>
        /// <param name="args">The <see cref="MessageEventArgs"/> instance containing the event data.</param>
        void OnMessageReceived(MessageEventArgs args);

        /// <summary>
        /// Occurs when [backup status changed].
        /// </summary>
        event RemotingEventHandler<BackupStatusChangedEventArgs> BackupStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="BackupStatusChangedEventArgs"/> instance containing the event data.</param>
        void OnBackupStatusChanged(BackupStatusChangedEventArgs args);

        /// <summary>
        /// Occurs when [backup progress changed].
        /// </summary>
        event RemotingEventHandler<ProgressChangedEventArgs> BackupProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupProgressChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ProgressChangedEventArgs"/> instance containing the event data.</param>
        void OnBackupProgressChanged(ProgressChangedEventArgs args);

        /// <summary>
        /// Occurs when [restore status changed].
        /// </summary>
        event RemotingEventHandler<RestoreStatusChangedEventArgs> RestoreStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="RestoreStatusChangedEventArgs"/> instance containing the event data.</param>
        void OnRestoreStatusChanged(RestoreStatusChangedEventArgs args);

        /// <summary>
        /// Occurs when [restore progress changed].
        /// </summary>
        event RemotingEventHandler<ProgressChangedEventArgs> RestoreProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreProgressChanged"/> event.
        /// </summary>
		/// <param name="args">The <see cref="Share430.Client.Remoting.Args.RestoreProgressChangedEventArgs"/> instance containing the event data.</param>
        void OnRestoreProgressChanged(ProgressChangedEventArgs args);

        /// <summary>
        /// Occurs when [application state changed].
        /// </summary>
        event EventHandler<ApplicationStateChangedEventArgs> ApplicationStateChanged;

        /// <summary>
        /// Raises the <see cref="E:ApplicationStateChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ApplicationStateChangedEventArgs"/> instance containing the event data.</param>
        void OnApplicationStateChanged(ApplicationStateChangedEventArgs args);

        /// <summary>
        /// Occurs when [application mode changed].
        /// </summary>
        event EventHandler<ApplicationModeChangedEventArgs> ApplicationModeChanged;

        /// <summary>
        /// Raises the <see cref="E:ApplicationModeChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ApplicationModeChangedEventArgs"/> instance containing the event data.</param>
        void OnApplicationModeChanged(ApplicationModeChangedEventArgs args);

        /// <summary>
        /// Occurs when [connection priority changed].
        /// </summary>
        event EventHandler<ConnectionPriorityChangedEventArgs> ConnectionPriorityChanged;

        /// <summary>
        /// Raises the <see cref="E:ConnectionPriorityChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ConnectionPriorityChangedEventArgs"/> instance containing the event data.</param>
        void OnConnectionPriorityChanged(ConnectionPriorityChangedEventArgs args);

        /// <summary>
        /// Occurs when [paused].
        /// </summary>
        event EventHandler<PauseEventArgs> Paused;

        /// <summary>
        /// Raises the <see cref="E:Paused"/> event.
        /// </summary>
        /// <param name="args">The <see cref="PauseEventArgs"/> instance containing the event data.</param>
        void OnPaused(PauseEventArgs args);

        /// <summary>
        /// Occurs when [RestorePreviousVersion].
        /// </summary>
        event EventHandler<RestoreFileEventArgs> RestorePreviousVersion;

        /// <summary>
        /// Raises the <see cref="E:RestorePreviousVersion"/> event.
        /// </summary>
        /// <param name="args">The <see cref="Share430.Client.Remoting.Args.RestoreFileEventArgs"/> instance containing the event data.</param>
        void OnRestorePreviousVersion(RestoreFileEventArgs args);

        /// <summary>
        /// Occurs when [restore speed changed].
        /// </summary>
        event RemotingEventHandler<SpeedChangedEventArgs> RestoreSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        void OnRestoreSpeedChanged(SpeedChangedEventArgs args);

        /// <summary>
        /// Occurs when [backup speed changed].
        /// </summary>
        event RemotingEventHandler<SpeedChangedEventArgs> BackupSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        void OnBackupSpeedChanged(SpeedChangedEventArgs args);
    }
}