using System;
using System.Collections.Generic;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remoting computer manager.
    /// </summary>
    public interface IRemoteComputerManager
    {
        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <returns>The list od existing computers.</returns>
        List<FolderModel> GetComputers();

        /// <summary>
        /// Adds the computer.
        /// </summary>
        /// <param name="computer">The computer.</param>
        /// <returns>The buy url.</returns>
        string AddComputer(ComputerInfo computer);

        /// <summary>
        /// Validates the computer.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="password">The password.</param>
        /// <returns>True if provided data is valid, otherwise false.</returns>
        bool ValidateComputer(int userId, Guid computerId, string password);

        /// <summary>
        /// Gets the backup users list.
        /// </summary>
        /// <param name="computer">The computer.</param>
        /// <returns>The list of users in the backuped computer.</returns>
        List<SyncronizeUsersEntry> GetBackupUsersList(ComputerInfo computer);

        /// <summary>
        /// Gets the local users list.
        /// </summary>
        /// <returns>The list of users from this computer</returns>
        List<SyncronizeUsersEntry> GetLocalUsersList();

        /// <summary>
        /// Updates the computer GUID.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        void UpdateComputerGuid(int computerId);

        void CreateFolder(string folderName);

        void RemoveFolder(string folderName);
    }
}