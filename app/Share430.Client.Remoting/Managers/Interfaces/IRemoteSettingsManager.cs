using System;
using Share430.Client.Core.Entities.Settings;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remoting settings manager.
    /// </summary>
    public interface IRemoteSettingsManager : IDisposable
    {
        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <returns>The settigs object.</returns>
        SettingsInfo LoadSettings();

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        void SaveSettings(SettingsInfo settings);
    }
}