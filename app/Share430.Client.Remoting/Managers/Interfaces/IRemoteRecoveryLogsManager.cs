using System;
using System.Collections.Generic;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remoting backup session manager.
    /// </summary>
    public interface IRemoteRecoveryLogsManager : IDisposable
    {
        /// <summary>
        /// Sets the file priority.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="priority">The priority.</param>
        /// <returns>Results of operation.</returns>
        bool SetFilePriority(string filePath, RestorePriority priority);

        /// <summary>
        /// Cancels the restore file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        void CancelRestoreFile(string filePath);

        /// <summary>
        /// Gets the entries.
        /// </summary>
        /// <param name="type">The type of recovery logs.</param>
        /// <returns>All files from specified recovery logs </returns>
        List<string> GetEntries(RecoveryLogTypes type);

        /// <summary>
        /// Gets the file queue item.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>Return file queue item info</returns>
        RestoreFileQueueItem GetFileQueueItem(string filePath);
    }
}