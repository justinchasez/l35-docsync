namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// The remote application manager.
    /// </summary>
    public interface ISyncronizationManager
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is users syncronized.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is users syncronized; otherwise, <c>false</c>.
        /// </value>
        bool IsUsersSyncronized { get; set; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <returns>Error message</returns>
        string GetErrorMessage();
    }
}