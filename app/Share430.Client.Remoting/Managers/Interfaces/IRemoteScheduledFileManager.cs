using System;
using System.Collections.Generic;
using System.Data;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Managers.Interfaces
{
    /// <summary>
    /// Add file to queue delegate
    /// </summary>
    /// <param name="filePathes">List of file pathes</param>
    /// <param name="force">Is file force</param>
    public delegate void AddFilesToQueueDelegate(string[] filePathes, bool force);

    /// <summary>
    /// Remove file from queue delegate
    /// </summary>
    /// <param name="filePathes">Path to file</param>
    public delegate void RemoveFilesFromQueueDelegate(string[] filePathes);

    /// <summary>
    /// Rename files in queue
    /// </summary>
    /// <param name="fileNames">Key - old path, value - new path</param>
    public delegate void RenameFilesInQueueDelegate(Dictionary<string, string> fileNames);

    /// <summary>
    /// Get restoring file delegate
    /// </summary>
    /// <returns>Current restoring file</returns>
    public delegate RestoreFileQueueItem GetCurrentRestoringFileDelegate();

    /// <summary>
    /// Remove file from S3 delegate
    /// </summary>
    /// <param name="filePath">Path to file</param>
    /// <returns>Is operation sucsessed</returns>
    public delegate bool RemoveFileFromS3Delegate(string filePath);

    /// <summary>
    /// Remove file from S3 delegate
    /// </summary>
    /// <param name="filePath">Path to file</param>
    /// <returns>Is operation sucsessed</returns>
    public delegate void CancelRestoreFileDelegate(string filePath);

    /// <summary>
    /// Report progress delegate
    /// </summary>
    public delegate void ReportProgressDelegate();

    public delegate void RemoveFolderFromServerDelegate(string folderPath);

    public delegate void CreateFolderOnServerDelegate(string folderPath);

    /// <summary>
    /// Remote scheduled file manager interface
    /// </summary>
    public interface IRemoteScheduledFileManager
    {
        /// <summary>
        /// Gets the file link.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <returns>The file link</returns>
        string GetFileLink(string fullPath);

        /// <summary>
        /// Get entries
        /// </summary>
        /// <param name="fullPath">Full path to folder</param>
        /// <param name="type">Some .. type</param>
        /// <returns>Entries in this path</returns>
        string[] GetEntries(string fullPath, int type);

        /// <summary>
        /// Back this up command
        /// </summary>
        /// <param name="fullPath">full path to file</param>
        void BackThisUpCommand(string fullPath);

        /// <summary>
        /// Dont back this up
        /// </summary>
        /// <param name="fullPath">full path to file</param>
        void DontBackThisUp(string fullPath);

        /// <summary>
        /// Check file or folder
        /// </summary>
        /// <param name="fullPath">Path to file or folder</param>
        /// <param name="state">File or folder state</param>
        /// <param name="isScheduled">Is file or folder scheduled</param>
        /// <param name="verCount">File versions count</param>
        /// <returns>Is folder or file exist in XML</returns>
        bool Check(string fullPath, ref BackupState state, ref bool isScheduled, ref int verCount);

        /// <summary>
        /// File upload complete handler
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">Event handler params</param>
        void FileUploadCompleteHandler(object sender, UploadFileEventArgs e);

        /// <summary>
        /// Check if the specify folder contains at least one file with specify state
        /// </summary>
        /// <param name="fullPath">full path to specify folder</param>
        /// <param name="entryState">state of the file</param>
        /// <returns>return finding result</returns>
        bool ContainsFilesWithState(string fullPath, BackupState entryState);

        /// <summary>
        /// Get file info from XML
        /// </summary>
        /// <param name="pathToFile">Full path to file</param>
        /// <returns>XML file info object</returns>
        XmlFileInfo GetFileInfo(string pathToFile);

        /// <summary>
        /// Get backed up files for search
        /// </summary>
        /// <returns>DataTable of backed up files</returns>
        DataTable GetBackedUpFilesForSearch();

        /// <summary>
        /// Delete file from S3 server
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>Is delete successed</returns>
        bool DeleteFileFromBackup(string pathToFile, bool skipServerSide = false);
        

        bool RemoveFileOrFolderFromFileSystem(string pathToFile);

        void RemoveFileOrFolder(string fullPath);

        /// <summary>
        /// Update XML file repository from server
        /// </summary>
        /// <param name="files">List of files from server</param>
        void SetFilesInXml(ComputerFilesDto files);

        /// <summary>
        /// Get Count Of Backed Up Files
        /// </summary>
        /// <returns>Count Of Backed Up Files</returns>
        int GetCountOfBackedUpFiles();

        /// <summary>
        /// Get the type of path.
        /// </summary>
        /// <param name="path">The checked path.</param>
        /// <returns>The path type value</returns>
        PathType GetPathType(string path);

        void CreateNewFolder(string path);

        /// <summary>
        /// Add file to queue for backup
        /// </summary>
        event AddFilesToQueueDelegate AddFilesToQueue;

        /// <summary>
        /// Remove file from queue for backup
        /// </summary>
        event RemoveFilesFromQueueDelegate RemoveFilesFromQueue;

        /// <summary>
        /// Rename file in queue for backup
        /// </summary>
        event RenameFilesInQueueDelegate RenameFelesInQueue;

        /// <summary>
        /// Get cerrent restoring file
        /// </summary>
        event GetCurrentRestoringFileDelegate GetCurrentRestoringFele;

        /// <summary>
        /// Remove file from S3 event
        /// </summary>
        event RemoveFileFromS3Delegate RemoveFileFromS3;

        /// <summary>
        /// Report progress event
        /// </summary>
        event ReportProgressDelegate ReportProgress;

        event CancelRestoreFileDelegate CancelRestoreFile;

        event CreateFolderOnServerDelegate CreateFolderOnServer;

        event RemoveFolderFromServerDelegate RemoveFolderFromServer;
    }
}