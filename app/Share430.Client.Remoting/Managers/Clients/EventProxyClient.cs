using System;
using Share430.Client.Core.Args;
using Share430.Client.Remoting.Args;
using Share430.Client.Remoting.Handlers;

namespace Share430.Client.Remoting.Managers.Clients
{
    /// <summary>
    /// Event proxy client.
    /// </summary>
    [Serializable]
    public class EventProxyClient : MarshalByRefObject
    {
        /// <summary>
        /// The remote backup manager.
        /// </summary>
        private readonly EventProxy manager;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventProxyClient"/> class.
        /// </summary>
        /// <param name="manager">The manager.</param>
        public EventProxyClient(EventProxy manager)
        {
            if (manager == null)
            {
                throw new ArgumentNullException("manager");
            }

            this.manager = manager;

            this.manager.MessageReceived += this.OnMessageReceived;
            this.manager.BackupStatusChanged += this.OnBackupStatusChanged;
            this.manager.BackupProgressChanged += this.OnBackupProgressChanged;
            this.manager.RestoreStatusChanged += this.OnRestoreStatusChanged;
            this.manager.RestoreProgressChanged += this.OnRestoreProgressChanged;
            this.manager.RestoreSpeedChanged += this.OnRestoreSpeedChanged;
            this.manager.BackupSpeedChanged += this.OnBackupSpeedChanged;
            this.manager.OptionChanged += this.OnOptionChanged;
            this.manager.Paused += this.OnPaused;
            this.manager.RestorePreviousVersion += this.OnRestorePreviousVersion;
        }

        /// <summary>
        /// Sets the notify window handle
        /// </summary>
        /// <param name="handle">handle of the window</param>
        public void SetNotifyWindow(int handle)
        {
            this.manager.NotifyWindow = handle;
        }

        /// <summary>
        /// Occurs when [Paused message received].
        /// </summary>
        public event EventHandler<RestoreFileEventArgs> RestorePreviousVersion;

        /// <summary>
        /// Raise the RestorePreviousVersion event
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        public void OnRestorePreviousVersion(object sender, RestoreFileEventArgs e)
        {
            EventHandler<RestoreFileEventArgs> handler = this.RestorePreviousVersion;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        /// <summary>
        /// Occurs when [Paused message received].
        /// </summary>
        public event EventHandler<PauseEventArgs> Paused;

        /// <summary>
        /// Notify server object to Raises the <see cref="E:Paused"/> event.
        /// </summary>
        /// <param name="args">The <see cref="PauseEventArgs"/> instance containing the event data.</param>
        public void NotifyUnPaused(PauseEventArgs args)
        {
            this.manager.OnPaused(args);
        }

        /// <summary>
        /// Raises the <see cref="E:OnPaused"/> event.
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        public void OnPaused(object sender, PauseEventArgs e)
        {
            EventHandler<PauseEventArgs> handler = this.Paused;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        #region IRemoteBackupManager Members

        /// <summary>
        /// Occurs when [SetOption message received].
        /// </summary>
        public event RemotingEventHandler<SetOptionsEventArgs> OptionChanged;

        /// <summary>
        /// Notify server object to Raises the <see cref="E:OptionChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        public void NotifyOptionChanged(SetOptionsEventArgs args)
        {
            this.manager.OnOptionChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:OptionChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SetOptionsEventArgs"/> instance containing the event data.</param>
        public void OnOptionChanged(SetOptionsEventArgs args)
        {
            RemotingEventHandler<SetOptionsEventArgs> handler = this.OptionChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [message received].
        /// </summary>
        public event RemotingEventHandler<MessageEventArgs> MessageReceived;

        /// <summary>
        /// Raises the <see cref="E:MessageReceived"/> event.
        /// </summary>
        /// <param name="args">The <see cref="MessageEventArgs"/> instance containing the event data.</param>
        public void OnMessageReceived(MessageEventArgs args)
        {
            RemotingEventHandler<MessageEventArgs> handler = this.MessageReceived;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [backup status changed].
        /// </summary>
        public event RemotingEventHandler<BackupStatusChangedEventArgs> BackupStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="BackupStatusChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupStatusChanged(BackupStatusChangedEventArgs args)
        {
            RemotingEventHandler<BackupStatusChangedEventArgs> handler = this.BackupStatusChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [backup progress changed].
        /// </summary>
        public event RemotingEventHandler<ProgressChangedEventArgs> BackupProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupProgressChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="ProgressChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupProgressChanged(ProgressChangedEventArgs args)
        {
            RemotingEventHandler<ProgressChangedEventArgs> handler = this.BackupProgressChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [restore status changed].
        /// </summary>
        public event RemotingEventHandler<RestoreStatusChangedEventArgs> RestoreStatusChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreStatusChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="RestoreStatusChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreStatusChanged(RestoreStatusChangedEventArgs args)
        {
            RemotingEventHandler<RestoreStatusChangedEventArgs> handler = this.RestoreStatusChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [restore progress changed].
        /// </summary>
        public event RemotingEventHandler<ProgressChangedEventArgs> RestoreProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreProgressChanged"/> event.
        /// </summary>
		/// <param name="args">The <see cref="Share430.Client.Remoting.Args.RestoreProgressChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreProgressChanged(ProgressChangedEventArgs args)
        {
            RemotingEventHandler<ProgressChangedEventArgs> handler = this.RestoreProgressChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [restore speed changed].
        /// </summary>
        public event RemotingEventHandler<SpeedChangedEventArgs> RestoreSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:RestoreSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        public void OnRestoreSpeedChanged(SpeedChangedEventArgs args)
        {
            RemotingEventHandler<SpeedChangedEventArgs> handler = this.RestoreSpeedChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        /// <summary>
        /// Occurs when [backup speed changed].
        /// </summary>
        public event RemotingEventHandler<SpeedChangedEventArgs> BackupSpeedChanged;

        /// <summary>
        /// Raises the <see cref="E:BackupSpeedChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        public void OnBackupSpeedChanged(SpeedChangedEventArgs args)
        {
            RemotingEventHandler<SpeedChangedEventArgs> handler = this.BackupSpeedChanged;
            if (handler != null)
            {
                handler(args);
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.manager.MessageReceived -= this.OnMessageReceived;
            this.manager.BackupStatusChanged -= this.OnBackupStatusChanged;
            this.manager.BackupProgressChanged -= this.OnBackupProgressChanged;
            this.manager.RestoreStatusChanged -= this.OnRestoreStatusChanged;
            this.manager.RestoreProgressChanged -= this.OnRestoreProgressChanged;
            this.manager.BackupSpeedChanged -= this.OnBackupSpeedChanged;
            this.manager.RestoreSpeedChanged -= this.OnRestoreSpeedChanged;
        }

        #endregion
    }
}