using System;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Remoting.Managers
{
    /// <summary>
    /// The remoting session manager.
    /// </summary>
    [Serializable]
    public class ProxyManager : MarshalByRefObject
    {
        /// <summary>
        /// Tests the connection.
        /// </summary>
        public void TestConnection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyManager"/> class.
        /// </summary>
        /// <param name="backupManager">The backup manager.</param>
        /// <param name="scheduleManager">The schedule manager.</param>
        /// <param name="settingsManager">The settings manager.</param>
        /// <param name="appManager">The app manager.</param>
        /// <param name="helpManager">The help manager.</param>
        /// <param name="computerManager">The computer manager.</param>
        /// <param name="userManager">The user manager.</param>
        /// <param name="fileManager">The scheduled file manager</param>
        /// <param name="recoveryLogsManager">The recovery logs manager.</param>
        public ProxyManager(IRemoteBackupManager backupManager, 
                            IRemoteScheduleManager scheduleManager, 
                            IRemoteSettingsManager settingsManager, 
                            IRemoteAppManager appManager,
                            IRemoteHelpManager helpManager,
                            IRemoteComputerManager computerManager,
                            IRemoteUserManager userManager,
                            IRemoteScheduledFileManager fileManager,
                            IRemoteRecoveryLogsManager recoveryLogsManager)
        {
            this.BackupManager = backupManager;
            this.ScheduleManager = scheduleManager;
            this.SettingsManager = settingsManager;
            this.AppManger = appManager;
            this.HelpManger = helpManager;
            this.ComputerManager = computerManager;
            this.UserManager = userManager;
            this.ScheduledFileManager = fileManager;
            this.RecoveryLogsManager = recoveryLogsManager;

            fileManager.AddFilesToQueue += backupManager.AddFileCollectionToBackup;
            fileManager.RemoveFilesFromQueue += backupManager.RemoveFileCollectionFromFromQueueToBackup;
            fileManager.RenameFelesInQueue += backupManager.RenameFilesInBackup;
            fileManager.GetCurrentRestoringFele += backupManager.GetCurrentRestoreFile;
            fileManager.RemoveFileFromS3 += backupManager.RemoveFileFromS3;
            fileManager.CancelRestoreFile += backupManager.CancelRestoreFile;
            fileManager.ReportProgress += backupManager.RaiseBackupProgressChanged;
            fileManager.CreateFolderOnServer += computerManager.CreateFolder;
            fileManager.RemoveFolderFromServer += computerManager.RemoveFolder;

            backupManager.GetFilesFromServerComplete += fileManager.SetFilesInXml;
        }

        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        /// <summary>
        /// Gets the scheduled file manager.
        /// </summary>
        /// <value>The file manager.</value>
        public IRemoteScheduledFileManager ScheduledFileManager { get; private set; }

        /// <summary>
        /// Gets the backup manager.
        /// </summary>
        /// <value>The backup manager.</value>
        public IRemoteBackupManager BackupManager { get; private set; }

        /// <summary>
        /// Gets the schedule manager.
        /// </summary>
        /// <value>The schedule manager.</value>
        public IRemoteScheduleManager ScheduleManager { get; private set; }

        /// <summary>
        /// Gets the settings manager.
        /// </summary>
        /// <value>The settings manager.</value>
        public IRemoteSettingsManager SettingsManager { get; private set; }

        /// <summary>
        /// Gets the app manger.
        /// </summary>
        /// <value>The app manger.</value>
        public IRemoteAppManager AppManger { get; private set; }

        /// <summary>
        /// Gets the help manger.
        /// </summary>
        /// <value>The help manger.</value>
        public IRemoteHelpManager HelpManger { get; private set; }

        /// <summary>
        /// Gets the computer manager.
        /// </summary>
        /// <value>The computer manager.</value>
        public IRemoteComputerManager ComputerManager { get; private set; }

        /// <summary>
        /// Gets the user manager.
        /// </summary>
        /// <value>The user manager.</value>
        public IRemoteUserManager UserManager { get; private set; }

        /// <summary>
        /// Gets the recovery logs manager.
        /// </summary>
        /// <value>The recovery logs manager.</value>
        public IRemoteRecoveryLogsManager RecoveryLogsManager { get; private set; }
    }
}