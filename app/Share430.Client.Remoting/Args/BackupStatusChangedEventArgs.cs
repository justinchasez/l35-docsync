using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The event args on backup status change event.
    /// </summary>
    [Serializable]
    public class BackupStatusChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public BackupStatus Status { get; set; }

        /// <summary>
        /// Error message if error occurs
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Is limit reached
        /// </summary>
        public bool IsLimitReached { get; set; }

        /// <summary>
        /// Used space
        /// </summary>
        public long UsedSpace { get; set; }

        /// <summary>
        /// Is computer expired
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Is invalid time
        /// </summary>
        public bool IsInvalidTime { get; set; }
    }
}