using System;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The evetn argument for the Pause event.
    /// </summary>
    [Serializable]
    public class PauseEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PauseEventArgs"/> class.
        /// </summary>
        public PauseEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PauseEventArgs"/> class.
        /// </summary>
        /// <param name="minutes">The minutes.</param>
        public PauseEventArgs(int minutes)
        {
            this.Minutes = minutes;
        }

        /// <summary>
        /// Gets or sets the minutes.
        /// </summary>
        /// <value>The minutes.</value>
        public int Minutes { get; set; }
    }
}