using System;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// Event argument to restore file
    /// </summary>
    [Serializable]
    public class RestoreFileEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreFileEventArgs"/> class.
        /// </summary>
        /// <param name="fullPath">path to restoring file</param>
        public RestoreFileEventArgs(string fullPath)
        {
            this.FilePath = fullPath;
        }

        /// <summary>
        /// Gets path to restoring file
        /// </summary>
        public string FilePath { get; private set; }
    }
}