using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The event args on restore status change event.
    /// </summary>
    [Serializable]
    public class RestoreStatusChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is invalid time.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is invalid time; otherwise, <c>false</c>.
        /// </value>
        public bool IsInvalidTime { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public RestoreStatus Status { get; set; }

        /// <summary>
        /// Gets or sets is computer expired value
        /// </summary>
        public bool IsComputerExpired { get; set; }
    }
}