using System;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The event argument for ApplicationModeChanged event.
    /// </summary>
    public class ApplicationModeChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationModeChangedEventArgs"/> class.
        /// </summary>
        public ApplicationModeChangedEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationModeChangedEventArgs"/> class.
        /// </summary>
        /// <param name="isRecoveryMode">if set to <c>true</c> [is recovery mode].</param>
        public ApplicationModeChangedEventArgs(bool isRecoveryMode)
        {
            this.IsRecoveryMode = isRecoveryMode;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is recovery mode.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is recovery mode; otherwise, <c>false</c>.
        /// </value>
        public bool IsRecoveryMode { get; set; }
    }
}