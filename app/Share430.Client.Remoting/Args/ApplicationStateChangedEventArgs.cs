using System;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// Event argument for ApplicationStateChanged event.
    /// </summary>
    public class ApplicationStateChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationStateChangedEventArgs"/> class.
        /// </summary>
        public ApplicationStateChangedEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationStateChangedEventArgs"/> class.
        /// </summary>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        public ApplicationStateChangedEventArgs(bool isEnabled)
        {
            this.IsEnabled = isEnabled;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsEnabled { get; set; }
    }
}