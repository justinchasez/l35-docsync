using System;
using Share430.Client.Core.Enums;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The event argument for ConnectionPriorityChanged event.
    /// </summary>
    public class ConnectionPriorityChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionPriorityChangedEventArgs"/> class.
        /// </summary>
        public ConnectionPriorityChangedEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionPriorityChangedEventArgs"/> class.
        /// </summary>
        /// <param name="connectionPriority">The connection priority.</param>
        public ConnectionPriorityChangedEventArgs(ConnectionPriority connectionPriority)
        {
            this.ConnectionPriority = connectionPriority;
        }

        /// <summary>
        /// Gets or sets the connection priority.
        /// </summary>
        /// <value>The connection priority.</value>
        public ConnectionPriority ConnectionPriority { get; set; }
    }
}