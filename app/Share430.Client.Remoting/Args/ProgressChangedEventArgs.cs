using System;
using Share430.Client.Core.Entities.Files;

namespace Share430.Client.Remoting.Args
{
    /// <summary>
    /// The event args on backup progress changed event.
    /// </summary>
    [Serializable]
    public class ProgressChangedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the current progress.
        /// </summary>
        /// <value>The current progress.</value>
        public Progress CurrentProgress { get; set; }
    }
}