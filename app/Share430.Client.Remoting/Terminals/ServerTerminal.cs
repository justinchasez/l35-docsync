using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Serialization.Formatters;
using System.Security.Principal;
using Share430.Client.Core.Entities;
using Share430.Client.Core.Utils;

namespace Share430.Client.Remoting.Terminals
{
    /// <summary>
    /// The server remoting terminal.
    /// </summary>
    public class ServerTerminal
    {
        /// <summary>
        /// Starts the listening.
        /// </summary>
        /// <param name="remoteObject">The remote object.</param>
        public static void StartListening(MarshalByRefObject remoteObject)
        {
            Loger.Instance.Log("Server terminal: Start remoting listening " + remoteObject.GetType());

            RemotingServices.Marshal(remoteObject, remoteObject.GetType().Name);
        }

        /// <summary>
        /// Creates the channel.
        /// </summary>
        /// <param name="port">The remoting port.</param>
        /// <param name="channelName">Name of the channel.</param>
        public static void RegisterChannel(int port, string channelName)
        {
            Loger.Instance.Log("Server terminal: Start server register ipc chanel");

            BinaryServerFormatterSinkProvider serverFormatter = new BinaryServerFormatterSinkProvider { TypeFilterLevel = TypeFilterLevel.Full };

            BinaryClientFormatterSinkProvider clientProv = new BinaryClientFormatterSinkProvider();

            Hashtable props = new Hashtable();
            props["name"] = String.Format("{0}:{1}", channelName, port);
            props["portName"] = String.Format("{0}:{1}", channelName, port);
            props["authorizedGroup"] = WindowGroupHelper.GetGroupName(WellKnownSidType.WorldSid, null, false);
            props["exclusiveAddressUse"] = "false";

            Loger.Instance.Log("Server terminal: Name: " + props["name"] + "; PortName: " + props["portName"] + "; AuthorizedGroup: " + props["authorizedGroup"]);

            IChannel channel = new IpcChannel(props, clientProv, serverFormatter);

            ChannelServices.RegisterChannel(channel, false);

            Loger.Instance.Log("Server terminal: Server register ipc chanel success");
        }
    }
}
