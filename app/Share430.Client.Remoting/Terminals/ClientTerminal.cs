using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Runtime.Serialization.Formatters;
using System.Security.Principal;
using System.ServiceProcess;
using Share430.Client.Core.Entities;
using Share430.Client.Core.Utils;

namespace Share430.Client.Remoting.Terminals
{
    /// <summary>
    /// The remoting terminal for client application.
    /// </summary>
    public class ClientTerminal
    {
        /// <summary>
        /// The current remoting channel.
        /// </summary>
        private IChannel channel;

        /// <summary>
        /// Connects the specified port.
        /// </summary>
        /// <typeparam name="T">The type of remotable objec.</typeparam>
        /// <param name="port">The remoting port.</param>
        /// <param name="channelName">Name of the channel.</param>
        /// <returns>The activated remote object.</returns>
        public T Connect<T>(int port, string channelName)
        {
            Loger.Instance.Log("Client terminal: waiting for connect");

            try
            {
                DateTime stamp = DateTime.Now;

				ServiceController serviceController = new ServiceController("Share430Service");

                TimeSpan timeout = TimeSpan.FromMilliseconds(120000);

                serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Client terminal: " + ex);
            }

            Loger.Instance.Log("Client terminal: Start client remoting connect to object " + typeof(T));

            if (this.channel == null)
            {
                this.RegisterChannel(port, channelName);
            }

//            string fullServerAddress = string.Format("tcp://{0}:{1}/{2}", "localhost", 1990, typeof(T).Name);
            string fullServerAddress = string.Format("ipc://{0}:{1}/{2}", channelName, port, typeof(T).Name);

            // Create a proxy from remote object.
            T res = (T)Activator.GetObject(typeof(T), fullServerAddress);

            return res;
        }

        /// <summary>
        /// Creates the channel.
        /// </summary>
        /// <param name="port">The remoting port.</param>
        /// <param name="channelName">Name of the channel.</param>
        private void RegisterChannel(int port, string channelName)
        {
            Loger.Instance.Log("Client terminal: Start client register ipc chanel");

            BinaryServerFormatterSinkProvider serverFormatter = new BinaryServerFormatterSinkProvider();
            serverFormatter.TypeFilterLevel = TypeFilterLevel.Full;

            BinaryClientFormatterSinkProvider clientProv = new BinaryClientFormatterSinkProvider();

            string tick = DateTime.Now.Ticks.ToString();

            Hashtable props = new Hashtable();
            props["name"] = String.Format("{0}:{1}:{2}", channelName, port, tick);
            props["portName"] = String.Format("{0}:{1}:{2}", channelName, port, tick);
            props["authorizedGroup"] = WindowGroupHelper.GetGroupName(WellKnownSidType.WorldSid, null, false);

            Loger.Instance.Log("Client terminal: Name: " + props["name"] + "; PortName: " + props["portName"] + "; AuthorizedGroup: " + props["authorizedGroup"]);

            this.channel = new IpcChannel(props, clientProv, serverFormatter);
            ChannelServices.RegisterChannel(this.channel, false);

            Loger.Instance.Log("Client terminal: Client register ipc chanel success");
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public void Disconnect()
        {
            if (this.channel != null)
            {
                Loger.Instance.Log("Client ipc chanel disconnect");
                ChannelServices.UnregisterChannel(this.channel);
            }
        }
    }
}
