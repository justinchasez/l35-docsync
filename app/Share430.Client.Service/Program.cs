using System.ServiceProcess;

namespace Share430.Client.Service
{
	/// <summary>
    /// The main class of programm
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">command line argument</param>
        public static void Main(string[] args)
        {
            ServiceBase[] servicesToRun = new ServiceBase[] 
                                              { 
                                                  new Service() 
                                              };

            ServiceBase.Run(servicesToRun);
        }
    }
}
