using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Share430.Client.Core.Utils;
using Share430.Client.Service.Core.Managers;

namespace Share430.Client.Service.Core.Repositories
{
    /// <summary>
    /// The restore file repository.
    /// </summary>
    public class SyncRepository : IDisposable
    {
        #region Private fields

        private static object syncObject = new object();

        /// <summary>
        /// The last update date.
        /// </summary>
        private DateTime? lastUpdate;

        private readonly HubConnection connection;

        /// <summary>
        /// The service url settings key.
        /// </summary>
        private const string SERVICE_URL_KEY = "Settings.ServiceUrl";

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SyncRepository"/> class.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        private SyncRepository(RemotingManager remotingManagerParam)
        {
            if (remotingManagerParam == null)
            {
                throw new ArgumentNullException("remotingManagerParam");
            }

            this.RemotingManager = remotingManagerParam;

            try
            {
                var url = ConfigurationManager.AppSettings[SERVICE_URL_KEY];

                //Set connection
                connection = new HubConnection(url + "/signalr");
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Sync repository: Cannot connect to signalr - " + ex.ToString());
            }
        }

        private void InitConnection(object state)
        {
            lock (syncObject)
            {
                try
                {
                    if (connection.State == ConnectionState.Connected)
                    {
                        connection.Stop();
                    }

                    var settings = Instance.RemotingManager.ProxyManager.SettingsManager.LoadSettings();
                    if (settings.IsFirstRun || string.IsNullOrEmpty(settings.TargetFolderLocation))
                    {
                        return;
                    }

                    //Make proxy to hub based on hub name on server
                    var myHub = connection.CreateHubProxy("folders");

                    connection.Closed += connection_Closed;

                    //Start connection
                    connection.Start().ContinueWith(task =>
                    {
                        if (task.IsFaulted)
                        {
                            Loger.Instance.Log("Sync repository: There was an error opening the connection: " +
                                               task.Exception.GetBaseException());
                            connection_Closed();
                        }
                        else
                        {
                            Loger.Instance.Log("Sync repository: Connected");
                        }

                    }).Wait();
                    
                    myHub.Invoke<string>("connect",
                        this.RemotingManager.ProxyManager.SettingsManager.LoadSettings().Credentials.Token).Wait();

                    settings = this.RemotingManager.ProxyManager.SettingsManager.LoadSettings();
                    myHub.Invoke("requestUpdates", settings.Credentials.Token, settings.LastUpdateDate ?? DateTime.MinValue).Wait();
                    settings.LastUpdateDate = DateTime.UtcNow;
                    this.lastUpdate = settings.LastUpdateDate;
                    this.RemotingManager.ProxyManager.SettingsManager.SaveSettings(settings);

                    myHub.On<int, string, int, string>("fileAdded", (id, name, folderId, fullName) =>
                    {
                        this.RemotingManager.ProxyManager.BackupManager.AddFileToBackup(fullName, false);
                        this.lastUpdate = DateTime.UtcNow;
                    });

                    myHub.On<int, string>("fileRemoved", (id, fullName) =>
                    {
                        this.RemotingManager.ProxyManager.ScheduledFileManager.RemoveFileOrFolderFromFileSystem(fullName);
                        this.lastUpdate = DateTime.UtcNow;
                    });

                    myHub.On<int, string, int>("fileChanged", (id, fullName, folderId) =>
                    {
                        this.RemotingManager.ProxyManager.BackupManager.AddFileToBackup(fullName, false);
                        this.lastUpdate = DateTime.UtcNow;
                    });

                    myHub.On<int, string, int, string>("folderAdded", (id, path, parentId, fullPath) =>
                    {
                        this.RemotingManager.ProxyManager.ScheduledFileManager.CreateNewFolder(fullPath);
                        this.lastUpdate = DateTime.UtcNow;
                    });

                    myHub.On<int, string>("folderRemoved", (id, fullName) =>
                    {
                        this.RemotingManager.ProxyManager.ScheduledFileManager.RemoveFileOrFolderFromFileSystem(fullName);
                        this.lastUpdate = DateTime.UtcNow;
                    });

                    myHub.On<int, string>("folderChanged", (id, name) =>
                    {
                        // TODO: implement when folders will change
                        this.lastUpdate = DateTime.UtcNow;
                    });
                }
                catch (Exception ex)
                {
                    Loger.Instance.Log("Sync repository: InitConnection error - " + ex.ToString());
                }
            }
        }

        void connection_Closed()
        {
            Thread.Sleep(10000);
            ThreadPool.QueueUserWorkItem(Instance.InitConnection);
        }
        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static SyncRepository Instance { get; private set; }

        /// <summary>
        /// Initializes the specified remoting manager param.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        /// <param name="downloadService">The download service.</param>
        /// <param name="dataFilePath">The data file path.</param>
        public static void Initialize(RemotingManager remotingManagerParam)
        {
            if (Instance == null)
            {
                Loger.Instance.Log("Sync repository: Initialize SyncRepository instance");
                Instance = new SyncRepository(remotingManagerParam);
                ThreadPool.QueueUserWorkItem(Instance.InitConnection);
            }
            else
            {
                Loger.Instance.Log("Sync repository: Update SyncRepository instance");
                
                ThreadPool.QueueUserWorkItem(Instance.InitConnection);
            }
        }
      
        /// <summary>
        /// Gets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        public RemotingManager RemotingManager { get; private set; }

        /// <summary>
        /// Dispose RestoreReposytory instance
        /// </summary>
        public void Dispose()
        {
            var settings = this.RemotingManager.ProxyManager.SettingsManager.LoadSettings();
            settings.LastUpdateDate = this.lastUpdate;
            this.RemotingManager.ProxyManager.SettingsManager.SaveSettings(settings);
        }
    }
}