using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services;
using Share430.Client.Core.Utils;
using Share430.Client.FileManager;
using Share430.Client.Remoting.Args;
using Share430.Client.Scheduler;
using Share430.Client.Service.Core.Managers;
using ProgressChangedEventArgs = Share430.Client.Remoting.Args.ProgressChangedEventArgs;
using Timer = System.Timers.Timer;

namespace Share430.Client.Service.Core.Repositories
{
    /// <summary>
    /// Tha main backup manager.
    /// </summary>
    public class BackupRepository : IDisposable
    {
        #region Private fields

        /// <summary>
        /// The internal timer.
        /// </summary>
        private readonly Timer timer;

        /// <summary>
        /// The files queue.
        /// </summary>
        private readonly BackupFileQueue<FileQueueItem> queue;

        /// <summary>
        /// The synchronization object.
        /// </summary>
        private static readonly object SyncObject = new object();

        /// <summary>
        /// Init event handle
        /// </summary>
        private static EventWaitHandle initEventHandle = new EventWaitHandle(false, EventResetMode.ManualReset, CoreConstantsHelper.ServiceEventName);

        /// <summary>
        /// The current process state.
        /// </summary>
        private BackupStatus currentState;

        /// <summary>
        /// failed count
        /// </summary>
        private int failedCount;

        /// <summary>
        /// Timer for retry failed files
        /// </summary>
        private Timer failedTimer;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <value>The singleton instance.</value>
        public static BackupRepository Instance { get; private set; }

        /// <summary>
        /// Is limit reached
        /// </summary>
        public bool IsLimitReached { get; set; }

        /// <summary>
        /// Used space on the server
        /// </summary>
        public long UsedSpace { get; set; }

        /// <summary>
        /// Is Computer expired
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Is invalid time
        /// </summary>
        public bool IsInvalidTime { get; set; }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        public BackupStatus CurrentState
        {
            get
            {
                return this.currentState;
            }

            set
            {
                if (this.currentState != value)
                {
                    Loger.Instance.Log("Backup repository: Raise state changed event to " + value);

                    if (this.failedTimer != null)
                    {
                        Loger.Instance.Log("Backup repository: Dispose failed timer");
                        this.failedTimer.Dispose();
                        this.failedTimer = null;
                    }

                    if (value == BackupStatus.Failed)
                    {
                        Loger.Instance.Log("Backup repository: Create failed timer");
                        this.failedTimer = new Timer(600000);
                        this.failedTimer.Elapsed += this.FailedTimer_Elapsed;
                        this.failedTimer.Start();
                    }
                }

                this.currentState = value;
                this.RemotingManager.ProxyPublisher.OnBackupStatusChanged(new BackupStatusChangedEventArgs
                                                                              {
                                                                                  Status = value,
                                                                                  ErrorMessage = this.CurrentProgress.ErrorMessage,
                                                                                  IsLimitReached = this.IsLimitReached,
                                                                                  UsedSpace = this.UsedSpace,
                                                                                  IsComputerExpired = this.IsComputerExpired,
                                                                                  IsInvalidTime = this.IsInvalidTime
                                                                              });
                this.IsComputerExpired = false;
                this.IsLimitReached = false;
                if (value == BackupStatus.Failed)
                {
                    this.IsInvalidTime = false;
                }
            }
        }

        /// <summary>
        /// The failed timer elapsed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void FailedTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Loger.Instance.Log("Backup repository: Failed timer elapsed");
            RemotingManager.ProxyManager.ScheduleManager.IsASAP = true;
        }

        /// <summary>
        /// Gets the current file.
        /// </summary>
        /// <value>The current file.</value>
        //public FileInfo CurrentFile { get; private set; }
        public FileQueueItem CurrentFile { get; private set; }

        /// <summary>
        /// Gets the upload service.
        /// </summary>
        /// <value>The upload service.</value>
        public IUploadService UploadService { get; private set; }

        /// <summary>
        /// Gets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        public RemotingManager RemotingManager { get; private set; }

        /// <summary>
        /// The current progress
        /// </summary>
        public Progress CurrentProgress { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="BackupRepository"/> class.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        /// <param name="uploadService">The upload service.</param>
        /// <param name="dataFilePath">The data file path.</param>
        private BackupRepository(RemotingManager remotingManagerParam, IUploadService uploadService, string dataFilePath)
        {
            if (remotingManagerParam == null)
            {
                throw new ArgumentNullException("remotingManagerParam");
            }

            this.CurrentProgress = new Progress(new BackupPercentCalculator());
            this.queue = new BackupFileQueue<FileQueueItem>(dataFilePath);
            this.queue.StateChanged += this.QueueStateChanged;

            this.timer = new Timer { Interval = 5000 };
            this.timer.Elapsed += this.TimerTick;

            this.RemotingManager = remotingManagerParam;
            
            this.CurrentProgress = this.GetCurrentProgress();
            this.CurrentState = (this.CurrentProgress.FilesPending > 0) ? BackupStatus.Pending : BackupStatus.Empty;

            this.InitServices(remotingManagerParam, uploadService);

            this.timer.Start();
        }

        /// <summary>
        /// Init services
        /// </summary>
        /// <param name="remotingManagerParam">remote manager object</param>
        /// <param name="uploadService">upload service to init</param>
        private void InitServices(RemotingManager remotingManagerParam, IUploadService uploadService)
        {
            this.RemotingManager = remotingManagerParam;

            if (this.UploadService != null)
            {
                this.UploadService.UploadFileError -= this.UploadService_UploadFileError;
                this.UploadService.UploadFileCompleted -= ScheduledFileManager.Instance.FileUploadCompleteHandler;
                this.UploadService.UploadFileCompleted -= this.UploadService_UploadFileCompleted;
                this.UploadService.UploadPartStarting -= this.UploadService_UploadPartStarting;
                this.UploadService.SpeedMan.SpeedChanged -= this.SpeedMan_SpeedChanged;
            }

            this.UploadService = uploadService;

            this.UploadService.UploadFileError += this.UploadService_UploadFileError;
            this.UploadService.UploadFileCompleted += ScheduledFileManager.Instance.FileUploadCompleteHandler;
            this.UploadService.UploadFileCompleted += this.UploadService_UploadFileCompleted;
            this.UploadService.UploadPartStarting += this.UploadService_UploadPartStarting;
            this.UploadService.SpeedMan.SpeedChanged += this.SpeedMan_SpeedChanged;

            initEventHandle.Set();
        }

        /// <summary>
        /// Handles the SpeedChanged event of the SpeedMan control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        private void SpeedMan_SpeedChanged(object sender, SpeedChangedEventArgs args)
        {
            this.RemotingManager.ProxyPublisher.OnBackupSpeedChanged(args);
        }

        /// <summary>
        /// Set failed status to file
        /// </summary>
        /// <param name="item">file item to process</param>
        private void SetFileFailed(FileQueueItem item)
        {
            item.FailedCount++;
            item.Force = false;
        }

        /// <summary>
        /// Event handler for upload error
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void UploadService_UploadFileError(object sender, DownloadFileErrorEventArgs e)
        {
            this.IsLimitReached = e.IsLimitReached;
            this.IsInvalidTime = e.IsInvalidTime;
            this.UsedSpace = e.UsedSpace;

            this.IsComputerExpired = e.IsComputerExpired;

            this.SetFileFailed(this.CurrentFile);
            this.CurrentProgress.ErrorMessage = e.Exception.ToString();

            this.InitializeBackup();
        }

        /// <summary>
        /// Reset init event for client app
        /// </summary>
        public static void ResetInitEvent()
        {
            initEventHandle.Reset();
        }

        /// <summary>
        /// call this method to make sure that repository is already inited
        /// </summary>
        /// <param name="timeout">The time out</param>
        /// <returns>Return bool value</returns>
        public static bool EnsureInited(int timeout)
        {
            return initEventHandle.WaitOne(timeout, false);
        }

        /// <summary>
        /// Event handler when event StateChanged occurs from queue
        /// </summary>
        /// <param name="sender">sender object (queue)</param>
        /// <param name="e">event argument, empty - contains no data</param>
        public void QueueStateChanged(object sender, EventArgs e)
        {
            Progress curProgress = this.GetCurrentProgress();

            if (this.CurrentState != BackupStatus.InProgress && curProgress.FilesPending > 0)
            {
                this.CurrentState = BackupStatus.Pending;
            }

            if (this.queue.Count == 0)
            {
                this.CurrentState = BackupStatus.Empty;
            }
            else
            {
                if (curProgress.FilesCompleted == this.queue.Count)
                {
                    this.CurrentState = BackupStatus.Completed;
                }

                if (curProgress.FilesErrors == this.queue.Count)
                {
                    this.CurrentState = BackupStatus.Failed;
                }
            }

            this.CurrentProgress = curProgress;
            this.ReportProgress();
        }

        /// <summary>
        /// Handles the UploadPartStarting event of the UploadService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void UploadService_UploadPartStarting(object sender, CancelEventArgs e)
        {
            e.Cancel = !ScheduleManager.Scheduler.IsCurrentDateInSchedule(false);
            if (e.Cancel)
            {
                this.CurrentState = (this.GetCurrentProgress().FilesPending > 0) ? BackupStatus.Pending : BackupStatus.Completed;
                this.timer.Start();
            }
        }

        /// <summary>
        /// Timers the tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerTick(object sender, ElapsedEventArgs e)
        {
            this.CurrentState = this.CurrentState;

            // Check if backing up process is not disable and is not paused
            if (this.IsBackupEnabled)
            {
                if (this.CurrentState == BackupStatus.Pending || ScheduleManager.Scheduler.IsASAP)
                {
                    this.timer.Stop();

                    if (!RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized)
                    {
                        RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized = true;
                    }

                    if (RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized)
                    {
                        this.InitializeBackup();
                    }
                    else
                    {
                        this.timer.Start();
                        ScheduleManager.Scheduler.IsASAP = false;
                        this.CurrentState = BackupStatus.Failed;
                        this.CurrentProgress.ErrorMessage =
                            RemotingManager.ProxyManager.AppManger.SyncronizationManager.GetErrorMessage();
                        Loger.Instance.Log("Backup repository: Users not syncronized");
                    }

                    this.ReportProgress();
                }
            }
        }

        /// <summary>
        /// Initializes the backup.
        /// </summary>
        private void InitializeBackup()
        {
            Loger.Instance.Log("Backup repository: Initialize Backup");
            // Check if backing up process is not disable and is not paused
            if (this.IsBackupEnabled)
            {
                Loger.Instance.Log("Backup repository: Backup enabled");
                FileQueueItem item = this.queue.GetFileFromQueue();

                while (item != null)
                {
                    if (item.FailedCount > this.failedCount)
                    {
                        this.CurrentState = BackupStatus.Failed;
                        this.failedCount++;

                        Loger.Instance.Log("Backup repository: In queue just failad files: " + this.queue.Count + " Count of tryes: " + this.failedCount);

                        ScheduleManager.Scheduler.IsASAP = false;
                        this.timer.Start();
                        break;
                    }
                    else if ((item.IsUpload && File.Exists(item.FilePath)) || (item.IsRename && File.Exists(item.RenamedFilePath)))
                    {
                        lock (RemotingManager.ProxyManager.AppManger.AppSyncObject)
                        {
                            bool isLockedBefore = item.IsLocked;
                            bool isFileLocked = this.IsFileLocked(item);
                            if (isFileLocked)
                            {
                                if (isLockedBefore)
                                {
                                    this.timer.Start();
                                    break;
                                }
                                else
                                {
                                    item = this.queue.GetFileFromQueue();
                                    continue;
                                }
                            }

                            Loger.Instance.Log("Backup repository: Current file: " + item.FilePath);

                            try
                            {
                                Loger.Instance.Log("Backup repository: Current file size: " + (item.IsRename ? new FileInfo(item.RenamedFilePath).Length : new FileInfo(item.FilePath).Length)); 
                            }
                            catch (Exception ex)
                            {
                                Loger.Instance.Log("Backup repository: Exceptino: " + ex);
                            }

                            this.CurrentFile = item;
                            this.CurrentState = BackupStatus.InProgress;

                            this.CurrentProgress = this.GetCurrentProgress();

                            Loger.Instance.Log("Backup repository: Starting upload");
                            this.UploadService.StartUpload(
                                item,
                                0,
                                SettingsManager.Settings.CurrentComputerEncryptInfo);
                        }

                        break;
                    }

                    Loger.Instance.Log("Backup repository: File " + item.FilePath + " was failed for backup");
                    // skip the file
                    this.SetFileFailed(item);

                    continue;
                }

                if (item == null)
                {
                    this.CurrentState = BackupStatus.Completed;
                    this.failedCount = 0;
                    this.IsLimitReached = false;
                    this.IsComputerExpired = false;
                    this.UsedSpace = 0;
                    ScheduleManager.Scheduler.IsASAP = false;
                    this.timer.Start();
                }

                this.CurrentProgress = this.GetCurrentProgress();
                this.ReportProgress();
            }
            else
            {
                Loger.Instance.Log("Backup repository: Backup disabled");
                this.CurrentState = (this.CurrentProgress.FilesPending > 0) ? BackupStatus.Pending : BackupStatus.Completed;
                this.timer.Start();
            }
        }

        /// <summary>
        /// Handles the UploadFileCompleted event of the UploadService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UploadFileEventArgs"/> instance containing the event data.</param>
        private void UploadService_UploadFileCompleted(object sender, UploadFileEventArgs e)
        {
            lock (SyncObject)
            {
                this.queue.SetFileState(e.FilePath, true);
            }

            this.InitializeBackup();
        }

        /// <summary>
        /// Initializes the specified remoting manager param.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        /// <param name="uploadService">The upload service.</param>
        /// <param name="dataFilePath">The data file path.</param>
        public static void Initialize(RemotingManager remotingManagerParam, IUploadService uploadService, string dataFilePath)
        {
            if (String.IsNullOrEmpty(dataFilePath))
            {
                throw new ArgumentNullException("dataFilePath");
            }

            if (Instance == null)
            {
                Loger.Instance.Log("Backup repository: Initialize BackupRepository instance");
                Instance = new BackupRepository(remotingManagerParam, uploadService, dataFilePath);
            }
            else
            {
                Loger.Instance.Log("Backup repository: Update BackupRepository instance");
                Instance.InitServices(remotingManagerParam, uploadService);
            }
        }

        /// <summary>
        /// Return true if Backup process is enabled
        /// </summary>
        private bool IsBackupEnabled
        { 
            get
            {
                //Loger.Instance.Log("Backup repository: Is backup enabled " +
                //    !RemotingManager.ProxyManager.AppManger.Disable +
                //    (RemotingManager.ProxyManager.AppManger.GetPause() == 0) +
                //    !SettingsManager.Settings.IsRecoverMode +
                //    ScheduleManager.Scheduler.IsCurrentDateInSchedule(true));
                return (!RemotingManager.ProxyManager.AppManger.Disable) &&
                       (RemotingManager.ProxyManager.AppManger.GetPause() == 0) &&
                       (!SettingsManager.Settings.IsRecoverMode) &&
                       (ScheduleManager.Scheduler.IsCurrentDateInSchedule(true));
            }
        }

        /// <summary>
        /// Determines whether [is file locked] [the specified item].
        /// </summary>
        /// <param name="item">The file queue item.</param>
        /// <returns>
        /// 	<c>true</c> if [is file locked] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsFileLocked(FileQueueItem item)
        {
            var restoreFile = this.RemotingManager.ProxyManager.BackupManager.GetCurrentRestoreFile();
            if ((restoreFile != null) && (restoreFile.RestoreFilePath.Equals(item.FilePath)))
            {
                item.IsLocked = true;
                return true;
            }
            else
            {
                item.IsLocked = false;
                return false;  
            }
        }

        #region Public inteface

        /// <summary>
        /// Sets the file force flag in backup queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void SetFileForce(string filePath, bool force)
        {
            lock (SyncObject)
            {
                this.queue.SetFileForce(filePath, force);
            }
        }

        /// <summary>
        /// Adds the file to backup queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void AddFile(string filePath, bool force)
        {
            lock (SyncObject)
            {
                FileQueueItem item = new FileQueueItem { FilePath = filePath, Force = force, IsUpload = true };
                this.queue.AddFileToQueue(item);
            }
        }

        /// <summary>
        /// Adds the files to backup queue
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void AddFileCollection(string[] filePathes, bool force)
        {
            lock (SyncObject)
            {
                List<FileQueueItem> items = new List<FileQueueItem>();

                foreach (string filePath in filePathes)
                {
                    items.Add(new FileQueueItem { FilePath = filePath, Force = force, IsUpload = true });
                }

                this.queue.AddFileCollectionToQueue(items);
            }
        }

        /// <summary>
        /// Rename files in backup
        /// </summary>
        /// <param name="files">File pathes for rename</param>
        public void RenameFiles(Dictionary<string, string> files)
        {
            lock (SyncObject)
            {
                List<FileQueueItem> fileItems = new List<FileQueueItem>();

                foreach (KeyValuePair<string, string> file in files)
                {
                    fileItems.Add(new FileQueueItem { FilePath = file.Key, RenamedFilePath = file.Value, IsRename = true });
                }

                this.queue.AddFileCollectionToQueue(fileItems);
            }
        }

        /// <summary>
        /// Removes the file from backup queue.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public void RemoveFile(string filePath)
        {
            lock (SyncObject)
            {
                this.queue.RemoveFileFromQueue(filePath);
            }
        }

        /// <summary>
        /// Removes files from backup queue
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        public void RemoveFileCollection(string[] filePathes)
        {
            lock (SyncObject)
            {
                this.queue.RemoveFileCollectionFromQueue(filePathes);
            }
        }

        /// <summary>
        /// Gets the current progress.
        /// </summary>
        /// <returns>The current backup progress.</returns>
        private Progress GetCurrentProgress()
        {
            return new Progress(new BackupPercentCalculator())
                        {
                            FilesTotal = RemotingManager.ProxyManager.ScheduledFileManager.GetCountOfBackedUpFiles(),
                            FilesTotalSize = SettingsManager.Settings.CurrentComputer == null ? 0 : SettingsManager.Settings.CurrentComputer.BytesUsed,
                            FilesPending = this.queue.Count - this.queue.CompletedCount,
                            FilesPendingSize = this.queue.Size - this.queue.CompletedSize,
                            FilesCompleted = this.queue.CompletedCount,
                            FilesCompletedSize = this.queue.CompletedSize,
                            ErrorMessage = string.Empty,
                            CurrentFilePath = this.CurrentFile != null ? this.CurrentFile.IsRename ? this.CurrentFile.RenamedFilePath : this.CurrentFile.FilePath : string.Empty
                        };
        }

        /// <summary>
        /// Cancels the backuping.
        /// </summary>
        public void CancelBackuping()
        {
            Loger.Instance.Log("Backup repository: Cancel backuping");
            var files = this.queue.GetAllFiles();

            foreach (var fileQueueItem in files)
            {
                if (!fileQueueItem.Value.Completed)
                {
                    this.SetFileFailed(fileQueueItem.Value);
                    fileQueueItem.Value.FailedCount = this.failedCount + 3;
                }
            }

            this.CurrentProgress.ErrorMessage = "Backup was canceled";
            this.CurrentState = BackupStatus.Empty;
            this.UploadService.CancelUpload();
            this.ReportProgress();
            this.CurrentFile = null;
            this.timer.Start();
        }

        public void ClearQueue()
        {
            this.queue.RemoveFileCollectionFromQueue(this.queue.GetAllFiles().Select(f => f.Key).ToArray());
        }

        /// <summary>
        /// Reports the progress.
        /// </summary>
        private void ReportProgress()
        {
            Loger.Instance.Log("Backup repository: Raise progress changed event");

            this.RemotingManager.ProxyPublisher.OnBackupProgressChanged(new ProgressChangedEventArgs
            {
                CurrentProgress = this.CurrentProgress
            });
        }

        /// <summary>
        /// Gets the current file.
        /// </summary>
        /// <returns>Current File</returns>
        public FileQueueItem GetCurrentFile()
        {
            return this.CurrentFile;
        }

        #endregion

        /// <summary>
        /// Dispose this object
        /// </summary>
        public void Dispose()
        {
            this.timer.Stop();
            this.timer.Dispose();
            this.UploadService.UploadFileError -= this.UploadService_UploadFileError;
            this.UploadService.UploadFileCompleted -= ScheduledFileManager.Instance.FileUploadCompleteHandler;
            this.UploadService.UploadFileCompleted -= this.UploadService_UploadFileCompleted;
            this.UploadService.UploadPartStarting -= this.UploadService_UploadPartStarting;
            this.UploadService.SpeedMan.SpeedChanged -= this.SpeedMan_SpeedChanged;
        }
    }
}