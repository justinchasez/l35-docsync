using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Core.Utils;
using Share430.Client.Remoting.Args;
using Share430.Client.Scheduler;
using Share430.Client.Service.Core.Managers;

namespace Share430.Client.Service.Core.Repositories
{
    /// <summary>
    /// The restore file repository.
    /// </summary>
    public class RestoreRepository : IDisposable
    {
        #region Private fields

        /// <summary>
        /// The synchronization object.
        /// </summary>
        private static readonly object SyncObject = new object();

        /// <summary>
        /// The internal timer.
        /// </summary>
        private readonly Timer timer;

        /// <summary>
        /// The files queue.
        /// </summary>
        private readonly RestoreFileQueue<RestoreFileQueueItem> queue;

        /// <summary>
        /// The current process state.
        /// </summary>
        private RestoreStatus currentState;

        /// <summary>
        /// Failed count
        /// </summary>
        private int failedCount;

        /// <summary>
        /// Is computer expired
        /// </summary>
        private bool isComputerExpired;

        /// <summary>
        /// Is invalid time
        /// </summary>
        private bool isInvalidTime;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreRepository"/> class.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        /// <param name="downloadService">The download service.</param>
        /// <param name="dataFilePath">The data file path.</param>
        private RestoreRepository(RemotingManager remotingManagerParam, IDownloadService downloadService, string dataFilePath)
        {
            if (remotingManagerParam == null)
            {
                throw new ArgumentNullException("remotingManagerParam");
            }

            this.queue = new RestoreFileQueue<RestoreFileQueueItem>(dataFilePath);
            this.queue.StateChanged += this.QueueStateChanged;

            this.InitServices(remotingManagerParam, downloadService);

            this.timer = new Timer { Interval = 5000 };
            this.timer.Elapsed += this.TimerTick;

            this.CurrentProgress = this.GetCurrentProgress();
            this.UpdateCurrentState();

            if (this.CurrentState == RestoreStatus.InProgress)
            {
                this.CurrentState = RestoreStatus.Pending;
            }

            this.timer.Start();
        }

        #region Properties

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static RestoreRepository Instance { get; private set; }

        /// <summary>
        /// Init services
        /// </summary>
        /// <param name="remotingManagerParam">remote manager object</param>
        /// <param name="downloadService">download service</param>
        private void InitServices(RemotingManager remotingManagerParam, IDownloadService downloadService)
        {
            this.RemotingManager = remotingManagerParam;

            if (this.DownloadService != null)
            {
                this.DownloadService.DownloadError -= this.DownloadService_DownloadFileError;
                this.DownloadService.DownloadCompleted -= this.DownloadService_DownloadCompleted;
                this.DownloadService.DownloadPartStarting -= this.DownloadService_DownloadPartStarting;
                this.DownloadService.SpeedMan.SpeedChanged -= this.SpeedMan_SpeedChanged;
            }

            this.DownloadService = downloadService;

            this.DownloadService.DownloadError += this.DownloadService_DownloadFileError;
            this.DownloadService.DownloadCompleted += this.DownloadService_DownloadCompleted;
            this.DownloadService.DownloadPartStarting += this.DownloadService_DownloadPartStarting;

            this.DownloadService.SpeedMan.SpeedChanged += this.SpeedMan_SpeedChanged;
        }

        /// <summary>
        /// Handles the SpeedChanged event of the SpeedMan control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="args">The <see cref="SpeedChangedEventArgs"/> instance containing the event data.</param>
        private void SpeedMan_SpeedChanged(object sender, SpeedChangedEventArgs args)
        {
            this.RemotingManager.ProxyPublisher.OnRestoreSpeedChanged(args);
        }
         
        /// <summary>
        /// Initializes the specified remoting manager param.
        /// </summary>
        /// <param name="remotingManagerParam">The remoting manager param.</param>
        /// <param name="downloadService">The download service.</param>
        /// <param name="dataFilePath">The data file path.</param>
        public static void Initialize(RemotingManager remotingManagerParam, IDownloadService downloadService, string dataFilePath)
        {
            if (String.IsNullOrEmpty(dataFilePath))
            {
                throw new ArgumentNullException("dataFilePath");
            }

            if (Instance == null)
            {
                Loger.Instance.Log("Restore repository: Initialize RestoreRepository instance");
                Instance = new RestoreRepository(remotingManagerParam, downloadService, dataFilePath);
            }
            else
            {
                Loger.Instance.Log("Restore repository: Update RestoreRepository instance");
                Instance.InitServices(remotingManagerParam, downloadService);
            }
        }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        public RestoreStatus CurrentState
        {
            get { return this.currentState; }
            private set
            {
                if (this.currentState != value)
                {
                    Loger.Instance.Log("Restore repository: Raise event current stane changed to " + value);
                }

                this.currentState = value;
                this.RemotingManager.ProxyPublisher.OnRestoreStatusChanged(new RestoreStatusChangedEventArgs
                                                                               {
                                                                                   Status = value,
                                                                                   IsComputerExpired = this.isComputerExpired,
                                                                                   IsInvalidTime = this.isInvalidTime
                                                                               });
                this.isComputerExpired = false;
                if (value == RestoreStatus.Failed)
                {
                    this.isInvalidTime = false;
                }
            }
        }

        /// <summary>
        /// Gets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        public RemotingManager RemotingManager { get; private set; }

        /// <summary>
        /// Gets the download service.
        /// </summary>
        /// <value>The download service.</value>
        public IDownloadService DownloadService { get; private set; }

        /// <summary>
        /// Current downloading file
        /// </summary>
        private RestoreFileQueueItem CurrentFile { get; set; }
        
        /// <summary>
        /// GetsB the current file.
        /// </summary>
        /// <returns>The current file.</returns>
        public RestoreFileQueueItem GetCurrentFile()
        {
            lock (SyncObject)
            {
                return this.CurrentFile;
            }
        }
        
        /// <summary>
        /// Gets the current progress.
        /// </summary>
        /// <value>The current progress.</value>
        public Progress CurrentProgress { get; private set; }

        /// <summary>
        /// Gets the current progress.
        /// </summary>
        /// <returns>The current restore progress.</returns>
        public Progress GetCurrentProgress()
        {
            long errorFilesSize;
            int fileErrors = this.FilesErrorsCount(out errorFilesSize);

            return new Progress(new RestorePercentCalculator())
                       {
                           FilesErrors = fileErrors,
                           FilesTotal = this.queue.Count,
                           FilesTotalSize = this.queue.Size,
                           FilesPending = this.queue.Count - this.queue.CompletedCount,
                           FilesPendingSize = this.queue.Size - this.queue.CompletedSize - errorFilesSize,
                           FilesCompleted = this.queue.CompletedCount,
                           FilesCompletedSize = this.queue.CompletedSize,
                           CurrentFilePath = this.CurrentFile != null ? this.CurrentFile.RestoreFilePath : null
                       };
        }

        /// <summary>
        /// Gets the files errors count.
        /// </summary>
        /// <param name="filesSize">Size of the files.</param>
        /// <returns>files errors count</returns>
        /// <value>The files errors count.</value>
        private int FilesErrorsCount(out long filesSize)
        {
            int count = 0;
            long size = 0;
            foreach (KeyValuePair<string, RestoreFileQueueItem> restoreFileQueueItem in this.GetAllFiles)
            {
                if ((!restoreFileQueueItem.Value.Completed) && (restoreFileQueueItem.Value.FailedCount > 0))
                {
                    size += restoreFileQueueItem.Value.Size;
                    count++;
                }
            }

            filesSize = size;
            return count;
        }

        /// <summary>
        /// Updates the state of the current.
        /// </summary>
        public void UpdateCurrentState()
        {
            if (this.CurrentProgress.FilesTotal > 0)
            {
                if (this.CurrentProgress.FilesErrors > 0 && this.CurrentProgress.FilesPending == this.CurrentProgress.FilesErrors)
                {
                    this.CurrentState = RestoreStatus.Failed;
                }
                else if (this.CurrentProgress.FilesPending > 0)
                {
                    this.CurrentState = this.CurrentState != RestoreStatus.InProgress ? RestoreStatus.Pending : RestoreStatus.InProgress;
                }
                else if (this.CurrentProgress.FilesCompleted == this.CurrentProgress.FilesTotal)
                {
                    this.CurrentState = RestoreStatus.Finished;
                }
            }
            else
            {
                this.CurrentState = RestoreStatus.Empty;
            }
        }

        #endregion

        #region Public interface

        /// <summary>
        /// Adds the file.
        /// </summary>
        /// <param name="item">The file item.</param>
        public void AddFile(RestoreFileQueueItem item)
        {
            Loger.Instance.Log("Restore repository: Add file " + item.FilePath);
            item.InProgress = false;
            item.Priority = (item.Force) ? RestorePriority.Highest : RestorePriority.Normal;
            item.FailedCount = 0;

            lock (SyncObject)
            {
                this.queue.AddFileToQueue(item);
            }
        }

        /// <summary>
        /// Cancels the restoring.
        /// </summary>
        public void CancelRestoring()
        {
            Loger.Instance.Log("Restore repository: Cancel restoring");
            var files = this.queue.GetAllFiles();

            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (!fileQueueItem.Value.Completed)
                {
                    this.SetFailed(fileQueueItem.Value, ErrorState.RestoreCanceled);
                    fileQueueItem.Value.LastRestored = DateTime.Now;
                    fileQueueItem.Value.FailedCount = this.failedCount + 3;
                }
            }

            this.queue.SaveChanges();

            this.failedCount++;

            this.CurrentProgress.ErrorMessage = "Restore was canceled";
            this.CurrentState = RestoreStatus.Failed;
            this.DownloadService.CancelDownload();
            this.ReportProgress();
            this.CurrentFile = null;
            this.timer.Start();
        }

        public void ClearQueue()
        {
            this.queue.RemoveFileCollectionFromQueue(this.queue.GetAllFiles().Select(f => f.Key).ToArray());
        }

        /// <summary>
        /// Cancels the restore file.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        public void DeleteFileFromQueue(string fullPath)
        {
            Loger.Instance.Log("Restore repository: delete file " + fullPath);
            lock (SyncObject)
            {
                this.queue.RemoveFileFromQueue(fullPath);
            }
            
            if ((this.CurrentFile != null) && (string.Equals(this.CurrentFile.FilePath, fullPath)))
            {
                this.DownloadService.CancelDownload();
                this.CurrentFile = null;
                this.timer.Start();
            }
        }

        /// <summary>
        /// Sets the file priority.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <param name="priority">The new priority.</param>
        /// <returns>return true if priority was set successfully</returns>
        public bool SetFilePriority(string fullPath, RestorePriority priority)
        {
            var file = this.queue.GetFileFromQueue(fullPath);
            if (file == null)
            {
                return false;
            }

            file.Priority = priority;

            return true;
        }

        /// <summary>
        /// Gets the get completed files.
        /// </summary>
        /// <value>The get completed files.</value>
        public Dictionary<string, RestoreFileQueueItem> GetAllFiles
        {
            get
            {
                lock (SyncObject)
                {
                    return this.queue.GetAllFiles();
                }
            }
        }

        #endregion

        #region Private members

        /// <summary>
        /// Handles the DownloadCompleted event of the DownloadService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DownloadFileEventArgs"/> instance containing the event data.</param>
        private void DownloadService_DownloadCompleted(object sender, DownloadFileEventArgs e)
        {
            Loger.Instance.Log("Restore repository: Download completed");

            this.queue.SetFileState(e.RestoreFileInfo.FilePath, true);
            if (!String.Equals(e.RestoreFileInfo.FilePath, e.RestoreFileInfo.RestoreFilePath))
            {
                string newFilePath = e.RestoreFileInfo.RestoreFilePath;
                // Add existing file "e.RestoreFileInfo.FilePath" to DB  with new path "e.RestoreFileInfo.RestoreFilePath"
                BackupFileResultModel result = RemotingManager.ProxyManager.BackupManager.AddExistingFileToBackup(e.RestoreFileInfo.ComputerId, GetRelativePath(e.RestoreFileInfo.FilePath), GetRelativePath(newFilePath));

                UploadFileEventArgs eventArgs = new UploadFileEventArgs
                                                    {
                                                        FileInfo = new FileInfo(newFilePath),
                                                        FilePath = newFilePath,
                                                        VersionsCount = (result != null) ? result.VersionsCount : 1
                                                    };
                RemotingManager.ProxyManager.ScheduledFileManager.FileUploadCompleteHandler(this, eventArgs);
            }
            else
            {
                UploadFileEventArgs eventArgs = new UploadFileEventArgs
                {
                    FileInfo = new FileInfo(e.RestoreFileInfo.RestoreFilePath),
                    FilePath = e.RestoreFileInfo.RestoreFilePath,
                    VersionsCount = 0
                };
                RemotingManager.ProxyManager.ScheduledFileManager.FileUploadCompleteHandler(this, eventArgs);
            }

            if (this.CurrentFile != null)
            {
                this.CurrentFile.LastRestored = DateTime.Now;
                this.CurrentFile = null;
            }

            this.InitializeRestore();
        }

        /// <summary>
        /// Event handler for upload error
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">event argument</param>
        private void DownloadService_DownloadFileError(object sender, DownloadFileErrorEventArgs e)
        {
            Loger.Instance.Log("Restore repository: Download file error");
            this.isComputerExpired = e.IsComputerExpired;
            this.isInvalidTime = e.IsInvalidTime;

            if (this.CurrentFile != null)
            {
                this.SetFailed(this.CurrentFile, ErrorState.AnotherError);
                this.CurrentFile.LastRestored = DateTime.Now;
                this.queue.AddFileToQueue(this.CurrentFile);
                this.CurrentFile = null;
            }

            this.CurrentProgress.ErrorMessage = e.Exception.ToString();
            this.InitializeRestore();
        }

        /// <summary>
        /// Sets the failed.
        /// </summary>
        /// <param name="restoreFileQueueItem">The restore file queue item.</param>
        /// <param name="errorState">State of the error.</param>
        private void SetFailed(RestoreFileQueueItem restoreFileQueueItem, ErrorState errorState)
        {
            restoreFileQueueItem.FailedCount++;
            restoreFileQueueItem.Priority = RestorePriority.Lowest;
            restoreFileQueueItem.State = errorState;
        }

        /// <summary>
        /// Event handler when event StateChanged occurs from queue
        /// </summary>
        /// <param name="sender">sender object (queue)</param>
        /// <param name="e">event argument, empty - contains no data</param>
        private void QueueStateChanged(object sender, EventArgs e)
        {
            this.CurrentProgress = this.GetCurrentProgress();
            this.UpdateCurrentState();
            this.ReportProgress();
        }

        /// <summary>
        /// Handles the DownloadPartStarting event of the DownloadService control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        private void DownloadService_DownloadPartStarting(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !ScheduleManager.Scheduler.IsCurrentDateInSchedule(false);
            if (e.Cancel)
            {
                this.CurrentState = (this.CurrentProgress.FilesPending > 0) ? RestoreStatus.InProgress : RestoreStatus.Finished;
            }
        }

        /// <summary>
        /// Timers the tick.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void TimerTick(object sender, ElapsedEventArgs e)
        {
            this.CurrentState = this.CurrentState;

            if (!RemotingManager.ProxyManager.AppManger.Disable)
            {
                if (this.CurrentState == RestoreStatus.Pending)
                {
                    this.timer.Stop();

                    if (!RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized)
                    {
                        RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized = true;
                    }

                    if (RemotingManager.ProxyManager.AppManger.SyncronizationManager.IsUsersSyncronized)
                    {
                        this.InitializeRestore();
                    }
                    else
                    {
                        this.SetErrorToAllFiles();
                        this.timer.Start();
                        this.CurrentState = RestoreStatus.Failed;
                        this.CurrentProgress.ErrorMessage =
                            RemotingManager.ProxyManager.AppManger.SyncronizationManager.GetErrorMessage();
                        Loger.Instance.Log("Restore repository: Users not syncronized");
                    }

                    this.ReportProgress();
                }
                else
                {
                    this.ReportProgress();
                }
            }
            else
            {
                Loger.Instance.Log("Restore repository: Application disabled");
            }
        }

        /// <summary>
        /// Reports the progress.
        /// </summary>
        private void ReportProgress()
        {
            Loger.Instance.Log("Restore repository: Report progress");

            this.RemotingManager.ProxyPublisher.OnRestoreProgressChanged(new ProgressChangedEventArgs
            {
                CurrentProgress = this.CurrentProgress
            });

            this.UpdateCurrentState();
        }

        /// <summary>
        /// Initializes the restore.
        /// </summary>
        private void InitializeRestore()
        {
            Loger.Instance.Log("Restore repository: Initialize restore");
            if (!RemotingManager.ProxyManager.AppManger.Disable)
            {
                RestoreFileQueueItem item = this.queue.GetFileFromQueue();

                while (item != null)
                {

                    RemotingManager.ProxyManager.ScheduledFileManager.DontBackThisUp(Path.Combine(SettingsManager.Settings.TargetFolderLocation, item.RestoreFilePath));

                    if (item.FailedCount > this.failedCount)
                    {
                        Loger.Instance.Log("Restore repository: In queue jast failed files");
                        this.CurrentState = RestoreStatus.Failed;
                        this.failedCount++;
                        this.timer.Start();
                        break;
                    }
                    else
                    {
                        lock (RemotingManager.ProxyManager.AppManger.AppSyncObject)
                        {
                            bool isLockedBefore = item.IsLocked;
                            if (this.IsFileLocked(item))
                            {
                                Loger.Instance.Log("Restore repository: File locked");
                                if (isLockedBefore)
                                {
                                    this.timer.Start();
                                    break;
                                }
                                else
                                {
                                    item = this.queue.GetFileFromQueue();
                                    continue;
                                }
                            }

                            this.CurrentFile = item;
                            this.CurrentState = RestoreStatus.InProgress;
                            this.CurrentProgress = this.GetCurrentProgress();

                            Loger.Instance.Log("Restore repository: Restore " + item.FilePath + " to " + item.RestoreFilePath);

                            this.DownloadService.StartDownload(item);
                        }

                        break;
                    }
                }

                if (item == null)
                {
                    this.CurrentState = RestoreStatus.Finished;
                    this.failedCount = 0;
                    this.timer.Start();
                }
            }
            else
            {
                Loger.Instance.Log("Restore repository: Application disabled");
                this.UpdateCurrentState();
                this.timer.Start();
            }
        }

        /// <summary>
        /// Determines whether [is file locked] [the specified item].
        /// </summary>
        /// <param name="item">The file queue item.</param>
        /// <returns>
        /// 	<c>true</c> if [is file locked] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsFileLocked(RestoreFileQueueItem item)
        {
            var backupFile = this.RemotingManager.ProxyManager.BackupManager.GetCurrentBackupFile();
            if ((backupFile != null) && 
                ((item.RestoreFilePath.Equals(backupFile.FilePath)) || (item.FilePath.Equals(backupFile.FilePath))) && 
                (!backupFile.Completed))
            {
                item.IsLocked = true;
                return true;
            }
            else
            {
                item.IsLocked = false;
                return false;
            }
        }

        /// <summary>
        /// Cancels the restoring.
        /// </summary>
        public void SetErrorToAllFiles()
        {
            Loger.Instance.Log("Restore repository: Set failed status to all files");
            var files = this.queue.GetAllFiles();

            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (!fileQueueItem.Value.Completed)
                {
                    this.SetFailed(fileQueueItem.Value, ErrorState.AnotherError);
                    fileQueueItem.Value.LastRestored = DateTime.Now;
                    fileQueueItem.Value.FailedCount = this.failedCount + 3;
                }
            }

            this.failedCount++;

            this.queue.SaveChanges();
        }

        private string GetRelativePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (!path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return path;
               // Loger.Instance.Log(string.Format("Upload Service: File in invalid location: {0} should be in {1} ", path, basePath));
            }

            return path.Substring(basePath.Length).Trim('\\');
        }

        #endregion

        /// <summary>
        /// Dispose RestoreReposytory instance
        /// </summary>
        public void Dispose()
        {
            this.timer.Stop();
            this.timer.Dispose();
            this.DownloadService.DownloadError -= this.DownloadService_DownloadFileError;
            this.DownloadService.DownloadCompleted -= this.DownloadService_DownloadCompleted;
            this.DownloadService.DownloadPartStarting -= this.DownloadService_DownloadPartStarting;

            this.DownloadService.SpeedMan.SpeedChanged -= this.SpeedMan_SpeedChanged;
        }
    }
}