namespace Share430.Client.Service.Core.Enums
{
    /// <summary>
    /// The upload/downlad services state.
    /// </summary>
    internal enum ServiceState
    {
        /// <summary>
        /// When the period is out of scheduling.
        /// </summary>
        OutOfSchedule,

        /// <summary>
        /// Service is working now
        /// </summary>
        InProgress,
        
        /// <summary>
        /// Waiting for a files in queue
        /// </summary>
        Waiting,
        
        /// <summary>
        /// Service is paused.
        /// </summary>
        OnHold
    }
}