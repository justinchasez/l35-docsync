using System;
using System.Collections.Generic;
using System.Threading;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Remoting.Managers.Interfaces;
using Share430.Client.Service.Core.Repositories;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote backup session manager
    /// </summary>
    [Serializable]
    public class RemoteRecoveryLogsManager : MarshalByRefObject, IRemoteRecoveryLogsManager 
    {
        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region Constants

        /// <summary>
        /// File prefix
        /// </summary>
        private const string FILE_PREFIX = "1";

        /// <summary>
        /// Directory prefix
        /// </summary>
        private const string DIRECTORY_PREFIX = "0";

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion

        #region Public

        /// <summary> Sets the file priority.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="priority">The priority.</param>
        /// <returns>Results of operation.</returns>
        public bool SetFilePriority(string filePath, RestorePriority priority)
        {
            return RestoreRepository.Instance.SetFilePriority(filePath, priority);
        }

        /// <summary>
        /// Cancels the restore file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public void CancelRestoreFile(string filePath)
        {
            ThreadPool.QueueUserWorkItem(
                        state => RestoreRepository.Instance.DeleteFileFromQueue(filePath));
        }

        /// <summary>
        /// Gets the entries.
        /// </summary>
        /// <param name="type">The type of recovery logs.</param>
        /// <returns>All files from specified recovery logs </returns>
        public List<string> GetEntries(RecoveryLogTypes type)
        {
            string pps = SettingsManager.Settings.RestoreDataFilePath;

            switch (type)
            {
                case RecoveryLogTypes.Completed:
                    return this.GetCompletedEntries(); 
                case RecoveryLogTypes.Errors:
                    return this.GetErrorsEntries(); 
                case RecoveryLogTypes.Pending:
                    return this.GetPendingEntries();
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        #endregion

        #region Private

        /// <summary>
        /// Gets the errors entries.
        /// </summary>
        /// <returns>Errors entries</returns>
        private List<string> GetErrorsEntries()
        {
            var files = RestoreRepository.Instance.GetAllFiles;

            var filesList = new List<string>();

            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (!fileQueueItem.Value.Completed && fileQueueItem.Value.FailedCount > 0)
                {
                    filesList.Add(fileQueueItem.Key);
                }
            }

            return filesList;
        }

        /// <summary>
        /// Gets the completed entries.
        /// </summary>
        /// <returns>Completed entries</returns>
        private List<string> GetCompletedEntries()
        {
            var files = RestoreRepository.Instance.GetAllFiles;

            var filesList = new List<string>();

            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (fileQueueItem.Value.Completed)
                {
                    filesList.Add(fileQueueItem.Key);
                }
            }

            return filesList;
        }

        /// <summary>
        /// Gets the pending entries.
        /// </summary>
        /// <returns>Pending entries</returns>
        private List<string> GetPendingEntries()
        {
            var files = RestoreRepository.Instance.GetAllFiles;
            
            var filesList = new List<string>();

            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (!fileQueueItem.Value.Completed && fileQueueItem.Value.FailedCount == 0)
                {
                    filesList.Add(fileQueueItem.Key);
                }
            }

            return filesList;
        }

        /// <summary>
        /// Gets the file queue item.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>Return file queue item info</returns>
        public RestoreFileQueueItem GetFileQueueItem(string filePath)
        {
            var files = RestoreRepository.Instance.GetAllFiles;
            foreach (KeyValuePair<string, RestoreFileQueueItem> fileQueueItem in files)
            {
                if (fileQueueItem.Key.Equals(filePath))
                {
                    return fileQueueItem.Value;
                }
            }

            return null;
        }

        #endregion
    }
}