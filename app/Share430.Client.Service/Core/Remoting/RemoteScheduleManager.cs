using System;
using System.Collections.Generic;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Schedule.Interface;
using Share430.Client.Remoting.Managers.Interfaces;
using Share430.Client.Scheduler;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remoting schedule manager.
    /// </summary>
    [Serializable]
    public class RemoteScheduleManager : MarshalByRefObject, IRemoteScheduleManager
    {
        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IScheduleManager Members

        /// <summary>
        /// Is Backup ASAP
        /// </summary>
        public bool IsASAP
        {
            get
            {
                return ScheduleManager.Scheduler.IsASAP;
            }

            set
            {
                ScheduleManager.Scheduler.IsASAP = value;
            }
        }

        /// <summary>
        /// Get or set is schedules changed
        /// </summary>
        public bool IsSchedulesChanged
        {
            get
            {
                return ScheduleManager.Scheduler.IsSchedulesChanged;
            }

            set
            {
                ScheduleManager.Scheduler.IsSchedulesChanged = value;
            }
        }

        /// <summary>
        /// Get schedules.
        /// </summary>
        public Dictionary<Guid, ISchedule> Schedules
        {
            get
            {
                return ScheduleManager.Scheduler.Schedules;
            }
        }

        /// <summary>
        /// Initializes the schedules.
        /// </summary>
        /// <returns>The list of existing schedules.</returns>
        public Dictionary<Guid, ISchedule> InitializeSchedules()
        {
            return ScheduleManager.Scheduler.Schedules;
        }

        /// <summary>
        /// Saves the schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        public void SaveSchedule(ISchedule schedule)
        {
            if (ScheduleManager.Scheduler.Schedules.ContainsKey(schedule.Id))
            {
                ScheduleManager.Scheduler.Schedules[schedule.Id] = schedule;
            }
            else
            {
                ScheduleManager.Scheduler.Schedules.Add(schedule.Id, schedule);
            }

            ScheduleManager.SaveSchedules(CoreConstantsHelper.PathForSaveData, ScheduleManager.Scheduler.Schedules);
        }

        /// <summary>
        /// Removes the schedule.
        /// </summary>
        /// <param name="schedule">The schedule.</param>
        public void RemoveSchedule(ISchedule schedule)
        {
            ScheduleManager.Scheduler.Schedules.Remove(schedule.Id);
            ScheduleManager.SaveSchedules(null, ScheduleManager.Scheduler.Schedules);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}