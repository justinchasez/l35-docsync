using System;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Settings;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote setting manager.
    /// </summary>
    [Serializable]
    public class RemoteSettingsManager : MarshalByRefObject, IRemoteSettingsManager
    {
        #region IRemoteSettingsManager Members

        /// <summary>
        /// Loads the settings.
        /// </summary>
        /// <returns>The settigs object.</returns>
        public SettingsInfo LoadSettings()
        {
            return SettingsManager.Settings;
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public void SaveSettings(SettingsInfo settings)
        {
            SettingsManager.Save(CoreConstantsHelper.PathForSaveData, settings);

            ServiceManager.Initialize(SettingsManager.Settings);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}