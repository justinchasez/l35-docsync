using System;
using System.Collections.Generic;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote help manager.
    /// </summary>
    public class RemoteHelpManager : MarshalByRefObject, IRemoteHelpManager
    {
        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IRemoteHelpManager Members

        /// <summary>
        /// Gets the help topics.
        /// </summary>
        /// <returns>Returns the list of topics.</returns>
        public List<string> GetHelpTopics()
        {
            return new List<string> { "Topic 1", "Topic 2", "Topic Last" };
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}