using System;
using System.Timers;
using Share430.Client.Core.Entities.Settings;
using Share430.Client.Core.Enums;
using Share430.Client.Remoting.Args;
using Share430.Client.Remoting.Managers.Interfaces;
using Share430.Client.Scheduler;
using Share430.Client.Service.Core.Managers;
using Share430.Client.Service.Core.Repositories;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote applcation manager.
    /// </summary>
    [Serializable]
    public class RemoteAppManager : MarshalByRefObject, IRemoteAppManager
    {
        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        /// <summary>
        /// Gets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        public RemotingManager RemotingManager { get; set; }

        /// <summary>
        /// Gets or sets the syncronization manager.
        /// </summary>
        /// <value>The syncronization manager.</value>
        public ISyncronizationManager SyncronizationManager { get; set; }

        /// <summary>
        /// The global synchronization object.
        /// </summary>
        private static readonly object SyncObject = new object();

        /// <summary>
        /// Timer for unpause
        /// </summary>
        private Timer pauseTimer;

        #region IRemoteAppManager Members

        /// <summary>
        /// Link to last version of client application
        /// </summary>
        public string LinkToLastVersionApp
        {
            get
            {
                string result = null;

                //try
                //{
                //    result = ServiceManager.UpdateService.GetLinkToLastVersion();
                //}
                //catch (Exception)
                //{
                //}

                return result;
            }
        }

        /// <summary>
        /// Gets the last version of client application
        /// </summary>
        public string LastClientAppVersion
        {
            get
            {
                string result = null;

                //try
                //{
                //    result = ServiceManager.UpdateService.GetLastVersionOfClientApp();
                //}
                //catch (Exception ex)
                //{
                //    Loger.Instance.Log("LastClientAppVersion " + ex.ToString());
                //}

                return result;
            }
        }

        /// <summary>
        /// Show dialog to restore previous version of file
        /// </summary>
        /// <param name="fullPath">full path to the file</param>
        public void ShowRestorePrevVersion(string fullPath)
        {
            this.RemotingManager.ProxyPublisher.OnRestorePreviousVersion(new RestoreFileEventArgs(fullPath));
        }

        /// <summary>
        /// Gets the state of the application enabled.
        /// </summary>
        /// <returns>
        /// True if application enabled, otherwise false.
        /// </returns>
        public bool GetApplicationEnabledState()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Gets the application recovery mode.
        /// </summary>
        /// <returns>
        /// True if application in recovery mode, otherwise false.
        /// </returns>
        public bool GetApplicationRecoveryMode()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Gets the connectionpriority.
        /// </summary>
        /// <returns>The connection priority</returns>
        public ConnectionPriority GetConnectionpriority()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Gets the pause.
        /// </summary>
        /// <returns>The minutes application paused for.</returns>
        public int GetPause()
        {
            SettingsInfo info = RemotingManager.ProxyManager.SettingsManager.LoadSettings();

            return (info.PauseTime == DateTime.MinValue || info.PauseTime < DateTime.Now)
                    ? 0
                    : Convert.ToInt32((info.PauseTime - DateTime.Now).TotalMinutes);
        }

        /// <summary>
        /// Sets the state of the application.
        /// </summary>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        public void SetApplicationState(bool enabled)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sets the application recovery mode.
        /// </summary>
        /// <param name="isRecoveryMode">if set to <c>true</c> [is recovery mode].</param>
        public void SetApplicationRecoveryMode(bool isRecoveryMode)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sets the connectionpriority.
        /// </summary>
        /// <param name="priority">The priority.</param>
        public void SetConnectionpriority(ConnectionPriority priority)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Sets the pause.
        /// </summary>
        /// <param name="minutes">The minutes.</param>
        public void SetPause(int minutes)
        {
            if (minutes > 0)
            {
                RemotingManager.ProxyManager.BackupManager.SetPause();
                if (this.pauseTimer != null)
                {
                    this.pauseTimer.Stop();
                    this.pauseTimer.Dispose();
                    this.pauseTimer = null;
                }

                this.pauseTimer = new Timer(minutes * 60000 + 5000);
                this.pauseTimer.Elapsed += this.PauseTimer_Elapsed;
                this.pauseTimer.Start();
            }
            else
            {
                RemotingManager.ProxyManager.BackupManager.UnsetPause();
            }

            SettingsInfo info = RemotingManager.ProxyManager.SettingsManager.LoadSettings();
            info.PauseTime = DateTime.Now.AddMinutes(minutes);
            RemotingManager.ProxyManager.SettingsManager.SaveSettings(info);
            RemotingManager.ProxyPublisher.OnPaused(new PauseEventArgs(minutes));
        }

        /// <summary>
        /// Pause timer elapsed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void PauseTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.pauseTimer.Stop();
            this.RemotingManager.ProxyPublisher.OnPaused(new PauseEventArgs { Minutes = this.GetPause() });
            BackupRepository.Instance.CurrentState = BackupRepository.Instance.CurrentState;
        }

        /// <summary>
        /// Disable Property, when property is set the app will not backup pending files
        /// </summary>
        public bool Disable
        {
            get
            {
                return this.isDisable;
            }

            set
            {
                this.isDisable = value;

                if (value)
                {
                    ScheduleManager.Scheduler.IsASAP = false;
                }
            }
        }

        /// <summary>
        /// true if app disable backing up process
        /// </summary>
        private bool isDisable;

        /// <summary>
        /// Gets the app synchronization object.
        /// </summary>
        /// <value>The app synchronization object.</value>
        public object AppSyncObject
        {
            get
            {
                return SyncObject;
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}