using System;
using Share430.Client.Core.Entities.Users;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote user manager.
    /// </summary>
    [Serializable]
    public class RemoteUserManager : MarshalByRefObject, IRemoteUserManager
    {
        #region IRemoteUserManager Members

        /// <summary>
        /// Registers the user.
        /// </summary>
        /// <param name="userInfo">The user info.</param>
        /// <returns>The user registration result.</returns>
        public UserRegistrationResult RegisterUser(UserRegistrationInputs userInfo)
        {
           throw new NotImplementedException();
        }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <param name="password">The password.</param>
        /// <returns>
        /// The user ID in validation success, otherwise Guid.Empty.
        /// </returns>
        public bool ValidateUser(string email, string password, out string token)
        {
            return ServiceManager.UsersService.ValidateUser(email, password, out token);
        }

        #endregion
    }
}