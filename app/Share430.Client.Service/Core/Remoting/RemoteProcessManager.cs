using System;
using System.Diagnostics;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote process manager.
    /// </summary>
    [Serializable]
    public class RemoteProcessManager : MarshalByRefObject, IRemoteProcessManager
    {
        #region IRemoteProcessManager Members

        /// <summary>
        /// Starts the process.
        /// </summary>
        /// <param name="appPath">The app path.</param>
        /// <param name="params">The inline params.</param>
        public void StartProcess(string appPath, string @params)
        {
            Process.Start(appPath, @params);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}