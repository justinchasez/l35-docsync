using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Args;
using Share430.Client.Remoting.Managers.Interfaces;
using Share430.Client.Scheduler;
using Share430.Client.Service.Core.Managers;
using Share430.Client.Service.Core.Repositories;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote backup session manager
    /// </summary>
    [Serializable]
    public class RemoteBackupManager : MarshalByRefObject, IRemoteBackupManager 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RemoteBackupManager"/> class.
        /// </summary>
        /// <param name="remotingManager">The remoting manager.</param>
        public RemoteBackupManager(RemotingManager remotingManager)
        {
            this.RemotingManager = remotingManager;
        }

        /// <summary>
        /// Gets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        public RemotingManager RemotingManager { get; set; }

        /// <summary>
        /// Obtains a lifetime service object to control the lifetime policy for this instance.
        /// </summary>
        /// <returns>
        /// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
        /// </returns>
        /// <exception cref="T:System.Security.SecurityException">
        /// The immediate caller does not have infrastructure permission.
        /// </exception>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
        /// </PermissionSet>
        public override object InitializeLifetimeService()
        {
            return null;
        }

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion

        #region Public

        /// <summary>
        /// Get files from server complete event
        /// </summary>
        public event GetFilesFromServerCompleteDelegate GetFilesFromServerComplete;

        /// <summary>
        /// Get files from server
        /// </summary>
        /// <param name="compId">Computer ID</param>
        public void GetFilesFromServer()
        {
            if (this.GetFilesFromServerComplete != null)
            {
                var inputs = new GetAllFilesFromComputerInputs
                                 {
                                     ComputerIdSpecified = true,
                                     FilesCategory = SelectFilesMode.All,
                                     FilesCategorySpecified = true
                                 };

                this.GetFilesFromServerComplete(
                    BackupRepository.Instance.UploadService.FileService.GetAllFilesFromComputer(inputs));
            }
        }

        public DateTime UpdateFilesFromServer(DateTime lastUpdate, EncryptInfo encryptKey)
        {
             if (ServiceManager.ComputersService != null)
            {
                var updates = ServiceManager.ComputersService.GetUpdates(lastUpdate);

                var restoreList = updates.Files.Select(file => new FileModel {FileName = file}).ToList();

                if (restoreList.Count > 0)
                {
                    ThreadPool.QueueUserWorkItem(state => this.RestoreFiles(restoreList, 0, encryptKey));
                }

                updates.Removed.ForEach(i => ThreadPool.QueueUserWorkItem(state => RemotingManager.ProxyManager.ScheduledFileManager.RemoveFileOrFolderFromFileSystem(GetFullPath(i))));

                return updates.CheckDate.ToUniversalTime();
            }

            return lastUpdate;
        }

        private string GetFullPath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            return Path.Combine(basePath, path);
        }


        /// <summary>
        /// Gets the backup progress.
        /// </summary>
        /// <returns>The backup progress.</returns>
        public Progress GetBackupProgress()
        {
            return BackupRepository.Instance != null ? BackupRepository.Instance.CurrentProgress : new Progress(new BackupPercentCalculator());
        }

        /// <summary>
        /// Raise backup progress changed
        /// </summary>
        public void RaiseBackupProgressChanged()
        {
            BackupRepository.Instance.QueueStateChanged(null, null);
        }

        /// <summary>
        /// Get current backup status
        /// </summary>
        /// <returns>the backup status</returns>
        public BackupStatusChangedEventArgs GetBackupStatus()
        {
            return BackupRepository.Instance != null ? new BackupStatusChangedEventArgs()
                       {
                           IsLimitReached = BackupRepository.Instance.IsLimitReached,
                           UsedSpace = BackupRepository.Instance.UsedSpace,
                           Status = BackupRepository.Instance.CurrentState,
                           IsComputerExpired = BackupRepository.Instance.IsComputerExpired
                       }

                       : new BackupStatusChangedEventArgs();
        }

        /// <summary>
        /// Gets the restore progress.
        /// </summary>
        /// <returns>The restore progress.</returns>
        public Progress GetRestoreProgress()
        {
            return RestoreRepository.Instance != null ? RestoreRepository.Instance.CurrentProgress : new Progress(new RestorePercentCalculator());
        }

        /// <summary>
        /// Get current restore status
        /// </summary>
        /// <returns>the restore status</returns>
        public RestoreStatus GetRestoreStatus()
        {
            return RestoreRepository.Instance != null ? RestoreRepository.Instance.CurrentState : new RestoreStatus();
        }

        /// <summary>
        /// Gets current restoring file
        /// </summary>
        /// <returns>current restoring file</returns>
        public RestoreFileQueueItem GetCurrentRestoreFile()
        {
            return RestoreRepository.Instance != null ? RestoreRepository.Instance.GetCurrentFile() : null;
        }

        /// <summary>
        /// Gets current backuping file
        /// </summary>
        /// <returns>current backuping file</returns>
        public FileQueueItem GetCurrentBackupFile()
        {
            return BackupRepository.Instance != null ? BackupRepository.Instance.GetCurrentFile() : null;
        }

        /// <summary>
        /// Backup this file asap, moved to the top of queue
        /// </summary>
        /// <param name="filePath">full path to the file</param>
        public void BackupFileAsap(string filePath)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.SetFileForce(filePath, true);
            }
        }

        /// <summary>
        /// Adds the file to backup.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void AddFileToBackup(string filePath, bool force)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.AddFile(filePath, force);
            }
        }

        /// <summary>
        /// Add files to backup
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        /// <param name="force">if set to <c>true</c> [force]</param>
        public void AddFileCollectionToBackup(string[] filePathes, bool force)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.AddFileCollection(filePathes, force);
            }
        }

        /// <summary>
        /// Rename files in Backup
        /// </summary>
        /// <param name="files">Dictionary of file names</param>
        public void RenameFilesInBackup(Dictionary<string, string> files)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.RenameFiles(files);
            }
        }

        /// <summary>
        /// Remove file from queue to backup
        /// </summary>
        /// <param name="filePath">The file path</param>
        public void RemoveFileFromQueueToBackup(string filePath)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.RemoveFile(filePath);
            }
        }

        /// <summary>
        /// Remove files from queue to backup
        /// </summary>
        /// <param name="filePathes">The file pathes</param>
        public void RemoveFileCollectionFromFromQueueToBackup(string[] filePathes)
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.RemoveFileCollection(filePathes);
            }
        }

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="restoreFilePath">The restore file path.</param>
        /// <param name="restoreVersion">The restore version.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void RestoreFile(string filePath, string restoreFilePath, int restoreVersion, bool force)
        {
            var settings = this.RemotingManager.ProxyManager.SettingsManager.LoadSettings();

            ThreadPool.QueueUserWorkItem(
                state => this.SendFileToRestoreQueue(new RestoreFileQueueItem
                                                                {
                                                                    ComputerId = 0,
                                                                    EncryptKey = settings.CurrentComputerEncryptInfo,
                                                                    FilePath = filePath,
                                                                    RestoreFilePath = restoreFilePath,
                                                                    RestoreFileVersion = restoreVersion,
                                                                    Force = force
                                                                }));
        }

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="restoreFilePath">The restore file path.</param>
        /// <param name="restoreVersion">The restore version.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        public void RestoreFile(int computerId, EncryptInfo encryptKey, string filePath, string restoreFilePath, int restoreVersion, bool force)
        {
                ThreadPool.QueueUserWorkItem(
                    state => this.SendFileToRestoreQueue(new RestoreFileQueueItem
                                                             {
                                                                 ComputerId = computerId,
                                                                 EncryptKey = encryptKey,
                                                                 FilePath = filePath,
                                                                 RestoreFilePath = restoreFilePath,
                                                                 RestoreFileVersion = restoreVersion,
                                                                 Force = force
                                                             }));
        }

        /// <summary>
        /// Set pause for upload
        /// </summary>
        public void SetPause()
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.UploadService.PauseUpload();
                ScheduleManager.Scheduler.IsASAP = false;
            }
        }

        /// <summary>
        /// Continue upload
        /// </summary>
        public void UnsetPause()
        {
            if (BackupRepository.Instance != null)
            {
                BackupRepository.Instance.UploadService.ContinueUpload();
            }
        }

        /// <summary>
        /// Searches the files.
        /// </summary>
        /// <param name="pattern">The pattern.</param>
        /// <returns>The list of files.</returns>
        public List<string> SearchFiles(string pattern)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Restores the computer.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        public void RestoreComputer(int computerId, EncryptInfo encryptKey)
        {
            if (ServiceManager.FilesService != null)
            {
                ComputerFilesDto filesFromComputer =
                    ServiceManager.FilesService.GetAllFilesFromComputer(new GetAllFilesFromComputerInputs
                                                                            {
                                                                                ComputerId = computerId,
                                                                                ComputerIdSpecified = true,
                                                                                FilesCategory = SelectFilesMode.WithoutDocuments,
                                                                                FilesCategorySpecified = true
                                                                            });

                var restoreList = new List<FileModel>();
                foreach (var file in filesFromComputer.FilesInfo)
                {
                    restoreList.Add(file);
                }

                if (restoreList.Count > 0)
                {
                    ThreadPool.QueueUserWorkItem(
                        state => this.RestoreFiles(restoreList, computerId, encryptKey));
                }

                var folders = ServiceManager.ComputersService.GetAllFolders().OrderBy(f => f.Length);

                foreach (var folder in folders)
                {
                       RemotingManager.ProxyManager.ScheduledFileManager.CreateNewFolder(folder);
                }
            }
        }

        /// <summary>
        /// Cancels the restoring.
        /// </summary>
        public void CancelRestoring()
        {
            if (RestoreRepository.Instance != null)
            {
                ThreadPool.QueueUserWorkItem(state => RestoreRepository.Instance.CancelRestoring());
            }
        }

        /// <summary>
        /// Restores the documents.
        /// </summary>
        /// <param name="originalUser">The original user.</param>
        /// <param name="targetUser">The target user.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        public void RestoreDocuments(SyncronizeUsersEntry originalUser, SyncronizeUsersEntry targetUser, int computerId, EncryptInfo encryptKey)
        {
            if (ServiceManager.FilesService != null)
            {
                ComputerFilesDto filesFromComputer =
                    ServiceManager.FilesService.GetAllFilesFromComputer(new GetAllFilesFromComputerInputs
                                                                            {
                                                                                ComputerId = computerId,
                                                                                ComputerIdSpecified = true,
                                                                                FilesCategory = SelectFilesMode.Documents,
                                                                                FilesCategorySpecified = true,
                                                                                UserSid = originalUser.Sid
                                                                            });

                var restoreList = new List<FileModel>();
                foreach (var file in filesFromComputer.FilesInfo)
                {
                        restoreList.Add(file);
                }

                if (restoreList.Count > 0)
                {
                    ThreadPool.QueueUserWorkItem(
                        state => this.RestoreFilesFromDocuments(restoreList, computerId, encryptKey, originalUser, targetUser));
                }
            }
        }

        /// <summary>
        /// Adds the existing file to backup.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        /// <param name="oldFilePath">The old file path.</param>
        /// <param name="newFilePath">The new file path.</param>
        /// <returns>Backup file result</returns>
        public BackupFileResultModel AddExistingFileToBackup(int computerId, string oldFilePath, string newFilePath)
        {
            if (ServiceManager.FilesService != null)
            {
                var result = ServiceManager.FilesService.BackupFile(new BackupFileRequestModel
                                                           {
                                                               CreateCopy = true,
                                                               FileName = newFilePath,
                                                               OldFileName = oldFilePath
                                                           });
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets the file priority.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="priority">The priority.</param>
        /// <returns>Results of operation.</returns>
        public bool SetFilePriority(string filePath, RestorePriority priority)
        {
            return RestoreRepository.Instance.SetFilePriority(filePath, priority);
        }

        /// <summary>
        /// Cancels the restore file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public void CancelRestoreFile(string filePath)
        {
            ThreadPool.QueueUserWorkItem(
                        state => RestoreRepository.Instance.DeleteFileFromQueue(filePath));
        }

        /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="computerId">The computer id.</param>
        /// <returns>Returns file info.</returns>
        public FileModel GetFileInfo(string filePath, int computerId)
        {
            GetFileInfoInputs inputs = new GetFileInfoInputs
                                           {
                                               ComputerId = computerId,
                                               ComputerIdSpecified = true,
                                               File = GetRelativePath(filePath)
                                           };

            return ServiceManager.FilesService.GetFileInfo(inputs);
        }

        /// <summary>
        /// Remove file from S3
        /// </summary>
        /// <param name="filePath">Path to file</param>
        /// <returns>Is successed</returns>
        public bool RemoveFileFromS3(string filePath)
        {
            RemovingFileResult result = BackupRepository.Instance.UploadService.FileService.RemoveFile(new GetFileInfoInputs
                                                                               {
                                                                                   ComputerId = 0,
                                                                                   ComputerIdSpecified = true,
                                                                                   File = GetRelativePath(filePath)
                                                                               });
            return result.IsSuccess;
        }

        #endregion

        #region Private

        /// <summary>
        /// Wait this event to make sure that service is already inited
        /// </summary>
        private EventWaitHandle initEventHandle;

        /// <summary>
        /// Restores the files.
        /// </summary>
        /// <param name="files">The files list.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        public void RestoreFiles(IEnumerable<FileModel> files, int computerId, EncryptInfo encryptKey)
        {
            if (RestoreRepository.Instance != null)
            {
                foreach (FileModel file in files)
                {
                    var fileQueueItem = new RestoreFileQueueItem
                                            {
                                                ComputerId = computerId,
                                                EncryptKey = encryptKey,
                                                FilePath = file.FileName,
                                                RestoreFilePath = file.FileName,
                                                RestoreFileVersion = -1, //file.FileVersions[0].VersionNumber),
                                                IsUpload = true
                                            };
                    ThreadPool.QueueUserWorkItem(
                        state => this.SendFileToRestoreQueue(fileQueueItem));
                }
            }
        }

        /// <summary>
        /// Reset init event for client app
        /// </summary>
        public void ResetInitEvent()
        {
            BackupRepository.ResetInitEvent();
        }

        /// <summary>
        /// call this method to make sure that repository is already inited
        /// </summary>
        /// <param name="timeout">timeout to wait</param>
        /// <returns>return true if Inited</returns>
        public bool EnsureInited(int timeout)
        {
            return BackupRepository.EnsureInited(timeout);
        }

        /// <summary>
        /// Restores the files.
        /// </summary>
        /// <param name="files">The files list.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptKey">The encrypt key.</param>
        /// <param name="originalUser">The original user.</param>
        /// <param name="targetUser">The target user.</param>
        private void RestoreFilesFromDocuments(IEnumerable<FileModel> files, int computerId, EncryptInfo encryptKey, SyncronizeUsersEntry originalUser, SyncronizeUsersEntry targetUser)
        {
            foreach (FileModel file in files)
            {
                string restoreFilePath;
                if (file.FileName.StartsWith(originalUser.DocumentsPath))
                {
                    restoreFilePath = file.FileName.Replace(originalUser.DocumentsPath, targetUser.DocumentsPath);
                }
                else
                {
                    restoreFilePath = file.FileName.Replace(originalUser.DesktopPath, targetUser.DesktopPath);
                }

                var fileQueueItem = new RestoreFileQueueItem
                                        {
                                            ComputerId = computerId,
                                            EncryptKey = encryptKey,
                                            FilePath = file.FileName,
                                            RestoreFilePath = restoreFilePath,
                                            RestoreFileVersion = -1,
                                            // file.FileVersions[0].VersionNumber,
                                            IsUpload = true
                                        };
                this.SendFileToRestoreQueue(fileQueueItem);
            }
        }

        /// <summary>
        /// Sends the file to restore queue.
        /// </summary>
        /// <param name="fileQueueItem">The file queue item.</param>
        private void SendFileToRestoreQueue(RestoreFileQueueItem fileQueueItem)
        {
            if (RestoreRepository.Instance != null)
            {
                int currentComputerId = 0;

                if (fileQueueItem.ComputerId == currentComputerId)
                {
                    var info = this.RemotingManager.ProxyManager.ScheduledFileManager.GetFileInfo(fileQueueItem.FilePath);
                    if (info != null)
                    {
                        fileQueueItem.Size = info.Size.Size;
                    }
                }
                else
                {
                    var info = this.GetFileInfo(fileQueueItem.FilePath, fileQueueItem.ComputerId);

                    if (fileQueueItem.RestoreFileVersion > 0)
                    {
                        for (int i = 0; i < info.FileVersions.Count; i++)
                        {
                            if (info.FileVersions[i].FileVersion == fileQueueItem.RestoreFileVersion)
                            {
                                fileQueueItem.Size = info.FileVersions[i].Size;
                                break;
                            }
                        }
                    }
                    else
                    {
                        fileQueueItem.Size = info.FileVersions[info.FileVersions.Count - 1].Size;
                    }
                }

                RestoreRepository.Instance.AddFile(fileQueueItem);
            }
        }

        private string GetRelativePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (!path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return path;
            }

            return path.Substring(basePath.Length).Trim('\\');
        }

        #endregion
    }
}