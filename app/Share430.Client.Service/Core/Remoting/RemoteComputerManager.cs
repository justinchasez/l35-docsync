using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Security.Principal;
using Microsoft.Win32;
using Share430.Client.Core.Entities;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Remoting
{
    /// <summary>
    /// The remote computer manager.
    /// </summary>
    [Serializable]
    public class RemoteComputerManager : MarshalByRefObject, IRemoteComputerManager
    {
        #region IRemoteComputerManager Members

        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <returns>The list of computers.</returns>
        public List<FolderModel> GetComputers()
        {
            return ServiceManager.ComputersService.GetComputers().ToList();
        }

        /// <summary>
        /// Adds the computer.
        /// </summary>
        /// <param name="computer">The computer.</param>
        /// <returns>The buy url.</returns>
        public string AddComputer(ComputerInfo computer)
        {
            return ServiceManager.ComputersService.AddComputer(computer);
        }

        /// <summary>
        /// Validates the computer.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="password">The password.</param>
        /// <returns>True if provided data is valid, otherwise false.</returns>
        public bool ValidateComputer(int userId, Guid computerId, string password)
        {
            return !String.IsNullOrEmpty(password);
        }

        /// <summary>
        /// Gets the backup users list.
        /// </summary>
        /// <param name="computer">The computer.</param>
        /// <returns>
        /// The list of users in the backuped computer.
        /// </returns>
        public List<SyncronizeUsersEntry> GetBackupUsersList(ComputerInfo computer)
        {
            return new List<SyncronizeUsersEntry>(ServiceManager.ComputersService.GetComputerUsers(computer).Users);
        }

        /// <summary>
        /// Gets the local users list.
        /// </summary>
        /// <returns>The list of users from this computer</returns>
        public List<SyncronizeUsersEntry> GetLocalUsersList()
        {
            List<SyncronizeUsersEntry> entries = new List<SyncronizeUsersEntry>();

            string[] pathes = new string[]
                                  {
                                      string.Format("WinNT://./{0},group", WindowGroupHelper.GetGroupName(WellKnownSidType.BuiltinAdministratorsSid, null, false)),
                                      string.Format("WinNT://./{0},group", WindowGroupHelper.GetGroupName(WellKnownSidType.BuiltinUsersSid, null, false))
                                  };

            foreach (string pathToUses in pathes)
            {
                using (DirectoryEntry groupEntry = new DirectoryEntry(pathToUses))
                {
                    foreach (object member in (IEnumerable) groupEntry.Invoke("Members"))
                    {
                        using (DirectoryEntry memberEntry = new DirectoryEntry(member))
                        {
                            RegistryKey registryKey = null;

                            try
                            {
                                var userName = memberEntry.Path.Substring(8);
                                userName = userName.Replace(@"/", @"\");
                                NTAccount account = new NTAccount(userName);
                                SecurityIdentifier sid =
                                    (SecurityIdentifier)account.Translate(typeof(SecurityIdentifier));

                                registryKey =
                                    Registry.Users.OpenSubKey(string.Concat(sid.ToString(),
                                                                            @"\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"));
                                if (registryKey != null)
                                {
                                    string pathToDesktop = (string) registryKey.GetValue("Desktop");
                                    string pathToDocuments = (string) registryKey.GetValue("Personal");

                                    if (!string.IsNullOrEmpty(pathToDesktop) &&
                                        !string.IsNullOrEmpty(pathToDocuments))
                                    {
                                        entries.Add(new SyncronizeUsersEntry
                                                        {
                                                            UserName =
                                                                userName.Substring(
                                                                    userName.LastIndexOf(@"\") + 1),
                                                            Sid = sid.ToString(),
                                                            DesktopPath = pathToDesktop,
                                                            DocumentsPath = pathToDocuments
                                                        });
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }
                            finally
                            {
                                if (registryKey != null)
                                {
                                    registryKey.Close();
                                }
                            }
                        }
                    }
                }
            }

            return entries;
        }

        /// <summary>
        /// Updates the computer GUID.
        /// </summary>
        /// <param name="computerId">The computer id.</param>
        public void UpdateComputerGuid(int computerId)
        {
            UpdateComputerGuidInputs inputs = new UpdateComputerGuidInputs
                                                  {
                                                      ComputerGuid = SettingsManager.Key,
                                                      ComputerId = computerId,
                                                      ComputerIdSpecified = true
                                                  };
            ServiceManager.ComputersService.UpdateComputerGuid(inputs);
        }

        public void CreateFolder(string folderName)
        {
            var relativePath = GetRelativePath(folderName);

            if (relativePath != null)
                ServiceManager.ComputersService.CreateFolder(relativePath);
        }

        public void RemoveFolder(string folderName)
        {
            var relativePath = GetRelativePath(folderName);

            if (relativePath != null)
                ServiceManager.ComputersService.RemoveFolder(relativePath);
        }

        private string GetRelativePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (!path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            return path.Substring(basePath.Length).Trim('\\');
        }

        #endregion
    }
}