using System;
using System.Collections.Generic;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Utils;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.Service.Core.Managers
{
    /// <summary>
    /// Syncronization Manager
    /// </summary>
    public class SyncronizationManager : ISyncronizationManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncronizationManager"/> class.
        /// </summary>
        /// <param name="remotingManager">The remoting manager.</param>
        public SyncronizationManager(RemotingManager remotingManager)
        {
            this.RemotingManager = remotingManager;
            this.isCorrectPc = true;
            this.syncObject = new object();
        }

        /// <summary>
        /// syncronization object
        /// </summary>
        private object syncObject;

        /// <summary>
        /// Gets or sets the remoting manager.
        /// </summary>
        /// <value>The remoting manager.</value>
        private RemotingManager RemotingManager { get; set; }

        /// <summary>
        /// If this instance is users syncronized; otherwise, <c>false</c>.
        /// </summary>
        private bool isUsersSyncronized;

        /// <summary>
        /// Is correct PC
        /// </summary>
        private bool isCorrectPc;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is users syncronized.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is users syncronized; otherwise, <c>false</c>.
        /// </value>
        public bool IsUsersSyncronized
        {
            get
            {
                lock (this.syncObject)
                {
                    return this.isUsersSyncronized;
                }
            }

            set
            {
                lock (this.syncObject)
                {
                    if (value && !this.isUsersSyncronized && this.isCorrectPc)
                    {
                        try
                        {
                            Loger.Instance.Log("SyncronizationManager: Try syncronize users");

                            List<SyncronizeUsersEntry> entries =
                                RemotingManager.ProxyManager.ComputerManager.GetLocalUsersList();

                            SyncronizeUsersInputs inputs = new SyncronizeUsersInputs
                                                               {
                                                                   UsersList = entries.ToArray(),
                                                                   Computer = SettingsManager.Settings.CurrentComputer,
                                                                   ComputerGuid = SettingsManager.Key
                                                               };

                            var result = ServiceManager.ComputersService.SyncronizeUsers(inputs);
                            if (result.IsSyncronized)
                            {
                                this.isUsersSyncronized = true;
                                Loger.Instance.Log("SyncronizationManager: Users syncronization successpully");
                            }
                            else
                            {
                                this.isCorrectPc = false;
                                Loger.Instance.Log(
                                    "SyncronizationManager: Users is not syncronized (Wrong computer GUID)");
                            }
                        }
                        catch (Exception ex)
                        {
                            Loger.Instance.Log("SyncronizationManager: Exception" + ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <returns>Error message</returns>
        public string GetErrorMessage()
        {
            if (this.isUsersSyncronized)
            {
                return String.Empty;
            }

            if (!this.isCorrectPc)
            {
                return "Incorrect PC";
            }
            else
            {
                return "Service not answer";
            }
        }
    }
}