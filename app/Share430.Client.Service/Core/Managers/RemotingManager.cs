using System;
using Share430.Client.Core.Utils;
using Share430.Client.FileManager;
using Share430.Client.Remoting.Managers;
using Share430.Client.Remoting.Terminals;
using Share430.Client.Service.Core.Remoting;

namespace Share430.Client.Service.Core.Managers
{
    /// <summary>
    /// The server remoting manager.
    /// </summary>
    public class RemotingManager
    {
        #region Constants

        /// <summary>
        /// The remoting port.
        /// </summary>
        private const int REMOTING_PORT = 999;

        /// <summary>
        /// The remoting channel name.
        /// </summary>
		private const string REMOTING_CHANNEL_NAME = "Share430";

        #endregion

        /// <summary>
        /// Gets the schedule proxy.
        /// </summary>
        /// <value>The schedule proxy.</value>
        public ProxyManager ProxyManager { get; private set; }

        /// <summary>
        /// Gets the proxy publisher.
        /// </summary>
        /// <value>The proxy publisher.</value>
        public EventProxy ProxyPublisher { get; private set; }

        /// <summary>
        /// Gets the proxy.
        /// </summary>
        /// <value>The event proxy.</value>
        public EventProxy Proxy { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RemotingManager"/> class.
        /// </summary>
        public RemotingManager()
        {
        }

        /// <summary>
        /// Initializes the connection.
        /// </summary>
        public void InitializeConnection()
        {
            RemoteAppManager appManager = new RemoteAppManager();
            appManager.RemotingManager = this;
            appManager.Disable = false;
            appManager.SyncronizationManager = new SyncronizationManager(this);

            this.ProxyManager = new ProxyManager(
                new RemoteBackupManager(this), 
                new RemoteScheduleManager(), 
                new RemoteSettingsManager(), 
                appManager, 
                new RemoteHelpManager(), 
                new RemoteComputerManager(),
                new RemoteUserManager(),
                ScheduledFileManager.Instance,
                new RemoteRecoveryLogsManager());

            this.ProxyPublisher = new EventProxy();

            try
            {

                ServerTerminal.RegisterChannel(REMOTING_PORT, REMOTING_CHANNEL_NAME);
                
                ServerTerminal.StartListening(this.ProxyManager);
                ServerTerminal.StartListening(this.ProxyPublisher);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log(ex.ToString());
            }
        }
    }
}
