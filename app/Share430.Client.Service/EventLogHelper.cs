using System.Diagnostics;

namespace Share430.Client.Service
{
    /// <summary>
    /// EventLogHelper class
    /// </summary>
    public class EventLogHelper
    {
        /// <summary>
        /// Event logger
        /// </summary>
        private static EventLog eventLogger;

        /// <summary>
        /// Initializes static members of the EventLogHelper class. object
        /// </summary>
        static EventLogHelper()
        {
			if (!EventLog.SourceExists("Share430Application"))
            {
				EventLog.CreateEventSource("Share430Application", "ServiceLog");
            }

            eventLogger = new EventLog();
			eventLogger.Source = "Share430Application";
        }

        /// <summary>
        /// Add event to event log
        /// </summary>
        /// <param name="message">message to add</param>
        public static void AddEvent(string message)
        {
            //Log a information to the eventLog
            eventLogger.WriteEntry(message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Add exception message to event log
        /// </summary>
        /// <param name="message">message to add</param>
        public static void AddException(string message)
        {
            //Log a exception to the eventLog
            eventLogger.WriteEntry(message, EventLogEntryType.Error);
        }

        /// <summary>
        /// Add warning message to event log
        /// </summary>
        /// <param name="message">message to add</param>
        public static void AddWarning(string message)
        {
            //Log a warning to the eventLog
            eventLogger.WriteEntry(message, EventLogEntryType.Warning);
        }
    }
}