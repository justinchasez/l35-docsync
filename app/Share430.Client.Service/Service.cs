using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Managers.Logging;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services;
using Share430.Client.FileManager;
using Share430.Client.FileManager.Services;
using Share430.Client.FileManager.Services.Download;
using Share430.Client.FileManager.Services.Upload;
using Share430.Client.Proxy;
using Share430.Client.Scheduler;
using Share430.Client.Service.Core.Managers;
using Share430.Client.Service.Core.Repositories;

namespace Share430.Client.Service
{
    /// <summary>
    /// The main form.
    /// </summary>
    public partial class Service : ServiceBase
    {
        #region Constants

        /// <summary>
        /// The name of the backup service.
        /// </summary>
		internal const string SERVICE_NAME = "Share430Service";

        /// <summary>
        /// The description of the backup service.
        /// </summary>
        internal const string SERVICE_DESCRIPTION = "This service shedule automatic backup of Inline Backup application.";

        #endregion

        /// <summary>
        /// The object for syncronize
        /// </summary>
        private object syncObject = new object();

        /// <summary>
        /// The remoting connection manager.
        /// </summary>
        private RemotingManager manager;

        /// <summary>
        /// Initializes a new instance of the <see cref="Service"/> class.
        /// </summary>
        public Service()
        {
            this.InitializeComponent();
            this.ServiceName = SERVICE_NAME;
        }

        /// <summary>
        /// The OnStart event.
        /// </summary>
        /// <param name="args">The inline arguments.</param>
        protected override void OnStart(string[] args)
        {
            log4net.GlobalContext.Properties[LoggingManager.LOGING_FILE_KEY] = Path.Combine(CoreConstantsHelper.PathForSaveData, Assembly.GetExecutingAssembly().GetName().Name + LoggingManager.LOG_FILE_EXTENTION);

            //EventLogHelper.AddEvent("On start service");
            LoggingManager.LogInfo(LoggingManager.Separator);
//            Loger.Instance.Log("Starting service...");
            LoggingManager.LogInfo("Starting service...");

            try
            {
                LoggingManager.LogInfo("Creating RemotingManager");
                //EventLogHelper.AddEvent("Creating RemotingManager");
                this.manager = new RemotingManager();
                LoggingManager.LogInfo("Initialize connections");
                //EventLogHelper.AddEvent("Initialize connections");
                this.manager.InitializeConnection();

                //Loger.Instance.Log("Initialize settings manager");
                //SettingsManager.Initialize(CoreConstantsHelper.PathForSaveData);
                LoggingManager.LogInfo("Initialize schedule manager");
                //EventLogHelper.AddEvent("Initialize schedule manager");
                ScheduleManager.Initialize(CoreConstantsHelper.PathForSaveData);

                LoggingManager.LogInfo("Initialize service manager");
                //EventLogHelper.AddEvent("Initialize service manager");
                ServiceManager.Initialize(SettingsManager.Settings);
                LoggingManager.LogInfo("Initialize service manager finished");
                //EventLogHelper.AddEvent("Initialize service manager finished");

                if (!SettingsManager.Settings.IsFirstRun)
                {
                    ThreadPool.QueueUserWorkItem(x => { this.OnCustomCommand(SettingsManager.SERVICE_COMMAND_INIT); });
//                    this.OnCustomCommand(SettingsManager.SERVICE_COMMAND_INIT);
                }
            }
            catch (Exception ex)
            {
                LoggingManager.LogError(ex);
                //EventLogHelper.AddEvent(ex.ToString());
//                Loger.Instance.Log(ex.ToString());
				//System.Diagnostics.EventLog.WriteEntry("Share430 service", ex.ToString());
            }

            LoggingManager.LogInfo("Service started");
            LoggingManager.LogInfo(LoggingManager.Separator);
            //EventLogHelper.AddEvent("Service started");
        }

        /// <summary>
        /// Event handler for service commands
        /// </summary>
        /// <param name="command">number of command (from 128 up to 255)</param>
        protected override void OnCustomCommand(int command)
        {
            if (command == SettingsManager.SERVICE_COMMAND_INIT)
            {
                LoggingManager.LogInfo("OnCustomCommand started");

                lock (this.syncObject)
                {
                    ServiceManager.Initialize(SettingsManager.Settings); // reinit services

                    IUploadService uploadService = new UploadService(new ServiceInitParams
                                                                         {
                                                                             AccessKeyId = AmazonSettingsManager.AwsAccessKey,
                                                                             SecretAccessKeyId = AmazonSettingsManager.AwsSecretAccessKey,
                                                                             BucketName = SettingsManager.DEFAULT_BACKET_NAME,
                                                                             PartSize = SettingsManager.DEFAULT_PART_SIZE,
                                                                             FilesService = ServiceManager.FilesService
                                                                         });

                    BackupRepository.Initialize(this.manager, uploadService, SettingsManager.Settings.BackupDataFilePath);

                    IDownloadService downloadService = new DownloadService(new ServiceInitParams
                                                                               {
                                                                                   AccessKeyId = AmazonSettingsManager.AwsAccessKey,
                                                                                   SecretAccessKeyId = AmazonSettingsManager.AwsSecretAccessKey,
                                                                                   BucketName = SettingsManager.DEFAULT_BACKET_NAME,
                                                                                   PartSize = SettingsManager.DEFAULT_PART_SIZE,
                                                                                   FilesService = ServiceManager.FilesService
                                                                               });

                    RestoreRepository.Initialize(this.manager, downloadService, SettingsManager.Settings.RestoreDataFilePath);
                }

                LoggingManager.LogInfo("OnCustomCommand finished");
            }     else if (command == SettingsManager.SERVICE_COMMAND_STOP)
            {
                LoggingManager.LogInfo("OnCustomCommand started: stop");
                ScheduleManager.Initialize(CoreConstantsHelper.PathForSaveData);
                ScheduledFileManager.Initialize();

                IUploadService uploadService = new UploadService(new ServiceInitParams
                {
                    AccessKeyId = AmazonSettingsManager.AwsAccessKey,
                    SecretAccessKeyId = AmazonSettingsManager.AwsSecretAccessKey,
                    BucketName = SettingsManager.DEFAULT_BACKET_NAME,
                    PartSize = SettingsManager.DEFAULT_PART_SIZE,
                    FilesService = ServiceManager.FilesService
                });

                BackupRepository.Instance.CancelBackuping();
                BackupRepository.Instance.ClearQueue();
                BackupRepository.Initialize(this.manager, uploadService, SettingsManager.Settings.BackupDataFilePath);

                IDownloadService downloadService = new DownloadService(new ServiceInitParams
                {
                    AccessKeyId = AmazonSettingsManager.AwsAccessKey,
                    SecretAccessKeyId = AmazonSettingsManager.AwsSecretAccessKey,
                    BucketName = SettingsManager.DEFAULT_BACKET_NAME,
                    PartSize = SettingsManager.DEFAULT_PART_SIZE,
                    FilesService = ServiceManager.FilesService
                });

                RestoreRepository.Instance.CancelRestoring();
                RestoreRepository.Instance.ClearQueue();
                RestoreRepository.Initialize(this.manager, downloadService, SettingsManager.Settings.RestoreDataFilePath);
                

                SyncRepository.Initialize(this.manager);
                LoggingManager.LogInfo("OnCustomCommand finished: stop");
            }     else if (command == SettingsManager.SERVICE_COMMAND_START_FOLDER)
            {
                LoggingManager.LogInfo("OnCustomCommand started: SERVICE_COMMAND_START_FOLDER");
                SyncRepository.Initialize(this.manager);
                LoggingManager.LogInfo("OnCustomCommand finished: SERVICE_COMMAND_START_FOLDER");
            }
        }

        /// <summary>
        /// The OnStop event.
        /// </summary>
        protected override void OnStop()
        {
            LoggingManager.LogInfo("Service stopped");
            LoggingManager.LogInfo(LoggingManager.Separator);
            //EventLogHelper.AddEvent("Service stoped");
        }
    }
}
