#pragma once
#include "VDFolderBehaviourBase.h"

class VDRootFolderBehaviour :
	public VDFolderBehaviourBase
{
public:
	VDRootFolderBehaviour(void);
	virtual ~VDRootFolderBehaviour(void);

	virtual void GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList);
	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child);

	virtual int StateChildByName(const CString& name);
};

