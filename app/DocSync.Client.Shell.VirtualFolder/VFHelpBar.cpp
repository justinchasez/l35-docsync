#include "StdAfx.h"
#include "VFHelpBar.h"
#include "dllmain.h"
#include "VFResources.h"


CVFHelpBar::CVFHelpBar(void)
{
}


CVFHelpBar::~CVFHelpBar(void)
{
}

LRESULT CVFHelpBar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {20,20,280,40};

	RECT rcName = {20,2,280,20};
	m_name.Create(m_hWnd,rcName, NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().StaticFont());
	m_name.SetWindowText(_T("Help"));


	m_helpLink.Create(m_hWnd,rect);
	m_helpLink.m_dwExtendedStyle = HLINK_UNDERLINEHOVER;
	m_helpLink.SetLabel(_T("- View Help"));
	m_helpLink.SetHyperLink(_Module.m_helpUrl);
	m_helpLink.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_helpLink.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_helpLink.m_clrLink = CVFResources::instance().LinkColor();
	m_helpLink.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	return 0;
}

LRESULT CVFHelpBar::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().StaticColor());
	SetBkMode((HDC)wParam,TRANSPARENT);
	return (LRESULT)GetStockObject(NULL_BRUSH);
}

LRESULT CVFHelpBar::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NMHDR* nmhdr = (NMHDR*)lParam;

	if (m_helpLink.m_hWnd == nmhdr->hwndFrom)
	{
	}

	return 0;
}

void CVFHelpBar::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);

	::FillRect(hDC,&rect,GetSysColorBrush(COLOR_WINDOW));*/

	//CVFResources::instance().HelpIcon().DrawIconEx(hDC,2,2,16,16,0);
}

LRESULT CVFHelpBar::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect;
	GetClientRect(&rect);

	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CVFHelpBar::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}
