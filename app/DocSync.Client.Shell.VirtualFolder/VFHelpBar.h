#pragma once
class CVFHelpBar  :	public CWindowImpl<CVFHelpBar>
{
public:
	CVFHelpBar(void);
	~CVFHelpBar(void);

BEGIN_MSG_MAP(CVFHelpBar)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC,OnColorStatic)
	MESSAGE_HANDLER(WM_NOTIFY,OnNotify)
END_MSG_MAP()

	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	void InnerPaint(HDC hDC);
private:
	CHyperLink m_helpLink;
	CStatic m_name;
};

