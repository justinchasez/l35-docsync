#pragma once
class RecoveryPendingPanel :
	public CWindowImpl<RecoveryPendingPanel>

{
public:
	RecoveryPendingPanel(void);
	~RecoveryPendingPanel(void);

BEGIN_MSG_MAP(RecoveryPendingPanel)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC,OnColorStatic)
	MESSAGE_HANDLER(WM_NOTIFY,OnNotify)
	MESSAGE_HANDLER(WM_SIZE, OnSize)
	MESSAGE_HANDLER(EventHandlerManager::WM_UPDATE_STATUS_INFO,OnUpdateStatusInfo)
END_MSG_MAP()

	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	SIZE CalcSize(SIZE& sz, bool moved);
private:
	void InnerPaint(HDC hDC);
private:
	CHyperLink m_filesCompleted;
	CHyperLink m_filesErrors;

	CStatic m_name;
};

