#pragma once

class VDPanelBlock  :	public CWindowImpl<VDPanelBlock>
{
public:
	VDPanelBlock(void);
	~VDPanelBlock(void);

BEGIN_MSG_MAP(VDPanelBlock)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
END_MSG_MAP()

	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};

