#include "StdAfx.h"
#include "VFBackupBar.h"
#include "VFResources.h"
#include "..\OnlineBackup.Client.Shell.ContextMenu\config.h"

CVFBackupBar::CVFBackupBar(void)
{
}


CVFBackupBar::~CVFBackupBar(void)
{
}


LRESULT CVFBackupBar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {20,20,280,40};

	RECT rcName = {20,2,280,20};
	m_name.Create(m_hWnd,rcName, NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().StaticFont());
	m_name.SetWindowText(_T("My Backup"));


	m_browseFiles.Create(m_hWnd,rect);
	m_browseFiles.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	m_browseFiles.SetLabel(_T("- Browse the files in my backup"));
	m_browseFiles.SetHyperLink(_T("- 'Backed-up Files' folder in Virtual Drive will be opened"));
	m_browseFiles.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_browseFiles.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_browseFiles.m_clrLink = CVFResources::instance().LinkColor();
	m_browseFiles.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	rect.bottom += 20;
	rect.top += 20;

	m_launchWizard.Create(m_hWnd,rect);
	m_launchWizard.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	m_launchWizard.SetLabel(_T("- Launch the Restore Wizard"));
	m_launchWizard.SetHyperLink(_T("Restore Wizard window will be opened"));
	m_launchWizard.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_launchWizard.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_launchWizard.m_clrLink = CVFResources::instance().LinkColor();
	m_launchWizard.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	return 0;
}

LRESULT CVFBackupBar::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NMHDR* nmhdr = (NMHDR*)lParam;

	if (m_browseFiles.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,0);
		}
	}
	else if (m_launchWizard.m_hWnd == nmhdr->hwndFrom)
	{
		try
		{
			ICppProxyPtr proxy;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
			if (proxy)
			{
				proxy->NotifyCommand(COMMAND_LAUNCHRESTOREWIZARD,0,_bstr_t());
			}
		}
		catch (...)
		{
		}
	}

	return 0;
}

LRESULT CVFBackupBar::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().StaticColor());
	SetBkMode((HDC)wParam,TRANSPARENT);
	return (LRESULT)GetStockObject(NULL_BRUSH);
}

void CVFBackupBar::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);

	::FillRect(hDC,&rect,GetSysColorBrush(COLOR_WINDOW));*/

	//CVFResources::instance().BackupIcon().DrawIconEx(hDC,2,2,16,16,0);
}

LRESULT CVFBackupBar::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CVFBackupBar::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}
