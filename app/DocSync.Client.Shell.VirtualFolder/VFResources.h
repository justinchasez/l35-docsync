#pragma once
class CVFResources
{
private:
	CVFResources(void);
	~CVFResources(void);
public:
	static CVFResources& instance()
	{
		static CVFResources resources;
		return resources;
	}

	HFONT HyperLinkFont();
	HFONT StaticFont(); // for static controls in blocks
	HFONT TitleFont(); // for title bar text
	CIcon& HelpIcon(); // icon for help block
	CIcon& ViewStatusIcon(); // icon for view status block
	CIcon& BackupIcon(); // icon for backup icon
	COLORREF LinkColor(); // color for links
	COLORREF LinkVisitedColor(); // color for visited links
	COLORREF TitleColor(); // text color for title bar
	COLORREF StaticColor(); // text color for static controls in blocks

	HBITMAP BackgroundBitmap();
private:
	void init();
private:
	CFont m_hyperLinkFont;
	CFont m_staticFont;
	CFont m_titleFont;

	CIcon m_helpIcon;
	CIcon m_backupIcon;
	CIcon m_viewstatusIcon;

	CBitmap m_background;
};

