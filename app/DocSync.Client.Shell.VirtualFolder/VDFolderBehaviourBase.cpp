#include "StdAfx.h"
#include "VDFolderBehaviourBase.h"
#include "Util.h"
#include "VDRootFolderBehaviour.h"
#include "VDBackupFolderBehaviour.h"
#include "VDPendingFolderBehaviour.h"
#include "VDRecoveryFolderBehaviour.h"
#include "VDRecoveryCompletedFolderBehaviour.h"
#include "VDRecoveryErrorsFolderBehaviour.h"
#include "VDRecoveryPendingFolderBehaviour.h"

VDFolderBehaviourBase::VDFolderBehaviourBase(void)
{
}

VDFolderBehaviourBase::~VDFolderBehaviourBase(void)
{
}

VDFolderBehaviourBase* VDFolderBehaviourBase::CreateBehaviour(int state)
{
	VDFolderBehaviourBase* pBehaviour = NULL;
	switch (state)
	{
	case ENTRY_ROOT:
		pBehaviour = new VDRootFolderBehaviour();
		break;
	case ENTRY_BACKUP:
		pBehaviour = new VDBackupFolderBehaviour();
		break;
	case ENTRY_PENDING:
		pBehaviour = new VDPendingFolderBehaviour();
		break;
	case ENTRY_RECOVERYLOG:
		pBehaviour = new VDRecoveryFolderBehaviour();
		break;
	case ENTRY_RECOVERY_COMPLETED:
		pBehaviour = new VDRecoveryCompletedFolderBehaviour();
		break;
	case ENTRY_RECOVERY_ERRORS:
		pBehaviour = new VDRecoveryErrorsFolderBehaviour();
		break;
	case ENTRY_RECOVERY_PENDING:
		pBehaviour = new VDRecoveryPendingFolderBehaviour();
		break;
	default:
		pBehaviour = new VDRootFolderBehaviour();
		break;
	}

	return pBehaviour;
}

void VDFolderBehaviourBase::CreateColumns(WTL::CListViewCtrl& wndList)
{
	wndList.InsertColumn ( 0, _T("Name"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 1, _T("State"), LVCFMT_LEFT, 80, 0 );
}

int VDFolderBehaviourBase::GetPanelMode(LPCITEMIDLIST pidl)
{
	return SPLIT_PANE_NONE;
}

int VDFolderBehaviourBase::TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	POINT point = {0,0};
	::GetCursorPos(&point);

	HMENU hMenu = ::LoadMenu(_Module.GetResourceInstance(),MAKEINTRESOURCE(IDR_BACKEDUP_FOLDER_MENU));

	HMENU hSubMenu = ::GetSubMenu(hMenu,0);

	::DeleteMenu(hSubMenu,ID_MENU_RESTORE,MF_BYCOMMAND);
	::DeleteMenu(hSubMenu,ID_MENU_RESTORETO,MF_BYCOMMAND);
	::DeleteMenu(hSubMenu,ID_MENU_DONOTBACK,MF_BYCOMMAND);

	int cmd = TrackPopupMenu(hSubMenu,TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, point.x, point.y, 0, window, NULL);
	::DestroyMenu(hMenu);

	return cmd;
}

DWORD VDFolderBehaviourBase::FolderViewMode(UINT viewMode)
{
	DWORD dwMode =  LVS_SINGLESEL;

	switch ( viewMode )
		{
		case FVM_ICON:      dwMode |= LVS_ICON;      break;
		case FVM_SMALLICON: dwMode |= LVS_SMALLICON; break;
		case FVM_LIST:      dwMode |= LVS_LIST;      break;
		case FVM_DETAILS:   dwMode |= LVS_REPORT;    break;
		default: dwMode |= LVS_ICON;      break;
		}
	return dwMode;
}

void VDFolderBehaviourBase::GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList)
{
	vctList.clear();

	TCHAR  tmpPath[MAX_PATH]=_TEXT("");
	DWORD  dwLen=MAX_PATH;

	int state = m_PidlMgr.GetItemState(pidl);

	m_PidlMgr.GetInternalPath(pidl,tmpPath,&dwLen);

	CString fullPath(tmpPath);

	try
	{
		VARIANT entries;
		::VariantInit(&entries);

		ICppProxyPtr proxy;
		proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

		if (proxy)
			proxy->GetEntries(_bstr_t((LPCTSTR)fullPath),state,&entries);

		int type = VT_ARRAY | VT_BSTR;
		if (entries.vt == (VT_ARRAY | VT_BSTR))
		{
			long iLBound = 0;
			long iUBound = 0;

			UINT dim = SafeArrayGetDim(entries.parray);
			SafeArrayGetLBound(entries.parray, 1, &iLBound);
			SafeArrayGetUBound(entries.parray, 1, &iUBound);

			BSTR value = NULL;
			for (long i = 0; i <= iUBound - iLBound; i++)
			{
				if (SafeArrayGetElement(entries.parray, &i, &value) == S_OK)
				{
					bool isFolder = true;
					CString name(value);
					if (name.GetLength()>0)
					{
						if (name.GetAt(0) == '1')
						{
							isFolder = true;
							name.Delete(0,1);
						}
						else if (name.GetAt(0) == '0')
						{
							isFolder = false;
							name.Delete(0,1);
						}
						else
						{
							isFolder = false;
						}
					}

					ITEMDATA data;
					data.type = isFolder ? NWS_FOLDER : NWS_FILE;

					data.state = state;
					data.sName = name;
					vctList.push_back(data);
				}
				else
					break;
			}
			::VariantClear(&entries);
		}
	}
	catch(...)	{/*do nothing*/}
}

void VDFolderBehaviourBase::SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data)
{
	// do nothing in the base class
}

void VDFolderBehaviourBase::ParseName(CString& name)
{
	// do nothing in the base class
}

LPCUSTOMFOLDER_DATA VDFolderBehaviourBase::CreateCustomData(const CString& name, IUnknown* proxy)
{
	return NULL;
}
