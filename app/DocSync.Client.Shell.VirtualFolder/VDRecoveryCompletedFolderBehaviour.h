#pragma once
#include "vdfolderbehaviourbase.h"
class VDRecoveryCompletedFolderBehaviour :
	public VDFolderBehaviourBase
{
public:
	VDRecoveryCompletedFolderBehaviour(void);
	virtual ~VDRecoveryCompletedFolderBehaviour(void);

	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child);

	virtual void SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data);
	virtual void ParseName(CString& name);
	virtual LPCUSTOMFOLDER_DATA CreateCustomData(const CString& name, IUnknown* proxy);

	virtual int TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index);

	virtual DWORD FolderViewMode(UINT viewMode);

	virtual void CreateColumns(WTL::CListViewCtrl& wndList);

	virtual int StateChildByName(const CString& name);
};

