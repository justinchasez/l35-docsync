#include "StdAfx.h"
#include "VDRecoveryPendingFolderBehaviour.h"
#include "CNWSPidlMgr.h"

VDRecoveryPendingFolderBehaviour::VDRecoveryPendingFolderBehaviour(void)
{
}


VDRecoveryPendingFolderBehaviour::~VDRecoveryPendingFolderBehaviour(void)
{
}

int VDRecoveryPendingFolderBehaviour::StateChildByName(const CString& name)
{
	return ENTRY_RECOVERY_PENDING;
}

VDFolderBehaviourBase* VDRecoveryPendingFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	return new VDRecoveryPendingFolderBehaviour();
}

void VDRecoveryPendingFolderBehaviour::CreateColumns(WTL::CListViewCtrl& wndList)
{
	wndList.InsertColumn ( 0, _T("Name"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 1, _T("Source"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 2, _T("Destination"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 3, _T("Priority"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 4, _T("State"), LVCFMT_LEFT, 80, 0 );
}

DWORD VDRecoveryPendingFolderBehaviour::FolderViewMode(UINT viewMode)
{
	return LVS_REPORT;
}

void VDRecoveryPendingFolderBehaviour::Command(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, int cmd, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	ICppProxyPtr shell_proxy = proxy;

	int sel_index = listCtrl.GetNextItem(-1, LVNI_SELECTED);

	while (sel_index > -1)
	{
		CString source;
		listCtrl.GetItemText(sel_index, 1, source);

		switch (cmd)
		{
		case ID_MENU_CANCELRESTORE:
			shell_proxy->CancelRestore(_bstr_t(source.AllocSysString()));
			break;
		case ID_MENU_HIGHPRIORITY:
		case ID_MENU_LOWPRIORITY:
		case ID_MENU_NORMALPRIORITY:
			shell_proxy->SetRestorePriority(cmd - ID_MENU_HIGHPRIORITY, _bstr_t(source.AllocSysString()));
			break;
		}

		sel_index = listCtrl.GetNextItem(sel_index, LVNI_SELECTED);
	}
}

int VDRecoveryPendingFolderBehaviour::TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	POINT point = {0,0};
	::GetCursorPos(&point);

	HMENU hMenu = ::LoadMenu(_Module.GetResourceInstance(),MAKEINTRESOURCE(IDR_RECOVERY_PENDING_MENU));

	HMENU hSubMenu = ::GetSubMenu(hMenu,0);

	WCHAR priority[25];

	UINT PriorityID = 0;
	UINT currentPriorityID = 0;
	bool samePriority = true;

	int sel_index = listCtrl.GetNextItem(-1, LVNI_SELECTED);
	while (sel_index > -1)
	{
		listCtrl.GetItemText(sel_index, 3, priority, 25);

		if (_tcscmp(priority,_T("Lowest")) == 0)
		{
			currentPriorityID = ID_MENU_LOWPRIORITY;
		}
		else if (_tcscmp(priority,_T("Normal")) == 0)
		{
			currentPriorityID = ID_MENU_NORMALPRIORITY;
		}
		else if (_tcscmp(priority,_T("Highest")) == 0)
		{
			currentPriorityID = ID_MENU_HIGHPRIORITY;
		}

		if (PriorityID == 0)
		{
			PriorityID = currentPriorityID;
		}
		else if (PriorityID != currentPriorityID)
		{
			samePriority = false;
		}

		sel_index = listCtrl.GetNextItem(sel_index, LVNI_SELECTED);
	}

	if (samePriority)
	{
		CheckMenuItem(hSubMenu, PriorityID, MF_CHECKED);
	}

	int cmd = TrackPopupMenu(hSubMenu,TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, point.x, point.y, 0, window, NULL);
	::DestroyMenu(hMenu);

	return cmd;
}

void VDRecoveryPendingFolderBehaviour::SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data)
{
	if (data == NULL)
		return;

	LPRECOVERY_DATA lpData = (LPRECOVERY_DATA)data;

	if (lpData)
	{
		listCtrl.SetItemText(index, 1, lpData->Source);
		listCtrl.SetItemText(index, 2, lpData->Destination);
		listCtrl.SetItemText(index, 3, lpData->Priority);
		listCtrl.SetItemText(index, 4, lpData->State);
	}
}

void VDRecoveryPendingFolderBehaviour::ParseName(CString& name)
{
	int pos = name.ReverseFind('\\');
	if (pos >= 0)
	{
		name = name.Right(name.GetLength()-pos-1);
	}
}

LPCUSTOMFOLDER_DATA VDRecoveryPendingFolderBehaviour::CreateCustomData(const CString& name, IUnknown* proxy)
{
	if (proxy == NULL)
		return NULL;

	LPRECOVERY_DATA lpData = new RECOVERY_DATA();
	lpData->Source = name;
	lpData->Destination = name;
	lpData->Priority = _T("Normal");
	lpData->State = _T("Test State");

	ICppProxyPtr proxyObject = proxy;
	//proxyObject->GetRecoveryData(_bstr_t(name.AllocSysString()),lpData->Destination.GetAddress(),lpData->State.GetAddress(),lpData->Priority.GetAddress(),&lpData->datetime);

	return lpData;
}