// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

#include "targetver.h"

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit
#define _CRT_SECURE_NO_WARNINGS

#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>
#include <atlstr.h>
#include <ShlObj.h>

using namespace ATL;

#include "atlapp.h"
#include "atlwin.h"
#include "atlctrls.h"
#include "atlctrlx.h"
#include "atlsplit.h"
#include "atlscrl.h"

#include "dllmain.h"
#include "Util.h"

#define GET_OWNER_WINDOW WM_USER + 10
#define NAVIGATE_VIRTUAL_FOLDER WM_USER + 11

#ifdef _DEBUG
#import "..\..\app\OnlineBackup.Client.CppProxy\Debug\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Debug\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#else
#import "..\..\app\OnlineBackup.Client.CppProxy\Release\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Release\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#endif

#define STATUS_BACKEDUP_ALL				0
#define STATUS_PENDING_BACKEDUP			1
#define STATUS_SUCCESSFULLY_RESTORED	2
#define STATUS_QUEUED_FOR_RESTORE		3
#define STATUS_COULD_NOT_BE_RESTORED	4