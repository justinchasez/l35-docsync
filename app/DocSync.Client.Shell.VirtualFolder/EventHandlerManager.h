#pragma once

#include <map>

#ifdef _DEBUG
#import "..\..\app\DocSync.Client.CppProxy\Debug\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\DocSync.Client.CppProxyServer\bin\Debug\DocSync.Client.CppProxyServer.tlb" named_guids no_namespace
#else
#import "..\..\app\DocSync.Client.CppProxy\Release\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\DocSync.Client.CppProxyServer\bin\Release\DocSync.Client.CppProxyServer.tlb" named_guids no_namespace
#endif


class EventHandlerManager :
	public IDispEventImpl<0, EventHandlerManager, &DIID__ICppProxyEvents, &LIBID_BackupDutyClientCppProxyLib, 1, 0>
{
public:
	EventHandlerManager(void);
	~EventHandlerManager(void);

	BEGIN_SINK_MAP(EventHandlerManager)
		SINK_ENTRY_EX(0, DIID__ICppProxyEvents, 0x4, OnUpdateStatusInfo)
	END_SINK_MAP()

	HRESULT __stdcall OnUpdateStatusInfo(LONG type, LONG count);

	void AdviceWindow(HWND window);
	void UnadviceWindow(HWND window);
	void NotifyUpdate();

	static int WM_UPDATE_STATUS_INFO;

private:
	ICppProxyPtr m_proxy;
	std::map<HWND,int> m_notifyWindows;

	ATL::CComAutoCriticalSection m_CS;

	HRESULT hrAdvice;
};
