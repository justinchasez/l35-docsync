#include "StdAfx.h"
#include "VDRecoveryFolderBehaviour.h"
#include "VDRecoveryCompletedFolderBehaviour.h"
#include "VDRecoveryErrorsFolderBehaviour.h"
#include "VDRecoveryPendingFolderBehaviour.h"

VDRecoveryFolderBehaviour::VDRecoveryFolderBehaviour(void)
{
}


VDRecoveryFolderBehaviour::~VDRecoveryFolderBehaviour(void)
{
}

int VDRecoveryFolderBehaviour::StateChildByName(const CString& name)
{
	CString childName;
	childName.LoadString(IDS_RLOG_COMPLETED);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_RECOVERY_COMPLETED;

	childName.LoadString(IDS_RLOG_ERRORS);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_RECOVERY_ERRORS;

	childName.LoadString(IDS_RLOG_PENDING);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_RECOVERY_PENDING;

	return ENTRY_RECOVERY_COMPLETED;
}

void VDRecoveryFolderBehaviour::GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList)
{
	vctList.clear();

	ITEMDATA data;
	data.type = NWS_FOLDER;

	data.state = ENTRY_RECOVERY_COMPLETED;
	data.sName.LoadString(IDS_RLOG_COMPLETED);
	vctList.push_back(data);

	data.state = ENTRY_RECOVERY_ERRORS;
	data.sName.LoadString(IDS_RLOG_ERRORS);
	vctList.push_back(data);

	data.state = ENTRY_RECOVERY_PENDING;
	data.sName.LoadString(IDS_RLOG_PENDING);
	vctList.push_back(data);
}

VDFolderBehaviourBase* VDRecoveryFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	int state = m_PidlMgr.GetItemState(child);


	VDFolderBehaviourBase* behaviour = NULL;
	switch (state)
	{
	case ENTRY_RECOVERY_COMPLETED:
		behaviour = new VDRecoveryCompletedFolderBehaviour();
		break;
	case ENTRY_RECOVERY_ERRORS:
		behaviour = new VDRecoveryErrorsFolderBehaviour();
		break;
	case ENTRY_RECOVERY_PENDING:
		behaviour = new VDRecoveryPendingFolderBehaviour();
		break;
	default:
		behaviour = new VDRecoveryFolderBehaviour();
		break;
	}

	return behaviour;
}