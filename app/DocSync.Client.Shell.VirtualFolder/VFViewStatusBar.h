#pragma once
class CVFViewStatusBar  :
	public CWindowImpl<CVFViewStatusBar>,
	public IDispEventImpl<0, CVFViewStatusBar, &DIID__ICppProxyEvents, &LIBID_BackupDutyClientCppProxyLib, 1, 0>
{
public:
	CVFViewStatusBar(void);
	~CVFViewStatusBar(void);

BEGIN_SINK_MAP(CVFViewStatusBar)
   //SINK_ENTRY_EX(0, DIID__ICppProxyEvents, 0x4, OnUpdateStatusInfo)
END_SINK_MAP()

BEGIN_MSG_MAP(CVFViewStatusBar)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC,OnColorStatic)
	MESSAGE_HANDLER(WM_NOTIFY,OnNotify)
	MESSAGE_HANDLER(EventHandlerManager::WM_UPDATE_STATUS_INFO,OnUpdateStatusInfo)
END_MSG_MAP()

	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	void InnerPaint(HDC hDC);
private:
	CHyperLink m_filesPendingBackup;
	CHyperLink m_filesPendingRestore;
	CHyperLink m_filesSuccessfullyRestored;
	CHyperLink m_viewRestoreErrors;
	CStatic m_name;
};

