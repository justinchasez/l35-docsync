#pragma once
#include "VDPanelBlock.h"
#include "VFTitleBar.h"
#include "VFViewStatusBar.h"
#include "VFBackupBar.h"
#include "VFHelpBar.h"

#include "BackedUPPanel.h"
#include "PendingPanel.h"
#include "RecoveryPanel.h"

#include "RecoveryCompletedPanel.h"
#include "RecoveryErrorsPanel.h"
#include "RecoveryPendingPanel.h"

class CVirtualDrivePanel :	public CScrollWindowImpl<CVirtualDrivePanel>
{
public:
	DECLARE_WND_CLASS_EX(NULL, 0, -1)

	CVirtualDrivePanel(void);
	~CVirtualDrivePanel(void);

BEGIN_MSG_MAP(CVirtualDrivePanel)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
    MESSAGE_HANDLER(WM_SIZE, OnSizePanel)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(GET_OWNER_WINDOW, OnGetOwnerWindow)
	CHAIN_MSG_MAP(CScrollWindowImpl<CVirtualDrivePanel>);
END_MSG_MAP()

	// Message handlers
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSizePanel(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/);
	LRESULT OnGetOwnerWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	// Set Owner window and type of panel (BackedUp Pending or Recovery Log)
	// must be called before create window
	void Init(HWND hWnd, int type);

	void DoPaint(CDCHandle dc);


private:
	int m_type;
	HWND m_ownerWindow;
	SIZE m_contentSize;
	// child panels
	CVFTitleBar m_titleBlock;
	CVFBackupBar m_backupBlock;
	CVFViewStatusBar m_viewStatusBlock;
	CVFHelpBar m_helpBlock;

	BackedUpPanel m_backedupPanel;
	PendingPanel m_pendingPanel;
	RecoveryPanel m_recoveryPanel;

	RecoveryCompletedPanel m_recCompletedPanel;
	RecoveryErrorsPanel m_recErrorsPanel;
	RecoveryPendingPanel m_recPendigPanel;
};

