#include "StdAfx.h"
#include "VDPendingFolderBehaviour.h"
#include "..\OnlineBackup.Client.Shell.ContextMenu\config.h"

VDPendingFolderBehaviour::VDPendingFolderBehaviour(void)
{
}


VDPendingFolderBehaviour::~VDPendingFolderBehaviour(void)
{
}

VDFolderBehaviourBase* VDPendingFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	return new VDPendingFolderBehaviour();
}

int VDPendingFolderBehaviour::StateChildByName(const CString& name)
{
	return ENTRY_PENDING;
}

int VDPendingFolderBehaviour::GetPanelMode(LPCITEMIDLIST pidl)
{
	return (m_PidlMgr.Count(pidl)>=2) ?  SPLIT_PANE_TOP : SPLIT_PANE_NONE;
}

int VDPendingFolderBehaviour::TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	if (fullPath.IsEmpty())
	{
		return VDFolderBehaviourBase::TrackContextMenu(window, pidl, fullPath, proxy, listCtrl, index);
	}

	POINT point = {0,0};
	::GetCursorPos(&point);

	ICppProxyPtr cpp_proxy = proxy;
	//proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

	long versions = 0;
	long state = STATE_NOTHING;
	ITEM_TYPE type = m_PidlMgr.GetItemType(pidl);
	long internalState = m_PidlMgr.GetItemState(pidl);
	long backedup = 0;

	if (cpp_proxy)
	{
		cpp_proxy->CheckFile(_bstr_t(fullPath.AllocSysString()),&state,&versions);
	}

	int cmd  = 0;
	long autobackup = 0;

	if (type == NWS_FOLDER)
	{
		if (cpp_proxy)
		{
			cpp_proxy->ContainsState(_bstr_t(fullPath.AllocSysString()),STATE_SCHEDULED,&backedup);
			if (state > STATE_NEW)
				cpp_proxy->CheckAutoBackup(_bstr_t(fullPath.AllocSysString()),&autobackup);
		}

		HMENU hMenu = ::LoadMenu(_Module.GetResourceInstance(),MAKEINTRESOURCE(IDR_PENDING_FOLDER_MENU));

		HMENU hSubMenu = ::GetSubMenu(hMenu,0);

		if (state == STATE_NEW || state == STATE_NOTHING || autobackup > 0)
		{
			::DeleteMenu(hSubMenu,ID_MENU_DONOTBACK,MF_BYCOMMAND);
		}
		if (versions == 0 && backedup == 0)
		{
			::DeleteMenu(hSubMenu,ID_MENU_RESTORE,MF_BYCOMMAND);
			::DeleteMenu(hSubMenu,ID_MENU_RESTORETO,MF_BYCOMMAND);
		}

		cmd = TrackPopupMenu(hSubMenu,TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, point.x, point.y, 0, window, NULL);
		::DestroyMenu(hMenu);
	}
	else
	{
		HMENU hMenu = ::LoadMenu(_Module.GetResourceInstance(),MAKEINTRESOURCE(IDR_PENDING_FILE_MENU));

		HMENU hSubMenu = ::GetSubMenu(hMenu,0);
		
		if (versions == 0 && backedup == 0)
		{
			::DeleteMenu(hSubMenu,ID_MENU_RESTORE,MF_BYCOMMAND);
			::DeleteMenu(hSubMenu,ID_MENU_RESTORETO,MF_BYCOMMAND);
		}

		if (versions < 2 && backedup == 0)
		{
			::DeleteMenu(hSubMenu,ID_MENU_RESTORETOPREVIOUSVERSION,MF_BYCOMMAND);
		}

		if (state == STATE_NEW || state == STATE_NOTHING)
		{
			::DeleteMenu(hSubMenu,ID_MENU_DONOTBACK,MF_BYCOMMAND);
		}

		cmd = TrackPopupMenu(hSubMenu,TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, point.x, point.y, 0, window, NULL);
		::DestroyMenu(hMenu);
	}

	return cmd;
}