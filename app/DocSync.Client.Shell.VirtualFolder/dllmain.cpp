// dllmain.cpp : Implementation of DllMain.

#include "stdafx.h"
#include "resource.h"
#include "OnlineBackupClientShellVirtualFolder_i.h"
#include "dllmain.h"
#include "dlldatax.h"

COnlineBackupClientShellVirtualFolderModule _Module;

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
#ifdef _MERGE_PROXYSTUB
	if (!PrxDllMain(hInstance, dwReason, lpReserved))
		return FALSE;
#endif
	hInstance;

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		_Module.Init();

		return _Module.DllMain(dwReason, lpReserved); 
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
        _Module.Term();
	}

	return TRUE;
}
