// VDFContextMenu.h : Declaration of the CVDFContextMenu

#pragma once
#include "resource.h"       // main symbols


#include "OnlineBackupClientShellVirtualFolder_i.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;

#include <shlobj.h>
#include <comdef.h>
#include "CNWSPidlMgr.h"

// CVDFContextMenu

class ATL_NO_VTABLE CVDFContextMenu :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CVDFContextMenu, &CLSID_VDFContextMenu>,
	public IObjectWithSiteImpl<CVDFContextMenu>,
//	public IDispatchImpl<IVDFContextMenu, &IID_IVDFContextMenu, &LIBID_BackupDutyClientShellVirtualFolderLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IShellExtInit,
	public IContextMenu3
{
public:
	CVDFContextMenu()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_VDFCONTEXTMENU)


BEGIN_COM_MAP(CVDFContextMenu)
//	COM_INTERFACE_ENTRY(IVDFContextMenu)
	COM_INTERFACE_ENTRY(IShellExtInit)
	COM_INTERFACE_ENTRY(IContextMenu)
	COM_INTERFACE_ENTRY(IContextMenu2)
	COM_INTERFACE_ENTRY(IContextMenu3)
//	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		m_hMenu = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		if (m_hMenu != NULL)
		{
			::DestroyMenu(m_hMenu);
			m_hMenu = NULL;
		}
	}

public:
		// IContextMenu
#ifdef WIN64
	STDMETHOD(GetCommandString)(UINT_PTR, UINT, UINT*, LPSTR, UINT);
#else
	STDMETHOD(GetCommandString)(UINT, UINT, UINT*, LPSTR, UINT);
#endif
	STDMETHOD(InvokeCommand)(LPCMINVOKECOMMANDINFO);
	STDMETHOD(QueryContextMenu)(HMENU, UINT, UINT , UINT, UINT);

	// IContextMenu2
	STDMETHOD(HandleMenuMsg)(UINT, WPARAM, LPARAM);

	// IContextMenu3
	STDMETHOD(HandleMenuMsg2)(UINT, WPARAM, LPARAM, LRESULT *);

	// IShellExtInit
	//
	STDMETHOD(Initialize)(LPCITEMIDLIST, LPDATAOBJECT, HKEY);

private:
	HMENU m_hMenu;
};

//OBJECT_ENTRY_AUTO(__uuidof(VDFContextMenu), CVDFContextMenu)
