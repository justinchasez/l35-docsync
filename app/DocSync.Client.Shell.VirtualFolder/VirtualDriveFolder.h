// VirtualDriveFolder.h : Declaration of the CVirtualDriveFolder

#pragma once
#include "resource.h"       // main symbols

#include "OnlineBackupClientShellVirtualFolder_i.h"
#include "VDFolderBehaviourBase.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

#include <shlobj.h>
#include <comdef.h>
#include "CNWSPidlMgr.h"

// CVirtualDriveFolder

class ATL_NO_VTABLE CVirtualDriveFolder :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CVirtualDriveFolder, &CLSID_VirtualDriveFolder>,
	public IDispatchImpl<IVirtualDriveFolder, &IID_IVirtualDriveFolder, &LIBID_BackupDutyClientShellVirtualFolderLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IShellFolder2,
    public IPersistFolder2,
	public IQueryInfo
{
public:
	CVirtualDriveFolder();
	~CVirtualDriveFolder();

DECLARE_REGISTRY_RESOURCEID(IDR_VIRTUALDRIVEFOLDER)


BEGIN_COM_MAP(CVirtualDriveFolder)
	COM_INTERFACE_ENTRY(IVirtualDriveFolder)
//	COM_INTERFACE_ENTRY_IID(IID_IShellFolder,IShellFolder)
//   COM_INTERFACE_ENTRY_IID(IID_IPersistFolder,IPersistFolder)
//   COM_INTERFACE_ENTRY_IID(IID_IQueryInfo,IQueryInfo)
   COM_INTERFACE_ENTRY(IPersist)
//	COM_INTERFACE_ENTRY(IVirtualDriveFolder)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShellFolder)
	COM_INTERFACE_ENTRY(IShellFolder2)
	COM_INTERFACE_ENTRY(IPersistFolder)
	COM_INTERFACE_ENTRY(IPersistFolder2)
//	COM_INTERFACE_ENTRY(IPersist)
//	COM_INTERFACE_ENTRY_IID(IID_IShellDetails, IShellDetails)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		m_pidlRoot = NULL;
		m_pFolderBehaviour = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		if (m_pFolderBehaviour != NULL)
		{
			delete m_pFolderBehaviour;
			m_pFolderBehaviour = NULL;
		}

		if(m_pidlRoot != NULL)
		{
			m_PidlMgr.Delete(m_pidlRoot);
			m_pidlRoot = NULL;
		}
	}

public:
//-------------------------------------------------------------------------------
	// IPersist

	STDMETHOD(GetClassID)(CLSID*);

	//-------------------------------------------------------------------------------
	// IPersistFolder(2)

	STDMETHOD(Initialize)(LPCITEMIDLIST);
	STDMETHOD(GetCurFolder)(LPITEMIDLIST *ppidl);

	//-------------------------------------------------------------------------------
	// IShellFolder

	STDMETHOD(BindToObject) (LPCITEMIDLIST, LPBC, REFIID, void**);
	STDMETHOD(CompareIDs) (LPARAM, LPCITEMIDLIST, LPCITEMIDLIST);
	STDMETHOD(CreateViewObject) (HWND, REFIID, void** );
	STDMETHOD(EnumObjects) (HWND, DWORD, LPENUMIDLIST*);
	STDMETHOD(GetAttributesOf) (UINT, LPCITEMIDLIST*, LPDWORD);
	STDMETHOD(GetUIObjectOf) (HWND, UINT, LPCITEMIDLIST*, REFIID, LPUINT, void**);
	STDMETHOD(BindToStorage) (LPCITEMIDLIST, LPBC, REFIID, void**);
	STDMETHOD(GetDisplayNameOf) (LPCITEMIDLIST, DWORD uFlags, LPSTRRET lpName);
	STDMETHOD(ParseDisplayName) (HWND, LPBC, LPOLESTR, LPDWORD, LPITEMIDLIST*, LPDWORD);
	STDMETHOD(SetNameOf) (HWND, LPCITEMIDLIST, LPCOLESTR, DWORD, LPITEMIDLIST*);

	//-------------------------------------------------------------------------------
	// IShellDetails

	STDMETHOD(ColumnClick) (UINT iColumn);
	STDMETHOD(GetDetailsOf) (LPCITEMIDLIST pidl, UINT iColumn, LPSHELLDETAILS pDetails);

	//-------------------------------------------------------------------------------
	// IShellFolder2

	STDMETHOD(EnumSearches) (IEnumExtraSearch **ppEnum);
	STDMETHOD(GetDefaultColumn) (DWORD dwReserved, ULONG *pSort, ULONG *pDisplay);
	STDMETHOD(GetDefaultColumnState) (UINT iColumn, SHCOLSTATEF *pcsFlags);
	STDMETHOD(GetDefaultSearchGUID) (GUID *pguid);
	STDMETHOD(GetDetailsEx) (LPCITEMIDLIST pidl, const SHCOLUMNID *pscid, VARIANT *pv);
	// already in IShellDetails: STDMETHOD(GetDetailsOf) (LPCITEMIDLIST pidl, UINT iColumn, SHELLDETAILS *psd);
	STDMETHOD(MapColumnToSCID) (UINT iColumn, SHCOLUMNID *pscid);

	//-------------------------------------------------------------------------------
	//IQueryInfo
   STDMETHOD(GetInfoTip)(DWORD dwFlags,LPWSTR *ppwszTip);
   STDMETHOD(GetInfoFlags)(DWORD *pdwFlags);

   // the same as GetCurFolder, but without copy - to internal usage
   LPCITEMIDLIST GetCurPidl() { return m_pidlRoot; };
protected:
	//CPidlData m_data;
	CNWSPidlMgr m_PidlMgr;
	LPITEMIDLIST m_pidlRoot;

public:
	VDFolderBehaviourBase* m_pFolderBehaviour;

	STDMETHOD(BrowseObject)(BSTR fullPath);

	//Drag and Drop	-- shkil
   HRESULT DoDrop(LPDATAOBJECT pDataObject, DWORD dwDropEffect, LPITEMIDLIST pidlDropDest,UINT iFmtIdx);
   HRESULT DoDrop_HDROP(HGLOBAL hMem, DWORD dwDropEffect, LPITEMIDLIST pidlDropDest);
};

OBJECT_ENTRY_AUTO(__uuidof(VirtualDriveFolder), CVirtualDriveFolder)
