

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Jan 17 15:43:15 2014
 */
/* Compiler settings for OnlineBackupClientShellVirtualFolder.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __OnlineBackupClientShellVirtualFolder_i_h__
#define __OnlineBackupClientShellVirtualFolder_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IVirtualDriveFolder_FWD_DEFINED__
#define __IVirtualDriveFolder_FWD_DEFINED__
typedef interface IVirtualDriveFolder IVirtualDriveFolder;

#endif 	/* __IVirtualDriveFolder_FWD_DEFINED__ */


#ifndef __IVirtualDriveView_FWD_DEFINED__
#define __IVirtualDriveView_FWD_DEFINED__
typedef interface IVirtualDriveView IVirtualDriveView;

#endif 	/* __IVirtualDriveView_FWD_DEFINED__ */


#ifndef __IVDFContextMenu_FWD_DEFINED__
#define __IVDFContextMenu_FWD_DEFINED__
typedef interface IVDFContextMenu IVDFContextMenu;

#endif 	/* __IVDFContextMenu_FWD_DEFINED__ */


#ifndef __IVirtualDriveLink_FWD_DEFINED__
#define __IVirtualDriveLink_FWD_DEFINED__
typedef interface IVirtualDriveLink IVirtualDriveLink;

#endif 	/* __IVirtualDriveLink_FWD_DEFINED__ */


#ifndef __VirtualDriveFolder_FWD_DEFINED__
#define __VirtualDriveFolder_FWD_DEFINED__

#ifdef __cplusplus
typedef class VirtualDriveFolder VirtualDriveFolder;
#else
typedef struct VirtualDriveFolder VirtualDriveFolder;
#endif /* __cplusplus */

#endif 	/* __VirtualDriveFolder_FWD_DEFINED__ */


#ifndef __VirtualDriveView_FWD_DEFINED__
#define __VirtualDriveView_FWD_DEFINED__

#ifdef __cplusplus
typedef class VirtualDriveView VirtualDriveView;
#else
typedef struct VirtualDriveView VirtualDriveView;
#endif /* __cplusplus */

#endif 	/* __VirtualDriveView_FWD_DEFINED__ */


#ifndef __VDFContextMenu_FWD_DEFINED__
#define __VDFContextMenu_FWD_DEFINED__

#ifdef __cplusplus
typedef class VDFContextMenu VDFContextMenu;
#else
typedef struct VDFContextMenu VDFContextMenu;
#endif /* __cplusplus */

#endif 	/* __VDFContextMenu_FWD_DEFINED__ */


#ifndef __VirtualDriveLink_FWD_DEFINED__
#define __VirtualDriveLink_FWD_DEFINED__

#ifdef __cplusplus
typedef class VirtualDriveLink VirtualDriveLink;
#else
typedef struct VirtualDriveLink VirtualDriveLink;
#endif /* __cplusplus */

#endif 	/* __VirtualDriveLink_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "shobjidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IVirtualDriveFolder_INTERFACE_DEFINED__
#define __IVirtualDriveFolder_INTERFACE_DEFINED__

/* interface IVirtualDriveFolder */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVirtualDriveFolder;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("5E4962B0-11E7-47E2-A7A7-F7203067750D")
    IVirtualDriveFolder : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE BrowseObject( 
            /* [in] */ BSTR fullPath) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IVirtualDriveFolderVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVirtualDriveFolder * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVirtualDriveFolder * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVirtualDriveFolder * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVirtualDriveFolder * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVirtualDriveFolder * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVirtualDriveFolder * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVirtualDriveFolder * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *BrowseObject )( 
            IVirtualDriveFolder * This,
            /* [in] */ BSTR fullPath);
        
        END_INTERFACE
    } IVirtualDriveFolderVtbl;

    interface IVirtualDriveFolder
    {
        CONST_VTBL struct IVirtualDriveFolderVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVirtualDriveFolder_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVirtualDriveFolder_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVirtualDriveFolder_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVirtualDriveFolder_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVirtualDriveFolder_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVirtualDriveFolder_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVirtualDriveFolder_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IVirtualDriveFolder_BrowseObject(This,fullPath)	\
    ( (This)->lpVtbl -> BrowseObject(This,fullPath) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVirtualDriveFolder_INTERFACE_DEFINED__ */


#ifndef __IVirtualDriveView_INTERFACE_DEFINED__
#define __IVirtualDriveView_INTERFACE_DEFINED__

/* interface IVirtualDriveView */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVirtualDriveView;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("93EDAC55-4613-4942-8909-A325E3DB2DD1")
    IVirtualDriveView : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IVirtualDriveViewVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVirtualDriveView * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVirtualDriveView * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVirtualDriveView * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVirtualDriveView * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVirtualDriveView * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVirtualDriveView * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVirtualDriveView * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IVirtualDriveViewVtbl;

    interface IVirtualDriveView
    {
        CONST_VTBL struct IVirtualDriveViewVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVirtualDriveView_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVirtualDriveView_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVirtualDriveView_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVirtualDriveView_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVirtualDriveView_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVirtualDriveView_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVirtualDriveView_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVirtualDriveView_INTERFACE_DEFINED__ */


#ifndef __IVDFContextMenu_INTERFACE_DEFINED__
#define __IVDFContextMenu_INTERFACE_DEFINED__

/* interface IVDFContextMenu */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVDFContextMenu;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E71E3D5C-8F4E-436C-9CA3-33BA76F68608")
    IVDFContextMenu : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IVDFContextMenuVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVDFContextMenu * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVDFContextMenu * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVDFContextMenu * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVDFContextMenu * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVDFContextMenu * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVDFContextMenu * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVDFContextMenu * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IVDFContextMenuVtbl;

    interface IVDFContextMenu
    {
        CONST_VTBL struct IVDFContextMenuVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVDFContextMenu_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVDFContextMenu_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVDFContextMenu_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVDFContextMenu_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVDFContextMenu_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVDFContextMenu_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVDFContextMenu_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVDFContextMenu_INTERFACE_DEFINED__ */


#ifndef __IVirtualDriveLink_INTERFACE_DEFINED__
#define __IVirtualDriveLink_INTERFACE_DEFINED__

/* interface IVirtualDriveLink */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVirtualDriveLink;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("261F7D48-953A-4BCF-B29C-91B7B0C04F97")
    IVirtualDriveLink : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IVirtualDriveLinkVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVirtualDriveLink * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVirtualDriveLink * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVirtualDriveLink * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVirtualDriveLink * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVirtualDriveLink * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVirtualDriveLink * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVirtualDriveLink * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IVirtualDriveLinkVtbl;

    interface IVirtualDriveLink
    {
        CONST_VTBL struct IVirtualDriveLinkVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVirtualDriveLink_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVirtualDriveLink_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVirtualDriveLink_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVirtualDriveLink_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVirtualDriveLink_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVirtualDriveLink_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVirtualDriveLink_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVirtualDriveLink_INTERFACE_DEFINED__ */



#ifndef __BackupDutyClientShellVirtualFolderLib_LIBRARY_DEFINED__
#define __BackupDutyClientShellVirtualFolderLib_LIBRARY_DEFINED__

/* library BackupDutyClientShellVirtualFolderLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_BackupDutyClientShellVirtualFolderLib;

EXTERN_C const CLSID CLSID_VirtualDriveFolder;

#ifdef __cplusplus

class DECLSPEC_UUID("281F347D-44EF-44EB-A4E3-D00B53707C2E")
VirtualDriveFolder;
#endif

EXTERN_C const CLSID CLSID_VirtualDriveView;

#ifdef __cplusplus

class DECLSPEC_UUID("946B3FBC-09EA-4082-ABD5-2958B6D11CF2")
VirtualDriveView;
#endif

EXTERN_C const CLSID CLSID_VDFContextMenu;

#ifdef __cplusplus

class DECLSPEC_UUID("0BE8CDFF-8AA8-4F94-8E9F-5C4BE6DFA18A")
VDFContextMenu;
#endif

EXTERN_C const CLSID CLSID_VirtualDriveLink;

#ifdef __cplusplus

class DECLSPEC_UUID("43750BB2-0F66-4A90-BD2E-4280E7242FAE")
VirtualDriveLink;
#endif
#endif /* __BackupDutyClientShellVirtualFolderLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


