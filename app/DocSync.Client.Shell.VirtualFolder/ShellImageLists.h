#pragma once

class CShellImageLists
{
public:

   HIMAGELIST m_hImageListSmall;
   HIMAGELIST m_hImageListLarge;

   CShellImageLists(void);
   ~CShellImageLists(void);

   BOOL CreateImageLists();
};

