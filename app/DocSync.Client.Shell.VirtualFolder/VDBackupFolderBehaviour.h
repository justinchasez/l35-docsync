#include "VDFolderBehaviourBase.h"

#pragma once

class VDBackupFolderBehaviour : public VDFolderBehaviourBase
{
public:
	VDBackupFolderBehaviour(void);
	virtual ~VDBackupFolderBehaviour(void);

	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child);
	virtual int TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index);

	virtual int GetPanelMode(LPCITEMIDLIST pidl);

	virtual int StateChildByName(const CString& name);
};

