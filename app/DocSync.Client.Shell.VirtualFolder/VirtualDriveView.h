// VirtualDriveView.h : Declaration of the CVirtualDriveView

#pragma once
#include "resource.h"       // main symbols



#include "OnlineBackupClientShellVirtualFolder_i.h"
#include "VirtualDriveFolder.h"
#include "VirtualDrivePanel.h"

//#include "_atlcontrols.h"               // my version w/bug fixes

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CVirtualDriveView
#include <shlobj.h>
#include <comdef.h>
#include "CNWSPidlMgr.h"

class ATL_NO_VTABLE CVirtualDriveView :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CVirtualDriveView, &CLSID_VirtualDriveView>,
	public IDispatchImpl<IVirtualDriveView, &IID_IVirtualDriveView, &LIBID_BackupDutyClientShellVirtualFolderLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IShellView2,
    public IOleCommandTarget,	
    public CWindowImpl<CVirtualDriveView>
{
public:
	enum { IDC_LISTVIEW = 123 };
    typedef enum {
       TBI_STD = 0,
       TBI_VIEW,
       TBI_LOCAL
    } TOOLBARITEM;

    typedef struct
    {
       TOOLBARITEM nType;
       TBBUTTON tb;
    } NS_TOOLBUTTONINFO, *PNS_TOOLBUTTONINFO;

	CVirtualDriveView()
	{
	}

	~CVirtualDriveView()
	{
		if ( NULL != m_psfContainingFolder )
            m_psfContainingFolder->Release();
	}
	
DECLARE_REGISTRY_RESOURCEID(IDR_VIRTUALDRIVEVIEW)


BEGIN_COM_MAP(CVirtualDriveView)
	COM_INTERFACE_ENTRY(IVirtualDriveView)
//	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY_IID(IID_IShellView,IShellView)
    COM_INTERFACE_ENTRY(IOleWindow)
	COM_INTERFACE_ENTRY_IID(IID_IShellView2,IShellView2)
    COM_INTERFACE_ENTRY(IOleCommandTarget)
END_COM_MAP()

BEGIN_MSG_MAP(CVirtualDriveView)
    MESSAGE_HANDLER(WM_CREATE, OnCreate)
    MESSAGE_HANDLER(WM_SIZE, OnSize)
    MESSAGE_HANDLER(WM_SETFOCUS, OnSetFocus)
    MESSAGE_HANDLER(WM_CONTEXTMENU, OnContextMenu)

    MESSAGE_HANDLER(WM_INITMENUPOPUP, OnInitMenuPopup)
    MESSAGE_HANDLER(WM_MENUSELECT, OnMenuSelect)

	MESSAGE_HANDLER(NAVIGATE_VIRTUAL_FOLDER, OnNavigateVirtualFolder)
	//MESSAGE_HANDLER(WM_UPDATE_STATUS_INFO, OnUpdateStatusInfo)

	NOTIFY_CODE_HANDLER(LVN_ITEMACTIVATE, OnItemActivated)
	NOTIFY_CODE_HANDLER(NM_RCLICK, OnRButtonClick)
END_MSG_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

    // IOleWindow
    STDMETHOD(GetWindow)(HWND* phwnd);

    STDMETHOD(ContextSensitiveHelp)(BOOL)
        { return E_NOTIMPL; }
    
    // IShellView methods
    STDMETHOD(CreateViewWindow)(LPSHELLVIEW, LPCFOLDERSETTINGS, LPSHELLBROWSER, LPRECT, HWND*);
    STDMETHOD(DestroyViewWindow)();
    STDMETHOD(GetCurrentInfo)(LPFOLDERSETTINGS);
    STDMETHOD(Refresh)();
    STDMETHOD(UIActivate)(UINT);

    STDMETHOD(AddPropertySheetPages)(DWORD, LPFNADDPROPSHEETPAGE, LPARAM)
        { return E_NOTIMPL; }
    STDMETHOD(EnableModeless)(BOOL)
        { return E_NOTIMPL; }
    STDMETHOD(GetItemObject)(UINT, REFIID, void**);
    
	STDMETHOD(SaveViewState)()
    {
	    CComPtr<IStream> spStream;

		if( m_spShellBrowser->GetViewStateStream(STGM_WRITE, &spStream) != S_OK) 
			return S_OK;

		DWORD dwWritten = 0;
		spStream->Write(&m_FolderSettings.fFlags, sizeof(m_FolderSettings.fFlags), &dwWritten);
		spStream->Write(&m_FolderSettings.ViewMode, sizeof(m_FolderSettings.ViewMode), &dwWritten);

		return S_OK;
	}

    STDMETHOD(SelectItem)(LPCITEMIDLIST, UINT)
    {
		return E_NOTIMPL;
	}

    STDMETHOD(TranslateAccelerator)(LPMSG);
    
	// IShellView2
	STDMETHOD(GetView)(SHELLVIEWID *pvid, ULONG uView);
	STDMETHOD(CreateViewWindow2)(LPSV2CVW2_PARAMS lpParams);
	STDMETHOD(HandleRename)(PCUITEMID_CHILD pidlNew);
	STDMETHOD(SelectAndPositionItem)(PCUITEMID_CHILD pidlItem, UINT uFlags, POINT *point);

    // IOleCommandTarget methods
    STDMETHOD(QueryStatus)(const GUID*, ULONG, OLECMD prgCmds[], OLECMDTEXT*);
    STDMETHOD(Exec)(const GUID*, DWORD, DWORD, VARIANTARG*, VARIANTARG*);

    // Message handlers
    LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnSetFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnInitMenuPopup(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
    LRESULT OnMenuSelect(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void NavigateVirtualFolder(int type, LPCTSTR name, int childType, LPCTSTR childName);

	LRESULT OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNavigateVirtualFolder(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	LRESULT OnItemActivated(UINT CtlID, LPNMHDR lpnmh, BOOL &bHandled);
	LRESULT OnRButtonClick(UINT CtlID, LPNMHDR lpnmh, BOOL &bHandled);

	void DeleteAllItems();

    // Other stuff
    HRESULT _init ( CVirtualDriveFolder* pContainingFolder )
    {
        m_psfContainingFolder = pContainingFolder;

        if ( NULL != m_psfContainingFolder )
            m_psfContainingFolder->AddRef();

        return S_OK;
    }

    static UINT sm_uListID;
	static UINT sm_uPanelID;

private:
    CNWSPidlMgr     m_PidlMgr;
    UINT         m_uUIState;
    int          m_nSortedColumn;
    bool         m_bForwardSort;

    FOLDERSETTINGS m_FolderSettings;

    HWND         m_hwndParent;
    HMENU        m_hMenu;

    CContainedWindowT<WTL::CListViewCtrl> m_wndList;
	CVirtualDrivePanel m_panel;
	WTL::CHorSplitterWindow m_splitter;

    CVirtualDriveFolder*      m_psfContainingFolder;
    CComPtr<IShellBrowser> m_spShellBrowser;

    void FillList();
    static int CALLBACK CompareItems ( LPARAM l1, LPARAM l2, LPARAM lData );
    void HandleActivate(UINT uState);
    void HandleDeactivate();

	/// context menu cmd handler

	//Shkil
	LPDROPTARGET m_pDropTarget;
};

OBJECT_ENTRY_AUTO(__uuidof(VirtualDriveView), CVirtualDriveView)
