#include "StdAfx.h"
#include "PidlEnum.h"

//========================================================================================
//
// Module:			CPidlEnum.cpp
// Author:          Zeng Xi
// Creation Date:	10.15.2005
//
//========================================================================================

#include "stdafx.h"
#include "PidlEnum.h"

BOOL CPidlEnum::DeleteList(void)
{
	LPENUMLIST  pDelete;

	while(m_pFirst)
	{
		pDelete = m_pFirst;
		m_pFirst = pDelete->pNext;

		//free the pidl
		m_PidlMgr.Delete(pDelete->pidl);

		//free the list item
		_Module.m_Allocator.Free(pDelete);
	}


	for (std::map<int,LPCUSTOMFOLDER_DATA>::iterator ii = m_customFolderData.begin(); ii != m_customFolderData.end(); ii++)
	{
		delete ii->second;
	}

	m_customFolderData.clear();

	return TRUE;
}
BOOL CPidlEnum::AddToEnumList(LPITEMIDLIST pidl)
{
	LPENUMLIST  pNew = NULL;

	pNew = (LPENUMLIST)_Module.m_Allocator.Alloc(sizeof(ENUMLIST));

	if(pNew)
	{
		//set the next pointer
		pNew->pNext = NULL;
		pNew->pidl = pidl;

		m_dataIndex = (int)pidl;

		//is this the first item in the list?
		if(!m_pFirst)
		{
			m_pFirst = pNew;
			m_pCurrent = m_pFirst;
		}

		if(m_pLast)
		{
			//add the new item to the end of the list
			m_pLast->pNext = pNew;
		}

		//update the last item pointer
		m_pLast = pNew;

		return TRUE;
	}

	return FALSE;
}

HRESULT  CPidlEnum::_AddPidls(ITEM_TYPE iItemType, int state, LPCTSTR pszName)
{
	LPITEMIDLIST   pidl=NULL;
	pidl=m_PidlMgr.Create(iItemType, state, pszName);
	if(pidl)
	{
		if(!AddToEnumList(pidl))
			return E_FAIL;
	}
	else
		return E_FAIL;

	return S_OK;
}

void CPidlEnum::AddCustomFolderData(LPCUSTOMFOLDER_DATA data)
{
	m_customFolderData[m_dataIndex] = data;
}

LPCUSTOMFOLDER_DATA CPidlEnum::GetCustomFolderData()
{
	if (m_customFolderData.find(m_dataIndex) != m_customFolderData.end())
		return m_customFolderData[m_dataIndex];
	return NULL;
}

HRESULT CPidlEnum::_Init(VDFolderBehaviourBase& behaviour, LPITEMIDLIST pidl, DWORD dwFlags)
{
	//TCHAR  tmpPath[MAX_PATH]=_TEXT("");
	//DWORD  dwLen=MAX_PATH;

	HRESULT  Hr;

	//int state = ENTRY_ROOT;

	//bool isRoot = false;

	//if( (NULL==pidl)||(0 == pidl->mkid.cb)  ) //current folder is root folder
	//{
	//	isRoot = true;
	//}
	//else  //sub-folder in NSE
	//{
	//	CLSID* pid = (CLSID*)(LPBYTE(pidl->mkid.abID)+2);
	//	if (*pid == CLSID_VirtualDriveFolder)
	//	{
	//		isRoot = true;
	//	}
	//}
	m_dataIndex = 0;

	std::vector<ITEMDATA> vctChildren;
	behaviour.GetChildren(pidl, vctChildren);
	
	ICppProxyPtr proxy;
	proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

	for (std::vector<ITEMDATA>::iterator ii = vctChildren.begin(); ii != vctChildren.end(); ii++)
	{

		LPCUSTOMFOLDER_DATA lpData = behaviour.CreateCustomData(ii->sName, proxy);
		behaviour.ParseName(ii->sName);

		bool newObject = false;
		if ((dwFlags & SHCONTF_FOLDERS) && (ii->type == NWS_FOLDER))// include folders
		{
			HR(_AddPidls(ii->type,ii->state,ii->sName));
			newObject = true;
		}
		else if ((dwFlags & SHCONTF_NONFOLDERS) && (ii->type == NWS_FILE))//include files
		{
			HR(_AddPidls(ii->type,ii->state,ii->sName));
			newObject = true;
		}

		if (lpData && newObject)
		{
			AddCustomFolderData(lpData);
		}
	}

	Reset();

    return S_OK;
}

HRESULT CPidlEnum::Next(DWORD dwElements, LPITEMIDLIST apidl[], LPDWORD pdwFetched)
{ 
    ATLTRACE2(atlTraceCOM, 0, _T("IEnumIDList::Next\n"));
	DWORD    dwIndex;
	HRESULT  hr = S_OK;

	if(dwElements > 1 && !pdwFetched)
	return E_INVALIDARG;

	for(dwIndex = 0; dwIndex < dwElements; dwIndex++)
	{
		//is this the last item in the list?
		if(!m_pCurrent)
		{
			hr =  S_FALSE;
			break;
		}

		apidl[dwIndex] = m_PidlMgr.Copy(m_pCurrent->pidl);
		m_dataIndex = (int)m_pCurrent->pidl;

		m_pCurrent = m_pCurrent->pNext;
	}

	if(pdwFetched)
	*pdwFetched = dwIndex;

	return hr;
}    

HRESULT CPidlEnum::Reset(void)
{ 
    ATLTRACE2(atlTraceCOM, 0, _T("IEnumIDList::Reset\n"));
	m_pCurrent = m_pFirst;
    return S_OK; 
}

HRESULT CPidlEnum::Skip(DWORD dwSkip)
{
    ATLTRACE2(atlTraceCOM, 0, _T("IEnumIDList::Skip"));
	DWORD    dwIndex;
	HRESULT  hr = S_OK;

	for(dwIndex = 0; dwIndex < dwSkip; dwIndex++)
	{
		//is this the last item in the list?
		if(!m_pCurrent)
		{
			hr = S_FALSE;
			break;
		}

		m_pCurrent = m_pCurrent->pNext;
	}

	return hr;
}

HRESULT CPidlEnum::Clone(LPENUMIDLIST *ppEnum)
{
    ATLTRACENOTIMPL(_T("IEnumIDList::Clone"));
}
