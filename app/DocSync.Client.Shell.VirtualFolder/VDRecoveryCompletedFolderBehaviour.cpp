#include "StdAfx.h"
#include "VDRecoveryCompletedFolderBehaviour.h"
#include "CNWSPidlMgr.h"
#include "ATLComTime.h"


VDRecoveryCompletedFolderBehaviour::VDRecoveryCompletedFolderBehaviour(void)
{
}


VDRecoveryCompletedFolderBehaviour::~VDRecoveryCompletedFolderBehaviour(void)
{
}

int VDRecoveryCompletedFolderBehaviour::StateChildByName(const CString& name)
{
	return ENTRY_RECOVERY_COMPLETED;
}

VDFolderBehaviourBase* VDRecoveryCompletedFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	return new VDRecoveryCompletedFolderBehaviour();
}

DWORD VDRecoveryCompletedFolderBehaviour::FolderViewMode(UINT viewMode)
{
	return LVS_REPORT | LVS_SINGLESEL;
}

void VDRecoveryCompletedFolderBehaviour::CreateColumns(WTL::CListViewCtrl& wndList)
{
	wndList.InsertColumn ( 0, _T("Name"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 1, _T("Source"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 2, _T("Destination"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 3, _T("State"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 4, _T("LastRestored"), LVCFMT_LEFT, 80, 0 );
}

int VDRecoveryCompletedFolderBehaviour::TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	return 0;
}

void VDRecoveryCompletedFolderBehaviour::SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data)
{
	if (data == NULL)
		return;

	LPRECOVERY_DATA lpData = (LPRECOVERY_DATA)data;

	if (lpData)
	{
		COleDateTime datetime(lpData->datetime);

		listCtrl.SetItemText(index, 1, lpData->Source);
		listCtrl.SetItemText(index, 2, lpData->Destination);
		listCtrl.SetItemText(index, 3, lpData->State);
		listCtrl.SetItemText(index, 4, datetime.Format());
	}
}

void VDRecoveryCompletedFolderBehaviour::ParseName(CString& name)
{
	int pos = name.ReverseFind('\\');
	if (pos >= 0)
	{
		name = name.Right(name.GetLength()-pos-1);
	}
}

LPCUSTOMFOLDER_DATA VDRecoveryCompletedFolderBehaviour::CreateCustomData(const CString& name, IUnknown* proxy)
{
	if (proxy == NULL)
		return NULL;

	LPRECOVERY_DATA lpData = new RECOVERY_DATA();
	lpData->Source = name;

	ICppProxyPtr proxyObject = proxy;
	proxyObject->GetRecoveryData(_bstr_t(name.AllocSysString()),lpData->Destination.GetAddress(),lpData->State.GetAddress(),lpData->Priority.GetAddress(),&lpData->datetime);

	return lpData;
}