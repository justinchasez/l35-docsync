//========================================================================================
//
// Module:			PidlMgr
// Author:          Pascal Hurni
// Creation Date:	11.07.2004
//
// Copyright 2004 Mortimer Systems
// This software is free. I grant you a non-exclusive license to use it.
//
// Greatly inspired from several examples from: Microsoft, Michael Dunn, ...
//
//========================================================================================


#ifndef __MORTIMER_PIDLMGR_H__
#define __MORTIMER_PIDLMGR_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "resource.h"

//========================================================================================
// encapsulate these classes in a namespace
#define PIDL_ROOT_FOLDER			0
#define PIDL_BACKEDUP_FOLDER		1
#define PIDL_PENDING_BACKUP_FOLDER	2
#define PIDL_RECOVERYLOG_FOLDER		3
#define PIDL_INSIDE_ENTRY			4

#define ENTRY_ROOT			0
#define ENTRY_BACKUP		1
#define ENTRY_PENDING		2
#define ENTRY_RECOVERYLOG	3

class CPidlData
{
public:
	CPidlData();
	CPidlData(unsigned short type, unsigned short entry_type, unsigned short isFolder, CString  name);

	virtual ~CPidlData() {};

//	CPidlData(const CPidlData &src);
//	virtual CPidlData &operator=(const CPidlData &src);
	virtual ULONG GetSize() const
	{
		ULONG sz = sizeof(m_type) + sizeof(m_entry_type) + sizeof(m_isFolder) + (_tcslen(m_name)+1)*sizeof(OLECHAR);
		return sz;
	};

	virtual void CopyTo(void *pTarget) const
	{
		*((unsigned short*)pTarget) = m_type;
		*((unsigned short*)pTarget+1) = m_entry_type;
		*((unsigned short*)pTarget+2) = m_isFolder ? 1 : 0;
	
	#ifdef _UNICODE
		wcscpy((OLECHAR*)((BYTE*)pTarget+sizeof(m_type)+sizeof(m_entry_type)+sizeof(m_isFolder)), m_name);
	#else
		mbstowcs((OLECHAR*)pTarget+sizeof(m_type)+sizeof(m_entry_type)+sizeof(m_isFolder), m_fullpath, strlen(m_fullpath)+1);
	#endif
	};

	virtual void FromPidl(LPCITEMIDLIST item)
	{
		LPVOID lpSource = (LPBYTE(item->mkid.abID)+1);
		m_type = PIDL_ROOT_FOLDER;
		if (item != NULL && item->mkid.cb >= 4)
		{
			unsigned short type = *(unsigned short*)lpSource;
			if (type==PIDL_ROOT_FOLDER)
			{
				m_name = _T("Backup Drive");
			}
			else if (type>PIDL_ROOT_FOLDER && type<=PIDL_INSIDE_ENTRY)
			{
				m_type = type;
				m_entry_type = *((unsigned short*)lpSource+1);
				m_isFolder = *((unsigned short*)lpSource+2);
				m_name.Format(_T("%s"),(OLECHAR*)((BYTE*)lpSource+sizeof(m_type)+sizeof(m_entry_type)+sizeof(m_isFolder)));

				if (m_entry_type>ENTRY_RECOVERYLOG || m_entry_type<ENTRY_ROOT)
				{
					m_entry_type = ENTRY_ROOT;
				}
			}
		}
	}

	virtual std::vector<CPidlData> GetChildren() { return m_children; };
	bool IsFolder () { return m_isFolder > 0; }


	unsigned short m_type;
	unsigned short m_entry_type;
	CString m_name;

	std::vector<CPidlData> m_children;

private:
	unsigned short m_isFolder;
};


class CPidlMgr
{
public:
	CPidlMgr()
	{
		HRESULT hr = SHGetMalloc(&m_MallocPtr);
		ATLASSERT(SUCCEEDED(hr));
	}

	LPITEMIDLIST Concatenate(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2);
	HRESULT GetFullName(LPCITEMIDLIST pidl,LPTSTR szFullName,DWORD *pdwLen);

	LPITEMIDLIST Create(const CPidlData &Data)
	{
		// Total size of the PIDL, including SHITEMID
		UINT TotalSize = sizeof(ITEMIDLIST) + Data.GetSize();

		// Also allocate memory for the final null SHITEMID.
		LPITEMIDLIST pidlNew = (LPITEMIDLIST) m_MallocPtr->Alloc(TotalSize + sizeof(ITEMIDLIST));
		if (pidlNew)
		{
			LPITEMIDLIST pidlTemp = pidlNew;

			// Prepares the PIDL to be filled with actual data
			pidlTemp->mkid.cb = TotalSize;

			// Fill the PIDL
			Data.CopyTo((void*)(LPBYTE(pidlTemp->mkid.abID)+1));

			// Set an empty PIDL at the end
			pidlTemp = GetNextItem(pidlTemp);
			pidlTemp->mkid.cb = 0;
			pidlTemp->mkid.abID[0] = 0;
		}

		return pidlNew;
	}

	void Delete(LPITEMIDLIST pidl)
	{
		if (pidl)
			m_MallocPtr->Free(pidl);
	}

	LPITEMIDLIST GetNextItem(LPCITEMIDLIST pidl)
	{
		ATLASSERT(pidl != NULL);
		if (!pidl)
			return NULL;

		return LPITEMIDLIST(LPBYTE(pidl) + pidl->mkid.cb);
	}

	LPITEMIDLIST GetLastItem(LPCITEMIDLIST pidl)
	{
		LPITEMIDLIST pidlLast = NULL;

		//get the PIDL of the last item in the list
		while (pidl && pidl->mkid.cb)
		{
			pidlLast = (LPITEMIDLIST)pidl;
			pidl = GetNextItem(pidl);
		}

		return pidlLast;
	}

	LPITEMIDLIST Copy(LPCITEMIDLIST pidlSrc)
	{
		LPITEMIDLIST pidlTarget = NULL;
		UINT Size = 0;

		if (pidlSrc == NULL)
			return NULL;

		// Allocate memory for the new PIDL.
		Size = GetSize(pidlSrc);
		pidlTarget = (LPITEMIDLIST) m_MallocPtr->Alloc(Size);

		if (pidlTarget == NULL)
			return NULL;

		// Copy the source PIDL to the target PIDL.
		CopyMemory(pidlTarget, pidlSrc, Size);

		return pidlTarget;
	}

	UINT GetSize(LPCITEMIDLIST pidl)
	{
		UINT Size = 0;
		LPITEMIDLIST pidlTemp = (LPITEMIDLIST) pidl;

		ATLASSERT(pidl != NULL);
		if (!pidl)
			return 0;

		while (pidlTemp->mkid.cb != 0)
		{
			Size += pidlTemp->mkid.cb;
			pidlTemp = GetNextItem(pidlTemp);
		}  

		// add the size of the NULL terminating ITEMIDLIST
		Size += sizeof(ITEMIDLIST);

		return Size;
	}

	bool IsSingle(LPCITEMIDLIST pidl)
	{
		LPITEMIDLIST pidlTemp = GetNextItem(pidl);
		return pidlTemp->mkid.cb == 0;
	}

	CString StrRetToCString(STRRET *pStrRet, LPCITEMIDLIST pidl)
	{
		int Length = 0;
		bool Unicode = false;
		LPCSTR StringA = NULL;
		LPCWSTR StringW = NULL;

		switch (pStrRet->uType)
		{
		case STRRET_CSTR:
			StringA = pStrRet->cStr;
			Unicode = false;
			Length = strlen(StringA);
			break;

		case STRRET_OFFSET:
			StringA = (char*)pidl+pStrRet->uOffset;
			Unicode = false;
			Length = strlen(StringA);
			break;

		case STRRET_WSTR:
			StringW = pStrRet->pOleStr;
			Unicode = true;
			Length = wcslen(StringW);
			break;
		}

		if (Length==0)
			return CString();

		CString Target;
		LPTSTR pTarget = Target.GetBuffer(Length);
		if (Unicode)
		{
#ifdef _UNICODE
			wcscpy(pTarget, StringW);
#else
			wcstombs(pTarget, StringW, Length+1);
#endif
		}
		else
		{
#ifdef _UNICODE
			mbstowcs(pTarget, StringA, Length+1);
#else
			strcpy(pTarget, StringA);
#endif
		}
		Target.ReleaseBuffer();

		// Release the OLESTR
		if (pStrRet->uType == STRRET_WSTR)
		{
			m_MallocPtr->Free(pStrRet->pOleStr);
		}

		return Target;
	}

	CComPtr<IMalloc> m_MallocPtr;
protected:
};


#endif // __MORTIMER_PIDLMGR_H__
