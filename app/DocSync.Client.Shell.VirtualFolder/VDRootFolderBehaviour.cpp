#include "StdAfx.h"
#include "VDRootFolderBehaviour.h"
#include "VDBackupFolderBehaviour.h"
#include "VDPendingFolderBehaviour.h"
#include "VDRecoveryFolderBehaviour.h"
#include "dllmain.h"

VDRootFolderBehaviour::VDRootFolderBehaviour(void)
{
}


VDRootFolderBehaviour::~VDRootFolderBehaviour(void)
{
}

int VDRootFolderBehaviour::StateChildByName(const CString& name)
{
	CString childName;
	
	childName.LoadString(IDS_BACKEDUP);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_BACKUP;

	childName.LoadString(IDS_PENDING);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_PENDING;

	childName.LoadString(IDS_RECOVERYLOG);
	if (childName.CompareNoCase(name) == 0)
		return ENTRY_RECOVERYLOG;

	return ENTRY_BACKUP;
}

void VDRootFolderBehaviour::GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList)
{
	vctList.clear();

	ITEMDATA data;
	data.type = NWS_FOLDER;

	data.state = ENTRY_BACKUP;
	data.sName.LoadString(IDS_BACKEDUP);
	vctList.push_back(data);

	data.state = ENTRY_PENDING;
	data.sName.LoadString(IDS_PENDING);
	vctList.push_back(data);
	
	data.state = ENTRY_RECOVERYLOG;
	data.sName.LoadString(IDS_RECOVERYLOG);
	vctList.push_back(data);
}

VDFolderBehaviourBase* VDRootFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	int state = m_PidlMgr.GetItemState(child);

	VDFolderBehaviourBase* behaviour = NULL;
	switch (state)
	{
	case ENTRY_BACKUP:
		behaviour = new VDBackupFolderBehaviour();
		break;
	case ENTRY_PENDING:
		behaviour = new VDPendingFolderBehaviour();
		break;
	case ENTRY_RECOVERYLOG:
		behaviour = new VDRecoveryFolderBehaviour();
		break;
	default:
		behaviour = new VDRootFolderBehaviour();
		break;
	}

	return behaviour;
}