#pragma once
class CVFBackupBar  :	public CWindowImpl<CVFBackupBar>
{
public:
	CVFBackupBar(void);
	~CVFBackupBar(void);

BEGIN_MSG_MAP(CVFBackupBar)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_CREATE, OnCreate)
	MESSAGE_HANDLER(WM_ERASEBKGND, OnEraseBackground)
	MESSAGE_HANDLER(WM_CTLCOLORSTATIC,OnColorStatic)
	MESSAGE_HANDLER(WM_NOTIFY,OnNotify)
END_MSG_MAP()

	LRESULT OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	void InnerPaint(HDC hDC);
private:
	CHyperLink m_browseFiles;
	CHyperLink m_launchWizard;
	CStatic m_name;
};

