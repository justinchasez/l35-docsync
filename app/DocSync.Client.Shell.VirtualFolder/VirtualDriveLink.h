// VirtualDriveLink.h : Declaration of the CVirtualDriveLink

#pragma once
#include "resource.h"       // main symbols


#include "OnlineBackupClientShellVirtualFolder_i.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CVirtualDriveLink
#include <shlobj.h>
#include <comdef.h>
#include "CNWSPidlMgr.h"


class ATL_NO_VTABLE CVirtualDriveLink :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CVirtualDriveLink, &CLSID_VirtualDriveLink>,
//	public IDispatchImpl<IVirtualDriveLink, &IID_IVirtualDriveLink, &LIBID_BackupDutyClientShellVirtualFolderLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IShellLink
{
public:
	CVirtualDriveLink()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_VIRTUALDRIVELINK)


BEGIN_COM_MAP(CVirtualDriveLink)
//	COM_INTERFACE_ENTRY(IVirtualDriveLink)
//	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShellLink)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		m_pidlRoot = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		if (m_pidlRoot != NULL)
		{
			m_PidlMgr.Delete(m_pidlRoot);
			m_pidlRoot = NULL;
		}
	}

public:

	STDMETHOD(GetPath)(LPWSTR pszFile,/* [in] */ int cch,/* [unique][out][in] */ WIN32_FIND_DATAW *pfd,/* [in] */ DWORD fFlags)
	{
		DWORD dwLen = cch;
		m_PidlMgr.GetFullName(m_pidlRoot,pszFile,&dwLen);
		cch = dwLen;

		ATLTRACE("GetPath");
		return S_OK;
	}
        
    STDMETHOD(GetIDList)(/* [out] */PIDLIST_ABSOLUTE *ppidl)
	{
		*ppidl = m_PidlMgr.Copy(m_pidlRoot);
		ATLTRACE("GetIDList");
		return S_OK;
	}
        
    STDMETHOD(SetIDList)(/* [unique][in] */ PCIDLIST_ABSOLUTE pidl)
	{
		if (m_pidlRoot == NULL)
		{
			m_pidlRoot = m_PidlMgr.Copy(pidl);
		}

		ATLTRACE("SetIDList");
		return S_OK;
	}

    STDMETHOD(GetDescription)(/* [size_is][string][out] */ LPWSTR pszName, int cch)
	{
		ATLTRACE("GetDescription");
		return E_NOTIMPL;
	}
        
    STDMETHOD(SetDescription)(/* [string][in] */ LPCWSTR pszName)
	{
		ATLTRACE("SetDescription");
		return E_NOTIMPL;
	}
        
    STDMETHOD(GetWorkingDirectory)(/* [size_is][string][out] */ LPWSTR pszDir, int cch)
	{
		ATLTRACE("GetWorkingDirectory");
		return E_NOTIMPL;
	}

    STDMETHOD(SetWorkingDirectory)(/* [string][in] */ LPCWSTR pszDir)
	{
		ATLTRACE("SetWorkingDirectory");
		return E_NOTIMPL;
	}

    STDMETHOD(GetArguments)(/* [size_is][string][out] */ LPWSTR pszArgs, /* [in] */ int cch)
	{
		ATLTRACE("GetArguments");
		return E_NOTIMPL;
	}

    STDMETHOD(SetArguments)(/* [string][in] */ LPCWSTR pszArgs)
	{
		ATLTRACE("SetArguments");
		return E_NOTIMPL;
	}
        
    STDMETHOD(GetHotkey)(/* [out] */ WORD *pwHotkey)
	{
		ATLTRACE("GetHotkey");
		return E_NOTIMPL;
	}
        
    STDMETHOD(SetHotkey)(/* [in] */ WORD wHotkey)
	{
		ATLTRACE("SetHotkey");
		return E_NOTIMPL;
	}
        
    STDMETHOD(GetShowCmd)(/* [out] */ int *piShowCmd)
	{
		ATLTRACE("GetShowCmd");
		return E_NOTIMPL;
	}
        
    STDMETHOD(SetShowCmd)(/* [in] */ int iShowCmd)
	{
		ATLTRACE("SetShowCmd");
		return E_NOTIMPL;
	}
        
    STDMETHOD(GetIconLocation)(/* [size_is][string][out] */ LPWSTR pszIconPath, /* [in] */ int cch, /* [out] */ int *piIcon)
    {
		ATLTRACE("GetIconLocation");
		return E_NOTIMPL;
	}

    STDMETHOD(SetIconLocation)(/* [string][in] */ LPCWSTR pszIconPath, /* [in] */ int iIcon)
	{
		ATLTRACE("SetPath");
		return E_NOTIMPL;
	}
        
    STDMETHOD(SetRelativePath)(/* [string][in] */ LPCWSTR pszPathRel, /* [in] */ DWORD dwReserved)
	{
		ATLTRACE("SetPath");
		return E_NOTIMPL;
	}
        
    STDMETHOD(Resolve)(/* [unique][in] */ HWND hwnd, /* [in] */ DWORD fFlags)
	{
		ATLTRACE("SetPath");
		return E_NOTIMPL;
	}
        
    STDMETHOD(SetPath)(/* [string][in] */ LPCWSTR pszFile)
	{
		ATLTRACE("SetPath");
		return E_NOTIMPL;
	}

private:
	CNWSPidlMgr m_PidlMgr;
	LPITEMIDLIST m_pidlRoot;
};

//OBJECT_ENTRY_AUTO(__uuidof(VirtualDriveLink), CVirtualDriveLink)
