#include "StdAfx.h"
#include "EventHandlerManager.h"

EventHandlerManager::EventHandlerManager(void)
{
	m_proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
	hrAdvice = this->DispEventAdvise(m_proxy);
}


EventHandlerManager::~EventHandlerManager(void)
{
	this->Unadvise(m_proxy);
}

void EventHandlerManager::AdviceWindow(HWND window)
{
	ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS);

	m_notifyWindows[window] = 1;
}

void EventHandlerManager::UnadviceWindow(HWND window)
{
	ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS);


	m_notifyWindows.erase(window);
}

void EventHandlerManager::NotifyUpdate()
{
	ATL::CComCritSecLock<ATL::CComAutoCriticalSection> Lock(m_CS);

	for (std::map<HWND,int>::iterator ii = m_notifyWindows.begin(); ii != m_notifyWindows.end(); ii++ )
	{
		::PostMessage(ii->first, WM_UPDATE_STATUS_INFO, 0, 0);
	}
}

HRESULT __stdcall EventHandlerManager::OnUpdateStatusInfo(LONG type, LONG count)
{
	ATLTRACE(_T("EventHandlerManager::OnUpdateStatusInfo()\r\n"));

	NotifyUpdate();

	return S_OK;
}

int EventHandlerManager::WM_UPDATE_STATUS_INFO = RegisterWindowMessage(_T("WM_UPDATE_STATUS_INFO"));