// dllmain.h : Declaration of module class.
#pragma once

#include "OnlineBackupClientShellVirtualFolder_i.h"
#include "ShellImageLists.h"
#include "EventHandlerManager.h"
#include "stdafx.h"
#include <map>

#define ADD_NOTIFY_WINDOW WM_USER + 12
#define REMOVE_NOTIFY_WINDOW WM_USER + 13
#define CHECK_WINDOW WM_USER + 14

class CShellMalloc
{
public:
   LPMALLOC m_pMalloc;
   HRESULT Init()
   {
      m_pMalloc = NULL;
      // It is safe to call ::SHGetMalloc()/::CoGetMalloc() without
      // first calling ::CoInitialize() according to MSDN.
      if( FAILED( ::SHGetMalloc(&m_pMalloc) ) ) 
	  {
         // TODO: TERMINATE
		  return E_FAIL;
      }
	  return S_OK;
   }
   void Term()
   {
      if( m_pMalloc!=NULL ) 
		  m_pMalloc->Release();
   }
   operator LPMALLOC() const
   {
      return m_pMalloc;
   }
   LPVOID Alloc(ULONG cb)
   {
      ATLASSERT(m_pMalloc!=NULL);
      ATLASSERT(cb>0);
      return m_pMalloc->Alloc(cb);
   }
   void Free(LPVOID p)
   {
      ATLASSERT(m_pMalloc!=NULL);
      ATLASSERT(p);
      m_pMalloc->Free(p);
   }
};

//#define BACKUPDUTYSHELLWINDOW _T("BackupDutyShellWindow")

class COnlineBackupClientShellVirtualFolderModule : public CAtlDllModuleT< COnlineBackupClientShellVirtualFolderModule >,
													public ATL::CAtlBaseModule
{
public :
	DECLARE_LIBID(LIBID_BackupDutyClientShellVirtualFolderLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ONLINEBACKUPCLIENTSHELLVIRTUALFOLDER, "{D5EFC554-ECA9-4D12-871F-14F845194E1B}")

	CShellImageLists m_imageLists;

	// Shell	Allocator
	CShellMalloc m_Allocator;

	LPITEMIDLIST m_pidlNSFROOT;

	CLIPFORMAT m_CFSTR_NSEDRAGDROP;

	USHORT m_usOSType;

	TCHAR m_szInstallPath[MAX_PATH];

	TCHAR m_commandLine[MAX_PATH];

	TCHAR m_szMyComputerShellName[MAX_PATH];

	TCHAR m_helpUrl[MAX_PATH];

	WORD	m_wWinMajor;
	WORD	m_wWinMinor;

	EventHandlerManager* EventHandlerManagerInstance()
	{
		if (m_eventHandler == NULL)
		{
			m_eventHandler = new EventHandlerManager();
		}
		return m_eventHandler;
	}

	void Init()
	{
		m_eventHandler = NULL;

		// Get Shell allocator
		m_Allocator.Init();

		m_imageLists.CreateImageLists();

		// Get Win version
		OSVERSIONINFO	ovi	= {	0 };
		ovi.dwOSVersionInfoSize =	sizeof(ovi);
		::GetVersionEx(&ovi);
		m_wWinMajor =	(WORD) ovi.dwMajorVersion;
		m_wWinMinor =	(WORD) ovi.dwMinorVersion;

		// Register the ClassName.
    // The ClassName comes from the string resource IDS_CLASSNAME!
		//TCHAR szClassName[64] = BACKUPDUTYSHELLWINDOW;
		//WNDCLASS wc = { 0 };

		//// If our window class has not been registered, then do so
		//if( !::GetClassInfo(GetModuleInstance(), szClassName, &wc) ) 
		//{
		//	//wc.style             = 0;
		//	wc.style             = CS_HREDRAW | CS_VREDRAW;
		//	wc.lpfnWndProc     = (WNDPROC)WndProc;
		//	wc.cbClsExtra      = 0;
		//	wc.cbWndExtra      = 0;
		//	wc.hInstance        = GetModuleInstance();
		//	wc.hIcon             = NULL;
		//	wc.hCursor          = ::LoadCursor(NULL, IDC_ARROW);
		//	wc.hbrBackground  = (HBRUSH)(COLOR_WINDOW + 1);
		//	wc.lpszMenuName    = NULL;
		//	wc.lpszClassName  = szClassName; 

		//	if( !::RegisterClass(&wc) ) 
		//	{
		//		HRESULT hr = ::GetLastError();
		//	}
		//}

		//CreateHelpShellWindow();
	}

	void CreateHelpShellWindow()
	{
		// Create host window
		/*ATLTRACE(_T("CreateHelpShellWindow()\r\n"));
		
		if (m_mainWindow != NULL)
		{
			::DestroyWindow(m_mainWindow);
			m_mainWindow = NULL;
		}

		m_mainWindow = ::CreateWindowEx(0,
								  BACKUPDUTYSHELLWINDOW,
								  NULL,
								  0,
								  0,
								  0,
								  100,
								  100,
								  NULL,
								  NULL,
								  GetModuleInstance(),
								  (LPVOID) this);

		if( m_mainWindow == NULL ) 
		{
			HRESULT hr = ::GetLastError();
		}*/
	}

	void	Term()
	{
		if(m_pidlNSFROOT)
			m_Allocator.Free(m_pidlNSFROOT);

		if (m_eventHandler)
		{
			delete m_eventHandler;
			m_eventHandler = NULL;
		}
		
		m_Allocator.Term();
	}

	//static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam)
	//{
	//	MSG msg = { hWnd, uMessage, wParam, lParam, 0, { 0, 0 } };

	//	// pass to the message map to process
	//	LRESULT lRes=1;
	//	BOOL bRet = FALSE;

	//	//if (uMessage == ADD_NOTIFY_WINDOW)
	//	//{
	//	//	notifyWindows[(HWND)wParam] = lParam;
	//	//	bRet = TRUE;
	//	//}
	//	//else if (uMessage == REMOVE_NOTIFY_WINDOW)
	//	//{
	//	//	notifyWindows.erase((HWND)wParam);
	//	//	bRet = TRUE;
	//	//}
	//	//else if (uMessage == WM_UPDATE_STATUS_INFO)
	//	//{
	//	//	for (std::map<HWND,int>::iterator ii = notifyWindows.begin(); ii != notifyWindows.end(); ii++ )
	//	//	{
	//	//		::PostMessage(ii->first,uMessage,wParam,lParam);
	//	//	}

	//	//	bRet = TRUE;
	//	//}
	//	//else if ( (uMessage == WM_DESTROY) || (uMessage == WM_QUIT) || (uMessage == WM_CLOSE))
	//	//{
	//	//	bRet = TRUE;
	//	//	//ATLASSERT(FALSE);
	//	//}
	//	//else if (uMessage == CHECK_WINDOW)
	//	//{
	//	//	bRet = TRUE;
	//	//	lRes = 9999;
	//	//}

	//	if( !bRet ) 
	//		lRes = ::DefWindowProc(hWnd,uMessage, wParam, lParam);

	//	return lRes;
	//}

private:
	EventHandlerManager* m_eventHandler;

};

extern class COnlineBackupClientShellVirtualFolderModule _Module;
