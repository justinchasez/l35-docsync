#ifndef _NEF_CPIDLENUM_H_
#define _NEF_CPIDLENUM_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CNWSPidlMgr.h"
#include <map>
#include "VDFolderBehaviourBase.h"

typedef struct tagENUMLIST
{
	struct tagENUMLIST   *pNext;
	LPITEMIDLIST         pidl;
}ENUMLIST, FAR *LPENUMLIST;

/////////////////////////////////////////////////////////////////////////////
// CPidlEnum

class ATL_NO_VTABLE CPidlEnum : 
   public CComObjectRootEx<CComSingleThreadModel>,
   public IEnumIDList
{
public:

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CPidlEnum)
    COM_INTERFACE_ENTRY_IID(IID_IEnumIDList,IEnumIDList)
END_COM_MAP()

private:
   LPENUMLIST m_pFirst;
   LPENUMLIST m_pLast;
   LPENUMLIST m_pCurrent;
   CNWSPidlMgr  m_PidlMgr;

   std::map<int,LPCUSTOMFOLDER_DATA> m_customFolderData;

   int m_dataIndex;

public:
	HRESULT FinalConstruct()
	{
		m_pFirst = m_pLast = m_pCurrent = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		DeleteList();
		m_pFirst = m_pLast = m_pCurrent = NULL;
	}

public:
	//IEnumIDList
	STDMETHOD (Next) (DWORD, LPITEMIDLIST*, LPDWORD);
	STDMETHOD (Skip) (DWORD);
	STDMETHOD (Reset) (void);
	STDMETHOD (Clone) (LPENUMIDLIST*);

	HRESULT  _AddPidls(ITEM_TYPE iItemType, int state, LPCTSTR pszName);
	
	void AddCustomFolderData(LPCUSTOMFOLDER_DATA data);
	LPCUSTOMFOLDER_DATA GetCustomFolderData();


	BOOL DeleteList(void);
	BOOL AddToEnumList(LPITEMIDLIST pidl);
    HRESULT _Init(VDFolderBehaviourBase& behaviour, LPITEMIDLIST pidl, DWORD dwFlags);
};

#endif //_NEF_CPIDLENUM_H_