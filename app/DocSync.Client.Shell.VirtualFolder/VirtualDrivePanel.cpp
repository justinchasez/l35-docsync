#include "StdAfx.h"
#include "VirtualDrivePanel.h"
#include "resource.h"
#include "VFResources.h"

#define BLOCK_SIZE_Y 100
#define BLOCK_SIZE_X 250
#define TITLE_Y	5

CVirtualDrivePanel::CVirtualDrivePanel(void)
{
	m_ownerWindow = NULL;
}

CVirtualDrivePanel::~CVirtualDrivePanel(void)
{
}

void CVirtualDrivePanel::Init(HWND hWnd, int type)
{
	m_type = type;
	m_ownerWindow = hWnd;
}

LRESULT CVirtualDrivePanel::OnGetOwnerWindow(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	return (LRESULT)m_ownerWindow;
}

LRESULT CVirtualDrivePanel::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {0,0,BLOCK_SIZE_X,BLOCK_SIZE_Y};

	m_titleBlock.Create(m_hWnd,rcDefault,NULL,WS_CHILD | WS_VISIBLE );
	switch (m_type)
	{
	case ENTRY_ROOT:
		m_backupBlock.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		m_viewStatusBlock.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		m_helpBlock.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_BACKUP:
		m_backedupPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_PENDING:
		m_pendingPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_RECOVERYLOG:
		m_recoveryPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_RECOVERY_COMPLETED:
		m_recCompletedPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_RECOVERY_ERRORS:
		m_recErrorsPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	case ENTRY_RECOVERY_PENDING:
		m_recPendigPanel.Create(m_hWnd,rect,NULL,WS_CHILD | WS_VISIBLE );
		break;
	}

	m_contentSize.cx = 3*BLOCK_SIZE_X;
	m_contentSize.cy = BLOCK_SIZE_Y + TITLE_Y;

	SetScrollOffset(0, 0, FALSE);
	SetScrollSize(m_contentSize);

	return 1;
}

#define SIZE_PANEL(panel) \
{ \
	SIZE szCalc = {sz.cx-20, sz.cy - TITLE_Y}; \
	szCalc = panel.CalcSize(szCalc, false); \
	\
	xx = sz.cx; \
	yy = szCalc.cy + TITLE_Y; \
	\
	panel.MoveWindow(x-ptOffset.x, y-ptOffset.y, szCalc.cx, szCalc.cy); \
} \

LRESULT CVirtualDrivePanel::OnSizePanel(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (!IsWindow() || lParam == 0)
		return 0;

	SIZE sz = {LOWORD(lParam), HIWORD(lParam)};

	ATLTRACE(_T("width %d  height %d\r\n"),sz.cx,sz.cy);

	if (sz.cx < 150)
	{
		sz.cx = 150; // min width
	}
	if (sz.cy < 50)
	{
		sz.cy = 50; // min height
	}

	int x = 0;
	int y = TITLE_Y;

	int xx = x;
	int yy = y;

	POINT ptOffset;
	GetScrollOffset(ptOffset);

	if (m_titleBlock.IsWindow())
		m_titleBlock.MoveWindow(0-ptOffset.x,0-ptOffset.y,sz.cx,TITLE_Y);

	switch (m_type)
	{
	case ENTRY_ROOT:
		if (m_backupBlock.IsWindow())
			m_backupBlock.MoveWindow(x-ptOffset.x,y-ptOffset.y,BLOCK_SIZE_X,BLOCK_SIZE_Y);

		x+=BLOCK_SIZE_X;

		if (sz.cx < x+BLOCK_SIZE_X)
		{
			y+=BLOCK_SIZE_Y;
			yy+=BLOCK_SIZE_Y;
			x=0;
		}
		else
			xx+=BLOCK_SIZE_X;


		if (m_viewStatusBlock.IsWindow())
			m_viewStatusBlock.MoveWindow(x-ptOffset.x,y-ptOffset.y,BLOCK_SIZE_X,BLOCK_SIZE_Y);

		x+=BLOCK_SIZE_X;

		if (sz.cx < x+BLOCK_SIZE_X)
		{
			y+=BLOCK_SIZE_Y;
			yy+=BLOCK_SIZE_Y;
			x=0;
		}
		else
			xx+=BLOCK_SIZE_X;

		if (m_helpBlock.IsWindow())
			m_helpBlock.MoveWindow(x-ptOffset.x,y-ptOffset.y,BLOCK_SIZE_X,BLOCK_SIZE_Y);

		xx+=BLOCK_SIZE_X;
		yy+=BLOCK_SIZE_Y;
		break;
	case ENTRY_BACKUP:
		if (m_backedupPanel.IsWindow())
		{
			SIZE_PANEL(m_backedupPanel);
		}
		break;
	case ENTRY_PENDING:
		if (m_pendingPanel.IsWindow())
		{
			SIZE_PANEL(m_pendingPanel);
		}
		break;
	case ENTRY_RECOVERYLOG:
		if (m_recoveryPanel.IsWindow())
		{
			SIZE_PANEL(m_recoveryPanel);
		}
		break;
	case ENTRY_RECOVERY_COMPLETED:
		if (m_recCompletedPanel.IsWindow())
		{
			SIZE_PANEL(m_recCompletedPanel);
		}
		break;
	case ENTRY_RECOVERY_ERRORS:
		if (m_recErrorsPanel.IsWindow())
		{
			SIZE_PANEL(m_recErrorsPanel);
		}
		break;
	case ENTRY_RECOVERY_PENDING:
		if (m_recPendigPanel.IsWindow())
		{
			SIZE_PANEL(m_recPendigPanel);
		}
		break;
	}

	if ((m_contentSize.cx != xx) || (m_contentSize.cy != yy))
	{
		m_contentSize.cx = xx;
		m_contentSize.cy = yy;
		SetScrollOffset(0, 0, FALSE);

		ATLTRACE(_T("width XX %d  height YY %d\r\n"),xx,yy);

		SetScrollSize(xx,yy);
	}

	Invalidate();

	return OnSize(uMsg, wParam, lParam, bHandled);
}

LRESULT CVirtualDrivePanel::OnEraseBackground(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	HDC dc =(HDC)wParam;

	RECT rect;
	GetClientRect(&rect);
	POINT pt;
	GetScrollOffset(pt);

	rect.top += pt.y;
	rect.bottom += pt.y;
	rect.left += pt.x;
	rect.right += pt.x;

	HDC memDC = ::CreateCompatibleDC(dc);
	HGDIOBJ object = SelectObject(memDC,CVFResources::instance().BackgroundBitmap());

	::StretchBlt(dc,0,0,rect.right-rect.left,rect.bottom-rect.top,memDC,0,0,770,150,SRCCOPY);

	::SelectObject(memDC,object);
    ::DeleteDC(memDC);

	return 0;
}

void CVirtualDrivePanel::DoPaint(CDCHandle dc)
{
	RECT rect;
	GetClientRect(&rect);
	POINT pt;
	GetScrollOffset(pt);

	rect.top += pt.y;
	rect.bottom += pt.y;
	rect.left += pt.x;
	rect.right += pt.x;

	HDC memDC = ::CreateCompatibleDC(dc.m_hDC);
	HGDIOBJ object = SelectObject(memDC,CVFResources::instance().BackgroundBitmap());

	dc.StretchBlt(0,0,rect.right-rect.left,rect.bottom-rect.top,memDC,0,0,770,150,SRCCOPY);
	//dc.FillRect(&rect,COLOR_WINDOW);

	::SelectObject(memDC,object);
    ::DeleteDC(memDC);
}
