//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OnlineBackup.Client.Shell.VirtualFolder.rc
//
#define IDS_PROJNAME                    100
#define IDR_ONLINEBACKUPCLIENTSHELLVIRTUALFOLDER 101
#define IDS_VFNAME                      101
#define IDS_RLOG_COMPLETED              102
#define IDR_VIRTUALDRIVEFOLDER          103
#define IDS_RLOG_ERRORS                 103
#define IDR_VIRTUALDRIVEVIEW            104
#define IDS_RLOG_PENDING                104
#define IDR_EXTRACTICON                 105
#define IDR_VDFCONTEXTMENU              106
#define IDR_VIRTUALDRIVEPANEL           107
#define IDS_BACKEDUP                    108
#define IDS_PENDING                     109
#define IDS_RECOVERYLOG                 110
#define IDR_VIRTUALDRIVELINK            118
#define IDR_VDDATAOBJECT                119
#define IDI_ICON1                       201
#define IDB_BITMAP1                     202
#define IDR_BACKEDUP_FOLDER_MENU        203
#define IDR_PENDING_FOLDER_MENU         204
#define IDR_BACKEDUP_FILE_MENU          205
#define IDR_MENU1                       206
#define IDR_PENDING_FILE_MENU           206
#define IDR_RECOVERY_ERRORS_MENU        207
#define IDR_MENU3                       208
#define IDR_RECOVERY_PENDING_MENU       208
#define IDI_ICON_FILE                   209
#define IDI_ICON_FOLDER                 210
#define IDB_PNG_BKG                     211
#define IDB_BITMAP2                     212
#define IDB_BMP_BKG                     212
#define ID_MENU_OPEN                    32768
#define ID_MENU_EXPLORER                32769
#define ID_MENU_RESTORE                 32770
#define ID_MENU_RESTORETO               32771
#define ID_MENU_DONOTBACK               32772
#define ID_MENU_RESTORETOPREVIOUSVERSION 32773
#define ID_MENU_REMOVEFROMBACKUP        32774
#define ID_MENU_RESTARTRESTOREJOB       32775
#define ID_MENU_HIGHPRIORITY            32776
#define ID_MENU_NORMALPRIORITY          32777
#define ID_MENU_LOWPRIORITY             32778
#define ID_MENU_CANCELRESTORE           32779
#define ID_MENU_COPYSHAREDLINK          32780

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        213
#define _APS_NEXT_COMMAND_VALUE         32781
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           120
#endif
#endif
