#pragma once
#include "vdfolderbehaviourbase.h"

class VDPendingFolderBehaviour :
	public VDFolderBehaviourBase
{
public:
	VDPendingFolderBehaviour(void);
	virtual ~VDPendingFolderBehaviour(void);

	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child);
	virtual int TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index);

	virtual int GetPanelMode(LPCITEMIDLIST pidl);

	virtual int StateChildByName(const CString& name);
};

