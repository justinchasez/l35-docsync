#include "StdAfx.h"
#include "VFResources.h"


CVFResources::CVFResources(void)
{
	init();
}


CVFResources::~CVFResources(void)
{
}

void CVFResources::init()
{
	m_hyperLinkFont.CreateFont(
        16,                        // nHeight
        0,                         // nWidth
        0,                         // nEscapement
        0,                         // nOrientation
        FW_NORMAL,				   // nWeight
        TRUE,                     // bItalic
        FALSE,                     // bUnderline
        0,                         // cStrikeOut
        ANSI_CHARSET,              // nCharSet
        OUT_DEFAULT_PRECIS,        // nOutPrecision
        CLIP_DEFAULT_PRECIS,       // nClipPrecision
        DEFAULT_QUALITY,           // nQuality
        DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
        _T("Arial"));          // lpszFacename

	m_staticFont.CreateFont(
        20,                        // nHeight
        0,                         // nWidth
        0,                         // nEscapement
        0,                         // nOrientation
		FW_BOLD,				   // nWeight
        FALSE,                     // bItalic
        FALSE,                     // bUnderline
        0,                         // cStrikeOut
        ANSI_CHARSET,              // nCharSet
        OUT_DEFAULT_PRECIS,        // nOutPrecision
        CLIP_DEFAULT_PRECIS,       // nClipPrecision
        DEFAULT_QUALITY,           // nQuality
        DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
        _T("Arial"));          // lpszFacename

	m_titleFont.CreateFont(
        16,                        // nHeight
        0,                         // nWidth
        0,                         // nEscapement
        0,                         // nOrientation
        FW_BOLD,				   // nWeight
        FALSE,                     // bItalic
        FALSE,                     // bUnderline
        0,                         // cStrikeOut
        ANSI_CHARSET,              // nCharSet
        OUT_DEFAULT_PRECIS,        // nOutPrecision
        CLIP_DEFAULT_PRECIS,       // nClipPrecision
        DEFAULT_QUALITY,           // nQuality
        DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
        _T("Arial"));          // lpszFacename

	m_helpIcon.LoadIcon(IDI_ICON1);
	m_backupIcon.LoadIcon(IDI_ICON1);
	m_viewstatusIcon.LoadIcon(IDI_ICON1);

	m_background.LoadBitmap(IDB_BMP_BKG);
}

HBITMAP CVFResources::BackgroundBitmap()
{
	return m_background.m_hBitmap;
}

HFONT CVFResources::HyperLinkFont()
{
	return m_hyperLinkFont.m_hFont;
}

HFONT CVFResources::StaticFont() // for static controls in blocks
{
	return m_staticFont.m_hFont;
}

HFONT CVFResources::TitleFont() // for title bar text
{
	return m_titleFont.m_hFont;
}

CIcon& CVFResources::HelpIcon() // icon for help block
{
	return m_helpIcon;
}

CIcon& CVFResources::ViewStatusIcon() // icon for view status block
{
	return m_viewstatusIcon;
}

CIcon& CVFResources::BackupIcon() // icon for backup icon
{
	return m_backupIcon;
}

COLORREF CVFResources::LinkColor() // color for links
{
	return RGB(27,119,254);
}

COLORREF CVFResources::LinkVisitedColor() // color for visited links
{
	return RGB(27,119,254);
}

COLORREF CVFResources::TitleColor() // text color for title bar
{
	return RGB(255,255,255);
}

COLORREF CVFResources::StaticColor() // text color for static controls in blocks
{
	return RGB(0,0,0);
}
