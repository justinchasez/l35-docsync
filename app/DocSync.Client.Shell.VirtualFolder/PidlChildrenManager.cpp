#include "StdAfx.h"
#include "PidlChildrenManager.h"
#include "atlsafe.h"
#include "resource.h"

PidlChildrenManager::PidlChildrenManager(void)
{
}


PidlChildrenManager::~PidlChildrenManager(void)
{
}

void PidlChildrenManager::FillChirdren(const CString& fullPath, CPidlData& data)
{
	data.m_children.clear();

	switch (data.m_type)
	{
	case PIDL_ROOT_FOLDER:
		FillRootChirdren(fullPath,data);
		break;
	case PIDL_BACKEDUP_FOLDER:
		FillBackedUpChirdren(fullPath,data);
		break;
	case PIDL_PENDING_BACKUP_FOLDER:
		FillPendingChirdren(fullPath,data);
		break;
	case PIDL_RECOVERYLOG_FOLDER:
		FillRecoveryLogChirdren(fullPath,data);
		break;
	case PIDL_INSIDE_ENTRY:
		FillEntryChirdren(fullPath,data);
		break;
	}
}

void PidlChildrenManager::FillRootChirdren(const CString& fullPath, CPidlData& data)
{
	CString name;
	name.LoadString(IDS_BACKEDUP);
	data.m_children.push_back(CPidlData(PIDL_BACKEDUP_FOLDER,ENTRY_BACKUP,true,name));
	name.LoadString(IDS_PENDING);
	data.m_children.push_back(CPidlData(PIDL_PENDING_BACKUP_FOLDER,ENTRY_PENDING,true,name));
	name.LoadString(IDS_RECOVERYLOG);
	data.m_children.push_back(CPidlData(PIDL_RECOVERYLOG_FOLDER,ENTRY_RECOVERYLOG,true,name));
}

void PidlChildrenManager::FillEntryChirdren(const CString& fullPath, CPidlData& data)
{
	try
	{
		//ATLTRACE(_T("PidlChildrenManager()::FillEntryChirdren(%d)%s\n"), data.m_entry_type,fullPath);

		VARIANT entries;
		::VariantInit(&entries);

		ICppProxyPtr proxy;
		proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
		//ICppProxy* proxy = ProxyServerContainer::Instance();
		if (proxy)
			proxy->GetEntries(_bstr_t(fullPath.AllocSysString()),data.m_entry_type,&entries);

		int type = VT_ARRAY | VT_BSTR;
		if (entries.vt == (VT_ARRAY | VT_BSTR))
		{
			long iLBound = 0;
			long iUBound = 0;

			UINT dim = SafeArrayGetDim(entries.parray);
			SafeArrayGetLBound(entries.parray, 1, &iLBound);
			SafeArrayGetUBound(entries.parray, 1, &iUBound);

			BSTR value = NULL;
			for (long i = 0; i <= iUBound - iLBound; i++)
			{
				if (SafeArrayGetElement(entries.parray, &i, &value) == S_OK)
				{
					bool isFolder = true;
					CString name(value);
					if (name.GetLength()>0)
					{
						isFolder = name.GetAt(0) == '1';
						name.Delete(0,1);
					}
					data.m_children.push_back(CPidlData(data.m_type,data.m_entry_type,isFolder,name));
					::SysFreeString(value);
				}
				else
					break;
			}
			::VariantClear(&entries);
		}
	}
	catch(...)	{/*do nothing*/}
}

void PidlChildrenManager::FillBackedUpChirdren(const CString& fullPath, CPidlData& data)
{
	FillEntryChirdren(fullPath,data);
}

void PidlChildrenManager::FillPendingChirdren(const CString& fullPath, CPidlData& data)
{
	FillEntryChirdren(fullPath,data);
}

void PidlChildrenManager::FillRecoveryLogChirdren(const CString& fullPath, CPidlData& data)
{
}
