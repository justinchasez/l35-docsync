#include "StdAfx.h"
#include "VDRecoveryErrorsFolderBehaviour.h"
#include "CNWSPidlMgr.h"
#include "ATLComTime.h"
#include "..\OnlineBackup.Client.Shell.ContextMenu\config.h"

VDRecoveryErrorsFolderBehaviour::VDRecoveryErrorsFolderBehaviour(void)
{
}


VDRecoveryErrorsFolderBehaviour::~VDRecoveryErrorsFolderBehaviour(void)
{
}

int VDRecoveryErrorsFolderBehaviour::StateChildByName(const CString& name)
{
	return ENTRY_RECOVERY_ERRORS;
}

void VDRecoveryErrorsFolderBehaviour::CreateColumns(WTL::CListViewCtrl& wndList)
{
	wndList.InsertColumn ( 0, _T("Name"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 1, _T("Source"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 2, _T("Destination"), LVCFMT_LEFT, 120, 0 );
	wndList.InsertColumn ( 3, _T("State"), LVCFMT_LEFT, 80, 0 );
	wndList.InsertColumn ( 4, _T("LastRestored"), LVCFMT_LEFT, 80, 0 );
}

int VDRecoveryErrorsFolderBehaviour::TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	POINT point = {0,0};
	::GetCursorPos(&point);

	long versions = 0;
	long state = STATE_NOTHING;

	ICppProxyPtr cpp_proxy = proxy;

	CString source;
	listCtrl.GetItemText(index,1,source);

	if (cpp_proxy)
	{
		cpp_proxy->CheckFile(_bstr_t(source.AllocSysString()),&state,&versions);
	}

	int cmd = 0;

	//if (state != STATE_NOTHING && state != STATE_NEW)
	{
		HMENU hMenu = ::LoadMenu(_Module.GetResourceInstance(),MAKEINTRESOURCE(IDR_RECOVERY_ERRORS_MENU));

		HMENU hSubMenu = ::GetSubMenu(hMenu,0);

		cmd = TrackPopupMenu(hSubMenu,TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, point.x, point.y, 0, window, NULL);
		::DestroyMenu(hMenu);
	}

	return cmd;
}

VDFolderBehaviourBase* VDRecoveryErrorsFolderBehaviour::CreateChildBehaviour(LPCITEMIDLIST child)
{
	return new VDRecoveryErrorsFolderBehaviour();
}

void VDRecoveryErrorsFolderBehaviour::SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data)
{
	if (data == NULL)
		return;

	LPRECOVERY_DATA lpData = (LPRECOVERY_DATA)data;

	if (lpData)
	{
		COleDateTime datetime(lpData->datetime);

		listCtrl.SetItemText(index, 1, lpData->Source);
		listCtrl.SetItemText(index, 2, lpData->Destination);
		listCtrl.SetItemText(index, 3, lpData->State);
		listCtrl.SetItemText(index, 4, datetime.Format());
	}
}

void VDRecoveryErrorsFolderBehaviour::Command(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, int cmd, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
{
	ICppProxyPtr shell_proxy = proxy;

	int sel_index = listCtrl.GetNextItem(-1, LVNI_SELECTED);

	while (sel_index > -1)
	{
		CString source;
		CString destination;

		listCtrl.GetItemText(sel_index, 1, source);
		listCtrl.GetItemText(sel_index, 2, destination);
		shell_proxy->RestartRestoreJob(_bstr_t(source.AllocSysString()), _bstr_t(destination.AllocSysString()));

		sel_index = listCtrl.GetNextItem(sel_index, LVNI_SELECTED);
	}
}

DWORD VDRecoveryErrorsFolderBehaviour::FolderViewMode(UINT viewMode)
{
	return LVS_REPORT;
}

void VDRecoveryErrorsFolderBehaviour::ParseName(CString& name)
{
	int pos = name.ReverseFind('\\');
	if (pos >= 0)
	{
		name = name.Right(name.GetLength()-pos-1);
	}
}

LPCUSTOMFOLDER_DATA VDRecoveryErrorsFolderBehaviour::CreateCustomData(const CString& name, IUnknown* proxy)
{
	if (proxy == NULL)
		return NULL;

	LPRECOVERY_DATA lpData = new RECOVERY_DATA();
	lpData->Source = name;

	ICppProxyPtr proxyObject = proxy;
	proxyObject->GetRecoveryData(_bstr_t(name.AllocSysString()),lpData->Destination.GetAddress(),lpData->State.GetAddress(),lpData->Priority.GetAddress(),&lpData->datetime);

	return lpData;
}