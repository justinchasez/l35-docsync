// ExtractIcon.h : Declaration of the CExtractIcon

#pragma once
#include "resource.h"       // main symbols



#include "OnlineBackupClientShellVirtualFolder_i.h"
#include "CNWSPidlMgr.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

using namespace ATL;


// CExtractIcon

class ATL_NO_VTABLE CExtractIcon :
	public CComObjectRootEx<CComSingleThreadModel>,
	public IExtractIcon
{
public:
	CExtractIcon()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_EXTRACTICON)


BEGIN_COM_MAP(CExtractIcon)
	COM_INTERFACE_ENTRY_IID(IID_IExtractIcon,IExtractIcon)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	LPITEMIDLIST m_pidl;
	CNWSPidlMgr  m_PidlMgr;

    HRESULT _Init(LPCITEMIDLIST pidl)
    {
		m_pidl=m_PidlMgr.Copy(pidl);
        return S_OK;
    }

    // GetIconLocation:Retrieves the location and index of an icon. 
    STDMETHOD(GetIconLocation)(UINT uFlags, 
                               LPTSTR pszIconFile, 
                               UINT cchMax, 
                               LPINT piIndex, 
                               LPUINT puFlags)
    {
        ATLTRACE2(atlTraceCOM, 0, _T("IExtractIcon::GetIconLocation\n"));
        ATLASSERT(piIndex);
        ATLASSERT(puFlags);

		if(m_pidl == NULL || m_pidl->mkid.cb == 0)
			return E_FAIL;

		ITEM_TYPE  type;
		type=m_PidlMgr.GetItemType(m_pidl);

        switch( type) 
		{
        case NWS_FOLDER:
            *piIndex = 0;//(uFlags & GIL_OPENICON)==0 ? ICON_INDEX_FOLDER : ICON_INDEX_FOLDER_OPEN;
            break;

		case NWS_FILE:
			*piIndex = 1;//ICON_INDEX_FILE;
			break;

        default:
			return E_FAIL;            
            break;
        }

        if( pszIconFile ) 
		{
            // HACK: Explorer needs *something*! It also must be unique...
            ::wsprintf(pszIconFile, _T("NSF%ld%ld"), *piIndex, (long) uFlags);
        }
        *puFlags = GIL_NOTFILENAME ;

        return S_OK;
    }

    STDMETHOD(Extract)(LPCTSTR /*pstrName*/, 
                             UINT nIconIndex, 
                             HICON *phiconLarge, 
                             HICON *phiconSmall, 
                             UINT /*nIconSize*/)
    {
        ATLTRACE2(atlTraceCOM, 0, _T("IExtractIcon::Extract\n"));
        ATLASSERT(phiconLarge);
        ATLASSERT(phiconSmall);

        *phiconLarge = ImageList_ExtractIcon(NULL, _Module.m_imageLists.m_hImageListLarge, nIconIndex);
        *phiconSmall = ImageList_ExtractIcon(NULL, _Module.m_imageLists.m_hImageListSmall, nIconIndex);

        return S_OK;
    };

};

//OBJECT_ENTRY_AUTO(__uuidof(ExtractIcon), CExtractIcon)
