#ifndef _CNWSPIDLMGR_H_
#define _CNWSPIDLMGR_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "comutil.h"

typedef	enum tagITEM_TYPE
{
	NWS_FOLDER=0x00000001,
	NWS_FILE=0x00000002,
} ITEM_TYPE;

#define ENTRY_ROOT					0
#define ENTRY_BACKUP				1
#define ENTRY_PENDING				2
#define ENTRY_RECOVERYLOG			3
#define ENTRY_RECOVERY_COMPLETED	4
#define ENTRY_RECOVERY_ERRORS		5
#define ENTRY_RECOVERY_PENDING		6

typedef class CustomFolderData
{
public:
	virtual ~CustomFolderData() {};

	int count;
} CUSTOMFOLDER_DATA, FAR* LPCUSTOMFOLDER_DATA;

class tagRecoveryData : public CUSTOMFOLDER_DATA
{
public:
	virtual ~tagRecoveryData() {};

	_bstr_t Source;
	_bstr_t Destination;
	DATE datetime;
	_bstr_t State;
	_bstr_t Priority;
};

typedef tagRecoveryData RECOVERY_DATA, *LPRECOVERY_DATA;

typedef struct tagPIDLDATA
   {
   ITEM_TYPE type;
   int state;
   TCHAR    szName[1];
   }PIDLDATA, FAR *LPPIDLDATA;

typedef struct tagITEMDATA
   {
	   ITEM_TYPE type;
	   int state;
	   CString sName;
   }ITEMDATA, FAR *LPITEMDATA;

class CNWSPidlMgr
{
public:

	LPITEMIDLIST Create(ITEM_TYPE iItemType, int state, LPCTSTR szName);
	void Delete(LPITEMIDLIST pidl);
	LPITEMIDLIST GetNextItem(LPCITEMIDLIST pidl);
	LPITEMIDLIST GetLastItem(LPCITEMIDLIST pidl);
	UINT GetByteSize(LPCITEMIDLIST pidl);
	bool IsSingle(LPCITEMIDLIST pidl);
	int Count(LPCITEMIDLIST pidl);
	LPITEMIDLIST Concatenate(LPCITEMIDLIST, LPCITEMIDLIST);
	LPITEMIDLIST Copy(LPCITEMIDLIST pidlSrc);

	LPITEMIDLIST ConcatenateToPart(LPCITEMIDLIST pidlEnd, LPCITEMIDLIST pidlStart, int count);
	
	// Retrieve encrypted file name (without the path)
	// The pidl MUST remain valid until the caller has finished with the returned string.
	HRESULT GetName(LPCITEMIDLIST,LPTSTR);

	// Retrieve the item type (see above)
	ITEM_TYPE GetItemType(LPCITEMIDLIST pidl);
	int GetItemState(LPCITEMIDLIST pidl);
	
	HRESULT GetFullName(LPCITEMIDLIST pidl,LPTSTR szFullName,DWORD *pdwLen); 
	HRESULT GetInternalPath(LPCITEMIDLIST pidl,LPTSTR szFullName,DWORD *pdwLen);
	BOOL  HasSubFolder(LPCITEMIDLIST pidl); 
	HRESULT GetItemAttributes(LPCITEMIDLIST pidl,USHORT iAttrNum,LPTSTR pszAttrOut);

private:
	LPPIDLDATA GetDataPointer(LPCITEMIDLIST);

};

#endif//_CNWSPIDLMGR_H_