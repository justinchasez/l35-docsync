#include "StdAfx.h"
#include "PendingPanel.h"
#include "VFResources.h"

PendingPanel::PendingPanel(void)
{
}


PendingPanel::~PendingPanel(void)
{
//	::PostMessage(_Module.m_mainWindow,REMOVE_NOTIFY_WINDOW,(WPARAM)m_hWnd,1);
}

LRESULT PendingPanel::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	_Module.EventHandlerManagerInstance()->UnadviceWindow(m_hWnd);

	return 0;
}

LRESULT PendingPanel::OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (IsWindow())
	{
		try
		{
			ICppProxyPtr proxy = NULL;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

			if (proxy)
			{
				CString str;

				long value = 0;
				proxy->GetStatusInfo(STATUS_BACKEDUP_ALL, &value);

				str.Format(_T("- View all the files and folders in my backup (%d files)"),value);
				m_files.SetLabel(str);

				Invalidate();

				GetParent().Invalidate();

				RECT rect = {0};
				GetWindowRect(&rect);
				PostMessage(WM_SIZE,0,MAKELPARAM(rect.right-rect.left, rect.bottom-rect.top));
			}
		}
		catch (...) { /*do nothing*/ }
	}

	return 0;
}

LRESULT PendingPanel::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {20,20,280,40};

	RECT rcName = {20,2,280,40};
	m_name.Create(m_hWnd,rcName,NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().StaticFont());
	m_name.SetWindowText(_T("This folder displays only the files waiting to be backed-up or updated. Right click on any file to remove it from your back-up, or to restore a previous version of the file"));

	CString str;

	m_files.Create(m_hWnd,rect, NULL, WS_CHILD | WS_VISIBLE);
	m_files.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- View all the files and folders in my backup (%d files)"),0);
	m_files.SetLabel(str);
	m_files.SetHyperLink(_T("All the files and folders in my backup"));
	m_files.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_files.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_files.m_clrLink = CVFResources::instance().LinkColor();
	m_files.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	_Module.EventHandlerManagerInstance()->AdviceWindow(m_hWnd);
	_Module.EventHandlerManagerInstance()->NotifyUpdate();

	return 0;
}

SIZE PendingPanel::CalcSize(SIZE& sz, bool moved)
{
	if (m_name.IsWindow() && m_files.IsWindow() && (sz.cx + sz.cy) > 0)
	{
		RECT rect = {0,0,sz.cx - 20,sz.cy};

		MoveChild(m_name, rect, moved);

		rect.top = rect.bottom;
		rect.bottom = sz.cy;
		rect.left = 0;
		rect.right = sz.cx - 20;

		MoveChild(m_files, rect, moved);

		sz.cy = rect.bottom;
	}

	return sz;
}

LRESULT PendingPanel::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SIZE sz = {LOWORD(lParam), HIWORD(lParam)};

	CalcSize(sz, true);

	return 0;
}

LRESULT PendingPanel::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NMHDR* nmhdr = (NMHDR*)lParam;

	if (m_files.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,0);
		}
	}

	return 0;
}

LRESULT PendingPanel::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().StaticColor());
	SetBkMode((HDC)wParam,TRANSPARENT);
	return (LRESULT)GetStockObject(NULL_BRUSH);
}

void PendingPanel::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);

	::FillRect(hDC,&rect,GetSysColorBrush(COLOR_WINDOW));*/

	//CVFResources::instance().ViewStatusIcon().DrawIconEx(hDC,2,2,16,16,0);
}

LRESULT PendingPanel::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{

	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT PendingPanel::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}