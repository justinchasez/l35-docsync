// VirtualDriveView.cpp : Implementation of CVirtualDriveView

#include "stdafx.h"
#include "VirtualDriveView.h"
#include "dllmain.h"
#include "..\OnlineBackup.Client.Shell.ContextMenu\config.h"
#include <vector>
#include <stack>
#include "PidlEnum.h"
#include "ATLComTime.h"

#pragma comment(lib,"shlwapi")  // link with shlwapi since we use StrFormatByteSize()

UINT CVirtualDriveView::sm_uListID = 101;
UINT CVirtualDriveView::sm_uPanelID = 102;
// CVirtualDriveView

HRESULT ViewModeFromSVID(const SHELLVIEWID *pvid, FOLDERVIEWMODE *pViewMode)
{
    HRESULT hr = S_OK;

    if (IsEqualIID(*pvid, VID_LargeIcons))
        *pViewMode = FVM_ICON;
    else if (IsEqualIID(*pvid, VID_SmallIcons))
        *pViewMode = FVM_SMALLICON;
    else if (IsEqualIID(*pvid, VID_Thumbnails))
        *pViewMode = FVM_THUMBNAIL;
    else if (IsEqualIID(*pvid, VID_ThumbStrip))
        *pViewMode = FVM_THUMBSTRIP;
    else if (IsEqualIID(*pvid, VID_List))
        *pViewMode = FVM_LIST;
    else if (IsEqualIID(*pvid, VID_Tile))
        *pViewMode = FVM_TILE;
    else if (IsEqualIID(*pvid, VID_Details))
        *pViewMode = FVM_DETAILS;
    else
    {
        *pViewMode = FVM_ICON;
        hr = E_INVALIDARG;
    }
    return hr;
}

HRESULT SVIDFromViewMode(FOLDERVIEWMODE mode, SHELLVIEWID * svid)
{
    switch (mode)
    {
    default:
    case FVM_ICON:
        *svid = VID_LargeIcons;
        return S_OK;
    case FVM_SMALLICON:
        *svid = VID_SmallIcons;
        return S_OK;
    case FVM_LIST:
        *svid = VID_List;
        return S_OK;
    case FVM_DETAILS:
        *svid = VID_Details;
        return S_OK;
    case FVM_THUMBNAIL:
        *svid = VID_Thumbnails;
        return S_OK;
    case FVM_TILE:
        *svid = VID_Tile;
        return S_OK;
    case FVM_THUMBSTRIP:
        *svid = VID_ThumbStrip;
        return S_OK;
    }
}

LRESULT OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	MessageBox(NULL,_T("OnUpdateStatusInfo"),_T("BisovTest"),MB_OK);
	return 0;
}

STDMETHODIMP CVirtualDriveView::GetWindow ( HWND* phwnd )
{
    // Return our container window's handle to the browser.

    *phwnd = m_hWnd;
    return S_OK;
}

STDMETHODIMP CVirtualDriveView::GetItemObject(UINT uFlag, REFIID refiid, void** pOut)
{
	ATLTRACE("CVirtualDriveView::GetItemObject()\n");

	*pOut = NULL;

	//::MessageBox(NULL,_T("!!!"),_T("CVirtualDriveView"),MB_OK);
	/*if (uFlag == SVGIO_ALLVIEW)
	{
		LPITEMIDLIST* items = new LPITEMIDLIST[m_wndList.GetItemCount()];

		LV_ITEM        lvItem;
		for (int ii=0; ii<m_wndList.GetItemCount(); ii++)
		{
			ZeroMemory(&lvItem, sizeof(lvItem));

			lvItem.mask = LVIF_PARAM;
			lvItem.iItem = ii;

			if(m_wndList.GetItem(&lvItem))
			{
				items[ii] = (LPITEMIDLIST)lvItem.lParam;
			}
		}

		if(refiid == IID_IDataObject)
		{
			if(m_psfContainingFolder->GetUIObjectOf(m_hWnd, m_wndList.GetItemCount(), items, IID_IDataObject, NULL, (LPVOID*)&pOut) == S_OK)
			{
				return S_OK;
			}

			return E_OUTOFMEMORY;
		}

		delete[] items;
	}
	else if (uFlag == SVGIO_SELECTION)
	{
		if(refiid == IID_IDataObject)
		{
			LV_ITEM        lvItem;
			ZeroMemory(&lvItem, sizeof(lvItem));
			lvItem.mask = LVIF_PARAM;

			if (m_wndList.GetSelectedItem(&lvItem))
			{
				if(m_psfContainingFolder->GetUIObjectOf(m_hWnd, 1, (LPITEMIDLIST*)&lvItem.lParam, IID_IDataObject, NULL, (LPVOID*)&pOut) == S_OK)
				{
					return S_OK;
				}
				return E_OUTOFMEMORY;
			}
		}
	}*/

	return E_NOINTERFACE;
}

// CreateViewWindow() creates the container window.  Once that window is
// created, it will create the list control.
STDMETHODIMP CVirtualDriveView::CreateViewWindow ( LPSHELLVIEW pPrevView, 
                                                LPCFOLDERSETTINGS lpfs,
                                                LPSHELLBROWSER psb, 
                                                LPRECT prcView, HWND* phWnd )
{
    *phWnd = NULL;

    // Init member variables.

    m_spShellBrowser = psb;
    m_FolderSettings = *lpfs;

    // Get the parent window from Explorer.
    
    m_spShellBrowser->GetWindow( &m_hwndParent );

    // Create a container window, which will be the parent of the list control.
    
    if ( NULL == Create ( m_hwndParent, *prcView ) )
       return E_FAIL;

    // Return our window handle to the browser.

    *phWnd = m_hWnd;

	ShowWindow(SW_SHOW);

	//Shkil
	m_psfContainingFolder->CreateViewObject(m_hWnd, IID_IDropTarget, (LPVOID*) &m_pDropTarget) ;
	if(m_pDropTarget!=NULL)
	{
		::RegisterDragDrop( m_hWnd, m_pDropTarget );
	}

    return S_OK;
}


// DestroyViewWindow() is responsible for destroying our windows & cleaning
// up stuff.
STDMETHODIMP CVirtualDriveView::DestroyViewWindow()
{
    // Clean up the UI.
    UIActivate ( SVUIA_DEACTIVATE );

    // Destroy our windows & other resources.

    if ( NULL != m_hMenu )
       DestroyMenu ( m_hMenu );

	if (::IsWindow(m_hWnd))
	{
		DeleteAllItems();
		m_wndList.DestroyWindow();
		DestroyWindow();
	}

    return S_OK;
}


// GetCurrentInfo() returns our FODLERSETTINGS to the browser.
STDMETHODIMP CVirtualDriveView::GetCurrentInfo ( LPFOLDERSETTINGS lpfs )
{
    *lpfs = m_FolderSettings;
    return S_OK;
}

void CVirtualDriveView::DeleteAllItems()
{
	int count = m_wndList.GetItemCount();
	
	for (int ii=0; ii<count; ii++)
	{
		m_PidlMgr.Delete((LPITEMIDLIST)m_wndList.GetItemData(ii));
	}

	m_wndList.DeleteAllItems();
}

// Refresh() refreshes the shell view.
STDMETHODIMP CVirtualDriveView::Refresh()
{
    // Repopulate the list control.
	DeleteAllItems();
    //m_wndList.DeleteAllItems();
    FillList();

    return S_OK;
}


// UIActivate() is called whenever the focus switches among the Address bar,
// the tree, and our shell view.
STDMETHODIMP CVirtualDriveView::UIActivate ( UINT uState )
{
    // Nothing to do if the state hasn't changed since the last call.
    
    if ( m_uUIState == uState )
        return S_OK;
    
    // Modify the Explorer menu and status bar.

    HandleActivate ( uState );

    return S_OK;
}


// QueryStatus() is called by the browser to determine which standard commands
// our extension supports.
STDMETHODIMP CVirtualDriveView::QueryStatus ( const GUID* pguidCmdGroup, ULONG cCmds,
                                           OLECMD prgCmds[], OLECMDTEXT* pCmdText )
{
    if ( NULL == prgCmds )
        return E_POINTER;

    // The only useful standard command I've figured out is "refresh".  I've put
    // some trace messages in so you can see the other commands that the
    // browser sends our way.  If you can figure out what they're all for,
    // let me know!

    if ( NULL == pguidCmdGroup )
        {
        for ( UINT u = 0; u < cCmds; u++ )
            {
            ATLTRACE(">> Query - DEFAULT: %u\n", prgCmds[u]);

            switch ( prgCmds[u].cmdID )
                {
                case OLECMDID_REFRESH:
                    prgCmds[u].cmdf = OLECMDF_SUPPORTED | OLECMDF_ENABLED;
                break;
                }
            }

        return S_OK;
        }
    else if ( CGID_Explorer == *pguidCmdGroup )
        {
        for ( UINT u = 0; u < cCmds; u++ )
            {
            ATLTRACE(">> Query - EXPLORER: %u\n", prgCmds[u]);
            }
        }
    else if ( CGID_ShellDocView == *pguidCmdGroup )
        {
        for ( UINT u = 0; u < cCmds; u++ )
            {
            ATLTRACE(">> Query - DOCVIEW: %u\n", prgCmds[u]);
            }
        }

    return OLECMDERR_E_UNKNOWNGROUP;
}


// Exec() is called when the user executes a command in Explorer that we
// have to deal with.
STDMETHODIMP CVirtualDriveView::Exec ( const GUID* pguidCmdGroup, DWORD nCmdID,
                                    DWORD nCmdExecOpt, VARIANTARG* pvaIn,
                                    VARIANTARG* pvaOut )
{
HRESULT hrRet = OLECMDERR_E_UNKNOWNGROUP;

    // The only standard command we act on is "refresh".  I've put
    // some trace messages in so you can see the other commands that the
    // browser sends our way.  If you can figure out what they're all for,
    // let me know!
    if ( NULL == pguidCmdGroup )
        {
        ATLTRACE(">> Exec - DEFAULT: %u\n", nCmdID);

        if ( OLECMDID_REFRESH == nCmdID )
            {
            Refresh();
            hrRet = S_OK;
            }
        }
    else if ( CGID_Explorer == *pguidCmdGroup )
        {
        ATLTRACE(">> Exec - EXPLORER : %u\n", nCmdID);
        }
    else if ( CGID_ShellDocView == *pguidCmdGroup )
        {
        ATLTRACE(">> Exec - DOCVIEW: %u\n", nCmdID);
        }
	else 
	{
		ATLTRACE(">> Exec - unknown: %u\n", nCmdID);
	}

    return hrRet;
}

STDMETHODIMP CVirtualDriveView::TranslateAccelerator(LPMSG)
{
	return S_FALSE; 
}
/////////////////////////////////////////////////////////////////////////////
// Message handlers

LRESULT CVirtualDriveView::OnCreate ( UINT uMsg, WPARAM wParam, 
                                   LPARAM lParam, BOOL& bHandled )
{
HWND hwndList;
DWORD dwListStyles = WS_CHILD | WS_VISIBLE | WS_TABSTOP | LVS_SHOWSELALWAYS | LVS_SHAREIMAGELISTS | LVS_ALIGNTOP;
DWORD dwListExStyles = WS_EX_CLIENTEDGE | WS_EX_CONTROLPARENT;
DWORD dwListExtendedStyles = LVS_EX_FULLROWSELECT | LVS_EX_HEADERDRAGDROP;

    // Set the list view's display style (large/small/list/report) based on
    // the FOLDERSETTINGS we were given in CreateViewWindow().

	if( (FWF_ALIGNLEFT & m_FolderSettings.fFlags) != 0 ) 
		dwListStyles |= LVS_ALIGNLEFT;
    if( (FWF_AUTOARRANGE & m_FolderSettings.fFlags) != 0 ) 
		dwListStyles |= LVS_AUTOARRANGE;

	int state = m_PidlMgr.GetItemState(m_psfContainingFolder->GetCurPidl());

	dwListStyles |= m_psfContainingFolder->m_pFolderBehaviour->FolderViewMode(m_FolderSettings.ViewMode);

	m_splitter.Create(m_hWnd, rcDefault);

    // Create the list control.  Note that m_hWnd (inherited from CWindowImpl)
    // has already been set to the container window's handle.

    hwndList = CreateWindowEx ( dwListExStyles, WC_LISTVIEW, NULL, dwListStyles,
                                0, 0, 0, 0, m_splitter.m_hWnd, (HMENU) sm_uListID, 
                                _Module.GetModuleInstance(), (LPVOID)this );

    if ( NULL == hwndList )
    {
			return -1;
	}

    // Attach m_wndList to the list control and initialize styles, image
    // lists, etc.

    m_wndList.Attach ( hwndList );

    m_wndList.SetExtendedListViewStyle ( dwListExtendedStyles );

	m_psfContainingFolder->m_pFolderBehaviour->CreateColumns(m_wndList);

	ListView_SetImageList(m_wndList, _Module.m_imageLists.m_hImageListLarge, LVSIL_NORMAL);
    ListView_SetImageList(m_wndList, _Module.m_imageLists.m_hImageListSmall, LVSIL_SMALL);

	m_panel.Init(m_hWnd, state);
	m_panel.Create(m_splitter.m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE);

	m_splitter.m_cxyMin = 150;
    m_splitter.SetSplitterExtendedStyle ( SPLIT_BOTTOMALIGNED );

	m_splitter.SetSinglePaneMode(m_psfContainingFolder->m_pFolderBehaviour->GetPanelMode(m_psfContainingFolder->GetCurPidl()));
	m_splitter.SetSplitterPanes(m_wndList.m_hWnd, m_panel.m_hWnd, true);
	m_splitter.SetDefaultActivePane(m_wndList.m_hWnd);

	m_splitter.m_cxySplitBar = 1;
    m_splitter.SetSplitterPos ( 300 );

    FillList();

    return 0;
}

LRESULT CVirtualDriveView::OnSize ( UINT uMsg, WPARAM wParam, 
                                 LPARAM lParam, BOOL& bHandled )
{
    // Resize the list control to the same size as the container window.

    if ( m_splitter.IsWindow() )
       m_splitter.MoveWindow ( 0, 0, LOWORD(lParam), HIWORD(lParam), TRUE );

    return 0;
}

void CVirtualDriveView::NavigateVirtualFolder(int type, LPCTSTR name, int childType, LPCTSTR childName)
{
	ATLASSERT(name != NULL);

	LPITEMIDLIST pidl = m_PidlMgr.Create(NWS_FOLDER,type,name);
	if (childName != NULL)
	{
		LPITEMIDLIST pidlChild = m_PidlMgr.Create(NWS_FOLDER, childType, childName);

		LPITEMIDLIST pidlTemp = m_PidlMgr.Concatenate(pidl,pidlChild);
		m_PidlMgr.Delete(pidl);
		m_PidlMgr.Delete(pidlChild);
		pidl = pidlTemp;
	}

	LPITEMIDLIST targetPidl = m_PidlMgr.Concatenate(_Module.m_pidlNSFROOT, pidl);

	m_spShellBrowser->BrowseObject( (LPITEMIDLIST)targetPidl, SBSP_DEFBROWSER | SBSP_ABSOLUTE);

	m_PidlMgr.Delete(pidl);
	m_PidlMgr.Delete(targetPidl);
}

LRESULT CVirtualDriveView::OnNavigateVirtualFolder(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	switch (lParam)
	{
	case 0:
		{
			CString name;
			name.LoadString(IDS_BACKEDUP);

			NavigateVirtualFolder(ENTRY_BACKUP,name,0, NULL);
		}
		break;
	case 1:
		{
			CString name;
			name.LoadString(IDS_RECOVERYLOG);
			CString nameChild;
			nameChild.LoadString(IDS_RLOG_COMPLETED);

			NavigateVirtualFolder(ENTRY_RECOVERYLOG, name, ENTRY_RECOVERY_COMPLETED, nameChild);
		}
		break;
	case 2:
		{
			CString name;
			name.LoadString(IDS_RECOVERYLOG);
			CString nameChild;
			nameChild.LoadString(IDS_RLOG_ERRORS);

			NavigateVirtualFolder(ENTRY_RECOVERYLOG, name, ENTRY_RECOVERY_ERRORS, nameChild);
		}
		break;
	case 3:
		{
			CString name;
			name.LoadString(IDS_RECOVERYLOG);
			CString nameChild;
			nameChild.LoadString(IDS_RLOG_PENDING);

			NavigateVirtualFolder(ENTRY_RECOVERYLOG, name, ENTRY_RECOVERY_PENDING, nameChild);
		}
		break;
	case 4:
		{
			CString name;
			name.LoadString(IDS_PENDING);

			NavigateVirtualFolder(ENTRY_PENDING,name,0, NULL);
		}
		break;
	}

	return 0;
}

LRESULT CVirtualDriveView::OnSetFocus ( UINT uMsg, WPARAM wParam,
                                     LPARAM lParam, BOOL& bHandled )
{
    // This handler is called when the list container window gets the focus, 
    // usually because the user tabbed to it.  Immediately set the focus to
    // the list control.
   
    m_wndList.SetFocus();
    return 0;
}


LRESULT CVirtualDriveView::OnContextMenu(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}


LRESULT CVirtualDriveView::OnInitMenuPopup(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;
}


LRESULT CVirtualDriveView::OnMenuSelect(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
    return 0;//DefWindowProc();
}

/////////////////////////////////////////////////////////////////////////////
// Other stuff

// FillList() populates the list control.
void CVirtualDriveView::FillList()
{
CComPtr<CPidlEnum> pEnum;
LPITEMIDLIST pidl = NULL;
HRESULT hr;
    
    // Get an enumerator object for the folder's contents.  Since this simple
    // extension doesn't deal with subfolders, we request only non-folder
    // objects.

    hr = m_psfContainingFolder->EnumObjects ( m_hWnd, SHCONTF_NONFOLDERS | SHCONTF_FOLDERS,
                                              (LPENUMIDLIST*)&pEnum );
    
    if ( FAILED(hr) )
        return;

    // Stop redrawing to avoid flickering

    m_wndList.SetRedraw ( FALSE );
        
    // Add items.
	DWORD dwFetched;

	pEnum->Reset();

	//ICppProxyPtr proxy = NULL;

	//CPidlData data;
    while( (S_OK == pEnum->Next(1, &pidl, &dwFetched)) && (dwFetched!=0) )
		{
			LV_ITEM lvi = { 0 };

			//set the mask
			lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;

            //add the item to the end of the list
            lvi.iItem = m_wndList.GetItemCount();

			lvi.lParam = (LPARAM)m_PidlMgr.Copy(pidl);

			int state = m_PidlMgr.GetItemState(pidl);

            //get text on a callback basis
			TCHAR szName[MAX_PATH]=TEXT("");
			m_PidlMgr.GetName(pidl,szName);
			lvi.pszText = szName;

			if( m_PidlMgr.GetItemType(pidl) == NWS_FOLDER)
			{
				lvi.iImage = 0;
			}
			else
			{
				lvi.iImage = 1;
			}

			ListView_InsertItem(m_wndList, &lvi);

			m_psfContainingFolder->m_pFolderBehaviour->SetCustomData(m_wndList, lvi.iItem, pEnum->GetCustomFolderData());
		}

	m_wndList.SetRedraw ( TRUE );
    m_wndList.Invalidate();
    m_wndList.UpdateWindow();
}


// CompareItems() is called when sorting the list control.  It calls the view's
// parent folder to compare the PIDLs of the items being compared.
int CALLBACK CVirtualDriveView::CompareItems ( LPARAM l1, LPARAM l2, LPARAM lData )
{
	return 0;
}


void CVirtualDriveView::HandleActivate ( UINT uState )
{
}


void CVirtualDriveView::HandleDeactivate()
{
}

LRESULT CVirtualDriveView::OnItemActivated(UINT CtlID, LPNMHDR lpnmh, BOOL &bHandled)
{
	LV_ITEM        lvItem;
    ZeroMemory(&lvItem, sizeof(lvItem));

    lvItem.mask = LVIF_PARAM;

    LPNMLISTVIEW   lpnmlv = (LPNMLISTVIEW)lpnmh;
    lvItem.iItem = lpnmlv->iItem;

    if(m_wndList.GetItem(&lvItem))
    {
        //folders to be activated
        if(NWS_FOLDER == m_PidlMgr.GetItemType((LPITEMIDLIST)lvItem.lParam))
        {
			//Tells Windows Explorer to browse to another folder
            m_spShellBrowser->BrowseObject( (LPITEMIDLIST)lvItem.lParam,
                                         SBSP_DEFBROWSER | SBSP_RELATIVE);
        }
    }
    return 0;
}

STDMETHODIMP CVirtualDriveView::GetView(SHELLVIEWID *pvid, ULONG uView)
{
	if (!pvid)
        return E_POINTER;

    switch (uView)
    {
    case 0:
        return SVIDFromViewMode(FVM_ICON, pvid);
    case 1:
        return SVIDFromViewMode(FVM_SMALLICON, pvid);
    case 2:
        return SVIDFromViewMode(FVM_LIST, pvid);
    case 3:
        return SVIDFromViewMode(FVM_DETAILS, pvid);
	//case 4:
	//	return SVIDFromViewMode(FVM_TILE, pvid);
	//case 5:
	//	return SVIDFromViewMode(FVM_CONTENT, pvid);
	//case 6:
	//	return SVIDFromViewMode(FVM_THUMBNAIL, pvid);
	//case 7:
	//	return SVIDFromViewMode(FVM_THUMBSTRIP, pvid);
    case SV2GV_CURRENTVIEW:
        return SVIDFromViewMode(FOLDERVIEWMODE(m_FolderSettings.ViewMode), pvid);
    case SV2GV_DEFAULTVIEW:
        return SVIDFromViewMode(FVM_ICON, pvid);
    }

    return E_INVALIDARG;
}

STDMETHODIMP CVirtualDriveView::CreateViewWindow2(LPSV2CVW2_PARAMS lpParams)
{
	ViewModeFromSVID(lpParams->pvid, (FOLDERVIEWMODE*)&lpParams->pfs->ViewMode);
	return CreateViewWindow(lpParams->psvPrev,lpParams->pfs,lpParams->psbOwner,lpParams->prcView,&lpParams->hwndView);
}

STDMETHODIMP CVirtualDriveView::HandleRename(PCUITEMID_CHILD pidlNew)
{
	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveView::SelectAndPositionItem(PCUITEMID_CHILD pidlItem, UINT uFlags, POINT *point)
{
	return E_NOTIMPL;
}

LRESULT CVirtualDriveView::OnRButtonClick(UINT CtlID, LPNMHDR lpnmh, BOOL &bHandled)
{
	LPNMITEMACTIVATE lpnmitem = (LPNMITEMACTIVATE) lpnmh;
	POINT point = {0,0};
	::GetCursorPos(&point);

	LV_ITEM        lvItem;
    ZeroMemory(&lvItem, sizeof(lvItem));

    lvItem.mask = LVIF_PARAM;

    LPNMLISTVIEW   lpnmlv = (LPNMLISTVIEW)lpnmh;
    lvItem.iItem = lpnmlv->iItem;

	int menu_res = 0;
	long versions = 0;
	long state = STATE_NOTHING;
	long internalState = 0;
	long backedup = 0;
	bool fileExist = true;
	bool simple = false;

	TCHAR  tmpPath[MAX_PATH]=_TEXT("");
	TCHAR  rootPath[MAX_PATH]=_TEXT("");
	DWORD  dwLen=MAX_PATH;

	ICppProxyPtr proxy = NULL;

	if(m_wndList.GetItem(&lvItem))
	{
		proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

		LPITEMIDLIST pidl = m_PidlMgr.Concatenate(m_psfContainingFolder->GetCurPidl(),(LPITEMIDLIST)lvItem.lParam);
		m_PidlMgr.GetInternalPath(pidl,tmpPath,&dwLen);
		dwLen = MAX_PATH;
		m_PidlMgr.GetFullName(pidl,rootPath,&dwLen);
		m_PidlMgr.Delete(pidl);

		CString fullPath(tmpPath);

		int cmd = m_psfContainingFolder->m_pFolderBehaviour->TrackContextMenu(m_hWnd,(LPITEMIDLIST)lvItem.lParam,fullPath, proxy, m_wndList, lvItem.iItem);

		CWaitCursor cursor;

		//CString fullPath(tmpPath);

		switch (cmd)
		{
		case ID_MENU_OPEN:
			m_spShellBrowser->BrowseObject( (LPITEMIDLIST)lvItem.lParam,
                                         SBSP_DEFBROWSER | SBSP_RELATIVE);
			break;
		case ID_MENU_EXPLORER:
			{
				CString	newPath;
				newPath.Format(_T("/e, \"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\%s\""),rootPath);

				HINSTANCE hInstance = ShellExecute(NULL,_T("open"),_T("explorer"),newPath,NULL,SW_SHOWNORMAL);
				/*m_spShellBrowser->BrowseObject( (LPITEMIDLIST)lvItem.lParam,
                                         SBSP_NEWBROWSER | SBSP_DEFBROWSER | SBSP_RELATIVE);*/
			}
			break;
		case ID_MENU_RESTORE:
			if (proxy)
				proxy->NotifyCommand(COMMAND_RESTORE,(long)m_hWnd,_bstr_t(fullPath.AllocSysString()));
			break;
		case ID_MENU_RESTORETO:
			{
				if (proxy)
					proxy->NotifyCommand(COMMAND_RESTORETO,(long)m_hWnd,_bstr_t(fullPath.AllocSysString()));
			}
			break;
		case ID_MENU_RESTORETOPREVIOUSVERSION:
			if (proxy)
				proxy->NotifyCommand(COMMAND_RESTOREPREVVERSION,(long)m_hWnd,_bstr_t(fullPath.AllocSysString()));
			break;
		case ID_MENU_DONOTBACK:
			if (proxy)
				proxy->NotifyCommand(COMMAND_DONOTBACKTHISUP,(long)m_hWnd,_bstr_t(fullPath.AllocSysString()));
			Refresh();
			break;
		case ID_MENU_REMOVEFROMBACKUP:
			if (proxy)
				proxy->NotifyCommand(COMMAND_REMOVEFROMBACKUP,(long)m_hWnd,_bstr_t(fullPath.AllocSysString()));
			Refresh();
			break;
		case ID_MENU_CANCELRESTORE:
			if (proxy)
				m_psfContainingFolder->m_pFolderBehaviour->Command(m_hWnd,(LPITEMIDLIST)lvItem.lParam, fullPath, ID_MENU_CANCELRESTORE, proxy, m_wndList, lvItem.iItem);
			Refresh();
			break;
		case ID_MENU_RESTARTRESTOREJOB:
			if (proxy)
				m_psfContainingFolder->m_pFolderBehaviour->Command(m_hWnd,(LPITEMIDLIST)lvItem.lParam, fullPath, ID_MENU_RESTARTRESTOREJOB, proxy, m_wndList, lvItem.iItem);
			/*if (proxy)
			{
				CString source;
				CString destination;

				m_wndList.GetItemText(lvItem.iItem,1,source);
				m_wndList.GetItemText(lvItem.iItem,2,destination);
				proxy->RestartRestoreJob(_bstr_t(source.AllocSysString()),_bstr_t(destination.AllocSysString()));
			}*/
			Refresh();
			break;
		case ID_MENU_HIGHPRIORITY:
		case ID_MENU_LOWPRIORITY:
		case ID_MENU_NORMALPRIORITY:
			m_psfContainingFolder->m_pFolderBehaviour->Command(m_hWnd,(LPITEMIDLIST)lvItem.lParam, fullPath, cmd, proxy, m_wndList, lvItem.iItem);
			Refresh();
			break;
		case ID_MENU_COPYSHAREDLINK:	
			try
			{
				if( proxy )
				{
					_bstr_t sLink;
					proxy->CopySharedLink( fullPath.AllocSysString(), sLink.GetAddress() );				
					LPSTR  lptstrCopy; 

					if( !OpenClipboard() )
						return 0;
					EmptyClipboard(); 
				
					HGLOBAL hglbCopy; 
					hglbCopy = GlobalAlloc(GMEM_MOVEABLE, 
						(sLink.length()+ 1) * sizeof(TCHAR));
					if (hglbCopy == NULL) 
					{ 
						CloseClipboard(); 
						return 0; 
					}
					lptstrCopy = (LPSTR)GlobalLock(hglbCopy); 
					memcpy(lptstrCopy, (char*)sLink, 
							sLink.length()); 
					lptstrCopy[sLink.length()] = '\0';    // null character 
					GlobalUnlock(hglbCopy); 
				
					SetClipboardData(CF_TEXT, hglbCopy); 

					CloseClipboard();
				}
			}
			catch(...)
			{
				//handle situation when server does not have CopySharedLink method
			}
			break;
		}

		bHandled = TRUE;
	}

	return 0;
}
