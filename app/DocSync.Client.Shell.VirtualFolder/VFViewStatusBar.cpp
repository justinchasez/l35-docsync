#include "StdAfx.h"
#include "VFViewStatusBar.h"
#include "VFResources.h"

CVFViewStatusBar::CVFViewStatusBar(void)
{
	//try
	//{
	//	m_proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
	//	if (m_proxy)
	//	{
	//		HRESULT hr = this->DispEventAdvise(m_proxy);
	//		hr = S_OK;
	//	}
	//}
	//catch(...)	{/*do nothing*/}
}


CVFViewStatusBar::~CVFViewStatusBar(void)
{
	//::PostMessage(_Module.m_mainWindow,REMOVE_NOTIFY_WINDOW,(WPARAM)m_hWnd,1);
	//try
	//{
	//	if (m_proxy)
	//	{
	//		this->DispEventUnadvise(m_proxy);
	//	}
	//}
	//catch(...)	{/*do nothing*/}
}

LRESULT CVFViewStatusBar::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	_Module.EventHandlerManagerInstance()->UnadviceWindow(m_hWnd);

	return 0;
}

LRESULT CVFViewStatusBar::OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (IsWindow())
	{
		try
		{
			ICppProxyPtr proxy = NULL;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

			if (proxy)
			{
				CString str;

				long value = 0;
				proxy->GetStatusInfo(STATUS_PENDING_BACKEDUP, &value);

				str.Format(_T("- Files Pending Backup (%d files)"),value);
				m_filesPendingBackup.SetLabel(str);

				value = 0;
				proxy->GetStatusInfo(STATUS_QUEUED_FOR_RESTORE, &value);
				str.Format(_T("- Files Pending Restore (%d files)"),value);
				m_filesPendingRestore.SetLabel(str);

				value = 0;
				proxy->GetStatusInfo(STATUS_SUCCESSFULLY_RESTORED, &value);
				str.Format(_T("- Files Successfully Restored (%d files)"),value);
				m_filesSuccessfullyRestored.SetLabel(str);

				value = 0;
				proxy->GetStatusInfo(STATUS_COULD_NOT_BE_RESTORED, &value);
				str.Format(_T("- View Restore Errors (%d files)"),value);
				m_viewRestoreErrors.SetLabel(str);

				Invalidate();

				GetParent().Invalidate();
			}
		}
		catch (...) { /*do nothing*/ }
	}

	return 0;
}

LRESULT CVFViewStatusBar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {20,20,280,40};

	RECT rcName = {20,2,280,40};
	m_name.Create(m_hWnd,rcName,NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().StaticFont());
	m_name.SetWindowText(_T("View Status"));

	CString str;

	m_filesPendingBackup.Create(m_hWnd,rect, NULL, WS_CHILD | WS_VISIBLE);
	m_filesPendingBackup.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- Files Pending Backup (%d files)"),0);
	m_filesPendingBackup.SetLabel(str);
	m_filesPendingBackup.SetHyperLink(_T("Files Pending Backup"));
	m_filesPendingBackup.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_filesPendingBackup.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_filesPendingBackup.m_clrLink = CVFResources::instance().LinkColor();
	m_filesPendingBackup.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	rect.bottom += 20;
	rect.top += 20;
	m_filesPendingRestore.Create(m_hWnd,rect);
	m_filesPendingRestore.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- Files Pending Restore (%d files)"),0);
	m_filesPendingRestore.SetLabel(str);
	m_filesPendingRestore.SetHyperLink(_T("Files Pending Restore"));
	m_filesPendingRestore.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_filesPendingRestore.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_filesPendingRestore.m_clrLink = CVFResources::instance().LinkColor();
	m_filesPendingRestore.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	rect.bottom += 20;
	rect.top += 20;
	m_filesSuccessfullyRestored.Create(m_hWnd,rect);
	m_filesSuccessfullyRestored.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- Files Successfully Restored (%d files)"),0);
	m_filesSuccessfullyRestored.SetLabel(str);
	m_filesSuccessfullyRestored.SetHyperLink(_T("Files Successfully Restored"));
	m_filesSuccessfullyRestored.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_filesSuccessfullyRestored.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_filesSuccessfullyRestored.m_clrLink = CVFResources::instance().LinkColor();
	m_filesSuccessfullyRestored.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	rect.bottom += 20;
	rect.top += 20;
	m_viewRestoreErrors.Create(m_hWnd,rect);
	m_viewRestoreErrors.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- View Restore Errors (%d files)"),0);
	m_viewRestoreErrors.SetLabel(str);
	m_viewRestoreErrors.SetHyperLink(_T("View Restore Errors"));
	m_viewRestoreErrors.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_viewRestoreErrors.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_viewRestoreErrors.m_clrLink = CVFResources::instance().LinkColor();
	m_viewRestoreErrors.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	_Module.EventHandlerManagerInstance()->AdviceWindow(m_hWnd);
	_Module.EventHandlerManagerInstance()->NotifyUpdate();

	return 0;
}

LRESULT CVFViewStatusBar::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NMHDR* nmhdr = (NMHDR*)lParam;

	if (m_filesSuccessfullyRestored.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,1);
		}
	}
	else if (m_viewRestoreErrors.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,2);
		}
	}
	else if (m_filesPendingRestore.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,3);
		}
	}
	else if (m_filesPendingBackup.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,4);
		}
	}

	return 0;
}

LRESULT CVFViewStatusBar::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().StaticColor());
	SetBkMode((HDC)wParam,TRANSPARENT);
	return (LRESULT)GetStockObject(NULL_BRUSH);
}

void CVFViewStatusBar::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);*/

	//::FillRect(hDC,&rect,GetSysColorBrush(COLOR_WINDOW));

	//CVFResources::instance().ViewStatusIcon().DrawIconEx(hDC,2,2,16,16,0);
}

LRESULT CVFViewStatusBar::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{

	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CVFViewStatusBar::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}