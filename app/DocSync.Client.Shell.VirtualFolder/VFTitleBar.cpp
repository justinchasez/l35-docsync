#include "StdAfx.h"
#include "VFTitleBar.h"
#include "VFResources.h"

CVFTitleBar::CVFTitleBar(void)
{
}


CVFTitleBar::~CVFTitleBar(void)
{
}

LRESULT CVFTitleBar::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	/*RECT rcName = {0,0,100,20};
	m_name.Create(m_hWnd,rcName, NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().TitleFont());
	m_name.SetWindowText(_T("Backup Drive"));*/

	return 0;
}

LRESULT CVFTitleBar::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().TitleColor());
	SetBkColor((HDC)wParam,GetSysColor(COLOR_HIGHLIGHT));
	return (LRESULT)GetSysColorBrush(COLOR_HIGHLIGHT);
}

void CVFTitleBar::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);

	::FillRect(hDC,&rect,(HBRUSH)GetSysColorBrush(COLOR_HIGHLIGHT));*/
}

LRESULT CVFTitleBar::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	/*if (m_name.IsWindow())
	{
		RECT rect;
		GetClientRect(&rect);
		m_name.MoveWindow(rect.right - 100, 0, 100, 20);
	}*/
	return 0;
}

LRESULT CVFTitleBar::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect;
	GetClientRect(&rect);

	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CVFTitleBar::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}
