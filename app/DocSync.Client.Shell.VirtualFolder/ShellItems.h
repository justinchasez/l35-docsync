//========================================================================================
//
// Module:			nseDOpusFavorites
// Author:          Pascal Hurni
// Creation Date:	11.07.2004
//
// Copyright 2004 Mortimer Systems
// This software is free. I grant you a non-exclusive license to use it.
//
//========================================================================================

#ifndef __SHELLITEMS_H_
#define __SHELLITEMS_H_


//========================================================================================

// Used by SetReturnString.
// Can also be used by anyone when the Shell Allocator is needed. Use the gloabl g_Malloc object which is a CMalloc.
struct CMalloc
{
	CComPtr<IMalloc> m_MallocPtr;
	CMalloc();
};

// Set the return string 'Source' in the STRRET struct.
// Note that it always allocate a UNICODE copy of the string.
// Returns false if memory allocation fails.
bool SetReturnStringA(LPCSTR Source, STRRET &str);
bool SetReturnStringW(LPCWSTR Source, STRRET &str);

#ifdef _UNICODE
	#define SetReturnString SetReturnStringW
#else
	#define SetReturnString SetReturnStringA
#endif

//========================================================================================
// Implements IExtracIcon.
//
// This interface is used to get the icon related to the Favorites Items.
// You can configure which icons are used for 'Normal' and 'Opened' states. See registry entries in *.rgs

class ATL_NO_VTABLE CExtractIcon :
	public CComObjectRootEx<CComSingleThreadModel>,
	public IExtractIcon
{
public:
	BEGIN_COM_MAP(CExtractIcon)
		COM_INTERFACE_ENTRY_IID(IID_IExtractIcon, IExtractIcon)
	END_COM_MAP()

	//-------------------------------------------------------------------------------

	// Ensure the owner object is not freed before this one
	void Init(IUnknown *pUnkOwner);

	// Set the icon location
	// This member must be called before any IExtractIcon member.
	void SetIconLocation(CIconLocation &IconLocation);

	//-------------------------------------------------------------------------------
	// IExtractIcon methods

	STDMETHOD(GetIconLocation) (UINT uFlags, LPTSTR szIconFile, UINT cchMax, int *piIndex, UINT *pwFlags);
	STDMETHOD(Extract) (LPCTSTR pszFile, UINT nIconIndex, HICON *phiconLarge, HICON *phiconSmall, UINT nIconSize);

protected:
	CComPtr<IUnknown> m_UnkOwnerPtr;
	CIconLocation m_IconLocation;
};

//========================================================================================
// Light implementation of IDataObject.
//
// This object is used when you double-click on an item in the FileDialog.
// It's purpose is simply to encapsulate the complete pidl for the item (remember it's a Favorite item)
// into the IDataObject, so that the FileDialog can pass it further to our IShellFolder::BindToObject().
// Because I'm only interested in the FileDialog behaviour, every methods returns E_NOTIMPL except GetData().

class ATL_NO_VTABLE CDataObject :
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDataObject, public IEnumFORMATETC
{
public:
	BEGIN_COM_MAP(CDataObject)
		COM_INTERFACE_ENTRY_IID(IID_IDataObject, IDataObject)
		COM_INTERFACE_ENTRY_IID(IID_IEnumFORMATETC, IEnumFORMATETC)
	END_COM_MAP()

	//-------------------------------------------------------------------------------

	CDataObject();
	~CDataObject();

	// Ensure the owner object is not freed before this one
	void Init(IUnknown *pUnkOwner);

	// Populate the object with the Favorite Item pidl.
	// This member must be called before any IDataObject member.
	void SetPidl(LPCITEMIDLIST pidlParent, LPCITEMIDLIST pidl);

	//-------------------------------------------------------------------------------
	// IDataObject methods

	STDMETHOD(GetData) (LPFORMATETC pFE, LPSTGMEDIUM pStgMedium);
	STDMETHOD(GetDataHere) (LPFORMATETC, LPSTGMEDIUM);
	STDMETHOD(QueryGetData) (LPFORMATETC);
	STDMETHOD(GetCanonicalFormatEtc) (LPFORMATETC, LPFORMATETC);
	STDMETHOD(SetData) (LPFORMATETC, LPSTGMEDIUM, BOOL);
	STDMETHOD(EnumFormatEtc) (DWORD, IEnumFORMATETC**);
	STDMETHOD(DAdvise) (LPFORMATETC, DWORD, IAdviseSink*, LPDWORD);
	STDMETHOD(DUnadvise) (DWORD dwConnection);
	STDMETHOD(EnumDAdvise) (IEnumSTATDATA** ppEnumAdvise);

	//-------------------------------------------------------------------------------
	// IEnumFORMATETC members

	STDMETHOD(Next) (ULONG, LPFORMATETC, ULONG*);
	STDMETHOD(Skip) (ULONG);
	STDMETHOD(Reset) ();
	STDMETHOD(Clone) (LPENUMFORMATETC*);

protected:
	CComPtr<IUnknown> m_UnkOwnerPtr;
	CPidlMgr m_PidlMgr;

	UINT m_cfShellIDList;

	LPITEMIDLIST m_pidl;
	LPITEMIDLIST m_pidlParent;
};


#endif // __SHELLITEMS_H_
