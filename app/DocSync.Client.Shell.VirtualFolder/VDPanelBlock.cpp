#include "StdAfx.h"
#include "VDPanelBlock.h"


VDPanelBlock::VDPanelBlock(void)
{
}


VDPanelBlock::~VDPanelBlock(void)
{
}

LRESULT VDPanelBlock::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	this->DefWindowProc(uMsg,wParam,lParam);
	{
		CDC dc( GetDC());
		RECT rect;
		GetClientRect(&rect);
		dc.FillRect(&rect,COLOR_HIGHLIGHT);
		dc.DeleteDC();
	}
	return 0;
}

LRESULT VDPanelBlock::OnEraseBackground(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}
