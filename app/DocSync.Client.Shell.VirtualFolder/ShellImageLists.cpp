#include "StdAfx.h"
#include "ShellImageLists.h"
#include "dllmain.h"

CShellImageLists::CShellImageLists(void)  : m_hImageListSmall(NULL), m_hImageListLarge(NULL)
{
}


CShellImageLists::~CShellImageLists(void)
{
      if( m_hImageListSmall ) 
		  ImageList_Destroy(m_hImageListSmall);
      if( m_hImageListLarge ) 
		  ImageList_Destroy(m_hImageListLarge);

}

BOOL CShellImageLists::CreateImageLists()
{
   // Set the small image list
   int nSmallCx = ::GetSystemMetrics(SM_CXSMICON);
   int nSmallCy = ::GetSystemMetrics(SM_CYSMICON);

   HIMAGELIST imgSmall = ImageList_Create(nSmallCx, nSmallCy, ILC_COLOR32 | ILC_MASK, 4, 0);
   if( imgSmall==NULL ) return FALSE;

   // Set the large image list
   int nLargeCx = ::GetSystemMetrics(SM_CXICON);
   int nLargeCy = ::GetSystemMetrics(SM_CYICON);
   HIMAGELIST imgLarge = ImageList_Create(nLargeCx, nLargeCy, ILC_COLOR32 | ILC_MASK, 4, 0);
   if( imgLarge==NULL ) return FALSE;

   HICON hIcon;

   HMODULE hModule = _Module.GetResourceInstance(); //::GetModuleHandle(_T("shell32.dll"));

   // Load NSF Root icon
   hIcon = (HICON)::LoadImage(hModule, 
                              MAKEINTRESOURCE(IDI_ICON_FOLDER),
                              IMAGE_ICON, 
                              nSmallCx, nSmallCy, 
                              LR_DEFAULTCOLOR);

   ImageList_AddIcon(imgSmall, hIcon);

   hIcon = (HICON)::LoadImage(hModule, 
                              MAKEINTRESOURCE(IDI_ICON_FILE),
                              IMAGE_ICON, 
                              nSmallCx, nSmallCy, 
                              LR_DEFAULTCOLOR);
   ImageList_AddIcon(imgSmall, hIcon);

   hIcon = (HICON)::LoadImage(hModule, 
                              MAKEINTRESOURCE(IDI_ICON_FOLDER),
                              IMAGE_ICON, 
                              nLargeCx, nLargeCy, 
                              LR_DEFAULTCOLOR);
   ImageList_AddIcon(imgLarge, hIcon);

   hIcon = (HICON)::LoadImage(hModule, 
                              MAKEINTRESOURCE(IDI_ICON_FILE),
                              IMAGE_ICON, 
                              nLargeCx, nLargeCy, 
                              LR_DEFAULTCOLOR);
   ImageList_AddIcon(imgLarge, hIcon);

   //save the created imagelist
   m_hImageListSmall = imgSmall;
   m_hImageListLarge = imgLarge;

   return TRUE;
}