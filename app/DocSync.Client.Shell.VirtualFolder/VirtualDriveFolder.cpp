// VirtualDriveFolder.cpp : Implementation of CVirtualDriveFolder

#include "stdafx.h"
#include "VirtualDriveFolder.h"

#include "VirtualDriveView.h"
//#include "CEnumFolderList.h"
//#include "PidlChildrenManager.h"
#include "resource.h"
#include "ExtractIcon.h"
#include "VDFContextMenu.h"
#include "dllmain.h"
#include "VirtualDriveLink.h"
#include "PidlEnum.h"
#include "VDRootFolderBehaviour.h"
#include "VDBackupFolderBehaviour.h"
#include "VDPendingFolderBehaviour.h"
#include "VDRecoveryFolderBehaviour.h"

#include "VFDropTarget.h"

#include "..\OnlineBackup.Client.Shell.ContextMenu\config.h"

#define GET_SHGDN_FOR(dwFlags)         ((DWORD)dwFlags & (DWORD)0x0000FF00)
#define GET_SHGDN_RELATION(dwFlags)    ((DWORD)dwFlags & (DWORD)0x000000FF)

// CVirtualDriveFolder
CVirtualDriveFolder::CVirtualDriveFolder() : m_pidlRoot(NULL), m_pFolderBehaviour(NULL)
{
}

CVirtualDriveFolder::~CVirtualDriveFolder()
{
}

STDMETHODIMP CVirtualDriveFolder::GetClassID(CLSID* pClsid)
{
	if ( NULL == pClsid )
		return E_POINTER;

	// Return our GUID to the shell.
	*pClsid = CLSID_VirtualDriveFolder;

	return S_OK;
}

// Initialize() is passed the PIDL of the folder where our extension is.
STDMETHODIMP CVirtualDriveFolder::Initialize(LPCITEMIDLIST pidl)
{
	//ATLTRACE(_T("CVirtualDriveFolder(0x%08x)::Initialize() pidl=[%s]\n"), this, PidlToString(pidl));
	if (m_pFolderBehaviour == NULL)
	{
		TCHAR path[MAX_PATH];
		DWORD len = MAX_PATH;
		m_PidlMgr.GetFullName(pidl,path,&len);

		int state = (m_pidlRoot == NULL) ? ENTRY_ROOT : m_PidlMgr.GetItemState(m_PidlMgr.GetLastItem(m_pidlRoot));

		m_pFolderBehaviour = VDFolderBehaviourBase::CreateBehaviour(state);
	}

	if(pidl && (!_Module.m_pidlNSFROOT))
	{
		_Module.m_pidlNSFROOT = m_PidlMgr.Copy(pidl);

		GetGlobalSettings();
	}

	if (_tcslen(_Module.m_helpUrl) == 0)
	{
		try
		{
			ICppProxyPtr proxy;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
					//ICppProxy* proxy = ProxyServerContainer::Instance();
			if (proxy)
			{
				_bstr_t option;
				proxy->CheckSettings(0,option.GetAddress());
				if (option.operator LPCWSTR() != NULL)
				{
					_tcscpy(_Module.m_helpUrl,option);
				}
			}
		}
		catch (...)
		{
		}
	}

	return S_OK;
}

STDMETHODIMP CVirtualDriveFolder::GetCurFolder(LPITEMIDLIST *ppidl)
{
	ATLTRACE(_T("CVirtualDriveFolder(0x%08x)::GetCurFolder()\n"), this);

	if (ppidl == NULL)
		return E_POINTER;

	*ppidl = m_PidlMgr.Concatenate(_Module.m_pidlNSFROOT,m_pidlRoot);

	return S_OK;
}

//-------------------------------------------------------------------------------
// IShellFolder

// BindToObject() is called when a folder in our part of the namespace is being browsed.
STDMETHODIMP CVirtualDriveFolder::BindToObject(LPCITEMIDLIST pidl, LPBC pbcReserved, REFIID riid, void** ppvOut)
{
	HRESULT hr;
	CComObject<CVirtualDriveFolder>* pShellFolder;

	*ppvOut = NULL;
    // Create a new CShellFolderImpl COM object.
	if( riid != IID_IShellFolder) 
		return E_NOINTERFACE;

    hr = CComObject<CVirtualDriveFolder>::CreateInstance ( &pShellFolder );

    if ( FAILED(hr) )
        return hr;

    // AddRef() the object while we're using it.

    pShellFolder->AddRef();

    // Object initialization - pass the object its parent folder (this)
    // and the pidl it will be browsing to.
	LPITEMIDLIST pidlNew = m_PidlMgr.Concatenate(m_pidlRoot,pidl);
	pShellFolder->m_pidlRoot = m_PidlMgr.Copy(pidlNew);

	int state = m_PidlMgr.GetItemState(pidl);
	pShellFolder->m_pFolderBehaviour = VDFolderBehaviourBase::CreateBehaviour(state);

	//m_pFolderBehaviour->CreateChildBehaviour(pidl);

	m_PidlMgr.Delete(pidlNew);

    // Return the requested interface back to the browser.

    hr = pShellFolder->QueryInterface ( riid, ppvOut );

    pShellFolder->Release();
    return hr;
}

// CompareIDs() is responsible for returning the sort order of two PIDLs.
// lParam can be the 0-based Index of the details column
STDMETHODIMP CVirtualDriveFolder::CompareIDs(LPARAM lParam, LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2)
{
	TCHAR szName1[MAX_PATH]=_T("");
	TCHAR szName2[MAX_PATH]=_T("");
	DWORD dwLen1=MAX_PATH;
	DWORD dwLen2=MAX_PATH;

	m_PidlMgr.GetFullName(pidl1,szName1,&dwLen1);
	m_PidlMgr.GetFullName(pidl2,szName2,&dwLen2);

	ITEM_TYPE it1,it2;

	it1=m_PidlMgr.GetItemType(pidl1);
	it2=m_PidlMgr.GetItemType(pidl2);

	if( it1 == it2 )
		return _tcscmp(szName1, szName2);
	else
	{
		if( NWS_FOLDER == it1)//pidl1 correspond a folder object
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}

// CreateViewObject() creates a new COM object that implements IShellView.
STDMETHODIMP CVirtualDriveFolder::CreateViewObject(HWND hwndOwner, REFIID riid, void** ppvOut)
{
	ATLTRACE(_T("CVirtualDriveFolder::CreateViewObject\n"));

	HRESULT hr;
CComObject<CVirtualDriveView>* pShellView;

    if ( NULL == ppvOut )
        return E_POINTER;

	*ppvOut = NULL;

    // Create a new CShellViewImpl COM object.
	if (IID_IShellView == riid || IID_IShellView2 == riid)
	{
		ATLTRACE(_T("CVirtualDriveFolder::CreateViewObject IID_IShellView\n"));

		hr = CComObject<CVirtualDriveView>::CreateInstance ( &pShellView );

		if ( FAILED(hr) )
			return hr;

		// AddRef() the object while we're using it.

		pShellView->AddRef();
    
		// Object initialization - pass the object its containing folder (this).

		hr = pShellView->_init ( this );

		if ( FAILED(hr) )
			{
			pShellView->Release();
			return hr;
			}

		// Return the requested interface back to the shell.

		hr = pShellView->QueryInterface ( riid, ppvOut );

		pShellView->Release();
		return hr;
	}

	if (IID_IQueryInfo == riid)
	{
		ATLTRACE(_T("CVirtualDriveFolder::CreateViewObject IID_IQueryInfo\n"));
	}

	if (IID_IShellDetails == riid)
	{
		ATLTRACE(_T("CVirtualDriveFolder::CreateViewObject IID_IShellDetails\n"));
	}
	
	if (IID_IDropTarget == riid)
	{		
		ATLTRACE(_T("CVirtualDriveFolder::CreateViewObject IID_IDropTarget\n"));

		CComObject<CVFDropTarget> *pDropTarget;
		HRESULT Hr = CComObject<CVFDropTarget>::CreateInstance(&pDropTarget);
		if(FAILED(Hr))
			return Hr;

		pDropTarget->AddRef();
		
		HR( pDropTarget->_Init(this, NULL) );
		
		Hr=pDropTarget->QueryInterface(IID_IDropTarget, ppvOut);
		pDropTarget->Release();
		
		return Hr;
	}

	return E_NOINTERFACE;
}

// EnumObjects() creates a COM object that implements IEnumIDList.
STDMETHODIMP CVirtualDriveFolder::EnumObjects(HWND hwndOwner, DWORD dwFlags, LPENUMIDLIST* ppEnumIDList)
{
	//return E_NOTIMPL;
	ATLTRACE(_T("CVirtualDriveFolder::EnumObjects\n"));

	if ( NULL == ppEnumIDList )
        return E_POINTER;

    *ppEnumIDList = NULL;

    // Enumerate all drives on the system and put the letters of the drives
    // into a vector.
    // Create an enumerator with CComEnumOnSTL<> and our copy policy class.

	CComObject<CPidlEnum>* pEnum;

    HRESULT hr = CComObject<CPidlEnum>::CreateInstance ( &pEnum );

    if ( FAILED(hr) )
        return hr;

    // AddRef() the object while we're using it.

    pEnum->AddRef();

    // Init the enumerator.  Init() will AddRef() our IUnknown (obtained with
    // GetUnknown()) so this object will stay alive as long as the enumerator 
    // needs access to the vector m_vecDriveLtrs.
	//TCHAR szTmp[MAX_PATH]=_TEXT("");
	//DWORD dwLen=MAX_PATH;
	//m_PidlMgr.GetFullName(m_pidlRoot,szTmp,&dwLen);

	hr = pEnum->_Init(*m_pFolderBehaviour, m_pidlRoot, dwFlags);

    // Return an IEnumIDList interface to the caller.

    if ( SUCCEEDED(hr) )
        hr = pEnum->QueryInterface ( IID_IEnumIDList, (void**) ppEnumIDList );

    pEnum->Release();

    return hr;
}


// GetAttributesOf() returns the attributes for the items whose PIDLs are passed in.
STDMETHODIMP CVirtualDriveFolder::GetAttributesOf(UINT uCount, LPCITEMIDLIST aPidls[], LPDWORD pdwAttribs)
{
	ATLTRACE(_T("CVirtualDriveFolder::GetAttributesOf\n"));

	*pdwAttribs = 0;//(DWORD)-1;

	if(( uCount==0 )||(aPidls[0]->mkid.cb == 0)) 
	{
		// Can happen on Win95. Queries the view folder.
		//DWORD dwAttr;
		//dwAttr |= SFGAO_FOLDER | SFGAO_HASSUBFOLDER;
		*pdwAttribs |= SFGAO_FOLDER | SFGAO_HASSUBFOLDER;
	}
	else
	{
		for( UINT i=0; i<uCount; i++ ) 
		{
			DWORD dwAttr = 0;

			//CPidlData data;
			//data.FromPidl(aPidls[i]);

			if (m_PidlMgr.GetItemType(aPidls[i]) == NWS_FOLDER)
			//if (data.IsFolder())
    		{
				*pdwAttribs |=SFGAO_FOLDER;
				*pdwAttribs |= SFGAO_HASSUBFOLDER;
    		}
			//*pdwAttribs &= dwAttr;
		}
	}

////	*pdwAttribs = 0;
//	
//	CPidlData data;
//	data.FromPidl(aPidls[0]);
//
//	TCHAR szTmp[MAX_PATH]=_TEXT("");
//	DWORD dwLen=MAX_PATH;
//	m_PidlMgr.GetFullName(aPidls[0],szTmp,&dwLen);
//
////	DWORD dwAttr;
////	dwAttr = 0x28000030;// SFGAO_BROWSABLE | SFGAO_FOLDER | SFGAO_HASSUBFOLDER |SFGAO_HASPROPSHEET |SFGAO_DROPTARGET;
//	*pdwAttribs = SFGAO_BROWSABLE | SFGAO_FOLDER | SFGAO_HASSUBFOLDER;
//	if (data.IsFolder())
//	{
//		*pdwAttribs = SFGAO_BROWSABLE | SFGAO_FOLDER | SFGAO_HASSUBFOLDER;
//	}
//	else
//	{
//		*pdwAttribs = 0;
//	}
//
    return S_OK;
}


// GetUIObjectOf() is called to get several sub-objects like IExtractIcon and IDataObject
STDMETHODIMP CVirtualDriveFolder::GetUIObjectOf(HWND hwndOwner, UINT uCount, LPCITEMIDLIST* pPidl, REFIID riid, LPUINT puReserved, void** ppvReturn)
{
	if(( riid == IID_IContextMenu ) || ( riid == IID_IContextMenu2))
	{
		CComObject< CVDFContextMenu > *pContextMenu;
	    CComObject< CVDFContextMenu >::CreateInstance(&pContextMenu);

		pContextMenu->AddRef();

		pContextMenu->Initialize(pPidl[0],NULL,NULL);
		HRESULT hr = pContextMenu->QueryInterface(riid, ppvReturn);

		pContextMenu->Release();

		ATLTRACE(_T("IID_IContextMenu \n"));
		return hr;
	}

	if ( riid == IID_IDropTarget)
	{
		ATLTRACE(_T("IID_IDropTarget \n"));
		return E_NOINTERFACE;
	}
	if ( riid == IID_IQueryInfo)
	{
		ATLTRACE(_T("IID_IQueryInfo \n"));
		return E_NOINTERFACE;
	}

	// If we should handle the item icon, let's do it
	if( riid == IID_IExtractIcon ) 
    {
	    CComObject< CExtractIcon > *pExtractIcon;
	    CComObject< CExtractIcon >::CreateInstance(&pExtractIcon);

		pExtractIcon->AddRef();

	    pExtractIcon->_Init(pPidl[0]);
		pExtractIcon->QueryInterface(IID_IExtractIcon, ppvReturn);

		pExtractIcon->Release();
		
		ATLTRACE(_T("IID_IExtractIcon \n"));

		return S_OK;
    }

	// Does the FileDialog need to embed some data?
	if (riid == IID_IDataObject)
	{
		/*CComObject<CNSFDropTarget> *pDropTarget;
		HR( CComObject<CNSFDropTarget>::CreateInstance(&pDropTarget) );
		if(Hr))
			return Hr;

		pDropTarget->AddRef();
		
		HR( pDropTarget->_Init(this, NULL) );
		
		Hr=pDropTarget->QueryInterface(IID_IDropTarget, ppvReturn);
		pDropTarget->Release();
		
		return Hr;*/
		ATLTRACE(_T("IID_IDataObject \n"));
		return E_NOINTERFACE;
	}

	/*if ( riid == IID_IShellLink)
	{
		CComObject< CVirtualDriveLink > *pVirtualLink;
	    CComObject< CVirtualDriveLink >::CreateInstance(&pVirtualLink);

		pVirtualLink->AddRef();

		pVirtualLink->SetIDList(*pPidl);
		pVirtualLink->QueryInterface(IID_IShellLink, ppvReturn);

		pVirtualLink->Release();
		
		ATLTRACE(_T("IID_IShellLink \n"));

		return S_OK;
	}*/

	ATLTRACE(_T("CVirtualDriveFolder::GetUIObjectOf\n"));

	return E_NOINTERFACE;
}

STDMETHODIMP CVirtualDriveFolder::BindToStorage(LPCITEMIDLIST, LPBC, REFIID, void** ppvOut)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::BindToStorage()\n", this);
	*ppvOut = NULL;
	return E_NOTIMPL;
}

#define GET_SHGDN_FOR(dwFlags)         ((DWORD)dwFlags & (DWORD)0x0000FF00)
#define GET_SHGDN_RELATION(dwFlags)    ((DWORD)dwFlags & (DWORD)0x000000FF)


STDMETHODIMP CVirtualDriveFolder::GetDisplayNameOf(LPCITEMIDLIST pidl, DWORD uFlags, LPSTRRET lpName)
{
	ATLTRACE("CVirtualDriveFolder::GetUIObjectOf\n");

	HRESULT Hr;
	TCHAR szText[MAX_PATH]=_TEXT("");
	TCHAR szTmp[MAX_PATH]=_TEXT("");
	DWORD dwLen=MAX_PATH;

	if (NULL==pidl || pidl->mkid.cb == 0)//root folder
	{
		_tcscat(szText,_Module.m_szMyComputerShellName);
		_tcscat(szText,_TEXT("\\"));
		::LoadString(_Module.GetResourceInstance(),IDS_VFNAME,szTmp,MAX_PATH);
		_tcscat(szText,szTmp);
	}
	else
	{
		if ((uFlags & SHGDN_INFOLDER) == SHGDN_INFOLDER)
		{
				m_PidlMgr.GetName(pidl,szText);
				HR(_tcslen(szText)>0);
		}
		else
		{
				_tcscat(szText,_Module.m_szMyComputerShellName);
				_tcscat(szText,_TEXT("\\"));
				::LoadString(_Module.GetResourceInstance(),IDS_VFNAME,szTmp,MAX_PATH);
				_tcscat(szText,szTmp);
				_tcscat(szText,_TEXT("\\"));
				//get the full pidl
				LPITEMIDLIST   tmpPidl;

				tmpPidl = m_PidlMgr.Concatenate(m_pidlRoot, pidl);    
				HR(m_PidlMgr.GetFullName(tmpPidl,szTmp,&dwLen));
				if( dwLen > 0 )
				{
					_tcscat(szText,szTmp);
				}
				m_PidlMgr.Delete(tmpPidl);
		}
	}

	USES_CONVERSION;
#ifdef _UNICODE
	// Allocate the wide character string 
	int cchOleStr = _tcslen(szText) + 1;  
	lpName->pOleStr = (LPWSTR)_Module.m_Allocator.Alloc(cchOleStr * sizeof(OLECHAR)); 

	if( lpName->pOleStr==NULL ) 
		return E_OUTOFMEMORY;  

	::ZeroMemory(lpName->pOleStr,cchOleStr * sizeof(OLECHAR));

	lpName->uType = STRRET_WSTR;
	_tcscpy( lpName->pOleStr, T2CW(szText));
#else
	lpName->uType = STRRET_CSTR;
	_tcscpy(lpName->cStr, T2CA(szText)); //cStr had allocated space
#endif
	return S_OK;
}

STDMETHODIMP CVirtualDriveFolder::ParseDisplayName(HWND, LPBC, LPOLESTR str, LPDWORD sz, LPITEMIDLIST* pPidl, LPDWORD pLen)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::ParseDisplayName()\n", this);

	CString path(str);
	LPITEMIDLIST root = NULL;

	int curpos = 0;
	CString token = path.Tokenize(_T("\\"),curpos);

	VDFolderBehaviourBase * behaviour = new VDRootFolderBehaviour();

	int state = 0;
	while (token.GetLength() > 0 )
	{
		state = behaviour->StateChildByName(token);

		if (root == NULL)
		{
			root = m_PidlMgr.Create(NWS_FOLDER,state,token);

			/*CString name;
			name.LoadString(IDS_BACKEDUP);
			if (name.Compare(token)==0)
			{
				state = ENTRY_BACKUP;
				root = m_PidlMgr.Create(NWS_FOLDER,ENTRY_BACKUP,name);
			}
			else
			{
				name.LoadString(IDS_PENDING);
				if (name.Compare(token)==0)
				{
					state = ENTRY_PENDING;
					root = m_PidlMgr.Create(NWS_FOLDER,ENTRY_PENDING,name);
				}
				else
				{
					name.LoadString(IDS_RECOVERYLOG);
					if (name.Compare(token)==0)
					{
						state = ENTRY_RECOVERYLOG;
						root = m_PidlMgr.Create(NWS_FOLDER,ENTRY_RECOVERYLOG,name);
					}
					else
					{
						break;
					}
				}
			}*/
		}
		else
		{
			// add token
			root = m_PidlMgr.Concatenate(root,m_PidlMgr.Create(NWS_FOLDER,state,token));
		}

		VDFolderBehaviourBase * tempBehaviour = behaviour;
		behaviour = behaviour->CreateChildBehaviour(root);
		delete tempBehaviour;

		// parse token
		
		token = path.Tokenize(_T("\\"),curpos);
	}

	*pPidl = root;

	return S_OK;
}

STDMETHODIMP CVirtualDriveFolder::SetNameOf(HWND, LPCITEMIDLIST, LPCOLESTR, DWORD, LPITEMIDLIST*)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::SetNameOf()\n", this);
	return E_NOTIMPL;
}

//-------------------------------------------------------------------------------
// IShellDetails

STDMETHODIMP CVirtualDriveFolder::ColumnClick(UINT iColumn)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::ColumnClick(iColumn=%d)\n", this, iColumn);

	// The caller must sort the column itself
	return E_NOTIMPL;
}


STDMETHODIMP CVirtualDriveFolder::GetDetailsOf(LPCITEMIDLIST pidl, UINT iColumn, LPSHELLDETAILS pDetails)
{
	ATLTRACE("CVirtualDriveFolder::GetDetailsOf");
	//ATLTRACE(_T("CVirtualDriveFolder(0x%08x)::GetDetailsOf(iColumn=%d) pidl=[%s]\n"), this, iColumn, PidlToString(pidl));

	return E_NOTIMPL;
}

//-------------------------------------------------------------------------------
// IShellFolder2

STDMETHODIMP CVirtualDriveFolder::EnumSearches(IEnumExtraSearch **ppEnum)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::EnumSearches()\n", this);
	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveFolder::GetDefaultColumn(DWORD dwReserved, ULONG *pSort, ULONG *pDisplay)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::GetDefaultColumn()\n", this);

	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveFolder::GetDefaultColumnState(UINT iColumn, SHCOLSTATEF *pcsFlags)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::GetDefaultColumnState(iColumn=%d)\n", this, iColumn);

	if (!pcsFlags)
		return E_POINTER;

	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveFolder::GetDefaultSearchGUID(GUID *pguid)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::GetDefaultSearchGUID()\n", this);
	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveFolder::GetDetailsEx(LPCITEMIDLIST pidl, const SHCOLUMNID *pscid, VARIANT *pv)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::GetDetailsEx()\n", this);
	return E_NOTIMPL;
}

STDMETHODIMP CVirtualDriveFolder::MapColumnToSCID(UINT iColumn, SHCOLUMNID *pscid)
{
	ATLTRACE("CVirtualDriveFolder(0x%08x)::MapColumnToSCID()\n", this);
	return E_NOTIMPL;
}

/////////////////////////////////////////////////////////////////////////////
// IQueryInfo
//

//GetInfoFlags() Retrieves the information flags for an item. 
//				 This method is not currently used. 
STDMETHODIMP CVirtualDriveFolder::GetInfoFlags(DWORD* pdwFlags)
{
	ATLTRACENOTIMPL(_T("CVirtualDriveFolder::GetInfoFlags"));
}

//GetInfoTip() Retrieves the info tip text information for an item. 
STDMETHODIMP CVirtualDriveFolder::GetInfoTip(DWORD, LPWSTR* ppwszTip)
{
	if (ppwszTip == NULL)
		return E_POINTER;

   CString bstr;
   bstr.LoadString(IDS_VFNAME);
   *ppwszTip = (LPWSTR)_Module.m_Allocator.Alloc((bstr.GetLength()+1) * sizeof(WCHAR)); 
   if( *ppwszTip==NULL ) 
	   return E_OUTOFMEMORY;  

   ::ZeroMemory(*ppwszTip,(bstr.GetLength()+1) * sizeof(WCHAR));

   wcscpy(*ppwszTip, bstr);
   return S_OK;
}

STDMETHODIMP CVirtualDriveFolder::BrowseObject(BSTR fullPath)
{
	return S_OK;
}

HRESULT CVirtualDriveFolder::DoDrop(LPDATAOBJECT pDataObj, 
								  DWORD dwDropEffect,
								  LPITEMIDLIST pidlDropDest,
								  UINT iFmtIdx)
{
    VALIDATE_POINTER(pDataObj);

	HRESULT Hr = E_INVALIDARG;
    STGMEDIUM stgmed;

	if( FMT_NSEDRAGDROP_INDEX == iFmtIdx )
	{
		// Check for CFSTR_NSEDRAGDROP
		FORMATETC fe2 = { (CLIPFORMAT)::RegisterClipboardFormat( _T("NSEDRAGDROP") ),
						   NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
		if( SUCCEEDED( pDataObj->GetData(&fe2, &stgmed) ) ) 
		{
			//MessageBox(NULL, L"_DoDrop_NSEDRAGDROP", L"Error", MB_OK );
			//Hr = _DoDrop_NSEDRAGDROP(stgmed.hGlobal, dwDropEffect,pidlDropDest);
			::ReleaseStgMedium(&stgmed);
			return Hr;
		}
	}
	else if(FMT_HDROP_INDEX == iFmtIdx)
	{
		//MessageBox(NULL, L"FMT_HDROP_INDEX", L"Error", MB_OK );
		// Check for HDROP
		FORMATETC fe1 = { CF_HDROP, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
		if( SUCCEEDED( pDataObj->GetData(&fe1, &stgmed) ) ) 
		{
			Hr = DoDrop_HDROP(stgmed.hGlobal, dwDropEffect,pidlDropDest);
			::ReleaseStgMedium(&stgmed);
			return Hr;
		}
	}

	//MessageBox(NULL, L"Droppped", L"Error", MB_OK );
	return 0;
}

HRESULT CVirtualDriveFolder::DoDrop_HDROP(HGLOBAL hMem, 
										DWORD dwDropEffect,
										LPITEMIDLIST pidlDropDest)
{
    VALIDATE_POINTER(hMem);
	TCHAR szMsg[MAX_PATH]={0};
    
    HRESULT Hr= S_OK;
    
    // Get target path
	TCHAR szDropDest[MAX_PATH]=_T("");
	DWORD dwSize=MAX_PATH;
	LPITEMIDLIST pidlComplexDest = m_PidlMgr.Concatenate(/*m_PidlPath*/m_pidlRoot,pidlDropDest);
	if(pidlComplexDest)
	{
		HR(m_PidlMgr.GetFullName(pidlComplexDest,szDropDest,&dwSize));
		if(dwSize == 0)
		{
			m_PidlMgr.Delete(pidlComplexDest);
			return E_FAIL;
		}
	}

    DWORD dwFileOpFlags = 0;
    // Iterate over HDROP information
    HDROP hDrop = (HDROP) ::GlobalLock(hMem);
    if( hDrop==NULL ) 
	{
		m_PidlMgr.Delete(pidlComplexDest);
		return E_OUTOFMEMORY;
	}

	TCHAR szItemName[MAX_PATH]={0};
	PTCHAR pChr = NULL;
	TCHAR szFileName[MAX_PATH]={0};
    UINT nFiles = ::DragQueryFile(hDrop, (UINT) -1, NULL, 0);
 //   for( UINT i=0; i<nFiles; i++ ) 
 //   {
 //   	// Get dragged filename

 //   	if( ::DragQueryFile(hDrop, i, szFileName, MAX_PATH)==0 ) 
	//	{
	//		m_PidlMgr.Delete(pidlComplexDest);
	//		::GlobalUnlock(hMem);
 //   		return E_FAIL;
	//	}
	//	
	//	// dragged a folder
	//	if((::GetFileAttributes(szFileName) & FILE_ATTRIBUTE_DIRECTORY)!=0)
	//	{
	//		MessageBox(NULL,_T("Drag & Drop operation be stopped!\nOur NSE doesn't support drag&drop operation on folder object,\nPlease make sure to drag file objects only!"),_T("NSExtDragDrop"),MB_OK);
	//		m_PidlMgr.Delete(pidlComplexDest);
	//		
	//		::GlobalUnlock(hMem);
	//		return E_FAIL;
	//	}
	//}

	ICppProxyPtr proxy;
	proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
	

    for(UINT i=0; i<nFiles; i++ ) 
    {
    	// Get dragged filename

    	if( ::DragQueryFile(hDrop, i, szFileName, MAX_PATH)==0 ) 
		{
			m_PidlMgr.Delete(pidlComplexDest);
			::GlobalUnlock(hMem);
    		return E_FAIL;
		}
		
		//MessageBox(NULL, szFileName, L"Error", MB_OK );
			
		if (proxy)
		{
			proxy->NotifyCommand( COMMAND_BACKTHISUP, 0, _bstr_t(szFileName) );
		}

		pChr = _tcsrchr(szFileName,_T('\\'));
		_tcscpy(szItemName,pChr+1);
			
		//AddItemToCfgFile(pidlComplexDest,szItemName,NWS_FILE);
		
		RefreshShellViewWndsExcept( NULL);    		
    }

	m_PidlMgr.Delete(pidlComplexDest);
    ::GlobalUnlock(hMem);

    return Hr;
}