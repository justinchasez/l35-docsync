#pragma once
#include "vdfolderbehaviourbase.h"
class VDRecoveryFolderBehaviour :
	public VDFolderBehaviourBase
{
public:
	VDRecoveryFolderBehaviour(void);
	virtual ~VDRecoveryFolderBehaviour(void);

	virtual void GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList);
	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child);

	virtual int StateChildByName(const CString& name);
};

