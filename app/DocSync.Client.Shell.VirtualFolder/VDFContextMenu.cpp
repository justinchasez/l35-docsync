// VDFContextMenu.cpp : Implementation of CVDFContextMenu

#include "stdafx.h"
#include "VDFContextMenu.h"

// CVDFContextMenu
HRESULT CVDFContextMenu::QueryContextMenu(HMENU hmenu, UINT indexMenu,
		UINT idCmdFirst, UINT idCmdLast, UINT uFlags)
{
	USES_CONVERSION;

	UINT idCmd = idCmdFirst;

	//InsertMenu(hmenu, indexMenu++, MF_STRING|MF_BYPOSITION,idCmd++, _T("Test Menu Item"));

	return MAKE_SCODE(SEVERITY_SUCCESS, FACILITY_NULL, 
		idCmd-idCmdFirst);
}

 
// InvokeCommand
HRESULT CVDFContextMenu::InvokeCommand(LPCMINVOKECOMMANDINFO lpcmi)
{
	return S_OK;
}

#ifdef WIN64
HRESULT CVDFContextMenu::GetCommandString(UINT_PTR idCmd, UINT uFlags, 
		UINT *pwReserved, LPSTR pszText, UINT cchMax)
#else
HRESULT CVDFContextMenu::GetCommandString(UINT idCmd, UINT uFlags, 
		UINT *pwReserved, LPSTR pszText, UINT cchMax)
#endif
{
	return E_NOTIMPL;

	/*USES_CONVERSION;

	if (uFlags == GCS_HELPTEXTA)
	{
		lstrcpyA(pszText, "Some Bisov Text");
	}

	if (uFlags == GCS_HELPTEXTW)
	{
		lstrcpyW((LPWSTR)pszText, L"Some Bisov Text");
	}

	return S_OK;*/
}



/////////////////////////////////////////////////////////////////////////////
// Methods of IContextMenu2

HRESULT CVDFContextMenu::HandleMenuMsg(UINT uMsg,WPARAM wParam, LPARAM lParam)
{
	return HandleMenuMsg2(uMsg, wParam, lParam, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// Methods of IContextMenu3

HRESULT CVDFContextMenu::HandleMenuMsg2(UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *plResult)
{
	switch(uMsg)
	{
		case WM_INITMENUPOPUP:
			break;

	    case WM_DRAWITEM:
			//DrawMenuItem((LPDRAWITEMSTRUCT) lParam);
			break;

		case WM_MEASUREITEM:
			//MeasureItem((LPMEASUREITEMSTRUCT) lParam);
			break;
	}
	
	return S_FALSE;
}

HRESULT CVDFContextMenu::Initialize(LPCITEMIDLIST pidlFolder, LPDATAOBJECT data, HKEY hKey)
{
	return S_OK;
}
