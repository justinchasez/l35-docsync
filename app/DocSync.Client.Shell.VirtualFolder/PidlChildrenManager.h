#pragma once

#include "pidlmgr.h"

class PidlChildrenManager
{
public:
	PidlChildrenManager(void);
	~PidlChildrenManager(void);

	void FillChirdren(const CString& fullPath, CPidlData& data);

private:
	void FillRootChirdren(const CString& fullPath, CPidlData& data);
	void FillEntryChirdren(const CString& fullPath, CPidlData& data);

	void FillBackedUpChirdren(const CString& fullPath, CPidlData& data);
	void FillPendingChirdren(const CString& fullPath, CPidlData& data);
	void FillRecoveryLogChirdren(const CString& fullPath, CPidlData& data);
};

