//========================================================================================
//========================================================================================

#include "stdafx.h"
#include "PidlMgr.h"

CPidlData::CPidlData()
{
	m_type = PIDL_ROOT_FOLDER;
	m_name = _T("Backup Drive");
	m_isFolder = true;
	m_entry_type = ENTRY_ROOT;
}

CPidlData::CPidlData(unsigned short type, unsigned short entry_type, unsigned short isFolder, CString  name)
{
	m_type = type;
	m_entry_type = entry_type;
	m_isFolder = isFolder;
	m_name = name;
}

LPITEMIDLIST CPidlMgr::Concatenate(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2)
{
LPITEMIDLIST   pidlNew;
UINT           cb1 = 0, 
               cb2 = 0;

	//are both of these NULL?
	if(!pidl1 && !pidl2)
	   return NULL;

	//if pidl1 is NULL, just return a copy of pidl2
	if(!pidl1)
	{
	   pidlNew = Copy(pidl2);
	   return pidlNew;
	}

	//if pidl2 is NULL, just return a copy of pidl1
	if(!pidl2)
	{
	   pidlNew = Copy(pidl1);
	   return pidlNew;
	}

	cb1 = GetSize(pidl1) - sizeof(ITEMIDLIST);
	cb2 = GetSize(pidl2);

	//create the new PIDL
	pidlNew = (LPITEMIDLIST)m_MallocPtr->Alloc(cb1 + cb2);
	if(pidlNew)
	{
		::ZeroMemory(pidlNew,cb1+cb2);

		//copy the first PIDL
		::CopyMemory(pidlNew, pidl1, cb1);
		//copy the second PIDL
		::CopyMemory(((LPBYTE)pidlNew) + cb1, pidl2, cb2);
	}
	return pidlNew;
}

HRESULT CPidlMgr::GetFullName(LPCITEMIDLIST pidl,LPTSTR szFullName,DWORD *pdwLen)
{
	if(!pdwLen)
		return E_FAIL;

	*pdwLen=0;

	if(!pidl) //NSE's root folder
	{
		return S_OK;
	}

	LPITEMIDLIST pidlTemp = (LPITEMIDLIST) pidl;
	CPidlData data;
	while (pidlTemp->mkid.cb != 0)
	{
//		TCHAR szTemp[MAX_PATH]=_TEXT("");

		data.FromPidl(pidlTemp);
	//	GetName(pidlTemp,szTemp);

		if( szFullName)
		{
			if(0 == *pdwLen)
			{
				_tcscpy(szFullName,data.m_name);
			}
			else if (_tcscmp(szFullName,data.m_name) != 0)
			{
				_tcscat(szFullName,_TEXT("\\"));
				_tcscat(szFullName,data.m_name);
			}
			*pdwLen =_tcslen(szFullName);
		}
		else
		{
			*pdwLen+=_tcslen(data.m_name);
		}
		pidlTemp = GetNextItem(pidlTemp);
	}  

	*pdwLen += 1;

	return S_OK;
}