#pragma once

#include <vector>
#include "CNWSPidlMgr.h"

class VDFolderBehaviourBase
{
public:
	VDFolderBehaviourBase(void);
	virtual ~VDFolderBehaviourBase(void);

	virtual void GetChildren(LPCITEMIDLIST pidl, std::vector<ITEMDATA>& vctList);
	virtual void CreateColumns(WTL::CListViewCtrl& wndList);

	virtual void SetCustomData(WTL::CListViewCtrl& listCtrl, int index, LPCUSTOMFOLDER_DATA data);
	virtual void ParseName(CString& name);
	virtual LPCUSTOMFOLDER_DATA CreateCustomData(const CString& name, IUnknown* proxy);
	virtual VDFolderBehaviourBase* CreateChildBehaviour(LPCITEMIDLIST child) = 0;

	virtual int TrackContextMenu(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index);
	virtual DWORD FolderViewMode(UINT viewMode);

	virtual int GetPanelMode(LPCITEMIDLIST pidl);

	static VDFolderBehaviourBase* CreateBehaviour(int state);

	virtual int StateChildByName(const CString& name) = 0;

	virtual void Command(HWND window, LPCITEMIDLIST pidl, const CString& fullPath, int cmd, IUnknown* proxy, WTL::CListViewCtrl& listCtrl, int index)
	{// do nothing in base class
	}

protected:
	CNWSPidlMgr m_PidlMgr;
};

