#include "StdAfx.h"
#include "RecoveryPendingPanel.h"
#include "VFResources.h"

RecoveryPendingPanel::RecoveryPendingPanel(void)
{
}


RecoveryPendingPanel::~RecoveryPendingPanel(void)
{
//	::PostMessage(_Module.m_mainWindow,REMOVE_NOTIFY_WINDOW,(WPARAM)m_hWnd,1);
}

LRESULT RecoveryPendingPanel::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	_Module.EventHandlerManagerInstance()->UnadviceWindow(m_hWnd);

	return 0;
}

LRESULT RecoveryPendingPanel::OnUpdateStatusInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (IsWindow())
	{
		try
		{
			ICppProxyPtr proxy = NULL;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);

			if (proxy)
			{
				CString str;

				long value = 0;
				proxy->GetStatusInfo(STATUS_SUCCESSFULLY_RESTORED, &value);
				str.Format(_T("- View files that have been successfully restored (%d files)"),value);
				m_filesCompleted.SetLabel(str);
			
				value = 0;
				proxy->GetStatusInfo(STATUS_COULD_NOT_BE_RESTORED, &value);
				str.Format(_T("- View files that could not be restored (%d files)"),value);
				m_filesErrors.SetLabel(str);

				Invalidate();

				GetParent().Invalidate();

				RECT rect = {0};
				GetWindowRect(&rect);
				PostMessage(WM_SIZE,0,MAKELPARAM(rect.right-rect.left, rect.bottom-rect.top));
			}
		}
		catch (...) { /*do nothing*/ }
	}

	return 0;
}

LRESULT RecoveryPendingPanel::OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rect = {20,20,280,40};

	RECT rcName = {20,2,280,40};
	m_name.Create(m_hWnd,rcName,NULL, WS_VISIBLE | WS_CHILD);
	m_name.SetFont(CVFResources::instance().StaticFont());
	m_name.SetWindowText(_T("This folder displays all the files currently queued for restore. Right click on any file or group of files to remove them from the restore queue or change their restore priority. To view the overall status of your restore jobs in progress or cancel all pending re-stores, double-click on the Restore Icon in your system tray to open the InfoCenter, then click the Restore Status bar"));

	CString str;

	m_filesCompleted.Create(m_hWnd,rect, NULL, WS_CHILD | WS_VISIBLE);
	m_filesCompleted.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- View files that have been successfully restored (%d files)"),0);
	m_filesCompleted.SetLabel(str);
	m_filesCompleted.SetHyperLink(_T("Files that have been successfully restored"));
	m_filesCompleted.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_filesCompleted.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_filesCompleted.m_clrLink = CVFResources::instance().LinkColor();
	m_filesCompleted.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	m_filesErrors.Create(m_hWnd,rect, NULL, WS_CHILD | WS_VISIBLE);
	m_filesErrors.m_dwExtendedStyle = HLINK_UNDERLINEHOVER | HLINK_NOTIFYBUTTON;
	str.Format(_T("- View files that could not be restored (%d files)"),0);
	m_filesErrors.SetLabel(str);
	m_filesErrors.SetHyperLink(_T("Files that could not be restored"));
	m_filesErrors.SetLinkFont(CVFResources::instance().HyperLinkFont());
	m_filesErrors.m_hFontNormal = CVFResources::instance().HyperLinkFont();
	m_filesErrors.m_clrLink = CVFResources::instance().LinkColor();
	m_filesErrors.m_clrVisited = CVFResources::instance().LinkVisitedColor();

	_Module.EventHandlerManagerInstance()->AdviceWindow(m_hWnd);
	_Module.EventHandlerManagerInstance()->NotifyUpdate();

	return 0;
}

SIZE RecoveryPendingPanel::CalcSize(SIZE& sz, bool moved)
{
	if (m_name.IsWindow() &&
		m_filesCompleted.IsWindow() &&
		(sz.cx + sz.cy) > 0 &&
		m_filesErrors.IsWindow())
	{
		RECT rect = {0,0,sz.cx - 20,sz.cy};

		MoveChild(m_name, rect, moved);

		rect.top = rect.bottom;
		rect.bottom = sz.cy;
		rect.left = 0;
		rect.right = sz.cx - 20;

		MoveChild(m_filesCompleted, rect, moved);

		rect.top = rect.bottom;
		rect.bottom = sz.cy;
		rect.left = 0;
		rect.right = sz.cx - 20;

		MoveChild(m_filesErrors, rect, moved);

		sz.cy = rect.bottom;
	}

	return sz;
}

LRESULT RecoveryPendingPanel::OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SIZE sz = {LOWORD(lParam), HIWORD(lParam)};

	CalcSize(sz, true);

	return 0;
}

LRESULT RecoveryPendingPanel::OnNotify(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	NMHDR* nmhdr = (NMHDR*)lParam;

	if (m_filesCompleted.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,1);
		}
	}
	else if (m_filesErrors.m_hWnd == nmhdr->hwndFrom)
	{
		HWND ownerWindow = (HWND)::SendMessage(GetParent().m_hWnd,GET_OWNER_WINDOW,0,0);
		if (::IsWindow(ownerWindow))
		{
			::PostMessage(ownerWindow,NAVIGATE_VIRTUAL_FOLDER,0,2);
		}
	}

	return 0;
}

LRESULT RecoveryPendingPanel::OnColorStatic(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	SetTextColor((HDC)wParam,CVFResources::instance().StaticColor());
	SetBkMode((HDC)wParam,TRANSPARENT);
	return (LRESULT)GetStockObject(NULL_BRUSH);
}

void RecoveryPendingPanel::InnerPaint(HDC hDC)
{
	/*RECT rect;
	GetClientRect(&rect);

	::FillRect(hDC,&rect,GetSysColorBrush(COLOR_WINDOW));*/

	//CVFResources::instance().ViewStatusIcon().DrawIconEx(hDC,2,2,16,16,0);
}

LRESULT RecoveryPendingPanel::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{

	if(wParam != NULL)
	{
		InnerPaint((HDC)wParam);
	}
	else
	{
		CPaintDC dc(m_hWnd);
		InnerPaint(dc.m_hDC);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT RecoveryPendingPanel::OnEraseBackground(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	return 1;
}