using DocSync.Server.Core.Entities;
using FluentNHibernate.Mapping;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// Key value pair map.
    /// </summary>
    public class SettingEntryMap : ClassMap<SettingEntry>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingEntryMap"/> class.
        /// </summary>
        public SettingEntryMap()
        {
            Table("Settings");
            Id(x => x.Key).GeneratedBy.Assigned().Column("[Key]");
            Map(x => x.Value).Column("[Value]");
        }
    }
}