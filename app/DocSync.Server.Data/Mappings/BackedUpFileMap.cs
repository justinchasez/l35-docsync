using DocSync.Server.Core.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// Backed up file file mapping override.
    /// </summary>
    public class BackedUpFileMap : IAutoMappingOverride<BackedUpFile>
    {
        /// <summary>
        /// Alter the automapping for this type
        /// </summary>
        /// <param name="mapping">Automapping to override.</param>
        public void Override(AutoMapping<BackedUpFile> mapping)
        {
            mapping.HasMany(x => x.FileVersions)
                .AsSet()
                .Cascade.AllDeleteOrphan();
        }
    }
}