﻿using DocSync.Server.Core.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    public class FileLinkMap : IAutoMappingOverride<FileLink>
    {
        /// <summary>
        /// Alter the automapping for this type
        /// </summary>
        /// <param name="mapping">Automapping</param>
        public void Override(AutoMapping<FileLink> mapping)
        {
        }
    }
}
