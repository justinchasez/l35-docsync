using DocSync.Server.Core.Entities.StaticPages;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// NHibernate mapping for static pages
    /// </summary>
    public class StaticPageMap : IAutoMappingOverride<StaticPage>
    {
        /// <summary>
        /// Alter the automapping for this type.
        /// </summary>
        /// <param name="mapping">Automapping to override.</param>
        public void Override(AutoMapping<StaticPage> mapping)
        {
            mapping.Map(x => x.PageUrl).ReadOnly();
        }
    }
}