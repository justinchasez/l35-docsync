using FluentNHibernate.Conventions.Inspections;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Has many to many table name convention.
    /// </summary>
    public class ManyToManyTableNameConvention : FluentNHibernate.Conventions.ManyToManyTableNameConvention
    {
        /// <summary>
        /// Gets the name used for bi-directional many-to-many tables. Implement this member to control how
        /// your join table is named for bi-directional relationships.
        /// </summary>
        /// <param name="collection">Main collection</param>
        /// <param name="otherSide">Inverse collection</param>
        /// <returns>Many-to-many table name</returns>
        /// <remarks>
        /// This method will be called once per bi-directional relationship; once one side of the relationship
        /// has been saved, then the other side will assume that name aswell.
        /// </remarks>
        protected override string GetBiDirectionalTableName(IManyToManyCollectionInspector collection, IManyToManyCollectionInspector otherSide)
        {
            return Inflector.Net.Inflector.Pluralize(collection.EntityType.Name) + "_" +
                   Inflector.Net.Inflector.Pluralize(otherSide.EntityType.Name);
        }

        /// <summary>
        /// Gets the name used for uni-directional many-to-many tables. Implement this member to control how
        /// your join table is named for uni-directional relationships.
        /// </summary>
        /// <param name="collection">Main collection</param>
        /// <returns>Many-to-many table name</returns>
        protected override string GetUniDirectionalTableName(IManyToManyCollectionInspector collection)
        {
            return Inflector.Net.Inflector.Pluralize(collection.EntityType.Name) + "_" +
                   Inflector.Net.Inflector.Pluralize(collection.ChildType.Name);
        }
    }
}