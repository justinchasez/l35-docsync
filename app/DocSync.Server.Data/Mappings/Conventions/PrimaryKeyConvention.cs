using FluentNHibernate.Conventions;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Primary key convention.
    /// </summary>
    public class PrimaryKeyConvention : IIdConvention
    {
        /// <summary>
        /// Apply changes to the target
        /// </summary>
        /// <param name="instance">Instance to map.</param>
        public void Apply(FluentNHibernate.Conventions.Instances.IIdentityInstance instance)
        {
            instance.Column("Id");
            instance.GeneratedBy.Native();
            instance.UnsavedValue("0");
        }
    }
}