using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Has many to many convention.
    /// </summary>
    public class HasManyToManyConvention : IHasManyToManyConvention
    {
        /// <summary>
        /// Apply changes to the target
        /// </summary>
        /// <param name="instance">Instance to map.</param>
        public void Apply(IManyToManyCollectionInstance instance)
        {
            instance.Cascade.SaveUpdate();
        }
    }
}