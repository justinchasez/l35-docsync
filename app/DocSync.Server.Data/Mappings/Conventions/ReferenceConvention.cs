using FluentNHibernate.Conventions;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// References convention.
    /// </summary>
    public class ReferenceConvention : IReferenceConvention
    {
        /// <summary>
        /// Apply changes to the target
        /// </summary>
        /// <param name="instance">Instance to map.</param>
        public void Apply(FluentNHibernate.Conventions.Instances.IManyToOneInstance instance)
        {
            instance.Column(instance.Property.DeclaringType.Name + "Id");
        }
    }
}