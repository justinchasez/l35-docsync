using FluentNHibernate.Conventions;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Table name convention.
    /// </summary>
    public class TableNameConvention : IClassConvention
    {
        /// <summary>
        /// Apply changes to the target
        /// </summary>
        /// <param name="instance">Instance to map.</param>
        public void Apply(FluentNHibernate.Conventions.Instances.IClassInstance instance)
        {
            instance.Table(Inflector.Net.Inflector.Pluralize(instance.EntityType.Name));
        }
    }
}