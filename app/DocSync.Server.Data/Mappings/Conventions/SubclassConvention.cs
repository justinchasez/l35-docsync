using System;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Represents subclass convention.
    /// </summary>
    public class SubclassConvention : ISubclassConvention
    {
        /// <summary>
        /// Apply changes to the target.
        /// </summary>
        /// <param name="instance">Instance of entity.</param>
        public void Apply(ISubclassInstance instance)
        {
            Type entityType = Type.GetType(instance.Name);
            instance.DiscriminatorValue(entityType.Name);
        }
    }
}