using FluentNHibernate.Conventions;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Has many convention.
    /// </summary>
    public class HasManyConvention : IHasManyConvention
    {
        /// <summary>
        /// Apply changes to the target.
        /// </summary>
        /// <param name="instance">Instance to apply.</param>
        public void Apply(FluentNHibernate.Conventions.Instances.IOneToManyCollectionInstance instance)
        {
            instance.Key.Column(instance.EntityType.Name + "Id");
            instance.Cascade.SaveUpdate();
            instance.Inverse();
        }
    }
}