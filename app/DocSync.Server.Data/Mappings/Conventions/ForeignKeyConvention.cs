using System;
using FluentNHibernate;

namespace DocSync.Server.Data.Mappings.Conventions
{
    /// <summary>
    /// Foreign key convention.
    /// </summary>
    public class ForeignKeyConvention : FluentNHibernate.Conventions.ForeignKeyConvention
    {
        /// <summary>
        /// Gets the name of the key.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="type">The entity type.</param>
        /// <returns>Entity key name.</returns>
        protected override string GetKeyName(Member property, Type type)
        {
            if (property == null)
            {
                return type.Name + "Id";
            }

            return property.Name + "Id";
        }
    }
}