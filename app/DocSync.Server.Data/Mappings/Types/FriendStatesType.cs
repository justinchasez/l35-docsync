using DocSync.Server.Core.Enums;

namespace DocSync.Server.Data.Mappings.Types
{
    /// <summary>
    /// Enum type for friend state.
    /// </summary>
    public class FriendStatesType : NHibernate.Type.EnumStringType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FriendStatesType"/> class.
        /// </summary>
        public FriendStatesType()
            : base(typeof(FriendStates))
        {
        }
    }
}