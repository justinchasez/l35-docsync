using DocSync.Server.Core.Entities;
using DocSync.Server.Data.Mappings.Types;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// friend table mapping override.
    /// </summary>
    public class FriendMap : IAutoMappingOverride<Friend>
    {
        /// <summary>
        /// Alter the automapping for this type
        /// </summary>
        /// <param name="mapping">Automapping to override.</param>
        public void Override(AutoMapping<Friend> mapping)
        {
            mapping.Map(x => x.State).CustomType(typeof(FriendStatesType));
        }
    }
}