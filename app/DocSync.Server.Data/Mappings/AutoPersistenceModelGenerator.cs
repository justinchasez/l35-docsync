using System;
using System.Linq;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Entities.Payments;
using DocSync.Server.Data.Mappings.Conventions;
using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions;
using SharpArch.Core.DomainModel;
using ForeignKeyConvention = DocSync.Server.Data.Mappings.Conventions.ForeignKeyConvention;
using ManyToManyTableNameConvention = DocSync.Server.Data.Mappings.Conventions.ManyToManyTableNameConvention;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// Autopersistence model generator.
    /// </summary>
    public class AutoPersistenceModelGenerator 
    {
        /// <summary>
        /// Generates this instance model.
        /// </summary>
        /// <returns><see cref="AutoPersistenceModel"/> instance.</returns>
        public AutoPersistenceModel Generate()
        {
            var mappings = new AutoPersistenceModel();
            mappings.AddEntityAssembly(typeof(RegisteredUser).Assembly).Where(this.GetAutoMappingFilter);
            mappings.Conventions.Setup(this.GetConventions());
            mappings.Setup(this.GetSetup());
            mappings.IgnoreBase<Entity>();
            mappings.IgnoreBase(typeof(EntityWithTypedId<>));
            mappings.IncludeBase(typeof(Payment));
            mappings.UseOverridesFromAssemblyOf<AutoPersistenceModelGenerator>();
            
            return mappings;
        }

        /// <summary>
        /// Gets the setup.
        /// </summary>
        /// <returns>Automapping expression.</returns>
        private Action<AutoMappingExpressions> GetSetup()
        {
            return c =>
                       {
                           c.FindIdentity = type => type.Name == "Id";
                           c.SubclassStrategy = type => SubclassStrategy.Subclass;
                       };
        }

        /// <summary>
        /// Gets the conventions.
        /// </summary>
        /// <returns>Conventions list.</returns>
        private Action<IConventionFinder> GetConventions()
        {
            return c =>
                       {
                           c.Add<ForeignKeyConvention>();
                           c.Add<HasManyConvention>();
                           c.Add<HasManyToManyConvention>();
                           c.Add<ManyToManyTableNameConvention>();
                           c.Add<PrimaryKeyConvention>();
                           c.Add<ReferenceConvention>();
                           c.Add<TableNameConvention>();
                           c.Add<SubclassConvention>();
                       };
        }

        /// <summary>
        /// Provides a filter for only including types which inherit from the IEntityWithTypedId interface.
        /// </summary>
        /// <param name="t">The type to filter.</param>
        /// <returns><see langword="true"/> if it is entity, otherwise <see langword="false"/></returns>
        private bool GetAutoMappingFilter(Type t)
        {
            if (t == typeof(RegisteredUser))
            {
                return false;
            }

            return t.GetInterfaces().Any(x =>
                                         x.IsGenericType &&
                                         x.GetGenericTypeDefinition() == typeof(IEntityWithTypedId<>));
        }
    }
}