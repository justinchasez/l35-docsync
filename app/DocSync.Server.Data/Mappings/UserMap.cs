using DocSync.Server.Core.Entities;
using FluentNHibernate.Mapping;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// User mapping
    /// </summary>
    public class UserMap : ClassMap<RegisteredUser>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserMap"/> class.
        /// </summary>
        public UserMap()
        {
            Table("aspnet_Users");
            Id(x => x.Id, "UserId").GeneratedBy.Guid();
            Map(x => x.Name).Column("UserName");
            Map(x => x.LoweredUserName).Column("LoweredUserName");

            Join("aspnet_Membership",
              j =>
              {
                  j.KeyColumn("UserId");
                  j.Map(x => x.Email, "Email");
                  j.Map(x => x.RegistrationDate, "CreateDate");
                  j.Map(x => x.HeardAbout, "Comment");
              });

            HasOne(x => x.PaymentPlan)
              .Access.CamelCaseField()
              .Cascade.SaveUpdate()
              .LazyLoad()
              .PropertyRef(f => f.User);

            HasMany(x => x.Friends).KeyColumn("UserId")
                .AsSet()
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .Access.CamelCaseField()
                .KeyColumn("UserId");
        }
    }
}