using DocSync.Server.Core.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// Computer mapping override
    /// </summary>
    public class ComputerMap : IAutoMappingOverride<Computer>
    {
        /// <summary>
        /// Alter the automapping for this type
        /// </summary>
        /// <param name="mapping">Automapping to override.</param>
        public void Override(AutoMapping<Computer> mapping)
        {
            mapping.HasMany(x => x.PayedPeriods)
                .KeyColumn("ComputerId")
                .AsSet()
                .Inverse()
                .Cascade.SaveUpdate();

            mapping.HasMany(x => x.Files)
                .KeyColumn("ComputerId")
                .AsSet()
                .Inverse()
                .Cascade.AllDeleteOrphan();

            mapping.HasMany(x => x.LocalUsers)
                .KeyColumn("ComputerId")
                .AsSet()
                .Inverse()
                .Cascade.AllDeleteOrphan();

            mapping.IgnoreProperty(x => x.BytesUsed);
        }
    }
}