using DocSync.Server.Core.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DocSync.Server.Data.Mappings
{
    /// <summary>
    /// Payment plan map.
    /// </summary>
    public class PaymentPlanMap : IAutoMappingOverride<PaymentPlan>
    {
        /// <summary>
        /// Alter the automapping for this type.
        /// </summary>
        /// <param name="mapping">Automapping to override.</param>
        public void Override(AutoMapping<PaymentPlan> mapping)
        {
            mapping.HasMany(x => x.Computers)
                .AsSet()
                .Inverse()
                .Cascade.SaveUpdate()
                .Access.CamelCaseField();
        }
    }
}