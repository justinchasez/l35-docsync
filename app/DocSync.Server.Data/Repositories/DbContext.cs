using System;
using System.Data;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Data base context implementation.
    /// </summary>
    public class DbContext : IDbContext
    {
        /// <summary>
        /// Commits the changes.
        /// </summary>
        public void CommitChanges()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Begins the transaction.
        /// </summary>
        /// <returns>Transaction instance.</returns>
        public IDisposable BeginTransaction()
        {
            var session = ServiceLocator.Current.GetInstance<ISession>();
            ITransaction transaction = session.BeginTransaction(IsolationLevel.ReadCommitted);
            return transaction;
        }

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        public void CommitTransaction()
        {
            var session = ServiceLocator.Current.GetInstance<ISession>();
            if (session.Transaction != null && session.Transaction.IsActive)
            {
                session.Transaction.Commit();
            }
        }

        /// <summary>
        /// Rollbacks the transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            var session = ServiceLocator.Current.GetInstance<ISession>();
            if (session.Transaction != null && session.Transaction.IsActive)
            {
                session.Transaction.Rollback();
            }
        }
    }
}