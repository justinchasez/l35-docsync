using System.Collections.Generic;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.LambdaExtensions;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Sent messages repository.
    /// </summary>
    public class SentMessagesRepository : Repository<SentMessage>, ISentMessagesRepository
    {
        /// <summary>
        /// Gets the messages paged.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List of paged messages.</returns>
        public IList<SentMessage> GetMessagesPaged(int pageNumber, int pageSize)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(SentMessage));
            criteria.AddOrder<SentMessage>(x => x.SendingDate, Order.Desc);

            criteria.SetFirstResult((pageNumber - 1) * pageSize);
            criteria.SetMaxResults(pageSize);

            var result = criteria.List<SentMessage>();
            return result;
        }

        /// <summary>
        /// Gets the messages count.
        /// </summary>
        /// <returns>Total number of messages.</returns>
        public int GetMessagesCount()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(SentMessage));
            criteria.SetProjection(Projections.RowCount());

            int result = criteria.UniqueResult<int>();
            return result;
        }
    }
}