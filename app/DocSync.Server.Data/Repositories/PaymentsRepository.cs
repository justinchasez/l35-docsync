using System.Collections.Generic;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities.Payments;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.LambdaExtensions;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Paymets repository implementation
    /// </summary>
    public class PaymentsRepository : Repository<Payment>, IPaymentsRepository
    {
        /// <summary>
        /// Gets the payments paged.
        /// </summary>
        /// <param name="pageNumber">The page number. 1 based index.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>Paged payments.</returns>
        public IList<Payment> GetPaymentsPaged(int pageNumber, int pageSize)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Payment));
            criteria.AddOrder<Payment>(x => x.PaymentDate, Order.Desc);
            criteria.SetMaxResults(pageSize);
            criteria.SetFirstResult((pageNumber - 1) * pageSize);

            return criteria.List<Payment>();
        }

        /// <summary>
        /// Gets total number of payments.
        /// </summary>
        /// <returns>Total number of all payments.</returns>
        public int GetPaymentsCount()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(Payment));
            criteria.SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>();
        }
    }
}