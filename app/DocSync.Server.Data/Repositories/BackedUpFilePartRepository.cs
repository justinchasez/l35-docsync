using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// BackedUpFileParts repository implementation.
    /// </summary>
    public class BackedUpFilePartRepository : Repository<BackedUpFilePart>, IBackedUpFilePartRepository
    {
    }
}