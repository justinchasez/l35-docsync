using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Since nearly all of the domain objects you create will have a type of int Id, this
    /// most frequently used base GenericDao leverages this assumption.  If you want an entity
    /// with a type other than int, such as string, then use
    /// <see cref="GenericDaoWithTypedId{T, IdT}"/>.
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    public class Repository<T> : RepositoryWithTypedId<T, int>, IRepository<T>
    {
    }
}
