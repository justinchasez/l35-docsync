using System.Collections.Generic;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using NHibernate.Criterion;

namespace DocSync.Server.Data.Repositories
{
    public class BackedUpFileRepository : Repository<BackedUpFile>, IBackedUpFileRepository
    {
        public IList<BackedUpFile> GetAllInClientPath(Computer computer, string clientPath)
        {
            return this.Session
                .CreateCriteria<BackedUpFile>()
                .Add(Restrictions.Eq("Computer", computer))
                .Add(Restrictions.Like("ClientPath", clientPath + "%"))
                .List<BackedUpFile>();
        }
    }
}