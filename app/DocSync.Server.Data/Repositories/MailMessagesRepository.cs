using System.Collections.Generic;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.LambdaExtensions;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Mail messages repository.
    /// </summary>
    public class MailMessagesRepository : Repository<InternalMailMessage>, IMessagesRepository
    {
        /// <summary>
        /// Gets the messages paged.
        /// </summary>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns>List of founded messages.</returns>
        public IList<InternalMailMessage> GetMessagesPaged(int pageNumber, int pageSize)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(InternalMailMessage));
            criteria.AddOrder<InternalMailMessage>(x => x.IsRead, Order.Asc);
            criteria.AddOrder<InternalMailMessage>(x => x.CreationDate, Order.Asc);

            criteria.SetMaxResults(pageSize);
            criteria.SetFirstResult(pageSize * (pageNumber - 1));

            return criteria.List<InternalMailMessage>();
        }

        /// <summary>
        /// Gets the total number of messages.
        /// </summary>
        /// <returns>Total number of messages.</returns>
        public int GetTotalNumberOfMessages()
        {
            ICriteria criteria = Session.CreateCriteria(typeof(InternalMailMessage));
            criteria.SetProjection(Projections.RowCount());
            int result = criteria.UniqueResult<int>();

            return result;
        }

        /// <summary>
        /// Gets the sorted messages paged.
        /// </summary>
        /// <param name="messagesCriteria">The messages criteria.</param>
        /// <returns>List of founded messages.</returns>
        public IList<InternalMailMessage> GetSortedMessagesPaged(SortableMessagesCriteria messagesCriteria)
        {
            ICriteria criteria = Session.CreateCriteria(typeof(InternalMailMessage));

            var expression = new Order(messagesCriteria.SortBy, messagesCriteria.SortOrder != "Descending");
            criteria.AddOrder(expression);

            criteria.SetMaxResults(messagesCriteria.PageSize);
            criteria.SetFirstResult(messagesCriteria.PageSize * (messagesCriteria.PageNumber - 1));

            return criteria.List<InternalMailMessage>();
        }
    }
}