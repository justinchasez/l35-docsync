﻿using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Data.Repositories
{
    public class FileLinksRepository : Repository<FileLink>, IFileLinksRepository
    {
    }
}
