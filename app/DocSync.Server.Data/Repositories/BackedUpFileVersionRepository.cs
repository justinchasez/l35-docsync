using System.Linq;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using NHibernate.Linq;

namespace DocSync.Server.Data.Repositories
{
    public class BackedUpFileVersionRepository : Repository<BackedUpFileVersion>, IBackedUpFileVersionRepository
    {
        /// <summary>
        /// Gets the last file version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        public virtual BackedUpFileVersion GetLastVersion(BackedUpFile file)
        {
            return (from f in Session.Linq<BackedUpFileVersion>()
                    where f.BackedUpFile.Id == file.Id &&
                          f.FileVersion == f.BackedUpFile.FileVersions.Max(x => x.FileVersion)
                    select f).SingleOrDefault();
//            computer.BytesUsed = (from fileVersion in Session.Linq<BackedUpFileVersion>()
//                                  where fileVersion.IsFinished && fileVersion.BackedUpFile.Computer.Id == computer.Id
//                                  select fileVersion.Size).Sum();
        }

        /// <summary>
        /// Gets the last finished version.
        /// </summary>
        /// <returns><see cref="BackedUpFileVersion"/> instance.</returns>
        public virtual BackedUpFileVersion GetLastFinishedVersion(BackedUpFile file)
        {
            //var finishedVersions = this.FileVersions.Where(f => f.IsFinished);
            //return (from f in finishedVersions
            //        where f.FileVersion == finishedVersions.Max(x => x.FileVersion)
            //        select f).SingleOrDefault();

            return (from f in Session.Linq<BackedUpFileVersion>()
                    where f.BackedUpFile.Id == file.Id &&
                          f.FileVersion == f.BackedUpFile.FileVersions.Max(x => x.FileVersion) &&
                          f.IsFinished
                    select f).SingleOrDefault();
        }   
    }
}