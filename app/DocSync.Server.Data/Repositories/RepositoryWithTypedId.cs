using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using NHibernate;
using NHibernate.Criterion;
using SharpArch.Core;
using SharpArch.Core.DomainModel;
using SharpArch.Core.PersistenceSupport;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Provides a fully loaded DAO which may be created in a few ways including:
    /// </summary>
    /// <typeparam name="T">Entity type.</typeparam>
    /// <typeparam name="IdT">The type of the Id.</typeparam>
    public class RepositoryWithTypedId<T, IdT> : IRepositoryWithTypedId<T, IdT>
    {
        /// <summary>
        /// Db context.
        /// </summary>
        private IDbContext dataBaseContext;

        /// <summary>
        /// Gets the db context.
        /// </summary>
        /// <value>The db context.</value>
        public virtual IDbContext DbContext
        {
            get
            {
                if (this.dataBaseContext == null)
                {
                    this.dataBaseContext = ServiceLocator.Current.GetInstance<IDbContext>();
                }

                return this.dataBaseContext;
            }
        }

        /// <summary>
        /// Gets the session.
        /// </summary>
        /// <value>The session.</value>
        protected virtual ISession Session
        {
            get
            {
                ISession session = ServiceLocator.Current.GetInstance<ISession>();
                return session;
            }
        }

        /// <summary>
        /// Gets the entity specified by id.
        /// </summary>
        /// <param name="id">The id of entity.</param>
        /// <returns>Entity instance.</returns>
        public virtual T Get(IdT id)
        {
            return this.Session.Get<T>(id);
        }

        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns>List of entities.</returns>
        public virtual IList<T> GetAll()
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(T));
            return criteria.List<T>();
        }

        /// <summary>
        /// Finds all entities specified by dictionary of equalities.
        /// </summary>
        /// <param name="propertyValuePairs">The property value pairs.</param>
        /// <returns>List of entities</returns>
        public virtual IList<T> FindAll(IDictionary<string, object> propertyValuePairs)
        {
            Check.Require(propertyValuePairs != null && propertyValuePairs.Count > 0, "propertyValuePairs was null or empty; it has to have at least one property/value pair in it");

            ICriteria criteria = this.Session.CreateCriteria(typeof(T));

            foreach (string key in propertyValuePairs.Keys)
            {
                if (propertyValuePairs[key] != null)
                {
                    criteria.Add(Expression.Eq(key, propertyValuePairs[key]));
                }
                else
                {
                    criteria.Add(Expression.IsNull(key));
                }
            }

            return criteria.List<T>();
        }

        /// <summary>
        /// Finds the one entity by equality.
        /// </summary>
        /// <param name="propertyValuePairs">The property value pairs.</param>
        /// <returns>Single entity instance</returns>
        /// <exception cref="NonUniqueResultException">When there is more results then one</exception>
        public virtual T FindOne(IDictionary<string, object> propertyValuePairs)
        {
            IList<T> foundList = this.FindAll(propertyValuePairs);

            if (foundList.Count > 1)
            {
                throw new NonUniqueResultException(foundList.Count);
            }

            if (foundList.Count == 1)
            {
                return foundList[0];
            }

            return default(T);
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public virtual void Delete(T entity)
        {
            this.Session.Delete(entity);
        }

        /// <summary>
        /// Although SaveOrUpdate _can_ be invoked to update an object with an assigned Id, you are
        /// hereby forced instead to use Save/Update for better clarity.
        /// </summary>
        /// <param name="entity">The entity to save.</param>
        /// <returns>Saved entity.</returns>
        public virtual T SaveOrUpdate(T entity)
        {
            Check.Require(!(entity is IHasAssignedId<IdT>), "For better clarity and reliability, Entities with an assigned Id must call Save or Update");

            this.Session.SaveOrUpdate(entity);
            return entity;
        }
    }
}