using System.Collections.Generic;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities.StaticPages;
using NHibernate;
using NHibernate.LambdaExtensions;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Static pages repository implementation
    /// </summary>
    public class StaticPagesRepository : IStaticPagesRepository
    {
        /// <summary>
        /// Nhibernate session
        /// </summary>
        private readonly ISession session;

        /// <summary>
        /// Initializes a new instance of the <see cref="StaticPagesRepository"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public StaticPagesRepository(ISession session)
        {
            this.session = session;
        }

        /// <summary>
        /// Gets all static pages.
        /// </summary>
        /// <returns>List of static pages</returns>
        public IList<StaticPage> GetAll()
        {
            ICriteria criteria = this.session.CreateCriteria(typeof(StaticPage));
            return criteria.List<StaticPage>();
        }

        /// <summary>
        /// Gets the page by URL.
        /// </summary>
        /// <param name="pageUrl">The page URL.</param>
        /// <returns>
        /// Static page if page founded, otherwise <see langword="null"/>
        /// </returns>
        public StaticPage GetPageByUrl(string pageUrl)
        {
            ICriteria criteria = this.session.CreateCriteria(typeof(StaticPage));
            criteria.Add<StaticPage>(x => x.PageUrl == pageUrl);
            IList<StaticPage> staticPages = criteria.List<StaticPage>();
            if (staticPages.Count > 0)
            {
                return staticPages[0];
            }

            return null;
        }

        /// <summary>
        /// Saves the or updates a static page.
        /// </summary>
        /// <param name="staticPage">The static page.</param>
        /// <returns>Saved static page</returns>
        public StaticPage SaveOrUpdate(StaticPage staticPage)
        {
            this.session.SaveOrUpdate(staticPage);
            return staticPage;
        }
    }
}