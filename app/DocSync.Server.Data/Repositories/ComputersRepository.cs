using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Computers repository implementation.
    /// </summary>
    public class ComputersRepository : Repository<Computer>, IComputersRepository
    {
        /// <summary>
        /// Sets the total bytes used for specify computer.
        /// </summary>
        public void BytesUsed(Computer computer)
        {
            //computer.BytesUsed = (from fileVersion in Session.Linq<BackedUpFileVersion>()
            //              where fileVersion.IsFinished && fileVersion.BackedUpFile.Computer.Id == computer.Id
            //              select fileVersion.Size).Sum();
        }
    }
}