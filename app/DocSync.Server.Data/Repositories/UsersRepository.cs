using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Entities.Payments;
using DocSync.Server.Core.Services;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.LambdaExtensions;
using NHibernate.Linq;
using NHibernate.Transform;

namespace DocSync.Server.Data.Repositories
{
    /// <summary>
    /// Users repository implementation
    /// </summary>
    public class UsersRepository : RepositoryWithTypedId<RegisteredUser, Guid>, IUsersRepository
    {
        /// <summary>
        /// Membership service.
        /// </summary>
        private readonly IMembershipService membershipService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersRepository"/> class.
        /// </summary>
        /// <param name="membershipService">The membership service.</param>
        public UsersRepository(IMembershipService membershipService)
        {
            this.membershipService = membershipService;
        }

        /// <summary>
        /// Gets users sources width users count
        /// </summary>
        /// <returns>Dictionary of users sources width users count</returns>
        public Dictionary<string, int> GetUsersSources()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            ISQLQuery query = this.Session.CreateSQLQuery("SELECT CASE WHEN  Comment LIKE 'Other%' OR Comment IS null THEN 'Other' ELSE CAST (Comment AS VARCHAR (4000)) END AS App_Group, COUNT(ApplicationId) AS App_Count FROM dbo.aspnet_Membership GROUP BY CASE WHEN  Comment LIKE 'Other%' OR Comment IS null THEN 'Other' ELSE CAST (Comment AS VARCHAR (4000)) END ORDER BY 2 DESC");
            IList list = query.List();

            foreach (object[] item in list)
            {
                result.Add((string)item[0], (int)item[1]);
            }

            return result;
        }

        /// <summary>
        /// Gets the signed up count.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public int GetSignedUpCount(DateTime startDate, DateTime endDate)
        {
            RegisteredUser userAlias = null;
            ICriteria usersCriteria = Session.CreateCriteria(typeof(RegisteredUser), () => userAlias);

            usersCriteria.Add<RegisteredUser>(x => x.RegistrationDate >= startDate.Date);

            usersCriteria.Add<RegisteredUser>(x => x.RegistrationDate < endDate.Date.AddDays(1));
            
            usersCriteria.SetProjection(LambdaProjection.CountDistinct<RegisteredUser>(x => x.Id));
            int result = usersCriteria.UniqueResult<int>();
            return result;
        }

        /// <summary>
        /// Gets the paid in range users count.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public int GetPaidInRangeUsersCount(DateTime startDate, DateTime endDate)
        {
            var result = (from payment in Session.Linq<Payment>()
                          where payment.PaymentDate >= startDate && payment.PaymentDate < endDate.Date.AddDays(1)
                          select payment.Computer.PaymentPlan.User.Id).Distinct();//
            return Enumerable.Count(result);
        }

        /// <summary>
        /// Gets the amount of data for trial users.
        /// </summary>
        /// <returns></returns>
        public long GetAmountOfDataForTrialUsers()
        {
            var result = (from fileVersion in Session.Linq<BackedUpFileVersion>()
                          where fileVersion.IsFinished && fileVersion.BackedUpFile.Computer.Payments.Count == 0
                          select fileVersion.Size).Sum();
            return result;
        }

        /// <summary>
        /// Gets the amount of data for paid users.
        /// </summary>
        /// <returns></returns>
        public long GetAmountOfDataForPaidUsers()
        {
            var result = (from fileVersion in Session.Linq<BackedUpFileVersion>()
                          where fileVersion.IsFinished && fileVersion.BackedUpFile.Computer.Payments.Count > 0
                          select fileVersion.Size).Sum();
            return result;
        }

        /// <summary>
        /// Gets the paid users count.
        /// </summary>
        /// <returns></returns>
        public int GetPaidUsersCount()
        {
            var result = (from payment in Session.Linq<Payment>()
                          select payment.Computer.PaymentPlan.User.Id).Distinct();
            return Enumerable.Count(result);
        }

        /// <summary>
        /// Gets the paid computers count.
        /// </summary>
        /// <returns></returns>
        public int GetPaidComputersCount()
        {
            var result = (from payment in Session.Linq<Payment>()
                          select payment.Computer.Id).Distinct();
            return Enumerable.Count(result);
        }

        public int GetTrialUsersCount(DateTime startDate, DateTime endDate)
        {
            var result = (from computer in Session.Linq<Computer>()
                          where computer.Payments.Count == 0 && computer.PayedPeriods.Min(pp => pp.StartDate) >= startDate.Date && computer.PayedPeriods.Min(pp => pp.StartDate) < endDate.Date.AddDays(1)
                          select computer.PaymentPlan.User.Id).Distinct();
            return Enumerable.Count(result);
        }

        /// <summary>
        /// Gets the user by name.
        /// </summary>
        /// <param name="name">The name of user.</param>
        /// <returns><see cref="RegisteredUser"/> instance.</returns>
        public RegisteredUser GetUserByName(string name)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(RegisteredUser));
            criteria.Add<RegisteredUser>(x => x.Name == name);

            var result = criteria.UniqueResult<RegisteredUser>();
            return result;
        }

        /// <summary>
        /// Searches the users specified by dto.
        /// </summary>
        /// <param name="dto">The dto search criteria.</param>
        /// <returns><see cref="UsersSearchResult"/> instance</returns>
        public UsersSearchResult Search(UsersSearchCriteria dto)
        {
            ICriteria usersCriteria = this.BuildCommonCriteria(dto);

            IList<RegisteredUser> registeredUsers = GetRegisteredUsers(usersCriteria, dto);

            int registeredUsersCount = GetRegisteredUsersCount(usersCriteria, dto.IsPaid);

            var result = new UsersSearchResult(dto.PageSize, dto.PageNumber, registeredUsers, registeredUsersCount);
            return result;
        }

        /// <summary>
        /// Gets the full user info with payment plan and computer with files fetched.
        /// </summary>
        /// <param name="id">The user id.</param>
        /// <returns><see cref="RegisteredUser"/> instance.</returns>
        public RegisteredUser GetFullUserInfo(Guid id)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(RegisteredUser));

            criteria.Add<RegisteredUser>(x => x.Id == id);

            criteria.SetFetchMode("PaymentPlan", FetchMode.Eager)
                    .SetFetchMode("PaymentPlan.Computers", FetchMode.Eager)
                    .SetFetchMode("PaymentPlan.Computers.Files", FetchMode.Eager)
                    .SetFetchMode("PaymentPlan.Computers.Files.FileVersions", FetchMode.Eager);
                    
            return criteria.UniqueResult<RegisteredUser>();   
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public override void Delete(RegisteredUser entity)
        {
            this.membershipService.Delete(entity.Id);
        }

        /// <summary>
        /// Gets the registered users count.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="isPaid">The is paid.</param>
        /// <returns>Total number of users for criteria</returns>
        private static int GetRegisteredUsersCount(ICriteria criteria, bool? isPaid)
        {
            var localCriteria = (ICriteria) criteria.Clone();

            localCriteria.SetProjection(LambdaProjection.CountDistinct<RegisteredUser>(x => x.Id));
            int result = localCriteria.UniqueResult<int>();
            return result;
        }

        /// <summary>
        /// Gets the registered users.
        /// </summary>
        /// <param name="usersCriteria">The users criteria.</param>
        /// <param name="dto">The search dto.</param>
        /// <returns>List of users that match criteria with sorting and paging</returns>
        private static IList<RegisteredUser> GetRegisteredUsers(ICriteria usersCriteria, UsersSearchCriteria dto)
        {
            var localCriteria = (ICriteria)usersCriteria.Clone();

            if (!String.IsNullOrEmpty(dto.SortBy) && !String.IsNullOrEmpty(dto.SortOrder))
            {
                var expression = new Order(dto.SortBy, dto.SortOrder != "Descending");
                localCriteria.AddOrder(expression);
            }
            else
            {
                localCriteria.AddOrder<RegisteredUser>(x => x.Name, Order.Asc);
            }

            localCriteria.SetProjection(
                Projections.Distinct(
                    Projections.ProjectionList().Add(LambdaProjection.Property<RegisteredUser>(x => x.Id), "Id")
                                                .Add(LambdaProjection.Property<RegisteredUser>(x => x.Name), "Name")
                                                .Add(LambdaProjection.Property<RegisteredUser>(x => x.RegistrationDate), "RegistrationDate")
                                                .Add(LambdaProjection.Property<RegisteredUser>(x => x.Email), "Email")));

            localCriteria.SetMaxResults(dto.PageSize);
            localCriteria.SetFirstResult(dto.PageSize * (dto.PageNumber - 1));

            localCriteria.SetResultTransformer(Transformers.AliasToBean<RegisteredUser>());

            return localCriteria.List<RegisteredUser>();
        }

        /// <summary>
        /// Builds the common criteria.
        /// </summary>
        /// <param name="dto">The search dto.</param>
        /// <returns>Users search criteria that has no aggregations etc.</returns>
        private ICriteria BuildCommonCriteria(UsersSearchCriteria dto)
        {
            RegisteredUser userAlias = null;
            var usersCriteria = Session.CreateCriteria(typeof(RegisteredUser), () => userAlias);

            if (!string.IsNullOrEmpty(dto.Email))
            {
                usersCriteria.Add(SqlExpression.Like<RegisteredUser>(x => x.Email, dto.Email, MatchMode.Anywhere));
            }

            if (dto.RegistrationStartDate.HasValue)
            {
                usersCriteria.Add<RegisteredUser>(x => x.RegistrationDate >= dto.RegistrationStartDate.Value.Date);
            }

            if (dto.RegistrationEndDate.HasValue)
            {
                usersCriteria.Add<RegisteredUser>(x => x.RegistrationDate < dto.RegistrationEndDate.Value.Date.AddDays(1));
            }

            if (dto.ComputersNumber.HasValue)
            {
                ICriteria criteria = usersCriteria.CreateCriteria<RegisteredUser>(x => x.PaymentPlan);

                DetachedCriteria computersCount = DetachedCriteria.For<Computer>();
                computersCount.SetProjection(Projections.RowCount());
                computersCount.CreateCriteria<Computer>(x => x.PaymentPlan)
                                    .Add<PaymentPlan>(x => x.User.Id == userAlias.Id);

                int value = dto.ComputersNumber.Value;
                criteria.Add(Subqueries.Eq(value, computersCount));
            }

            DetachedCriteria payedAmout = DetachedCriteria.For<Payment>();
            payedAmout.SetProjection(LambdaProjection.Sum<Payment>(x => x.Amount));
            payedAmout
                .CreateCriteria<Payment>(x => x.Computer)
                .CreateCriteria<Computer>(x => x.PaymentPlan)
                    .Add<PaymentPlan>(x => x.User.Id == userAlias.Id);

            if (dto.MinPayedMoney.HasValue)
            {
                usersCriteria.Add(Subqueries.Lt(dto.MinPayedMoney, payedAmout));
            }

            if (dto.MaxPayedMoney.HasValue)
            {
                usersCriteria.Add(Subqueries.Gt(dto.MaxPayedMoney, payedAmout));
            }

            if (dto.IsPaid != null)
            {
                if (dto.IsPaid.Value)
                {
                    usersCriteria.Add(Subqueries.Lt(0, payedAmout));
                }
                else
                {
                    DetachedCriteria payedAmount = DetachedCriteria.For<Payment>();
                    payedAmount.SetProjection(LambdaProjection.Count<Payment>(x => x.Amount));
                    payedAmount
                        .CreateCriteria<Payment>(x => x.Computer)
                        .CreateCriteria<Computer>(x => x.PaymentPlan)
                            .Add<PaymentPlan>(x => x.User.Id == userAlias.Id);

                    usersCriteria.Add(Subqueries.Gt(1, payedAmount));
                }
            }

            return usersCriteria;
        }
    }
}