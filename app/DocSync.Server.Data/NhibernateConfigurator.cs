using DocSync.Server.Data.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace DocSync.Server.Data
{
    /// <summary>
    /// Used to configure NHibernate
    /// </summary>
    public class NhibernateConfigurator
    {
        /// <summary>
        /// Creates the session factory.
        /// </summary>
        /// <returns>Configured <see cref="ISessionFactory"/> instance </returns>
        public ISessionFactory CreateSessionFactory()
        {
            var modelGenerator = new AutoPersistenceModelGenerator();
            //modelGenerator.Generate().WriteMappingsTo("D:/mappings");
            FluentConfiguration configuration = Fluently.Configure()
                                                        .Database(MsSqlConfiguration.MsSql2008.ConnectionString(cs => cs.FromConnectionStringWithKey("dbConnection")))
                                                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
                                                        .Mappings(m => m.AutoMappings.Add(modelGenerator.Generate()));

            return configuration.BuildSessionFactory();
        }
    }
}