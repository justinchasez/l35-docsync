using System;
using Castle.Facilities.WcfIntegration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using DocSync.Server.ServiceLocation;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Global application class.
    /// </summary>
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <value>The container.</value>
        public static IWindsorContainer Container { get; private set; }

        /// <summary>
        /// Handles the Start event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Start(object sender, EventArgs e)
        {
            Container = new WindsorContainer();

            Container.AddFacility<WcfFacility>();
            Container.Register(Component.For<IUsersService>().ImplementedBy<UsersService>().ActAs(new DefaultServiceModel().Hosted()).LifeStyle.PerWcfOperation());
            Container.Register(Component.For<IComputersService>().ImplementedBy<ComputersService>().ActAs(new DefaultServiceModel().Hosted()).LifeStyle.PerWcfOperation());
            Container.Register(Component.For<IFilesService>().ImplementedBy<FilesService>().ActAs(new DefaultServiceModel().Hosted()).LifeStyle.PerWcfOperation());

            ServiceLocatorInitializer.AddComponentsForServicesTo(Container);

            //this.collectorTimer = new Timer(o =>
            //                                    {
            //                                        GC.Collect();
            //                                        GC.WaitForPendingFinalizers();
            //                                        GC.Collect();
            //                                    },
            //                                    null,
            //                                    CollectorTimerInterval,
            //                                    CollectorTimerInterval);
        }

        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            var error = Server.GetLastError();
        }
    }
}