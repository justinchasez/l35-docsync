using System.IdentityModel.Claims;
using System.IdentityModel.Policy;

namespace DocSync.Server.Services.Authorization
{
    /// <summary>
    /// Authorization policy
    /// </summary>
    public class AuthorizationPolicy : IAuthorizationPolicy
    {
        /// <summary>
        /// Evaluates whether a user meets the requirements for this authorization policy.
        /// </summary>
        /// <param name="evaluationContext">An <see cref="T:System.IdentityModel.Policy.EvaluationContext"/> that contains the claim set that the authorization policy evaluates.</param>
        /// <param name="state">A <see cref="T:System.Object"/>, passed by reference that represents the custom state for this authorization policy.</param>
        /// <returns>
        /// false if the <see cref="M:System.IdentityModel.Policy.IAuthorizationPolicy.Evaluate(System.IdentityModel.Policy.EvaluationContext,System.Object@)"/> method for this authorization policy must be called if additional claims are added by other authorization policies to <paramref name="evaluationContext"/>; otherwise, true to state no additional evaluation is required by this authorization policy.
        /// </returns>
        public bool Evaluate(EvaluationContext evaluationContext, ref object state)
        {
			//HttpContext context = HttpContext.Current;

			//if (context != null)
			//{
			//    evaluationContext.Properties["Principal"] = context.User;
			//    evaluationContext.Properties["Identities"] = new List<IIdentity> { context.User.Identity };
			//}

            return true;
        }

        /// <summary>
        /// Gets a claim set that represents the issuer of the authorization policy.
        /// </summary>
        /// <value></value>
        /// <returns>A <see cref="T:System.IdentityModel.Claims.ClaimSet"/> that represents the issuer of the authorization policy.</returns>
        public ClaimSet Issuer
        {
            get { return ClaimSet.System; }
        }

        /// <summary>
        /// Gets a string that identifies this authorization component.
        /// </summary>
        /// <value></value>
        /// <returns>A string that identifies this authorization component.</returns>
        public string Id
        {
            get { return "HttpContextPrincipalPolicy"; }
        }
    }
}