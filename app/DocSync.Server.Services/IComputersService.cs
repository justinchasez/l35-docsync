using System.Collections.Generic;
using System.ServiceModel;
using DocSync.Server.Services.Dtos;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Computers management service interface.
    /// </summary>
    [ServiceContract]
    public interface IComputersService
    {
        /// <summary>
        /// Tests this instance.
        /// </summary>
        /// <returns>true true true</returns>
        [OperationContract]
        bool Test();

        /// <summary>
        /// Gets the computers for current user.
        /// </summary>
        /// <returns>List of computers.</returns>
        [OperationContract]
        List<ComputerInfo> GetComputers();

        /// <summary>
        /// Adds the computer to current user.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>if empty string, then trial computer was added. Otherwise link on click bank to perform payment.</returns>
        [OperationContract]
        string AddComputer(ComputerInfo computerInfo);

        /// <summary>
        /// Adds the computer key.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        void AddComputerKey(AddComputerKeyInputs inputs);

        /// <summary>
        /// Gets the computer users.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>Computer users list</returns>
        [OperationContract]
        ComputerUsersResult GetComputerUsers(ComputerInfo computerInfo);

        /// <summary>
        /// Syncronize windows users
        /// </summary>
        /// <param name="syncronizeUsersInputs">List of windows users</param>
        /// <returns>Result of syncronization</returns>
        [OperationContract]
        SyncronizeUsersResult SyncronizeUsers(SyncronizeUsersInputs syncronizeUsersInputs);

        /// <summary>
        /// Updates the computer GUID.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        [OperationContract]
        void UpdateComputerGuid(UpdateComputerGuidInputs inputs);
    }
}
