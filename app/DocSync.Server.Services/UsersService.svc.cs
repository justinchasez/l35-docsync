using System;
using System.ServiceModel.Activation;
using System.Web.Security;
using DocSync.Server.Core.Services;
using DocSync.Server.Services.Dtos;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Registration service.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UsersService : IUsersService, IDisposable
    {
        /// <summary>
        /// Membership service.
        /// </summary>
        private readonly IMembershipService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersService"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public UsersService(IMembershipService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Registers the specified by inputs user.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>
        /// 	<see cref="UserRegistrationResult"/> instance.
        /// </returns>
        public UserRegistrationResult Register(UserRegistrationInputs inputs)
        {
            MembershipCreateStatus createStatus = this.service.CreateUser(inputs.Email, inputs.Password, inputs.Email, inputs.Source);
            var registrationResult = new UserRegistrationResult
                                                            {
                                                                CreateStatus = createStatus
                                                            };
            if (registrationResult.CreateStatus != MembershipCreateStatus.Success)
            {
                registrationResult.ErrorMessage = AccountValidation.ErrorCodeToString(registrationResult.CreateStatus);
            }

            return registrationResult;
        }

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns>
        /// 	<see langword="true"/> if user credentials are valid, otherwise <see langword="false"/>
        /// </returns>
        public bool ValidateUser(string name, string password)
        {
            return this.service.ValidateUser(name, password);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Global.Container.Release(this);
        }
    }
}
