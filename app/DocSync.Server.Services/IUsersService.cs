using System.ServiceModel;
using DocSync.Server.Services.Dtos;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Registration service.
    /// </summary>
    [ServiceContract]
    public interface IUsersService
    {
        /// <summary>
        /// Registers the specified by inputs user.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns><see cref="UserRegistrationResult"/> instance.</returns>
        [OperationContract]
        UserRegistrationResult Register(UserRegistrationInputs inputs);

        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="name">The user name.</param>
        /// <param name="password">The password.</param>
        /// <returns><see langword="true"/> if user credentials are valid, otherwise <see langword="false"/></returns>
        [OperationContract]
        bool ValidateUser(string name, string password);
    }
}
