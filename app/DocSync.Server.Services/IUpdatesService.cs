using System.ServiceModel;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Updates service
    /// </summary>
    [ServiceContract]
    public interface IUpdatesService
    {
        /// <summary>
        /// Gets the last version of client app.
        /// </summary>
        /// <returns>The last version of client app.</returns>
        [OperationContract]
        string GetLastVersionOfClientApp();

        /// <summary>
        /// Gets the link to last version.
        /// </summary>
        /// <returns>Link to last version of client app.</returns>
        [OperationContract]
        string GetLinkToLastVersion();
    }
}
