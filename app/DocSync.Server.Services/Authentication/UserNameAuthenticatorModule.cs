using System;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;

namespace DocSync.Server.Services.Authentication
{
    /// <summary>
    /// Http module to authenticate user.
    /// </summary>
    public class UserNameAuthenticatorModule : IHttpModule
    {
        /// <summary>
        /// The sync object for log authentication
        /// </summary>
        private static readonly object syncObject = new object();

        /// <summary>
        /// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Inits the specified application.
        /// </summary>
        /// <param name="application">The application.</param>
        public void Init(HttpApplication application)
        {
            application.AuthenticateRequest += this.OnAuthenticateRequest;
            application.EndRequest += this.OnEndRequest;
        }

        /// <summary>
        /// Called when authenticate request.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnAuthenticateRequest(object source, EventArgs eventArgs)
        {
            HttpApplication app = (HttpApplication)source;
            //the Authorization header is checked if present 
            string authHeader = app.Request.Headers["Authorization"];
        	//return;
            if (!app.Request.RawUrl.Contains("UsersService"))
            {
                if (!string.IsNullOrEmpty(authHeader))
                {
                    string authStr = app.Request.Headers["Authorization"];

                    if (string.IsNullOrEmpty(authStr))
                    {
                        // No credentials; anonymous request 
                        return;
                    }

                    authStr = authStr.Trim();
                    if (authStr.IndexOf("Basic", 0) != 0)
                    {
                        //header not correct we do not authenticate 
                        return;
                    }

                    authStr = authStr.Trim();
                    string encodedCredentials = authStr.Substring(6);
                    byte[] decodedBytes = Convert.FromBase64String(encodedCredentials);
                    string s = new ASCIIEncoding().GetString(decodedBytes);
                    string[] userPass = s.Split(new[] { ':' });
                    string username = userPass[0];
                    string password = userPass[1];

                    //the user is validated against the SqlMemberShipProvider 
                    //If it is validated then the roles are retrieved from the 
                    //role provider and a generic principal is created 
                    //the generic principal is assigned to the user context 
                    // of the application 

                    if (Membership.ValidateUser(username, password))
                    {
                        //System.Diagnostics.Trace.WriteLine((string.Format("Access alow: User not valid: name={0}; password={1}", username, password)));
                        string[] roles = Roles.GetRolesForUser(username);
                        app.Context.User = new GenericPrincipal(new GenericIdentity(username, "Membership Provider"), roles);
                    }
                    else
                    {
                        //System.Diagnostics.Trace.WriteLine((string.Format("Access deny: User not valid: name={0}; password={1}", username, password)));

                        this.DenyAccess(app);
                        return;
                    }
                }
                else
                {
                    //the authorization header is not present 
                    //the status of response is set to 401 and it ended 
                    //the end request will check if it is 401 and add 
                    //the authentication header so the client knows 
                    //it needs to send credentials to authenticate 
                    //app.Response.StatusCode = 401;
                    //app.Response.End();
                    //System.Diagnostics.Trace.WriteLine("Access deny: No auth header");

                    this.DenyAccess(app);
                    //context.Response.StatusCode = 401; 
                    //context.Response.End(); 
                }
            }
        }

        /// <summary>
        /// Called when end request.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnEndRequest(object source, EventArgs eventArgs)
        {
            if (HttpContext.Current.Response.StatusCode == 401)
            {
                //if the status is 401 the WWW-Authenticated is added to  
                //the response so client knows it needs to send credentials  
                HttpContext context = HttpContext.Current;
                context.Response.StatusCode = 401;
                context.Response.AddHeader("WWW-Authenticate", "Basic Realm");
            }
        }

        /// <summary>
        /// Denies the access.
        /// </summary>
        /// <param name="app">The http app.</param>
        private void DenyAccess(HttpApplication app)
        {
            app.Response.StatusCode = 401;
            app.Response.StatusDescription = "Access Denied";

            // error not authenticated 
            app.Response.Write("401 Access Denied");

            app.CompleteRequest();
        }
    }
}
