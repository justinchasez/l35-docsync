using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using DocSync.Server.Core.Additional;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Services;
using DocSync.Server.Core.Settings;
using DocSync.Server.Services.Dtos;
using SharpArch.Core;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Computers Service.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ComputersService : IComputersService, IDisposable
    {
        /// <summary>
        /// Users repository.
        /// </summary>
        private readonly IUsersRepository usersRepository;

        /// <summary>
        /// Computers repository.
        /// </summary>
        private readonly IComputersRepository computersRepository;

        /// <summary>
        /// Product settings container.
        /// </summary>
        private readonly IProductSettings productSettings;

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        /// <value>The name of the current user.</value>
        protected virtual string CurrentUserName
        {
            get
            {
                return System.Web.HttpContext.Current.User.Identity.Name;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputersService"/> class.
        /// </summary>
        /// <param name="usersRepository">The users repository.</param>
        /// <param name="cleverBridgeService">The click bank service.</param>
        /// <param name="productSettings">The product settings.</param>
        /// <param name="computersRepository">The computers repository.</param>
        public ComputersService(IUsersRepository usersRepository, IProductSettings productSettings, IComputersRepository computersRepository)
        {
            this.usersRepository = usersRepository;
            this.productSettings = productSettings;
            this.computersRepository = computersRepository;
        }

        /// <summary>
        /// Tests this instance.
        /// </summary>
        /// <returns>true true true</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public bool Test()
        {
            return true;
        }

        /// <summary>
        /// Gets the computers.
        /// </summary>
        /// <returns>List of computers for current user.</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public List<ComputerInfo> GetComputers()
        {
            var user = this.usersRepository.GetUserByName(this.CurrentUserName);
            return user.PaymentPlan.Computers.Select(computer => new ComputerInfo(computer, this.computersRepository)).ToList();
        }

        /// <summary>
        /// Adds the computer to current user.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>
        /// if empty string, then trial computer was added. Otherwise link on click bank to perform payment.
        /// </returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public string AddComputer(ComputerInfo computerInfo)
        {
            string result;
            
            var user = this.usersRepository.GetUserByName(this.CurrentUserName);

            if (user.PaymentPlan.Computers.IsEmpty)
            {
                int? backupLimit = this.productSettings.GetTrialBackupLimit();
                user.PaymentPlan.AddTrialComputer(computerInfo.Name, backupLimit, computerInfo.ManualEncriptionState);
                result = String.Empty;
            }
            else
            {
				// create payment request
	            result = string.Empty;
            }

            return result;
        }

        /// <summary>
        /// Adds the computer key.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        public void AddComputerKey(AddComputerKeyInputs inputs)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the computer users.
        /// </summary>
        /// <param name="computerInfo">The computer info.</param>
        /// <returns>Computer users list</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public ComputerUsersResult GetComputerUsers(ComputerInfo computerInfo)
        {
            Computer computer = this.computersRepository.Get(computerInfo.Id);

            Check.Require(computer != null, "Computer with such Id doesnt exists. Passed id : " + computerInfo.Id);

            ComputerUsersResult result = new ComputerUsersResult();

            foreach (ComputerLocalUser localUser in computer.LocalUsers)
            {
                int documentsFileCount =
                    computer.Files.Where(f => f.ClientPath.StartsWith(localUser.DocumentsFolder)).Count();

                int desktopFileCount =
                    computer.Files.Where(f => f.ClientPath.StartsWith(localUser.DesktopFolder)).Count();

                if ((documentsFileCount > 0) || (desktopFileCount > 0))
                {
                    result.Users.Add(new SyncronizeUsersEntry
                                         {
                                             DesktopPath = localUser.DesktopFolder,
                                             DocumentsPath = localUser.DocumentsFolder,
                                             UserName = localUser.UserName,
                                             Sid = localUser.UserSid
                                         });
                }
            }

            return result;
        }

        /// <summary>
        /// Syncronize windows users
        /// </summary>
        /// <param name="syncronizeUsersInputs">List of windows users</param>
        /// <returns>Result of syncronization</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public SyncronizeUsersResult SyncronizeUsers(SyncronizeUsersInputs syncronizeUsersInputs)
        {
            Computer currentComputer = this.computersRepository.Get(syncronizeUsersInputs.Computer.Id);

            if (currentComputer != null && 
                currentComputer.LocalComputerGuid != null && 
                currentComputer.LocalComputerGuid.Trim().Equals(syncronizeUsersInputs.ComputerGuid))
            {
                bool needUpdate = false;

                foreach (SyncronizeUsersEntry user in syncronizeUsersInputs.UsersList)
                {
                    ComputerLocalUser localUser = currentComputer.LocalUsers.Where(x => x.UserSid == user.Sid).FirstOrDefault();

                    if (localUser == null)
                    {
                        currentComputer.LocalUsers.Add(new ComputerLocalUser(currentComputer)
                                                           {
                                                               UserName = user.UserName,
                                                               UserSid = user.Sid,
                                                               DesktopFolder = user.DesktopPath,
                                                               DocumentsFolder = user.DocumentsPath
                                                           });
                        needUpdate = true;
                    }
                    else if (!localUser.UserName.Equals(user.UserName) ||
                             !localUser.DesktopFolder.Equals(user.DesktopPath) ||
                             !localUser.DocumentsFolder.Equals(user.DocumentsPath))
                    {
                        localUser.UserName = user.UserName;
                        localUser.DesktopFolder = user.DesktopPath;
                        localUser.DocumentsFolder = user.DocumentsPath;
                        needUpdate = true;
                    }
                }

                List<ComputerLocalUser> usersForDelete = new List<ComputerLocalUser>();

                foreach (ComputerLocalUser user in currentComputer.LocalUsers)
                {
                    if (syncronizeUsersInputs.UsersList.Where(x => x.Sid == user.UserSid).FirstOrDefault() == null)
                    {
                        if (currentComputer.Files.Where(x => x.ClientPath.StartsWith(user.DesktopFolder)).Count() == 0 &&
                            currentComputer.Files.Where(x => x.ClientPath.StartsWith(user.DocumentsFolder)).Count() == 0)
                        {
                            usersForDelete.Add(user);
                        }
                    }
                }

                foreach (ComputerLocalUser userForDelete in usersForDelete)
                {
                    needUpdate = true;
                    currentComputer.LocalUsers.Remove(userForDelete);
                }

                if (needUpdate)
                {
                    this.computersRepository.SaveOrUpdate(currentComputer);
                }

                return new SyncronizeUsersResult { IsSyncronized = true };
            }

            return new SyncronizeUsersResult { IsSyncronized = false };
        }

        /// <summary>
        /// Updates the computer GUID.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        public void UpdateComputerGuid(UpdateComputerGuidInputs inputs)
        {
            Computer computer = this.computersRepository.Get(inputs.ComputerId);

            Check.Require(computer != null, "Computer with such Id doesnt exists. Passed id : " + inputs.ComputerId);

            computer.LocalComputerGuid = inputs.ComputerGuid;
            this.computersRepository.SaveOrUpdate(computer);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Global.Container.Release(this);
        }
    }
}
