using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Activation;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;
using DocSync.Server.Core.Services;
using DocSync.Server.Services.Dtos;
using NHibernate;
using SharpArch.Core;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Files service. 
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class FilesService : IFilesService, IDisposable
    {
        #region Private Fields
        /// <summary>
        /// Backed up files repository.
        /// </summary>
        private readonly IComputersRepository computersRepository;

        /// <summary>
        /// Backed up files parts repository.
        /// </summary>
        private readonly IBackedUpFilePartRepository backedUpFilePartsRepository;

        /// <summary>
        /// Backed up files repository.
        /// </summary>
        private readonly IBackedUpFileRepository backedUpFileRepository;

        /// <summary>
        /// The s3 service.
        /// </summary>
        private readonly IS3Service simpleStorageService;

        /// <summary>
        /// The file links repository
        /// </summary>
        private readonly IFileLinksRepository fileLinksRepository;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="FilesService"/> class.
        /// </summary>
        /// <param name="computersRepository">The backed up file repository.</param>
        /// <param name="simpleStorageService">The s3 service.</param>
        /// <param name="backedUpFilePartsRepository">The backed up file parts repository.</param>
        /// <param name="backedUpFileRepository">the backed up files repository</param>
        /// <param name="fileLinksRepository">The file links repository.</param>
        public FilesService(IComputersRepository computersRepository, IS3Service simpleStorageService, IBackedUpFilePartRepository backedUpFilePartsRepository, IBackedUpFileRepository backedUpFileRepository, IFileLinksRepository fileLinksRepository)
        {
            this.computersRepository = computersRepository;
            this.simpleStorageService = simpleStorageService;
            this.backedUpFilePartsRepository = backedUpFilePartsRepository;
            this.backedUpFileRepository = backedUpFileRepository;
            this.fileLinksRepository = fileLinksRepository;
        }

        #region Interface Implementation

        /// <summary>
        /// Backups the specified by backup inputs file.
        /// </summary>
        /// <param name="backupFileInputs">The backup inputs.</param>
        /// <returns><see cref="BackupFileResult"/> instance.</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public BackupFileResult BackupFile(BackupFileInputs backupFileInputs)
        {
            Check.Require(!string.IsNullOrEmpty(backupFileInputs.File), "Provide file name to backup data");

            Computer computer = this.computersRepository.Get(backupFileInputs.ComputerId);
            this.computersRepository.BytesUsed(computer);

            Check.Require(computer != null, "Computer with such Id doesnt exists. Passed id : " + backupFileInputs.ComputerId);

            if (computer.GetEndDate().Date <= DateTime.Now.Date)
            {
                return new BackupFileResult(computer) { IsFinished = false, IsComputerExpired = true };
            }

            long? freeSpace = computer.FreeSpace;
            if (freeSpace != null)
            {
                if (freeSpace.Value < backupFileInputs.Size)
                {
                    return new BackupFileResult(computer) { IsFinished = false, IsLimitReached = true };
                }
            }

            BackedUpFile backedUpFile = this.GetFileByName(computer, backupFileInputs.FileOld);
//            computer.GetFileNamed(backupFileInputs.FileOld));

            if (backedUpFile == null)
            {
                // file was never backed up before
                this.BackupNewFile(backupFileInputs, computer);
                return new BackupFileResult(computer)
                           {
                               IsFinished = false,
                               VersionsCount = 1
                           };
            }

            int versionsCount = backedUpFile.FileVersions.Count;

            if (backupFileInputs.CreateCopy && versionsCount > 0)
            {
                this.CreateCopyOfFile(backupFileInputs);
                return new BackupFileResult(computer)
                           {
                               IsFinished = true,
                               VersionsCount = backedUpFile.FileVersions.Count
                           };
            }

            BackedUpFileVersion lastVersion = backedUpFile.GetLastVersion();

            if (lastVersion.FileHash == backupFileInputs.FileHash)
            {
                // rename file in DB
                if (backupFileInputs.Rename)
                {
                    backedUpFile.ClientPath = backupFileInputs.File;
                    this.computersRepository.SaveOrUpdate(computer);
                }

                if (!lastVersion.IsFinished)
                {
                    // file backuping is not finished yet. Client should continue upload

                    int finishedPartsNumber = lastVersion.Parts.Count;
                    return new BackupFileResult(computer)
                               {
                                   PartsCount = finishedPartsNumber,
                                   IsFinished = false,
                                   VersionsCount = backedUpFile.FileVersions.Count
                               };
                }

                // file upload is finished, and file has not changed since then
                return new BackupFileResult(computer)
                           {
                               IsFinished = true,
                               VersionsCount = backedUpFile.FileVersions.Count
                           };
            }

            if (!lastVersion.IsFinished)
            {
                backedUpFile.FileVersions.Remove(lastVersion);
                this.simpleStorageService.DeleteFileVersion(lastVersion);
            }

            this.CreateNewFileVersion(backupFileInputs, backedUpFile);

            return new BackupFileResult(computer)
                       {
                           IsFinished = false,
                           VersionsCount = backedUpFile.FileVersions.Count
                       };
        }
        
        /// <summary>
        /// Backups the part.
        /// </summary>
        /// <param name="backupPartInputs">The backup part inputs.</param>
        /// <returns>Backup part result.</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public BackupPartResult BackupPart(BackupPartInputs backupPartInputs)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Parts the finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The inputs.</param>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public void BackupPartFinished(PartBackupFinishedInputs backupFinishedInputs)
        {
            Check.Require(!string.IsNullOrEmpty(backupFinishedInputs.File), "Provide file name to backup");

            Computer computer = this.computersRepository.Get(backupFinishedInputs.ComputerId);

            Check.Require(computer != null, "Computer id provided doesn't exist");

            BackedUpFile fileNamed = this.GetFileByName(computer, backupFinishedInputs.File);
            
            Check.Require(fileNamed != null, "There is no file witch such name on specified computer");

            BackedUpFileVersion backedUpFile = fileNamed.GetLastVersion();

            BackedUpFilePart part = backedUpFile.GetLastPart();

            BackedUpFilePart backedUpFilePart = new BackedUpFilePart(backedUpFile)
                                                    {
                                                        Length = backupFinishedInputs.PartLength,
                                                        S3FileId = backupFinishedInputs.S3PartId,
                                                        Number = part != null ? part.Number + 1 : 1
                                                    };
            backedUpFile.Parts.Add(backedUpFilePart);
        }

        /// <summary>
        /// Files the backup finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The backup finished inputs.</param>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public void BackupFileFinished(FileBackupFinishedInputs backupFinishedInputs)
        {
            Computer computer = this.computersRepository.Get(backupFinishedInputs.ComputerId);

            Check.Require(computer != null, "Invalid computer id passed");

            BackedUpFile backedUpFile = this.GetFileByName(computer, backupFinishedInputs.File);
            BackedUpFileVersion backedUpFileVersion = backedUpFile.GetLastVersion();
            backedUpFileVersion.IsFinished = true;
            backedUpFileVersion.FinisedDate = DateTime.Now;
        }

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="restoreFileInputs">The restore file inputs.</param>
        /// <returns><see cref="RestoreFileResult"/> instance.</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public RestoreFileResult RestoreFile(RestoreFileInputs restoreFileInputs)
        {
            Check.Require(!String.IsNullOrEmpty(restoreFileInputs.File), "File path is required");

            Computer computer = this.computersRepository.Get(restoreFileInputs.ComputerId);

            Check.Require(computer != null, "Provide Id of existing computer");

            if (computer.GetEndDate().Date < DateTime.Now.Date)
            {
                return new RestoreFileResult { IsComputerExpired = true };
            }

            BackedUpFile backedUpFile = this.GetFileByName(computer, restoreFileInputs.File);

            Check.Require(backedUpFile != null, "Requested file was not founded");

            BackedUpFileVersion backedUpFileVersion;
            if (restoreFileInputs.Version < 0)
            {
                backedUpFileVersion = backedUpFile.GetLastVersion();
            }
            else
            {
                backedUpFileVersion = backedUpFile.GetSpecificVersion(restoreFileInputs.Version);
            }

            Check.Require(backedUpFileVersion != null, "Requested file version was not founded");

            var result = new RestoreFileResult
                             {
                                 FileSize = backedUpFileVersion.Size,
                                 FilePath = backedUpFile.ClientPath
                             };

            foreach (BackedUpFilePart backedUpFilePart in backedUpFileVersion.Parts)
            {
                FilePartDto dto = new FilePartDto
                                      {
                                          S3PartId = backedUpFilePart.S3FileId,
                                          Number = backedUpFilePart.Number,
                                          Length = backedUpFilePart.Length
                                      };
                result.FileParts.Add(dto);
            }

            return result;
        }

        /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <param name="fileInfoInputs">The file info inputs.</param>
        /// <returns><see cref="FileInfoDto"/> instance.</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public FileInfoDto GetFileInfo(GetFileInfoInputs fileInfoInputs)
        {
            Computer computer = this.computersRepository.Get(fileInfoInputs.ComputerId);
            Check.Require(computer != null, "Provide Id of existing computer");
            BackedUpFile backedUpFile = this.GetFileByName(computer, fileInfoInputs.File);
            Check.Require(backedUpFile != null, "Requested file was not founded");

            var result = new FileInfoDto();
            foreach (var backedUpFileVersion in backedUpFile.FileVersions)
            {
                var fileVersionDto = new FileVersionDto
                                         {
                                             VersionNumber = backedUpFileVersion.FileVersion,
                                             FinishedDate = backedUpFileVersion.FinisedDate ?? DateTime.MinValue,
                                             LastSavedDate = backedUpFileVersion.LastSavedOn ?? DateTime.MinValue,
                                             FileSize = backedUpFileVersion.Size
                                         };
                result.FileVersions.Add(fileVersionDto);
            }

            return result;
        }

        /// <summary>
        /// Gets all files from computer.
        /// </summary>
        /// <param name="inputs">The GetAllFilesFromComputer inputs.</param>
        /// <returns>Files list.</returns>
        // [PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public ComputerFilesDto GetAllFilesFromComputer(GetAllFilesFromComputerInputs inputs)
        {
        	//var session = Global.Container.Resolve<ISession>();
             Computer computer = this.computersRepository.Get(inputs.ComputerId);
             Check.Require(computer != null, "Provide Id of existing computer");
            //var allFiles = new List<FileInfoDto>();
            ComputerFilesDto result = new ComputerFilesDto();

            switch (inputs.FilesCategory)
            {
                case SelectFilesMode.All:
                    {
                        foreach (BackedUpFile file in computer.Files)
                        {
                            var versions = file.FileVersions.ToList();
                            var version = (from f in versions
                                           where f.FileVersion == versions.Max(x => x.FileVersion)
                                           select f).SingleOrDefault();
                            result.FilesInfo.Add(new FileInfoDto(file, version));
                        }

                        break;
                    }

                case SelectFilesMode.Documents:
                    {
                        var localUsers = computer.LocalUsers.Where(u => u.UserSid == inputs.UserSid);
                        if (localUsers.Count() > 0)
                        {
                            var localUser = localUsers.First();
                            var files =
                                computer.Files.Where(
                                    f =>
                                    f.ClientPath.StartsWith(localUser.DocumentsFolder) ||
                                    f.ClientPath.StartsWith(localUser.DesktopFolder));

                            result.FilesInfo.AddRange(files.Select(f => new FileInfoDto(f)));
                        }

                        break;
                    }

                case SelectFilesMode.WithoutDocuments:
                    {
                        List<string> pathList = new List<string>();
                        foreach (ComputerLocalUser localUser in computer.LocalUsers)
                        {
                            pathList.Add(localUser.DesktopFolder);
                            pathList.Add(localUser.DocumentsFolder);
                        }

                        foreach (BackedUpFile file in computer.Files)
                        {
                            int fromFoldersCount = pathList.Where(p => file.ClientPath.StartsWith(p)).Count();
                            if (fromFoldersCount == 0)
                            {
                                result.FilesInfo.Add(new FileInfoDto(file));
                            }
                        }

                        break;
                    }
            }

            return result;
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="inputs">The file inputs.</param>
        /// <returns>
        /// [true] if the removal was successful, otherwise - [false]
        /// </returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public RemovingFileResult RemoveFile(GetFileInfoInputs inputs)
        {
            Computer computer = this.computersRepository.Get(inputs.ComputerId);
            Check.Require(computer != null, "Provide Id of existing computer");
            BackedUpFile backedUpFile = this.GetFileByName(computer, inputs.File);
            Check.Require(backedUpFile != null, "Requested file was not founded");

            try
            {
                foreach (BackedUpFileVersion backedUpFileVersion in backedUpFile.FileVersions)
                {
                    if (!this.DuplicatePartsExists(backedUpFileVersion))
                    {
                        this.simpleStorageService.DeleteFileVersion(backedUpFileVersion);                        
                    }
                }

                computer.Files.Remove(backedUpFile);

                return new RemovingFileResult { IsSuccess = true };
            }
            catch
            {
                return new RemovingFileResult { IsSuccess = false };
            }
        }

        /// <summary>
        /// Gets the file link.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>The link to file</returns>
        //[PrincipalPermission(SecurityAction.Demand, Authenticated = true)]
        public string GetFileLink(BackupFileInputs inputs)
        {
            Check.Require(!string.IsNullOrEmpty(inputs.File), "File of folder path is required");
            var computer = this.computersRepository.Get(inputs.ComputerId);
            Check.Require(computer != null, "Provide Id of existing computer");

            if (computer.IsManualEncryptionEnabled)
            {
                return "Can't get download link for manual encrypted computers";
            }

            var link = this.fileLinksRepository.FindOne(new Dictionary<string, object> { { "Computer", computer }, { "Path", inputs.File } });

            if (link == null)
            {
                link = new FileLink
                           {
                               Computer = computer,
                               Path = inputs.File,
                               Link = Guid.NewGuid().ToString()
                           };

                this.fileLinksRepository.SaveOrUpdate(link);
            }

            return string.Format(ConfigurationManager.AppSettings["FileLinkTemplate"], link.Link);
        }

        #endregion 

        #region Private Methods

        /// <summary>
        /// Get BackedUpFile by filename for specify computer
        /// </summary>
        /// <param name="computer">specify computer</param>
        /// <param name="filename">filename path</param>
        /// <returns>return backedup file</returns>
        private BackedUpFile GetFileByName(Computer computer, string filename)
        {
            return this.backedUpFileRepository.FindOne(new Dictionary<string, object>() { { "Computer", computer }, { "ClientPath", filename } });
        }

        /// <summary>
        /// Duplicates the parts exists.
        /// </summary>
        /// <param name="backedUpFileVersion">The backed up file version.</param>
        /// <returns>[true] if duplicates the parts exists.</returns>
        private bool DuplicatePartsExists(BackedUpFileVersion backedUpFileVersion)
        {
            if (backedUpFileVersion.Parts.Count == 0)
            {
                return false;
            }

            Guid findingPartId = backedUpFileVersion.GetLastPart().S3FileId;

            var param = new Dictionary<string, object>
                            {
                                { "S3FileId", findingPartId }
                            };

            var findingResult = this.backedUpFilePartsRepository.FindAll(param);

            if (findingResult.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Backups the new file.
        /// </summary>
        /// <param name="backupFileInputs">The backup inputs.</param>
        /// <param name="computer">The computer.</param>
        private void BackupNewFile(BackupFileInputs backupFileInputs, Computer computer)
        {
            BackedUpFile file = new BackedUpFile(computer)
            {
                ClientPath = backupFileInputs.File
            };
            BackedUpFileVersion version = new BackedUpFileVersion(file)
            {
                Size = backupFileInputs.Size,
                FileHash = backupFileInputs.FileHash,
                FileVersion = 1,
                LastSavedOn = backupFileInputs.LastSavedDate
            };

            file.FileVersions.Add(version);
            computer.Files.Add(file);
        }

        /// <summary>
        /// Creates the new file version.
        /// </summary>
        /// <param name="backupFileInputs">The backup file inputs.</param>
        /// <param name="backedUpFile">The backed up file.</param>
        private void CreateNewFileVersion(BackupFileInputs backupFileInputs, BackedUpFile backedUpFile)
        {
            var backedUpFileVersion = new BackedUpFileVersion(backedUpFile)
            {
                FileVersion = backedUpFile.GetLastVersionNumber() + 1,
                Size = backupFileInputs.Size,
                FileHash = backupFileInputs.FileHash,
                LastSavedOn = backupFileInputs.LastSavedDate
            };

            backedUpFile.FileVersions.Add(backedUpFileVersion);
        }

        /// <summary>
        /// Creates the copy of file.
        /// </summary>
        /// <param name="backupFileInputs">The backup file inputs.</param>
        private void CreateCopyOfFile(BackupFileInputs backupFileInputs)
        {
            Computer computer = this.computersRepository.Get(backupFileInputs.ComputerId);

            BackedUpFile oldFile = this.GetFileByName(computer, backupFileInputs.FileOld);

            BackedUpFile newFile;

            newFile = this.GetFileByName(computer, backupFileInputs.File);
            if (newFile != null)
            {
                var oldFileVersion = oldFile.GetLastVersion();
                BackedUpFileVersion newVersion = new BackedUpFileVersion(newFile)
                                                     {
                                                         FileHash = oldFileVersion.FileHash,
                                                         FileVersion = newFile.FileVersions.Count,
                                                         FinisedDate = oldFileVersion.FinisedDate,
                                                         IsFinished = oldFileVersion.IsFinished,
                                                         Size = oldFileVersion.Size,
                                                         LastSavedOn = oldFileVersion.LastSavedOn
                                                     };
                newFile.FileVersions.Add(newVersion);
                foreach (BackedUpFilePart oldFilePart in oldFileVersion.Parts)
                {
                    newVersion.Parts.Add(new BackedUpFilePart(newVersion)
                    {
                        Length = oldFilePart.Length,
                        Number = oldFilePart.Number,
                        S3FileId = oldFilePart.S3FileId
                    });
                }
            }
            else
            {
                newFile = new BackedUpFile(computer)
                              {
                                  ClientPath = backupFileInputs.File,
                                  SpecialClientPath = oldFile.SpecialClientPath
                              };
                foreach (BackedUpFileVersion oldFileVersion in oldFile.FileVersions)
                {
                    BackedUpFileVersion newVersion = new BackedUpFileVersion(newFile)
                                                         {
                                                             FileHash = oldFileVersion.FileHash,
                                                             FileVersion = oldFileVersion.FileVersion,
                                                             FinisedDate = oldFileVersion.FinisedDate,
                                                             IsFinished = oldFileVersion.IsFinished,
                                                             Size = oldFileVersion.Size,
                                                             LastSavedOn = oldFileVersion.LastSavedOn
                                                         };
                    newFile.FileVersions.Add(newVersion);
                    foreach (BackedUpFilePart oldFilePart in oldFileVersion.Parts)
                    {
                        newVersion.Parts.Add(new BackedUpFilePart(newVersion)
                                                 {
                                                     Length = oldFilePart.Length,
                                                     Number = oldFilePart.Number,
                                                     S3FileId = oldFilePart.S3FileId
                                                 });
                    }
                }

                computer.Files.Add(newFile);
            }

            this.computersRepository.SaveOrUpdate(computer);
        }

        #endregion

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        public void Dispose()
        {
            Global.Container.Release(Global.Container.Resolve<ISession>());
        }
    }
}
