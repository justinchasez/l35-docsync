using System.Collections.Generic;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Dto for getting information about files in computer.
    /// </summary>
    public class ComputerFilesDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerFilesDto"/> class.
        /// </summary>
        public ComputerFilesDto()
        {
            this.FilesInfo = new List<FileInfoDto>();
        }

        /// <summary>
        /// Gets or sets the files info.
        /// </summary>
        /// <value>The files info.</value>
        public List<FileInfoDto> FilesInfo { get; set; }
    }
}