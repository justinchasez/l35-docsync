namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// UpdateComputerGuid inputs.
    /// </summary>
    public class UpdateComputerGuidInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the computer GUID.
        /// </summary>
        /// <value>The computer GUID.</value>
        public string ComputerGuid { get; set; }
    }
}