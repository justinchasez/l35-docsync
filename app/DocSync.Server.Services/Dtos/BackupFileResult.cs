using DocSync.Server.Core.Entities;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Response from server to begin backup.
    /// </summary>
    public class BackupFileResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackupFileResult"/> class.
        /// </summary>
        public BackupFileResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackupFileResult"/> class.
        /// </summary>
        /// <param name="computer">The computer.</param>
        public BackupFileResult(Computer computer)
        {
            this.FreeSpace = computer.FreeSpace;
            this.UsedSpace = computer.BytesUsed;
        }

        /// <summary>
        /// Is computer expired
        /// </summary>
        public bool IsComputerExpired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is finished.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is finished; otherwise, <c>false</c>.
        /// </value>
        public bool IsFinished { get; set; }

        /// <summary>
        /// Is limit reached
        /// </summary>
        public bool IsLimitReached { get; set; }

        /// <summary>
        /// Gets or sets the number of finished parts.
        /// </summary>
        /// <value>The parts count.</value>
        public int PartsCount { get; set; }

        /// <summary>
        /// Used space
        /// </summary>
        public long UsedSpace { get; set; }

        /// <summary>
        /// Gets or sets the free space.
        /// </summary>
        /// <value>The free space.</value>
        public long? FreeSpace { get; set; }

        /// <summary>
        /// Get or set file versions count
        /// </summary>
        public int VersionsCount { get; set; }
    }
}