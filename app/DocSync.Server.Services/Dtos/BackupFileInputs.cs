using System;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Backup starting input data.
    /// </summary>
    public class BackupFileInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the absolute client path to the file.
        /// </summary>
        /// <value>The absolute path file.</value>
        public string File { get; set; }

        /// <summary>
        /// Gets or sets the file size.
        /// </summary>
        /// <value>The file size.</value>
        public long Size { get; set; }

        /// <summary>
        /// Gets or sets the file hash.
        /// </summary>
        /// <value>The file hash.</value>
        public string FileHash { get; set; }

        /// <summary>
        /// Is file upload boolean value
        /// </summary>
        public bool Upload { get; set; }

        /// <summary>
        /// Is file rename boolean value
        /// </summary>
        public bool Rename { get; set; }

        /// <summary>
        /// Get or set old file path
        /// </summary>
        public string FileOld { get; set; }

        /// <summary>
        /// Is file create copy boolean value.
        /// </summary>
        public bool CreateCopy { get; set; }

        /// <summary>
        /// Gets or sets the last saved on.
        /// </summary>
        /// <value>The last saved on.</value>
        public DateTime LastSavedDate { get; set; }
    }
}