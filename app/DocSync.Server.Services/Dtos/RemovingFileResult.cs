namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Removing file result.
    /// </summary>
    public class RemovingFileResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool IsSuccess { get; set; }
    }
}