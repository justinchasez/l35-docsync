using System.Collections.Generic;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Computer info dto.
    /// </summary>
    public class ComputerUsersResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerUsersResult"/> class.
        /// </summary>
        public ComputerUsersResult()
        {
            this.Users = new List<SyncronizeUsersEntry>();
        }

        /// <summary>
        /// Gets or sets the users.
        /// </summary>
        /// <value>The users.</value>
        public List<SyncronizeUsersEntry> Users { get; set; }
    }
}