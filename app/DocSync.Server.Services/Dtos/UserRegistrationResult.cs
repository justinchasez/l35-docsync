using System.Web.Security;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// User registration result.
    /// </summary>
    public class UserRegistrationResult
    {
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the create status.
        /// </summary>
        /// <value>The create status.</value>
        public MembershipCreateStatus CreateStatus { get; set; }
    }
}