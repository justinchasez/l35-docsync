﻿namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Syncronize Users Result class
    /// </summary>
    public class SyncronizeUsersResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether this instance is syncronized.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is syncronized; otherwise, <c>false</c>.
        /// </value>
        public bool IsSyncronized { get; set; }
    }
}