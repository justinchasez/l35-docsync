namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Restore file inputs.
    /// </summary>
    public class RestoreFileInputs
    {
        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string File { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the required version number.
        /// </summary>
        /// <value>The version number.</value>
        public int Version { get; set; }
    }   
}