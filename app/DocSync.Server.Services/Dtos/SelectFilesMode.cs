namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Select files mode for GetAllFilesFromComputer method
    /// </summary>
    public enum SelectFilesMode
    {
        /// <summary>
        /// Select all files
        /// </summary>
        All,

        /// <summary>
        /// Select only documents files
        /// </summary>
        Documents,

        /// <summary>
        /// Select all files without documents
        /// </summary>
        WithoutDocuments
    }
}