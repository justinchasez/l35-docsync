﻿namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Syncronize users entry class
    /// </summary>
    public class SyncronizeUsersEntry
    {
        /// <summary>
        /// SID of user in windows
        /// </summary>
        public string Sid { get; set; }

        /// <summary>
        /// The name of user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Path to user desktop
        /// </summary>
        public string DesktopPath { get; set; }

        /// <summary>
        /// Path to user documents
        /// </summary>
        public string DocumentsPath { get; set; }
    }
}