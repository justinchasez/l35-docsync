using System;
using DocSync.Server.Core.DataInterfaces;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Computer info dto.
    /// </summary>
    public class ComputerInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerInfo"/> class.
        /// </summary>
        public ComputerInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComputerInfo"/> class.
        /// </summary>
        /// <param name="data">The computer data.</param>
        /// <param name="computersRepository">computers repository</param>
        public ComputerInfo(Computer data, IComputersRepository computersRepository)
        {
            computersRepository.BytesUsed(data);

            this.Id = data.Id;
            this.Name = data.Name;
            this.RegistrationDate = data.GetStartDate();
            this.ExpirationDate = data.GetEndDate();
            this.ManualEncriptionState = data.IsManualEncryptionEnabled;
            this.IsTrialComputer = (data.Payments.Count == 0);
            this.BytesUsed = data.BytesUsed;
            this.FreeSpace = data.FreeSpace;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The computer id.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The computer id.</value>
        public bool ManualEncriptionState { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The computer name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the registration date.
        /// </summary>
        /// <value>The registration date.</value>
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>The expiration date.</value>
        public DateTime ExpirationDate { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether this instance is trial computer.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is trial computer; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrialComputer { get; set; }

        /// <summary>
        /// Gets or sets the bytes used.
        /// </summary>
        /// <value>The bytes used.</value>
        public long BytesUsed { get; set; }

        /// <summary>
        /// Gets or sets the free space.
        /// </summary>
        /// <value>The free space.</value>
        public long? FreeSpace { get; set; }
    }
}