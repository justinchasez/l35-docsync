using System;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Commit part dto.
    /// </summary>
    public class PartBackupFinishedInputs
    {
        /// <summary>
        /// Gets or sets the s3 part id.
        /// </summary>
        /// <value>The s3 part id.</value>
        public Guid S3PartId { get; set; }

        /// <summary>
        /// Gets or sets the length of the part.
        /// </summary>
        /// <value>The length of the part.</value>
        public int PartLength { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string File { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }
    }
}