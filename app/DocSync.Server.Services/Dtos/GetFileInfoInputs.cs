namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Get file info inputs.
    /// </summary>
    public class GetFileInfoInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the full file path.
        /// </summary>
        /// <value>The client file path.</value>
        public string File { get; set; }
    }
}