using System;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Dto for file part.
    /// </summary>
    public class FilePartDto
    {
        /// <summary>
        /// Gets or sets the s3 part id.
        /// </summary>
        /// <value>The s3 part id.</value>
        public Guid S3PartId { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>The number.</value>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        /// <value>The length.</value>
        public int Length { get; set; }
    }
}