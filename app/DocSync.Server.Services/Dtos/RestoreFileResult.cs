using System.Collections.Generic;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Restore file result
    /// </summary>
    public class RestoreFileResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestoreFileResult"/> class.
        /// </summary>
        public RestoreFileResult()
        {
            this.FileParts = new List<FilePartDto>();
        }

        /// <summary>
        /// Gets or sets the file parts.
        /// </summary>
        /// <value>The file parts.</value>
        public List<FilePartDto> FileParts { get; set; }

        /// <summary>
        /// Gets or sets the size of the file.
        /// </summary>
        /// <value>The size of the file.</value>
        public long FileSize { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets is computer expired value
        /// </summary>
        public bool IsComputerExpired { get; set; }
    }
}