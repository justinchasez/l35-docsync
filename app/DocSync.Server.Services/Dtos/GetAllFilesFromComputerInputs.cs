namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// GetAllFilesFromComputer inputs.
    /// </summary>
    public class GetAllFilesFromComputerInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the files category.
        /// </summary>
        /// <value>The files category.</value>
        public SelectFilesMode FilesCategory { get; set; }

        /// <summary>
        /// Gets or sets the user sid.
        /// </summary>
        /// <value>The user sid.</value>
        public string UserSid { get; set; }
    }
}