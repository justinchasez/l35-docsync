﻿using System.Collections.Generic;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Syncronize Users Inputs class
    /// </summary>
    public class SyncronizeUsersInputs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SyncronizeUsersInputs"/> class.
        /// </summary>
        public SyncronizeUsersInputs()
        {
            this.UsersList  = new List<SyncronizeUsersEntry>();
        }

        /// <summary>
        /// Windows users list.
        /// </summary>
        public List<SyncronizeUsersEntry> UsersList { get; set; }

        /// <summary>
        /// The computer info.
        /// </summary>
        public ComputerInfo Computer { get; set; }

        /// <summary>
        /// Gets or sets the computer GUID.
        /// </summary>
        /// <value>The computer GUID.</value>
        public string ComputerGuid { get; set; }
    }
}