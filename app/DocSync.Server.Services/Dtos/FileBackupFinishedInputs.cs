namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// File backup finished inputs.
    /// </summary>
    public class FileBackupFinishedInputs
    {
        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }

        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>The file path.</value>
        public string File { get; set; }
    }
}