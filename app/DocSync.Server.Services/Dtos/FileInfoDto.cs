using System;
using System.Collections.Generic;
using DocSync.Server.Core.Entities;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Dto for getting information about files.
    /// </summary>
    public class FileInfoDto
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileInfoDto"/> class.
        /// </summary>
        public FileInfoDto()
        {
            this.FileVersions = new List<FileVersionDto>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileInfoDto"/> class.
        /// </summary>
        /// <param name="backedUpFile">The backed up file.</param>
        public FileInfoDto(BackedUpFile backedUpFile)
        {
            var fileVersion = new FileVersionDto();
            var lastVersion = backedUpFile.GetLastVersion();
            if (lastVersion != null)
            {
                fileVersion.VersionNumber = lastVersion.FileVersion;
                fileVersion.FinishedDate = lastVersion.FinisedDate ?? DateTime.MinValue;
                fileVersion.LastSavedDate = lastVersion.LastSavedOn ?? DateTime.MinValue;
                fileVersion.FileSize = lastVersion.Size;
            }

            this.FileName = backedUpFile.ClientPath;
            this.FileVersions = new List<FileVersionDto> { fileVersion };
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="FileInfoDto"/> class.
		/// </summary>
		/// <param name="backedUpFile">The backed up file.</param>
		/// <param name="lastVersion">The last version.</param>
    	public FileInfoDto(BackedUpFile backedUpFile, BackedUpFileVersion lastVersion)
    	{
			var fileVersion = new FileVersionDto();
			if (lastVersion != null)
			{
				fileVersion.VersionNumber = lastVersion.FileVersion;
				fileVersion.FinishedDate = lastVersion.FinisedDate ?? DateTime.MinValue;
				fileVersion.LastSavedDate = lastVersion.LastSavedOn ?? DateTime.MinValue;
				fileVersion.FileSize = lastVersion.Size;
			}

			this.FileName = backedUpFile.ClientPath;
			this.FileVersions = new List<FileVersionDto> { fileVersion };
    	}

    	/// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>The name of the file.</value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file versions.
        /// </summary>
        /// <value>The file versions.</value>
        public List<FileVersionDto> FileVersions { get; set; }

        /// <summary>
        /// Gets or sets the name of the owner.
        /// </summary>
        /// <value>The name of the owner.</value>
        public string SpecialClientPath { get; set; }
    }
}