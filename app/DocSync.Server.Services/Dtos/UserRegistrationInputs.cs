namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// Class for user registration data.
    /// </summary>
    public class UserRegistrationInputs
    {
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Source
        /// </summary>
        public string Source { get; set; }
    }
}