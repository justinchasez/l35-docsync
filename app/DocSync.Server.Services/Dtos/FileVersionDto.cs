using System;

namespace DocSync.Server.Services.Dtos
{
    /// <summary>
    /// File version dto
    /// </summary>
    public struct FileVersionDto
    {
        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        /// <value>The version number.</value>
        public int VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the finished date.
        /// </summary>
        /// <value>The finished date.</value>
        public DateTime FinishedDate { get; set; }

        /// <summary>
        /// Gets or sets the last saved date.
        /// </summary>
        /// <value>The last saved date.</value>
        public DateTime LastSavedDate { get; set; }

        /// <summary>
        /// Gets or sets the size of the file.
        /// </summary>
        /// <value>The size of the file.</value>
        public long FileSize { get; set; }
    }
}