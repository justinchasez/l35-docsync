using System.Data;
using System.ServiceModel.Dispatcher;
using Microsoft.Practices.ServiceLocation;
using NHibernate;

namespace DocSync.Server.Services.Transactions
{
    /// <summary>
    /// Transaction manager
    /// </summary>
    public class TransactionManager : IDispatchMessageInspector
    {
        #region IDispatchMessageInspector Members

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)"/> method.
        /// </returns>
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
			var session = ServiceLocator.Current.GetInstance<ISession>();
			session.BeginTransaction(IsolationLevel.ReadCommitted);
            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)"/> method.</param>
        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
			var session = ServiceLocator.Current.GetInstance<ISession>();

			if (session.Transaction.IsActive)
			{
				if (!reply.IsFault)
				{
					session.Transaction.Commit();
				}
				else
				{
					session.Transaction.Rollback();
				}
			}
        }

        #endregion
    }
}