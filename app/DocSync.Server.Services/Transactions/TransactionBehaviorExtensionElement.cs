using System;
using System.ServiceModel.Configuration;

namespace DocSync.Server.Services.Transactions
{
    /// <summary>
    /// dsfa sdfsadfs
    /// </summary>
    public class TransactionBehaviorExtensionElement : BehaviorExtensionElement
    {
        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>The behavior extension.</returns>
        protected override object CreateBehavior()
        {
            return new TransactionBehaviour();
        }

        /// <summary>
        /// Gets the type of behavior.
        /// </summary>
        /// <value></value>
        /// <returns>A <see cref="T:System.Type"/>.</returns>
        public override Type BehaviorType
        {
            get
            {
                return typeof(TransactionBehaviour);
            }
        }
    }
}