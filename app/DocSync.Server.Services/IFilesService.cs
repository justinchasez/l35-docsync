using System.ServiceModel;
using DocSync.Server.Services.Dtos;

namespace DocSync.Server.Services
{
    /// <summary>
    /// Files service.
    /// </summary>
    [ServiceContract]
    public interface IFilesService
    {
        /// <summary>
        /// Backups the specified by backup inputs file.
        /// </summary>
        /// <param name="backupFileInputs">The backup inputs.</param>
        /// <returns><see cref="BackupFileResult"/> instance.</returns>
        [OperationContract]
        BackupFileResult BackupFile(BackupFileInputs backupFileInputs);

        /// <summary>
        /// Backups the part.
        /// </summary>
        /// <param name="backupPartInputs">The backup part inputs.</param>
        /// <returns>Backup part result.</returns>
        [OperationContract]
        BackupPartResult BackupPart(BackupPartInputs backupPartInputs);

        /// <summary>
        /// Parts the finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The inputs.</param>
        [OperationContract]
        void BackupPartFinished(PartBackupFinishedInputs backupFinishedInputs);

        /// <summary>
        /// Files the backup finished.
        /// </summary>
        /// <param name="backupFinishedInputs">The backup finished inputs.</param>
        [OperationContract]
        void BackupFileFinished(FileBackupFinishedInputs backupFinishedInputs);

        /// <summary>
        /// Restores the file.
        /// </summary>
        /// <param name="restoreFileInputs">The restore file inputs.</param>
        /// <returns><see cref="RestoreFileResult"/> instance.</returns>
        [OperationContract]
        RestoreFileResult RestoreFile(RestoreFileInputs restoreFileInputs);

        /// <summary>
        /// Gets the file info.
        /// </summary>
        /// <param name="fileInfoInputs">The file info inputs.</param>
        /// <returns><see cref="FileInfoDto"/> instance.</returns>
        [OperationContract]
        FileInfoDto GetFileInfo(GetFileInfoInputs fileInfoInputs);

        /// <summary>
        /// Gets all files from computer.
        /// </summary>
        /// <param name="inputs">The GetAllFilesFromComputer inputs.</param>
        /// <returns>Files list.</returns>
        [OperationContract]
        ComputerFilesDto GetAllFilesFromComputer(GetAllFilesFromComputerInputs inputs);

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="inputs">The file inputs.</param>
        /// <returns>[true] if the removal was successful, otherwise - [false]</returns>
        [OperationContract]
        RemovingFileResult RemoveFile(GetFileInfoInputs inputs);

        /// <summary>
        /// Gets the file link.
        /// </summary>
        /// <param name="inputs">The inputs.</param>
        /// <returns>The link to file</returns>
        [OperationContract]
        string GetFileLink(BackupFileInputs inputs);
    }
}
