using System;
using Share430.Client.Core.Entities.Schedule;
using Share430.Client.Core.Entities.Schedule.Interface;
using Share430.Client.Core.Enums;

namespace Share430.Client.Scheduler
{
    /// <summary>
    /// The schedules factory.
    /// </summary>
    public static class ScheduleFactory
    {
        /// <summary>
        /// Gets the schedule.
        /// </summary>
        /// <param name="mode">The selected mode.</param>
        /// <param name="days">The selected days.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>The ISchedule entity.</returns>
        public static ISchedule GetSchedule(BackupScheduleMode mode, ScheduleDayOfWeek days, DateTime startDate, DateTime endDate)
        {
            switch (mode)
            {
                case BackupScheduleMode.EveryDay:
                    return new EveryDaySchedule(startDate, endDate);
                case BackupScheduleMode.WorkDays:
                    return new WorkingDaySchedule(startDate, endDate);
                case BackupScheduleMode.Specific:
                    return new SpecificSchedule(days, startDate, endDate);
                default: return null;
            }
        }
    }
}
