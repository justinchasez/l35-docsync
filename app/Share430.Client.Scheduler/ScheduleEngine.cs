using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Timers;
using Share430.Client.Core.Entities.Schedule.Interface;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;

namespace Share430.Client.Scheduler
{
    /// <summary>
    /// The Schedule engine.
    /// </summary>
    public class ScheduleEngine
    {
        /// <summary>
        /// The schedule engine timer.
        /// </summary>
        private readonly Timer scheduleTimer;

        /// <summary>
        /// Inidcates when the scheduling working.
        /// </summary>
        private bool sheduleWorking;

        /// <summary>
        /// The schedukes list.
        /// </summary>
        private Dictionary<Guid, ISchedule> schedules;

        /// <summary>
        /// Counter of processor load
        /// </summary>
        private PerformanceCounter cpuCounter;

        /// <summary>
        /// Cpu counter timer
        /// </summary>
        private Timer cpuCounterTimer;

        /// <summary>
        /// Cpu load values
        /// </summary>
        private float[] cpuLoadValues;

        /// <summary>
        /// Cpu load values pointer
        /// </summary>
        private int cpuLoadValuesPointer;

        /// <summary>
        /// Gets the schedules.
        /// </summary>
        /// <value>The schedules.</value>
        public Dictionary<Guid, ISchedule> Schedules
        {
            get
            {
                if (this.schedules == null)
                {
                    this.schedules = new Dictionary<Guid, ISchedule>();
                }

                return this.schedules;
            }

            set
            {
                this.schedules = value;
            }
        }

        /// <summary>
        /// Get or set is ASAP
        /// </summary>
        public bool IsASAP { get; set; }

        /// <summary>
        /// Get or set Is Schedules Changed
        /// </summary>
        public bool IsSchedulesChanged { get; set; }

        /// <summary>
        /// Timer for daily mode
        /// </summary>
        private Timer dailyTimer;

        #region Events

        /// <summary>
        /// Occurs when [schedule started].
        /// </summary>
        public event EventHandler ScheduleStarted;

        /// <summary>
        /// Raises the <see cref="E:ScheduleStarted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnScheduleStarted(EventArgs e)
        {
            EventHandler handler = this.ScheduleStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when [schedule stopped].
        /// </summary>
        public event EventHandler ScheduleStopped;

        /// <summary>
        /// Raises the <see cref="E:ScheduleStopped"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnScheduleStopped(EventArgs e)
        {
            EventHandler handler = this.ScheduleStopped;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleEngine"/> class.
        /// </summary>
        public ScheduleEngine()
        {
            this.scheduleTimer = new Timer(60000);
            this.scheduleTimer.Elapsed += this.ScheduleTimer_Elapsed;

            //Initialize cpu load monitoring
            this.cpuCounter = new PerformanceCounter
                             {
                                 CategoryName = "Processor",
                                 CounterName = "% Processor Time",
                                 InstanceName = "_Total"
                             };
            this.cpuLoadValues = new float[10];

            this.cpuCounterTimer = new Timer(1000);
            this.cpuCounterTimer.Elapsed += this.CpuCounterTimer_Elapsed;

            this.cpuCounterTimer.Start();
        }

        /// <summary>
        /// Cpu Counter Timer Elapsed event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void CpuCounterTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.cpuLoadValues[this.cpuLoadValuesPointer] = this.cpuCounter.NextValue();

            if (++this.cpuLoadValuesPointer >= 10)
            {
                this.cpuLoadValuesPointer = 0;
            }
        }

        /// <summary>
        /// Get cpu load
        /// </summary>
        /// <returns>Value of cpu load</returns>
        private float GetCpuLoad()
        {
            float result = 0;

            foreach (float value in this.cpuLoadValues)
            {
                result += value;
            }

            return result / this.cpuLoadValues.Length;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            this.scheduleTimer.Start();
        }

        /// <summary>
        /// Gets a value indicating whether this instance is current date in schedule.
        /// </summary>
        /// <param name="checkCpuLoad">Is need check cpu load</param>
        /// <returns>Boolean value</returns>
        public bool IsCurrentDateInSchedule(bool checkCpuLoad)
        {
            return this.CheckDateInSchedule(DateTime.Now, checkCpuLoad);
        }

        /// <summary>
        /// Handles the Elapsed event of the ScheduleTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        private void ScheduleTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            bool result = this.CheckDateInSchedule(e.SignalTime, true);

            if (!this.sheduleWorking && result)
            {
                this.OnScheduleStarted(EventArgs.Empty);
                this.sheduleWorking = true;
            }
            else if (this.sheduleWorking && !result)
            {
                this.OnScheduleStopped(EventArgs.Empty);
                this.sheduleWorking = false;
            }
        }

        /// <summary>
        /// Checks the date in schedule.
        /// </summary>
        /// <param name="date">The check date.</param>
        /// <param name="checkCpuLoad">Is need check cpu load</param>
        /// <returns>True oif the date is in schedule, otherwise false.</returns>
        private bool CheckDateInSchedule(DateTime date, bool checkCpuLoad)
        {
            if (!this.IsASAP)
            {
                switch (SettingsManager.Settings.ScheduleSettings.ScheduleMode)
                {
                    case ScheduleMode.Automatic:

                        return checkCpuLoad ? this.GetCpuLoad() < 45 : true;

                    case ScheduleMode.Advanced:

                        bool result = false;

                        foreach (ISchedule currentSchedule in this.Schedules.Values)
                        {
                            result |= currentSchedule.IsDateInPeriod(date);
                        }

                        return result;

                    case ScheduleMode.InSpecifiedPeriod:

                        if (SettingsManager.Settings.ScheduleSettings.StartDate.Date < SettingsManager.Settings.ScheduleSettings.EndDate.Date)
                        {
                            return SettingsManager.Settings.ScheduleSettings.StartDate.TimeOfDay > date.TimeOfDay;
                        }
                        else
                        {
                            return SettingsManager.Settings.ScheduleSettings.StartDate.TimeOfDay > date.TimeOfDay ||
                                SettingsManager.Settings.ScheduleSettings.EndDate.TimeOfDay < date.TimeOfDay;
                        }

                    case ScheduleMode.Daily:

                        if (this.IsSchedulesChanged && this.dailyTimer != null)
                        {
                            this.dailyTimer.Stop();
                            this.dailyTimer.Dispose();
                            this.dailyTimer = null;
                            this.IsSchedulesChanged = false;
                        }

                        if (this.dailyTimer == null)
                        {
                            this.dailyTimer = new Timer();
                            this.dailyTimer.AutoReset = true;
                            this.dailyTimer.Elapsed += this.DailyTimer_Elapsed;
                            this.dailyTimer.Interval = SettingsManager.Settings.ScheduleSettings.StartDate.TimeOfDay > date.TimeOfDay ?
                                (SettingsManager.Settings.ScheduleSettings.StartDate.TimeOfDay - date.TimeOfDay).TotalMilliseconds :
                                (TimeSpan.FromDays(1) - date.TimeOfDay + SettingsManager.Settings.ScheduleSettings.StartDate.TimeOfDay).TotalMilliseconds;
                            this.dailyTimer.Start();
                        }

                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Daily timer event handler
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void DailyTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.IsASAP = true;
            this.dailyTimer.Interval = TimeSpan.FromDays(1).TotalMilliseconds;
        }
    }
}
