using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Share430.Client.Core.Entities.Schedule.Interface;
using Share430.Client.Core.Utils;

namespace Share430.Client.Scheduler
{
    /// <summary>
    /// Manages the scheduling options.
    /// </summary>
    public class ScheduleManager
    {
        /// <summary>
        /// The name of the scheduling file.
        /// </summary>
		private const string SCHEDULE_FILE_NAME = "Share430.sce";

        /// <summary>
        /// The csheduler engine.
        /// </summary>
        private static ScheduleEngine scheduler;

        /// <summary>
        /// Gets the scheduler.
        /// </summary>
        /// <value>The scheduler.</value>
        public static ScheduleEngine Scheduler
        {
            get
            {
                if (scheduler == null)
                {
                    scheduler = new ScheduleEngine();
                }

                return scheduler;
            }
        }

        /// <summary>
        /// Initializes the specified application data path.
        /// </summary>
        /// <param name="applicationDataPath">The application data path.</param>
        public static void Initialize(string applicationDataPath)
        {
            Loger.Instance.Log("Initialize schedule manager " + applicationDataPath);

            Scheduler.Schedules = InitializeSchedules(applicationDataPath);

            Loger.Instance.Log("Initialize schedule manager Scheduler.Initialize");

            Scheduler.Initialize();
        }

        /// <summary>
        /// Initializes the specified application data path.
        /// </summary>
        /// <param name="applicationDataPath">The application data path.</param>
        /// <returns>The schedules list.</returns>
        private static Dictionary<Guid, ISchedule> InitializeSchedules(string applicationDataPath)
        {
            if (string.IsNullOrEmpty(applicationDataPath))
            {
                applicationDataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }

            string filePath = Path.Combine(applicationDataPath, SCHEDULE_FILE_NAME);
            if (File.Exists(filePath))
            {
                using (Stream steam = File.OpenRead(filePath))
                {
                    IFormatter formatter = new BinaryFormatter();
                    return formatter.Deserialize(steam) as Dictionary<Guid, ISchedule>;
                }
            }

            return new Dictionary<Guid, ISchedule>();
        }

        /// <summary>
        /// Saves the specified application data path.
        /// </summary>
        /// <param name="applicationDataPath">The application data path.</param>
        /// <param name="schedules">The schedules.</param>
        public static void SaveSchedules(string applicationDataPath, Dictionary<Guid, ISchedule> schedules)
        {
            if (schedules != null && schedules.Count > 0)
            {
                if (string.IsNullOrEmpty(applicationDataPath))
                {
                    applicationDataPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                }

                string filePath = Path.Combine(applicationDataPath, SCHEDULE_FILE_NAME);
                using (Stream steam = File.Open(filePath, FileMode.OpenOrCreate))
                {
                    IFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(steam, schedules);
                }
            }
        }
    }
}
