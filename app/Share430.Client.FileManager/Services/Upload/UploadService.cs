using System;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Application;
using Share430.Client.Core.Managers.Files;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services;
using Share430.Client.Core.Services.Web;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Core.Utils;
using Share430.Client.Proxy;

namespace Share430.Client.FileManager.Services.Upload
{
    /// <summary>
    /// The upload service.
    /// </summary>
    public class UploadService : IUploadService
    {
        /// <summary>
        /// Amazon access key.
        /// </summary>
        private readonly string accessKeyId;

        /// <summary>
        /// Amazon secret access key.
        /// </summary>
        private readonly string secretAccessKeyId;

        /// <summary>
        /// Inidcates where the current upload is paused.
        /// </summary>
        private bool isPaused;
     
        /// <summary>
        /// Delegate to call method to upload file to the server
        /// </summary>
        /// <param name="fileInfo">the file info</param>
        /// <param name="computerId">computer identifier</param>
        /// <param name="encryptInfo">encryption info</param>
        /// <param name="isUpload">true if the file should be uploaded</param>
        /// <param name="isRename">true if the file should be renamed</param>
        /// <param name="oldName">original file name, if isRename if false it must be the same as filename in fileInfo</param>
        private delegate void UpLoadData(FileInfo fileInfo, int computerId, EncryptInfo encryptInfo, bool isUpload, bool isRename, string oldName);

        /// <summary>
        /// Initializes a new instance of the <see cref="UploadService"/> class.
        /// </summary>
        /// <param name="initParams">The init params.</param>
        public UploadService(ServiceInitParams initParams)
        {
            if (initParams == null)
            {
                throw new ArgumentNullException("initParams");
            }

            if (initParams.FilesService == null)
            {
                throw new ArgumentNullException("UploadServiceInitParams.FilesService");
            }

            if (String.IsNullOrEmpty(initParams.AccessKeyId))
            {
                throw new ArgumentNullException("UploadServiceInitParams.AccessKeyId");
            }

            if (String.IsNullOrEmpty(initParams.SecretAccessKeyId))
            {
                throw new ArgumentNullException("UploadServiceInitParams.SecretAccessKeyId");
            }

            this.SpeedMan = new SpeedManager();

            this.accessKeyId = initParams.AccessKeyId;
            this.secretAccessKeyId = initParams.SecretAccessKeyId;

            this.PartSize = initParams.PartSize;
            this.BucketName = initParams.BucketName;  

            this.FileService = initParams.FilesService;
        }

        #region IUploadService Members

        #region Events

        /// <summary>
        /// Occurs when upload starting.
        /// </summary>
        public event EventHandler<UploadFileEventArgs> UploadFileStarting;

        /// <summary>
        /// Raises the <see cref="E:UploadStarting"/> event.
        /// </summary>
        /// <param name="e">The <see cref="UploadFileEventArgs"/> instance containing the event data.</param>
        private void OnUploadStarting(UploadFileEventArgs e)
        {
            EventHandler<UploadFileEventArgs> handler = this.UploadFileStarting;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload started].
        /// </summary>
        public event EventHandler<UploadFileEventArgs> UploadFileStarted;

        /// <summary>
        /// Raises the <see cref="E:UploadStarted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUploadStarted(UploadFileEventArgs e)
        {
            EventHandler<UploadFileEventArgs> handler = this.UploadFileStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when [upload part starting].
        /// </summary>
        public event EventHandler<CancelEventArgs> UploadPartStarting;

        /// <summary>
        /// Raises the <see cref="E:UploadPartStarting"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        public void OnUploadPartStarting(CancelEventArgs e)
        {
            EventHandler<CancelEventArgs> handler = this.UploadPartStarting;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when [upload part started].
        /// </summary>
        public event EventHandler<UploadPartEventArgs> UploadPartStarted;

        /// <summary>
        /// Raises the <see cref="E:UploadPartStarted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnUploadPartStarted(UploadPartEventArgs e)
        {
            EventHandler<UploadPartEventArgs> handler = this.UploadPartStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when [upload part completed].
        /// </summary>
        public event EventHandler<UploadPartEventArgs> UploadPartCompleted;

        /// <summary>
        /// Raises the <see cref="E:UploadPartCompleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnUploadPartCompleted(UploadPartEventArgs e)
        {
            EventHandler<UploadPartEventArgs> handler = this.UploadPartCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload paused.
        /// </summary>
        public event EventHandler UploadFilePaused;

        /// <summary>
        /// Raises the <see cref="E:UploadPaused"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUploadPaused(EventArgs e)
        {
            EventHandler handler = this.UploadFilePaused;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload continued.
        /// </summary>
        public event EventHandler UploadFileContinued;

        /// <summary>
        /// Raises the <see cref="E:UploadContinued"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUploadContinued(EventArgs e)
        {
            EventHandler handler = this.UploadFileContinued;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload canceled.
        /// </summary>
        public event EventHandler UploadFileCanceled;

        /// <summary>
        /// Raises the <see cref="E:UploadCanceled"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUploadCanceled(EventArgs e)
        {
            EventHandler handler = this.UploadFileCanceled;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload completed.
        /// </summary>
        public event EventHandler<UploadFileEventArgs> UploadFileCompleted;

        /// <summary>
        /// Raises the <see cref="E:UploadCompleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void OnUploadCompleted(UploadFileEventArgs e)
        {
            EventHandler<UploadFileEventArgs> handler = this.UploadFileCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when upload error.
        /// </summary>
        public event EventHandler<DownloadFileErrorEventArgs> UploadFileError;

        /// <summary>
        /// Raises the <see cref="E:UploadError"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadFileErrorEventArgs"/> instance containing the event data.</param>
        private void OnUploadError(DownloadFileErrorEventArgs e)
        {
            EventHandler<DownloadFileErrorEventArgs> handler = this.UploadFileError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Speed manager
        /// </summary>
        public SpeedManager SpeedMan { get; protected set; }

        /// <summary>
        /// Gets the size of the part.
        /// </summary>
        /// <value>The size of the part.</value>
        public int PartSize { get; private set; }

        /// <summary>
        /// Gets the name of the bucken.
        /// </summary>
        /// <value>The name of the bucken.</value>
        public string BucketName { get; private set; }

        /// <summary>
        /// Gets the file service.
        /// </summary>
        /// <value>The file service.</value>
        public IFilesService FileService { get; private set; }

        /// <summary>
        /// Gets the file path.
        /// </summary>
        /// <value>The file path.</value>
//        public FileInfo FileInfo { get; private set; }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        public UploadState CurrentState { get; private set; }

        #endregion

        /// <summary>
        /// Starts the upload.
        /// </summary>
        /// <param name="fileItem">The file info.</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptInfo">The encrypt info.</param>
        public void StartUpload(FileQueueItem fileItem, int computerId, EncryptInfo encryptInfo)
        {
            FileInfo fileInfo = fileItem.IsRename ? new FileInfo(fileItem.RenamedFilePath) : new FileInfo(fileItem.FilePath);

            Loger.Instance.Log("Upload Service: Raise event OnUploadStarting");
            this.OnUploadStarting(new UploadFileEventArgs { FileInfo = fileInfo, FilePath = fileItem.FilePath });

            this.CurrentState = UploadState.InProgress;

            try
            {
                if (encryptInfo == null)
                {
                    this.OnUploadError(new DownloadFileErrorEventArgs(new Exception("Encryption key is absent")));
                    return;
                }

                UpLoadData uploadData = this.LoadData;
                uploadData.BeginInvoke(fileInfo,
                                        computerId,
                                        encryptInfo,
                                        fileItem.IsUpload,
                                        fileItem.IsRename,
                                        fileItem.FilePath,
                                        this.LoadDataCompleted,
                                        uploadData);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Upload Service: Exception " + ex);
                string token = ServiceManager.GetCredentials();
                this.OnUploadError(new DownloadFileErrorEventArgs(new Exception(ex + string.Format(" Token ({0})", token))));
                return;
            }

            this.OnUploadStarted(new UploadFileEventArgs { FileInfo = fileInfo, FilePath = fileItem.FilePath });
        }

        /// <summary>
        /// Pauses the download.
        /// </summary>
        public void PauseUpload()
        {
            if (!this.isPaused)
            {
                try
                {
                    this.isPaused = true;
                    this.CurrentState = UploadState.Paused;
                    this.OnUploadPaused(EventArgs.Empty);
                }
                catch (Exception ex)
                {
                    this.CurrentState = UploadState.Paused;
                    this.OnUploadError(new DownloadFileErrorEventArgs(ex));
                }
            }
        }

        /// <summary>
        /// Cancels the download.
        /// </summary>
        public void CancelUpload()
        {
            //this.worker.Abort();
            this.CurrentState = UploadState.Canceled;
            this.OnUploadCanceled(EventArgs.Empty);
        }

        /// <summary>
        /// Continues the download.
        /// </summary>
        public void ContinueUpload()
        {
            this.isPaused = false;
            this.OnUploadContinued(EventArgs.Empty);
        }

        #endregion

        #region Private memebers

        /// <summary>
        /// Callback function when LoadData method is completed
        /// </summary>
        /// <param name="asyncResult">async result</param>
        private void LoadDataCompleted(IAsyncResult asyncResult)
        {
            UpLoadData uploadData = (UpLoadData)asyncResult.AsyncState;
            try
            {
                uploadData.EndInvoke(asyncResult);
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        /// <param name="fileInfo">File info to upload</param>
        /// <param name="computerId">The computer id.</param>
        /// <param name="encryptInfo">The encrypt info.</param>
        /// <param name="isUpload">Is file upload</param>
        /// <param name="isRename">Is file rename</param>
        /// <param name="oldName">Old name of file</param>
        private void LoadData(FileInfo fileInfo, int computerId, EncryptInfo encryptInfo, bool isUpload, bool isRename, string oldName)
        {
            try
            {
                Loger.Instance.Log("Upload Service: Begin upload data");
                // Check if file exists in the file system
                if (fileInfo.Exists)
                {
                    bool scheduleBreak = false;
                    // initialize the backup file request.
                    BackupFileRequestModel inputs = new BackupFileRequestModel
                                                  {
                                                      FileName = GetRelativePath(fileInfo.FullName),
                                                      FileHash = FilesManager.GetFileHashCode(fileInfo.FullName),
                                                      Size = fileInfo.Length,
                                                      //Upload = isUpload,
                                                     // Rename = isRename,
                                                      OldFileName = GetRelativePath(oldName),
                                                      LastSavedDate = fileInfo.LastWriteTime,
                                                  };

//                    Trace.WriteLine(inputs.File);

                    Loger.Instance.Log("Upload Service: Request to server 'BackupFile'");
                    BackupFileResultModel backupFileResult = this.FileService.BackupFile(inputs);

                    SettingsManager.Settings.CurrentComputer.BytesUsed = backupFileResult.UsedSpace;
                    SettingsManager.Settings.CurrentComputer.FreeSpace = backupFileResult.FreeSpace;
                    SettingsManager.Save(CoreConstantsHelper.PathForSaveData, SettingsManager.Settings);
//                    Trace.WriteLine(backupFileResult.VersionsCount);

                    // check if file already backed up.);
                    if (backupFileResult != null)
                    {
                        if (!backupFileResult.IsFinished)
                        {
                            if (!backupFileResult.IsLimitReached)
                            {
                                using (AmazonS3 amazonClient = AWSClientFactory.CreateAmazonS3Client(this.accessKeyId, this.secretAccessKeyId))
                                {
                                    using (FileStream fileStream = fileInfo.OpenRead())
                                    {
                                        try
                                        {
                                            this.SpeedMan.StartManager();
                                            // fix the file stream position if file backup not finished.
                                            fileStream.Position = backupFileResult.PartsCount * this.PartSize;

                                            while (fileStream.Position < fileStream.Length)
                                            {
                                                // Wait thread if paused.
                                                this.WaitPause();

                                                // Raise starting event.
                                                CancelEventArgs partStartingEventArgs = new CancelEventArgs();

                                                this.OnUploadPartStarting(partStartingEventArgs);

                                                if (partStartingEventArgs.Cancel)
                                                {
                                                    scheduleBreak = true;
                                                    break;
                                                }

                                                // calculate part size
                                                int partSize = (int)(fileStream.Length - fileStream.Position) >= this.PartSize ?
                                                    this.PartSize :
                                                    (int)(fileStream.Length - fileStream.Position);

                                                byte[] buffer = new byte[partSize];
                                                string partName = Guid.NewGuid().ToString();

                                                this.OnUploadPartStarted(new UploadPartEventArgs { PartName = partName, PartSize = partSize });

                                                RijndaelManaged algorithm = new RijndaelManaged();
                                                //{
                                                //    Padding = PaddingMode.None
                                                //};
                                                //{
                                                //    Padding = PaddingMode.PKCS7,
                                                //    Mode = CipherMode.CFB
                                                //};

                                                MemoryProxyStream outStream = new MemoryProxyStream(this.SpeedMan);
                                                //outStream.BytesPerSecond = 100 * 1024; // 20 kb
                                                int bytesPerSecond = 0;

                                                if (SettingsManager.Settings.ConnectionPriority == ConnectionPriority.Low)
                                                {
                                                    bytesPerSecond = SettingsManager.Settings.LowPrioritySpeed;
                                                }
                                                else if (SettingsManager.Settings.ScheduleSettings.ScheduleMode == ScheduleMode.Automatic)
                                                {
                                                    bytesPerSecond = 100;
                                                }

                                                if (bytesPerSecond > 0)
                                                {
                                                    outStream.BytesPerSecond = bytesPerSecond * 1024;
                                                }

                                                CryptoStream encStream = new CryptoStream(outStream, algorithm.CreateEncryptor(encryptInfo.Key, encryptInfo.Vector), CryptoStreamMode.Write);

                                                //int blockSizeBytes = algorithm.BlockSize / 8;
                                                //byte[] data = new byte[blockSizeBytes];
                                                //int bytesRead = 0;
                                                //int count;
                                                //do
                                                //{
                                                //    count = fileStream.Read(data, 0, blockSizeBytes);
                                                //    encStream.Write(data, 0, count);
                                                //    bytesRead += blockSizeBytes;
                                                //}
                                                //while (count > 0);

                                                //encStream.FlushFinalBlock();
                                                //encStream.Close();

                                                int bytesRead = fileStream.Read(buffer, 0, buffer.Length);
                                                encStream.Write(buffer, 0, bytesRead);
                                                encStream.Flush();
                                                encStream.FlushFinalBlock();
                                                //encStream.FlushFinalBlock();

                                                PutObjectRequest request = new PutObjectRequest
                                                                               {
                                                                                   BucketName = this.BucketName,
                                                                                   Key = partName
                                                                               };

                                                request.WithInputStream(outStream);

//                                                DateTime stamp = DateTime.Now;

                                                Loger.Instance.Log("Upload Service: Request to S3 for upload part");

                                                amazonClient.PutObject(request);

//                                                Trace.WriteLine("Total " + (stamp - outStream.TimeStamp).TotalSeconds.ToString());
                                                // Commit part upload to the server.)
                                                this.FileService.BackupPartFinished(new BackupPartFinishedRequestModel()
                                                                                        {
                                                                                            FileName = GetRelativePath(fileInfo.FullName),
                                                                                            PartLength = buffer.Length,
                                                                                            S3PartId = new Guid(partName)
                                                                                        });

                                                // Raise part upload finished and progress events.
                                                this.OnUploadPartCompleted(new UploadPartEventArgs
                                                                               {
                                                                                   PartName = partName,
                                                                                   PartSize = partSize
                                                                               });
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var amazonExeption = ex as AmazonS3Exception;
                                            if ((amazonExeption != null) && (amazonExeption.ErrorCode.Equals("RequestTimeTooSkewed")))
                                            {
                                                Loger.Instance.Log("Upload service: Invalid time exception " + ex);
                                                this.OnUploadError(new DownloadFileErrorEventArgs(ex) { IsInvalidTime = true });
                                                return;
                                            }

                                            Loger.Instance.Log("Upload Service: Exception " + ex);
                                            this.OnUploadError(new DownloadFileErrorEventArgs(ex));
                                            return;
                                        }
                                    }
                                }

                                if (!scheduleBreak)
                                {
                                    this.FileService.BackupFileFinished(new BackupFileFinishedInputs
                                                                            {
                                                                                ComputerId = computerId,
                                                                                ComputerIdSpecified = true,
                                                                                File = GetRelativePath(fileInfo.FullName)
                                                                            });

                                    Loger.Instance.Log("Upload Service: Raise event Upload complete");

                                    SettingsManager.Settings.CurrentComputer.BytesUsed += fileInfo.Length;
                                    if (SettingsManager.Settings.CurrentComputer.FreeSpace != null)
                                    {
                                        SettingsManager.Settings.CurrentComputer.FreeSpace -= fileInfo.Length;
                                    }

                                    SettingsManager.Save(CoreConstantsHelper.PathForSaveData, SettingsManager.Settings);

                                    this.OnUploadCompleted(new UploadFileEventArgs { FileInfo = fileInfo, FilePath = oldName, VersionsCount = backupFileResult.VersionsCount });
                                }
                            }
                            else
                            {
                                if (backupFileResult.IsLimitReached)
                                {
                                    Loger.Instance.Log("Upload Service: Limit reached");

                                    this.OnUploadError(new DownloadFileErrorEventArgs(new Exception("Limit is reached"))
                                                           {
                                                               IsLimitReached = true,
                                                               UsedSpace = backupFileResult.UsedSpace
                                                           });
                                }
                            }
                        }
                        else
                        {
                            Loger.Instance.Log("Upload Service: Backup file finished");
                            this.OnUploadCompleted(new UploadFileEventArgs { FileInfo = fileInfo, FilePath = oldName, VersionsCount = backupFileResult.VersionsCount });
                        }
                    }
                    else
                    {
                        Loger.Instance.Log("Upload Service: Web service not answer");
                        this.OnUploadError(new DownloadFileErrorEventArgs(new Exception("Web service dont answer")));
                    }
                }
                else
                {
                    Loger.Instance.Log("Upload Service: File not found");
                    this.OnUploadError(new DownloadFileErrorEventArgs(new FileNotFoundException(string.Format("File {0} not found for backup", fileInfo.FullName))));
                }
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Upload Service: Exception " + ex);
                this.OnUploadError(new DownloadFileErrorEventArgs(ex));
            }
        }

        /// <summary>
        /// Waits the pause.
        /// </summary>
        private void WaitPause()
        {
            while (this.isPaused)
            {
                Thread.Sleep(5000);
                if (!this.isPaused)
                {
                    break;
                }
            }
        }

        private string GetRelativePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (!path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                Loger.Instance.Log(string.Format("Upload Service: File in invalid location: {0} should be in {1} ", path, basePath));
            }

            return path.Substring(basePath.Length).Trim('\\');
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}