using System;
using System.IO;
using System.Threading;
using Share430.Client.Core.Managers.Application;

namespace Share430.Client.FileManager.Services.Upload
{
    /// <summary>
    /// MemoryProxyStream class, controls speed of read (in bytes per second)
    /// </summary>
    public class MemoryProxyStream : MemoryStream
    {
        /// <summary>
        ///  min speed value in bytes
        /// </summary>
        private const int MinSpeed = 8 * 1024;
        
        /// <summary>
        /// Speed manager
        /// </summary>
        private SpeedManager speedMan;
        
        /// <summary>
        /// Bytes per second
        /// </summary>
        private int bytesPerSecond;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryProxyStream"/> class.
        /// </summary>
        /// <param name="speedMan">The speed man.</param>
        public MemoryProxyStream(SpeedManager speedMan)
        {
            this.bytesPerSecond = 0;
            this.TimeStamp = DateTime.Now;
            this.speedMan = speedMan;
        }

        /// <summary>
        /// last time stamp value
        /// </summary>
        public DateTime TimeStamp { get; private set; }

        /// <summary>
        /// Gets or sets Bytes per second value
        /// </summary>
        public int BytesPerSecond
        {
            get
            {
                return this.bytesPerSecond;
            }

            set
            {
                this.bytesPerSecond = value >= MinSpeed ? value : MinSpeed;
            }
        }

        /// <summary>
        /// Read method
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="offset">offset value</param>
        /// <param name="count">count of byte</param>
        /// <returns>return read bytes</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this.BytesPerSecond > 0)
            {
                DateTime tempNow = DateTime.Now;

                TimeSpan localStamp = tempNow - this.TimeStamp;
                TimeSpan second = new TimeSpan(0, 0, 1);

                if (count > this.BytesPerSecond)
                {
                    count = this.BytesPerSecond;
                }
                else
                {
                    second = TimeSpan.FromTicks(second.Ticks * count / this.BytesPerSecond);
                }

                if (localStamp < second)
                {
                    TimeSpan delta = second - localStamp;
                    Thread.Sleep(delta);
                }

                this.TimeStamp = DateTime.Now;
            }

            int result = base.Read(buffer, offset, count);

            this.speedMan.UpdateSpeed(count);
            return result;
        }
    }
}