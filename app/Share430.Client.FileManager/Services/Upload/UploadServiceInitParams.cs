using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Services.Web;

namespace Share430.Client.FileManager.Services.Upload
{
    /// <summary>
    /// The init params for the upload service.
    /// </summary>
    public class UploadServiceInitParams
    {
        /// <summary>
        /// Gets or sets the size of the part.
        /// </summary>
        /// <value>The size of the part.</value>
        public int PartSize { get; set; }

        /// <summary>
        /// Gets or sets the access key id.
        /// </summary>
        /// <value>The access key id.</value>
        public string AccessKeyId { get; set; }

        /// <summary>
        /// Gets or sets the secret access key id.
        /// </summary>
        /// <value>The secret access key id.</value>
        public string SecretAccessKeyId { get; set; }

        /// <summary>
        /// Gets or sets the name of the bucket.
        /// </summary>
        /// <value>The name of the bucket.</value>
        public string BucketName { get; set; }

        /// <summary>
        /// Gets or sets the files service.
        /// </summary>
        /// <value>The files service.</value>
        public IFilesService FilesService { get; set; }

        /// <summary>
        /// Gets or sets the encrypt key.
        /// </summary>
        /// <value>The encrypt key.</value>
        public EncryptInfo EncryptKey { get; set; }

        /// <summary>
        /// Gets or sets the computer id.
        /// </summary>
        /// <value>The computer id.</value>
        public int ComputerId { get; set; }
    }
}