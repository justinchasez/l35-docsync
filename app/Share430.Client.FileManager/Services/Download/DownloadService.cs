using System;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Share430.Client.Core.Args;
using Share430.Client.Core.Collections;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Application;
using Share430.Client.Core.Managers.Files;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Services;
using Share430.Client.Core.Services.Web;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.Core.Utils;

namespace Share430.Client.FileManager.Services.Download
{
    /// <summary>
    /// The download file service.
    /// </summary>
    public class DownloadService : IDownloadService
    {
        #region Private members

        /// <summary>
        /// The amazon access key id.
        /// </summary>
        private readonly string accessKeyId;

        /// <summary>
        /// The amazon secret access key id.
        /// </summary>
        private readonly string secretAccessKeyId;

        /// <summary>
        /// True, if the downloading is paused, otherwise false.
        /// </summary>
        private bool isPaused;

      #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DownloadService"/> class.
        /// </summary>
        /// <param name="initParams">The init params.</param>
        public DownloadService(ServiceInitParams initParams)
        {
            if (initParams.FilesService == null)
            {
                throw new ArgumentNullException("DownloadServiceInitParams.FilesService");
            }

            this.SpeedMan = new SpeedManager();

            this.accessKeyId = initParams.AccessKeyId;
            this.secretAccessKeyId = initParams.SecretAccessKeyId;

            this.PartSize = initParams.PartSize;
            this.BucketName = initParams.BucketName;

            this.FilesService = initParams.FilesService;
        }

        #region IDownloadService Members

        #region Events

        /// <summary>
        /// Occurs when Download started.
        /// </summary>
        public event EventHandler<DownloadFileEventArgs> DownloadStarted;

        /// <summary>
        /// Raises the <see cref="E:DownloadStarted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadFileEventArgs"/> instance containing the event data.</param>
        public void OnDownloadStarted(DownloadFileEventArgs e)
        {
            EventHandler<DownloadFileEventArgs> handler = this.DownloadStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when download part started.
        /// </summary>
        public event EventHandler<CancelEventArgs> DownloadPartStarting;

        /// <summary>
        /// Raises the <see cref="E:DownloadPartStarting"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs"/> instance containing the event data.</param>
        public void OnDownloadPartStarting(CancelEventArgs e)
        {
            EventHandler<CancelEventArgs> handler = this.DownloadPartStarting;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when download part started.
        /// </summary>
        public event EventHandler<DownloadPartEventArgs> DownloadPartStarted;

        /// <summary>
        /// Raises the <see cref="E:DownloadPartStarted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadPartEventArgs"/> instance containing the event data.</param>
        public void OnDownloadPartStarted(DownloadPartEventArgs e)
        {
            EventHandler<DownloadPartEventArgs> handler = this.DownloadPartStarted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download paused.
        /// </summary>
        public event EventHandler DownloadPaused;

        /// <summary>
        /// Raises the <see cref="E:DownloadPaused"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnDownloadPaused(EventArgs e)
        {
            EventHandler handler = this.DownloadPaused;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download continued.
        /// </summary>
        public event EventHandler DownloadContinued;

        /// <summary>
        /// Raises the <see cref="E:DownloadContinued"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnDownloadContinued(EventArgs e)
        {
            EventHandler handler = this.DownloadContinued;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download canceled.
        /// </summary>
        public event EventHandler DownloadCanceled;

        /// <summary>
        /// Raises the <see cref="E:DownloadCanceled"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnDownloadCanceled(EventArgs e)
        {
            EventHandler handler = this.DownloadCanceled;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download progress changed.
        /// </summary>
        public event EventHandler<DownloadFileProgressChangedEventArgs> DownloadProgressChanged;

        /// <summary>
        /// Raises the <see cref="E:DownloadProgressChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadFileProgressChangedEventArgs"/> instance containing the event data.</param>
        public void OnDownloadProgressChanged(DownloadFileProgressChangedEventArgs e)
        {
            EventHandler<DownloadFileProgressChangedEventArgs> handler = this.DownloadProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when download part completed.
        /// </summary>
        public event EventHandler<DownloadPartEventArgs> DownloadPartCompleted;

        /// <summary>
        /// Raises the <see cref="E:DownloadPartCompleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadPartEventArgs"/> instance containing the event data.</param>
        public void OnDownloadPartCompleted(DownloadPartEventArgs e)
        {
            EventHandler<DownloadPartEventArgs> handler = this.DownloadPartCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download completed.
        /// </summary>
        public event EventHandler<DownloadFileEventArgs> DownloadCompleted;

        /// <summary>
        /// Raises the <see cref="E:DownloadCompleted"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadFileEventArgs"/> instance containing the event data.</param>
        public void OnDownloadCompleted(DownloadFileEventArgs e)
        {
            EventHandler<DownloadFileEventArgs> handler = this.DownloadCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Occurs when Download error.
        /// </summary>
        public event EventHandler<DownloadFileErrorEventArgs> DownloadError;

        /// <summary>
        /// Raises the <see cref="E:DownloadError"/> event.
        /// </summary>
        /// <param name="e">The <see cref="DownloadFileErrorEventArgs"/> instance containing the event data.</param>
        public void OnDownloadError(DownloadFileErrorEventArgs e)
        {
            EventHandler<DownloadFileErrorEventArgs> handler = this.DownloadError;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Speed manager
        /// </summary>
        public SpeedManager SpeedMan { get; protected set; }

        /// <summary>
        /// Gets the size of the part.
        /// </summary>
        /// <value>The size of the part.</value>
        public int PartSize { get; private set; }

        /// <summary>
        /// Gets the name of the bucket.
        /// </summary>
        /// <value>The name of the bucket.</value>
        public string BucketName { get; private set; }

        /// <summary>
        /// Gets or sets the file service.
        /// </summary>
        /// <value>The file service.</value>
        private IFilesService FilesService { get; set; }

        /// <summary>
        /// Gets the Download file info.
        /// </summary>
        /// <value>The Download file info.</value>
        public RestoreFileQueueItem CurrentFile { get; private set; }

        /// <summary>
        /// Gets the state of the current.
        /// </summary>
        /// <value>The state of the current.</value>
        public DownloadState CurrentState { get; private set; }

        #endregion

        /// <summary>
        /// Starts the Download.
        /// </summary>
        /// <param name="item">The restore file item.</param>
        public void StartDownload(RestoreFileQueueItem item)
        {
            Loger.Instance.Log("Download service: Start download");

            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            this.CurrentFile = item;

            this.OnDownloadStarted(new DownloadFileEventArgs(this.CurrentFile));

            this.CurrentState = DownloadState.InProgress;
            ThreadPool.QueueUserWorkItem(
                state => this.DownloadData());
        }

        /// <summary>
        /// Pauses the download.
        /// </summary>
        public void PauseDownload()
        {
            this.isPaused = true;
            this.CurrentState = DownloadState.Paused;
            this.OnDownloadPaused(EventArgs.Empty);
        }

        /// <summary>
        /// Cancels the download.
        /// </summary>
        public void CancelDownload()
        {
            this.CurrentState = DownloadState.Canceled;
            this.OnDownloadCanceled(EventArgs.Empty);
        }

        /// <summary>
        /// Continues the download.
        /// </summary>
        public void ContinueDownload()
        {
            this.isPaused = false;
            this.CurrentState = DownloadState.InProgress;
            this.OnDownloadContinued(EventArgs.Empty);
        }

        #endregion

        #region Private members

        /// <summary>
        /// Downloads the data.
        /// </summary>
        private void DownloadData()
        {
            Loger.Instance.Log("Download service: Request to server");
            try
            {
                // Get the file info from the server.
                RestoreFileRequestModel restoreFileInputs = new RestoreFileRequestModel
                {
                    //File = this.CurrentFile.File.FullName,
                    FileName = this.CurrentFile.FilePath,
                    Version =
                        this.CurrentFile.
                        RestoreFileVersion,
                };
                this.CurrentFile.RestoreFilePath = GetFullPath(this.CurrentFile.RestoreFilePath);

                RestoreFileResponseModel fileResult = this.FilesService.RestoreFile(restoreFileInputs);

                if (fileResult != null && fileResult.FileParts != null)
                {
                    if (fileResult.FileParts.Count == 0)
                    {
                        string path = Path.GetDirectoryName(this.CurrentFile.RestoreFilePath);
                        if (!String.IsNullOrEmpty(path) && (!Directory.Exists(path)))
                        {
                            Directory.CreateDirectory(path);
                        }
                        CreateFile(this.CurrentFile.RestoreFilePath);
                        Loger.Instance.Log("Download service: Download completed");
                        this.OnDownloadCompleted(new DownloadFileEventArgs(this.CurrentFile));

                        this.CurrentState = DownloadState.Finished;
                        //this.CurrentFile = null;
                    }
                    if (File.Exists(this.CurrentFile.RestoreFilePath) && CheckFileHash(fileResult))
                    {
                        Loger.Instance.Log("Download service: File Already exists");
                        this.OnDownloadCompleted(new DownloadFileEventArgs(this.CurrentFile));

                        this.CurrentState = DownloadState.Finished;

                    }
                    else
                    {
                        try
                        {
                            using (AmazonS3 amazonClient = AWSClientFactory.CreateAmazonS3Client(this.accessKeyId, this.secretAccessKeyId))
                            {
                                int completedPartNumber = 0;
                                FileInfo restoreFileInfo = new FileInfo(this.CurrentFile.RestoreFilePath);

                                // Indetify if file was in downloading progress and downloaded parts count.
                                if (restoreFileInfo.Exists)
                                {
                                    if (restoreFileInfo.Length % this.PartSize == 0)
                                    {
                                        completedPartNumber = (int)(restoreFileInfo.Length / this.PartSize);
                                    }
                                    else
                                    {
                                        restoreFileInfo.Delete();
                                    }
                                }
                                else
                                {
                                    // Create path to file if he don't exists
                                    string path = Path.GetDirectoryName(this.CurrentFile.RestoreFilePath);
                                    if (!String.IsNullOrEmpty(path) && (!Directory.Exists(path)))
                                    {
                                        Directory.CreateDirectory(path);
                                    }
                                }
                                
                                this.SpeedMan.StartManager();

                                using (FileStream fileStream = OpenFile(this.CurrentFile.RestoreFilePath))
                                {
                                    foreach (FilePartModel currentPart in fileResult.FileParts)
                                    {
                                        // Skip downloaded parts.
                                        if (currentPart.Number <= completedPartNumber)
                                        {
                                            continue;
                                        }

                                        // Wait if paused.
                                        this.WaitPause();

                                        this.OnDownloadPartStarted(new DownloadPartEventArgs(currentPart.Length, currentPart.S3PartId.ToString()));

                                        // Create an amazon request.
                                        GetObjectRequest request = new GetObjectRequest
                                                                       {
                                                                           BucketName = this.BucketName,
                                                                           Key = currentPart.S3PartId.ToString()
                                                                       };

                                        // Handle amazon response.
                                        Loger.Instance.Log("Download service: Request to S3");
                                        using (S3Response response = amazonClient.GetObject(request))
                                        {
                                            using (Stream responseStream = response.ResponseStream)
                                            {
                                                RijndaelManaged algorithm = new RijndaelManaged();
                                                //{
                                                //    Padding = PaddingMode.None
                                                //};
                                                //{
                                                //    Padding = PaddingMode.PKCS7,
                                                //    Mode = CipherMode.CFB
                                                //};

                                                using (CryptoProxyStream decStream = new CryptoProxyStream(responseStream, algorithm.CreateDecryptor(this.CurrentFile.EncryptKey.Key, this.CurrentFile.EncryptKey.Vector), CryptoStreamMode.Read))
                                                {
                                                    int bytesPerSecond = 0;

                                                    if (SettingsManager.Settings.ConnectionPriority == ConnectionPriority.Low)
                                                    {
                                                        bytesPerSecond = SettingsManager.Settings.LowPrioritySpeed;
                                                    }
                                                    else if (SettingsManager.Settings.ScheduleSettings.ScheduleMode == ScheduleMode.Automatic)
                                                    {
                                                        bytesPerSecond = 100;
                                                    }

                                                    if (bytesPerSecond > 0)
                                                    {
                                                        decStream.BytesPerSecond = bytesPerSecond * 1024;
                                                    }

                                                    byte[] buffer = new byte[10240];

                                                    int bytesRead;
                                                    do
                                                    {
                                                        bytesRead = decStream.Read(buffer, 0, buffer.Length);
                                                        fileStream.Write(buffer, 0, bytesRead);
                                                        this.SpeedMan.UpdateSpeed(bytesRead);
                                                    }
                                                    while (bytesRead > 0);

                                                    fileStream.Flush();
                                                }
                                            }
                                        }

                                        this.OnDownloadPartCompleted(new DownloadPartEventArgs(currentPart.Length,
                                                                                               currentPart.S3PartId.ToString()));
                                        completedPartNumber++;

                                        if (this.CurrentState == DownloadState.Canceled)
                                        {
                                            return;
                                        }
                                    }
                                }

                                Loger.Instance.Log("Download service: Download complete");
                                this.OnDownloadCompleted(new DownloadFileEventArgs(this.CurrentFile));

                                this.CurrentState = DownloadState.Finished;
                                //this.CurrentFile = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            var amazonExeption = ex as AmazonS3Exception;
                            if ((amazonExeption != null) && (amazonExeption.ErrorCode.Equals("RequestTimeTooSkewed")))
                            {
                                Loger.Instance.Log("Download service: Invalid time exception " + ex);
                                this.OnDownloadError(new DownloadFileErrorEventArgs(ex) { IsInvalidTime = true });
                                return;
                            }

                            Loger.Instance.Log("Download service: Exception " + ex);
                            this.OnDownloadError(new DownloadFileErrorEventArgs(ex));
                            return;
                        }
                    }
                }
                else
                {
                    Loger.Instance.Log("Download service: Download service not unswer");
                    this.OnDownloadError(new DownloadFileErrorEventArgs(new Exception("Wrong file data in web service")));
                }
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Download service: exception " + ex);
                this.OnDownloadError(new DownloadFileErrorEventArgs(ex));
            }
        }

        private bool CheckFileHash(RestoreFileResponseModel fileResult)
        {
            try
            {
                return FilesManager.GetFileHashCode(this.CurrentFile.RestoreFilePath) == fileResult.FileHash;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Waits the pause.
        /// </summary>
        private void WaitPause()
        {
            while (this.isPaused)
            {
                Thread.Sleep(5000);
                if (!this.isPaused)
                {
                    break;
                }
            }
        }

        private string GetFullPath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            return Path.Combine(basePath, path);
        }

        private void CreateFile(string path)
        {
            int attempts = 5;
            while (attempts > 0)
            {
                try
                {
                    new FileInfo(path).Create();
                    return;
                }
                catch (Exception ex)
                {
                    attempts--;
                    if (attempts <= 0)
                    {
                        throw;
                    }

                    Thread.Sleep(100);
                }
            }
        }

        private FileStream OpenFile(string path)
        {
            int attempts = 5;
            while (attempts > 0)
            {
                try
                {
                    return new FileInfo(path).OpenWrite();
                }
                catch (Exception ex)
                {
                    attempts--;
                    if (attempts <= 0)
                    {
                        throw;
                    }

                    Thread.Sleep(100);
                }
            }

            throw new Exception("Cannot open file");
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        #endregion
    }
}