using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;

namespace Share430.Client.FileManager.Services.Download
{
    /// <summary>
    /// MemoryProxyStream class, controls speed of read (in bytes per second)
    /// </summary>
    public class CryptoProxyStream : CryptoStream
    {
        /// <summary>
        ///  min speed value in bytes
        /// </summary>
        private const int MinSpeed = 8 * 1024;

        /// <summary>
        /// Bytes per second
        /// </summary>
        private int bytesPerSecond;

        /// <summary>
        /// Initializes a new instance of the <see cref="CryptoProxyStream"/> class.
        /// </summary>
        /// <param name="stream">The stream on which to perform the cryptographic transformation.</param>
        /// <param name="transform">The cryptographic transformation that is to be performed on the stream.</param>
        /// <param name="mode">One of the <see cref="T:System.Security.Cryptography.CryptoStreamMode"/> values.</param>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="stream"/> is not readable.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="stream"/> is not writable.
        /// </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="stream"/> is invalid.
        /// </exception>
        public CryptoProxyStream(Stream stream, ICryptoTransform transform, CryptoStreamMode mode) : base(stream, transform, mode)
        {
            this.bytesPerSecond = 0;
            this.TimeStamp = DateTime.Now;
        }

        /// <summary>
        /// last time stamp value
        /// </summary>
        public DateTime TimeStamp { get; private set; }

        /// <summary>
        /// Gets or sets Bytes per second value
        /// </summary>
        public int BytesPerSecond
        {
            get
            {
                return this.bytesPerSecond;
            }

            set
            {
                this.bytesPerSecond = value >= MinSpeed ? value : MinSpeed;
            }
        }

        /// <summary>
        /// Read method
        /// </summary>
        /// <param name="buffer">destination buffer</param>
        /// <param name="offset">offset value</param>
        /// <param name="count">count of byte</param>
        /// <returns>return read bytes</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            if (this.BytesPerSecond > 0)
            {
                DateTime tempNow = DateTime.Now;

                TimeSpan localStamp = tempNow - this.TimeStamp;
                TimeSpan second = new TimeSpan(0, 0, 1);

                if (count > this.BytesPerSecond)
                {
                    count = this.BytesPerSecond;
                }
                else
                {
                    second = TimeSpan.FromTicks(second.Ticks * count / this.BytesPerSecond);
                }

                if (localStamp < second)
                {
                    TimeSpan delta = second - localStamp;
                    Thread.Sleep(delta);
                }

                this.TimeStamp = DateTime.Now;
            }

            return base.Read(buffer, offset, count);
        }
    }
}