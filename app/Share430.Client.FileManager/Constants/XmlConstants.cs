namespace Share430.Client.FileManager.Constants
{
    /// <summary>
    /// The constants helper for working with the file xml.
    /// </summary>
    public class XmlConstants
    {
        /// <summary>
        /// Xml File Repository Path
        /// </summary>
        public const string XML_FILE_REPOSITORY_NAME = "fileRepository.xml";

        /// <summary>
        /// Root tag name
        /// </summary>
        public const string TAG_ROOT = "root";

        /// <summary>
        /// Folder tag name
        /// </summary>
        public const string TAG_FOLDER = "folder";

        /// <summary>
        /// File tag name
        /// </summary>
        public const string TAG_FILE = "file";

        /// <summary>
        /// Name attribute name
        /// </summary>
        public const string ATTRIBUTE_NAME = "name";

        /// <summary>
        /// File state attribute name
        /// </summary>
        public const string ATTRIBUTE_FILE_STATE = "state";

        /// <summary>
        /// File versions count attribute name
        /// </summary>
        public const string ATTRIBUTE_FILE_VERSION_COUNT = "vercount";

        /// <summary>
        /// Folder mode attribute name
        /// </summary>
        public const string ATTRIBUTE_MONITORING_MODE = "mode";

        /// <summary>
        /// File modified attribute name
        /// </summary>
        public const string ATTRIBUTE_FILE_MODIFIED_DATE = "modified";

        /// <summary>
        /// File size attribute name
        /// </summary>
        public const string ATTRIBUTE_FILE_SIZE = "size";

        /// <summary>
        /// File state attribute pending value
        /// </summary>
        public const string FILE_STATE_PENDING = "Pending";

        /// <summary>
        /// File state attribute backedup value
        /// </summary>
        public const string FILE_STATE_BACKED_UP = "Backedup";

        /// <summary>
        /// Folder mode monitoring value
        /// </summary>
        public const string MONITORING_MODE_MONITORING = "monitoring";
    }
}