using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using Share430.Client.Core.Constants;
using Share430.Client.Core.Entities;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Services.Web.Models;
using Share430.Client.FileManager.Constants;

namespace Share430.Client.FileManager
{
    /// <summary>
    /// Class wrapper for XmlDocument, to easy use XmlDocument
    /// </summary>
    public class XmlDocumentWrapper
    {
        #region Constants

        /// <summary>
        /// Tag width attribute format
        /// </summary>
        private const string TAG_WITH_ATTRIBUTE_AND_SLASH_FORMAT = "/{0}[@{1} = \"{2}\"]";

        /// <summary>
        /// Tag width attribute format
        /// </summary>
        private const string TAG_WITH_ATTRIBUTE_FORMAT = "{0}[@{1} = \"{2}\"]";

        /// <summary>
        /// Xml path for top level folders
        /// </summary>
        private const string XML_PATH_FOR_TOP_LEVEL_FOLDERS_FORMAT = ".//{0}[count({1}) > 0 or count({0}) > 1] | .//{0}[count({1}) = 0 and count({0}) = 0]";

        /// <summary>
        /// Xml path for select all backed up files
        /// </summary>
        private readonly string XML_PATH_FOR_SELECT_ALL_BACKED_UP_FILES = string.Format("//{0}[@{1} > 0]", XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT);

        /// <summary>
        /// Separator for path
        /// </summary>
        private const char SEPARATOR = '\\';

        #endregion

        /// <summary>
        /// The xml documetn with files.
        /// </summary>
        private XmlDocument document;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlDocumentWrapper"/> class.
        /// </summary>
        public XmlDocumentWrapper()
        {
            this.document = new XmlDocument();

            try
            {
                if (File.Exists(Path.Combine(CoreConstantsHelper.PathForSaveData, XmlConstants.XML_FILE_REPOSITORY_NAME)))
                {
                    try
                    {
                        this.document.Load(Path.Combine(CoreConstantsHelper.PathForSaveData, XmlConstants.XML_FILE_REPOSITORY_NAME));
                    }
                    catch (Exception)
                    {
                        File.Delete(Path.Combine(CoreConstantsHelper.PathForSaveData, XmlConstants.XML_FILE_REPOSITORY_NAME));
                        //throw;
                    }
                }
                else
                {
                    this.document.AppendChild(this.document.CreateNode(XmlNodeType.XmlDeclaration, string.Empty, string.Empty));
                    this.document.AppendChild(this.document.CreateNode(XmlNodeType.Element, XmlConstants.TAG_ROOT, string.Empty));
                    this.Save();
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Update XML file repository from server
        /// </summary>
        /// <param name="files">Files from server</param>
        public void SetFilesInXml(ComputerFilesDto files)
        {
            this.document = new XmlDocument();

            this.document.AppendChild(this.document.CreateNode(XmlNodeType.XmlDeclaration, string.Empty, string.Empty));
            this.document.AppendChild(this.document.CreateNode(XmlNodeType.Element, XmlConstants.TAG_ROOT, string.Empty));

            foreach (FileModel file in files.FilesInfo)
            {
                this.AddFileInXml(file.FileName, XmlConstants.FILE_STATE_BACKED_UP, file.FileVersions.Count, file.FileVersions[file.FileVersions.Count - 1].LastSavedOn.ToString(), file.FileVersions[file.FileVersions.Count - 1].Size, false);
            }

            this.Save();
        }

        /// <summary>
        /// Get top level folders for file watcher
        /// </summary>
        /// <returns>List of top level folders</returns>
        public IList<string> GetTopLevelFoldersFromXml()
        {
            //string xmlPath = string.Format(XML_PATH_FOR_TOP_LEVEL_FOLDERS_FORMAT, XmlConstants.TAG_FOLDER, XmlConstants.TAG_FILE);

            List<string> result = new List<string>();

            XPathNavigator rootNavigator = this.document.CreateNavigator();
            XPathNodeIterator rootIterator = rootNavigator.Select(string.Format("/{0}/{1}", XmlConstants.TAG_ROOT, XmlConstants.TAG_FOLDER));

            while (rootIterator.MoveNext())
            {
                string logicalDisk = rootIterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty) + "\\";

                XPathNodeIterator iterator = rootIterator.Current.Select(string.Format("./{0}", XmlConstants.TAG_FOLDER));

                while (iterator.MoveNext())
                {
                    result.Add(Path.Combine(logicalDisk, iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                }

                //XPathNodeIterator iterator = rootIterator.Current.Select(xmlPath);

                //while (iterator.MoveNext())
                //{
                //    string path = iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty);

                //    while (iterator.Current.MoveToParent())
                //    {
                //        path = string.Concat(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty), SEPARATOR, path);
                //    }

                //    path = path.TrimStart(SEPARATOR);
                //    bool flag = true;

                //    foreach (string p in result)
                //    {
                //        flag = flag & !(path.IndexOf(p) > -1);
                //    }

                //    if (flag)
                //    {
                //        result.Add(path);
                //    }
                //}
            }

            return result;
        }

        /// <summary>
        /// Get logical disks
        /// </summary>
        /// <returns>List of logical disk</returns>
        public IList<string> GetLogicalDiskFromXml()
        {
            List<string> result = new List<string>();

            XPathNavigator rootNavigator = this.document.CreateNavigator();
            XPathNodeIterator rootIterator = rootNavigator.Select(string.Format("/{0}/{1}", XmlConstants.TAG_ROOT, XmlConstants.TAG_FOLDER));

            while (rootIterator.MoveNext())
            {
                result.Add(rootIterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty));
            }

            return result;
        }

        /// <summary>
        /// Add folder node in XML
        /// </summary>
        /// <param name="path">folder full path</param>
        /// <param name="mode">folder mode</param>
        public void AddFolderInXml(string path, string mode)
        {
            string[] folderNames = path.Split(SEPARATOR);

            XPathNavigator navigator = this.AddFolderInXmlAndNavigate(folderNames);

            if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
            {
                navigator.SetValue(mode);
            }
        }

        /// <summary>
        /// Add file in XML
        /// </summary>
        /// <param name="path">File full path</param>
        /// <param name="state">File state</param>
        /// <param name="versionsCount">Versions count</param>
        /// <param name="modifiedDateStr">String of modified date</param>
        /// <param name="fileSize">Size of file</param>
        /// <param name="isFileMonitored">Is file motitored</param>
        public void AddFileInXml(string path, string state, int versionsCount, string modifiedDateStr, long fileSize, bool isFileMonitored)
        {    
            string[] folderNames = Path.GetDirectoryName(path).TrimEnd(SEPARATOR).Split(SEPARATOR);
            string fileName = Path.GetFileName(path);

            XPathNavigator navigator = this.AddFolderInXmlAndNavigate(folderNames);
            XPathNodeIterator iterator = navigator.Select(string.Format(TAG_WITH_ATTRIBUTE_FORMAT, XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_NAME, fileName));

            if (!iterator.MoveNext())
            {
                navigator.AppendChildElement(string.Empty, XmlConstants.TAG_FILE, string.Empty, string.Empty);
                navigator.MoveToFirstChild();

                while (navigator.MoveToNext())
                {
                }

                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_NAME, string.Empty, fileName);
                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty, state);
                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty, versionsCount.ToString());
                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty, isFileMonitored ? XmlConstants.MONITORING_MODE_MONITORING : string.Empty);
                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty, modifiedDateStr);
                navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_FILE_SIZE, string.Empty, fileSize.ToString());
            }
        }

        /// <summary>
        /// Set backed up attribute and increment version count for file
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <param name="lastModifiedDate">Last modified date of file</param>
        /// <param name="fileSize">The size of file</param>
        /// <param name="versionsCount">Count of file versions</param>
        public void SetBackedUpAttribute(string pathToFile, DateTime lastModifiedDate, long fileSize, int versionsCount)
        {
            if (!this.IsFileInXml(pathToFile))
            {
                this.AddFileInXml(pathToFile, XmlConstants.FILE_STATE_BACKED_UP, versionsCount, lastModifiedDate.ToString(), fileSize, true);
            }
            else
            {
                XPathNavigator navigator = this.NavigateToFile(pathToFile);

                if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty))
                {
                    navigator.SetValue(XmlConstants.FILE_STATE_BACKED_UP);
                    navigator.MoveToParent();
                }

                if (versionsCount > 0)
                {
                    if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty))
                    {
                        navigator.SetValue(versionsCount.ToString());
                        navigator.MoveToParent();
                    }
                }

                if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty))
                {
                    navigator.SetValue(lastModifiedDate.ToString());
                    navigator.MoveToParent();
                }

                if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_FILE_SIZE, string.Empty))
                {
                    navigator.SetValue(fileSize.ToString());
                    navigator.MoveToParent();
                }

                if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    navigator.SetValue(XmlConstants.MONITORING_MODE_MONITORING);
                }
            }
        }

        /// <summary>
        /// Set pending attribute for file
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        public void SetPendingAttribute(string pathToFile)
        {
            XPathNavigator navigator = this.NavigateToFile(pathToFile);

            if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty))
            {
                navigator.SetValue(XmlConstants.FILE_STATE_PENDING);
                navigator.MoveToParent();
            }

            if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
            {
                navigator.SetValue(XmlConstants.MONITORING_MODE_MONITORING);
            }
        }

        /// <summary>
        /// Set monitoring attribute for folder
        /// </summary>
        /// <param name="pathToFolder">Path to folder</param>
        public void SetFolderMonitoringAttribute(string pathToFolder)
        {
            XPathNavigator navigator = this.NavigateToFolder(pathToFolder);

            if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
            {
                navigator.SetValue(XmlConstants.MONITORING_MODE_MONITORING);
            }
        }

        /// <summary>
        /// Unset file monitoring attribute
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        public void UnsetFileMonitoringAttribute(string pathToFile)
        {
            XPathNavigator navigator = this.NavigateToFile(pathToFile);

            if (int.Parse(navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty)) <= 0)
            {
                navigator.DeleteSelf();
            }
            else if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
            {
                navigator.SetValue(string.Empty);
            }
        }

        /// <summary>
        /// Unset file monitoring attribute in all files
        /// </summary>
        /// <param name="pathToFolder">Path to folder</param>
        public void UnsetFileMonitoringAttributeInAllFiles(string pathToFolder)
        {
            XPathNavigator navigator = this.NavigateToFolder(pathToFolder);

            XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}", XmlConstants.TAG_FILE));

            List<XPathNavigator> filesForRemove = new List<XPathNavigator>();

            while (iterator.MoveNext())
            {
                if (int.Parse(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty)) <= 0)
                {
                    filesForRemove.Add(iterator.Current.Clone());
                }
                else if (iterator.Current.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    iterator.Current.SetValue(string.Empty);
                }
            }

            foreach (XPathNavigator nav in filesForRemove)
            {
                nav.DeleteSelf();
            }

            iterator = navigator.Select(string.Format(".//{0}[@{1} = \"{2}\"]", XmlConstants.TAG_FOLDER, XmlConstants.ATTRIBUTE_MONITORING_MODE, XmlConstants.MONITORING_MODE_MONITORING));

            while (iterator.MoveNext())
            {
                if (iterator.Current.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    iterator.Current.SetValue(string.Empty);
                }
            }

            List<XPathNavigator> foldersForRemove = new List<XPathNavigator>();
            iterator = navigator.Select(string.Format(".//{0}[count(*) = 0]", XmlConstants.TAG_FOLDER));

            while (iterator.MoveNext())
            {
                foldersForRemove.Add(iterator.Current.Clone());
            }

            foreach (XPathNavigator n in foldersForRemove)
            {
                this.RemoveEmptyFolders(n);
            }

            if (!navigator.HasChildren)
            {
                navigator.DeleteSelf();
            }
            else
            {
                if (navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    navigator.SetValue(string.Empty);
                }
            }
        }

        /// <summary>
        /// Is file in XML
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>Boolean value</returns>
        public bool IsFileInXml(string pathToFile)
        {
            return this.NavigateToFile(pathToFile).GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty).Equals(Path.GetFileName(pathToFile));
        }

        /// <summary>
        /// Is folder in XML
        /// </summary>
        /// <param name="pathToFolder">Path to folder</param>
        /// <returns>Boolean value</returns>
        public bool IsFolderInXml(string pathToFolder)
        {
            XPathNavigator navigator = this.NavigateToFolder(pathToFolder);
            string[] folderNames = pathToFolder.TrimEnd(SEPARATOR).Split(SEPARATOR);
            return navigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty).Equals(folderNames[folderNames.Length - 1]);
        }

        /// <summary>
        /// Is file panding
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>Boolean value</returns>
        public bool IsFilePending(string pathToFile)
        {
            return this.NavigateToFile(pathToFile).GetAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty).Equals(XmlConstants.FILE_STATE_PENDING);
        }

        /// <summary>
        /// Is file monitored
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>Is file monitored boolean value</returns>
        public bool IsFileMonitored(string pathToFile)
        {
            return this.NavigateToFile(pathToFile).GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty).Equals(XmlConstants.MONITORING_MODE_MONITORING);
        }

        /// <summary>
        /// Is folder Monitored
        /// </summary>
        /// <param name="pathToFolder">Path to file or folder</param>
        /// <returns>Bool value of monitoring</returns>
        public bool IsFolderMonitored(string pathToFolder)
        {
            bool result = false;

            string[] folderNames = pathToFolder.TrimEnd(SEPARATOR).Split(SEPARATOR);

            for (int i = 0; i < folderNames.Length; i++)
            {
                StringBuilder path = new StringBuilder();
                for (int j = 0; j <= i; j++)
                {
                    path.AppendFormat("{0}\\", folderNames[j]);
                }

                XPathNavigator navigator = this.NavigateToFolder(path.ToString().TrimEnd(SEPARATOR));

                result = result || navigator.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty).Equals(XmlConstants.MONITORING_MODE_MONITORING);
            }

            return result;
        }

        /// <summary>
        /// Rename file in XML
        /// </summary>
        /// <param name="oldFilePath">Old file path</param>
        /// <param name="newFilePath">New file path</param>
        public void FileRename(string oldFilePath, string newFilePath)
        {
            XPathNavigator navigator = this.NavigateToFile(oldFilePath);
            navigator.MoveToAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty);
            navigator.SetValue(Path.GetFileName(newFilePath));
        }

        /// <summary>
        /// Rename folder in XML
        /// </summary>
        /// <param name="oldFolderPath">Old folder path</param>
        /// <param name="newFolderPath">New folder path</param>
        /// <returns>Dictionary of file names</returns>
        public Dictionary<string, string> FolderRename(string oldFolderPath, string newFolderPath)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            string[] newFolderNames = newFolderPath.TrimEnd(SEPARATOR).Split(SEPARATOR);
            XPathNavigator oldNavigator = this.NavigateToFolder(oldFolderPath);
            XPathNavigator newNavigator;

            if (!this.IsFolderInXml(newFolderPath))
            {
                oldNavigator.InsertElementAfter(string.Empty, XmlConstants.TAG_FOLDER, string.Empty, string.Empty);

                newNavigator = oldNavigator.Clone();
                newNavigator.MoveToNext();
                newNavigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_NAME, string.Empty, newFolderNames[newFolderNames.Length - 1]);
                newNavigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty, oldNavigator.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty));
            }
            else
            {
                newNavigator = this.NavigateToFolder(newFolderPath);
                if (newNavigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    newNavigator.SetValue(oldNavigator.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty));
                    newNavigator.MoveToParent();
                }
            }

            if (oldNavigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
            {
                oldNavigator.SetValue(string.Empty);
                oldNavigator.MoveToParent();
            }

            XPathNodeIterator iterator = oldNavigator.Select(string.Format(".//{0}[@{1} = \"{2}\"]", XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_MONITORING_MODE, XmlConstants.MONITORING_MODE_MONITORING));

            List<XPathNavigator> listTempNavigators = new List<XPathNavigator>();

            while (iterator.MoveNext())
            {
                listTempNavigators.Add(iterator.Current.Clone());
            }

            foreach (XPathNavigator tempNavigator in listTempNavigators)
            {
                XPathNavigator tempNavigatorForDelete = tempNavigator.Clone();

                string oldFilePath = tempNavigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty);

                while (tempNavigator.MoveToParent())
                {
                    if (tempNavigator.Name.Equals(XmlConstants.TAG_FOLDER))
                    {
                        oldFilePath = string.Format("{0}\\{1}", tempNavigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty), oldFilePath);
                    }
                    else
                    {
                        break;
                    }
                }

                string newFilePath = oldFilePath.Replace(oldFolderPath, newFolderPath);

                this.AddFileInXml(newFilePath,
                    tempNavigatorForDelete.GetAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty),
                    int.Parse(tempNavigatorForDelete.GetAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty)),
                    tempNavigatorForDelete.GetAttribute(XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty),
                    long.Parse(tempNavigatorForDelete.GetAttribute(XmlConstants.ATTRIBUTE_FILE_SIZE, string.Empty)),
                    true);

                tempNavigatorForDelete.DeleteSelf();
                this.RemoveEmptyFolders(tempNavigatorForDelete);

                result.Add(oldFilePath, newFilePath);
            }

            iterator = oldNavigator.Select(string.Format(".//{0}[@{1} = \"{2}\"]", XmlConstants.TAG_FOLDER, XmlConstants.ATTRIBUTE_MONITORING_MODE, XmlConstants.MONITORING_MODE_MONITORING));

            while (iterator.MoveNext())
            {
                XPathNavigator tempNavigator = iterator.Current.Clone();

                string tempOldFolderPath = tempNavigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty);

                while (tempNavigator.MoveToParent())
                {
                    if (tempNavigator.Name.Equals(XmlConstants.TAG_FOLDER))
                    {
                        tempOldFolderPath = string.Format("{0}\\{1}", tempNavigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty), tempOldFolderPath);
                    }
                    else
                    {
                        break;
                    }
                }

                string tempNewFolderPaht = tempOldFolderPath.Replace(oldFolderPath, newFolderPath);

                if (this.IsFolderInXml(tempNewFolderPaht))
                {
                    XPathNavigator tempFolderNavigator = this.NavigateToFolder(tempNewFolderPaht);

                    if (tempFolderNavigator.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                    {
                        tempFolderNavigator.SetValue(XmlConstants.MONITORING_MODE_MONITORING);
                    }
                }

                if (iterator.Current.MoveToAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty))
                {
                    iterator.Current.SetValue(string.Empty);
                    iterator.Current.MoveToParent();
                }
            }

            return result;
        }

        /// <summary>
        /// Remove file from xml
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        public void RemoveFileFromXml(string pathToFile)
        {
            if (this.IsFileInXml(pathToFile))
            {
                XPathNavigator navigator = this.NavigateToFile(pathToFile);
                navigator.DeleteSelf();
                //this.RemoveEmptyFolders(navigator);
            }
        }

        /// <summary>
        /// Remove folder from xml
        /// </summary>
        /// <param name="pathToFolder">Path to folder</param>
        public void RemoveFolderFromXml(string pathToFolder)
        {
            if (this.IsFolderInXml(pathToFolder))
            {
                XPathNavigator navigator = this.NavigateToFolder(pathToFolder);
                navigator.DeleteSelf();
                //this.RemoveEmptyFolders(navigator);
            }
        }

        /// <summary>
        /// Get files from folder
        /// </summary>
        /// <param name="folderPath">Folder path</param>
        /// <param name="fileState">File state</param>
        /// <returns>Array names of files</returns>
        public string[] GetEntriesFromFolder(string folderPath, string fileState)
        {
            List<string> files = new List<string>();

            if (!string.IsNullOrEmpty(folderPath))
            {
                if (this.IsFolderInXml(folderPath))
                {
                    XPathNavigator navigator = this.NavigateToFolder(folderPath);
                    XPathNodeIterator iterator = navigator.Select(string.Format(TAG_WITH_ATTRIBUTE_FORMAT, XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_FILE_STATE, fileState));

                    while (iterator.MoveNext())
                    {
                        files.Add(string.Concat("0", iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                    }

                    iterator = navigator.Select(XmlConstants.TAG_FOLDER);
                    while (iterator.MoveNext())
                    {
                        if (fileState.Equals(XmlConstants.FILE_STATE_BACKED_UP))
                        {
                            if (this.IsFolderBackedUp(iterator.Current))
                            {
                                files.Add(string.Concat("1", iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                            }
                        }
                        else
                        {
                            if (this.IsFolderPending(iterator.Current))
                            {
                                files.Add(string.Concat("1", iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                            }
                        }
                    }
                }
            }
            else
            {
                XPathNavigator navigator = this.document.CreateNavigator();
                XPathNodeIterator iterator = navigator.Select(string.Format("/{0}/{1}", XmlConstants.TAG_ROOT, XmlConstants.TAG_FOLDER));

                while (iterator.MoveNext())
                {
                    if (fileState.Equals(XmlConstants.FILE_STATE_BACKED_UP))
                    {
                        if (this.IsFolderBackedUp(iterator.Current))
                        {
                            files.Add(string.Concat("1", iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                        }
                    }
                    else
                    {
                        if (this.IsFolderPending(iterator.Current))
                        {
                            files.Add(string.Concat("1", iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty)));
                        }
                    }
                }
            }

            return files.ToArray();
        }

        /// <summary>
        /// Check the file
        /// </summary>
        /// <param name="path">Path to file</param>
        /// <param name="state">File state</param>
        /// <param name="isScheduled">Is Scheduled</param>
        /// <param name="verCount">Count of versions</param>
        public void CheckFile(string path, ref BackupState state, ref bool isScheduled, ref int verCount)
        {
            XPathNavigator navigator = this.NavigateToFile(path);

            state = navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty).Equals(XmlConstants.FILE_STATE_PENDING)
                    ? BackupState.Pending
                    : BackupState.Backedup;

            isScheduled = navigator.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty).Equals(XmlConstants.MONITORING_MODE_MONITORING);

            verCount = int.Parse(navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty));
        }

        /// <summary>
        /// Check the folder
        /// </summary>
        /// <param name="path">Path to folder</param>
        /// <param name="state">Files state</param>
        /// <param name="isScheduled">Files is scheduled</param>
        public void CheckFolder(string path, ref BackupState state, ref bool isScheduled)
        {
            XPathNavigator navigator = this.NavigateToFolder(path);
            XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}", XmlConstants.TAG_FILE));

            while (iterator.MoveNext())
            {
                if (iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_FILE_STATE, string.Empty).Equals(XmlConstants.FILE_STATE_PENDING))
                {
                    state = BackupState.Pending;
                }

                if (iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty).Equals(XmlConstants.MONITORING_MODE_MONITORING))
                {
                    isScheduled = true;
                }

                if (state == BackupState.Pending && isScheduled)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Is folder in state
        /// </summary>
        /// <param name="pathToFolder">Full path to folder</param>
        /// <param name="state">State of folder</param>
        /// <returns>Boolean value</returns>
        public bool IsFolderInState(string pathToFolder, BackupState state)
        {
            bool result = false;
            int tempVerCount = 0;

            XPathNavigator navigator = this.NavigateToFolder(pathToFolder);

            if (pathToFolder.ToLower().EndsWith(navigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty).ToLower()))
            {
                XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}", XmlConstants.TAG_FILE));

                while (iterator.MoveNext())
                {
                    tempVerCount = int.Parse(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT, string.Empty));

                    if (state == BackupState.Backedup && tempVerCount > 0)
                    {
                        result = true;
                        break;
                    }

                    if (state == BackupState.Pending && tempVerCount == 0)
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Saves the specified file name.
        /// </summary>
        public void Save()
        {
            this.document.Save(Path.Combine(CoreConstantsHelper.PathForSaveData, XmlConstants.XML_FILE_REPOSITORY_NAME));
        }

        /// <summary>
        /// Get backed up files for search
        /// </summary>
        /// <returns>DataTable of backed up files</returns>
        public DataTable GetBackedUpFilesForSearch()
        {
            DataTable result = new DataTable();

            result.Columns.Add("Name", typeof(string));
            result.Columns.Add("Size", typeof(SizeEntity));
            result.Columns.Add("Date modified", typeof(DateTime));
            result.Columns.Add("Location", typeof(string));

            XPathNavigator navigator = this.document.CreateNavigator();
            XPathNodeIterator iterator = navigator.Select(this.XML_PATH_FOR_SELECT_ALL_BACKED_UP_FILES);

            long tempSize = 0;
            DateTime tempDate = DateTime.Now;

            while (iterator.MoveNext())
            {
                result.Rows.Add(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty),
                    long.TryParse(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_FILE_SIZE, string.Empty), out tempSize) ? new SizeEntity(tempSize) : new SizeEntity(0),
                    DateTime.TryParse(iterator.Current.GetAttribute(XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty), out tempDate) ? (object)tempDate : null,
                    this.GetFilePath(iterator.Current));
            }

            return result;
        }

        /// <summary>
        /// Get file info
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>XML file info object</returns>
        public XmlFileInfo GetFileInfo(string pathToFile)
        {
            XmlFileInfo result = new XmlFileInfo();

            XPathNavigator navigator = this.NavigateToFile(pathToFile);

            result.Size = new SizeEntity(long.Parse(navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_SIZE, string.Empty)));

            DateTime modDate;
            DateTime.TryParse(navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty),
                              out modDate);

            result.LastModifiedDate = modDate;
            //result.LastModifiedDate = DateTime.Parse(navigator.GetAttribute(XmlConstants.ATTRIBUTE_FILE_MODIFIED_DATE, string.Empty));

            return result;
        }

        /// <summary>
        /// Get Count Of Backed Up Files
        /// </summary>
        /// <returns>Count Of Backed Up Files</returns>
        public int GetCountOfBackedUpFiles()
        {
            XPathNavigator navigator = this.document.CreateNavigator();

            return navigator.Select(string.Format("//{0}[@{1} > 0]",
                XmlConstants.TAG_FILE,
                XmlConstants.ATTRIBUTE_FILE_VERSION_COUNT)).Count;
        }

        /// <summary>
        /// Get all files from folder
        /// </summary>
        /// <param name="folderPath">Path to folder</param>
        /// <returns>List of the files</returns>
        public IList<string> GetAllFilesFromFolder(string folderPath)
        {
            List<string> result = new List<string>();

            XPathNavigator navigator = this.NavigateToFolder(folderPath);
            XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}", XmlConstants.TAG_FILE));

            while (iterator.MoveNext())
            {
                result.Add(this.GetFilePath(iterator.Current.Clone()));
            }

            return result;
        }

        #region Private Methods

        /// <summary>
        /// Add folder in XML and navigate to this
        /// </summary>
        /// <param name="path">Names of folders</param>
        /// <returns>Navigator to added folder</returns>
        private XPathNavigator AddFolderInXmlAndNavigate(string[] path)
        {
            XPathNavigator navigator = this.document.CreateNavigator();
            navigator.MoveToFirstChild();

            foreach (string folderName in path)
            {
                XPathNodeIterator iterator = navigator.Select(string.Format(TAG_WITH_ATTRIBUTE_FORMAT, XmlConstants.TAG_FOLDER, XmlConstants.ATTRIBUTE_NAME, folderName));

                if (iterator.MoveNext())
                {
                    navigator = iterator.Current;
                }
                else
                {
                    navigator.AppendChildElement(String.Empty, XmlConstants.TAG_FOLDER, string.Empty, string.Empty);
                    navigator.MoveToChild(XmlConstants.TAG_FOLDER, string.Empty);

                    while (navigator.MoveToNext())
                    {
                    }

                    navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_NAME, string.Empty, folderName);
                    navigator.CreateAttribute(string.Empty, XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty, string.Empty);
                }
            }

            return navigator;
        }

        /// <summary>
        /// Navigate to file in XML
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>XPathNavigator to file</returns>
        private XPathNavigator NavigateToFile(string pathToFile)
        {
            string[] folderNames = Path.GetDirectoryName(pathToFile).TrimEnd(SEPARATOR).Split(SEPARATOR);
            string fileName = Path.GetFileName(pathToFile);

            StringBuilder xmlPath = new StringBuilder(string.Format("/{0}", XmlConstants.TAG_ROOT));

            foreach (string folder in folderNames)
            {
                xmlPath.AppendFormat(TAG_WITH_ATTRIBUTE_AND_SLASH_FORMAT, XmlConstants.TAG_FOLDER, XmlConstants.ATTRIBUTE_NAME, folder);
            }

            xmlPath.AppendFormat(TAG_WITH_ATTRIBUTE_AND_SLASH_FORMAT, XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_NAME, fileName);

            XPathNodeIterator iterator = this.document.CreateNavigator().Select(xmlPath.ToString());

            iterator.MoveNext();

            return iterator.Current;
        }

        /// <summary>
        /// Navigate to folder in XML
        /// </summary>
        /// <param name="pathToFolder">Path to folder</param>
        /// <returns>XPathNavigator to folder</returns>
        private XPathNavigator NavigateToFolder(string pathToFolder)
        {
            string[] folderNames = pathToFolder.TrimEnd(SEPARATOR).Split(SEPARATOR);

            StringBuilder xmlPath = new StringBuilder(string.Format("/{0}", XmlConstants.TAG_ROOT));

            foreach (string folder in folderNames)
            {
                xmlPath.AppendFormat(TAG_WITH_ATTRIBUTE_AND_SLASH_FORMAT, XmlConstants.TAG_FOLDER, XmlConstants.ATTRIBUTE_NAME, folder);
            }

            XPathNodeIterator iterator = this.document.CreateNavigator().Select(xmlPath.ToString());

            iterator.MoveNext();

            return iterator.Current;
        }

        /// <summary>
        /// Remove empty folders
        /// </summary>
        /// <param name="navigator">Navigator to folder</param>
        private void RemoveEmptyFolders(XPathNavigator navigator)
        {
            while (navigator.Name.Equals(XmlConstants.TAG_FOLDER) &&
                   navigator.Select("./*").Count == 0 &&
                   !navigator.GetAttribute(XmlConstants.ATTRIBUTE_MONITORING_MODE, string.Empty).Equals(XmlConstants.MONITORING_MODE_MONITORING))
            {
                navigator.DeleteSelf();
            }
        }

        /// <summary>
        /// Check State of folder based on sub files, should replace recursive call
        /// </summary>
        /// <param name="node">folder node to check</param>
        /// <param name="state">required state</param>
        /// <returns>if one file has required state, return true</returns>
        //private bool CheckFolderStateByFile(XmlNode node, string state)
        //{
        //    XmlNode next = node.FirstChild;
        //    while (next != null)
        //    {
        //        if (next.Name == XmlConstants.TAG_FILE)
        //        {
        //            if (next.Attributes[XmlConstants.ATTRIBUTE_FILE_STATE].Value == state)
        //            {
        //                return true;
        //            }
        //        }
        //        else
        //        {
        //            if (this.CheckFolderStateByFile(next, state))
        //            {
        //                return true;
        //            }
        //        }

        //        next = next.NextSibling;
        //    }

        //    return false;
        //}

        /// <summary>
        /// Is folder pending
        /// </summary>
        /// <param name="navigator">Navigator to folder</param>
        /// <returns>Boolean value</returns>
        private bool IsFolderPending(XPathNavigator navigator)
        {
            //if (navigator is IHasXmlNode)
            //{
            //    XmlNode node = ((IHasXmlNode)navigator).GetNode();
            //    return this.CheckFolderStateByFile(node, XmlConstants.FILE_STATE_PENDING);
            //}

            XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}[@{1} = \"{2}\"]", XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_FILE_STATE, XmlConstants.FILE_STATE_PENDING));

            return iterator.Count > 0;
        }

        /// <summary>
        /// Is folder backed up
        /// </summary>
        /// <param name="navigator">Navigator to folder</param>
        /// <returns>Boolean value</returns>
        private bool IsFolderBackedUp(XPathNavigator navigator)
        {
            //if (navigator is IHasXmlNode)
            //{
            //    XmlNode node = ((IHasXmlNode)navigator).GetNode();
            //    return this.CheckFolderStateByFile(node, XmlConstants.FILE_STATE_BACKED_UP);
            //}

            XPathNodeIterator iterator = navigator.Select(string.Format(".//{0}[@{1} = \"{2}\"]", XmlConstants.TAG_FILE, XmlConstants.ATTRIBUTE_FILE_STATE, XmlConstants.FILE_STATE_BACKED_UP));

            return iterator.Count > 0;
        }

        /// <summary>
        /// Get file path by navigator
        /// </summary>
        /// <param name="navigator">Navigator to file</param>
        /// <returns>Path of the file</returns>
        private string GetFilePath(XPathNavigator navigator)
        {
            string result = navigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty);

            while (navigator.MoveToParent())
            {
                if (navigator.Name.Equals(XmlConstants.TAG_FOLDER))
                {
                    result = string.Concat(navigator.GetAttribute(XmlConstants.ATTRIBUTE_NAME, string.Empty), SEPARATOR, result);
                }
                else
                {
                    break;
                }
            }

            return result;
        }

        #endregion
    }
}