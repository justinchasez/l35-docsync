using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Share430.Client.Core.Args;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Enums;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Utils;
using Share430.Client.FileManager.Constants;
using Share430.Client.FileManager.Watcher;
using Share430.Client.Proxy;
using Share430.Client.Remoting.Managers.Interfaces;

namespace Share430.Client.FileManager
{
    /// <summary>
    /// Class to manage helper files
    /// </summary>
    public class ScheduledFileManager : MarshalByRefObject, IRemoteScheduledFileManager
    {
        #region Private fields

        /// <summary>
        /// Force of file
        /// </summary>
        private const bool FORCE = false;

        /// <summary>
        /// The xml document wrapper
        /// </summary>
        private XmlDocumentWrapper xmlFileRepository;

        /// <summary>
        /// The file watcher
        /// </summary>
        private FileWatcher watcher;

        /// <summary>
        /// Sync object
        /// </summary>
        private static object syncObject = new object();

        /// <summary>
        /// Instance of FileNanager
        /// </summary>
        private static ScheduledFileManager instance;

        #endregion

        #region Events

        /// <summary>
        /// Add file to queue for backup
        /// </summary>
        public event AddFilesToQueueDelegate AddFilesToQueue;

        /// <summary>
        /// Remove file from queue for backup
        /// </summary>
        public event RemoveFilesFromQueueDelegate RemoveFilesFromQueue;

        /// <summary>
        /// Rename files in queue
        /// </summary>
        public event RenameFilesInQueueDelegate RenameFelesInQueue;

        /// <summary>
        /// Get cerrent restoring file
        /// </summary>
        public event GetCurrentRestoringFileDelegate GetCurrentRestoringFele;

        /// <summary>
        /// Remove file from S3 event
        /// </summary>
        public event RemoveFileFromS3Delegate RemoveFileFromS3;

        /// <summary>
        /// Report progress event
        /// </summary>
        public event ReportProgressDelegate ReportProgress;

        public event CreateFolderOnServerDelegate CreateFolderOnServer;
        public event RemoveFolderFromServerDelegate RemoveFolderFromServer;

        public event CancelRestoreFileDelegate CancelRestoreFile;
        public void CreateNewFolder(string path)
        {
            var fullpath = this.GetAbsolutePath(path);

            if (!Directory.Exists(fullpath))
            {
                Directory.CreateDirectory(fullpath);
            }

            if (this.xmlFileRepository.IsFolderInXml(fullpath))
            {
                this.xmlFileRepository.SetFolderMonitoringAttribute(fullpath);
            }
            else
            {
                this.xmlFileRepository.AddFolderInXml(fullpath, XmlConstants.MONITORING_MODE_MONITORING);
            }

            this.watcher.AddFolderToWatch(fullpath);
        }

        #endregion

        /// <summary>
        /// Delegate to do back this up command async
        /// </summary>
        /// <param name="fullpath">full path to target object (file or folder)</param>
        /// <param name="isUnsetMonitoringAttrRequired">is unset monitoring attr required</param>
        internal delegate void DoBackUpCommand(string fullpath, bool isUnsetMonitoringAttrRequired);
        internal delegate void RemoveFromBackupCommand(string fullpath);

        /// <summary>
        /// Get instance of ScheduledFileManager
        /// </summary>
        public static ScheduledFileManager Instance
        {
            get { return instance ?? (instance = new ScheduledFileManager()); }
        }

        /// <summary>
        /// Gets current restoring file
        /// </summary>
        public string CurrentRestoringFilePath
        {
            get
            {
                var restoreFileQueueItem = this.GetCurrentRestoringFele != null ? this.GetCurrentRestoringFele() : null;
                return restoreFileQueueItem != null ? restoreFileQueueItem.RestoreFilePath : null;
            }
        }

        /// <summary>
        /// Prevents a default instance of the ScheduledFileManager class from being created.
        /// </summary>
        private ScheduledFileManager()
        {
            this.xmlFileRepository = new XmlDocumentWrapper();

            // Initialize File Watcher
            FileWatcher.GetTopFolders += this.xmlFileRepository.GetTopLevelFoldersFromXml;
            FileWatcher.GetDisks += this.xmlFileRepository.GetLogicalDiskFromXml;
            this.watcher = new FileWatcher();
            this.watcher.FileChanged += this.FileOrFolderChanged;
            this.watcher.FileCreated += this.FileOrFolderCreated;
            this.watcher.FileDeleted += this.FileOrFolderDeleted;
            this.watcher.FileRenamed += this.FileOrFolderRenamed;
        }

        #region FileWatcher events handlers

        /// <summary>
        /// File or folder created event handler
        /// </summary>
        /// <param name="path">path to file or folder</param>
        private void FileOrFolderCreated(string path)
        {
            if (!SettingsManager.Settings.IsPathIncluded(path))
            {
                return;
            }

            if (!path.Equals(this.CurrentRestoringFilePath))
            {
                lock (syncObject)
                {
                    try
                    {
                        if ((File.GetAttributes(path) & FileAttributes.Directory) != FileAttributes.Directory)
                        {
                            if (this.xmlFileRepository.IsFolderMonitored(Path.GetDirectoryName(path)))
                            {
                                this.xmlFileRepository.AddFileInXml(path, XmlConstants.FILE_STATE_PENDING, 0, string.Empty, 0, true);

                                if (this.AddFilesToQueue != null)
                                {
                                    this.AddFilesToQueue(new string[] { path }, FORCE);
                                }

                                this.xmlFileRepository.Save();
                            }
                        }
                        else
                        {
                            if (this.xmlFileRepository.IsFolderMonitored(path))
                            {
                                if (this.CreateFolderOnServer != null)
                                    this.CreateFolderOnServer(path);

                                this.xmlFileRepository.AddFolderInXml(path, string.Empty);
                                this.xmlFileRepository.Save();
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        /// <summary>
        /// File or folder deleted event handler
        /// </summary>
        /// <param name="path">path to file or folder</param>
        private void FileOrFolderDeleted(string path)
        {
            if (!SettingsManager.Settings.IsPathIncluded(path))
            {
                return;
            }
            
            if (!path.Equals(this.CurrentRestoringFilePath))
            {
                try
                {
                    this.RemoveFileOrFolder(path);
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// File or folder changed event handler
        /// </summary>
        /// <param name="path">path to file or folder</param>
        private void FileOrFolderChanged(string path)
        {   
            if (!SettingsManager.Settings.IsPathIncluded(path))
            {
                return;
            }
            
            if (!path.Equals(this.CurrentRestoringFilePath))
            {
                try
                {
                    FileAttributes curFileAttr = File.GetAttributes(path);

                    if ((curFileAttr & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        lock (syncObject)
                        {
                            try
                            {
                                if ((curFileAttr & FileAttributes.Directory) != FileAttributes.Directory)
                                {
                                    if (!this.xmlFileRepository.IsFilePending(path) &&
                                        this.xmlFileRepository.IsFileMonitored(path))
                                    {
                                        if (this.xmlFileRepository.IsFileInXml(path))
                                        {
                                            this.xmlFileRepository.SetPendingAttribute(path);
                                        }
                                        else
                                        {
                                            this.xmlFileRepository.AddFileInXml(path, XmlConstants.FILE_STATE_PENDING, 0, string.Empty, 0, true);
                                        }

                                        if (this.AddFilesToQueue != null)
                                        {
                                            this.AddFilesToQueue(new string[] { path }, FORCE);
                                        }

                                        this.xmlFileRepository.Save();
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                    else
                    {
                        this.OnDontBackThisUp(path, false);
                    }
                }
                catch (IOException)
                {
                }
            }
        }

        /// <summary>
        /// File or folder renamed event handler
        /// </summary>
        /// <param name="oldPath">old path to file or folder</param>
        /// <param name="newPath">new path to file or folder</param>
        private void FileOrFolderRenamed(string oldPath, string newPath)
        {
            if (!SettingsManager.Settings.IsPathIncluded(newPath))
            {
                this.FileOrFolderDeleted(oldPath);
                return;
            }

            lock (syncObject)
            {
                try
                {
                    if ((File.GetAttributes(newPath) & FileAttributes.Directory) != FileAttributes.Directory)
                    {
                        if (this.xmlFileRepository.IsFileMonitored(oldPath))
                        {
                            if (!this.xmlFileRepository.IsFileInXml(newPath))
                            {
                                if (this.xmlFileRepository.IsFileInXml(oldPath))
                                {
                                    this.xmlFileRepository.FileRename(oldPath, newPath);
                                    this.xmlFileRepository.Save();

                                    if (this.RenameFelesInQueue != null)
                                    {
                                        Dictionary<string, string> file = new Dictionary<string, string>();
                                        file.Add(oldPath, newPath);
                                        this.RenameFelesInQueue(file);
                                    }
                                }
                                else
                                {
                                    this.xmlFileRepository.AddFileInXml(newPath, XmlConstants.FILE_STATE_PENDING, 0, string.Empty, 0, true);

                                    if (this.AddFilesToQueue != null)
                                    {
                                        this.AddFilesToQueue(new string[] { newPath }, FORCE);
                                    }

                                    this.xmlFileRepository.Save();
                                }
                            }
                            else
                            {
                                if (this.xmlFileRepository.IsFileInXml(oldPath))
                                {
                                    this.DontBackThisUp(oldPath);
                                }

                                this.BackThisUpCommand(newPath);
                            }
                        }
                        else
                        {
                            
                        }
                    }
                    else
                    {
                        if (this.xmlFileRepository.IsFolderInXml(oldPath))
                        {
                            Dictionary<string, string> files = this.xmlFileRepository.FolderRename(oldPath, newPath);
                            this.xmlFileRepository.Save();

                            if (this.RenameFelesInQueue != null)
                            {
                                this.RenameFelesInQueue(files);
                            }

                            if (this.RemoveFolderFromServer != null)
                                this.RemoveFolderFromServer(oldPath);

                            if (this.CreateFolderOnServer != null)
                                this.CreateFolderOnServer(newPath);
                        }
                    }
                }
                catch
                {
                }
            }
        }

        #endregion

        #region Interface implementation

        /// <summary>
        /// Check if the specify folder contains at least one file with specify state
        /// </summary>
        /// <param name="fullPath">full path to specify folder</param>
        /// <param name="entryState">state of the file</param>
        /// <returns>return finding result</returns>
        public bool ContainsFilesWithState(string fullPath, BackupState entryState)
        {
            return this.xmlFileRepository.IsFolderInState(fullPath, entryState);
        }

        /// <summary>
        /// File upload complete handler
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">Event handler params</param>
        public void FileUploadCompleteHandler(object sender, UploadFileEventArgs e)
        {
            lock (syncObject)
            {
                this.xmlFileRepository.SetBackedUpAttribute(GetAbsolutePath(e.FileInfo.FullName), e.FileInfo.LastWriteTime, e.FileInfo.Length, e.VersionsCount);
                this.xmlFileRepository.Save();
            }
        }

        /// <summary>
        /// Gets the file link.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <returns>The file link</returns>
        public string GetFileLink(string fullPath)
        {
            return ServiceManager.FilesService.GetFileLink(new BackupFileInputs
                                                               {
                                                                   ComputerId = 0,
                                                                   ComputerIdSpecified = true,
                                                                   File = fullPath
                                                               });
        }

        /// <summary>
        /// Gets the entries.
        /// </summary>
        /// <param name="fullPath">The full path.</param>
        /// <param name="type">The type of entry.</param>
        /// <returns>The list of entries name.</returns>
        public string[] GetEntries(string fullPath, int type)
        {
            lock (syncObject)
            {
                string fileState = type == 1 ? XmlConstants.FILE_STATE_BACKED_UP : XmlConstants.FILE_STATE_PENDING;
                return this.xmlFileRepository.GetEntriesFromFolder(fullPath, fileState);
            }
        }

        public void RemoveFileOrFolder(string fullPath)
        {
            RemoveFromBackupCommand removeFromBackupCommand = RemoveFromBackupCommandHandler;
            removeFromBackupCommand.BeginInvoke(fullPath, OnRemoveFromBackUpCommandComplete, removeFromBackupCommand);
        }

        public void RemoveFromBackupCommandHandler(string fullPath)
        {
            fullPath = GetAbsolutePath(fullPath);
            if (this.xmlFileRepository.IsFileInXml(fullPath))
            {
                DeleteFileFromBackup(fullPath);
            }
            else  if (this.xmlFileRepository.IsFolderInXml(fullPath))
            {
                IList<string> files = this.xmlFileRepository.GetAllFilesFromFolder(fullPath);
                foreach (var file in files)
                {
                    DeleteFileFromBackup(file);
                }

                if (this.RemoveFolderFromServer != null)
                    this.RemoveFolderFromServer(fullPath);
            }
        }

        public bool RemoveFileOrFolderFromFileSystem(string fullPath)
        {
            fullPath = GetAbsolutePath(fullPath);

            if (this.xmlFileRepository.IsFileInXml(fullPath) && File.Exists(fullPath) && (File.GetAttributes(fullPath) & FileAttributes.Directory) != FileAttributes.Directory)
            {
                DeleteFileFromBackup(fullPath, true);

                if (fullPath == this.CurrentRestoringFilePath)
                {
                    if (this.CancelRestoreFile != null)
                    {
                        this.CancelRestoreFile(fullPath);
                    }
                }
                else
                {
                    File.Delete(fullPath);
                }
                return true;
            }
            else if (this.xmlFileRepository.IsFolderInXml(fullPath))
            {
                if (!Directory.Exists(fullPath))
                {
                    return true;
                }

                IList<string> files = this.xmlFileRepository.GetAllFilesFromFolder(fullPath);
                foreach (var file in files)
                {
                    DeleteFileFromBackup(file, true);
                }
                Directory.Delete(fullPath, true);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Event handler on back this up command
        /// </summary>
        /// <param name="fullpath">full path to target object (file or folder)</param>
        /// <param name="isUnmonitorRequired">if unmonitor required</param>
        public void OnBackThisUpCommand(string fullpath, bool isUnmonitorRequired)
        {
            lock (syncObject)
            {
                fullpath = GetAbsolutePath(fullpath);

                if ((File.GetAttributes(fullpath) & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    if (this.AddFileInRepositoryAsPanding(fullpath))
                    {
                        this.xmlFileRepository.Save();
                        this.watcher.AddFolderToWatch(Path.GetDirectoryName(fullpath));

                        if (this.AddFilesToQueue != null)
                        {
                            this.AddFilesToQueue(new string[] { fullpath }, FORCE);
                        }
                    }
                }
                else
                {
                    if (this.xmlFileRepository.IsFolderInXml(fullpath))
                    {
                        this.xmlFileRepository.SetFolderMonitoringAttribute(fullpath);
                    }
                    else
                    {
                        this.xmlFileRepository.AddFolderInXml(fullpath, XmlConstants.MONITORING_MODE_MONITORING);
                    }

                    this.watcher.AddFolderToWatch(fullpath);

                    string[] filePathes = new string[0];

                    try
                    {
                        filePathes = Directory.GetFiles(fullpath, "*", SearchOption.AllDirectories);
                    }
                    catch (Exception e)
                    {
                        Loger.Instance.Log("File mahager: " + e);
                        return;
                    }
                    
                    List<string> filesToAdd = new List<string>();

                    foreach (string filePath in filePathes)
                    {
                        try
                        {
                            if ((File.GetAttributes(filePath) & FileAttributes.Hidden) != FileAttributes.Hidden)
                            {
                                if (this.AddFileInRepositoryAsPanding(filePath))
                                {
                                    filesToAdd.Add(filePath);
                                }
                            }
                        }
                        catch (IOException)
                        {
                        }
                    }

                    this.xmlFileRepository.Save();

                    if (this.AddFilesToQueue != null)
                    {
                        this.AddFilesToQueue(filesToAdd.ToArray(), FORCE);
                    }
                }
            }
        }

        /// <summary>
        /// Notificate when OnBackUpCommand compeleted
        /// </summary>
        /// <param name="ar">async result</param>
        public void OnBackUpCommandComplete(IAsyncResult ar)
        {
            DoBackUpCommand backUpCommand = (DoBackUpCommand)ar.AsyncState;

            try
            {
                backUpCommand.EndInvoke(ar);
            }
            catch
            {
                // do nothing
            }
        }

        public void OnRemoveFromBackUpCommandComplete(IAsyncResult ar)
        {
            RemoveFromBackupCommand backUpCommand = (RemoveFromBackupCommand)ar.AsyncState;

            try
            {
                backUpCommand.EndInvoke(ar);
            }
            catch
            {
                // do nothing
            }
        }

        /// <summary>
        /// Schedule file or folder for back up
        /// </summary>
        /// <param name="fullPath">full path to file or folder</param>
        public void BackThisUpCommand(string fullPath)
        {
            DoBackUpCommand backUpCommand = this.OnBackThisUpCommand;
            backUpCommand.BeginInvoke(fullPath, true, this.OnBackUpCommandComplete, backUpCommand);
        }

        /// <summary>
        /// Event handler on dont back this up command
        /// </summary>
        /// <param name="fullPath">full path to file or folder</param>
        /// <param name="unsetMonitoringAttrRequired">unset monitoring attr required</param>
        public void OnDontBackThisUp(string fullPath, bool unsetMonitoringAttrRequired)
        {
            lock (syncObject)
            {
                fullPath = GetAbsolutePath(fullPath);

                if (this.xmlFileRepository.IsFileInXml(fullPath) && File.Exists(fullPath) && (File.GetAttributes(fullPath) & FileAttributes.Directory) != FileAttributes.Directory)
                {
                    if (this.xmlFileRepository.IsFilePending(fullPath) && this.RemoveFilesFromQueue != null)
                    {
                        this.RemoveFilesFromQueue(new string[] { fullPath });
                    }

                    if (unsetMonitoringAttrRequired)
                    {
                        this.xmlFileRepository.UnsetFileMonitoringAttribute(fullPath);
                    }

                    string dirName = Path.GetDirectoryName(fullPath);

                    if (Directory.Exists(dirName))
                    {
                        try
                        {
                            if (Directory.GetFiles(dirName, "*", SearchOption.AllDirectories).Length == 0
                                && Directory.GetDirectories(dirName, "*", SearchOption.AllDirectories).Length == 0)
                            {
                                this.watcher.RemoveFolderFromWatch(dirName);
                            }
                        }
                        catch (Exception)
                        {
                            this.watcher.RemoveFolderFromWatch(dirName);
                        }
                    }
                }
                else if (this.xmlFileRepository.IsFolderInXml(fullPath))
                {
                    this.watcher.RemoveFolderFromWatch(fullPath);
                    //string[] files = Directory.GetFiles(fullPath, "*", SearchOption.AllDirectories);
                    IList<string> files = this.xmlFileRepository.GetAllFilesFromFolder(fullPath);
                    List<string> filesToRemove = new List<string>();

                    foreach (string file in files)
                    {
                        if (this.xmlFileRepository.IsFileInXml(file) && this.xmlFileRepository.IsFilePending(file))
                        {
                            filesToRemove.Add(file);
                        }
                    }

                    if (this.RemoveFilesFromQueue != null)
                    {
                        this.RemoveFilesFromQueue(filesToRemove.ToArray());
                    }

                    if (unsetMonitoringAttrRequired)
                    {
                        this.xmlFileRepository.UnsetFileMonitoringAttributeInAllFiles(fullPath);
                    }
                }

                this.xmlFileRepository.Save();
            }
        }

        /// <summary>
        /// Unschedule file or folder for back up
        /// </summary>
        /// <param name="fullPath">The full entry path.</param>
        public void DontBackThisUp(string fullPath)
        {
            DoBackUpCommand backUpCommand = this.OnDontBackThisUp;
            backUpCommand.BeginInvoke(fullPath, true, this.OnBackUpCommandComplete, backUpCommand);
        }

        /// <summary>
        /// Check file or folder
        /// </summary>
        /// <param name="fullPath">Path to file or folder</param>
        /// <param name="state">File or folder state</param>
        /// <param name="isScheduled">Is file or folder scheduled</param>
        /// <param name="verCount">File versions count</param>
        /// <returns>Is folder or file exist in XML</returns>
        public bool Check(string fullPath, ref BackupState state, ref bool isScheduled, ref int verCount)
        {
            lock (syncObject)
            {
                bool result = true;
                fullPath = GetAbsolutePath(fullPath);

                if (!Path.GetPathRoot(fullPath).Equals(fullPath))
                {
                    if (this.xmlFileRepository.IsFolderInXml(fullPath))
                    {
                        state = BackupState.Backedup;
                        isScheduled = false;
                        verCount = 0;
                        this.xmlFileRepository.CheckFolder(fullPath, ref state, ref isScheduled);
                    }
                    else if (this.xmlFileRepository.IsFileInXml(fullPath))
                    {
                        this.xmlFileRepository.CheckFile(fullPath, ref state, ref isScheduled, ref verCount);
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }

                return result;
            }
        }

        /// <summary>
        /// Get backed up files for search
        /// </summary>
        /// <returns>DataTable of backed up files</returns>
        public DataTable GetBackedUpFilesForSearch()
        {
            DataTable result;

            lock (syncObject)
            {
                result = this.xmlFileRepository.GetBackedUpFilesForSearch();
            }

            return result;
        }

        /// <summary>
        /// Get file info from XML
        /// </summary>
        /// <param name="pathToFile">Full path to file</param>
        /// <returns>XML file info object</returns>
        public XmlFileInfo GetFileInfo(string pathToFile)
        {
            XmlFileInfo result = null;
            pathToFile = GetAbsolutePath(pathToFile);
            lock (syncObject)
            {
                if (this.xmlFileRepository.IsFileInXml(pathToFile))
                {
                    result = this.xmlFileRepository.GetFileInfo(pathToFile);
                }
            }

            return result;
        }

        /// <summary>
        /// Delete file from S3 server
        /// </summary>
        /// <param name="pathToFile">Path to file</param>
        /// <returns>Is delete successed</returns>
        public bool DeleteFileFromBackup(string pathToFile, bool skipServerSide = false)
        {
            bool result = skipServerSide || (this.RemoveFileFromS3 != null && this.RemoveFileFromS3(pathToFile));

            if (result)
            {
                pathToFile = GetAbsolutePath(pathToFile);
                lock (syncObject)
                {

                    if (this.xmlFileRepository.IsFileInXml(pathToFile) && File.Exists(pathToFile) && (File.GetAttributes(pathToFile) & FileAttributes.Directory) != FileAttributes.Directory)
                    {
                        this.xmlFileRepository.RemoveFileFromXml(pathToFile);
                        this.xmlFileRepository.Save();
                    }
                    else if (this.xmlFileRepository.IsFolderInXml(pathToFile))
                    {
                        this.xmlFileRepository.RemoveFolderFromXml(pathToFile);
                        this.xmlFileRepository.Save();
                    }
                }

                if (this.ReportProgress != null)
                {
                    this.ReportProgress();
                }
            }

            return result;
        }

        /// <summary>
        /// Update XML file repository from server
        /// </summary>
        /// <param name="files">Files from server</param>
        public void SetFilesInXml(ComputerFilesDto files)
        {
            foreach (var file in files.FilesInfo)
            {
                file.FileName = GetAbsolutePath(file.FileName);
            }
            this.xmlFileRepository.SetFilesInXml(files);
            this.watcher.Init();
        }

        /// <summary>
        /// Get Count Of Backed Up Files
        /// </summary>
        /// <returns>Count Of Backed Up Files</returns>
        public int GetCountOfBackedUpFiles()
        {
            int result;

            lock (syncObject)
            {
                result = this.xmlFileRepository.GetCountOfBackedUpFiles();
            }

            return result;
        }

        /// <summary>
        /// Get the type of path.
        /// </summary>
        /// <param name="path">The checked path.</param>
        /// <returns>The path type value</returns>
        public PathType GetPathType(string path)
        {
            path = GetAbsolutePath(path);

            if (this.xmlFileRepository.IsFileInXml(path))
            {
                return PathType.File;
            }

            if (this.xmlFileRepository.IsFolderInXml(path))
            {
                return PathType.Folder;
            }

            return PathType.NotFound;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Add file in repository as panding
        /// </summary>
        /// <param name="path">Full path to file</param>
        /// <returns>Is file added</returns>
        private bool AddFileInRepositoryAsPanding(string path)
        {
            bool result = false;
            path = GetAbsolutePath(path);

            if (this.xmlFileRepository.IsFileInXml(path))
            {
                this.xmlFileRepository.SetPendingAttribute(path);
                result = true;
            }
            else
            {
                this.xmlFileRepository.AddFileInXml(path, XmlConstants.FILE_STATE_PENDING, 0, string.Empty, 0, true);

                result = true;
            }

            return result;
        }

        private string GetAbsolutePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return path;
            } else if (Path.IsPathRooted(path))
            {
                // wTF?
                Loger.Instance.Log("File manager: Found path with abother root: " + path);
                return path;
            }

            return Path.Combine(basePath, path);
        }

        #endregion

        public static void Initialize()
        {
            // Initialize File Watcher
            FileWatcher.GetTopFolders -= instance.xmlFileRepository.GetTopLevelFoldersFromXml;
            FileWatcher.GetDisks -= instance.xmlFileRepository.GetLogicalDiskFromXml;

            instance.xmlFileRepository = new XmlDocumentWrapper();
            // Initialize File Watcher
            FileWatcher.GetTopFolders += instance.xmlFileRepository.GetTopLevelFoldersFromXml;
            FileWatcher.GetDisks += instance.xmlFileRepository.GetLogicalDiskFromXml;

            instance.watcher.Init();
            var basePath = SettingsManager.Settings.TargetFolderLocation;

            lock(syncObject) 
            {
                if (instance.xmlFileRepository.IsFolderInXml(basePath))
                {
                    instance.xmlFileRepository.SetFolderMonitoringAttribute(basePath);
                }
                else
                {
                    instance.xmlFileRepository.AddFolderInXml(basePath, XmlConstants.MONITORING_MODE_MONITORING);
                }
            }
        }
    }
}
