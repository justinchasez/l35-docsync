using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Share430.Client.Core.Utils;

namespace Share430.Client.FileManager.Watcher
{
    /// <summary>
    /// Class to watch for file system
    /// </summary>
    public class FileWatcher
    {
        #region Events

        /// <summary>
        /// Get top folders event
        /// </summary>
        public static event GetTopFoldersDelegate GetTopFolders;

        /// <summary>
        /// Get disc from XML
        /// </summary>
        public static event GetTopFoldersDelegate GetDisks;

        /// <summary>
        /// File or folder created event
        /// </summary>
        public event FileOrFolderActionDelegate FileCreated;

        /// <summary>
        /// File or folder deleted event
        /// </summary>
        public event FileOrFolderActionDelegate FileDeleted;

        /// <summary>
        /// File or folder renamed
        /// </summary>
        public event FileOrFolderRenamedDelegate FileRenamed;

        /// <summary>
        /// File changed
        /// </summary>
        public event FileOrFolderActionDelegate FileChanged;

        #endregion

        /// <summary>
        /// list of watchers
        /// </summary>
        private IList<FileSystemWatcher> watchers;

        /// <summary>
        /// list fo disk watchers
        /// </summary>
        private IList<FileSystemWatcher> diskWatchers;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileWatcher"/> class.
        /// </summary>
        public FileWatcher()
        {
            this.Init();
        }

        /// <summary>
        /// Add folder to watch
        /// </summary>
        /// <param name="folderPath">Path to folder</param>
        public void AddFolderToWatch(string folderPath)
        {
            if (folderPath.Length > 3)
            {
                bool isFolderWatched = false;

                foreach (FileSystemWatcher watcher in this.watchers)
                {
                    isFolderWatched = isFolderWatched || folderPath.IndexOf(watcher.Path) > -1;
                }

                if (!isFolderWatched)
                {
                    string rootPath = string.Empty;

                    while (rootPath.Length == 0)
                    {
                        string p = Path.GetDirectoryName(folderPath);
                        if (p.Length <= 3)
                        {
                            rootPath = folderPath;
                        }
                        else
                        {
                            folderPath = Path.GetDirectoryName(folderPath);
                        }
                    }

                    this.InitFileSystemWatcher(rootPath);
                }
            }
        }

        /// <summary>
        /// Remove folder from watch
        /// </summary>
        /// <param name="folderPath">Path to folder</param>
        public void RemoveFolderFromWatch(string folderPath)
        {
            IList<FileSystemWatcher> watchersForRemove = new List<FileSystemWatcher>();

            foreach (FileSystemWatcher watcher in this.watchers)
            {
                if (watcher.Path.ToLower().Contains(folderPath.ToLower().TrimEnd('\\')))
                {
                    watcher.EnableRaisingEvents = false;
                    watchersForRemove.Add(watcher);
                }
            }

            foreach (FileSystemWatcher watcher in watchersForRemove)
            {
                this.watchers.Remove(watcher);
                watcher.Dispose();
            }

            Thread.Sleep(50);

            this.InitFileSystemWatcher(folderPath);
        }

        #region File watchers events handlers
        
        /// <summary>
        /// FileSystemWatcher event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">File system event arguments</param>
        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (this.FileChanged != null)
            {
                this.FileChanged(e.FullPath);
            }
        }

        /// <summary>
        /// FileSystemWatcher event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">File system event arguments</param>
        private void Watcher_Renamed(object sender, RenamedEventArgs e)
        {
            try
            {
                Thread.Sleep(50);
                if ((File.GetAttributes(e.FullPath) & FileAttributes.Hidden) != FileAttributes.Hidden && this.FileRenamed != null)
                {
                    this.FileRenamed(e.OldFullPath, e.FullPath);
                }
            }
            catch (IOException)
            {
            }
        }

        /// <summary>
        /// FileSystemWatcher event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">File system event arguments</param>
        private void Watcher_Deleted(object sender, FileSystemEventArgs e)
        {
            if (this.FileDeleted != null)
            {
                this.FileDeleted(e.FullPath);
            }
        }

        /// <summary>
        /// FileSystemWatcher event handler
        /// </summary>
        /// <param name="sender">sender of the event</param>
        /// <param name="e">File system event arguments</param>
        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                if ((File.GetAttributes(e.FullPath) & FileAttributes.Hidden) != FileAttributes.Hidden &&
                    this.FileCreated != null)
                {
                    this.FileCreated(e.FullPath);
                }
            }
            catch (IOException)
            {
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Init watchers
        /// </summary>
        public void Init()
        {
            Loger.Instance.Log("File watcher: Start initialize file watchers");

            IList<string> folders = GetTopFolders();
           
            if (this.watchers != null)
            {
                foreach (FileSystemWatcher w in this.watchers)
                {
                    w.Dispose();
                }
            }

            if (this.diskWatchers != null)
            {
                foreach (FileSystemWatcher w in this.diskWatchers)
                {
                    w.Dispose();
                }
            }

            this.watchers = new List<FileSystemWatcher>();

            foreach (string folder in folders)
            {
                this.InitFileSystemWatcher(folder);
            }

            this.InitFileSystemWatchersForDisks();

            ThreadPool.QueueUserWorkItem(x => this.VerifyDisksTypes());

            Loger.Instance.Log("File watcher: Initialize file watchers finished");
        }

        /// <summary>
        /// Verify disk types
        /// </summary>
        private void VerifyDisksTypes()
        {
            Loger.Instance.Log("File watcher: Verify Disks Types");

            Dictionary<string, uint> disks = DiskUtil.DisksTypes;

            if (disks != null)
            {
                List<string> notHddDisks = new List<string>();

                foreach (KeyValuePair<string, uint> disk in disks)
                {
                    if (disk.Value != 3)
                    {
                        notHddDisks.Add(disk.Key);
                    }
                }

                List<FileSystemWatcher> watchersForRemove = new List<FileSystemWatcher>();

                foreach (FileSystemWatcher watcher in this.watchers)
                {
                    if (notHddDisks.Contains(watcher.Path.Substring(0, 2)))
                    {
                        watchersForRemove.Add(watcher);
                    }
                }

                foreach (FileSystemWatcher watcher in watchersForRemove)
                {
                    Loger.Instance.Log("File watcher: Remove watcher  " + watcher.Path);
                    this.watchers.Remove(watcher);
                    watcher.Dispose();
                }

                watchersForRemove = new List<FileSystemWatcher>();

                foreach (FileSystemWatcher watcher in this.diskWatchers)
                {
                    if (notHddDisks.Contains(watcher.Path.Substring(0, 2)))
                    {
                        watchersForRemove.Add(watcher);
                    }
                }

                foreach (FileSystemWatcher watcher in watchersForRemove)
                {
                    Loger.Instance.Log("File watcher: Remove watcher  " + watcher.Path);
                    this.diskWatchers.Remove(watcher);
                    watcher.Dispose();
                }
            }
            else
            {
                Loger.Instance.Log("File watcher: Can not load disk types");
            }

            Loger.Instance.Log("File watcher: Verify Disks Types complete");
        }

        /// <summary>
        /// Init one watcher
        /// </summary>
        /// <param name="folderPath">Path to folder</param>
        private void InitFileSystemWatcher(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                Loger.Instance.Log("File watcher: start add folder " + folderPath + " to watch");

                FileSystemWatcher watcher = new FileSystemWatcher();

                try
                {
                    watcher.Path = folderPath;
                    watcher.IncludeSubdirectories = true;
                    watcher.NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.Size |
                                           NotifyFilters.Attributes;

                    watcher.Created += this.Watcher_Created;
                    watcher.Deleted += this.Watcher_Deleted;
                    watcher.Renamed += this.Watcher_Renamed;
                    watcher.Changed += this.Watcher_Changed;

                    watcher.Error += this.Watcher_Error;

                    watcher.EnableRaisingEvents = true;

                    this.watchers.Add(watcher);

                    Loger.Instance.Log("File watcher: Folder " + folderPath + " was added to watch");
                }
                catch (Exception ex)
                {
                    Loger.Instance.Log("File watcher: Folder " + folderPath + " throw exception: " + ex);
                }
            }
        }

        /// <summary>
        /// Error of the file watchers
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">The event arguments</param>
        private void Watcher_Error(object sender, ErrorEventArgs e)
        {
            FileSystemWatcher curWatcher = (FileSystemWatcher)sender;
            string path = curWatcher.Path;

            curWatcher.Dispose();
            this.watchers.Remove(curWatcher);

            //Loger.Instance.Log("File watcher: Error event. Sender: " + path);
            //Loger.Instance.Log("File watcher: Error event. Exception: " + e.GetException());

           // Win32Exception ex = e.GetException() as Win32Exception;

            //if (ex != null)
            //{
            //    Loger.Instance.Log("File watcher: Error event. Inner data: ");
            //    foreach (object o in ex.Data.Keys)
            //    {
            //        Loger.Instance.Log(o + " -> " + ex.Data[o]);
            //    }
            //}

            Thread.Sleep(100);

            this.InitFileSystemWatcher(path);
        }

        /// <summary>
        /// Init File System Watchers To Disks
        /// </summary>
        private void InitFileSystemWatchersForDisks()
        {
            this.diskWatchers = new List<FileSystemWatcher>();

            if (GetDisks != null)
            {
                IList<string> disks = GetDisks();

                foreach (string disk in disks)
                {
                    if (Directory.Exists(disk))
                    {
                        FileSystemWatcher watcher = new FileSystemWatcher();

                        watcher.Path = string.Format("{0}\\", disk);
                        watcher.IncludeSubdirectories = false;
                        watcher.NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.Size | NotifyFilters.Attributes;

                        watcher.Created += this.Watcher_Created;
                        watcher.Deleted += this.Watcher_Deleted;
                        watcher.Renamed += this.Watcher_Renamed;
                        watcher.Changed += this.Watcher_Changed;

                        watcher.EnableRaisingEvents = true;

                        this.diskWatchers.Add(watcher);

                        Loger.Instance.Log("File watcher: Disk " + disk + "was added to watch");
                    }
                }
            }
        }

        #endregion
    }
}
