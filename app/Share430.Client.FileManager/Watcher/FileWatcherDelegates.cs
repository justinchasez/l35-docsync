using System.Collections.Generic;

namespace Share430.Client.FileManager.Watcher
{
    /// <summary>
    /// Receive top folders list delegate
    /// </summary>
    /// <returns>top folders list</returns>
    public delegate IList<string> GetTopFoldersDelegate();

    /// <summary>
    /// File Created Delegate
    /// </summary>
    /// <param name="path">path to file or folder</param>
    public delegate void FileOrFolderActionDelegate(string path);

    /// <summary>
    /// File Renamed Delegate
    /// </summary>
    /// <param name="oldPath">old path to file or folder</param>
    /// <param name="newPath">new path to file or folder</param>
    public delegate void FileOrFolderRenamedDelegate(string oldPath, string newPath);
}
