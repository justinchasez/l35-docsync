namespace DocSync.Client.CppProxyServer.Entities
{
    public enum BackupStateCppProxy
    {
        /// <summary>
        /// backed up file
        /// </summary>
        Backedup = 1,

        /// <summary>
        /// file in queue to back up
        /// </summary>
        Pending = 2,

        /// <summary>
        /// RecoveryLog root
        /// </summary>
        RecoveryLog = 3,

        /// <summary>
        /// Recovery logs: Completed restoring 
        /// </summary>
        RecoveryLogCompleted = 4,
            
        /// <summary>
        /// Recovery logs: Errors logs 
        /// </summary>
        RecoveryLogErrors = 5,

        /// <summary>
        /// Recovery logs: Pending restore files 
        /// </summary>
        RecoveryLogPending = 6
    }
}