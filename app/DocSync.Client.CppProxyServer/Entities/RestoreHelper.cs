using System;
using System.Windows.Forms;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Entities.Settings;
using DocSync.Client.CppProxyServer.Managers;

namespace DocSync.Client.CppProxyServer.Entities
{
    public class RestoreHelper
    {
        public string FullPath { get; set; }
        public int Window { get; set; }
        public bool Result { get; set; }
        public string RestorePath { get; set; }

        [STAThread]
        public void RestorePrevVersion()
        {
            SettingsInfo info = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
            FileInfoDto version = RemotingManager.Instance.ProxyManager.BackupManager.GetFileInfo(this.FullPath,
                                                                                                  info.CurrentComputer.Id);

            version.FileName = this.FullPath;
            FileVersion dialog = new FileVersion(version);
            WindowWrapper parent = new WindowWrapper((IntPtr)this.Window);
            if (dialog.ShowDialog(parent) == DialogResult.OK)
            {
                RemotingManager.Instance.ProxyManager.BackupManager.RestoreFile(info.CurrentComputer.Id,
                                                                                info.CurrentComputerEncryptInfo,
                                                                                this.FullPath,
                                                                                dialog.SavePath,
                                                                                dialog.SelectedVersion.VersionNumber,
                                                                                false);
            }
        }

        [STAThread]
        public void RestoreTo()
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();
                        browserDialog.ShowNewFolderButton = true;
                        browserDialog.SelectedPath = this.FullPath;
                        browserDialog.Description = "Select target folder to Restore";

            WindowWrapper parent = new WindowWrapper((IntPtr)this.Window);

            this.Result = browserDialog.ShowDialog(parent) == DialogResult.OK;
            this.RestorePath = browserDialog.SelectedPath;
        }
    }
}