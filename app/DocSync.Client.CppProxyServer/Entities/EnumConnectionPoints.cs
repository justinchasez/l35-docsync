using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace DocSync.Client.CppProxyServer.Entities
{
    public class EnumConnectionPoints : IEnumConnectionPoints
    {
        private IConnectionPoint connectionPoint;
        private int position;

        public EnumConnectionPoints(IConnectionPoint point)
        {
            connectionPoint = point;
            position = 0;
        }

        public int Next(int celt, IConnectionPoint[] rgelt, IntPtr pceltFetched)
        {
            if (this.position == 0 && celt == 1)
            {
                rgelt = new IConnectionPoint[1];
                rgelt[0] = connectionPoint;
                Marshal.WriteInt32(pceltFetched, 0, 1);
                this.position = 1;
                return 0;
            }
            return 1;
        }

        public int Skip(int celt)
        {
            if (this.position == 0 && celt == 1)
            {
                this.position += celt;
                return 0;
            }
            return 1;
        }

        public void Reset()
        {
            this.position = 0;
        }

        public void Clone(out IEnumConnectionPoints ppenum)
        {
            ppenum = new EnumConnectionPoints(connectionPoint);
        }
    }
}