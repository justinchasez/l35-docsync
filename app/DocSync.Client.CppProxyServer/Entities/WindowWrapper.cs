using System;
using System.Windows.Forms;

namespace DocSync.Client.CppProxyServer.Entities
{
    public class WindowWrapper : IWin32Window
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="handle">handle of the window</param>
        public WindowWrapper(IntPtr handle)
        {
            this._handle = handle;
        }

        /// <summary>
        /// Handle of the window
        /// </summary>
        public IntPtr Handle
        {
            get { return this._handle; }
        }

        /// <summary>
        /// Handle of window
        /// </summary>
        private IntPtr _handle;
    }
}