using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using BackupDutyClientCppProxyLib;
using CONNECTDATA = System.Runtime.InteropServices.ComTypes.CONNECTDATA;

namespace DocSync.Client.CppProxyServer.Entities
{
    public class ConnectionPoint : IConnectionPoint, IEnumConnections, _ICppProxyEvents
    {
        private readonly Dictionary<int, object> _connections;
        private readonly IConnectionPointContainer _pointContainer;
        private Dictionary<int, object>.Enumerator _enumerator;
        private bool isActualEnum;

        public ConnectionPoint(IConnectionPointContainer container)
        {
            this._connections = new Dictionary<int, object>();
            this._pointContainer = container;
            this.isActualEnum = false;
        }

        public void GetConnectionInterface(out Guid pIID)
        {
            pIID = typeof (_ICppProxyEvents).GUID;
        }

        public void GetConnectionPointContainer(out IConnectionPointContainer ppCPC)
        {
            ppCPC = this._pointContainer;
        }

        public void Advise(object pUnkSink, out int pdwCookie)
        {
            isActualEnum = false;
            pdwCookie = pUnkSink.GetHashCode();
            this._connections.Add(pdwCookie, pUnkSink);
        }

        public void Unadvise(int dwCookie)
        {
            isActualEnum = false;
            this._connections.Remove(dwCookie);
        }

        public void EnumConnections(out IEnumConnections ppEnum)
        {
            ppEnum = this;
        }

        public int Next(int celt, CONNECTDATA[] rgelt, IntPtr pceltFetched)
        {
            if (!isActualEnum)
            {
                this._enumerator = this._connections.GetEnumerator();
                isActualEnum = true;
            }

            List<CONNECTDATA> data = new List<CONNECTDATA>();
            int nIndex = 0;
            while (this._enumerator.MoveNext() && nIndex++<celt)
            {
                data.Add(new CONNECTDATA()
                             {
                                 dwCookie = this._enumerator.Current.Key,
                                 pUnk = this._enumerator.Current.Value
                             });
            }

            rgelt = data.ToArray();
            Marshal.WriteInt32(pceltFetched, 0, nIndex);
            return celt == nIndex ? 0 : 1;
        }

        public int Skip(int celt)
        {
            int nIndex = 0;
            while (this._enumerator.MoveNext() && nIndex++<celt)
            {
            }
            return celt == nIndex ? 0 : 1;
        }

        public void Reset()
        {
            isActualEnum = false;
        }

        public void Clone(out IEnumConnections ppenum)
        {
            throw new NotImplementedException();
        }

        // this._connections
        public void OnNotify(int id, string value)
        {
            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents) connection.Value;
                events.OnNotify(id,value);
            } 
        }

        public void OnCheckFile(string fullPath, out int state, out int verCount)
        {
            state = 0;
            verCount = 0;

            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents)connection.Value;
                try
                {
                    events.OnCheckFile(fullPath, out state, out verCount);
                }
                catch(Exception)
                {
                    continue;
                }
            }
        }

        public void OnNotifyCommand(int cmd, string fullPath)
        {
            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents)connection.Value;
                try
                {
                    events.OnNotifyCommand(cmd, fullPath);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        public void OnUpdateStatusInfo(int type, int count)
        {
            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents)connection.Value;
                try
                {
                    events.OnUpdateStatusInfo(type, count);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        public void OnCheckStatusInfo()
        {
            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents)connection.Value;
                try
                {
                    events.OnCheckStatusInfo();
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }

        public void OnGetEntries(string fullPath, int type, out object entries)
        {
            entries = 0;
            foreach (KeyValuePair<int, object> connection in _connections)
            {
                _ICppProxyEvents events = (_ICppProxyEvents)connection.Value;
                try
                {
                    events.OnGetEntries(fullPath, type, out entries);
                }
                catch (Exception)
                {
                    continue;
                }
            }
        }
    }
}