using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Windows.Forms;
using DocSync.Client.Controls.Core.Enums;
using DocSync.Client.Controls.Dialogs;
using DocSync.Client.Core.Collections;
using DocSync.Client.Core.Entities;
using DocSync.Client.Core.Entities.Files;
using DocSync.Client.Core.Entities.Settings;
using DocSync.Client.Core.Enums;
using DocSync.Client.Core.Managers.Application;
using DocSync.Client.Core.Utils;
using DocSync.Client.CppProxyServer.Managers;
using BackupDutyClientCppProxyLib;

namespace DocSync.Client.CppProxyServer.Entities
{
    [ComVisible(true)]
    [Guid("C28FEC82-2151-4DFA-8BB8-95551376BC8B"),
     ProgId("DocSync.Client.CppProxyServer.Entities.ShellCppProxy")]
    public class ShellCppProxy : ICppProxy, IConnectionPointContainer, IDisposable
    {
        public ShellCppProxy()
        {
            _connectionPoint = new ConnectionPoint(this);
            if (RemotingManager.Instance.ProxyPublisher != null)
            {
                RemotingManager.Instance.ProxyPublisher.BackupProgressChanged += this.ProxyPublisher_BackupProgressChanged;
                RemotingManager.Instance.ProxyPublisher.RestoreProgressChanged += this.ProxyPublisher_RestoreProgressChanged;
            }
        }

        void ProxyPublisher_RestoreProgressChanged(Remoting.Args.ProgressChangedEventArgs args)
        {
            _connectionPoint.OnUpdateStatusInfo(0, 0);
//            ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 0, 0);
            //ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 3, args.CurrentProgress.FilesPending);
            //ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 4, args.CurrentProgress.FilesErrors);
        }

        public void Dispose()
        {
            if (RemotingManager.Instance.ProxyPublisher != null)
            {
                RemotingManager.Instance.ProxyPublisher.BackupProgressChanged -= this.ProxyPublisher_BackupProgressChanged;
                RemotingManager.Instance.ProxyPublisher.RestoreProgressChanged -= this.ProxyPublisher_RestoreProgressChanged;
            }
        }

        void ProxyPublisher_BackupProgressChanged(Remoting.Args.ProgressChangedEventArgs args)
        {
            _connectionPoint.OnUpdateStatusInfo(0, 0);
//            ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 0, 0);
            //ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 0, args.CurrentProgress.FilesCompleted);
            //ApplicationInstanceManager.PostBroadcastMessage(ApplicationInstanceManager.WM_UPDATE_STATUS_INFO, 1, args.CurrentProgress.FilesPending);
        }

        private ConnectionPoint _connectionPoint;

        public void CheckOption(int option, out int value)
        {
            value = -1; // nothing

            switch ((AppOptions)option)
            {
                case AppOptions.DisplayColoredDots:
                    value = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().ShowDisplayMarkers
                                ? 1
                                : 0;
                    break;
            }
        }
        
        public void Notify(int id, string value)
        {
//            RemotingManager.Instance.ProxyPublisher.SetNotifyWindow(id);
        }

        public void ContainsState(string fullPath, int state, out int result)
        {
            BackupState entryState = (BackupState)state;
            result = RemotingManager.Instance.ProxyManager.ScheduledFileManager.ContainsFilesWithState(fullPath, entryState)
                       ? 1
                       : 0;
        }

        public void CheckSettings(int option, out string value)
        {
            switch (option)
            {
                case 0:
                    value = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().HelpUrl;
                    break;
                default:
                    value = string.Empty;
                    break;
            }
        }

        public void GetRecoveryData(string Name, out string Destination, out string state, out string priority, out DateTime lastRestored)
        {
            RestoreFileQueueItem item = RemotingManager.Instance.ProxyManager.RecoveryLogsManager.GetFileQueueItem(Name);

            if (item != null)
            {
                Destination = item.RestoreFilePath;
                state = item.StateMessage;
                priority = item.Priority.ToString();
                lastRestored = item.LastRestored;
            }
            else
            {
                Destination = String.Empty;
                state = String.Empty;
                priority = String.Empty;
                lastRestored = DateTime.MinValue;
            }
        }

        public void CancelRestore(string fullPath)
        {
            RemotingManager.Instance.ProxyManager.RecoveryLogsManager.CancelRestoreFile(fullPath);
        }

        public void RestartRestoreJob(string source, string destination)
        {
            RestoreFileQueueItem item = RemotingManager.Instance.ProxyManager.RecoveryLogsManager.GetFileQueueItem(source);
            RemotingManager.Instance.ProxyManager.BackupManager.RestoreFile(source, destination, item.RestoreFileVersion, false);
        }

        public void SetRestorePriority(int priority, string fullPath)
        {
            switch (priority)
            {
                case 0:
                    RemotingManager.Instance.ProxyManager.RecoveryLogsManager.SetFilePriority(fullPath, RestorePriority.Highest);
                    break;
                case 1:
                    RemotingManager.Instance.ProxyManager.RecoveryLogsManager.SetFilePriority(fullPath, RestorePriority.Normal);
                    break;
                case 2:
                    RemotingManager.Instance.ProxyManager.RecoveryLogsManager.SetFilePriority(fullPath, RestorePriority.Lowest);
                    break;
            }
            
        }

        public void GetStatusInfo(int type, out int value)
        {
            switch (type)
            {
                case 0:
                    // backup completed
                    value = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetCountOfBackedUpFiles();
                    break;
                case 1:
                    // backup pending
                    value = RemotingManager.Instance.ProxyManager.BackupManager.GetBackupProgress().FilesPending;
                    break;
                case 2:
                    // restore completed
                    value = RemotingManager.Instance.ProxyManager.BackupManager.GetRestoreProgress().FilesCompleted;
                    break;
                case 3:
                    // restore pending
                    Progress progress = RemotingManager.Instance.ProxyManager.BackupManager.GetRestoreProgress();
                    value = progress.FilesPending - progress.FilesErrors;
                    break;
                case 4:
                    // restore errors
                    value = RemotingManager.Instance.ProxyManager.BackupManager.GetRestoreProgress().FilesErrors;
                    break;
                default:
                    value = 0;
                    break;
            }
        }

        public void CheckAutoBackup(string fullPath, out int result)
        {
            result = 0;
            if (fullPath.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)) ||
                fullPath.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory)))
            {
                result =
                    RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings().UsersForAutomaticBackup.
                        Contains(Environment.UserName)
                        ? 1
                        : 0;
            }
        }

        public void CheckFile(string fullPath, out int state, out int verCount)
        {
            if (DiskUtil.CheckDiskType(fullPath, DiskTypes.Hdd))
            {
                BackupState entryState = BackupState.New;
                int versionCount = 0;
                bool isScheduled = false;

                if (RemotingManager.Instance.ProxyManager.ScheduledFileManager.Check(fullPath, ref entryState,
                                                                                     ref isScheduled, ref versionCount))
                {
                    state = isScheduled ? (int) entryState : (int) BackupState.New;
                    verCount = versionCount;
                }
                else
                {
                    state = (int) BackupState.New;
                    verCount = 0;
                }
            }
            else
            {
                state = (int)BackupState.Nothing;
                verCount = 0;
            }
        }

        public void NotifyCommand(int cmd, int window, string fullPath)
        {
            try
            {
                WindowWrapper parent = new WindowWrapper((IntPtr)window);

                switch ((ShellCommands)cmd)
                {
                    case ShellCommands.BackThisUp:
                        RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(fullPath);
                        break;
                    case ShellCommands.BackUpAsap:
                        RemotingManager.Instance.ProxyManager.BackupManager.BackupFileAsap(fullPath);
                        break;
                    case ShellCommands.DonotBackThisUp:
                        RemotingManager.Instance.ProxyManager.ScheduledFileManager.DontBackThisUp(fullPath);
                        break;
                    case ShellCommands.Restore:
                        this.Restore(window, fullPath, fullPath);
                        break;
                    case ShellCommands.RestoreTo:
                        {
                            RestoreHelper helper = new RestoreHelper()
                                                       {
                                                           FullPath = fullPath,
                                                           Window = window
                                                       };


                            Thread thread = new Thread(helper.RestoreTo);
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                            thread.Join();

                            if (helper.Result)
                            {
                                this.Restore(window, fullPath, helper.RestorePath);
                            }
                        }
//                        FolderBrowserDialog browserDialog = new FolderBrowserDialog();
//                        browserDialog.ShowNewFolderButton = true;
//                        browserDialog.SelectedPath = fullPath;
//                        browserDialog.Description = "Select target folder to Restore";
//
//                        if (browserDialog.ShowDialog(parent) == DialogResult.OK)
//                        {
//                            this.Restore(window, fullPath, browserDialog.SelectedPath);
//                        }
                        break;
                    case ShellCommands.RestorePrevVersion:
                        {
                            RestoreHelper helper = new RestoreHelper()
                                                       {
                                                           FullPath = fullPath,
                                                           Window = window
                                                       };

                            Thread thread = new Thread(helper.RestorePrevVersion);
                            thread.SetApartmentState(ApartmentState.STA);
                            thread.Start();
                            thread.Join();
                        }
                        break;
                    case ShellCommands.RemoveFromBackup:
                        if (!RemotingManager.Instance.ProxyManager.ScheduledFileManager.DeleteFileFromBackup(fullPath))
                        {
                            CustomMessageBox.Show(parent, "Removing this file from your backup has failed", "Error Message", CustomMessageBoxButtons.OK);
                        }
                        break;
                    case ShellCommands.LaunchRestoreWizard:
                        {
                            ApplicationInstanceManager.PostBroadcastMessageWithCheckingAppRunning(ApplicationInstanceManager.WM_LAUNCH_RESTORE_WIZARD);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                WindowWrapper parent = new WindowWrapper((IntPtr)window);

                CustomMessageBox.Show(parent,"Command could not be completed", "Error Message", CustomMessageBoxButtons.OK);
            }
        }

        

        private List<ItemData> GetEntries(string fullPath, int type)
        {
            string[] entries = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetEntries(fullPath, type);
            List<ItemData> list = new List<ItemData>();
            foreach (string entry in entries)
            {
                list.Add(new ItemData()
                             {
                                 IsFolder = entry.StartsWith("1"),
                                 Name = entry.Remove(0,1)
                             });
            }
            return list;
        }

        private void Restore(int window, string fullPath, string targetPath)
        {
            WindowWrapper parent = new WindowWrapper((IntPtr)window);

            List<string> vctFiles = new List<string>();
	        Stack<List<ItemData>> stackFolders = new Stack<List<ItemData>>();
            
            bool restoreTo = targetPath != fullPath;
            bool applyAll = restoreTo;

	        List<ItemData> list;

            string msg = string.Empty;
            CustomMessageBoxButtons buttons = CustomMessageBoxButtons.YesNo;

            bool isFolder = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetPathType(fullPath) == PathType.Folder;

            if (isFolder)
            {
                msg = string.Format("Do you want to replace files in ({0}) folder?", fullPath);
                buttons = CustomMessageBoxButtons.YesYesToAllNoCancel;
            }
            else
            {
                if (File.Exists(fullPath))
                {
                    XmlFileInfo xmlFileInfo =
                        RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetFileInfo(fullPath);
                    FileInfo fileInfo = new FileInfo(fullPath);
                    msg =
                        string.Format(
                            "Do you want to replace the current file ({0}, {1}, {2}) with the backed-up one ({3}, {4}, {5}) ?",
                            Path.GetFileName(fullPath),
                            new SizeEntity(fileInfo.Length),
                            fileInfo.LastWriteTime,
                            Path.GetFileName(fullPath),
                            xmlFileInfo.Size,
                            xmlFileInfo.LastModifiedDate);
                    buttons = CustomMessageBoxButtons.YesNo;
                }
                else
                {
                    applyAll = true;
                }
            }

            CustomMessageBoxResult result = applyAll ? CustomMessageBoxResult.YesToAll : CustomMessageBox.Show(parent, msg, "Restore", buttons);
            
            applyAll = result == CustomMessageBoxResult.YesToAll;

            if (!applyAll && result != CustomMessageBoxResult.Yes)
                return;

            if (isFolder)
            {
                list = GetEntries(fullPath, (int)BackupStateCppProxy.Backedup);

                string path = fullPath;

                while (list.Count != 0)
                {
                    ItemData data = list[0];
                    if (data.IsFolder)
                    {
                        list.RemoveAt(0);

                        path += ("\\");
                        path += data.Name;

                        msg = string.Format("Do you want to replace files in ({0}) folder?", path);
                        
                        result = applyAll
                                     ? CustomMessageBoxResult.Yes
                                     : CustomMessageBox.Show(parent, msg, "Restore", CustomMessageBoxButtons.YesYesToAllNoCancel);

                        if (result == CustomMessageBoxResult.Yes || applyAll)
                        {
                            stackFolders.Push(list);
                            list = GetEntries(path, (int)BackupStateCppProxy.Backedup);
                        }
                        else if (result == CustomMessageBoxResult.Cancel)
                        {
                            return;
                        }
                    }
                    else
                    {
                        string file = path;
                        file += "\\";
                        file += data.Name;
                        vctFiles.Add(file);

                        list.RemoveAt(0);
                    }

                    while (list.Count == 0 && stackFolders.Count > 0)
                    {
                        list = stackFolders.Pop();

                        path = Path.GetDirectoryName(path);
                    }
                }
            }
            else
            {
                vctFiles.Add(fullPath);
            }

            foreach (string vctFile in vctFiles)
            {
                string restorePath = restoreTo ? targetPath + "\\" + vctFile.Replace(":", "") : vctFile;
                // send to restore list of files
                RemotingManager.Instance.ProxyManager.BackupManager.RestoreFile(vctFile, restorePath,-1,true);
            }
        }

        public void UpdateStatusInfo(int type, int count)
        {
            _connectionPoint.OnUpdateStatusInfo(type,count);
        }

        public void CheckStatusInfo()
        {
            _connectionPoint.OnCheckStatusInfo();
        }

        public void GetEntries(string fullPath, int type, out object entries)
        {
            BackupStateCppProxy state = (BackupStateCppProxy)type;
            switch (state)
            {
                case BackupStateCppProxy.Backedup:
                case BackupStateCppProxy.Pending:
                    entries = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetEntries(fullPath, type);
                    break;
                case BackupStateCppProxy.RecoveryLog:
                case BackupStateCppProxy.RecoveryLogCompleted:
                    entries = RemotingManager.Instance.ProxyManager.RecoveryLogsManager.GetEntries(RecoveryLogTypes.Completed).ToArray();
                    break;
                case BackupStateCppProxy.RecoveryLogErrors:
                    entries = RemotingManager.Instance.ProxyManager.RecoveryLogsManager.GetEntries(RecoveryLogTypes.Errors).ToArray();
                    break;
                case BackupStateCppProxy.RecoveryLogPending:
                    entries = RemotingManager.Instance.ProxyManager.RecoveryLogsManager.GetEntries(RecoveryLogTypes.Pending).ToArray();
                    break;
                default:
                    entries = null;
                    break;
            }
            
        }

        public void CopySharedLink(string fullPath, out string sLink)
        {
            sLink = RemotingManager.Instance.ProxyManager.ScheduledFileManager.GetFileLink(fullPath);
        }

        // IConnectionPointContainer implementation
        public void EnumConnectionPoints(out IEnumConnectionPoints ppEnum)
        {
            ppEnum = new EnumConnectionPoints(_connectionPoint);
        }

        public void FindConnectionPoint(ref Guid riid, out IConnectionPoint ppCP)
        {
            ppCP = null;

            if (riid == typeof(_ICppProxyEvents).GUID)
            {
                ppCP = _connectionPoint;
            }
        }
    }
}