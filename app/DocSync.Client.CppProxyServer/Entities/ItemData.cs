namespace DocSync.Client.CppProxyServer.Entities
{
    /// <summary>
    /// Item data to restore
    /// </summary>
    public struct ItemData
    {
        /// <summary>
        /// true if item is folder, false if it is file
        /// </summary>
        public bool IsFolder;

        /// <summary>
        /// name of item
        /// </summary>
        public string Name;
    }
}