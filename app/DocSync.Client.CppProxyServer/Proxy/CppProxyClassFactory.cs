using System;
using System.Runtime.InteropServices;
using DocSync.Client.CppProxyServer.Entities;
using BackupDutyClientCppProxyLib;

namespace DocSync.Client.CppProxyServer.Proxy
{
    public class CppProxyClassFactory : ClassFactoryBase
    {
        private static ShellCppProxy CppProxy = new ShellCppProxy();

        public override void virtual_CreateInstance(IntPtr pUnkOuter, ref Guid riid, out IntPtr ppvObject)
        {
            if (riid == Marshal.GenerateGuidForType(typeof(ICppProxy)) ||
                riid == Program.IID_IDispatch ||
                riid == Program.IID_IUnknown)
            {
                //NetCppProxy netCppProxy = new NetCppProxy();

                ppvObject = Marshal.GetComInterfaceForObject(CppProxy, typeof(ICppProxy));
            }
            else
            {
                throw new COMException("No interface", unchecked((int)0x80004002));
            }
        }
    }
}