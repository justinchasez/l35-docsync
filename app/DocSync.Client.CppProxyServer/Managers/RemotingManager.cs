using System;
using System.Diagnostics;
using System.Reflection;
using DocSync.Client.Core.Utils;
using DocSync.Client.Remoting.Managers;
using DocSync.Client.Remoting.Managers.Clients;
using DocSync.Client.Remoting.Terminals;

namespace DocSync.Client.CppProxyServer.Managers
{
    /// <summary>
    /// The remoting events maanger.
    /// </summary>
    public class RemotingManager
    {
        #region Constants

        /// <summary>
        /// The remoting port.
        /// </summary>
        private const int REMOTING_PORT = 999;

        /// <summary>
        /// The remoting channel name.
        /// </summary>
        private const string REMOTING_CHANNEL_NAME = "DocSync";

        #endregion

        /// <summary>
        /// The single instance of remoting manager.
        /// </summary>
        private static RemotingManager instance;

        /// <summary>
        /// The client remoting terminal.
        /// </summary>
        private ClientTerminal terminal;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static RemotingManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RemotingManager();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets the session manager.
        /// </summary>
        /// <value>The session manager.</value>
        public ProxyManager ProxyManager { get; private set; }

        /// <summary>
        /// Gets the proxy publisher.
        /// </summary>
        /// <value>The proxy publisher.</value>
        public EventProxyClient ProxyPublisher { get; private set; }

        /// <summary>
        /// Prevents a default instance of the <see cref="RemotingManager"/> class from being created.
        /// </summary>
        private RemotingManager()
        {
            this.Connect();
        }

        /// <summary>
        /// Connects this instance.
        /// </summary>
        public void Connect()
        {
            if (this.terminal == null)
            {
                this.terminal = new ClientTerminal();
            }

            try
            {
                this.TryConnect();
            }
            catch (TargetInvocationException)
            {
                if (Process.Start("Online Backup service") != null)
                {
                    this.TryConnect();
                }
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("CppProxy Remoting manager: Exception " + ex);
            }
        }

        /// <summary>
        /// Try connect to service
        /// </summary>
        private void TryConnect()
        {
            this.ProxyManager = this.terminal.Connect<ProxyManager>(REMOTING_PORT, REMOTING_CHANNEL_NAME);
            Loger.Instance.Log("CppProxy Remoting manager: Connection to " + typeof(ProxyManager) + " successfully");
            this.ProxyPublisher = new EventProxyClient(this.terminal.Connect<EventProxy>(REMOTING_PORT, REMOTING_CHANNEL_NAME));
            Loger.Instance.Log("CppProxy Remoting manager: Connection to " + typeof(EventProxy) + " successfully");

            try
            {
                this.ProxyManager.TestConnection();
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("CppProxy Remoting manager: Test connection Exception " + ex);
                this.terminal.Disconnect();

                throw;
            }
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public void Disconnect()
        {
            if (this.terminal == null)
            {
                return;
            }

            try
            {
                this.ProxyPublisher.Dispose();

                this.terminal.Disconnect();
                this.terminal = null;
            }
            catch (Exception)
            {
                // TODO: Log server shutdown error.
            }
        }
    }
}