// OnlineBackupIconNormal.cpp : Implementation of COnlineBackupIconNormal

#include "stdafx.h"
#include "OnlineBackupIconNormal.h"
#include "ProxyServerContainer.h"
#include "VFCppProxyEventHandler.h"

// COnlineBackupIconNormal
HRESULT COnlineBackupIconNormal::IsMemberOf(THIS_ LPCWSTR pwszPath, DWORD dwAttrib)
{
	if (!CVFCppProxyEventHandler::DisplayColorDots)
		return S_FALSE;

	long state = STATE_NOTHING;
	long vercount = 0;

	CString path(pwszPath);

	try
	{
		ICppProxyPtr proxy;
		proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
		//ICppProxyPtr proxy = ProxyServerContainer::Instance();

		if (proxy)
		{
			long option = -1;
			proxy->CheckOption(2, &option);
			if (option == 1)
			{
				proxy->CheckFile(_bstr_t(path.AllocSysString()),&state,&vercount);
			}
		}
	}
	catch(_com_error &e)
	{
		state = STATE_NOTHING;
		//::MessageBox(NULL,e.ErrorMessage(),e.Description(),MB_OK);
	}
	catch(...)
	{
		state = STATE_NOTHING;
	}

	return (state == STATE_SCHEDULED) ? S_OK : S_FALSE;
}

HRESULT COnlineBackupIconNormal::GetOverlayInfo(THIS_ __out_ecount(cchMax) LPWSTR pwszIconFile, int cchMax, __out int * pIndex, __out DWORD * pdwFlags)
{
	// Get our module's full path

  GetModuleFileNameW(_AtlBaseModule.GetModuleInstance(), pwszIconFile, cchMax);

  // Use first icon in the resource

  *pIndex=1; 

  *pdwFlags = ISIOI_ICONFILE | ISIOI_ICONINDEX;
  return S_OK;
}

HRESULT COnlineBackupIconNormal::GetPriority(THIS_ __out int * pIPriority)
{
	// we want highest priority 

	*pIPriority=0;
	return S_OK;
}

