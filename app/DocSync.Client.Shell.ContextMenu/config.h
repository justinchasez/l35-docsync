#pragma once

#define STATE_NOTHING 0
#define STATE_NEW 1
#define STATE_PENDING 2
#define STATE_SCHEDULED 3

#define COMMAND_NOTHING 0
#define COMMAND_BACKTHISUP 1
#define COMMAND_DONOTBACKTHISUP 2
#define COMMAND_BACKUPASAP 3
#define COMMAND_RESTORE 4
#define COMMAND_RESTORETO 5
#define COMMAND_RESTOREPREVVERSION 6
#define COMMAND_REMOVEFROMBACKUP 7
#define COMMAND_LAUNCHRESTOREWIZARD 8

#define COMMAND_OPENBACKUPDRIVE 9

#define COMMAND_COPYLINK 10		



