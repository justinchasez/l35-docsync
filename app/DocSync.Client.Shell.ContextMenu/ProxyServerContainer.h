#pragma once

#ifdef _DEBUG
#import "..\..\app\OnlineBackup.Client.CppProxy\Debug\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Debug\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#else
#import "..\..\app\OnlineBackup.Client.CppProxy\Release\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Release\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#endif

class ProxyServerContainer
{
public:
	ProxyServerContainer(void);
	~ProxyServerContainer(void);

	static ICppProxy* Instance1()
	{
		if (m_remotingProxy == NULL)
		{
			m_remotingProxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
			HRESULT hr = CoMarshalInterThreadInterfaceInStream(IID_ICppProxy,m_remotingProxy,&m_stream);
		}
		else
		{
			IStream* pCloneStream;
            m_stream->Clone(&pCloneStream);
			ICppProxy* proxy = NULL;
			HRESULT hr = CoGetInterfaceAndReleaseStream(pCloneStream,IID_ICppProxy,(void**)&proxy);
			return proxy;
		}

		return m_remotingProxy;
	}

private:
	static IStream* m_stream;
	static ICppProxyPtr m_remotingProxy;
};
