

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Jan 17 15:44:56 2014
 */
/* Compiler settings for OnlineBackupClientShellContextMenu.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __OnlineBackupClientShellContextMenu_i_h__
#define __OnlineBackupClientShellContextMenu_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IOBContextMenu_FWD_DEFINED__
#define __IOBContextMenu_FWD_DEFINED__
typedef interface IOBContextMenu IOBContextMenu;

#endif 	/* __IOBContextMenu_FWD_DEFINED__ */


#ifndef __IOnlineBackupIconNormal_FWD_DEFINED__
#define __IOnlineBackupIconNormal_FWD_DEFINED__
typedef interface IOnlineBackupIconNormal IOnlineBackupIconNormal;

#endif 	/* __IOnlineBackupIconNormal_FWD_DEFINED__ */


#ifndef __IOnlineBackupIconModified_FWD_DEFINED__
#define __IOnlineBackupIconModified_FWD_DEFINED__
typedef interface IOnlineBackupIconModified IOnlineBackupIconModified;

#endif 	/* __IOnlineBackupIconModified_FWD_DEFINED__ */


#ifndef __OBContextMenu_FWD_DEFINED__
#define __OBContextMenu_FWD_DEFINED__

#ifdef __cplusplus
typedef class OBContextMenu OBContextMenu;
#else
typedef struct OBContextMenu OBContextMenu;
#endif /* __cplusplus */

#endif 	/* __OBContextMenu_FWD_DEFINED__ */


#ifndef __OnlineBackupIconNormal_FWD_DEFINED__
#define __OnlineBackupIconNormal_FWD_DEFINED__

#ifdef __cplusplus
typedef class OnlineBackupIconNormal OnlineBackupIconNormal;
#else
typedef struct OnlineBackupIconNormal OnlineBackupIconNormal;
#endif /* __cplusplus */

#endif 	/* __OnlineBackupIconNormal_FWD_DEFINED__ */


#ifndef __OnlineBackupIconModified_FWD_DEFINED__
#define __OnlineBackupIconModified_FWD_DEFINED__

#ifdef __cplusplus
typedef class OnlineBackupIconModified OnlineBackupIconModified;
#else
typedef struct OnlineBackupIconModified OnlineBackupIconModified;
#endif /* __cplusplus */

#endif 	/* __OnlineBackupIconModified_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IOBContextMenu_INTERFACE_DEFINED__
#define __IOBContextMenu_INTERFACE_DEFINED__

/* interface IOBContextMenu */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOBContextMenu;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("05754B16-BF0E-434A-AEF5-9089A82579E8")
    IOBContextMenu : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IOBContextMenuVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOBContextMenu * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOBContextMenu * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOBContextMenu * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOBContextMenu * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOBContextMenu * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOBContextMenu * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOBContextMenu * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IOBContextMenuVtbl;

    interface IOBContextMenu
    {
        CONST_VTBL struct IOBContextMenuVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOBContextMenu_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOBContextMenu_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOBContextMenu_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOBContextMenu_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOBContextMenu_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOBContextMenu_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOBContextMenu_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOBContextMenu_INTERFACE_DEFINED__ */


#ifndef __IOnlineBackupIconNormal_INTERFACE_DEFINED__
#define __IOnlineBackupIconNormal_INTERFACE_DEFINED__

/* interface IOnlineBackupIconNormal */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOnlineBackupIconNormal;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7BC6EF69-55A1-4A3D-A3E6-2153DBC0BE63")
    IOnlineBackupIconNormal : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IOnlineBackupIconNormalVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOnlineBackupIconNormal * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOnlineBackupIconNormal * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOnlineBackupIconNormal * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOnlineBackupIconNormal * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOnlineBackupIconNormal * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOnlineBackupIconNormal * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOnlineBackupIconNormal * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IOnlineBackupIconNormalVtbl;

    interface IOnlineBackupIconNormal
    {
        CONST_VTBL struct IOnlineBackupIconNormalVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOnlineBackupIconNormal_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOnlineBackupIconNormal_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOnlineBackupIconNormal_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOnlineBackupIconNormal_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOnlineBackupIconNormal_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOnlineBackupIconNormal_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOnlineBackupIconNormal_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOnlineBackupIconNormal_INTERFACE_DEFINED__ */


#ifndef __IOnlineBackupIconModified_INTERFACE_DEFINED__
#define __IOnlineBackupIconModified_INTERFACE_DEFINED__

/* interface IOnlineBackupIconModified */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IOnlineBackupIconModified;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("38FF1C42-E55E-41C7-A0B4-0FF36FD9211E")
    IOnlineBackupIconModified : public IDispatch
    {
    public:
    };
    
    
#else 	/* C style interface */

    typedef struct IOnlineBackupIconModifiedVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IOnlineBackupIconModified * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IOnlineBackupIconModified * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IOnlineBackupIconModified * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IOnlineBackupIconModified * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IOnlineBackupIconModified * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IOnlineBackupIconModified * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IOnlineBackupIconModified * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } IOnlineBackupIconModifiedVtbl;

    interface IOnlineBackupIconModified
    {
        CONST_VTBL struct IOnlineBackupIconModifiedVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IOnlineBackupIconModified_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IOnlineBackupIconModified_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IOnlineBackupIconModified_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IOnlineBackupIconModified_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IOnlineBackupIconModified_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IOnlineBackupIconModified_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IOnlineBackupIconModified_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IOnlineBackupIconModified_INTERFACE_DEFINED__ */



#ifndef __BackupDutyClientShellContextMenuLib_LIBRARY_DEFINED__
#define __BackupDutyClientShellContextMenuLib_LIBRARY_DEFINED__

/* library BackupDutyClientShellContextMenuLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_BackupDutyClientShellContextMenuLib;

EXTERN_C const CLSID CLSID_OBContextMenu;

#ifdef __cplusplus

class DECLSPEC_UUID("3DFB7EA1-B718-4B8D-8BAA-13B11DEE9A54")
OBContextMenu;
#endif

EXTERN_C const CLSID CLSID_OnlineBackupIconNormal;

#ifdef __cplusplus

class DECLSPEC_UUID("2942E61C-9302-45D9-AABA-BC248F943ABE")
OnlineBackupIconNormal;
#endif

EXTERN_C const CLSID CLSID_OnlineBackupIconModified;

#ifdef __cplusplus

class DECLSPEC_UUID("591CBCCE-5D7C-4A11-9FBD-C6C44E5B863E")
OnlineBackupIconModified;
#endif
#endif /* __BackupDutyClientShellContextMenuLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


