// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef STRICT
#define STRICT
#endif

#include "targetver.h"

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include "resource.h"
#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>
#include <atlstr.h>

using namespace ATL;

#ifdef _DEBUG
//#import "..\\..\\lib\\RemotingCppProxy\\Debug\\OnlineBackupClientRemotingCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxy\Debug\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Debug\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#else
//#import "..\\..\\lib\\RemotingCppProxy\\Release\\OnlineBackupClientRemotingCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxy\Release\BackupDutyClientCppProxy.tlb" named_guids no_namespace
#import "..\..\app\OnlineBackup.Client.CppProxyServer\bin\Release\BackupDuty.Client.CppProxyServer.tlb" named_guids no_namespace
#endif
