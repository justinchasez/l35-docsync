// OBContextMenu.cpp : Implementation of COBContextMenu

#include "stdafx.h"
#include "OBContextMenu.h"
#include "ProxyServerContainer.h"

// COBContextMenu
static UINT g_idCmdFirst;
static HMENU g_hMenu;

extern HINSTANCE g_hInstance;

struct Command
{
public:
	int commandId;
	int resourceId;
};

#define MAX_COMMANDS 5
#define MAX_STATES 3
#define VERSION_REQUIRED_COUNT 2

static const Command g_filecommands[MAX_STATES][MAX_COMMANDS] = {
												{// state new
													{COMMAND_BACKTHISUP,IDS_BACKTHISUP},
												    {COMMAND_NOTHING,0},
												    {COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												},
												{// state pending
													{COMMAND_DONOTBACKTHISUP,IDS_DONOTBACKUP},
													{COMMAND_BACKUPASAP,IDS_BACKUPASAP},
													{COMMAND_RESTOREPREVVERSION,IDS_RESTORE},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												},
												{// state scheduled
													{COMMAND_DONOTBACKTHISUP,IDS_DONOTBACKUP},
													{COMMAND_RESTOREPREVVERSION,IDS_RESTORE},
													{COMMAND_COPYLINK,IDS_COPYLINK},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												}
											};

static const Command g_foldercommands[MAX_STATES][MAX_COMMANDS] = {
												{// state new
													{COMMAND_BACKTHISUP,IDS_BACKTHISUP},
												    {COMMAND_NOTHING,0},
												    {COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												},
												{// state pending
													{COMMAND_DONOTBACKTHISUP,IDS_DONOTBACKUP},
													{COMMAND_OPENBACKUPDRIVE,IDS_OPENBACKUPDRIVE},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												},
												{// state scheduled
													{COMMAND_DONOTBACKTHISUP,IDS_DONOTBACKUP},
													{COMMAND_OPENBACKUPDRIVE,IDS_OPENBACKUPDRIVE},
													{COMMAND_COPYLINK,IDS_COPYLINK},
													{COMMAND_NOTHING,0},
													{COMMAND_NOTHING,0}
												}
											};

// QueryContextMenu
HRESULT COBContextMenu::QueryContextMenu(HMENU hmenu, UINT indexMenu,
		UINT idCmdFirst, UINT idCmdLast, UINT uFlags)
{
	USES_CONVERSION;

	g_idCmdFirst = idCmdFirst;
	UINT idCmd = idCmdFirst;

	if (m_hMenu != NULL)
	{
		::DestroyMenu(m_hMenu);
		m_hMenu = NULL;
	}

	m_commands.clear();

	m_state = STATE_NOTHING;

	if (!m_fileName.IsEmpty() && ((uFlags & CMF_VERBSONLY) != CMF_VERBSONLY))
	{
		DWORD attr = GetFileAttributes(m_fileName);

		if ((attr & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
		{
			long verCount = 0;
			long autobackup = 0;

			try
			{
				ICppProxyPtr proxy;
				HRESULT hr = proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
				//ICppProxyPtr proxy = ProxyServerContainer::Instance();
				if (proxy)
				{
					proxy->CheckFile(_bstr_t(m_fileName.AllocSysString()),&m_state,&verCount);
					if (m_state > STATE_NEW)
						proxy->CheckAutoBackup(_bstr_t(m_fileName.AllocSysString()),&autobackup);
				}
			}
			catch(...)
			{
			}

			if ((m_state > STATE_NOTHING) && (m_state <= MAX_STATES))
			{
				CString menuName;
				m_hMenu = CreatePopupMenu();

				for (int ii=0; ii<MAX_COMMANDS; ii++)
				{
					Command command = ((attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
										? g_foldercommands[m_state-1][ii]
										: g_filecommands[m_state-1][ii];

					UINT flags = MF_STRING | MF_BYPOSITION;

					if (command.commandId == COMMAND_NOTHING)
						break;

					if ((command.commandId == COMMAND_RESTOREPREVVERSION) && (verCount < VERSION_REQUIRED_COUNT))
						continue;

					if ((command.commandId == COMMAND_DONOTBACKTHISUP) && (autobackup > 0))
					{
						flags |= MF_GRAYED;
					}

					m_commands.push_back(command.commandId);

					menuName.LoadString(command.resourceId);
					InsertMenu(m_hMenu, ii, flags, idCmd++, menuName);
				}

				menuName.LoadString(IDS_ROOTMENUNAME);

				// Add it to the context menu 
				InsertMenu(hmenu, indexMenu++, MF_POPUP|MF_BYPOSITION,
						(UINT)m_hMenu, menuName);
			}
		}
	}

	return MAKE_SCODE(SEVERITY_SUCCESS, FACILITY_NULL, 
		idCmd-idCmdFirst);
}

 
// InvokeCommand
HRESULT COBContextMenu::InvokeCommand(LPCMINVOKECOMMANDINFO lpcmi)
{
	USES_CONVERSION;

	if (HIWORD(lpcmi->lpVerb) == 0)
	{
		UINT idCmd = LOWORD(lpcmi->lpVerb);

		DWORD attr = GetFileAttributes(m_fileName);

		long shellCmd = COMMAND_NOTHING;
		/*if (((m_state > STATE_NOTHING) && (m_state<=MAX_STATES)) &&
			(idCmd >=0) && (idCmd < MAX_COMMANDS))*/
		if ((idCmd >=0) && (idCmd < m_commands.size()))
		{
			/*Command command = ((attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
									? g_foldercommands[m_state-1][idCmd]
									: g_filecommands[m_state-1][idCmd];*/

			shellCmd = m_commands[idCmd];
		}

		if (shellCmd == COMMAND_OPENBACKUPDRIVE)
		{
			CString newPath;
			CString sFile;
			if ((attr & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY)
			{
				int pos = m_fileName.ReverseFind('\\');
				sFile = _T(", ") + m_fileName.Right(m_fileName.GetLength()-pos-1);
				m_fileName.Delete(pos,m_fileName.GetLength()-pos-1);
			}

			if (m_state == STATE_PENDING)
			{
				newPath.Format(_T("\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Pending Back-up\\%s\""),m_fileName);
			}
			else if (m_state == STATE_SCHEDULED)
			{
				newPath.Format(_T("\"::{20D04FE0-3AEA-1069-A2D8-08002B30309D}\\::{281F347D-44EF-44EB-A4E3-D00B53707C2E}\\Backed-up Files\\%s\""),m_fileName);
			}
			else
				return S_OK;

			if (!sFile.IsEmpty())
			{
				newPath += sFile;
			}

			HINSTANCE hInstance = ShellExecute(NULL,_T("open"),_T("explorer"),newPath,NULL,SW_SHOWNORMAL);
		}
		else if( shellCmd == COMMAND_COPYLINK )
		{
			ICppProxyPtr proxy;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
			//ICppProxy* proxy = ProxyServerContainer::Instance();
			if (proxy)
			{
				_bstr_t sLink;
				proxy->CopySharedLink( _bstr_t( m_fileName.AllocSysString() ), sLink.GetAddress() );				
				LPSTR  lptstrCopy; 

				if( !OpenClipboard(lpcmi->hwnd) )
					return 0;
				EmptyClipboard(); 
				
				HGLOBAL hglbCopy; 
				hglbCopy = GlobalAlloc(GMEM_MOVEABLE, 
					(sLink.length()+ 1) * sizeof(TCHAR));
				if (hglbCopy == NULL) 
				{ 
					CloseClipboard(); 
					return 0; 
				}
				lptstrCopy = (LPSTR)GlobalLock(hglbCopy); 
				memcpy(lptstrCopy, (char*)sLink, 
						sLink.length()); 
				lptstrCopy[sLink.length()] = '\0';    // null character 
				GlobalUnlock(hglbCopy); 
				
				SetClipboardData(CF_TEXT, hglbCopy); 

				CloseClipboard();
			}
		}
		else
		{
			
			ICppProxyPtr proxy;
			proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
			//ICppProxy* proxy = ProxyServerContainer::Instance();
			if (proxy)
				proxy->NotifyCommand(shellCmd,(long)lpcmi->hwnd,_bstr_t(m_fileName.AllocSysString()));
		}
	}

	return S_OK;
}

#ifdef WIN64
HRESULT COBContextMenu::GetCommandString(UINT_PTR idCmd, UINT uFlags, 
		UINT *pwReserved, LPSTR pszText, UINT cchMax)
#else
HRESULT COBContextMenu::GetCommandString(UINT idCmd, UINT uFlags, 
		UINT *pwReserved, LPSTR pszText, UINT cchMax)
#endif
{
	USES_CONVERSION;

	if (uFlags == GCS_HELPTEXTA)
	{
		lstrcpyA(pszText, "Some Bisov Text");
	}

	if (uFlags == GCS_HELPTEXTW)
	{
		lstrcpyW((LPWSTR)pszText, L"Some Bisov Text");
	}

	return S_OK;
}



/////////////////////////////////////////////////////////////////////////////
// Methods of IContextMenu2

HRESULT COBContextMenu::HandleMenuMsg(UINT uMsg,WPARAM wParam, LPARAM lParam)
{
	return HandleMenuMsg2(uMsg, wParam, lParam, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// Methods of IContextMenu3

HRESULT COBContextMenu::HandleMenuMsg2(UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT *plResult)
{
	switch(uMsg)
	{
		case WM_INITMENUPOPUP:
			g_hMenu = (HMENU) wParam;
			break;

	    case WM_DRAWITEM:
			//DrawMenuItem((LPDRAWITEMSTRUCT) lParam);
			break;

		case WM_MEASUREITEM:
			//MeasureItem((LPMEASUREITEMSTRUCT) lParam);
			break;
	}
	
	return S_FALSE;
}

HRESULT COBContextMenu::Initialize(LPCITEMIDLIST pidlFolder, LPDATAOBJECT data, HKEY hKey)
{
	if (data == NULL)
	{
		return S_OK;
	}

	// Now get IShellFolder for pidlFolder
	/*if (pidlFolder != NULL)
	{
   
		CComQIPtr<IShellFolder> ishDesk;
		SHGetDesktopFolder(&ishDesk);
		CComQIPtr<IShellFolder> ishFolder;
		ishDesk->BindToObject(pidlFolder, NULL,
			IID_IShellFolder, (void**)&ishFolder);

		CString name;
	   STRRET str;
	   str.uType = STRRET_WSTR;
	   if (ishFolder)
	   {
		   if (ishFolder->GetDisplayNameOf(pidlFolder, SHGDN_FORPARSING, &str) == S_OK) {
			  name = str.pOleStr;
		   }
	   }
	}*/

	/*Format 49268 
Format 49562 
Format 49563 
Format 15 
Format 49158 
Format 49159 
*/
    FORMATETC fmt;
	fmt.cfFormat = CF_HDROP;
    fmt.ptd = NULL;
    fmt.dwAspect = DVASPECT_CONTENT;
    fmt.lindex = -1;
    fmt.tymed = TYMED_HGLOBAL;

    STGMEDIUM medium;
	data->GetData(&fmt, &medium);

	HDROP hDrop = static_cast<HDROP>(GlobalLock(medium.hGlobal));

	int nCount = DragQueryFile(hDrop, 0xFFFFFFFF, NULL, 0);

	for (UINT i = 0; i < nCount; i++)
	{
		::DragQueryFile(hDrop, i, m_fileName.GetBufferSetLength(MAX_PATH), MAX_PATH);
		m_fileName.ReleaseBuffer();
	}

	GlobalUnlock(medium.hGlobal);

	ReleaseStgMedium(&medium);

	return S_OK;
}

void COBContextMenu::MeasureItem(LPMEASUREITEMSTRUCT lpmis) 
{ 
	lpmis->itemWidth = 150; 
	lpmis->itemHeight = 20; 
}


void COBContextMenu::DrawMenuItem(LPDRAWITEMSTRUCT lpdis)
{
}