// OBContextMenu.h : Declaration of the COBContextMenu

#pragma once
#include "resource.h"       // main symbols

#include "OnlineBackupClientShellContextMenu_i.h"

#include "comdef.h"		
#include <AtlCom.h>
#include <ShlObj.h>

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

#include "config.h"
#include <vector>

// COBContextMenu

class ATL_NO_VTABLE COBContextMenu :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COBContextMenu, &CLSID_OBContextMenu>,
	public IShellExtInit,
	public IContextMenu3,
//	public IContextMenu2,
//	public IContextMenu3,
	public IObjectWithSiteImpl<COBContextMenu>,
	public IDispatchImpl<IOBContextMenu, &IID_IOBContextMenu, &LIBID_BackupDutyClientShellContextMenuLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	COBContextMenu()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_OBCONTEXTMENU)


BEGIN_COM_MAP(COBContextMenu)
	COM_INTERFACE_ENTRY(IOBContextMenu)
	COM_INTERFACE_ENTRY(IShellExtInit)
	COM_INTERFACE_ENTRY(IContextMenu)
	COM_INTERFACE_ENTRY(IContextMenu2)
	COM_INTERFACE_ENTRY(IContextMenu3)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IObjectWithSite)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		m_state = STATE_NOTHING;
		m_hMenu = NULL;
		return S_OK;
	}

	void FinalRelease()
	{
		if (m_hMenu != NULL)
		{
			::DestroyMenu(m_hMenu);
			m_hMenu = NULL;
		}
	}

public:
	// IContextMenu
#ifdef WIN64
	STDMETHOD(GetCommandString)(UINT_PTR, UINT, UINT*, LPSTR, UINT);
#else
	STDMETHOD(GetCommandString)(UINT, UINT, UINT*, LPSTR, UINT);
#endif
	STDMETHOD(InvokeCommand)(LPCMINVOKECOMMANDINFO);
	STDMETHOD(QueryContextMenu)(HMENU, UINT, UINT , UINT, UINT);

	// IContextMenu2
	STDMETHOD(HandleMenuMsg)(UINT, WPARAM, LPARAM);

	// IContextMenu3
	STDMETHOD(HandleMenuMsg2)(UINT, WPARAM, LPARAM, LRESULT *);

	// IShellExtInit
	//
	STDMETHOD(Initialize)(LPCITEMIDLIST, LPDATAOBJECT, HKEY);

private:
	void MeasureItem(LPMEASUREITEMSTRUCT);
	void DrawMenuItem(LPDRAWITEMSTRUCT);

private:
	CString m_fileName;
	HMENU m_hMenu;
	long m_state;

	std::vector<int> m_commands;

private:
};

OBJECT_ENTRY_AUTO(__uuidof(OBContextMenu), COBContextMenu)
