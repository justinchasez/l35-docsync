// dllmain.h : Declaration of module class.

class COnlineBackupClientShellContextMenuModule : public CAtlDllModuleT< COnlineBackupClientShellContextMenuModule >
{
public :
	DECLARE_LIBID(LIBID_BackupDutyClientShellContextMenuLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ONLINEBACKUPCLIENTSHELLCONTEXTMENU, "{D805C3A0-6E1B-4B9B-8B28-D0C0E79853F0}")
};

extern class COnlineBackupClientShellContextMenuModule _AtlModule;
