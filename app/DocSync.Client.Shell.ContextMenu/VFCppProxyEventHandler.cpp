#include "StdAfx.h"
#include "VFCppProxyEventHandler.h"

bool CVFCppProxyEventHandler::DisplayColorDots = true;

CVFCppProxyEventHandler::CVFCppProxyEventHandler()
{
	try
	{
		m_proxy.CreateInstance(CLSID_ShellCppProxy,NULL,CLSCTX_LOCAL_SERVER);
		if (m_proxy)
		{
			HRESULT hr = this->DispEventAdvise(m_proxy);
			hr = S_OK;
		}
	}
	catch(...)	{/*do nothing*/}
}

CVFCppProxyEventHandler::~CVFCppProxyEventHandler()
{
	try
	{
		if (m_proxy)
		{
			this->DispEventUnadvise(m_proxy);
		}
	}
	catch(...)	{/*do nothing*/}
}