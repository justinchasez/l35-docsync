#pragma once
class ATL_NO_VTABLE CVFCppProxyEventHandler : 
	public IDispEventImpl<0, CVFCppProxyEventHandler, &DIID__ICppProxyEvents, &LIBID_BackupDutyClientCppProxyLib, 1, 0>
{
public:
	CVFCppProxyEventHandler();
	~CVFCppProxyEventHandler();

	static bool DisplayColorDots;

BEGIN_SINK_MAP(CVFCppProxyEventHandler)
   SINK_ENTRY_EX(0, DIID__ICppProxyEvents, 0x4, OnUpdateStatusInfo)
END_SINK_MAP()

	HRESULT __stdcall OnUpdateStatusInfo(LONG type, LONG count)
	{
		if (type == 5)
			DisplayColorDots = (count == 1);

		return 0;
	}

private:
	ICppProxyPtr m_proxy;
};

