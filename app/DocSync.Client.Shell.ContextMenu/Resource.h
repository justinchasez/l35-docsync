//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OnlineBackup.Client.Shell.ContextMenu.rc
//
#define IDS_PROJNAME                    100
#define IDR_ONLINEBACKUPCLIENTSHELLCONTEXTMENU 101
#define IDS_ROOTMENUNAME                101
#define IDR_OBCONTEXTMENU               102
#define IDR_NEWMENU                     102
#define IDS_BACKTHISUP                  102
#define IDS_DONOTBACKUP                 103
#define IDS_BACKUPASAP                  104
#define IDS_RESTORE                     105
#define IDS_OPENBACKUPDRIVE             106
#define IDR_OBEXTRACTICON               107
#define IDR_OBICONOVERLAYIDENTIFIER     108
#define IDR_ONLINEBACKUPICONNORMAL      109
#define IDR_ONLINEBACKUPICONMODIFIED    110
#define IDS_COPYLINK                    123
#define IDR_SCHEDULEDFILEMENU           204
#define IDR_PENDINGFOLDERMENU           205
#define IDR_SCHEDULEDFOLDERMENU         206
#define IDI_ICON1                       209
#define IDI_ICON2                       210
#define ID_BACK_BACKTHISUP              32768
#define IDR_PENDINGFILEMENU             32768
#define ID_BACK_DON                     32769
#define ID_BACK_BACK                    32770
#define ID_BACK_DON32771                32771
#define ID_BACK_RESTOREPREVIOUSVERSIONS 32772
#define ID_BACK_RESTOREPREVIOUSVERSIONS32773 32773
#define ID_BACK_DON32774                32774
#define ID_BACK_OPENFOLDERINBACKUPDRIVE 32775
#define ID_BACK_DON32776                32776
#define ID_BACK_OPENFOLDERINBACKUPDRIVE32777 32777
#define ID_DONOTBACKUP                  32778
#define ID_BACKUP                       32779
#define ID_BACKUPASAP                   32780
#define ID_RESTORE                      32781
#define ID_OPENFOLDER                   32782
#define ID_BACK_COPYSHAREDLINK          32783
#define ID_COPYLINK                     32784
#define ID_BACK_COPYSHAREDLINK32785     32785
#define ID_BACK_COPYSHAREDLINK32786     32786
#define ID_BACK_COPYSHAREDLINK32787     32787
#define ID_BACK_COPYSHAREDLINK32788     32788
#define ID_BACK_COPYSHAREDLINK32789     32789
#define ID_COPYSHAREDLINK               32790
#define ID_BACK_COPYSHAREDLINK32791     32791

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        211
#define _APS_NEXT_COMMAND_VALUE         32792
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
