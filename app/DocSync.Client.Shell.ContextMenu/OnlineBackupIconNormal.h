// OnlineBackupIconNormal.h : Declaration of the COnlineBackupIconNormal

#pragma once
#include "resource.h"       // main symbols

#include "OnlineBackupClientShellContextMenu_i.h"
#include "VFCppProxyEventHandler.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

#include "comdef.h"		
#include <AtlCom.h>
#include <ShlObj.h>

#include "config.h"

// COnlineBackupIconNormal

class ATL_NO_VTABLE COnlineBackupIconNormal :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<COnlineBackupIconNormal, &CLSID_OnlineBackupIconNormal>,
	public IDispatchImpl<IOnlineBackupIconNormal, &IID_IOnlineBackupIconNormal, &LIBID_BackupDutyClientShellContextMenuLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IShellIconOverlayIdentifier
{
public:
	COnlineBackupIconNormal()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ONLINEBACKUPICONNORMAL)


BEGIN_COM_MAP(COnlineBackupIconNormal)
	COM_INTERFACE_ENTRY(IOnlineBackupIconNormal)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IShellIconOverlayIdentifier)
END_COM_MAP()



	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:
	// *** IShellIconOverlayIdentifier methods ***
    STDMETHOD (IsMemberOf)(THIS_ LPCWSTR pwszPath, DWORD dwAttrib);
    STDMETHOD (GetOverlayInfo)(THIS_ __out_ecount(cchMax) LPWSTR pwszIconFile, int cchMax, __out int * pIndex, __out DWORD * pdwFlags);
    STDMETHOD (GetPriority)(THIS_ __out int * pIPriority);

private:
	//CVFCppProxyEventHandler _handler;
};

OBJECT_ENTRY_AUTO(__uuidof(OnlineBackupIconNormal), COnlineBackupIconNormal)
