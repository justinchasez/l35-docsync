using System.Net;
using Share430.Client.Core.Entities.Settings;
using Share430.Client.Core.Managers.Logging;
using Share430.Client.Core.Services.Web;
using Share430.Client.Proxy.Rest;

namespace Share430.Client.Proxy
{
    /// <summary>
    /// The web services manager.
    /// </summary>
    public static class ServiceManager
    {
        /// <summary>
        /// The users service.
        /// </summary>
        private static AuthService usersService;

        /// <summary>
        /// The computers service.
        /// </summary>
        private static FolderService computersService;

        /// <summary>
        /// The files service.
        /// </summary>
        private static FileService filesService;
        private static WebProxy Proxy { get; set; }

        private static string Token { get; set; }

        private static object syncObject = new object();

        /// <summary>
        /// Gets the registration service.
        /// </summary>
        /// <value>The registration service.</value>
        public static IUsersService UsersService
        {
            get
            {
                lock (syncObject)
                {
                    if (usersService == null)
                    {
                        LoggingManager.LogInfo("Initialize UsersService");
                        usersService = new AuthService();
                        usersService.SetWebProxy(Proxy);
                    }

                    return usersService;
                }
            }
        }

        /// <summary>
        /// Gets the computers service.
        /// </summary>
        /// <value>The computers service.</value>
        public static IComputersService ComputersService
        {
            get
            {
                lock (syncObject)
                {
                    if (computersService == null)
                    {
                        LoggingManager.LogInfo("Initialize ComputersService");
                        computersService = new FolderService();
                        computersService.SetWebProxy(Proxy);
                        SetCredentials(computersService, Token);
                    }

                    return computersService;
                }
            }
        }

        /// <summary>
        /// Gets the files service.
        /// </summary>
        /// <value>The files service.</value>
        public static IFilesService FilesService
        {
            get
            {
                lock (syncObject)
                {
                    if (filesService == null)
                    {
                        LoggingManager.LogInfo("Initialize FilesService");
                        filesService = new FileService();
                        filesService.SetWebProxy(Proxy);
                        SetCredentials(filesService, Token);
                    }

                    return filesService;
                }
            }
        }

        /// <summary>
        /// Initializes the specified credential.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public static void Initialize(SettingsInfo settings)
        {
            if (settings.IsProxyServerUsed)
            {
                Proxy = new WebProxy(settings.ProxyServerAddress, settings.ProxyServerPort);
            }

            Token = settings.Credentials.Token;

            lock (syncObject)
            {
                if (filesService != null)
                {
                    filesService.SetWebProxy(Proxy);
                    SetCredentials(filesService, Token);
                }


                if (computersService != null)
                {
                    computersService.SetWebProxy(Proxy);
                    SetCredentials(computersService, Token);
                }
            }
        }

        /// <summary>
        /// Get current Credentials
        /// </summary>
        /// <returns>return UserCredential object</returns>
        public static string GetCredentials()
        {
            return Token;
        }

        /// <summary>
        /// Sets the credentials.
        /// </summary>
        /// <param name="service">The serivice.</param>
        /// <param name="credential">The credential.</param>
        private static void SetCredentials(IServiceBase service, string token)
        {
            service.SetToken(token);
        }
    }
}
