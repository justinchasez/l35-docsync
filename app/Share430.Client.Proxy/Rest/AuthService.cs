﻿using System;
using RestSharp;
using Share430.Client.Core.Entities.Users;
using Share430.Client.Core.Services.Web;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Proxy.Rest
{
    public class AuthService : BaseService, IUsersService
    {
        public UserRegistrationResult Register(UserRegistrationInputs inputs)
        {
            throw new NotImplementedException();
        }

        public bool ValidateUser(string name, string password, out string token)
        {
            var request = this.GetRequest("auth", Method.POST);
            request.AddBody(new LoginViewModel() {UserName = name, Password = password});
            var result = this.restClient.Execute<MembershipResult>(request);
            if (result.Data.IsSuccess)
            {
                token = result.Data.Token;
                return true;
            }

            token = null;
            return false;
        }
    }
}
