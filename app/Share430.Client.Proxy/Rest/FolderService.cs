﻿using System;
using System.Collections.Generic;
using RestSharp;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Services.Web;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Proxy.Rest
{
    public class FolderService : BaseService, IComputersService
    {
        public FolderModel[] GetComputers()
        {
            var request = this.GetRequest("folder", Method.GET);
            var response = this.restClient.Execute<FolderModel>(request);

            return new FolderModel[] {response.Data};
        }

        public string AddComputer(ComputerInfo computerInfo)
        {
            throw new NotImplementedException();
        }

        public ComputerUsersResult GetComputerUsers(ComputerInfo computerInfo)
        {
          //  throw new NotImplementedException();
            return new ComputerUsersResult();
        }

        public List<string> GetAllFolders()
        {
            var request = this.GetRequest("folder/list", Method.GET);

            var response = this.restClient.Execute<List<string>>(request);

            return response.Data;
        }

        public void CreateFolder(string folderName)
        {
            var request = this.GetRequest("folder/create", Method.POST);
            request.AddParameter("folderName", folderName, ParameterType.QueryString);

            this.restClient.Execute(request);
        }

        public void RemoveFolder(string folderName)
        {
            var request = this.GetRequest("folder/remove", Method.DELETE);
            request.AddParameter("folderName", folderName, ParameterType.QueryString);

            this.restClient.Execute(request);
        }

        public SyncronizeUsersResult SyncronizeUsers(SyncronizeUsersInputs syncronizeUsersInputs)
        {
          //  throw new NotImplementedException();
            return new SyncronizeUsersResult { IsSyncronized = true};
        }

        public void UpdateComputerGuid(UpdateComputerGuidInputs inputs)
        {
           // throw new NotImplementedException();
        }

        public UpdatesModel GetUpdates(DateTime lastDate)
        {
            var request = this.GetRequest("folder/getupdates", Method.GET);
            request.AddParameter("lastDate", lastDate);
            var response = this.restClient.Execute<UpdatesModel>(request);

            return response.Data;
        }
    }
}
