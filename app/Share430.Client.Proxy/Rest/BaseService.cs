﻿using System.Configuration;
using System.Net;
using RestSharp;

namespace Share430.Client.Proxy.Rest
{
    public abstract class BaseService
    {
        protected RestClient restClient;
        private string tokenValue;

        /// <summary>
        /// The registreation service url settings key.
        /// </summary>
        private const string SERVICE_URL_KEY = "Settings.ServiceUrl";

        private const string AuthorizeTokenName = "AuthToken";

        public BaseService()
        {
            var url = ConfigurationManager.AppSettings[SERVICE_URL_KEY];
            this.restClient = new RestClient(url + "api/");
        }

        public void SetWebProxy(WebProxy proxy)
        {

        }

        public void SetToken(string token)
        {
            this.tokenValue = token;
        }

        public RestRequest GetRequest(string route, Method method)
        {
            var request = new RestRequest(route, method);
            if (!string.IsNullOrEmpty(tokenValue))
            {
                request.AddHeader(AuthorizeTokenName, tokenValue);
            }

            request.RequestFormat = DataFormat.Json;
            return request;
        }
    }
}