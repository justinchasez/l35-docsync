﻿using System;
using System.Collections.Generic;
using System.Net;
using RestSharp;
using Share430.Client.Core.Entities.Files;
using Share430.Client.Core.Services.Web;
using Share430.Client.Core.Services.Web.Models;

namespace Share430.Client.Proxy.Rest
{
    public class FileService : BaseService, IFilesService
    {
        public BackupFileResultModel BackupFile(BackupFileRequestModel backupFileInputs)
        {
            var request = this.GetRequest("file/backupFile", Method.POST);
            request.AddBody(backupFileInputs);
            var response = this.restClient.Execute<BackupFileResultModel>(request);

            return response.Data;
        }

        public void BackupPartFinished(BackupPartFinishedRequestModel backupFinishedInputs)
        {
            var request = this.GetRequest("file/backupPartFinished", Method.POST);
            request.AddBody(backupFinishedInputs);
            this.restClient.Execute(request);
        }


        public void BackupFileFinished(BackupFileFinishedInputs backupFinishedInputs)
        {
            var request = this.GetRequest("file/backupFileFinished", Method.POST);
            request.AddParameter("fileName", backupFinishedInputs.File, ParameterType.QueryString);
            this.restClient.Execute(request);
        }

        public RestoreFileResponseModel RestoreFile(RestoreFileRequestModel restoreFileInputs)
        {
            var request = this.GetRequest("file/restoreFile", Method.POST);
            request.AddBody(restoreFileInputs);
            var response = this.restClient.Execute<RestoreFileResponseModel>(request);

            return response.Data;
        }

        FileModel IFilesService.GetFileInfo(GetFileInfoInputs fileInfoInputs)
        {
            var request = this.GetRequest("file", Method.GET);
            request.AddParameter("fileName", fileInfoInputs.File);
            var response = this.restClient.Execute<FileModel>(request);

            return response.Data;
        }

        public ComputerFilesDto GetAllFilesFromComputer(GetAllFilesFromComputerInputs inputs)
        {
            var request = this.GetRequest("folder/files", Method.GET);

            var response = this.restClient.Execute<List<FileModel>>(request);

            return new ComputerFilesDto
            {
                    FilesInfo = response.Data
            };
        }

        public RemovingFileResult RemoveFile(GetFileInfoInputs inputs)
        {
            var request = this.GetRequest("file", Method.DELETE);
            request.AddParameter("fileName", inputs.File);
            var response = this.restClient.Execute(request);

            return new RemovingFileResult {IsSuccess = response.StatusCode != HttpStatusCode.InternalServerError && response.StatusCode != HttpStatusCode.BadRequest};
        }

        public string GetFileLink(BackupFileInputs inputs)
        {
            throw new NotImplementedException();
        }
    }
}
