

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* at Fri Jan 17 15:42:59 2014
 */
/* Compiler settings for OnlineBackupClientCppProxy.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 8.00.0603 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __OnlineBackupClientCppProxy_i_h__
#define __OnlineBackupClientCppProxy_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __ICppProxy_FWD_DEFINED__
#define __ICppProxy_FWD_DEFINED__
typedef interface ICppProxy ICppProxy;

#endif 	/* __ICppProxy_FWD_DEFINED__ */


#ifndef ___ICppProxyEvents_FWD_DEFINED__
#define ___ICppProxyEvents_FWD_DEFINED__
typedef interface _ICppProxyEvents _ICppProxyEvents;

#endif 	/* ___ICppProxyEvents_FWD_DEFINED__ */


#ifndef __CppProxy_FWD_DEFINED__
#define __CppProxy_FWD_DEFINED__

#ifdef __cplusplus
typedef class CppProxy CppProxy;
#else
typedef struct CppProxy CppProxy;
#endif /* __cplusplus */

#endif 	/* __CppProxy_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __ICppProxy_INTERFACE_DEFINED__
#define __ICppProxy_INTERFACE_DEFINED__

/* interface ICppProxy */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ICppProxy;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D58CA9CC-83E9-49F1-87A6-B1CECF992B53")
    ICppProxy : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Notify( 
            /* [in] */ LONG id,
            /* [in] */ BSTR value) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckFile( 
            /* [in] */ BSTR fullPath,
            /* [out] */ LONG *state,
            /* [out] */ LONG *verCount) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE NotifyCommand( 
            /* [in] */ LONG cmd,
            /* [in] */ LONG window,
            /* [in] */ BSTR fullPath) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE UpdateStatusInfo( 
            /* [in] */ LONG type,
            /* [in] */ LONG count) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CheckStatusInfo( void) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetEntries( 
            /* [in] */ BSTR fullPath,
            /* [in] */ LONG type,
            /* [out] */ VARIANT *entries) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CheckOption( 
            /* [in] */ LONG option,
            /* [out] */ LONG *value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ContainsState( 
            /* [in] */ BSTR fullPath,
            /* [in] */ LONG state,
            /* [out] */ LONG *result) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CheckSettings( 
            /* [in] */ LONG option,
            /* [out] */ BSTR *value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetRecoveryData( 
            /* [in] */ BSTR Name,
            /* [out] */ BSTR *Destination,
            /* [out] */ BSTR *state,
            /* [out] */ BSTR *priority,
            /* [out] */ DATE *lastRestored) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CancelRestore( 
            /* [in] */ BSTR fullPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE RestartRestoreJob( 
            /* [in] */ BSTR source,
            /* [in] */ BSTR destination) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SetRestorePriority( 
            /* [in] */ LONG priority,
            /* [in] */ BSTR fullPath) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetStatusInfo( 
            /* [in] */ LONG type,
            /* [out] */ LONG *value) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE CheckAutoBackup( 
            /* [in] */ BSTR fullPath,
            /* [out] */ LONG *result) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE CopySharedLink( 
            /* [in] */ BSTR value,
            /* [out] */ BSTR *sLink) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct ICppProxyVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ICppProxy * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ICppProxy * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ICppProxy * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ICppProxy * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ICppProxy * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ICppProxy * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ICppProxy * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Notify )( 
            ICppProxy * This,
            /* [in] */ LONG id,
            /* [in] */ BSTR value);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckFile )( 
            ICppProxy * This,
            /* [in] */ BSTR fullPath,
            /* [out] */ LONG *state,
            /* [out] */ LONG *verCount);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *NotifyCommand )( 
            ICppProxy * This,
            /* [in] */ LONG cmd,
            /* [in] */ LONG window,
            /* [in] */ BSTR fullPath);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *UpdateStatusInfo )( 
            ICppProxy * This,
            /* [in] */ LONG type,
            /* [in] */ LONG count);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CheckStatusInfo )( 
            ICppProxy * This);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetEntries )( 
            ICppProxy * This,
            /* [in] */ BSTR fullPath,
            /* [in] */ LONG type,
            /* [out] */ VARIANT *entries);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CheckOption )( 
            ICppProxy * This,
            /* [in] */ LONG option,
            /* [out] */ LONG *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *ContainsState )( 
            ICppProxy * This,
            /* [in] */ BSTR fullPath,
            /* [in] */ LONG state,
            /* [out] */ LONG *result);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CheckSettings )( 
            ICppProxy * This,
            /* [in] */ LONG option,
            /* [out] */ BSTR *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetRecoveryData )( 
            ICppProxy * This,
            /* [in] */ BSTR Name,
            /* [out] */ BSTR *Destination,
            /* [out] */ BSTR *state,
            /* [out] */ BSTR *priority,
            /* [out] */ DATE *lastRestored);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CancelRestore )( 
            ICppProxy * This,
            /* [in] */ BSTR fullPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *RestartRestoreJob )( 
            ICppProxy * This,
            /* [in] */ BSTR source,
            /* [in] */ BSTR destination);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *SetRestorePriority )( 
            ICppProxy * This,
            /* [in] */ LONG priority,
            /* [in] */ BSTR fullPath);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *GetStatusInfo )( 
            ICppProxy * This,
            /* [in] */ LONG type,
            /* [out] */ LONG *value);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *CheckAutoBackup )( 
            ICppProxy * This,
            /* [in] */ BSTR fullPath,
            /* [out] */ LONG *result);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *CopySharedLink )( 
            ICppProxy * This,
            /* [in] */ BSTR value,
            /* [out] */ BSTR *sLink);
        
        END_INTERFACE
    } ICppProxyVtbl;

    interface ICppProxy
    {
        CONST_VTBL struct ICppProxyVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ICppProxy_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ICppProxy_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ICppProxy_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ICppProxy_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ICppProxy_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ICppProxy_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ICppProxy_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ICppProxy_Notify(This,id,value)	\
    ( (This)->lpVtbl -> Notify(This,id,value) ) 

#define ICppProxy_CheckFile(This,fullPath,state,verCount)	\
    ( (This)->lpVtbl -> CheckFile(This,fullPath,state,verCount) ) 

#define ICppProxy_NotifyCommand(This,cmd,window,fullPath)	\
    ( (This)->lpVtbl -> NotifyCommand(This,cmd,window,fullPath) ) 

#define ICppProxy_UpdateStatusInfo(This,type,count)	\
    ( (This)->lpVtbl -> UpdateStatusInfo(This,type,count) ) 

#define ICppProxy_CheckStatusInfo(This)	\
    ( (This)->lpVtbl -> CheckStatusInfo(This) ) 

#define ICppProxy_GetEntries(This,fullPath,type,entries)	\
    ( (This)->lpVtbl -> GetEntries(This,fullPath,type,entries) ) 

#define ICppProxy_CheckOption(This,option,value)	\
    ( (This)->lpVtbl -> CheckOption(This,option,value) ) 

#define ICppProxy_ContainsState(This,fullPath,state,result)	\
    ( (This)->lpVtbl -> ContainsState(This,fullPath,state,result) ) 

#define ICppProxy_CheckSettings(This,option,value)	\
    ( (This)->lpVtbl -> CheckSettings(This,option,value) ) 

#define ICppProxy_GetRecoveryData(This,Name,Destination,state,priority,lastRestored)	\
    ( (This)->lpVtbl -> GetRecoveryData(This,Name,Destination,state,priority,lastRestored) ) 

#define ICppProxy_CancelRestore(This,fullPath)	\
    ( (This)->lpVtbl -> CancelRestore(This,fullPath) ) 

#define ICppProxy_RestartRestoreJob(This,source,destination)	\
    ( (This)->lpVtbl -> RestartRestoreJob(This,source,destination) ) 

#define ICppProxy_SetRestorePriority(This,priority,fullPath)	\
    ( (This)->lpVtbl -> SetRestorePriority(This,priority,fullPath) ) 

#define ICppProxy_GetStatusInfo(This,type,value)	\
    ( (This)->lpVtbl -> GetStatusInfo(This,type,value) ) 

#define ICppProxy_CheckAutoBackup(This,fullPath,result)	\
    ( (This)->lpVtbl -> CheckAutoBackup(This,fullPath,result) ) 

#define ICppProxy_CopySharedLink(This,value,sLink)	\
    ( (This)->lpVtbl -> CopySharedLink(This,value,sLink) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ICppProxy_INTERFACE_DEFINED__ */



#ifndef __BackupDutyClientCppProxyLib_LIBRARY_DEFINED__
#define __BackupDutyClientCppProxyLib_LIBRARY_DEFINED__

/* library BackupDutyClientCppProxyLib */
/* [version][uuid] */ 


EXTERN_C const IID LIBID_BackupDutyClientCppProxyLib;

#ifndef ___ICppProxyEvents_DISPINTERFACE_DEFINED__
#define ___ICppProxyEvents_DISPINTERFACE_DEFINED__

/* dispinterface _ICppProxyEvents */
/* [uuid] */ 


EXTERN_C const IID DIID__ICppProxyEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("D12762BC-F528-4D9A-ADDB-69E3B9160E1C")
    _ICppProxyEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _ICppProxyEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _ICppProxyEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _ICppProxyEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _ICppProxyEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _ICppProxyEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _ICppProxyEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _ICppProxyEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _ICppProxyEvents * This,
            /* [annotation][in] */ 
            _In_  DISPID dispIdMember,
            /* [annotation][in] */ 
            _In_  REFIID riid,
            /* [annotation][in] */ 
            _In_  LCID lcid,
            /* [annotation][in] */ 
            _In_  WORD wFlags,
            /* [annotation][out][in] */ 
            _In_  DISPPARAMS *pDispParams,
            /* [annotation][out] */ 
            _Out_opt_  VARIANT *pVarResult,
            /* [annotation][out] */ 
            _Out_opt_  EXCEPINFO *pExcepInfo,
            /* [annotation][out] */ 
            _Out_opt_  UINT *puArgErr);
        
        END_INTERFACE
    } _ICppProxyEventsVtbl;

    interface _ICppProxyEvents
    {
        CONST_VTBL struct _ICppProxyEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _ICppProxyEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _ICppProxyEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _ICppProxyEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _ICppProxyEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _ICppProxyEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _ICppProxyEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _ICppProxyEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___ICppProxyEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_CppProxy;

#ifdef __cplusplus

class DECLSPEC_UUID("5478CC67-DE5E-42FB-8F93-282654D6FF13")
CppProxy;
#endif
#endif /* __BackupDutyClientCppProxyLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


