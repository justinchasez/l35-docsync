// CppProxy.h : Declaration of the CCppProxy

#pragma once
#include "resource.h"       // main symbols



#include "OnlineBackupClientCppProxy_i.h"
#include "_ICppProxyEvents_CP.h"



using namespace ATL;


// CCppProxy

class ATL_NO_VTABLE CCppProxy :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<CCppProxy, &CLSID_CppProxy>,
	public IConnectionPointContainerImpl<CCppProxy>,
	public CProxy_ICppProxyEvents<CCppProxy>,
	public IDispatchImpl<ICppProxy, &IID_ICppProxy, &LIBID_BackupDutyClientCppProxyLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCppProxy()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CPPPROXY)


BEGIN_COM_MAP(CCppProxy)
	COM_INTERFACE_ENTRY(ICppProxy)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CCppProxy)
	CONNECTION_POINT_ENTRY(__uuidof(_ICppProxyEvents))
END_CONNECTION_POINT_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

public:

	STDMETHOD(Notify)(LONG id, BSTR value);
	STDMETHOD(CheckFile)(BSTR fullPath, LONG* state, LONG* verCount);
	STDMETHOD(NotifyCommand)(LONG cmd, LONG window, BSTR fullPath);
	STDMETHOD(UpdateStatusInfo)(LONG type, LONG count);
	STDMETHOD(CheckStatusInfo)();
	STDMETHOD(GetEntries)(BSTR fullPath, LONG type, VARIANT* entries);
	STDMETHOD(CheckOption)(LONG option, LONG* value);

	STDMETHOD(ContainsState)(BSTR fullPath, LONG state, LONG* result);
	STDMETHOD(CheckSettings)(LONG option, BSTR* value);
	STDMETHOD(GetRecoveryData)(BSTR Name, BSTR* Destination, BSTR* state, BSTR* priority, DATE* lastRestored);
	STDMETHOD(CancelRestore)(BSTR fullPath);
	STDMETHOD(RestartRestoreJob)(BSTR source, BSTR destination);
	STDMETHOD(SetRestorePriority)(LONG priority, BSTR fullPath);
	STDMETHOD(GetStatusInfo)(LONG type, LONG* value);
	STDMETHOD(CheckAutoBackup)(BSTR fullPath, LONG* result);
	STDMETHOD(CopySharedLink)(BSTR fullPath, BSTR* sLink);
};

OBJECT_ENTRY_AUTO(__uuidof(CppProxy), CCppProxy)
