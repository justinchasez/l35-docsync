// CppProxy.cpp : Implementation of CCppProxy

#include "stdafx.h"
#include "CppProxy.h"


// CCppProxy
STDMETHODIMP CCppProxy::CheckOption(LONG option, LONG* value)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::Notify(LONG id, BSTR value)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::CheckFile(BSTR fullPath, LONG* state, LONG* verCount)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::NotifyCommand(LONG cmd, LONG window, BSTR fullPath)
{
	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::UpdateStatusInfo(LONG type, LONG count)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::CheckStatusInfo()
{
	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::GetEntries(BSTR fullPath, LONG type, VARIANT* entries)
{
	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::ContainsState(BSTR fullPath, LONG state, LONG* result)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::CheckSettings(LONG option, BSTR* value)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::GetRecoveryData(BSTR Name, BSTR* Destination, BSTR* state, BSTR* priority, DATE* lastRestored)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::CancelRestore(BSTR fullPath)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::RestartRestoreJob(BSTR source, BSTR destination)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::SetRestorePriority(LONG priority, BSTR fullPath)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::GetStatusInfo(LONG type, LONG* value)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}


STDMETHODIMP CCppProxy::CheckAutoBackup(BSTR fullPath, LONG* result)
{
	// TODO: Add your implementation code here

	return E_NOTIMPL;
}

STDMETHODIMP CCppProxy::CopySharedLink(BSTR fullPath, BSTR* sLink)
{
	return E_NOTIMPL;
}
