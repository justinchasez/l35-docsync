// dllmain.h : Declaration of module class.

class COnlineBackupClientCppProxyModule : public ATL::CAtlDllModuleT< COnlineBackupClientCppProxyModule >
{
public :
	DECLARE_LIBID(LIBID_BackupDutyClientCppProxyLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_ONLINEBACKUPCLIENTCPPPROXY, "{B5238403-CF7B-45AD-AC78-74FA70636A4B}")
};

extern class COnlineBackupClientCppProxyModule _AtlModule;
