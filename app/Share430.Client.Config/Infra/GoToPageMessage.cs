namespace Share430.Client.Config.Infra
{
    public class GoToPageMessage
    {
        public string PageName { get; set; }
    }
}