﻿using System.Text;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Messaging;
using Share430.Client.Config.Infra;

namespace Share430.Client.Config
{
    /// <summary>
    /// Interaction logic for Start.xaml
    /// </summary>
    public partial class Start : NavigationWindow
    {
        public Start()
        {
            InitializeComponent();
            Messenger.Default.Register<GoToPageMessage>(this, (action) => ReceiveMessage(action));

            Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Main" });
        }

        
        private object ReceiveMessage(GoToPageMessage action)
        {
            var sb = new StringBuilder("/Views/");
            sb.Append(action.PageName);
            sb.Append("View.xaml");
            this.Navigate(
               new System.Uri(sb.ToString(),
                     System.UriKind.Relative));
            return null;
        }
    }
}
