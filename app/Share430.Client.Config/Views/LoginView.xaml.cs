﻿using System.Windows.Controls;

namespace Share430.Client.Config.Views
{
    /// <summary>
    /// Description for LoginView.
    /// </summary>
    public partial class LoginView : Page
    {
        /// <summary>
        /// Initializes a new instance of the LoginView class.
        /// </summary>
        public LoginView()
        {
            InitializeComponent();
        }
    }
}