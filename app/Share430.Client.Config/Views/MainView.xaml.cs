﻿using System.Windows.Controls;

namespace Share430.Client.Config.Views
{
    /// <summary>
    /// Description for MainView.
    /// </summary>
    public partial class MainView : Page
    {
        /// <summary>
        /// Initializes a new instance of the MainView class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
        }           

    }
}