﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Share430.Client.Config.Core.Managers;
using Share430.Client.Config.Infra;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Remoting.Args;

namespace Share430.Client.Config.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        /// <summary>
        /// The <see cref="SyncFolder" /> property's name.
        /// </summary>
        public const string SyncFolderPropertyName = "SyncFolder";
        public const string RestoreStatusPropertyName = "RestoreStatus";
        public const string RestoreProgressPropertyName = "RestoreProgress";
        public const string BackupStatusStatusPropertyName = "BackupStatus";
        public const string BackupProgressPropertyName = "BackupProgress";

        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string SyncFolder
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(SyncFolderPropertyName);
            }
        }

         public string RestoreStatus { get; set; }
         public string BackupStatus { get; set; }

         public string RestoreProgress { get; set; }
         public string BackupProgress { get; set; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (ClientSettingsManager.Settings.IsFirstRun)
            {
                Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Login" });
            }

            this.Update = new RelayCommand(this.UpdateCommand);
            this.Logout = new RelayCommand(this.LogoutCommand);
            this.GoToSettings = new RelayCommand(this.GoToSettingsCommand);
            this.SyncFolder = ClientSettingsManager.Settings.TargetFolderLocation;
            this.RestoreStatus = string.Empty;
            this.BackupStatus = string.Empty;
            RemotingManager.Instance.ProxyPublisher.BackupStatusChanged += ProxyPublisherOnBackupStatusChanged;
            RemotingManager.Instance.ProxyPublisher.RestoreStatusChanged += ProxyPublisherOnRestoreStatusChanged;
            RemotingManager.Instance.ProxyPublisher.RestoreProgressChanged += ProxyPublisher_RestoreProgressChanged;
            RemotingManager.Instance.ProxyPublisher.BackupProgressChanged += ProxyPublisher_BackupProgressChanged;
            Messenger.Default.Register<GoToPageMessage>(this, ReceiveMessage);
        }

        void ProxyPublisher_BackupProgressChanged(ProgressChangedEventArgs args)
        {
            var p = args.CurrentProgress;
            if (string.IsNullOrEmpty(p.CurrentFilePath))
            {
                BackupProgress = string.Empty;
            }
            else if (p.FilesCompleted + p.FilesPending == 0)
            {
                BackupProgress = string.Format("{0} - {1}%", this.GetRelativePath(p.CurrentFilePath), p.Percentage);
            } 
            else
            {
                BackupProgress = string.Format("{0} - {1}% ({2}/{3})", this.GetRelativePath(p.CurrentFilePath), p.Percentage,
                    p.FilesCompleted, p.FilesPending + p.FilesCompleted);
            }
            RaisePropertyChanged(BackupProgressPropertyName);
        }

        private void ProxyPublisher_RestoreProgressChanged(ProgressChangedEventArgs args)
        {
            var p = args.CurrentProgress;
            if (string.IsNullOrEmpty(p.CurrentFilePath))
            {
                RestoreProgress = string.Empty;
            }
            else if (p.FilesTotal == 0)
            {
                RestoreProgress = string.Format("{0} - {1}%", this.GetRelativePath(p.CurrentFilePath), p.Percentage);
            } 
            else
            {
                RestoreProgress = string.Format("{0} - {1}% ({2}/{3})", this.GetRelativePath(p.CurrentFilePath), p.Percentage,
                    p.FilesCompleted, p.FilesTotal);
            }
            RaisePropertyChanged(RestoreProgressPropertyName);
        }

        private void ReceiveMessage(GoToPageMessage action)
        {
            if (action.PageName == "Main")
            {
                this.SyncFolder = ClientSettingsManager.Settings.TargetFolderLocation;
                this.RestoreStatus = string.Empty;
                this.BackupStatus = string.Empty;
            }
        }

        private void LogoutCommand()
        {
            ClientSettingsManager.Reset();
            ServiceController serviceController = new ServiceController("Share430Service");
            serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_STOP);
                                                                                 
            Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Login" });
        }

        private void GoToSettingsCommand()
        {
            Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Settings" });
        }

        private void ProxyPublisherOnRestoreStatusChanged(RestoreStatusChangedEventArgs args)
        {
            RestoreStatus = args.Status.ToString();
            RestoreStatus = RestoreStatus == "Empty" ? string.Empty : RestoreStatus;
            RaisePropertyChanged(RestoreStatusPropertyName);
        }

        private void ProxyPublisherOnBackupStatusChanged(BackupStatusChangedEventArgs args)
        {
            BackupStatus = args.Status.ToString();
            BackupStatus = BackupStatus == "Empty" ? string.Empty : BackupStatus;
            RaisePropertyChanged(BackupStatusStatusPropertyName);
        }

        private void UpdateCommand()
        {
            try
            {
                Path.GetFullPath(this.SyncFolder);
            }
            catch
            {
                MessageBox.Show("Invalid windows path");
                return;
            }
            if (!Path.IsPathRooted(this.SyncFolder))
            {
                MessageBox.Show("Path should be absolute");
                return;
            }

            if (ClientSettingsManager.Settings.TargetFolderLocation != this.SyncFolder)
            {
                RemotingManager.Instance.ProxyManager.ScheduledFileManager.DontBackThisUp(
                    ClientSettingsManager.Settings.TargetFolderLocation);

                ClientSettingsManager.Settings.TargetFolderLocation = this.SyncFolder;
                ClientSettingsManager.Settings.LastUpdateDate = DateTime.UtcNow;

                ClientSettingsManager.Save();
            }
            Task.Factory.StartNew(() =>
            {
                RemotingManager.Instance.ProxyManager.ScheduledFileManager.BackThisUpCommand(this.SyncFolder);
                RemotingManager.Instance.ProxyManager.BackupManager.RestoreComputer(0,
                    ClientSettingsManager.Settings.CurrentComputerEncryptInfo);
                ServiceController serviceController = new ServiceController("Share430Service");
                serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_START_FOLDER);
            });
        }

        private string GetRelativePath(string path)
        {
            var basePath = SettingsManager.Settings.TargetFolderLocation;
            if (!path.StartsWith(basePath, StringComparison.InvariantCultureIgnoreCase))
            {
                return path;
            }

            return path.Substring(basePath.Length).Trim('\\');
        }

        public RelayCommand Update { get; private set; }

        public RelayCommand GoToSettings { get; private set; }

        public RelayCommand Logout { get; private set; }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}