﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Share430.Client.Config.Core.Managers;
using Share430.Client.Config.Infra;

namespace Share430.Client.Config.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// </summary>
    public class SettingsViewModel : ViewModelBase
    {
        public const string AddNewPropertyName = "AddNew";

        private string _addNew = string.Empty;

        /// <summary>
        /// Gets the AddNew property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string AddNew
        {
            get
            {
                return _addNew;
            }

            set
            {
                if (_addNew == value)
                {
                    return;
                }

                _addNew = value;
                RaisePropertyChanged(AddNewPropertyName);
            }
        }

        public ObservableCollection<string> IncludedLocations { get; set; }
        public ObservableCollection<string> ExcludedLocations { get; set; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
         public SettingsViewModel()
        {
            this.Include = new RelayCommand(this.IncludeCommand);
            this.Exclude = new RelayCommand(this.ExcludeCommand);
            this.ReturnToMain = new RelayCommand(this.ReturnToMainCommand);
            this.RemoveFromInclude = new RelayCommand<string>(this.RemoveFromIncludeCommand);
            this.RemoveFromExclude = new RelayCommand<string>(this.RemoveFromExcludeCommand);
            this.AddNew = "";
            ClientSettingsManager.Settings.IncludedLocations = ClientSettingsManager.Settings.IncludedLocations ??
                                                               new List<string>();
            ClientSettingsManager.Settings.ExcludedLocations = ClientSettingsManager.Settings.ExcludedLocations ??
                                                               new List<string>();
            this.IncludedLocations = new ObservableCollection<string>(ClientSettingsManager.Settings.IncludedLocations);
            this.ExcludedLocations = new ObservableCollection<string>(ClientSettingsManager.Settings.ExcludedLocations);
        }

        private void RemoveFromExcludeCommand(string obj)
        {
            this.ExcludedLocations.Remove(obj);
            ClientSettingsManager.Settings.ExcludedLocations.Remove(obj);
            ClientSettingsManager.Save();
        }

        private void RemoveFromIncludeCommand(string obj)
        {
            this.IncludedLocations.Remove(obj);
            ClientSettingsManager.Settings.IncludedLocations.Remove(obj);
            ClientSettingsManager.Save();
        }

        private void ReturnToMainCommand()
        {
            Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Main" });
        }

        private void IncludeCommand()
         {
            if (string.IsNullOrEmpty(AddNew) || this.IncludedLocations.Contains(this.AddNew))
            {
                return;
            }

             this.IncludedLocations.Add(this.AddNew);
            ClientSettingsManager.Settings.IncludedLocations.Add(this.AddNew);
            ClientSettingsManager.Save();
             this.AddNew = string.Empty;
         }

         private void ExcludeCommand()
         {
             if (string.IsNullOrEmpty(AddNew) || this.ExcludedLocations.Contains(this.AddNew))
             {
                 return;
             }


             this.ExcludedLocations.Add(this.AddNew);
             ClientSettingsManager.Settings.ExcludedLocations.Add(this.AddNew);
             ClientSettingsManager.Save();
             this.AddNew = string.Empty;
         }

        public RelayCommand Include { get; private set; }
        public RelayCommand Exclude { get; private set; }
        public RelayCommand ReturnToMain { get; private set; }
        public RelayCommand<string> RemoveFromInclude { get; private set; }
        public RelayCommand<string> RemoveFromExclude { get; private set; }
    }
}