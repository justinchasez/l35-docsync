﻿using System;
using System.ServiceProcess;
using System.Threading;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Share430.Client.Config.Core.Managers;
using Share430.Client.Config.Infra;
using Share430.Client.Core.Entities.Computers;
using Share430.Client.Core.Entities.Encryption;
using Share430.Client.Core.Managers.Settings;
using Share430.Client.Core.Utils;

namespace Share430.Client.Config.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class LoginViewModel : ViewModelBase
    {
        private string _userName = string.Empty;
        private string _password = string.Empty;
        private bool isShowError = false;

        public string UserNamePropertyName = "UserName";
        public string PasswordPropertyName = "UserName";
        public string IsFieldsNotEmptyPropertyName = "IsFieldsNotEmpty";
        public string IsShowErrorPropertyName = "IsShowError";
        

        public string UserName
        {
            get
            {
                return _userName;
            }

            set
            {
                if (_userName == value)
                {
                    return;
                }

                _userName = value;
                RaisePropertyChanged(UserNamePropertyName);
                RaisePropertyChanged(IsFieldsNotEmptyPropertyName);
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                if (_password == value)
                {
                    return;
                }

                _password = value;
                RaisePropertyChanged(PasswordPropertyName);
                RaisePropertyChanged(IsFieldsNotEmptyPropertyName);
            }
        }

        public RelayCommand Login { get; private set; }

        public bool IsShowError
        {
            get { return isShowError; }
            set
            {
                if (isShowError == value)
                {
                    return;
                }

                isShowError = value;
                RaisePropertyChanged(IsShowErrorPropertyName);
            }
        }

        public bool IsFieldsNotEmpty
        {
            get { return !string.IsNullOrEmpty(this.UserName) && !string.IsNullOrEmpty(this.Password); }
        }

        /// <summary>
        /// Initializes a new instance of the LoginViewModel class.
        /// </summary>
        public LoginViewModel()
        {
            this.Login = new RelayCommand(this.LoginCommand);
            this.IsShowError = false;
        }

        private void LoginCommand()
        {
            IsShowError = false;
            bool result;
            string token;
            try
            {
                result = RemotingManager.Instance.ProxyManager.UserManager.ValidateUser(this.UserName, this.Password, out token);
            }
            catch (Exception)
            {
                MessageBox.Show("The service for validate user not answer", "Service not answer", MessageBoxButton.OK);
                return;
            }

            if (!result)
            {
                IsShowError = true;
                return;
            }

            ClientSettingsManager.Settings.Credentials.Name = this.UserName;
            ClientSettingsManager.Settings.Credentials.Token = token;
            ClientSettingsManager.Settings.CurrentComputerEncryptInfo = EncryptInfo.LoadDefault();
            ClientSettingsManager.Settings.CurrentComputer = new ComputerInfo();
            ClientSettingsManager.Save();

            RemotingManager.Instance.ProxyManager.BackupManager.ResetInitEvent();
            var computers = RemotingManager.Instance.ProxyManager.ComputerManager.GetComputers();

            ServiceController serviceController = new ServiceController("Share430Service");
            serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_INIT);

            Thread thr = new Thread(() =>
            {
                if (RemotingManager.Instance.ProxyManager.BackupManager.EnsureInited(10000))
                {
                    this.FirstRun();
                }
            });
            thr.Start();

            Messenger.Default.Send<GoToPageMessage>(new GoToPageMessage { PageName = "Main" });
        }

        private void FirstRun()
        {

            ClientSettingsManager.Settings.IsFirstRun = false;

            try
            {
             //   RemotingManager.Instance.ProxyManager.ComputerManager.UpdateComputerGuid(currentComputer.Id);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Client: Can not update computer GUID in server");
            }

            ClientSettingsManager.Save();

            RemotingManager.Instance.ProxyManager.BackupManager.ResetInitEvent();

            ServiceController serviceController = new ServiceController("Share430Service");
            serviceController.ExecuteCommand(SettingsManager.SERVICE_COMMAND_INIT);
        }

        
    }
}