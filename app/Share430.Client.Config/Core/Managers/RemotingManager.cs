using System;
using System.Reflection;
using System.Windows;
using Share430.Client.Core.Utils;
using Share430.Client.Remoting.Managers;
using Share430.Client.Remoting.Managers.Clients;
using Share430.Client.Remoting.Terminals;

namespace Share430.Client.Config.Core.Managers
{
    /// <summary>
    /// The remoting events maanger.
    /// </summary>
    internal class RemotingManager
    {
        #region Constants

        /// <summary>
        /// The remoting port.
        /// </summary>
        private const int REMOTING_PORT = 999;

        /// <summary>
        /// The remoting channel name.
        /// </summary>
		private const string REMOTING_CHANNEL_NAME = "Share430";

        #endregion

        /// <summary>
        /// The single instance of remoting manager.
        /// </summary>
        private static RemotingManager instance;

        /// <summary>
        /// The client remoting terminal.
        /// </summary>
        private ClientTerminal terminal;

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <value>The instance.</value>
        public static RemotingManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RemotingManager();
                }

                return instance;
            }
        }

        /// <summary>
        /// Gets the session manager.
        /// </summary>
        /// <value>The session manager.</value>
        public ProxyManager ProxyManager { get; private set; }

        /// <summary>
        /// Gets the proxy publisher.
        /// </summary>
        /// <value>The proxy publisher.</value>
        public EventProxyClient ProxyPublisher { get; private set; }

        /// <summary>
        /// Prevents a default instance of the <see cref="RemotingManager"/> class from being created.
        /// </summary>
        private RemotingManager()
        {
            this.Connect();
        }

        /// <summary>
        /// Connects this instance.
        /// </summary>
        public void Connect()
        {
            if (this.terminal != null)
            {
                Loger.Instance.Log("Client Remoting manager: Client is already connected!");
                //MessageBox.Show("Client is already connected!");
                return;
            }

            this.terminal = new ClientTerminal();

            try
            {
                this.ProxyManager = this.terminal.Connect<ProxyManager>(REMOTING_PORT, REMOTING_CHANNEL_NAME);
                Loger.Instance.Log("Client Remoting manager: Connection to " + typeof(ProxyManager) + " successfully");
                this.ProxyPublisher = new EventProxyClient(this.terminal.Connect<EventProxy>(REMOTING_PORT, REMOTING_CHANNEL_NAME));
                Loger.Instance.Log("Client Remoting manager: Connection to " + typeof(EventProxy) + " successfully");

                try
                {
                    this.ProxyManager.TestConnection();
                }
                catch (Exception ex)
                {
                    Loger.Instance.Log("Client Remoting manager: Test connection Exception " + ex);
                    this.terminal.Disconnect();

                    throw;
                }
            }
            catch (TargetInvocationException ex)
            {
                Loger.Instance.Log("Client Remoting manager: Exception " + ex);
                MessageBox.Show("Connection cannot be established!\n" + ex.InnerException);
            }
            catch (Exception ex)
            {
                Loger.Instance.Log("Client Remoting manager: Exception " + ex);
                MessageBox.Show("Connection cannot be established!\n" + ex);
            }
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public void Disconnect()
        {
            if (this.terminal == null)
            {
                return;
            }

            try
            {
                this.ProxyPublisher.Dispose();

                this.terminal.Disconnect();
                this.terminal = null;
            }
            catch (Exception)
            {
                // TODO: Log server shutdown error.
            }
        }
    }
}