using Share430.Client.Core.Entities.Settings;

namespace Share430.Client.Config.Core.Managers
{
    /// <summary>
    /// The client wrapper for settings.
    /// </summary>
    internal static class ClientSettingsManager
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private static SettingsInfo settings;

        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <value>The settings.</value>
        internal static SettingsInfo Settings
        {
            get
            {
                if (settings == null)
                {
                    settings = RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
                }

                return settings;
            }
        }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        internal static void Save()
        {
            RemotingManager.Instance.ProxyManager.SettingsManager.SaveSettings(settings);
        }

        internal static void Reset()
        {
            RemotingManager.Instance.ProxyManager.SettingsManager.SaveSettings(null);
            RemotingManager.Instance.ProxyManager.SettingsManager.LoadSettings();
            //foreach (var file in Directory.GetFiles(CoreConstantsHelper.PathForSaveData))
            //{
            //    File.Delete(file);
            //}
        }
    }
}